<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(12)

descricao	=ucase(rec("descricao"))
opcao		=rec("opcao")
grupo		=rec("grupo")

link="cli_CadastroGrupo1.asp"

if opcao="I" then ''incluir
	if len(descricao)=0 then
		session("msg")="Para incluir um Grupo, necess�rio informar a descri��o."
		Response.Redirect link
		Response.end
	end if
	
	strsql="insert into fat_grupo (atividade,grupo,descricao,icmssub,valorminimo,dataatualizacao,desconto,ordem,valorMaximo) " & _
		"select " & session("atividade") & ",isnull(max(grupo),0)+1,'" & descricao & "','n',0,getdate(),0,0,0 from fat_grupo "
	locConnVenda.execute(strsql)
	session("msg")="Grupo inclu�do com sucesso."
elseif opcao="A" then ''alterar
	strsql="select grupo from fat_grupo where atividade=" & session("atividade") 
	set adors1=locconnvenda.execute(strsql)
	do while not adors1.eof
		grupo=adors1("grupo")		
		valor=ucase(rec(grupo))
		
		strsql=	"update fat_grupo set descricao='" & valor & "' " & _
				"where atividade=" & session("atividade") & " and grupo=" & grupo
		locconnvenda.execute(strsql)		
		adors1.movenext
	loop
	set adors1=nothing
	session("msg")="Atualizado com sucesso."

elseif opcao="E" then ''excluir		
	
	strsql=	"select top 1 a.integracao,b.nome " & _
			"from fat_Comercio a inner join fat_Destinatario b on a.destinatario=b.destinatario " & _
			"where a.filial='" & session("emitente") & "' and a.faixa_preco=" & grupo
	set adors1=locconnvenda.execute(strsql)
	if not adors1.eof then
		session("msg")="Existe cliente cadastrado nesse Grupo (" & adors1("integracao") & " - " & adors1("nome") & "), n�o � poss�vel excluir."
		Response.Redirect link
		Response.end
	end if
	set adors1=nothing
	
	strsql="delete fat_faixa_preco where atividade=" & session("atividade") & " and faixa=" & grupo
	locconnvenda.execute(strsql)
	
	strsql="delete fat_Grupo where atividade=" & session("atividade") & " and grupo=" & grupo
	locconnvenda.execute(strsql)
	session("msg")="Grupo exclu�do com sucesso."	
	
end if

Response.Redirect link

%>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->