<%'@LANGUAGE = VBScript.Encode %>
<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%call Verifica_Permissao(10005)%>
<!--#include virtual="public/pub_BodyRel750.asp"-->
<%
reqDest =request("dest")
strAno	=request("ano")
strMes	=request("mes")
strProd	=request("prod")
sac		=request("sac")
pedido	=request("pedido")
entrega	=request("entrega")

completo	=request("completo")
redirec="cli_Atendimento.asp"
redirec1="cli_Atendimento_1.asp"
redirec2="cli_Atendimento_2.asp"

'if cstr(cliente_controle_hierarquia)="1" then
	Response.Redirect redirec2 & "?dest=" & reqDest
'end if

campoQtdValor	=request("campoQtdValor")
if len(trim(campoQtdValor))=0 then campoQtdValor="v"
if campoQtdValor="a" then
	tit="(QUANTIDADE)"
	tit2="valor"
	tit3="v"
else
	tit="(VALOR)"
	tit2="quantidade"
	tit3="a"
end if

if len(trim(strAno))<1 then strAno=year(date)
if len(trim(strMes))<1 then strMes=month(date)

if strProd > "020000" then
	strProd2=mid(strProd,1,2) & "%"
else
	strProd2=mid(strProd,1,3) & "%"
end if
dim idioma_campos(50)
call idioma()
strSql="select integracao=isnull(a.integracao,''),a.nome,a.logradouro,a.endereco,a.numero,a.complemento,a.cidade,a.bairro,a.estado,a.cep,a.contato,a.nome_abreviado," + _
	   "a.fisica_juridica,a.cgc_cpf,a.inscricao_rg,a.email,a.data_cadastro,integracaoCom=isnull(integracaoCom,'')," + _
	   "grupo=d.descricao,a.descricao_status, " & _
	   "a.data_nascimento nasc,a.filialCa,b.cod_depto,b.nome_abreviado NomeFilialCa,ultimaCompra," + _
	   "distrito=isnull(b.distrito,''),divisao=isnull(b.divisao,''),subgrupo=isnull(b.subgrupo,''),b.destinatario eCodDepto, " + _
	   "a.depto,a.nomdepto,a.rota,comosoube,a.site " + _
	   "from view_Cliente a left join view_depto b on a.filialCa=b.destinatario " + _
	   "left join fat_comercio c on a.destinatario=c.destinatario " & _
	   "left join fat_grupo d on c.grupo=d.grupo " & _
	   "where a.destinatario='" & reqDest & "' " & verifica
if session("hierarquia_rede")=1 then	
	sql = "select c=count(0) from fat_Hierarquia_permissao where usuario='" & session("usuario") & "'"
    set adoRs4 = conn1.Execute(sql)
    If Not adoRs4.EOF Then
        If CDbl(adoRs4("c")) > 0 Then
            StrSql = StrSql & " and a.destinatario in(select c.destinatario " & _
                    "from fat_hierarquia_permissao a inner join fat_hierarquia_view b on a.hierarquia=b.hierarquia " & _
                    "inner join view_consultor c on b.permissao=c.hierarquia " & _
                    "where a.usuario='" & session("usuario") & "') "
        End If
    End If
    Set adoRs4 = Nothing		
end if
if session("empresa_id")>="10000001" then
	strsql	=strsql & " and a.depto='" & session("emitente") & "' "
end if
if session("empresa_id")="00000002" and 1=2 then 'ol '''cancelada verifica��o em 02/04/09 = Kleber
	strsqlx=	"select hierarquia " & _
			"from fat_hierarquia_permissao " & _
			"where usuario='" & session("usuario") & "'"
	set adors4=conn1.execute(strsqlx)
	if not adors4.eof then
		strsql=strsql & " and a.destinatario in(select x.destinatario " & _
			"	from fat_Comercio x inner join fat_hierarquia y on x.rota=y.hierarquia " & _
			"	inner join fat_hierarquia_permissao z on y.hierarquia=z.hierarquia " & _
			"	where z.usuario='" & session("usuario") & "' " & _
			") "
	end if
	set adors4=nothing
end if
'call rw(strsql,1)
set adoDest=conn1.Execute(strsql)
if adoDest.eof then
	call redi_aviso("Acesso","N�o foi poss�vel acessar os dados do cliente")
else
	dtUltimaCompra=adoDest("UltimaCompra")
	strIntegracao=trim(adoDest("integracao"))
	strIntegracaoCom=trim(adoDest("integracaoCom"))
	strNome		=trim(adoDest("nome"))
	strNomeAbr	=trim(adoDest("nome_abreviado"))
	strEndereco	=adoDest("logradouro") &  " " &adoDest("endereco") &  " " & adoDest("numero") & " " & adoDest("complemento")
	strBairro	=adoDest("bairro")
	strCidade	=adoDest("cidade") & " - " & adoDest("estado")
	strCep		=adoDest("cep")
	strContato	=adoDest("contato")	
	strGrupo	=adoDest("grupo")	
	strFJ		=adoDest("fisica_juridica")
	strCPF		=adoDest("cgc_cpf")
	strRG		=adoDest("inscricao_rg")
	strEmail	=adoDest("email")
	dtDataCad	=mid(adoDest("data_cadastro"),1,10)
	strDtNasc	=adoDest("nasc")
	strCodDepto	=adoDest("cod_depto")
	strNFilialCa=adoDest("NomeFilialCa")
	strDistrito	=adoDest("distrito")
	strDivisao	=adoDest("divisao")
	strSubGrupo	=adoDest("subGrupo")
	strECodigoDepto=adoDest("eCodDepto")
	strdepto    =adoDest("depto")
	strnomdepto =adoDest("nomdepto")
	rota		=adoDest("rota")
	strComoSoube=adoDest("comosoube")
	strSite		=adoDest("site")
	descricao_status	=adoDest("descricao_status")
	'strCondicaoPagto	=adoDest("descCondicaoPagto")
'	exibe_codigo=adoDest("exibe_codigo")

	regra="0"
	if session("empresa_id")="00000007" then  ''zerar variaveis quando NG (nao exibir os dados).
		strsql=	"select count(0) as c " & _
				"from fat_hierarquia_permissao where usuario='" & session("usuario") & "'"
		set adors1=locconnvenda.execute(strsql)
		if not adors1.eof then
			if adors1("c")>0 then
				regra="1"
				dtUltimaCompra=""
				strEndereco	=""
				strBairro	=""
				strCidade	=""
				strCep		=""
				strContato	=""
				strGrupo	=""
				strFJ		=""
				strCPF		=""
				strRG		=""
				dtDataCad	=""
				strDtNasc	=""
				strCodDepto	=""
				strNFilialCa=""
				strDistrito	=""
				strDivisao	=""
				strSubGrupo	=""
				strECodigoDepto=""
				strdepto    =""
				strnomdepto =""
				'rota		=""
				strComoSoube=""
				strSite		=""
				descricao_status	=""
			end if
		end if
		set adors1=nothing
	end if
end if
adoDest.close

strSql=	"select endereco=isnull(endereco,'') + ' ' + isnull(numero,'') + ' ' + isnull(complemento,''),cidade,bairro,estado,cep " & _
		"from fat_Comercio a inner join fat_Destinatario b on a.local_cobranca=b.destinatario " & _
		"and a.destinatario='" & reqDest & "'"
set adoDest=locConnVenda.execute(strSql)
if not adoDest.eof then
	strendpagto =adoDest("endereco")
	strbaipagto =adoDest("bairro")
	strmunpagto =adoDest("cidade") & " - " & adoDest("estado")
	strceppagto =adoDest("cep")
end if
adoDest.close

strSql = "select * from fat_hierarquia where hierarquia = " & rota
set adoDest=conn1.Execute(strsql)
if not adoDest.eof then strVendedor = adoDest("descricao")
adoDest.close

if len(trim(strSubGrupo))<1 then
	strSql="select cod_depto,depto,distrito,divisao,subgrupo,grupo " + _
		   "from view_depto where destinatario='" & reqDest & "'"
	set adoF=conn1.execute(strSql)
 	if not adoF.eof then
		strCodDepto	 =adoF("cod_depto")
		strNFilialCa =adoF("depto")
		strDistrito	= adoF("distrito")
		strDivisao	= adoF("divisao")
		strSubGrupo	= adoF("subgrupo")
	end if
	adoF.close
end if

strsql = "select descricao, ddd, numero=telefone"
strsql = strsql & "	 from view_telefone"
strsql = strsql & "	 where destinatario = '" & reqDest & "'"
set adoTel=conn1.Execute(strsql)




'response.Write len(strIntegracao) & "--"
if len(trim(strIntegracao))<1 then strIntegracao=strIntegracaoCom
%>
<script>
function AbreLimite(strD)	{
	window.open("../../Manutencao/LimiteCredito/Manu_Credito1.asp?d=" + strD,"DetKit1","scrollbars=yes,toobar=no,width=600,height=320,screenX=0,screenY=0,top=100,left=100");	
						}
function AbreStatus(strD,strEmi)	{
	window.open("../pedido/ListaStatus.asp?p=" + strD + "&Filial=" + strEmi,"ListaStatus","scrollbars=yes,toobar=no,width=600,height=330,screenX=0,screenY=0,top=100,left=100");	
						}										
function AbreSituacao(strTipo,strDoc,strEmite,strD,strUsuario)	{	
	window.open("../cobranca/ocorrencia/cob_OperacaoInt.asp?Tipo=" + strTipo + "&doc=" + strDoc + "&Filial=" + strEmite + "&dest=" + strD + "&xusuario=" + strUsuario,"ListaSituacao","scrollbars=no,toobar=no,width=750,height=520,screenX=0,screenY=0,top=0,left=0");	
						}	
						
function janelaBoleto(documento) { 
	window.open('../pedido/express/ped_gerarBoleto.asp?pedido='+documento+'', 'BOLETO','toolbar=no,menubar=no,resizable=no,status=no,scrollbars=yes,width=675,height=485');
	}						
</script>

<%


 if ( session("empresa_id")="00000007" ) then
  descricaoIndicante = "Patrocinador"
 else
  descricaoIndicante = "Indicante"
 end if




 '' INI - ALTERACOES DE HIERARQUIA - NIVEL SUPERIOR
 
  alterarHierarquia        = replace(trim(request("alterarHierarquia")),"'","") 				
  
  'O usu�rio tem permissao para alterar hierarquia e est� na alteracao				
  if ( Verifica_Permissao_Aplicacao(10314) and alterarHierarquia="1" ) then					
	
	
	destinatarioOrigem        = replace(trim(request("destinatarioOrigem")),"'","")
	confirmaAlterarHierarquia = ucase(replace(trim(request("confirmaAlterarHierarquia")),"'",""))
	novoDestinatario          = replace(trim(request("dest")),"'","")
	 
	'resposta do usu�rio se deseja alterar ou n�o 
	if ( confirmaAlterarHierarquia="S" )then
	
	  '' INI - Rotinas de alteracao do nivel superior
			 'Pega a hierarquia do destinatario	
		strSql="SELECT hierarquia FROM fat_hierarquia WHERE destinatario='" &destinatarioOrigem& "' "
		set adoa=locconnvenda.execute(strSql)
		if not adoa.eof then hierarquiaOrigem = adoa("hierarquia")
		set adoa=nothing
			   
		'Pega a hierarquia do novo nivel_superior
		strSql="SELECT hierarquia FROM fat_hierarquia WHERE destinatario='" &novoDestinatario& "' "
		set adoa=locconnvenda.execute(strSql)
		if not adoa.eof then novoNivelSuperior = adoa("hierarquia")
		set adoa=nothing
		
		strsql=	"select a.descricao " & _
				"from fat_hierarquia a inner join fat_hierarquia b on a.nivel_superior=b.hierarquia " & _
				"where a.hierarquia=" & novoNivelSuperior & " and b.destinatario='" & destinatarioOrigem & "'"
		set adoVerificaC=locconnvenda.execute(strsql)
		if adoVerificaC.eof then
				'Grava o log
			strSql =" INSERT INTO fat_hierarquia_inicio (hierarquia,superior,inicio,usuario,datamovimento) " & _
			 	   " SELECT hierarquia,nivel_superior,getdate(),'" &session("usuario")& "',getdate() " & _
			 	   " FROM fat_hierarquia " & _
			 	   " WHERE destinatario='" &destinatarioOrigem& "'"
			locconnvenda.execute(strSql)		
			'Altera o nivel superior
			strSql="UPDATE fat_hierarquia set usuario='" & session("usuario") & "',nivel_superior='" &novoNivelSuperior& "' WHERE hierarquia='" &hierarquiaOrigem& "'"
			set adoa=locconnvenda.execute(strSql)
					   
			 '' INI - Grava log de alteracao e altera o nivel superior
			session("msg") = descricaoIndicante&" alterado com sucesso!"
			response.redirect("?dest="&destinatarioOrigem & "&completo=" & completo)
			'' FIM - Rotinas de alteracao do nivel superior
		else
			session("msg")="- N�vel Superior selecionado possui como Superior esse cadastro (refer�ncia circular). Altera��o n�o efetuada."
			response.redirect("?dest="&destinatarioOrigem)
		end if
		set adoVerificaC=nothing
			   
	else
	'Mensagem perguntando se deseja realmente alterar
		strSql    = "SELECT a.nome,b.integracao " & _
					" FROM fat_destinatario a INNER JOIN fat_comercio b ON (a.destinatario=b.destinatario) " & _
					" WHERE a.destinatario='" & destinatarioOrigem & "'"
		
		set adoa=locconnvenda.execute(strsql)
		
		if not adoa.eof then 
		  nomeOrigem = adoa("nome")
		  codOrigem	 = adoa("integracao")
		end if
		
		set adoa=nothing
	  
	    response.write "<div style='background-color:#eaeaea' align='center'>"
		    
			response.write "<font face='Verdana' size='2' color='red'>"
			response.write "Deseja alterar o " &descricaoIndicante& " de: "&codOrigem & " - "&nomeOrigem & ", para o perfil abaixo ??"
			response.write "<br />"
			response.write "<a href='?"&request.QueryString&"&confirmaAlterarHierarquia=S'><b>SIM</b></a>"
			response.write "&nbsp;&nbsp;&nbsp;&nbsp;" 
			response.write "<a href='?dest="&request("destinatarioOrigem")&"'><b>N�O</b></a>"
			response.write "</font>" 
		
		response.write "</div>"
	
	end if
  else
	alterarHierarquia        = ""
	destinatarioOrigem       = ""
    cofirmaAlterarHierarquia = ""
	novoDestinatario         = ""	
  end if
				
 '' FIM - ALTERACOES DE HIERARQUIA - NIVEL SUPERIOR

%>






<table border="0" width="100%" cellspacing="1" cellpadding="0">           
	<tr>
		<td width="100%" align="center">
			<table border="0" width="100%" cellspacing="1" cellpadding="1">                  
				 
				<%
				 'Verifica se existe codigo de Integracao, se sim exibe fat_comercio.integracao do destinatario
				    if (session("hierarquia_rede") = 1) or session("empresa_id")>="10000001" then
						strDestInteg = strIntegracao
					else
						strDestInteg = reqDest 
					end if
				%>
				<tr>
					<td width="12%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">C�digo:</font></td>
                    <td colspan="3"><font face="Verdana" size="2"><b><%=strDestInteg%></b></font></td>
                    <td width="15%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Status:</font></td>
					<td width="21%"><font face="Verdana" size="1"><%=descricao_status%></font></td>
                    
                </tr>
				<tr>
					<td width="12%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Nome:</font></td>
                    <td width="52%" colspan="3"><font face="Verdana" size="2"><b><%=strNome%></b></font></td>
                    <td width="15%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Tipo Pessoa:</font></td>
					<td width="21%"><font face="Verdana" size="1"><%=strFJ%></font></td>
                </tr>
                <%
                if (session("hierarquia_rede") = 1) then
					strsql=	"select b.responsavel,b.destinatario " & _
							"from fat_hierarquia a inner join fat_hierarquia b on a.nivel_superior=b.hierarquia " & _
							"where a.destinatario='" & reqDest & "'"
					set adoa=locconnvenda.execute(strsql)
					if not adoa.eof then 
						hierarquia_responsavel  =adoa("responsavel")
						hierarquia_dest			=adoa("destinatario")
					end if
					set adoa=nothing
					%>
					<tr>
						<td width="12%" align="right" bgcolor="#eaeaea">
						
						
						
					<%
					if Verifica_Permissao_Aplicacao(10314) then					
					%>
					
					<font face="Verdana" size="1">
						<a href="cli_PesquisarCliente.asp?destinatarioOrigem=<%=reqDest%>&alterarHierarquia=1&completo=<%=completo%>">
						 <%=descricaoIndicante%>:
						</a>
					</font>		
					
					<%
					else
					%>	
					
					<font face="Verdana" size="1"><%=descricaoIndicante%>:</font>	
					
					<%				
					end if
					%>
						
						
						</td>
					    <td width="88%" colspan="5">
							<a href="cli_Dados.asp?dest=<%=hierarquia_dest%>">
							<font face="Verdana" size="1" color="orange"> <b><%=hierarquia_responsavel%></b></font>
							</a>
							
							
							
							
							</td>
					</tr>
					<%
                end if
                %>
                <!--
                <tr>
                    <td width="12%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Departamento:</font></td>
                    <td width="37%" colspan="2">
                      <%if isnull(strECodigoDepto) then
							x=strdepto
							y=strnomdepto
						else
							x=strECodigoDepto
							y=strNFilialCa
						end if%>
                      <a href="cli_Dados.asp?dest=<%=x%>&">
                      <font face="Verdana" size="2" color="black"><b><%=x & "-" & y%></b></font></a></td>
				    <td width="51%" align="left" colspan="3"><font face="Verdana" size="1"><%=strH%></font></td>						
                </tr>                   
                -->
                <%if regra="0" then%>
					<tr>
					    <td width="12%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Grupo:</font></td>
					    <td width="20%" align="left"><font face="Verdana" size="1"><%=strgrupo%></font></td>
						<td width="17%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Data In�cio:</font></td>
					    <td width="15%" align="left"><font face="Verdana" size="1"><%=dtDataCad%></font></td>
						<td width="15%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Data Ult. Compra:</font></td>
						<td width="21%"><font face="Verdana" size="1"><%=dtUltimaCompra%></font></td>
					</tr>
					    <%
					  if strFJ="F" then 
						strCampo1="CPF"
						strCampo2=idioma_campos(1)
						opcao="1"
						
						
					  else
					    strCampo1="CNPJ"
					    strCampo2=idioma_campos(2)
					    opcao="2"
					  end if
					  %>
					<tr>                  
					    <td width="12%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1"><%=strCampo1%>:</font></td>
					    <td width="20%"><font face="Verdana" size="1">
							<%=strCpf%>
							<%'=cpf_formata(strCPF)%></font></td>
					    <td width="17%" align="right" bgcolor="#eaeaea">
							<font face="Verdana" size="1"><%=strCampo2%>:</font></td>
					    <td width="15%" align="left">
							<font face="Verdana" size="1"><%=strRg%><%'rg_formata(strRg,opcao)%></font></td>
					    <td width="15%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Dt-Nasc:</b></font></td>
					    <td width="21%"><font face="Verdana" size="1"><%=strDtNasc%></font></td>
					</tr>
					<tr>                  
					    <td width="13%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Condi��o Pagto:</font></td>
					    <td width="20%"><font face="Verdana" size="1"><%=strCondicaoPagto%></font></td>
					    <td width="17%" align="right">
							<font face="Verdana" size="1">&nbsp</font></td>
					    <td width="15%" align="left">
							<font face="Verdana" size="1">&nbsp</font></td>
					    <td width="15%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Vendedor:</b></font></td>
					    <td width="21%"><font face="Verdana" size="1"><%=strVendedor%></font></td>
					</tr>
             
<%
				end if
							do while not adoTel.eof														
%>                <tr>                  
                    <td width="12%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Telefone:</font></td>
                    <td width="88%" colspan="5"><font face="Verdana" size="1"><%=adoTel("descricao") & " (" & adoTel("ddd") & ")" & " - " & adoTel("numero") %></font></td>
                  </tr>
<%								adoTel.movenext
							loop
					
				if regra="0" then		
%>             
					<tr>
					    <td width="100%" align="right" colspan="6"><hr size="1"></td>
					</tr>                                             
					<tr>
					    <td width="12%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Endere�o:</font></td>
					    <td width="37%" colspan="2"><font face="Verdana" size="1"><%=strEndereco%></font></td>
					    <td width="15%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Bairro:</font></td>
					    <td width="36%" colspan="2"><font face="Verdana" size="1"><%=strBairro%></font></td>
					</tr>
					<tr>
					    <td width="12%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Cidade:</font></td>
					    <td width="37%" colspan="2"><font face="Verdana" size="1"><%=strCidade%></font></td>
					    <td width="15%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">CEP:</font></td>
					    <td width="36%" colspan="2"><font face="Verdana" size="1"><%=strCep%><%'cep_formata(strCep)%></font></td>
					  </tr>
				<%end if%>
					<tr>
					    <td width="12%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">E-mail:</font></td>
					    <td width="37%" colspan="2"><a href="mailto:<%=strEmail%>"><font face="Verdana" size="1"><%=strEmail%></font></a></td>
					    <td width="15%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Contato:</font></td>
					    <td width="36%" colspan="2"><font face="Verdana" size="1"><%=strContato%></font></td>
					</tr>
				<%if regra="0" then	%>
					<tr>
					    <td width="12%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Site:</font></td>
					    <td width="88%" colspan="5"><font face="Verdana" size="1"><a href="http://<%=strsite%>" target="_new"><%=strSite%></a></font></td>                    
					</tr>
					<%	if sessioN("empresa_id")="00000007" then
							strsql=	"select a.banco,a.agencia,a.conta_c,b.nome " & _
									"from fat_comercio a left join fat_banco b on a.banco=b.banco " & _
									"where a.destinatario='" & reqDest & "'"
							set adoa=conn5.execute(strsql)
							if not adoa.eof then
								strBanco	=adoa("banco") & "-" & adoa("nome")
								strAgConta	=adoa("agencia") & "&nbsp;&nbsp;" & adoa("conta_c")
							end if
							set adoa=nothing
					%>
							<tr>
							    <td width="12%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Banco:</font></td>
							    <td width="37%" colspan="2"><font face="Verdana" size="1"><%=strBanco%></font></td>
							    <td width="15%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Ag�ncia | C/C:</font></td>
							    <td width="36%" colspan="2"><font face="Verdana" size="1"><%=strAgConta%></font></td>
							</tr>
					<%	end if%>
					<tr>
					    <td width="12%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Como Soube:</font></td>
					    <td colspan="4"><font face="Verdana" size="1"><%=strComoSoube%></font></td>
					    <%
					    'Response.Write "XXXXXXXX " & session("manut_destinatario_SN")
					    if session("manut_destinatario_SN")="S" then%>
							<td><font face="Verdana" size="1"><%call button_criar("button","btnAlterar","Alterar Dados","onclick=alterar('" & reqDest & "')")%></font></td>
						<%end if%>
					</tr>
					<tr>
					    <td width="100%" align="right" colspan="6"><hr size="1"></td>
					</tr>  
					<tr>
					    <td width="12%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">End.Pagto:</font></td>
					    <td width="37%" colspan="2"><font face="Verdana" size="1"><%=strendpagto%></font></td>
					    <td width="15%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Bairro Pagto:</font></td>
					    <td width="36%" colspan="2"><font face="Verdana" size="1"><%=strbaipagto%></font></td>
					</tr>
					<tr>
					    <td width="12%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">Cidade Pagto:</font></td>
					    <td width="37%" colspan="2"><font face="Verdana" size="1"><%=strmunpagto%></font></td>
					    <td width="15%" align="right" bgcolor="#eaeaea"><font face="Verdana" size="1">CEP Pagto:</font></td>
					    <td width="36%" colspan="2"><font face="Verdana" size="1"><%=strceppagto%><%'cep_formata(strceppagto)%></font></td>
					</tr>
                <%
                end if
                %>
             </table>
          </td>
        </tr>
        <tr>
			<td width="100%" align="center"><hr size="1">
			 
	    	</td>
        </tr>  		
  		<%
  		if session("empresa_id")<"10000001" then
  		
  			if regra="0" then%>
			  <tr>
				<td width="100%" align="center">
				<strong><%call label("FORMAS DE PAGAMENTO","1","","")%></strong>
				<br />
			    &nbsp;
				<font face="Verdana" size="1">
				<%
				
				strFormasPagmto = "SELECT b.descricao FROM ped_pedidoTotal a INNER JOIN fat_tipo_cobranca b ON (a.TipoCobranca=b.tipo) WHERE a.stnfCodigo IN (1,26,51) and a.TipoCobranca not in (11) and a.destinatario = '"&reqDest&"' GROUP BY b.descricao ORDER BY b.descricao"

					set adoFormasPagmto=locConnVenda.execute(strFormasPagmto)
			    
				do while not adoFormasPagmto.eof
					Response.Write adoFormasPagmto("descricao")
					
					adoFormasPagmto.movenext
					if not adoFormasPagmto.eof then Response.Write (", ")
				  loop
			   adoFormasPagmto.close
				%>
				
			      </font>
				</td>
			</tr> 
		
			  <tr>
				<td width="100%" align="center"><hr size="1">
				 
				</td>
			</tr>  		 	
			<%end if
		
		
				%>
			    <tr>
		            <td width="100%" align="right"><b><%call label("COBRAN�A","1","","")%></b></td>
			    </tr>
		        <tr>
		            <td width="100%" align="right">				
						<form name="frm1" method="post" action="cli_dados_calculo.asp">       
					<%
					call zerar_relatorio()
					
					strSql="cob_movimento_cliente '" & reqDest & "',1,''"
					colspan=6
					
					set adoRs1=conn1.execute(strSql)
					call define_coluna("T�tulo","documento","10","c","t","n","n","","")
					call define_coluna("Pedido","pedido","7","c","t","n","n","","")
					call define_coluna("Remessa","carqcodigo","7","c","t","n","n","","")
					call define_coluna("Ag.","calcular","3","c","t","n","n","","")
					call define_coluna("Nosso N�mero","cnab","5","c","t","n","n","","")
					call define_coluna("Data","data","7","c","t","n","s","","")							
					call define_coluna("Venc","vencimento","7","c","t","n","s","","")
					call define_coluna("Opera��o","operacao","15","c","t","n","s","","")
					call define_coluna("Valor","valor","8","d","2","s","s","","")
					call define_coluna("Obs","obs","*","c","t","n","n","","")
					call define_coluna("Situa��o","situacao","8","c","t","n","s","","")
					call define_coluna("Usu�rio","usuario","8","c","t","n","s","","")
					if session("empresa_id")="00000007" then
						call define_coluna("",1,"1","c","t","n","s","","")
						call define_link(13,"javascript:janelaBoleto('[doc]')")
						colspan=colspan+1
					end if
					
									
					call define_quebra("","documento","n","")
					
					'call define_link(1,"../pedido/ped_Pedido.asp?ped=[pedido]&em=[emitente]")
					call define_link(1,"../cobranca/cob_ocorrencia.asp?filial=[emitente]&doc=[doc]&tipocob=[tipo]&dest=" & reqDest)
					call define_link(2,"../pedido/ped_Pedido.asp?em=[emitente]&ped=[pedido]")
					call define_link(11,"javascript:AbreSituacao([tipo],'[doc]','[emitente]','" & reqDest & "','" & session("usuario") & "')")

					impcab=0
					imp_MaxLinha=1000
					call fun_rel_cab()
					do while not adoRs1.eof
						
						variavel(1)="<img src='../imagem/sistema/img_imprimir.jpg' border=0>"
						'
						call fun_rel_det()
						for n = 1 to 10
							if len(cpod(9)) > 0 then
								tg(9,n) = tg(9,n) - cdbl(cpod(9))
								if ucase(adors1("processo"))="S" or adors1("op") = "99" then
									tg(9,n) = tg(9,n) + cdbl(cpod(9))
								end if
								if ucase(adors1("processo"))="D" then
									tg(9,n) = tg(9,n) - cdbl(cpod(9))
								end if
							end if
						next
						adoRs1.movenext
					loop
					call fun_fim_rel()
					
					%>
						<tr>					
							<td width="*%" colspan="6" align="right">
								<input type="hidden" name="dest" value="<%=reqdest%>">
								<a href="javascript:document.frm1.submit()">
								<font face="arial" size="1" color="blue">AGENDAR PAGAMENTOS DOS T�TULOS</font></a></td>
							<td width="*%" colspan="<%=colspan%>" align="right">
								<a href="../cobranca/cob_TitulosCliente.asp?d=<%=reqDest%>">
								<font face="arial" size="1" color="blue">EXIBIR HIST�RICO DE T�TULOS</font></a></td>
						</tr>
						</form>
					</table>
				</td>
			</tr>
			 <%
			strSql=	"select isnull(sum(valor),0) total " + _
					"from view_cobranca_saldo where destinatario= '" & reqDest & "' and tipo_cartao='N'"
			set adoRs1=locConnVenda.execute(strSql)
			if not adoRs1.eof then
				dblValorTotalF=adoRs1("total")
			end if
			set adoRs1=nothing
							
			strSql=	"select top 1 limite=isnull(limite,0),data_limite=isnull(data_limite,0) from fat_Destinatario_Credito " & _
					"where destinatario='" & reqDest & "' order by alteracao desc"
			set adoA=locConnVenda.execute(strSql)
			if not adoA.eof then
				strLimite	=adoA("limite")
				strDataLimite=adoA("data_limite")
			end if
			set adoA=nothing
			%>
			<tr>
		        <td width="100%" align="right">
		    		<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="10%" bgcolor="#eaeaea" align="right">
								<a href="../cobranca/cob_Credito.asp?d=<%=reqDest%>">
								<font face="verdana" size="1">Limite:</font></td>
							<td width="15%"><font face="verdana" size="1"><%=formatnumber(strLimite,2)%></font></td>
							<td width="10%" bgcolor="#eaeaea" align="right"><font face="verdana" size="1">Limite Data:</font></td>
							<td width="15%"><font face="verdana" size="1"><%=strDataLimite%></font></td>
							<td width="13%" bgcolor="#eaeaea" align="right"><font face="verdana" size="1">Limite Usado:</font></td>
							<td width="15%"><font face="verdana" size="1"><%=formatnumber(dblValorTotalF,2)%></font></td>
							<td width="7%" bgcolor="#eaeaea" align="right"><font face="verdana" size="1">Cr�dito:</font></td>
							<td width="15%"><font face="verdana" size="1"><%=formatnumber(cdbl(strLimite)-cdbl(dblValorTotalF),2)%></font></td>
						</tr>
					</table>
			   </td>
		    </tr>
		    <tr>
		        <td width="100%" align="right"><hr size="1"></td>
		    </tr>     
			<tr>
		       <td width="100%" align="right"><b><%call label("DEP�SITOS","1","","")%></b></td>
			</tr>
		    <tr>
		        <td width="100%" align="right">
		        <%
				call zerar_relatorio()
		                
		         strSql="select banco,agencia,documento,b.descricao,dt_deposito=convert(varchar(10),dt_deposito,103)" & _
						",valor,numero_conciliacao,dt_conciliacao=convert(varchar(10),dt_conciliacao,103)" & _
						",dt_movimento=convert(varchar(10),dt_movimento,103),dt_documento=convert(varchar(10),dt_documento,103) " + _
						"from cob_deposito a left join sys_finalidade b on a.finalidade=b.finalidade " + _
						" inner join cob_deposito_saldo s on b.finalidade = s.finalidade  " + _
						"where destinatario='" & reqDest & "' " + _
						" and (month(dt_movimento)='" & strMes & "' or '" & StrAno & "'='" & StrAnoGeral & "')" + _
						" and year(dt_movimento)='" & strAno & "' and s.operacao_saldo <> 0 " + _
						"order by dt_movimento"
		'				   Response.Write strsql
				set adoRs1=locConnVenda.execute(strSql)
								     
				call define_coluna("banco","banco","5","c","t","n","n","","")
				call define_coluna("ag�ncia","agencia","5","c","t","n","n","","")
				call define_coluna("documento","documento","15","c","t","n","n","","")
				call define_coluna("finalidade","descricao","20","e","t","n","n","","")
				call define_coluna("dt dep�s.","dt_deposito","9","c","t","n","n","","")
				call define_coluna("valor","valor","9","d","2","n","s","","")
				call define_coluna("n concil.","numero_conciliacao","9","c","0","n","n","","")
				call define_coluna("dt concil.","dt_conciliacao","9","c","t","n","n","","")
				call define_coluna("dt mov.","dt_movimento","9","c","t","n","n","","")
				call define_coluna("dt doc","dt_documento","9","c","t","n","n","","")
						
				call fun_rel_cab()
				do while not adoRs1.eof														
					call fun_rel_det()								
					adoRs1.movenext
				loop
				call fun_fim_rel()
		                 %>
					</table>                               
				</td>
			</tr>
			<tr>
				<td width="100%" align="right" colspan="6"><hr size="1"></td>
			</tr> 
	<%end if%>  	
    <tr>
        <td width="100%" align="right"><b><%call label("FATURAMENTO " & mes(strMes) & "/" & strAno,"1","","")%></b></td>
    </tr>
    <tr>
        <td width="100%" align="right">
	<%
		call zerar_relatorio()
		
		strSql=	"select emitente,nota,serie,referencia,sum(valor_total) valor,sum(icms) icms" & _
				",sum(quantidade) qtd,nf=convert(varchar(8),nota) + '-' + serie,descOperacao " & _
				"from view_Notas " & _
				"where destinatario='" & reqDest & "' " & _
				"  and month(referencia)=" & strMes & _
				"  and year(referencia)=" & strAno & _
				" group by nota,serie,referencia,descOperacao,emitente " & _
				"order by nota"
				'Response.Write strsql
		set adoRs1=conn1.execute(strSql)
		call define_Coluna("nota","nf","7","c","t","n","n","","")
		call define_Coluna("refer�ncia","referencia","7","c","t","n","n","","")
		call define_Coluna("opera��o","descOperacao","20","e","t","n","n","","")
		call define_Coluna("quantidade","qtd","10","d","0","s","s","","")
		call define_Coluna("valor total","valor","10","d","2","s","s","","")
		if session("hierarquia_rede")<>1 then
			call define_Coluna("icms","icms","10","d","2","s","s","","")
		end if
					
		call define_link(1,"../faturamento/fat_Nf.asp?nf=[nota]&se=[serie]&filial=[emitente]")
					
		call fun_rel_cab()
		do while not adoRs1.eof														
			call fun_rel_det()
			adoRs1.movenext
		loop
		call fun_fim_rel()
		
		
		if session("empresa_id")<"10000001" then
                %>
                 <tr>
					<td width="*" colspan="3" align="left">
						<a href="../faturamento/fat_HistoricoNota.asp?d=<%=reqDest%>">
						<font face="verdana" size="1" color="blue">EXIBIR HIST�RICO DE NOTAS</font></a>
					</td>
					<td width="*" colspan="4" align="right">
						<a href="../pedido/ped_HistoricoPedido.asp?d=<%=reqDest%>">
						<font face="verdana" size="1" color="blue">EXIBIR HIST�RICO DE PEDIDOS</font></a>
					</td>
				</tr>
		<%
		end if
		%>
			</table>
		</td>
	</tr>
    <tr>
        <td width="100%" align="right"><hr size="1"></td>
    </tr>                                                                                             
    <tr>
		<td width="100%" align="right">
			<table border="0" width="100%">
				<tr>
					<td width="50%" align="right"><b><%call label("FATURAMENTO ANUAL","1","","")%></b></td>
					<td width="50%" align="right"><a href="cli_Dados.asp?dest=<%=reqDest%>&ano=<%=strAno%>&mes=<%=strMes%>&prod=<%=strProd%>&campoQtdValor=<%=tit3%>"><%call label("exibir " & tit2,"1","right","")%></a></td>
				</tr>
			</table>
		</td>
    </tr>                                             
    <tr>
        <td width="100%" align="right">
						<%
			call zerar_relatorio()
			strSql="exec fat_HistoricoVendas_sp '" & reqDest & "'"
			'Response.write strsql
			'Response.end
			set adoRs1=conn1.execute(strSql)
						
			if campoQtdValor="v" then 
				qtdF=2
			else
				qtdF=0
			end if						
			
			retzero=true			
			call define_Coluna("ano","ano","7","c","t","n","s","","")						
			for i=1 to 12
				strCampo=cstr(campoQtdValor) + cstr(i)
				call define_Coluna(mes(i),strCampo,"7","d",qtdF,"s","s","","")
			next
			call define_Coluna("Total","total" & campoQtdValor,"*","d",qtdF,"s","s","","")
						
			'call define_link(1,"../faturamento/anual/fat_AnualPorProduto1.asp?dest=" & reqDest & "&ano=[ano]&campoQtdValor=" & campoQtdValor)
					
			for i=1 to 12
				call define_link(1+i,"cli_Dados.asp?dest=" & reqDest & "&ano=[ano]&mes=" & i & "&campoQtdValor=" & campoQtdValor)
			next						
						
			call fun_rel_cab()
			do while not adoRs1.eof														
				call fun_rel_det()
				adoRs1.movenext
			loop
			call fun_fim_rel()																		
			%></table>
        </td>
    </tr>
    <tr>
        <td width="100%" align="right"><hr size="1"></td>
    </tr>
    <%if session("empresa_id")<"10000001" then  %>
     <tr>
        <td width="100%" align="right">
			<table border="0" width="100%" cellpadding="0" cellspacing="1">
				<tr>
					<td width="50%" align="left" valign="top">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<%if session("empresa_id")="00000008" then%>
							<tr>
								<td width="100%" align="left" bgcolor="#f5f5f5">
									<a href="../faturamento/fat_analiseglobal1.asp?codigo=<%=strdestinteg%>&ano=<%=year(date)%>">
									<font face="verdana" size="1" color="blue">.AN�LISE GLOBAL</font></a></td>
							</tr>
						<%else%>
							<tr>
								<td width="100%" align="left" bgcolor="#f5f5f5">
									<a href="../faturamento/anual/fat_geral1.asp?dest=<%=reqdest%>">
									<font face="verdana" size="1" color="blue">.AN�LISE DE FATURAMENTO GERAL</font></a></td>
							</tr>
							<tr>
								<td width="100%" align="left" bgcolor="#f5f5f5">
									<a href="cli_dados_analise.asp?dest=<%=reqdest%>&ano=<%=year(date)%>">
									<font face="verdana" size="1" color="blue">.COMPARATIVO DE FATURAMENTO</font></a></td>
							</tr>
							<tr>
								<td width="100%" align="left" bgcolor="#f5f5f5">
									<a href="cli_dados_consignacao.asp?dest=<%=reqdest%>">
									<font face="verdana" size="1" color="blue">.AN�LISE DE CONSIGNA��O</font></a>
									
									<font face="verdana" size="1" color="blue">| </font>
									<a href="cli_dados_consignacao_ContaCorrente.asp?dest=<%=reqdest%>">
									<font face="verdana" size="1" color="blue">.C/C DE CONSIGNA��O</font></a>
									
									</td>
							</tr>
						<%end if%>
						</table>
					</td>
					<td width="50%" align="left" valign="top">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr><td width="100%" align="right" bgcolor="#f5f5f5">
								<a href="cli_Ocorrencia.asp?dest=<%=reqdest%>">
								<font face="verdana" size="1" color="blue">OCORR�NCIAS.</font></a></td></tr>
							<tr><td width="100%" align="right" bgcolor="#f5f5f5">
								<a href="../sac/sac_cliente.asp?dest=<%=reqDest%>">
								<font face="verdana" size="1" color="blue">SAC.</font></a></td></tr>
							<tr><td width="100%" align="right" bgcolor="#f5f5f5">
								<a href="../email/eml_ListaEmail1.asp?dest=<%=reqDest%>&status=0">
								<font face="verdana" size="1" color="blue">E-MAIL.</font></a></td></tr>
						</table>
					</td>
				</tr>
			</table>
        </td>
     </tr>
     <%end if%>
</table>     
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->

<%
function idioma()
	idioma_campos(1)="RG"
	idioma_campos(2)="Inscri��o Estadual"
	
	if len(trim(session("idioma_campo")))>0 then
		idioma_campos(1)="C.C."
		idioma_campos(2)="C.C."
	end if
end function
%>
<script language="JavaScript">
	function alterar(d) {		
		location.href="cli_DadosIncluir.asp?dest=" + d + "&opcao=A";
	}
</script>