<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10854)
%><!--#include virtual='public/pub_BodyRel750.asp'-->

<%
consultor	=trim(request("consultor"))
kit=request("kit")
acao=request("acao")

link="cli_IncluirKit.asp"

if len(consultor)=0 then
	session("msg")="Informe o c�digo do Patrocinador."	
	Response.Redirect link
end if


if len(kit)=0 then
	session("msg")="Selecione o Kit."	
	Response.Redirect link
end if

if len(consultor)<8 then consultor=string(8-len(consultor),"0") & cstr(consultor)

strsql=	"select destinatario,ativosn,nome " & _
		"from view_consultor " & _
		"where integracao='" & consultor & "'"
set adors1=locconnvenda.execute(strsql)
if adors1.eof then
	session("msg")="Consultor n�o localizado."	
	Response.Redirect link
else
	nome=adors1("Nome")
	destinatario=adors1("destinatario")
	if adors1("ativosn")=0 or adors1("ativosn")=false then
		strsql="select destinatario from fat_Destinatario_Ocorrencia where destinatario='" & adors1("destinatario") & "' and ocorrencia=43"
		set adors2=locconnvenda.execute(strsql)
		if adors2.eof then
			session("msg")="Consultor inativo, n�o � poss�vel efetuar cadastro abaixo dele."
			Response.Redirect link
			Response.end
		end if
		set adors2=nothing
	end if
end if
set adors1=nothing


if len(session("idPedidoTMP"))=0 then
	session("idPedidoTMP")=session("empresa_id") & year(date) & month(date) & day(date) & hour(now) & minute(now) & second(now)
end if

if acao="K" then
	'' inserir o kit primeiramente
	strsql="delete hinode_loja.dbo.TabPedidoTemp where idpedidotemp='" & session("idPedidoTMP") & "'"
	locconnvenda.execute(strsql)
	
	strsql= "insert into hinode_loja.dbo.TabPedidoTemp (idPedidoTemp,idProduto,nome,chamada,valor,quantidade,data,operacao,valor_original,pontos,pontos_original) " & _
				"select '" & session("idPedidoTMP") & "',idproduto,nome,chamada,valor,1,getdate(),0,valor,pontuacao,pontuacao " & _
				"from hinode_loja.dbo.tabprodutos where idproduto=" & kit
	locconnvenda.execute(strsql)				
end if

if acao="I"	then

	produto=request("Produto")
	if len(produto)=0 then call redi_aviso("Adicionar produto","Selecione o produto para ser adicionado.")
	
	qtd=request("qtd")
	if len(qtd)=0 then qtd=1
	
	
	'' validar estoque
	estoque=0
	strsql="select estoque=" & session("campo_estoque") & ",codigo from hinode_loja.dbo.tabprodutos where idproduto=" & produto
	set adors1=locconnvenda.execute(strsql)
	if not adors1.eof then 
		estoque=adors1("estoque")
		codigo=adors1("codigo")
	end if
	set adors1=nothing
	
	pendente=0
	strsql=	"select qtd=isnull(sum(b.quantidade),0) " & _
			"from hinode_loja.dbo.tabPedidos a inner join hinode_loja.dbo.tabPedidosItens b on a.idPedido=b.idPedido " & _
			"where a.emitente='" & session("empresa_id") & "' and idproduto=" & produto & " and a.pedicodigo is null"
	set adors2=locconnvenda.execute(strsql)
	if not adors2.eof then pendente=adors2("qtd")
	set adors2=nothing
							
	empenho=0
	strsql=	"select qtd " & _
			"from view_PedidoProdPendente " & _
			"where emitente='" & session("empresa_id") & "' and produto='" & codigo & "'"
	set adors2=locconnvenda.execute(strsql)
	if not adors2.eof then empenho=adors2("qtd")
	set adors2=nothing
	
	estoque=cdbl(estoque) - cdbl(empenho) - cdbl(pendente)	
	
	if cdbl(qtd)>cdbl(estoque) then
		call redi_aviso("Adicionar produto","Produto " & codigo & " sem estoque (Estoque: " & estoque & ")")
	else
		strsql="delete hinode_loja.dbo.TabPedidoTemp where idpedidotemp='" & session("idPedidoTMP") & "' and idproduto=" & produto
		locconnvenda.execute(strsql)
		
		strsql= "insert into hinode_loja.dbo.TabPedidoTemp (idPedidoTemp,idProduto,nome,chamada,valor,quantidade,data,operacao,valor_original,pontos,pontos_original) " & _
			"select '" & session("idPedidoTMP") & "',a.idproduto,nome,chamada,b.valor," & qtd & ",getdate(),0,b.valor,pontuacao,pontuacao " & _
			"from hinode_loja.dbo.tabprodutos a inner join hinode_loja.dbo.tabProdutosGrupos b on a.idproduto=b.idproduto " & _
			"where a.idproduto=" & produto
		locconnvenda.execute(strsql)
	end if
end if

if acao="E" then
	produto=request("Produto")
	
	strsql="delete hinode_loja.dbo.TabPedidoTemp where idpedidotemp='" & session("idPedidoTMP") & "' and idproduto=" & produto
	locconnvenda.execute(strsql)
end if




qtd_itens=0
strsql="select nome,codigo,qtd_itens=isnull(qtd_itens,0),valor from hinode_loja..tabprodutos where idProduto=" & kit
set adors1=locconnvenda.execute(strsql)
if not adors1.eof then 
	codigo		=adors1("codigo")
	descricao	=adors1("nome")
	qtd_itens	=adors1("qtd_itens")
	valor	=adors1("valor")
end if
set adors1=nothing

''frete
strsql=	"select top 1 a.transporte,descricao=a.descricao_site,b.frete from fat_transporte a inner join fat_Frete b on a.transporte=b.transporte " & _
		"where a.atividade=" & session("atividade")
'call rw(strsql,0)		
set adors1=locconnvenda.execute(strsql)
if not adors1.eof then
	transporte=adors1("transporte")
	transporte_descricao=adors1("descricao")
	transporte_frete=adors1("frete")	
end if
set adors1=nothing		

'' forma de pagamento
strsql="select idformapagamento,pagamento from hinode_loja.dbo.tabFormaPagamento where idFormaPagamento=17"
set adors1=locconnvenda.execute(strsql)
if not adors1.eof then
	pagamento =adors1("idFormaPagamento")
	pagamento_descricao=adors1("pagamento")
end if
set adors1=nothing

valor_total=cdbl(valor) + cdbl(transporte_frete)

%>

<table border=0 width="100%">
	<tr>
		<td width=20% bgcolor='#f5f5f5'><font face=verdana size=2><b>Patrocinador:</b></font></td>
		<td width=80%><font face=verdana size=2 color=blue><%=consultor%> - <%=nome%></font></td>		
	</tr>
	<tr>
		<td bgcolor='#f5f5f5'><font face=verdana size=2><b>Kit:</b></font></td>
		<td><font face=verdana size=2><%=codigo%> - <%=descricao%></font></td>		
	</tr>
	<tr>
		<td bgcolor='#f5f5f5'><font face=verdana size=2>Pre�o:</font></td>
		<td><font face=verdana size=2>R$ <%=formatnumber(valor,2)%></font></td>		
	</tr>
	
	<%if cdbl(qtd_itens)>0 then%>
		<tr>
			<td bgcolor='#f5f5f5'><font face=verdana size=2>Col�nias Gold:</font></td>
			<td><font face=verdana size=2><%=formatnumber(qtd_itens,0)%></font></td>		
		</tr>
		<tr><td colspan=2>
			<table border=0 width=90% align=center>
				<tr>
					<td align=center width="5%"><font face=verdana size=1><u>C�digo</u></td>
					<td align=center width="%"><font face=verdana size=1><u>Descri��o</u></td>
					<td align=center width="10%"><font face=verdana size=1><u>Pre�o Unit�rio</u></td>
					<td align=center width="10%"><font face=verdana size=1><u>Quantidade</u></td>					
					<td align=center width="10%"><font face=verdana size=1><u>SubTotal</u></td>
					<td align=center width="10%"><font face=verdana size=1><u>Pontos</u></td>
					<td align=center width="3%"><font face=verdana size=1><u>x</u></td>
				</tr>
		
			<%
			tot_qtd=0
			total_pontos=0
			strsql="select a.idproduto,a.nome,a.valor,a.quantidade,b.codigo,pontos=isnull(a.pontos,0) " & _
					 "from hinode_loja.dbo.TabPedidoTemp a inner join hinode_loja.dbo.tabProdutos b on a.idproduto=b.idproduto " & _
					 "where idPedidoTemp='" & session("idPedidoTMP") & "' and b.idCategoria<>15 " & _
					 "order by b.codigo"
			  set adors1=locconnvenda.execute(strsql)
			  do while not adors1.eof
				subtotal=cdbl(adors1("valor")) * cdbl(adors1("quantidade"))
				
				valor_total=cdbl(valor_total) + cdbl(subtotal)
				tot_qtd=cdbl(tot_qtd)+cdbl(adors1("quantidade"))
				
				pontos=cdbl(adors1("quantidade")) * cdbl(adors1("pontos"))
				total_pontos=cdbl(total_pontos) + cdbl(pontos)
			  
				%><tr>
					<td align=center><font face=verdana size=1><%=adors1("codigo")%></td>
					<td align=left><font face=verdana size=1><%=adors1("nome")%></td>
					<td align=center><font face=verdana size=1><%=formatnumber(adors1("valor"),2)%></td>
					<td align=center><font face=verdana size=1><%=adors1("quantidade")%></td>					
					<td align=center><font face=verdana size=1><%=formatnumber(subtotal,2)%></td>
					<td align=center><font face=verdana size=1><%=formatnumber(pontos,2)%></td>
					<td align=center><a href="cli_IncluirKit1.asp?consultor=<%=consultor%>&kit=<%=kit%>&produto=<%=adors1("idproduto")%>&acao=E"><font face=verdana size=1>x</a></td>
				</tr>
					
				<%
				adors1.movenext
			  loop
			  set adors1=nothing
			  
			  call form_criar("frm1","cli_IncluirKit1.asp")
				call txtOculto_Criar("consultor",consultor)
				call txtOculto_Criar("kit",kit)
				call txtOculto_Criar("acao","I")
			  %>
				<tr>
					<td colspan=3>
						<%strsql="select IdProduto,codigo,nome,estoque=isnull(" & session("campo_estoque") & ",0) " & _
								 "FROM hinode_loja..TabProdutos " & _
								 "where IdCategoria=31 and nome like '%gold%' and Ativo=1 " & _
								 "and idproduto not in(select idproduto from hinode_loja..tabPedidoTemp where idPedidoTemp='" & session("idPedidoTMP") & "') " & _
								 "order by nome"
						set adors1=locconnvenda.execute(strsql)
						
						call select_criar("produto","")
						do while not adors1.eof
						
							estoque=adors1("estoque")
							
							pendente=0
							strsql=	"select qtd=isnull(sum(b.quantidade),0) " & _
									"from hinode_loja.dbo.tabPedidos a inner join hinode_loja.dbo.tabPedidosItens b on a.idPedido=b.idPedido " & _
									"where a.emitente='" & session("empresa_id") & "' and idproduto=" & adors1("idproduto") & " and a.pedicodigo is null"
							set adors2=locconnvenda.execute(strsql)
							if not adors2.eof then pendente=adors2("qtd")
							set adors2=nothing
							
							empenho=0
							strsql=	"select qtd " & _
									"from view_PedidoProdPendente " & _
									"where emitente='" & session("empresa_id") & "' and produto='" & adors1("codigo") & "'"
							set adors2=locconnvenda.execute(strsql)
							if not adors2.eof then empenho=adors2("qtd")
							set adors2=nothing
							
							estoque=cdbl(estoque) - cdbl(empenho) - cdbl(pendente)
						
							call select_item(adors1("idproduto"),adors1("nome") & " (Estoque: " & estoque & ")","")
							adors1.movenext
						loop
						set adors1=nothing
						%>
					</td>
					<td align=center><%call txt_Criar("inteiro","qtd",4,4,"")%></td>
					<td align=center><%call button_criar("submit","btnAdd","Adicionar","")%></td>
				</tr>
				<%call form_fim()
				
				valida_kit=false								
				if cdbl(tot_qtd) < cdbl(qtd_itens) then
					diferenca=cdbl(qtd_itens)-cdbl(tot_qtd)
					
					if diferenca=1 then
						texto="Falta 1 item"
					else
						texto="Faltam " & diferenca & " itens"
					end if
					%>
					<tr><td colspan=10 align=center><font face=verdana size=1 color=green><b><%=texto%></td></tr>
					<%
				else				
					valida_kit=true
				end if
				%>
			  
			  </table>
			</td>
		</tr>
	<%
	else 
		valida_kit=true	
	end if%>
	
	<tr><td colspan=2><hr size=1></td></tr>
	<tr>
		<td bgcolor='#f5f5f5'><font face=verdana size=2>Transportadora:</font></td>
		<td><font face=verdana size=2><%=transporte_descricao%> (R$ <%=formatnumber(transporte_frete,2)%>)</font></td>		
	</tr>
	<tr>
		<td bgcolor='#f5f5f5'><font face=verdana size=2>Forma de Pagamento:</font></td>
		<td><font face=verdana size=2><%=pagamento_descricao%></font></td>		
	</tr>
	<tr><td colspan=2><hr size=1></td></tr>
	<tr>
		<td bgcolor='#f5f5f5'><font face=verdana size=2>Total Pontos:</font></td>
		<td><font face=verdana size=2> <%=formatnumber(total_pontos,2)%></b></font></td>		
	</tr>
	<tr>
		<td bgcolor='#f5f5f5'><font face=verdana size=2>Valor Total:</font></td>
		<td><font face=verdana size=2><b>R$ <%=formatnumber(valor_total,2)%></b></font></td>		
	</tr>
	<tr><td colspan=2><hr size=1></td></tr>
	<%
	if valida_kit=true then
		
	
		call form_criar("frmConcluir","cli_IncluirKit1_Valida.asp")
			call txtOculto_criar("consultor",destinatario)						
			
			strsql="select codigo from fat_Codigo_cdh where emitente='" & session("empresa_id") & "' and isnull(usado,0)=0 order by codigo"
			set adors1=locConnvenda.execute(strsql)
		
			call rel_item_selectini(0,0,"C�digo Excluir","codigo_excluir","")
				do while not adors1.eof	
					call select_item(adors1("codigo"),adors1("codigo"),"")
					adors1.movenext
				loop
				set adors1=nothing
			call rel_item_selectfim()
		
			%>
			<tr><td bgcolor='#f5f5f5'><font face=verdana size=1>E-mail do novo consultor:</td>
				<td><%call txt_Criar("texto","email",50,100,"")%></td>
			</tr>		
			<tr><td colspan=10 align=center><%call button_criar("submit","bnt1","Efetuar Cadastro >>","")%></td></tr>
			<%
		call form_fim()
		
	end if
	%>
</table>



<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->