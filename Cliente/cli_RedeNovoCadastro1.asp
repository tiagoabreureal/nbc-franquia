<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10435)

strini			=request("strini")
strfim			=request("strfim")
codigo			=request("codigo")
ciclo			=request("ciclo")
relatorio		=request("relatorio")
classificacao	=request("classificacao")

if len(codigo)=0 then codigo="1"
if len(codigo)<8 then codigo=string(8-len(codigo),"0") & cstr(codigo)

lbl1	=""
txt1	=""
lbl2	=""
txt2	=""
lbl3	="Período:"
txt3	=strIni & "  -  " & strFim
lbl4	=""
txt4	=""

impcab = 1

if relatorio="1" then ''por cidade
	configura_arquivo=true
	session("arquivo_titulo")="Estado,Cidade,Qtd Indicações,Participação (%), Valor Total"
	session("arquivo_campo")="Estado,Cidade,qtd,porc,valor"
	if len(ciclo)>0 then
		lbl4	="Ciclo: "
		txt4	=ciclo
		strSql ="exec cli_novosconsultoresCidade_sp '" & dt_fim(strIni) & "','" & dt_fim(strFim) & "','" & codigo & "','1','" & dt_fim(ciclo) & "'," & classificacao
	else
		strSql ="exec cli_novosconsultoresCidade_sp '" & dt_fim(strIni) & "','" & dt_fim(strFim) & "','" & codigo & "',@classificacao=" & classificacao
	end if
	'call rw(strsql,1)
	if len(trim(request("btn2"))) > 0 then call Redi_Arquivo(strSql,"")
	set adoRs1=locConnVenda.execute(strSql)

	call define_coluna ("Estado","1","10","c","t","n","s","","")
	call define_coluna ("Cidade","cidade","*","e","t","n","s","","")	
	call define_coluna ("Qtd Indicações","qtd","10","d","0","s","s","","")
	call define_coluna ("Participação (%)","porc","12","d","2","s","s","","")
	call define_coluna ("Valor Total","valor","10","d","2","s","s","","")
	
elseif relatorio="2" then 'por capital/interior
	configura_arquivo=true
	session("arquivo_titulo")="Estado,Cidade,Qtd Indicações,Participação (%), Valor Total"
	session("arquivo_campo")="Estado,Cidade,qtd,porc,valor"
	if len(ciclo)>0 then
		lbl4	="Ciclo: "
		txt4	=ciclo
		strSql ="exec cli_novosconsultoresCapital_sp '" & dt_fim(strIni) & "','" & dt_fim(strFim) & "','" & codigo & "','1','" & dt_fim(ciclo) & "'," & classificacao
	else
		strSql ="exec cli_novosconsultoresCapital_sp '" & dt_fim(strIni) & "','" & dt_fim(strFim) & "','" & codigo & "',@classificacao=" & classificacao
	end if
	'call rw(strsql,1)
	if len(trim(request("btn2"))) > 0 then call Redi_Arquivo(strSql,"")
	set adoRs1=locConnVenda.execute(strSql)

	call define_coluna ("Estado","estado","10","c","t","n","n","","")
	call define_coluna ("Descrição","descricao","*","e","t","n","s","","")	
	call define_coluna ("Qtd Indicações","qtd","10","d","0","s","s","","")
	'call define_coluna ("Participação (%)","porc","12","d","2","s","s","","")
	'call define_coluna ("Valor Total","valor","10","d","2","s","s","","")
	
	tam_tabela="300"
	tipo_tam_tabela=""

else
	if len(ciclo)>0 then
		lbl4	="Ciclo: "
		txt4	=ciclo
		strSql ="exec cli_novosconsultores_sp '" & dt_fim(strIni) & "','" & dt_fim(strFim) & "','" & codigo & "','1','" & dt_fim(ciclo) & "'," & classificacao
	else
		strSql ="exec cli_novosconsultores_sp '" & dt_fim(strIni) & "','" & dt_fim(strFim) & "','" & codigo & "',@classificacao=" & classificacao
	end if
	'call rw(strsql,1)
	if len(trim(request("btn2"))) > 0 then call Redi_Arquivo(strSql,"")
	set adoRs1=locConnVenda.execute(strSql)

	call define_coluna ("ID","integracao","10","c","t","n","s","","")
	call define_coluna ("Patrocinador","superior","*","e","t","n","s","","")
	call define_coluna ("Qualificação","descricao","10","e","t","n","s","","")
	call define_coluna ("Qtd Indicações","qtd","10","d","0","s","s","","")
	call define_coluna ("Valor Total","valor","10","d","2","s","s","","")
	'call define_LinkCab(1,"ped_lista1.asp?ordem=pediCodigo&pesquisar=" & strCampo & "&strini=" & strIni & "&strFim=" & strfim & "&Status=" & Status & "&Filial=" & Filial & "&cobranca=" & cob & "&tipo=" & tipo)
	'call define_quebra ("TOTAL ", "nome_abreviado","s","nome_abreviado")
	call define_link(4,"cli_RedeNovoCadastro2.asp?strini=" & strini & "&strfim=" & strfim & "&codigo=[integracao]&ciclo=" & ciclo & "&classificacao=" & classificacao)
end if


call fun_rel_cab()
do while not adoRs1.eof
	if relatorio="1" then
		if adors1("ordem")="1" then 'total
			variavel(1)=""
			for i=1 to iColuna
				corColunaFundo(i)="##E8E8FF"				
			next
			iLink(3)=""
			iLink(4)=""
			iLink(5)=""
		elseif adors1("ordem")="99" then
			variavel(1)=""
			for i=1 to iColuna
				corColunaFundo(i)="##C4C4FF"
			next		
			iLink(3)=""
			iLink(4)=""
			iLink(5)=""
		else
			variavel(1)=adors1("estado")
			for i=1 to iColuna
				corColunaFundo(i)=""
			next
			link="cli_RedeNovoCadastro3.asp?strini=" & strini & "&strfim=" & strfim & "&codigo=" & codigo & "&ciclo=" & ciclo & "&estado=[estado]&cidade=[cidade]"
			iLink(3)=link
			iLink(4)=link
			iLink(5)=link			
		end if
	end if
	
	if relatorio="2" then
		if adors1("ordem")="0" or adors1("ordem")="99" then 'total
			for i=1 to iColuna
				corColunaFundo(i)="##E8E8FF"				
			next
		else
			for i=1 to iColuna
				corColunaFundo(i)=""
			next
		end if
	end if

	call fun_rel_det()
	adoRs1.movenext
loop
if relatorio<>"1" and relatorio<>"2" then call fun_fim_rel()

call Func_ImpPag(0,iColuna)
%>
</table></td></tr></table>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->