<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->



<%	
call Verifica_Permissao(10227)

 if ( session("empresa_id")="00000007" ) then
  descricaoIndicante = "Patroc."
 else
  descricaoIndicante = "Indic."
 end if



'Vari�veis submetidas do formul�rio de filtros

 strCepInicio  	  = trim(replace(request("cep_inicio"),"'",""))
 strCepFim  	  = trim(replace(request("cep_fim"),"'","")) 
 strEstado   	  	   = trim(replace(request("estado"),"'",""))
 strCidade   	       = trim(replace(request("cidade"),"'",""))
 strPessoa   	  	   = trim(replace(request("pessoa"),"'",""))
 strGrupo    	  	   = trim(replace(request("grupo"),"'",""))
 strProduto       	   = trim(replace(request("produto"),"'",""))
 strEmailCompleto 	   = trim(replace(request("emailCompleto"),"'",""))
 strRecebeMd 	  	   = trim(replace(request("recebeMd"),"'",""))
 strAtivoSn  	  	   = trim(replace(request("ativoSn"),"'",""))
 strSituacaoFinanceira = trim(replace(request("situacaoFinanceira"),"'",""))
 strMesNiver 	  	   = trim(replace(request("mesNiver"),"'",""))
 strOrdenarA 	  	   = trim(replace(request("ordemA"),"'",""))
 strIntegracao    	   = trim(replace(request("codIntegracao"),"'",""))
 strOnlyMail 	  	   = trim(replace(request("onlyMail"),"'",""))
 strTelefone      	   = trim(replace(request("exibirTelefone"),"'",""))
 
 strDataNasc      	   = trim(replace(request("dataNascimento"),"'",""))
 strRede               = trim(replace(request("rede"),"'","")) 
 strDataCadastro  	   = trim(replace(request("dataCadastro"),"'",""))
 
 strNivelSuperior      = trim(replace(request("nivelSuperior"),"'",""))
 
 qtdEmails             = trim(replace(request("qtdEmails"),"'",""))
 
 

'Monta a colum list
 
 if ( strIntegracao <> "TRUE" ) then
  strColumList = strColumList&" a.destinatario as Codigo, "
 else
  strColumList = strColumList&" b.integracao as Codigo, "
 end if
 
 if ( strOnlyMail <> "TRUE" ) then
    strColumList = strColumList&" a.nome as Nome,a.email as Email,a.cidade as Cidade,a.estado as Estado "
 else
    strColumList = strColumList&" a.email+';' as email,a.nome"
 end if 
 
  IF ( strTelefone = "TRUE" ) THEN
 
 strColumList = strColumList&",(SELECT TOP 1 '('+ddd+') '+telefone FROM view_telefone telRes WHERE descricao LIKE 'RESIDENCIAL' and destinatario=a.destinatario) as Residencial" & _
 ",(SELECT TOP 1 '('+ddd+') '+telefone FROM view_telefone telCel WHERE descricao LIKE 'CELULAR' and destinatario=a.destinatario) as Celular"
  
 END IF 
 
 
 IF ( strDataNasc = "TRUE") THEN
   strColumList = strColumList&",CONVERT(char(10),c.data_nascimento,103) as Data_nascimento"
 END IF
 
  IF ( strDataCadastro = "TRUE") THEN
   strColumList = strColumList&",CONVERT(char(10),a.data_cadastro,103) as Data_cadastro"
 END IF



 'Colunas DEFAULT
  strColumList = strColumList&",a.destinatario"





'Monta a clausula WHERE de acordo com as  op��es selecionadas pelo usu�rio

 'Condicao Where inicial, se n�o for filtrar os emails coloca 1=1 para n�o dar problema com os and das proximas clausulas
 if ( strEmailCompleto = "TRUE" ) then
   strWhere  = " AND len(a.email)>4 and a.email LIKE '%@%' " 
 end if


 if ( strProduto <> "9999999" ) then
   strTabelas = " INNER JOIN ped_pedidoTotal d ON (d.destinatario=a.destinatario) " &_
                " INNER JOIN ped_pedidoItem e ON (d.pediCodigo=e.pediCodigo) "
   strWhere = strWhere&" AND e.prodCodigo='" &strProduto& "'"
   
   strColumList = " DISTINCT "&strColumList
 end if
 

 
  'Exibe somente a rede selecionada
  if ( strNivelSuperior = "TRUE" or strRede <> "1" ) then
   strTabelas = strTabelas & " INNER JOIN fat_hierarquia g ON (a.destinatario=g.destinatario) " &_
                " INNER JOIN fat_hierarquia h ON (h.hierarquia=g.nivel_superior) " &_
				" INNER JOIN fat_destinatario i ON (i.destinatario=h.destinatario) " &_
				" INNER JOIN fat_comercio j ON (j.destinatario=i.destinatario) "
				
				if ( strRede <> "1" ) then
				 strTabelas = strTabelas & " INNER JOIN fat_hierarquia_view as viewHierarquia ON (viewHierarquia.permissao=h.hierarquia) "
				 strWhere = strWhere& " and viewHierarquia.hierarquia= '"& strRede &"' "
				end if
				
  
   strColumList = strColumList&" ,i.destinatario as destSuperior, i.nome as nomeSuperior, j.integracao as codigoSuperior "
   
   strOrdenarA  = "i.nome,a.nome ASC "
   
 end if


 if ( strEstado <> "999" )  then 
  strWhere = strWhere& " and  a.estado= '"& strEstado &"' "
 end if
 
 if len(strCepInicio)>0 then 
  strWhere = strWhere& " and  a.cep>='"& strCepInicio &"' "
 end if
 if len(strCepFim)>0 then 
  strWhere = strWhere& " and  a.cep<='"& strCepFim &"' "
 end if

 if ( strPessoa <> "999" )  then 
  strWhere = strWhere&" and a.fisica_juridica= '"& strPessoa &"' "
 end if

 if ( strGrupo  <> "999" )  then 
  strWhere = strWhere& " and b.faixa_preco= '"& strGrupo &"' "
 end if

 if ( strRecebeMd <> "999" ) then
  strWhere = strWhere& " and a.recebe_email='"& strRecebeMd &"'"
 end if 

 if ( strAtivoSn <> "999" ) then
  strWhere = strWhere& " and a.ativoSn='"& strAtivoSn &"'"
 end if 
 
 
 
 
  if ( strSituacaoFinanceira <> "-1" ) then
  
		'Inadimplentes
		if ( strSituacaoFinanceira = "1" ) then

		  strWhere = strWhere& " AND a.destinatario IN " & _
			 "(SELECT DISTINCT destinatario FROM VIEW_cobranca_saldo WHERE vencimento<getdate()+2 AND situacao='1')"
		
		else

		   strWhere = strWhere& " AND a.destinatario NOT IN " & _
			 "(SELECT DISTINCT destinatario FROM VIEW_cobranca_saldo WHERE vencimento<getdate()+2 AND situacao='1')"
		
		end if  
	  
   end if 







 if ( len(strMesNiver) = "2" ) then
  strWhere = strWhere& " and MONTH(c.data_nascimento)='"& strMesNiver &"'"
 end if 
 
 
 

 

 if ( len(strCidade) > "2" ) then
  strWhere = strWhere& " and a.cidade LIKE '%"& strCidade &"%'"
 end if 
 
 




 strsql="SELECT  "&strColumList & _
 " FROM fat_destinatario a LEFT JOIN fat_comercio b ON (a.destinatario=b.destinatario) LEFT JOIN fat_destinatario_pfisica c ON (c.destinatario=b.destinatario) " & strTabelas & _
 " WHERE 1=1 "&strWhere&" ORDER BY "&strOrdenarA&" "
 
'set adors1=locconnvenda.execute(strsql)
'response.write strsql
'response.end


if len(trim(request("btn2")))>0 then call Redi_Arquivo(strsql,"")


set adors1=server.createobject("AdoDB.Recordset")
set adors1=locconnvenda.execute(strsql)
strTotalReg = 0


if ( request("teste")="s" ) then
		response.write strsql
		response.end
end if

            'percorre o array para contar o total de registros encontrados
			do while not adoRs1.eof
				 strTotalReg = strTotalReg +1
				adoRs1.movenext
			  loop
		   
		   if not adoRs1.bof then
		    adoRs1.movefirst
           end if

'Exibe somente os emails para o usu�rio copiar
if ( strOnlyMail = "TRUE" ) then

    contadorEmails = 0
	nomeTxt       = 1
   
   
    
	response.write "<body>"
	
   
    response.write "<form name='frm1' >"
	response.write "<div align='center'>"
	response.write "Registros: "&strTotalReg
	response.write "<br /><textarea rows='10' cols='75' wrap='soft' name='txt" & nomeTxt & "' >"
			do while not adoRs1.eof
				 
			  IF ( qtdEmails > "0" ) THEN  
				  
				 IF ( cdbl(contadorEmails) = cdbl(qtdEmails) ) THEN
				   response.write "</textarea>"
				   
				   response.write "Total: "&contadorEmails
				  ' response.write  "<a href='javascript:document.frm1.txt1.focus();document.frm1.txt1.select();'>"
				   'response.write  "<font face='Arial, Helvetica, sans-serif' size='2'>Selecionar Tudo</font></a>"	 
				   response.write "<textarea rows='10' cols='75' wrap='soft' name='txt" & nomeTxt & "' >"
				   contadorEmails=0
				   nomeTxt      = nomeTxt+1
				 END IF
				 
				 contadorEmails = contadorEmails+1
				 
			  END IF	 
				 
				response.Write adoRs1("email")
				adoRs1.movenext
			  loop
			  adoRs1.close
	response.write "</textarea>"
	
	IF (qtdEmails >"0" ) THEN
	  response.write "Total: "&contadorEmails	
    END IF
		
	response.write "</div>" 
	response.write "</form>"   

else

    
	lbl1	=""
	txt1	=""
	lbl2	=""
	txt1	=""
	lbl3	="Registros:"
	txt3	=strTotalReg
	lbl4	=""
	txt4	=""

    impcab = 1
	
	'call define_coluna (Titulo, campo, tamanho, alinhamento, formato, total,repete) 
	
	
	IF ( strNivelSuperior = "TRUE" ) THEN
	
	 call define_coluna ("C�digo "&descricaoIndicante,"codigoSuperior","8","c","t","n","n","","")
	 call define_coluna ("Nome "&descricaoIndicante,"nomeSuperior","30","e","t","n","n","","")
	
	END IF
	
	call define_coluna ("C�digo","codigo","8","c","t","n","s","","")
	call define_coluna ("Nome","nome","30","e","t","n","s","","")
	call define_coluna ("Email","email","25","e","t","n","s","","")
	
	
	
	
	 IF ( strTelefone = "TRUE" ) THEN
	  call define_coluna ("Telefone","Residencial","13","c","t","n","s","","")
	  call define_coluna ("Celular","Celular","13","c","t","n","s","","")
	 END IF
	
	
    call define_coluna ("Cidade","cidade","20","e","t","n","s","","")
	call define_coluna ("Estado","estado","4","c","t","n","s","","")
	
	 
	IF( strDataNasc = "TRUE") THEN
	  call define_coluna ("Nascimento","data_nascimento","10","e","t","n","s","","")
	END IF
	
	'if ( strPessoa = "999" ) then
	' call define_coluna ("PF / PJ","tipo_pessoa","6","c","t","n","s","","")
	'end if
	
	IF ( strDataCadastro = "TRUE" ) THEN
	  call define_coluna ("Cadastro","data_cadastro","10","e","t","n","s","","") 
	END IF   



	IF ( strNivelSuperior = "TRUE" ) THEN
	
		call define_link(1,"../cliente/cli_Dados.asp?dest=[destSuperior]")	
	    call define_link(2,"../cliente/cli_Dados.asp?dest=[destSuperior]")
		call define_link(3,"../cliente/cli_Dados.asp?dest=[destinatario]")	
	    call define_link(4,"../cliente/cli_Dados.asp?dest=[destinatario]")
	
	else
	
		call define_link(1,"../cliente/cli_Dados.asp?dest=[destinatario]")	
	    call define_link(2,"../cliente/cli_Dados.asp?dest=[destinatario]")	
	
	end if

	
	
	
	call fun_rel_cab()
	do while not adoRs1.eof
		call fun_rel_det()
		adoRs1.movenext
	loop
    call fun_fim_rel()	
call Func_ImpPag(0,iColuna)
end if




%>


</table></td></tr></table>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->