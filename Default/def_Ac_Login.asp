<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<!--#include virtual='public/pub_criptografia.asp'-->
<%

function redi_Aviso(Titulo,Msg)
	session("msgTitulo")=Titulo
	session("msgAviso")=msg
	response.redirect "def_Aviso.asp"
end function

strUsuario	=replace(ucase(request("usuario")),"'","")
strSenha	=replace(request("senha"),"'","")
strAtividade= request("Atividade")
strAmbienteTeste =request("ambiente_teste")

'' Listar somente clientes que est�o vinculado nessa hierarquia_superior (fat_Hierarquia)
'' para logar tamb�m
session("parametro_nivel_superior")="10001,10002,10003"


if decode(request("opcao"))="1" then
	strUsuario	=decode(Request.Form("usuario"))
	strSenha	=decode(Request.Form("senha"))
	strAtividade=decode(Request.Form("atividade"))
end if

''codigo de empresa
session("empresa_id")=""
strsql	="select top 1 empresa=isnull(empresa,0) from sys_atualizacao_sistema_p"
set adoa=conn5.execute(strsql)
if not adoa.eof then session("empresa_id")=adoa("empresa")
set adoa=nothing


strsql=	"select top 1 a.atividade " & _
		" from sys_usuariomovatividade a" & _
		" inner join sys_atividade b on b.atividade = a.atividade " & _
		" inner join fat_comercio c on c.destinatario = b.destinatario_matriz and c.grupo in (3,4,5) " & _
		" inner join fat_Destinatario d on d.destinatario = c.destinatario and d.ativosn = 1 " & _
		" where a.usuario='" & strUsuario & "' "	
'call rw(strsql,1)
set adors1=conn5.execute(strsql)
if adors1.eof then 
	session("msg")=""
	call redi_aviso("Acesso","Cliente n�o possui acesso ao sistema! Em caso de d�vidas entre em contato com a NBC.")
else
	strAtividade=adors1("atividade")
end if
set adors1=nothing


strRediAtiv=request("auto")
if strRediAtiv = "S" then strAtividade=request("atividade")

strRequestTeste=""
if strAmbienteTeste="1" then strRequestTeste = "?ambiente_teste=1"


if len(request("xusuario")) > 0 then
	strUsuario = request("xusuario")
	strSenha = request("xsenha")
	stratividade= request("xatividade")
end if

if strRediAtiv = "S" then  
	strDestino="../index.asp"
else
	redirec_login="def_Login.asp" & strRequestTeste
	
	''autentica��o
	if session("empresa_id")>="10000001" then
		strsql="exec sys_autenticacao_site_sp '" & strUsuario & "','" & strSenha & "','" & Request.ServerVariables("REMOTE_ADDR") & "'," & strAtividade
	else	
		strsql="exec sys_autenticacao_sp '" & strUsuario & "','" & strSenha & "','" & Request.ServerVariables("REMOTE_ADDR") & "'," & strAtividade
	end if	
	
	set adoUsu=conn5.Execute(strSql)
	if adoUsu.eof then
		session("msg")="Erro de Autentica��o"
		Response.Redirect redirec_login
	else
		if cstr(adoUsu("erro"))<>"0" then
			session("msg")=adousu("msg")
			Response.Redirect redirec_login
		end if
	end if
	set adoUsu=nothing
		

	strSql	="select a.emitente,a.data,email=isnull(a.email,''),a.nome,a.validade," + _
			 "nome_Abreviado Nome_Emitente,contadorErro,senha,funcionario,a.manut_destinatario_SN,a.grupo_pedido " & _
			 "from sys_usuario a inner join fat_Destinatario b on a.emitente=b.destinatario " & _
			 "where a.usuario='" & strUsuario & "' and usuario_ativo=1"
	
	set adoUsu=conn5.Execute(strSql)		
	if adousu.eof then
		session("msg")="Erro ao carregar dados."
		Response.Redirect redirect_login
	else
		if dateadd("d",adoUsu("validade"),adoUsu("data")) <= date then
			session("msg")="Venceu o prazo de validade da sua senha, favor alterar"
			strDestino="def_LoginEx.asp" & strRequestTeste
		else					
			strDestino="../index.asp"		
		end if

		session("emitente")	=adoUsu("emitente")	
		session("usuario")	=strUsuario	
		session("email")	=trim(adoUsu("email"))
		session("nome")		=trim(adoUsu("nome"))
		session("manut_destinatario_SN")=adoUsu("manut_destinatario_SN")
		session("grupo_pedido")=adoUsu("grupo_pedido")
		if len(trim(session("nome")))=0 then session("nome")=strUsuario
		session("Nome_Emitente") = trim(adoUsu("Nome_Emitente"))
		
	end if		
end if

''ip atual
strSql="update sys_usuario set contadorerro=0,ip='" & Request.ServerVariables("remote_addr") & "',sessao='" & session.SessionID & "' " & _
		   "where usuario='" & strUsuario & "'"
'call rw(strSql,1)		   
conn5.execute(strSql)



'CONSULTANDO POR ATIVIDADE OS DADOS E VALIDANDO SE  �  UM EBN OU EBN2'
strSql=	"select Titulo,a.descricao,imgFundo,nome=c.nome_abreviado,a.empresa_Adm,serie_padrao=isnull(e.serie_padrao,'99') " & _
		",cor_menuSuperior,cor_titulo,cor_rel_topo,cor_rel_alternar,cor_rel_subtotal,cor_rel_total,cor_rel_parametro," & _
		"a.destinatario_matriz,a.atualizacao_historico,c.cgc_cpf,c.inscricao_rg,c.email " & _	
		",campo_estoque=(0)" & _	
		",integracao=isnull((select top 1 integracao from view_consultor x where c.destinatario=x.destinatario),'0') " & _
        ",serie_padrao_forcar=isnull((select top 1 '1' from fat_Destinatario_ocorrencia x where x.destinatario=a.destinatario_matriz and x.ocorrencia=72),'0') " & _
        ",serie_padrao_forcar_nfe=isnull((select top 1 y.serie from fat_Destinatario_ocorrencia x inner join fat_nfe y on x.destinatario=y.emitente where x.destinatario=a.destinatario_matriz and x.ocorrencia=74),'') " & _
        ",certificado_validade=isnull((select top 1 convert(varchar(10),certificado_validade,103) from fat_nfe x inner join fat_filial_aux y on x.emitente=y.destinatario where c.destinatario=x.emitente),'') " & _
		"from sys_atividade a, sys_LayoutIntranet b, fat_Destinatario c, sys_LayoutPadraoCor d, fat_filial e , fat_comercio f " & _
		"where a.atividade=b.atividade " + _
		" and b.codigo_cor=d.codigo_cor and b.atividade=d.atividade " & _
		" and a.atividade=" & strAtividade & " " + _
		" and a.destinatario_matriz=c.destinatario and c.destinatario=e.destinatario "&_
		" and f.destinatario = a.destinatario_matriz and f.grupo in (3,4,5) "
'call rw(strsql,1)				
set adoAt=conn5.Execute(strSql)
'response.write strSql
'response.end
if not adoAt.eof then
	session("Atividade")=strAtividade
	session("Tit")=adoAt("titulo")
	
	session("cdh_serie_padrao")=adoAt("serie_padrao")
    session("cdh_serie_padrao_forcar")=adoAt("serie_padrao_forcar")
    session("cdh_certificado_validade")=adoAt("certificado_validade")

    if len(trim(adoAt("serie_padrao_forcar_nfe")))>0 then
        session("cdh_serie_padrao")=adoAt("serie_padrao_forcar_nfe")
        session("cdh_serie_padrao_forcar")="1"
    end if
	
	session("cor_MenuSuperior")=adoAt("cor_MenuSuperior")
	session("Cor")=adoAt("cor_Titulo")
	session("cor_rel_topo")=adoAt("cor_rel_topo")
	session("cor_rel_alternar")=adoAt("cor_rel_alternar")
	session("cor_rel_subtotal")=adoAt("cor_rel_subtotal")
	session("cor_rel_total")=adoAt("cor_rel_total")
	session("cor_rel_parametro")=adoAt("cor_rel_parametro")
	
	session("DescAtividade")=adoAt("Descricao")
	session("imgFundo")		=adoAt("imgFundo")
	session("Empresa")		=adoAt("destinatario_matriz")
	session("Empresa_Adm")	=adoAt("empresa_Adm")
	session("descEmpresa")	=adoAt("nome")
	session("historico")	=adoAt("atualizacao_historico")
	session("sh")			="admsql3"	
	session("ip_atual")		=Request.ServerVariables("remote_addr")
	session("sessao_atual")	=session.SessionID
	session("campo_estoque")=adoAt("campo_Estoque")
	session("rede_integracao")=adoAt("integracao")
	session("email")=adoAt("email") 'ALTERADO POR MEDIAW 02/04/13
		
		
	''idioma
	session("idioma_campo")=""	
	strsql	="select top 1 campo=isnull(campo,'') from sys_parametro a inner join sys_idioma b on a.idioma=b.idioma " & _
			 "where a.atividade=" & session("Atividade")			
	set adoa=conn5.execute(strsql)
	if not adoa.eof then session("idioma_campo")=trim(adoa("campo"))
	set adoa=nothing
else
	session("Atividade")=""
	session("Tit")=""
	session("Cor")=""
	session("DescAtividade")=""
	session("imgFundo")=""
	session("Empresa")=""
	session("Empresa_Adm")=""
	session("descEmpresa")=""
	session("ambiente_teste")=""
	session("p_Cosmeticos")=""
	session("exibirAviso")=""	
	session("ip_atual")=""
	session("sessao_atual")	=""
	session("campo_estoque")=""
	session("cdh_serie_padrao")=""
    session("cdh_serie_padrao_forcar")=""
end if

Response.Cookies("Usuario")=session("usuario")
Response.Cookies("Usuario").Expires=now+20
Response.Cookies("Atividade")=session("atividade")
Response.Cookies("Atividade").Expires=now+20

session("cod_tipo")	="011"
session("cod_geral")="001"

strsql = "select atividade from sys_usuario su inner join sys_atividade sa on su.emitente = sa.destinatario_matriz where usuario = '" &strUsuario& "'"
'call rw (strsql,1)
set adousu = conn1.execute(strsql)
if not adousu.eof then
    session("atividade") = adousu("atividade")
end if

'' log
	strsql= "insert into sys_MovAcesso (usuario,aplicacao,datamovimento,autorizado,obs) values " & _
	        "('" & session("usuario") & "',10297,getdate(),'S','Login Franquia - " & Request.ServerVariables("REMOTE_ADDR") & "')"
	conn5.execute(strsql)


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'' carregar vari�veis do sistema
''parametro geral
session("exibe_integracao")	=""
session("hierarquia_rede")	=""
session("alterna_linha")	=""
session("cliente_controle_hierarquia")=""


strsql=	"select isnull(exibe_codigo_integracao,0) exibe,msg=isnull(msg_inicio_sistema,'')" & _
		",hierarquia_rede=isnull(hierarquia_rede,0),alterna_linha=isnull(alterna_linha,0),cosmeticos,versao=isnull(versao,0) " & _
		",combo_vendedor=isnull(combo_vendedor,'S'),cliente_controle_hierarquia=isnull(cliente_controle_hierarquia,0) " & _
		"from sys_Parametro where atividade=" & session("Atividade")
'response.Write strsql
    'response.End
set adoa=conn5.execute(strsql)
if not adoa.eof then 
	session("exibe_integracao")	=adoa("exibe")
	session("hierarquia_rede")	=adoa("hierarquia_rede")
	session("alterna_linha")	=adoa("alterna_linha")
	session("combo_vendedor")   =adoa("combo_vendedor")
	session("cliente_controle_hierarquia")   =adoa("cliente_controle_hierarquia")
	session("p_Cosmeticos")		=adoa("cosmeticos")
	session("versao")			=adoa("versao")
	
	if len(trim(adoa("msg")))>0 then
		if Request.Cookies("MsgSistema")<>"1" then			
			session("msg")=adoa("msg")						
			Response.Cookies("MsgSistema")="1"
			Response.Cookies("MsgSistema").Expires=now+1			
		end if
	end if
end if
set adoa=nothing

''parametro pedido
session("romaneio")	=""
session("ordem_produto")	=""

''avisos
strSql=	"select top 1 aviso c " & _
		"from sys_AvisoUsuario where usuario='" & strUsuario & "' and lido=0 order by data"	
set adoA=conn5.execute(strSql)
if not adoA.eof then
	aviso=adoA("c")
	session("exibirAviso")=aviso
end if 
set adoA=nothing		
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

if len(trim(session("httpgo")))>0 then ''redirecionamento automatico
	go="../../.." & session("httppag") & "?" & replace(session("httpgo"),"&go=1","")
	strDestino=go
else
	''REDIRECIONAR CLIENTE DIRETO PARA PEDIDO
'	strsql = "SELECT integracao FROM fat_comercio WHERE integracao='" &strUsuario&  "'"
'	set adoA=conn5.execute(strSql)
'	if not adoA.eof then
'		strDestino ="../cliente/cli_PesquisarCliente.asp?pedido=1"
'	end if 
'	set adoA=nothing
end if

'Response.Write strdestino
'Response.end

'' AUDITORIA DE ESTOQUE
'' Regra: executar a auditoria na primeira segunda-feira ap�s o dia 05 de cada m�s.
''

session("cdh_autorizado_auditoria")="1"

'if session("usuario")="ADMIN" then
            '' auditoria de estoque
            '' verificar se � obrigat�rio a franquia ter auditoria
            strsql="select ocorrencia from fat_Destinatario_Ocorrencia where destinatario='" & session("empresa_id") & "' and ocorrencia=64"
            set adoAuditoria=conn5.execute(strSql)
            if not adoAuditoria.eof then

                forcar="0"

                 '' for�ar auditoria em qualquer data no respectivo mes (da ocorrencia)
                strsql="select ocorrencia from fat_Destinatario_Ocorrencia where destinatario='" & session("empresa_id") & "' and ocorrencia=65 and month(data)=" & month(date) & " and year(data)=" & year(date)
                set adoAuditoria2=conn5.execute(strSql)
                if not adoAuditoria2.eof then forcar="1"
                set adoAuditoria2=nothing
                'response.write data & "-" & date
                'response.end

    
                if day(date)>5 or forcar="1" or session("empresa_id")="10143544" then       

                    if forcar="1" then
                        data=date
                    else

                        data=date
                        for i=1 to 7
                            if weekday(data)=2 then '' segunda feira.               
                                exit for
                            end if

                            data=dateadd("d",1,data)
                        next

                    end if
    
                    if data=date or session("empresa_id")="10143544" then
                        strsql="select destinatario from fat_Destinatario_ocorrencia where destinatario='" & session("empresa_id") & "' and ocorrencia=62 and month(data)=" & month(date) & " and year(data)=" & year(date)
                        set adoAuditoria2=conn5.execute(strSql)
                        if adoAuditoria2.eof then '' nao fez, precisa fazer valida��o
                            session("cdh_autorizado_auditoria")="0"
                            session("msg")="Necess�rio efetuar confer�ncia mensal de estoque da franquia."
                            response.Redirect "../estoque/est_Auditoria.asp"
                        end if
                        set adoAuditoria2=nothing
                    end if

                end if

            end if
            set adoAuditoria=nothing
'end if

Response.Redirect strDestino

set conn5=nothing
%>
<!--#include virtual="public/pub_Geral1.asp"-->