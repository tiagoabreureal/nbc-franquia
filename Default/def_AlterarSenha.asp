﻿<!--#include file="../public/pub_Componentes.asp"-->
<!--#include file='../public/pub_ParametrosTopo.asp'-->
<%	
	if session("mensagem") <> "" then
		msgErro    = session("mensagem")
	end if
	
	if Request("prt")="" then
		Response.Redirect "rede_login.asp"
	end if	
	
	prt = Request("prt")
	
	strSql = "set arithabort on select prt = dbo.codificarTexto_fn('"&prt&"', 1) set arithabort off"
	set adoPrt = conn1.execute(strSql)
	
	protocolo = split(adoPrt("prt"), "|")
	
	session("prt") = prt
	session("usuario") = protocolo(0)
	session("ip") = Request.ServerVariables("remote_addr")
%>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Hinode - Redefinir Senha</title>

        <link rel="stylesheet" href="../css/esqueci_senha.css" type="text/css" charset="utf-8" />
        <script src="../js/jquery-1.8.3.min.js" type="text/javascript" charset="utf-8"></script>
		
		<script type='text/Javascript'>
			msg = "<%=msgErro%>"
			if (msg!="") {
				alert(msg);
			}
			<%session("mensagem") = ""%>
		</script>				
    </head>
	
	<form name="frm1" action="def_alterarSenha1.asp"  method="post">
		<div>
			<!--container-->
			<div>
				<div id="container">
					<br/>
					<br/>
					<div id="tabs">
						<form>
						   <table width="100%" class="redefinirsenha">
							  <tbody>
								 <tr>
									<td align="center" colspan="2">
									   <h3><img src="../images/password.png" alt="Senha" title="Senha">&nbsp;Redefinir Senha</h3>
									</td>
								 </tr>
								 <tr>
									<td align="center" colspan="2" width="100%">
									   <hr/>
									</td>
								 </tr>
								 <tr>
									<td align="right">Código de Verificação:&nbsp;</td>
									<td align="right">
										<input class="cod-input" type="text" placeholder="Código Verificador" id="cod_verificador" name="cod_verificador" required="">
									</td>
								 </tr>
								 <tr>
									<td align="right">Nova Senha:&nbsp;</td>
									<td align="right">
										<input class="senha-input" type="password" placeholder="Nova Senha" id="nova_senha" name="nova_senha" required="">
									</td>
								 </tr>
								 <tr>
									<td align="right">Confirmar Senha:&nbsp;</td>
									<td align="right">
										<input class="senha-input" type="password" placeholder="Confirme sua Senha" id="conf_senha" name="conf_senha" required="" onpaste="return false;">
									</td>
								 </tr>
								 <tr>
									<td>
										<br/>
										<br/>
									</td>
								</tr>
								 <tr>
									<td align="center" colspan="2" width="100%">
										<input class="senha-botao" type="submit" value="Confirmar" onclick="">
									</td>
								 </tr>
							  </tbody>
						   </table>
						</form>
					</br>
					</br>
	   
					</div>

				</div>
			
				<div style="clear:both"></div>

			</div>

			<div>
				<div style="clear:both;"></div>
				<div id="footer"></div>
			</div>

		</div>
	</form>
    </body>
</html>