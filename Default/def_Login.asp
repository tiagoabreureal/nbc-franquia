﻿<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><%=application("nome_empresa")%> - Home</title>

<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual="public/pub_ParametrosTopo.asp"-->
<%
session("usuario")=""

if session("msg")<>"" then
	msg = session("msg")
  session("msg")=""
end if

strCUser	  =request("usuario")
strCAtividade =request("atividade")
session("ambiente_teste")=request("ambiente_teste")
	
if session("ambiente_teste")="1" then 
	session("Tit")				="## Ambiente Teste - Demonstração ##"
	session("DescAtividade")	="EMPRESA " & session("atividade")
	session("cor_menusuperior")	="#FFA07A"
	session("cor")				="#A52A2A"
	session("imgFundo")			=""
end if

if len(trim(strCUser))=0 then	strCUser	 =Request.Cookies("Usuario")
if len(trim(strCAtividade))=0 then strCAtividade =Request.Cookies("Atividade")
if len(strCAtividade) = 0 then
	strCAtividade = 1
end if


''codigo de empresa
IF (session("empresa_id")="" ) THEN
	strsql	="select top 1 empresa=isnull(empresa,0) from sys_atualizacao_sistema_p"
	set adoa=conn5.execute(strsql)
	if not adoa.eof then session("empresa_id")=adoa("empresa")
	set adoa=nothing
END IF


 If Request.ServerVariables("SERVER_PORT")=80 Then
      Dim strSecureURL
      strSecureURL = application("http")
      strSecureURL = strSecureURL & Request.ServerVariables("SERVER_NAME")
      strSecureURL = strSecureURL & Request.ServerVariables("URL")
      'Response.Redirect strSecureURL
   End If

%>

<style >

*{margin:0; padding:0;}

#total{}

/*#cabecalho{ width:100%; height:130px; background:rgb(171, 131, 50);}*/
#cabecalho{ width:100%; height:130px; background:#555;}

#conteudo{ width:1200px; height:600px; margin-left:auto; margin-right:auto; background:; }

#cont{ float:left; width:600px; height:400px;}

h2{ font-family: "PT Sans",sans-serif; color:#5F5E5C;}

.campo{width:100px; float:left; font-family: "PT Sans",sans-serif; font-size:1.05em;}

.campo2{}

input[type="text"]{
    background:#f2f2f2;
    border:1px solid #999;
    padding:5px;
}

input[type="password"]{
    background:#f2f2f2;
    border:1px solid #999;
    padding:5px;
}

input[type="submit"]{
    background:#f2f2f2;
    border:1px solid #999;
    padding:5px;
	color:#5F5E5C;
}

form{border: 0px solid #1064BE; padding: 15px;}

#rodape{}

</style>

<script type="text/javascript">
	<%if msg <> "" then%>
		alert("<%=msg%>");
	<%end if%>
</script>

</head>

<body>

	<div id="total">
    
    	<div id="cabecalho">
        </div>
        
        <div id="conteudo">
        
        <div id="cont">
       	  <img style="float:none; display:block; margin-left:auto; margin-right:auto; margin-top:34px;" src="<%=application("caminho_logo")%>" >
          
          <br>
          <br>
          <br>
          <br>
          
          <h2 align="center">Home</h2>
        </div>
        
       <!-- Fim div cont-->
               
		<div id="cont">
        	
            <div style="width:280px; height:150px; margin:90px auto 0 auto;">
            
        	<form name='frm1' method='post' action='def_Ac_Login.asp' onsubmit='frm1.btn1.disabled=true'><input type='hidden' name='ambiente_teste' value=''>
                    
                <div class="campo">Usuário:</div>
                
                <div class="campo2">
                    <input type="text" required name='usuario' size='15' maxlength='15' value=''>
                </div>
            	
                <br>
               
                <div class="campo">Senha:</div>
                    
                <div class="campo2">
                    <input type="password" required name='senha' size='15' maxlength='15' value='' onkeyup='autotab2(this,event)'>
                </div>
                 
                <br>
                    
                <div align="center">
                    <input type='submit' name='btn1' value='Acessar' onclick='javascript:frm1.btn1.disabled=false'>
                </div>

                <br/>
                <div align="center">
			        <a onclick="popUpJs('def_EsqueciSenha.asp','300','100');">
			            <font face="Verdana" size="2" style="text-decoration: underline; cursor:pointer;">Esqueci minha senha</font>
                    </a>
                </div>
                
      		</form>
            
            </div>
            
       </div>
       <!-- Fim div cont-->
          
      </div>
        
        <div id="rodape">
        </div>
        
    </div>
    
</body>

</html>
<script type="text/Javascript">

    function popUpJs(pagina, w, h) {
        window.open(pagina, '', 'toolbar=no,menubar=no,resizable=no,status=no,scrollbars=no,width=' + w + ',height=' + h + ',top=100,left=100');
    }	

</script>