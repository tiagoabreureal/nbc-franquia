<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%call Verifica_Permissao(10002)
strOnload="frm1.senha.focus()"
nomeBotao="Alterar Senha"

minimo=0
strsql="select senha_minimo=isnull(senha_minimo,0) from sys_parametro where atividade=" & session("atividade")
set adoa=conn5.execute(strsql)
if not adoa.eof then minimo=adoa("senha_minimo")
set adoa=nothing

repetir=0
strsql=	"select contador_senha_igual=isnull(contador_senha_igual,0) " & _
		"from sys_usuario where usuario='" & session("usuario") & "'"
set adoa=conn5.execute(strsql)
if not adoa.eof then repetir=adoa("contador_senha_igual")
set adoa=nothing
%>

<!--#include virtual="public/pub_Body.asp"-->
<br>
<p align="left"><font face="verdana" size="1">Informa��es:</font>
<br><font face="verdana" size="1" color="green">- Nova Senha precisa ter Letras e N�meros</font>
<%if cstr(minimo)<>"0" then%>
	<br><font face="verdana" size="1" color="green">- M�nimo de <%=minimo%> caracteres</font>
<%
end if
if cstr(repetir)<>"0" then%>
	<br><font face="verdana" size="1" color="green">- N�o pode repetir a(s) <%=repetir %> �ltima(s) senha(s)</font>
<%
end if
form_Validar="Validar(" & minimo & ")"
call Rel_Criar (50,"frm1","def_Ac_LoginEx.asp")	
	call Rel_Item_txt(40,60,"Usu�rio","disabled","usuario",15,15,session("usuario"))	
	call Rel_Item_Linha
	call Rel_Item_txtSenha(40,60,"Nova Senha","senha",15,15,"")
	call Rel_Item_txtSenha(40,60,"Confirme a Nova Senha","senha2",15,15,"")
call Rel_Fim(0)
%>

<script language="VBSCRIPT">
function Validar(m)

	if len(document.frm1.senha.value)=0 or len(document.frm1.senha2.value)=0 then	
		'verificar se foi digitado
		msgbox "Necess�rio preencher e confirmar a nova senha",vbCritical,"ATEN��O"
		document.frm1.senha.focus
		Validar=false
	elseif document.frm1.senha.value<>document.frm1.senha2.value then
		msgbox "Senhas digitadas n�o conferem",vbCritical,"ATEN��O"
		document.frm1.senha.value=""
		document.frm1.senha2.value=""
		document.frm1.senha.focus
		Validar=false
	elseif len(trim(document.frm1.senha.value))<m then
		'verificar minimo de caracteres (6)
		msgbox "Nova senha precisa ter no minimo " & m & " caracteres",vbCritical,"ATEN��O"
		document.frm1.senha.value=""
		document.frm1.senha2.value=""
		document.frm1.senha.focus
		Validar=false	
	else
		flagNumero=0
		flagLetra=0
		for i=1 to len(document.frm1.senha.value)
			if isnumeric(mid(document.frm1.senha.value,i,1)) then
				flagNumero=1
				exit for
			end if
		next				
		for i=1 to len(document.frm1.senha.value)
			if not isnumeric(mid(document.frm1.senha.value,i,1)) then
				flagLetra=1
				exit for
			end if
		next	
	
		if flagNumero=0 or flagLetra=0 then
			msgbox "Nova senha precisa conter letras e n�meros",vbCritical,"ATEN��O"
			document.frm1.senha.value=""
			document.frm1.senha2.value=""
			document.frm1.senha.focus
			Validar=false	
		else
			Validar=true
		end if
	end if
	
end function 
</script>

<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->