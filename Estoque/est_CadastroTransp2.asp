<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10555)

opcao		=request("opcao")
transporte	=request("transporte")

descricao	=ucase(rec("descricao"))
endereco	=ucase(rec("endereco"))
bairro		=ucase(rec("bairro"))
cidade		=ucase(rec("cidade"))
uf			=ucase(rec("uf"))
cep			=ucase(rec("cep"))
inscricao	=ucase(rec("inscricao"))
placa		=ucase(rec("placa"))
uf_placa	=ucase(rec("uf_placa"))
cnpj		=ucase(rec("cnpj"))
ativo		=ucase(rec("ativo"))

link="est_CadastroTransp1.asp"

if opcao="I" then ''incluir
	if len(descricao)=0 then
		session("msg")="Para incluir uma Transportadora, necess�rio informar a descri��o."
		Response.Redirect link
		Response.end
	end if
	
	maxOp=0
	strsql="select transporte=isnull(max(transporte),0)+1 from fat_transporte"
	set adors1=locConnVenda.execute(strsql)
	if not adors1.eof then maxTransporte=adors1("transporte")
	set adors1=nothing
	
	strsql= "insert into fat_Transporte (atividade,transporte,descricao,endereco,bairro,cidade,estado,cep,cgc,inscricao,ativo,placa,uf_placa) values " & _
			"(" & session("atividade") & "," & maxTransporte & ",'" & descricao & "','" & endereco & "','" & bairro & "','" & cidade & "'" & _
			",'" & uf & "','" & cep & "','" & cnpj & "','" & inscricao & "'," & ativo & ",'" & placa & "','" & uf_placa & "')"		
	'call rw(strsql,1)
	locConnVenda.execute(strsql)
			
	session("msg")="Transportadora inclu�da com sucesso."
elseif opcao="A" then ''alterar
	strsql="select transporte from fat_Transporte where atividade=" & session("atividade")
	set adors1=locconnvenda.execute(strsql)
	do while not adors1.eof
		transporte	=adors1("transporte")
		cdescricao	="descricao" & transporte
		cendereco	="endereco" & transporte
		cbairro		="bairro" & transporte
		ccidade		="cidade" & transporte
		cuf			="uf" & transporte
		ccep		="cep" & transporte
		ccnpj		="cnpj" & transporte
		cinscricao	="inscricao" & transporte
		cativo		="ativo" & transporte
		cplaca		="placa" & transporte
		cuf_placa	="uf_placa" & transporte
				
		descricao	=ucase(rec(cdescricao))
		endereco	=ucase(rec(cendereco))
		bairro		=ucase(rec(cbairro))
		cidade		=ucase(rec(ccidade))
		uf			=ucase(rec(cuf))
		cep			=rec(ccep)
		cnpj		=rec(ccnpj)
		inscricao	=rec(cinscricao)
		ativo		=rec(cativo)
		placa		=ucase(rec(cplaca))
		uf_placa	=ucase(rec(cuf_placa))
		
		
		strsql=	"update fat_Transporte set descricao='" & descricao & "'" & _
				",endereco='" & endereco & "',bairro='" & bairro & "',cidade='" & cidade & "'" & _
				",estado='" & uf & "',cep='" & cep & "',inscricao='" & inscricao & "'" & _
				",cgc='" & cnpj & "',ativo=" & ativo & " " & _
				",placa='" & placa & "',uf_placa='" & uf_placa & "' " & _
				"where atividade=" & session("atividade") & " and transporte=" & transporte
		'call rw(strsql,1)
		locconnvenda.execute(strsql)
												
		adors1.movenext
	loop
	set adors1=nothing
	session("msg")="Atualizado com sucesso."

elseif opcao="E" then ''excluir
	
	strsql="select top 1 transportadora from fat_saida where transportadora=" & transporte
	set adors1=locconnvenda.execute(strsql)
	if not adors1.eof then
		session("msg")="Existe nota fiscal emitida com essa transportadora, n�o � poss�vel excluir."
		Response.Redirect link
		Response.end
	end if
	set adors1=nothing
		
	strsql="delete fat_Transporte where atividade=" & session("atividade") & " and transporte=" & transporte
	locconnvenda.execute(strsql)	
	
	session("msg")="Transportadora exclu�da com sucesso."	
	
end if


Response.Redirect link

%>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->