<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10585)

consultor	=trim(replace(request("consultor"),"'",""))
produto		=trim(replace(request("produto"),"'",""))

troca		=trim(replace(request("troca"),"'",""))
qtd			=trim(replace(request("qtd"),"'",""))
nota		=trim(replace(request("nota"),"'",""))
serie		=trim(replace(request("serie"),"'",""))

if len(troca)>0 then strOnload="frm1.novoProduto.focus();"

link="est_TrocaProduto.asp"

if len(consultor)=0 then
	session("msg")="Informe o c�digo do consultor."
	Response.Redirect link
	Response.end
end if

if len(produto)=0 then
	session("msg")="Informe o c�digo do produto."
	Response.Redirect link
	Response.end
end if

if len(consultor)<8 then consultor=string(8-len(consultor),"0") & cstr(consultor)
if len(produto)<6	then produto=string(6-len(produto),"0") & cstr(produto)

strsql="select destinatario,nome,estado from view_Consultor where integracao='" & consultor & "'"
set adors1=locconnvenda.execute(strsql)
if adors1.eof then
	session("msg")="Consultor n�o localizado."
	Response.Redirect link
	Response.end
else
	destinatario=adors1("destinatario")
	nome	=adors1("nome")	
	estado	=adors1("estado")
end if
set adors1=nothing

'' data-limite para troca
strsql="select valor=isnull(valor,0) from loja_parametros where id=5"
set adors1=locconnvenda.execute(strsql)
if adors1.eof then
	session("msg")="N�o localizado par�metro de dias para an�lise de troca."
	Response.Redirect link
	Response.end
else
	dias=adors1("valor")
end if
set adors1=nothing

'' pre�o do produto hoje
strsql=	"select top 1 preco_consultor " & _
		"from view_regra_promocao " & _ 
		"where produto='" & produto & "' and convert(datetime,convert(varchar(10),getdate(),120)) between inicio and fim " & _
		"order by prioridade"
set adors1=locconnvenda.execute(strsql)
if not adors1.eof then	
	precoAtual	=adors1("preco_consultor")
end if
set adors1=nothing


lbl1	=""
txt1	=""
lbl2	=""
txt2	=""
lbl3	=""
txt3	=""
lbl4	=""
txt4	=""

impcab = 1

dataLimite	=dateadd("d",dias*-1,date)

strSql ="select a.emitente,b.produto,b.descricao,a.referencia,a.nota,a.serie,a.quantidade,a.unitario,valor_total" & _
		",nf=convert(varchar(10),nota) + '-' + serie " & _
		"from fat_Saida a inner join sys_produto b on a.produto=b.produto " & _
		"where a.emitente='" & session("empresa_id") & "' and a.destinatario='" & destinatario & "' and a.produto='" & produto & "' and a.referencia>='" & dt_fim(dataLimite) & "' " & _
		"order by referencia desc"
'call rw(strsql,1)
if len(trim(request("btn2"))) > 0 then call Redi_Arquivo(strSql,"")
set adoRs1=locConnVenda.execute(strSql)
if not adors1.eof then	
	descricao=produto & " - " & adoRs1("descricao")
end if


call define_coluna ("Nota Fiscal","nf","7","c","t","n","n","","")
call define_coluna ("Data","referencia","9","c","t","n","n","","")
call define_coluna ("Produto","produto","6","c","t","n","n","","")
call define_coluna ("Descri��o","descricao","*","e","t","n","n","","")
call define_coluna ("Qtd","quantidade","5","d","0","n","s","","")
call define_coluna ("Unit�rio","unitario","6","d","2","n","s","","")
call define_coluna ("Valor Total","valor_total","8","d","2","n","s","","")
call define_coluna ("Pre�o Atual","1","8","d","2","n","s","##ffcc99","")
call define_coluna ("Mensagem","2","15","c","t","n","s","##ffcc99","")

'call define_LinkCab(1,"ped_lista1.asp?ordem=pediCodigo&pesquisar=" & strCampo & "&strini=" & strIni & "&strFim=" & strfim & "&Status=" & Status & "&Filial=" & Filial & "&cobranca=" & cob & "&tipo=" & tipo)
'call define_quebra ("TOTAL ", "nome_abreviado","s","nome_abreviado")
call define_link(1,"../faturamento/fat_nf.asp?nf=[nota]&se=[serie]&filial=[emitente]")

link="est_TrocaProduto1.asp?consultor=" & consultor & "&produto=" & produto & "&qtd=[quantidade]&nota=[nota]&serie=[serie]&troca=S"
call define_link(9,link)

strParametroTab1="<tr>" & _
	"	<td align='left' bgcolor='#f6f6f6' colspan=3><font face='verdana' size='2'>Consultor:</font></td>" & _
	"	<td colspan=10><font face='verdana' size='2'>" & consultor & " - " & nome & "</font></td>" & _
	"</tr>" & _
	"<tr>" & _
	"	<td align='left' bgcolor='#f6f6f6' colspan=3><font face='verdana' size='2'>Data Limite Troca:</font></td>" & _
	"	<td colspan=10><font face='verdana' size='2'>" & dataLimite & "</font></td>" & _
	"</tr>"

variavel(1)=precoAtual
call fun_rel_cab()
do while not adoRs1.eof
	
	if cdbl(precoAtual) > cdbl(adors1("unitario")) then
		variavel(2)="Eventual Preju�zo<br>" & _
					"<font color='blue'>[efetuar troca]</font>"
		iLink(9)=link
	elseif cdbl(precoAtual) < cdbl(adors1("unitario")) then
		variavel(2)="N�o pode ser feita."
		iLink(9)=""
	else
		variavel(2)="<font color='blue'>[efetuar troca]</font>"
		iLink(9)=link
	end if

	call fun_rel_det()
	adoRs1.movenext
loop
call fun_fim_rel()
call Func_ImpPag(0,iColuna)
%>
</table></td></tr>



<tr><td colspan="10">
<br><br><br>

<%

if troca="S" then
	call form_criar("frm1","est_TrocaProduto2.asp")
		call txtOculto_Criar("consultor",consultor)
		call txtOculto_Criar("produto",produto)
		call txtOculto_Criar("qtd",qtd)
		call txtOculto_Criar("nota",nota)
		call txtOculto_Criar("serie",serie)
		
%>
<table border="1" width="500" style='border-collapse: collapse' cellpadding="0" cellspacing="1">
	<tr>
		<td colspan="10" align="left" bgcolor="#f5f5f5"><font face="verdana" size="2" color="red"><b>.Confirma��o de Troca</b></font></td>
	</tr>
	<tr>
		<td width="20%" bgcolor="#f5f5f5"><font face="verdana" size="2">Produto:</font></td>
		<td width="80%"><font face="verdana" size="2"><%=descricao%></font></td>
	</tr>
	<tr>
		<td width="*" bgcolor="#f5f5f5"><font face="verdana" size="2">Trocar para:</font></td>
		<td width="*"><font face="verdana" size="2"><%call txt_Criar("inteiro","novoProduto",6,6,"")%></font></td>
	</tr>
	<tr>
		<td width="*" bgcolor="#f5f5f5"><font face="verdana" size="2">Quantidade:</font></td>
		<td width="*"><font face="verdana" size="2">
			<%			
			call select_criar("novaQtd","")								
				for i=1 to qtd
					call select_item(i,i,"")
				next
			call select_fim()
			%>
			</font>
		</td>
	</tr>
	<tr>
		<td colspan="10" align="center"><font face="verdana" size="2"><%call button_criar("submit","btn1","Confirmar Troca","")%></font></td>
	</tr>
</table>
<%
	call form_fim()
end if
%>

</table>

<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->