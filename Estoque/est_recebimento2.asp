<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10543)

nota	=replace(trim(request("nota")),"'","")
serie	=replace(trim(request("serie")),"'","")

link	="est_recebimento1.asp"
data=date

'if session("usuario")="ADMIN" then

    '' implementado em 29/09/2015 13:40 (Kleber)
    'strsql="est_ValidarEntrada_sp '" & session("empresa_id") & "'," & nota & ",'" & serie & "'"
    'set adors1=locconnvenda.execute(strSql)
    'if not adors1.eof then
        'if cstr(adors1("cod"))<>"0" then
            'call redi_aviso("Entrada/Recebimento",adors1("msg"))
            'response.end
        'end if
    'end if
    'set adors1=nothing

'end if

'' Colocado valida��o em 24/06/15 (Kleber), para n�o duplicar movimento.
strSql="select id_movimento from est_Movimento where local_estoque='" & session("empresa_id") & "' and documento='" & nota & "' and serie='" & serie & "' and operacao=17"
set adors1=locconnvenda.execute(strSql)
if not adors1.eof then
    call redi_aviso("Entrada/Recebimento","Nota Fiscal " & nota & "-" & serie & "' j� registrada como Entrada na franquia.")
    response.end
end if
set adors1=nothing


strsql="select produto,quantidade,pedido " & _
		"from fat_saida " & _
		"where emitente in ('00000001') and destinatario='" & session("empresa_id") & "' and nota=" & nota & " and serie='" & serie & "' " & _
		" and produto<>'999999' and situacao=0 and dtentrada is null order by produto"
'call rw(strsql,1)		
set adors1=locconnvenda.execute(strsql)
if adors1.eof then
	session("msg")="Nota n�o localizada."
else
	'' Efetuando consist�ncia de dados. Verifica se quantidade de quebrado + faltando n�o � maior que a quantidade do produto
	'' Criado em 21/06/2013. Helsen.
	do while not adors1.eof
		p	=adors1("produto")
		qtd	=adors1("quantidade")	
		pedido =adors1("pedido")
        empresa = session("empresa_id")
        usu = session("usuario")
		quebrado	=rec("p_quebrado" & p)
		faltando	=rec("p_faltando" & p)
		
		if len(trim(quebrado))=0 then quebrado = 0		
		if len(trim(faltando))=0 then faltando = 0
		
		if isnumeric(quebrado)=false then call redi_aviso("Entrada Estoque","Produto " & P & " foi informado quantidade inv�lida para produto quebrado")
		if isnumeric(faltando)=false then call redi_aviso("Entrada Estoque","Produto " & P & " foi informado quantidade inv�lida para produto faltando")
		
		queb_falt=abs(cdbl(quebrado)) + abs(cdbl(faltando))
		
		if (queb_falt > cdbl(qtd)) then
			call redi_aviso("Quantidade Inv�lida","Quebrado e Faltando superior a quantidade do produto: " & p & " <br>>>Quebrado+Faltando = " & queb_falt & " <br>>>Quantidade = " & cdbl(qtd))
			Response.End
		end if
		adors1.movenext
	loop 
	
	if adors1.eof = false or adors1.bof = false then adors1.movefirst
	
	do while not adors1.eof
		
		
		p	=adors1("produto")
		qtd	=adors1("quantidade")						
		
		strsql=	"insert into est_movimento (id_movimento,local_estoque,produto,data,operacao,quantidade,situacao,dataatualizacao,usuario,observacao,documento,serie) " & _
				"select max(isnull(id_movimento,0))+1,'" & session("empresa_id") & "','" & p & "','" & dt_fim(data) & "'" & _
				",42," & qtd & ",0,getdate(),'" & session("usuario") & "','confirmado recebimento de mercadoria matriz - nota " & nota & "-" & serie & "','" & nota & "','" & serie & "' from est_movimento"
		'call rw(strsql,1)					
		locconnvenda.execute(strsql)
		
		
		'' Alterado em 19/02/2013 - Solicita��o de Ancelmo: retirada da coluna "produto a mais"
		p_quebrado	=rec("p_quebrado" & p)
		p_faltando	=rec("p_faltando" & p)
'		p_mais		=rec("p_mais" & p)
		
		'Response.Write p & " - " & p_quebrado & "<br>"
		'Response.Write p & " - " & p_faltando & "<br>"
		'Response.Write p & " - " & p_mais & "<br>"
		'Response.end
		
		''produto quebrado
		if len(p_quebrado)>0 then
			if isnumeric(p_quebrado) and cdbl(p_quebrado)>0 then			
				qtd=abs(p_quebrado)
				obs="Produto Quebrado"
				operacao=43
				strsql=	"insert into est_movimento (id_movimento,local_estoque,produto,data,operacao,quantidade,situacao,dataatualizacao,usuario,observacao,documento,serie) " & _
						"select max(isnull(id_movimento,0))+1,'" & session("empresa_id") & "','" & p & "','" & dt_fim(data) & "'" & _
						"," & operacao & "," & qtd & ",0,getdate(),'" & session("usuario") & "','" & obs & " - nota " & nota & "-" & serie & "','" & nota & "','" & serie & "' from est_movimento"				
				'call rw(strsql,1)
				locconnvenda.execute(strsql)
                ''strproc = "exec ped_gerardevolucao_sp '"& pedido &"','"& empresa &"','"& usu &"','"& p &"','"& qtd &"','"& obs &"'"
                ''response.Write strproc
               '' response.End
                ''SET adorproc = locconnvenda.execute(strproc)						
			end if
		end if
		
		''produto faltando
		if len(p_faltando)>0 then
			if isnumeric(p_faltando) and cdbl(p_faltando)>0 then	
				qtd=abs(p_faltando)
				obs="Produto Faltando"
				operacao=43
				strsql=	"insert into est_movimento (id_movimento,local_estoque,produto,data,operacao,quantidade,situacao,dataatualizacao,usuario,observacao,documento,serie) " & _
						"select max(isnull(id_movimento,0))+1,'" & session("empresa_id") & "','" & p & "','" & dt_fim(data) & "'" & _
						"," & operacao & "," & qtd & ",0,getdate(),'" & session("usuario") & "','" & obs & " - nota " & nota & "-" & serie & "','" & nota & "','" & serie & "' from est_movimento"
				'call rw(strsql,1)
				locconnvenda.execute(strsql)						
			end if
		end if
		
		''produto a mais
'		if len(p_mais)>0 then
'			if isnumeric(p_mais) then
'				qtd=abs(p_mais)
'				obs="Produto a mais"
'				operacao=42
'				strsql=	"insert into est_movimento (id_movimento,local_estoque,produto,data,operacao,quantidade,situacao,dataatualizacao,usuario,observacao,documento) " & _
'						"select max(isnull(id_movimento,0))+1,'" & session("empresa_id") & "','" & p & "','" & dt_fim(data) & "'" & _
'						"," & operacao & "," & qtd & ",0,getdate(),'" & session("usuario") & "','" & obs & " - nota " & nota & "-" & serie & "','" & nota & "' from est_movimento"
'				'call rw(strsql,1)
'				locconnvenda.execute(strsql)						
'			end if
'		end if		
		
		adors1.movenext		
	loop	
	
	strsql=	"update fat_saida set dtentrada='" & dt_fim(data) & "' " & _
			"where destinatario='" & session("empresa_id") & "' and nota=" & nota & " and serie='" & serie & "'"
	locconnvenda.execute(strsql)
	       
		
	strsql="exec est_EstoqueAtual_sp_i"
	locconnvenda.execute(strsql)
	
    session("msg")="Nota fiscal confirmada no estoque."
	
end if
set adors1=nothing



Response.Redirect link
%>

<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->