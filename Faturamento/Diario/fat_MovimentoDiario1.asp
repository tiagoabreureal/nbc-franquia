<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10424)

emitente = session("empresa_id")
relatorio  = request("relatorio")
strIni	   = replace(request("strini"),"'","")
strFim	   = replace(request("strFim"),"'","")
ordenar    = request("ordenar")
liberado_por=request("liberado_por")
cupom=request("cupom")
if len(cupom)=0 then cupom="0"

transportadora = request("transportadora")
'transportadora = "-1"


if relatorio = "produto" then
    if ordenar = "nota" or ordenar = "pedido" or ordenar = "a.produto" then ordenar = "produto"
    if ordenar = "b.descricao" then ordenar = "descricao"
    'if ordenar = "descricao"
end if

lbl1	=""
txt1	=""
lbl2	=""
txt1	=""
lbl3	="Per�odo:"
txt3	=strIni & "  -  " & strFim

if liberado_por="-1" then
    lbl4	=""
    txt4	=""
else
    lbl4	="Liberado por: "
    txt4	=liberado_por
end if

if cupom="1" then
    lbl5	="Cupom: "
    txt5	="Sim"
end if

impcab = 1
retzero = true

SELECT CASE relatorio

  CASE "produto"
   'Response.Write relatorio
   
   strSql=" SET nocount ON " & _ 
          " SELECT a.produto,b.descricao,convert(int,sum(a.quantidade)) as qtd " & _
          " INTO #final " & _
          " FROM fat_saida a INNER JOIN sys_produto b ON (a.produto=b.produto) " & _
          "		INNER JOIN fat_operacao c ON (a.operacao=c.operacao) " & _
          " WHERE " & _
          " a.referencia BETWEEN '" & dt_fim(strIni) & "' and '" & dt_fim(strFim) & "'" & _
          " and emitente='" &session("empresa_id")& "' " & _
          " and a.produto<>'999999' and situacao=0 and entsai=2 "

          if cupom="1" then
            strSql=strSql & "and a.pedido in(select idpedido from hinode_loja.dbo.cupom_movimento) "
          end if

   strSql=strSql & " GROUP BY a.produto,b.descricao " & _ 
          " UNION ALL " & _
          " SELECT d.produto,e.descricao,convert(int,sum(d.quantidade)) as qtd " & _
          " FROM hinode_historico..fat_saida2015 d INNER JOIN sys_produto e ON (d.produto=e.produto) " & _
          "		INNER JOIN fat_operacao f ON (d.operacao=f.operacao) " & _
          " WHERE " & _
          " d.referencia BETWEEN '" & dt_fim(strIni) & "' and '" & dt_fim(strFim) & "'" & _
          " and emitente='" &session("empresa_id")& "' " & _
          " and d.produto<>'999999' and situacao=0 and entsai=2 "

          if cupom="1" then
            strSql=strSql & "and d.pedido in(select idpedido from hinode_loja.dbo.cupom_movimento) "
          end if

          strSql=strSql & " GROUP BY d.produto,e.descricao " & _
          " SELECT produto, descricao, convert(int,sum(qtd)) as qtd " & _
          " FROM #final " & _
          " GROUP BY produto,descricao " & _
          " ORDER BY " & ordenar & " ASC " & _
          " DROP table #final " & _ 
          " SET nocount OFF " 
          
          if session("usuario")="ADMIN" and request("teste") = "0" then call rw(strSql,0)
		  if len(trim(request("btn2"))) > 0 then call Redi_Arquivo(strSql,"")
		  
		  set adoRs1=locConnVenda.execute(strSql)

		  call define_coluna ("Produto","produto","10","c","t","n","s","","")
		  call define_coluna ("Descri��o","descricao","*","e","t","n","s","","")
		  call define_coluna ("Qtd","qtd","10","d","0","s","s","","")
			
		  link="fat_MovimentoDiario2.asp?strIni=" & strini & "&strfim=" & strfim & "&produto=[produto]"
		  call define_link(1,link)
		  call define_link(2,link)
		  call define_link(3,link)

  CASE "nota"
   'Response.Write relatorio
   
      SELECT CASE ordenar
      
       CASE "a.produto"
        ordenar = "c.integracao"
   
       CASE "b.descricao" 
        ordenar = "a.nome"
   
      END SELECT
      
      impressao_tipo="P"
      
   
      strSql=" SELECT DISTINCT c.integracao as codigo,a.destinatario,a.nome,a.estado,b.emitente ,c.integracao " & _
          "          ,b.nota,b.serie,b.pedido,convert(char(10),b.dtFaturamento,103) as emissao " & _
          "          ,d.descricao as transportadora,valorTotal=f.valorTotalNf,f.valorfrete,g.descricao Cobranca " & _ 
          "          ,convert(bigint,isnull((SELECT TOP 1 replace(replace(right(isnull(xx.complemento,'0'),len(xx.complemento)-4),',',''),'.','') from ped_observacao xx WHERE xx.observacao='201' and xx.pediCodigo=b.pedido ORDER BY xx.data DESC),'0')) as peso " & _
          "			 ,juros=isnull((select top 1 convert(numeric(9,2),complemento) from ped_Observacao x where x.pediCodigo=f.pediCodigo and observacao=211),0)	" & _
	  "         ,credito=isnull((select top 1 convert(numeric(9,2),complemento) from ped_Observacao x where x.pediCodigo=f.pediCodigo and observacao=212),0)	" & _
	  "  ,liberado=isnull((select top 1 usuario from ped_Movimentostatus x where b.pedido=x.pediCodigo and x.stnfcodigo=10 order by datamovimento),'') " & _
          " FROM fat_destinatario a INNER JOIN fat_saida b ON (a.destinatario=b.destinatario) " & _
          "		 INNER JOIN fat_operacao e ON (b.operacao=e.operacao) " & _
          "      LEFT JOIN fat_comercio c ON (a.destinatario=c.destinatario) "  & _
          "      LEFT JOIN fat_transporte d ON (d.transporte=b.transportadora)  " & _
		  "      INNER JOIN ped_pedidoTotal f ON (f.pediCodigo=b.pedido) " & _        
		  "      inner join fat_tipo_cobranca g on f.tipoCobranca=g.tipo " & _
          " WHERE " & _
          " b.dtfaturamento BETWEEN '" & dt_fim(strIni) & "' and '" & dt_fim(strFim) & "'" & _
          " and b.emitente='" &session("empresa_id")& "' " & _
          " and b.produto<>'999999' and situacao=0 and e.entsai=2 " & _
		  " and (b.transportadora='"&transportadora&"' OR '-1'='"&transportadora&"' )"

          if liberado_por<>"-1" then
             strSql=strSql & " and b.pedido in(select pedicodigo from ped_Movimentostatus where usuario='" & liberado_por & "' and stnfcodigo=10) "
          end if

            if cupom="1" then
            strSql=strSql & "and b.pedido in(select idpedido from hinode_loja.dbo.cupom_movimento) "
            end if

      strSql = strSql & " UNION ALL " & _
      " SELECT DISTINCT x.integracao as codigo,a.destinatario,a.nome,a.estado,b.emitente ,c.integracao " & _
          "          ,b.nota,b.serie,b.pedido,convert(char(10),b.dtFaturamento,103) as emissao " & _
          "          ,d.descricao as transportadora,valorTotal=f.valorTotalNf,f.valorfrete,g.descricao Cobranca " & _ 
          "          ,convert(bigint,isnull((SELECT TOP 1 replace(replace(right(isnull(xx.complemento,'0'),len(xx.complemento)-4),',',''),'.','') from ped_observacao xx WHERE xx.observacao='201' and xx.pediCodigo=b.pedido ORDER BY xx.data DESC),'0')) as peso " & _
          "			 ,juros=isnull((select top 1 convert(numeric(9,2),complemento) from view_ped_Observacao2015 x where x.pediCodigo=f.pediCodigo and observacao=211),0)	" & _
	  "         ,credito=isnull((select top 1 convert(numeric(9,2),complemento) from ped_Observacao x where x.pediCodigo=f.pediCodigo and observacao=212),0)	" & _
	  "  ,liberado=isnull((select top 1 usuario from view_ped_movimentostatus2015 x where b.pedido=x.pediCodigo and x.stnfcodigo=10 order by datamovimento),'') " & _
          " FROM fat_destinatario a INNER JOIN hinode_historico..fat_saida2015 b ON (a.destinatario=b.destinatario) " & _
          "		 INNER JOIN fat_operacao e ON (b.operacao=e.operacao) " & _
          "      LEFT JOIN fat_comercio c ON (a.destinatario=c.destinatario) "  & _
          "      LEFT JOIN fat_transporte d ON (d.transporte=b.transportadora)  " & _
		  "      INNER JOIN hinode_historico..ped_pedidoTotal2015 f ON (f.pediCodigo=b.pedido) " & _
          "      inner join view_consultor x on b.destinatario=x.destinatario" & _
		  "      inner join fat_tipo_cobranca g on f.tipoCobranca=g.tipo " & _
          " WHERE " & _
          " b.dtfaturamento BETWEEN '" & dt_fim(strIni) & "' and '" & dt_fim(strFim) & "'" & _
          " and b.emitente='" &session("empresa_id")& "' " & _
          " and b.produto<>'999999' and situacao=0 and e.entsai=2 " & _
		  " and (b.transportadora='"&transportadora&"' OR '-1'='"&transportadora&"' )"

          if liberado_por<>"-1" then
             strSql=strSql & " and b.pedido in(select pedicodigo from view_ped_movimentostatus2015 where usuario='" & liberado_por & "' and stnfcodigo=10) "
          end if

            if cupom="1" then
            strSql=strSql & "and b.pedido in(select idpedido from hinode_loja.dbo.cupom_movimento) "
            end if

          strSql=strSql & " ORDER BY " & ordenar & " ASC " 
          
          if session("usuario")="ADMIN" and request("teste") = "0" then call rw(strSql,0)
		  if len(trim(request("btn2"))) > 0 then call Redi_Arquivo(strSql,"")
		  
		  set adoRs1=locConnVenda.execute(strSql)

		  call define_coluna ("C�digo","codigo","4","c","t","n","s","","")
		  call define_coluna ("Nota","nota","3","c","t","n","s","","")
		  call define_coluna ("Pedido","pedido","3","c","t","n","s","","")
		  call define_coluna ("Faturamento","emissao","5","c","t","n","s","","")
		  call define_coluna ("Nome","nome","20","e","t","n","s","","")
		  call define_coluna ("UF","estado","1","c","t","n","s","","")
		  call define_coluna ("Transportadora","transportadora","8","e","t","n","s","","")
		  call define_coluna ("Pagamento","cobranca","8","e","t","n","s","","")
		  call define_coluna ("Valor","valorTotal","3","d","2","s","s","s","")
		  call define_coluna ("Valor Frete","valorfrete","3","d","2","s","s","s","")
		  call define_coluna ("Juros","juros","3","d","2","s","s","s","")
		  call define_coluna ("Credito","credito","3","d","2","s","s","s","")
		  call define_coluna ("Valor Total","3","3","d","2","s","s","s","")
		  call define_coluna ("Peso","peso","3","d","0","s","s","s","")
		  call define_coluna ("Liberado por","liberado","6","c","t","n","s","s","style='font-size: 5pt'")
		'  call define_coluna ("Itens","qtd","8","e","0","s","s","","")
		  
          call define_link (1,"../../cliente/Cli_Dados.asp?dest=[destinatario]")
          call define_link (2,"../../nfe/nfe_Nf.asp?nf=[nota]&se=[serie]")
          call define_link (3,"../../pedido/Ped_Pedido.asp?ped=[pedido]")

	  call define_link(8,"javascript:AbreStatus([pedido],'[emitente]','" & session("usuario") & "'," & session("atividade") & ")")
  


  CASE ELSE
   
   session("msg")="Relat�rio n�o selecionado ou Inv�lido!"
   Response.Redirect "fat_MovimentoDiario.asp"
  
 END SELECT  
 
 call fun_rel_cab()

 do while not adoRs1.eof

	 if relatorio="nota" then
		 variavel(3)=cdbl(adors1("valortotal")) + cdbl(adors1("juros")) + cdbl(adors1("valorfrete")) - cdbl(adors1("credito"))
	 end if

	 call fun_rel_det()
	  	 intConta=intConta+1
	 inttotal = intTotal + 1
	 adoRs1.movenext
 loop

 call fun_fim_rel()

    %></table>
</td></tr>
	<tr><td width="100%" align="center"><br/><br/>
<%

    if (relatorio ="nota") then
        
        call zerar_relatorio()

        strSql2 = "exec fat_resumo_movdiario_sp '" &emitente &"' , '" &dt_fim(strIni) &"' , '" &dt_fim(strFim) &"','" & liberado_por & "','" & cupom & "'"
        if session("usuario")="ADMIN" and request("teste") = "0" then call rw(strSql2,0)
    
        set adoRs1=locConnVenda.execute(strSql2)
        tipo_tam_tabela=""
        tam_tabela=400      
        retzero = "True"
             'call define_coluna (Titulo, campo, tamanho, alinhamento, formato, total,repete,corColuna,outros)
              call define_coluna ("Forma de Pagamento","descricao","10","e","t","n","s","","")
		      call define_coluna ("Valor Total","valor_total","5","d","2","s","s","","")
		      call define_coluna ("Cr�dito","credito","5","d","2","s","s","","")
		      call define_coluna ("L�quido","valor_liquido","5","d","2","s","s","","")
	
        response.write "<center>"
        call fun_rel_cab()

            do while not adoRs1.eof
	            call fun_rel_det()
	            adoRs1.movenext
            loop

        call fun_fim_rel()
        response.write "</center>"

    end if 

call Func_ImpPag(1,iColuna)





%>
</td></tr>
<tr><td width="100%" colspan="9">

</table>


<script>
function AbreStatus(strD,strFil,c,d)	{
	window.open("../../pedido/ped_Status.asp?xatividade=" + d + "&p=" + strD + "&Filial=" + strFil + "&xusuario=" + c,"Status","scrollbars=yes,toobar=no,width=600,height=320,screenX=0,screenY=0,top=100,left=100");	
						}						
</script>

<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->
