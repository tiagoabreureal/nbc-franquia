<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%call Verifica_Permissao(158)%>
<!--#include virtual="public/pub_Body.asp"-->
<%

dim strDados(2)

strTipo	=request("tipo")
if len(trim(strTipo))=0 then strTipo="002"
strSql="select campoCod from fat_TipoEntidade where tipo='" & strTipo & "'"
set adoR=locConnVenda.execute(strSql)
if not adoR.eof then novoCampo=adoR("campoCod")
set adoR=nothing

strEnt	=request("ent")
strGrupo	=request("g")
if len(trim(strGrupo))=0 then strGrupo="001"

strCampo	=request("campo")
strCampoD	=request("campoD")

strProd	=request("produto")
strMes	=request("mes")
strAno	=request("ano")
media	=request("media")

strMostrarV=request("MostrarV")

strAgencia	=request("agencia")
strComercio	=request("comercio")

caCom	=request("caCom")

p=strProd

if strProd > "020000" then
	strProd=mid(strProd,1,2) & "%"
else
	strProd=mid(strProd,1,3) & "%"
end if

if len(trim(strCampo))>0 then
	strDesc=strCampoD
elseif len(trim(strEnt))>0 then
	if strTipo="000" then '' depto
		strSql="select depto descricao from view_depto where cod_depto='" & strEnt & "'"
	else
		strSql="select descricao from fat_entidade where entidade='" & strEnt & "'"
	end if
	set adoA=locConnVenda.execute(strSql)
	if not adoA.eof then
		strDesc=trim(adoA("descricao"))
	end if
	adoA.close	
else
	strDesc="TODOS"
end if

Set oChart = CreateObject("OWC.Chart")
Set c = oChart.Constants

oChart.Border.Color = c.chColorNone
dim vals(31)
dim Categories(31)

oChart.Charts.Add
oChart.Charts(0).Type = oChart.Constants.chChartTypeColumnClustered
oChart.Charts(0).PlotArea.Interior.color = "white"

'ochart.Constants

for i = 1 to 31
	categories(i) = i
next 
'' CA
oChart.Charts(0).SeriesCollection.Add 
oChart.Charts(0).SeriesCollection(0).Caption		= "VENDAS"
oChart.Charts(0).SeriesCollection(0).Interior.Color	= "white"
if strMostrarV="1" then
	ochart.Charts(0).SeriesCollection(0).DataLabelsCollection.Add
	ochart.Charts(0).SeriesCollection(0).DataLabelsCollection(0).HasValue = true	
end if

'' M�dia
oChart.Charts(0).SeriesCollection.Add 
oChart.Charts(0).SeriesCollection(1).Caption		= "M�DIA"
oChart.Charts(0).SeriesCollection(1).Interior.Color	= "yellow"
if strMostrarV="1" then
	ochart.Charts(0).SeriesCollection(1).DataLabelsCollection.Add
	ochart.Charts(0).SeriesCollection(1).DataLabelsCollection(1).HasValue = true	
end if
oChart.Charts(0).SeriesCollection(0).Interior.Color	= "blue"

'' OBJETIVO
oChart.Charts(0).SeriesCollection.Add 
oChart.Charts(0).SeriesCollection(2).Caption		= "OBJETIVO"
oChart.Charts(0).SeriesCollection(2).Interior.Color	= "white"
if strMostrarV="1" then
	ochart.Charts(0).SeriesCollection(2).DataLabelsCollection.Add
	ochart.Charts(0).SeriesCollection(2).DataLabelsCollection(2).HasValue = true	
end if

oChart.Charts(0).SeriesCollection(2).Type = oChart.Constants.chChartTypeLine
oChart.Charts(0).SeriesCollection(2).Line.Color = "red"


oChart.Charts(0).SeriesCollection(0).SetData c.chDimCategories, c.chDataLiteral, Categories
oChart.Charts(0).SeriesCollection(1).SetData c.chDimCategories, c.chDataLiteral, Categories
oChart.Charts(0).SeriesCollection(2).SetData c.chDimCategories, c.chDataLiteral, Categories

dim dado(31)
dim dado2(31)
vendas = 0
strObj=0

if strTipo<>"000" then
	strSql=	"select isnull(sum(qtd),0) qtd " + _
			"from view_Obj a " & _
			"where tipoObj=1 and produto like '" & strProd & "' and month(referencia)=" & strMes & " and year(referencia)=" & strAno
			if len(trim(strCampo))>0 then
				strSql=strSql & " and " & strCampo & "='" & strCampoD & "'"
			elseif len(trim(strEnt))>0 then
				strSql=strSql & " and " & novoCampo & "='" & strEnt & "'"		
			end if
	
	'Response.Write strsql
	'Response.end		
	set adoA=locConnVenda.execute(strSql)
	if not adoA.eof then strObj=adoA("qtd")
	adoA.close		
end if

if strAgencia="1" then
	''' obj agencia
	strSql=	"select isnull(sum(qtd),0) qtd " + _
			"from fat_objetivo a  " & _
			"where tipoobj=1 and produto like '" & strProd & "' and month(referencia)=" & strMes & " and year(referencia)=" & strAno & _
			" and destinatario in('017','018','145','146') "
	if len(trim(strCampo))>0 then
		strSql=strSql & " and " & strCampo & "='" & strCampoD & "'"
	elseif len(trim(strEnt))>0 then
		strSql=strSql & " and destinatario='" & strEnt & "'"		
	end if	
	'Response.Write strsql			
	set adoA=locConnVenda.execute(strSql)
	if not adoA.eof then strObj=strObj+cdbl(adoA("qtd"))	
end if
'-----------------------------------------------


i=0
strSql="select atualizacao_historico from sys_atividade where atividade=1"
set adoA=locConnVenda.execute(strSql)
if not adoA.eof then novaData=adoA("atualizacao_historico")
set adoA=nothing

data="1/" & strMes & "/" & strAno'strAno & "-" & strMes & "-1"
'Response.Write dt_Fim(data) & "-" & dt_Fim(novadata)
if dt_fim(data)>dt_Fim(novaData) then
	strSql=	"select day(Referencia) dia,qtd=sum(quantidade) " + _
			"from view_VendasMesAtual " + _
			"where cod_grupo in(" & strGrupo & ") and cod_depto<'1000' " + _
			" and produto like '" & strprod & "' " + _
			"  and atividade=" & session("atividade")	& _
			" and month(Referencia)=" & strMes & " and year(referencia)=" & strAno
	if len(trim(strCampo))>0 then
		strSql=strSql & " and " & strCampo & "='" & strCampoD & "'"
	elseif len(trim(strEnt))>0 then
		strSql=strSql & " and " & novoCampo & "='" & strEnt & "'"		
	end if
	strSql=strSql & " group by day(Referencia) order by day(Referencia)"
				'Response.Write strsql
				'Response.End 
	set adoR=locConnVenda.Execute(strSql)	

	''saida de fabrica / COM
	if strComercio="1" then
		strSql=	"select isnull(sum(quantidade),0) qtd,day(referencia) dia " + _
				"from view_VendasFabrica " + _
				"where produto like '" & strProd & "' and month(referencia)=" & strMes & " and year(referencia)=" & strAno & " and day(referencia)<=" & media & _
				"  and cod_grupo='002' and cod_depto>'8000' " + _
				"  and cod_grupo in(" & strGrupo & ") "
	if len(trim(strCampo))>0 then
		strSql=strSql & " and " & strCampo & "='" & strCampoD & "'"
	elseif len(trim(strEnt))>0 then
		strSql=strSql & " and " & novoCampo & "='" & strEnt & "'"		
	end if	
		strSql=strSql & "  and atividade=" & session("atividade") & _
						" group by day(referencia)"
				'Response.Write strsql
				'Response.End 
		set adoSaidaF=locConnVenda.execute(strSql)		
	end if
		
	''agencia
	if strAgencia="1" then			
		if strEnt="017" then strEnt2="145"
		if strEnt="145" then strEnt2="017"
		if strEnt="018" then strEnt2="146"
		if strEnt="146" then strEnt2="018"
		
		strSql=	"select isnull(sum(quantidade),0) qtd,day(referencia) dia " + _
				"from view_VendasFabrica " + _
				"where produto like '" & strProd & "' and month(referencia)=" & strMes & " and year(referencia)=" & strAno + _
				" and day(referencia)<=" & media & " and cod_grupo='003' and (cacom ='" & cacom & "' or " & len(cacom) & "=0) "
	if len(trim(strCampo))>0 then
		strSql=strSql & " and " & strCampo & "='" & strCampoD & "'"
	elseif len(trim(strEnt))>0 then
		strSql=strSql & " and (" & novoCampo & "='" & strEnt2 & "' or " & novoCampo & "='" & strEnt & "')"		
	end if				
		strSql=strSql & " and atividade=" & session("atividade") & _
						" group by day(referencia)"
				'Response.Write strsql
		set adoSaidaAG=locConnVenda.execute(strSql)	
	end if
	
else
	strSql=	"select day(Referencia) dia,qtd=sum(quantidade) " + _
			"from fat_Saida" & strAno & " a inner join view_Depto b on a.emitente=b.destinatario " & _
			"inner join fat_operacao c on a.operacao=c.operacao " + _
			"where cod_grupo in(" & strGrupo & ") and produto like '" & strprod & "' " + _
			" and month(Referencia)=" & strMes & " and year(referencia)=" & strAno + _
			" and day(Referencia)<=" & media & " and situacao=0 and c.venda=1 "
	if len(trim(strCampo))>0 then
		strSql=strSql & " and " & strCampo & "='" & strCampoD & "'"
	elseif len(trim(strEnt))>0 then
		strSql=strSql & " and " & novoCampo & "='" & strEnt & "'"
	end if
	strSql=strSql & " group by day(Referencia) order by day(Referencia)"	
	'Response.Write strsql
	'Response.end
	set adoR=conn6.Execute(strSql)	
	
		''saida de fabrica / COM
	if strComercio="1" then
		strSql=	"select day(Referencia) dia,qtd=sum(quantidade) " + _
			"from fat_Saida" & strAno & " a inner join view_Depto b on a.destinatario=b.destinatario " & _
			"inner join view_depto c on a.emitente=c.destinatario " + _
			"inner join fat_operacao d on a.operacao=d.operacao " + _
			"where c.cod_grupo='005' and b.cod_grupo='002' and b.cod_grupo in(" & strGrupo & ") and produto like '" & strprod & "' " + _
			" and month(Referencia)=" & strMes & " and year(referencia)=" & strAno + _
			" and day(referencia)<=" & media & " and situacao=0 and d.venda=1 "
		if len(trim(strCampo))>0 then
			strSql=strSql & " and b." & strCampo & "='" & strCampoD & "'"
		elseif len(trim(strEnt))>0 then
			strSql=strSql & " and b." & novoCampo & "='" & strEnt & "'"
		end if
		strSql=strSql & " group by day(Referencia) order by day(Referencia)"	
		set adoSaidaF=conn6.Execute(strSql)	
		'Response.Write strsql
		'Response.End 
	end if
	
	if strAgencia="1" then
		if strEnt="017" then strEnt2="145"
		if strEnt="145" then strEnt2="017"
		if strEnt="018" then strEnt2="146"
		if strEnt="146" then strEnt2="018"
		
		strSql=	"select day(Referencia) dia,qtd=sum(quantidade) " + _
			"from fat_Saida" & strAno & " a inner join view_Depto b on a.destinatario=b.destinatario " & _
			"inner join view_depto c on a.emitente=c.destinatario " + _
			"inner join fat_operacao d on a.operacao=d.operacao " + _
			"where c.cod_grupo='005' and b.cod_grupo='003' and b.cod_grupo in(" & strGrupo & ") and produto like '" & strprod & "' " + _
			" and month(Referencia)=" & strMes & " and year(referencia)=" & strAno + _
			" and day(referencia)<=" & media & " and situacao=0 and d.venda=1 and (a.caCom='" & caCom & "' or " & len(caCom) & "=0) "
		if len(trim(strCampo))>0 then
			strSql=strSql & " and b." & strCampo & "='" & strCampoD & "'"
		elseif len(trim(strEnt))>0 then
			strSql=strSql & " and (b." & novoCampo & "='" & strEnt & "' or b." & novoCampo & "='" & strEnt2 & "')"
		end if
		strSql=strSql & " group by day(Referencia) order by day(Referencia)"	
		set adoSaidaAG=conn6.Execute(strSql)	
		'Response.Write strsql
		'Response.End 
	end if
	'		
'	''agencia
'	if strAgencia="1" then	
'		strSql=	"select isnull(sum(qtd),0) qtd,day(saida) dia " + _
'				"from view_VendaAgencia " + _
'				"where produto like '" & strProd & "' and month(saida)=" & strMes & " and year(saida)=" & strAno + _
'				" and cod_subgrupo in('017','018','145','146') "
'	if len(trim(strCampo))>0 then
'		strSql=strSql & " and " & strCampo & "='" & strCampoD & "'"
'	elseif len(trim(strEnt))>0 then
'		strSql=strSql & " and " & novoCampo & "='" & strEnt & "'"		
'	end if	
'			strSql=strSql & "  and atividade=" & session("atividade") & _
'				" group by day(Saida)"
'				'Response.Write strsql
'		set adoSaidaAG=locConnVenda.execute(strSql)	
'	end if
	
end if
'Response.Write strsql

i=0	
do while not adoR.eof
	i=i+1
	dia = adoR("dia")	
	if i=dia then
		dado(i) = cdbl(adoR("qtd"))
		adoR.moveNext
	else
		dado(i) = 0
	end if
loop	
adoR.close

if strComercio="1" then
''' somar venda de saida de fabrica
	dim saidaF(31)
	i=0
	do while not adoSaidaF.eof
		i=i+1
		dia=adoSaidaF("dia")
		if i=dia then
			saidaF(i)=cdbl(adoSaidaF("qtd"))
			adoSaidaF.movenext
		else
			saidaF(i)=0
		end if
	loop
	adoSaidaF.close

	for i=1 to 31
		dado(i)=dado(i) + cdbl(SaidaF(i))
	next
end if

if strAgencia="1" then
	''' somar venda de agencia
	dim saidaAG(31)
	i=0
	do while not adoSaidaAG.eof
		i=i+1
		dia=adoSaidaAG("dia")
		if i=dia then
			saidaAG(i)=cdbl(adoSaidaAG("qtd"))
			adoSaidaAG.movenext
		else
			saidaAG(i)=0
		end if
	loop
	adoSaidaAG.close

	for i=1 to 31
		dado(i)=dado(i) + cdbl(SaidaAG(i))
	next
end if
	

'acumulado
for i=1 to 31
	if i = 1 then 
		dado2(i)=dado(i)
	else
		dado2(i) = dado2(i-1) + dado(i)
	end if
next

''' venda
for l = 1 to 31
	vals(l) = dado(l)
	'Response.Write vals(l) & "<br>"
	oChart.Charts(0).SeriesCollection(0).SetData c.chDimValues, c.chDataLiteral, Vals
next

''' m�dia
for l = 1 to 31
	vals(l) = dado2(l)/l
	oChart.Charts(0).SeriesCollection(1).SetData c.chDimValues, c.chDataLiteral, Vals
next

''' obj
for l = 1 to 31
	vals(l) = strObj
	oChart.Charts(0).SeriesCollection(2).SetData c.chDimValues, c.chDataLiteral, Vals
next

if len(trim(session("n")))= 0 then session("n")=0

session("n")=int(session("n"))+1

oChart.Charts(0).HasLegend = true
oChart.Charts(0).HasTitle = True
oChart.Charts(0).Title.Caption= "VENDA DI�RIA " & strDesc & " - " & mes(strMes) & "/" & strAno

sFname = "arquivo/" & trim(session("usuario")) & "_VD_" & session("n") & ".gif"
oChart.ExportPicture server.MapPath(sFname), "gif", 750, 420
'oChart.ExportPicture server.MapPath(sFname),,700,380
Session("sTempFile" & Session("n")) = Server.MapPath(sFname)

Response.Cookies("GRAF_VD")=session("n")
Response.Cookies("GRAF_VD").Expires=now+60
'=====
%>
		<table border="0" width="100%">
		<%call form_Criar("frm1","graf_VendaDiaria.asp")
			call txtOculto_Criar("produto",p)
			call txtOculto_Criar("ent",strEnt)
			call txtOculto_Criar("g",strGrupo)
			call txtOculto_Criar("campo",strCampo)
			call txtOculto_Criar("campoD",strCampoD)
			call txtOculto_Criar("tipo",strTipo)
			call txtOculto_Criar("agencia",strAgencia)
			call txtOculto_Criar("comercio",strComercio)						
			call txtOculto_Criar("media",media)	
			%>				
			<tr>
				<td width="20%" align="right"><font face="verdana" size="1">Per�odo:</font></td>
				<td width="80%">
					<%call Select_Mes("mes",strmes)%>-
					<%call txt_Criar("texto","ano",4,4,strAno)
					  call button_Criar("submit","btn1","Atualizar","")%>										
					</td>
			</tr>
				<td width="20%" align="right"><font face="verdana" size="1">M�dia:</font></td>
				<td width="80%"><font face="verdana" size="1"><%=media%></font></td>					
			<tr>
			<%call form_Fim()%>
		</table>
    </td>
  </tr>  
   <tr>
    <td width="100%" align="center"><img src="<%=sFname%>"></center></td></tr>

<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->