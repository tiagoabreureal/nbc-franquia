﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="../mw-conexao-db.asp"-->
<!--#include file="../json/JSON_2.0.4.asp"-->
<!--#include file="../json/JSON_UTIL_0.1.1.asp"-->

<%

vacao = LimpaLixo(ExpurgaApostrofe(Request("acao")))
vEmail = Request("Email")
vgrp = Request("grp")
Select Case vacao

Case "lista_reconhecimento"

	Vretorno = Fc_lista_reconhecimento(conexao_mw,vgrp)
	Response.Write(Vretorno)

Case "envia_contato"

	Vretorno = Fc_envia_contato_portal(conexao_mw,vEmail)
	Response.Write(Vretorno)
	
Case "obter_cdh"

Vretorno = Fc_obter_cdh(conexao_mw)
response.Write(Vretorno)

Case "obter_seminario_eventos"

Vretorno = Fc_obter_seminario_eventos(conexao_mw)
response.Write(Vretorno)	

Case Else
	Response.Write("0")		
End Select


Function Fc_obter_seminario_eventos(conexao)

strsql = "SELECT CdSeminario,SeminarioxCdPalestrante,SeminarioxCdTiposeminario,Seminario_bairro, "&_
"Seminario_cidade,Seminario_uf,Seminario_data,Seminario_programacao,Seminario_local,Seminario_endereco, "&_
"Seminario_cep,Seminario_url,Seminario_info,Seminario_coordenadas,Seminario_status,Pale_palestrante,"&_
"Tiposeminario_nome,Tiposeminario_destaque,Pale_foto,Pale_qualificacao,Seminario_convidado,Seminario_convidado_quali "&_
" FROM [hinode].[dbo].[tbl_mw_seminario] "&_
" INNER JOIN [hinode].[dbo].[tbl_mw_seminarioxpale] ON CdPalestrante = SeminarioxCdPalestrante "&_
" INNER JOIN [hinode].[dbo].[tbl_mw_seminarioxtipo] ON CdTiposeminario = SeminarioxCdTiposeminario "&_ 
" where Seminario_status=1 and Seminario_data >= DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0) "&_
" order by Seminario_data asc "
	
QueryToJSON(conexao, strsql).Flush				  

conexao.Close
set conexao=nothing

	
Exit Function				  
End Function

Function Fc_obter_cdh(conexao)

strsql = " select b.destinatario,replace(a.descricao,'CDH','Franquia') AS descricao,c.estado,c.email "&_ 
    " from [hinode].[dbo].[sys_atividade] a "&_ 
	" inner join [hinode].[dbo].[fat_filial] b on a.destinatario_matriz=b.destinatario " & _
	" inner join [hinode].[dbo].[fat_Destinatario] c on b.destinatario=c.destinatario " & _
	" where b.cd in(1,2) and b.destinatario not in(10085876,10094265,10123644) and c.ativosn=1 order by c.cidade,c.estado asc "

QueryToJSON(conexao, strsql).Flush				  

conexao.Close
set conexao=nothing

	
Exit Function				  
End Function


function Fc_lista_reconhecimento(conexao,vgrp)

vSQLREC = " SELECT ar.Diretorio,ar.Tipo,ar.Status,ar.URL,ar.Exibir,ar.DescricaoArquivoObs,ar.NomeArquivo, "&_
" ar.Grupo,ar.Ciclo,ar.Cidade,ar.Estado  "&_
" from [Hinode].[dbo].[tbl_mw_arquivo] ar  "&_
" INNER JOIN [Hinode].[dbo].[tbl_mw_arquivo_portal] arp ON arp.CdArxpo_cdarq = ar.CdArquivo" 

if vgrp <> "" then
vSQLREC = vSQLREC & " where ar.Tipo = 6 and ar.Status = 1 AND arp.CdArxpo_cdpor =1 and ar.Grupo='"&vgrp&"'" 
else
vSQLREC = vSQLREC & " where ar.Tipo = 6 and ar.Status = 1 AND arp.CdArxpo_cdpor =1 and ar.Grupo "&_ 
" IN('IMPERIAL_DIAMANTE','TRIPLO_DIAMANTE','DUPLO_DIAMANTE','DIAMANTE') "
end if

vSQLREC = vSQLREC & " ORDER BY ar.IdGrupo DESC, ar.NomeArquivo ASC"

QueryToJSON(conexao, vSQLREC).Flush				  

conexao.Close
set conexao=nothing
	
Exit Function
end function


function Fc_envia_contato_portal(conexao,vEmail)

'XXXXXXXXXXXXXXXXX INICIO  - NOTIFICAÇÃO POR E-MAIL - PATROCINADOR XXXXXXXXXXXXXXXXXXXX
	  
if vEmail <> "" then
	
vMsg = "<table bgcolor='#FFFFFF' border='0' style='padding:20px;'>"& _
	"<tbody>"& _
	"<tr>"& _
	"<td width='550'>"& _
	"<table align='center' border='0' cellpadding='0' cellspacing='0'>"& _
	"<tbody>"& _
	"<tr>"& _
	"<td style='padding-bottom:5px;' width='550'>"& _
	"<table style='background-color:#f1e3c8; border:1px solid #e1caa8; width:100%; padding:10px 15px;'>"& _
	"<tbody>"& _
	"<tr>"& _
	"<td width='180'>"& _
"<img alt='Hinode.com.br' src='http://vo.hinode.com.br/images/logo-hinode-cosmeticos-email.png' /></td>"& _
"<td align='center' style='font-family:arial; font-size:16px; font-weight:bold; color:#000000;'>"& _
"CONTATO REALIZADO VIA PORTAL HINODE</td>"& _
"</tr>"& _
"</tbody>"& _
"</table>"& _
"</td>"& _
"</tr>"& _
"<tr>"& _
"<td valign='top' style='padding:15px; border:1px solid #e1caa8;'>"& _
"<table align='center' border='0' cellpadding='0' cellspacing='0'>"& _
"<tbody>"& _
"<tr>"& _
"<td style='font:13px Arial;' width='520'>"& _
"<p>"& _
"<strong>Nome:</strong>" & REQUEST.Form("Nome")& "<br>"&_
"<strong>E-mail:</strong>" & vEmail & "<br>"&_
"<strong>Telefone:</strong>" &  REQUEST.Form("Telefone")& "<br>"&_
"<strong>Celular:</strong>" & REQUEST.Form("Celular") & "<br>"&_
"<strong>Assunto:</strong>" & REQUEST.Form("Assunto") & "<br>"&_
"<strong>Mensagem:</strong>" & REQUEST.Form("Mensagem") & "</p><br>"&_

"</td>"& _
"</tr>"& _
"</tbody>"& _
"</table>"& _
"</td>"& _
"</tr>"& _
"<tr>"& _
"<td height='90' style='font:12px Arial; background:#f1e3c8; color:#000000; padding:10px 15px'>"& _
"<table>"& _
"<tbody>"& _
"<tr>"& _
"<td style='padding-left:10px'>"& _
"<p style='font:12px arial; color:#000000'>"& _
"Atenciosamente,<br />"& _
"<span style='font:bold 20px arial;'>HINODE COSMÉTICOS</span><br />"& _
"<a href='http://www.hinode.com.br' style='color:#000000' target='_blank' "& _ 
"title='www.hinode.com.br'>www.hinode.com.br</a></p>"& _
"</td>"& _
"</tr>"& _
"</tbody>"& _
"</table>"& _
"</td>"& _
"</tr>"& _
"</tbody>"& _
"</table>"& _
"</td>"& _
"</tr>"& _
"</tbody>"& _
"</table>"
	 
		servidor="smtp.hinode.com.br"

		'de  ="naoresponda@hinode.com.br"
		de  ="naoresponda@hinode.com.br" 'Adicionado por Robson em 21/11/2013		

		user="hinode@hinode.com.br"

		pass="druida@107"

		para = "hinode@hinode.com.br"

		assunto	= "CONTATO VIA PORTAL HINODE - "& REQUEST.Form("Assunto")

Set Mail = Server.CreateObject("CDO.Message") 

               Set MailCon = Server.CreateObject ("CDO.Configuration") 

MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = servidor
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 587 
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 

               if len(user)>0 then

MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = user
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = pass

               end if

               MailCon.Fields.update
			   
			   Set Mail.Configuration = MailCon 
				   Mail.From = de
				  'Mail.CC="suporte@xcompany.com.br"
				   Mail.To = para
				   Mail.ReplyTo = vEmail
	               Mail.Subject = assunto
				   'id_Body	="Esse é um teste de email."
                   Mail.HTMLBody=vMsg
	               Mail.Fields.update
	               
				   On Error Resume Next
				   Mail.Send
				   
				   If Err.Number = 0 then
				   
				   
				   vNome = LimpaLixo(ExpurgaApostrofe(REQUEST.Form("Nome")))
				   vEmail = LimpaLixo(ExpurgaApostrofe(vEmail))
				   vTelefone =  LimpaLixo(ExpurgaApostrofe(REQUEST.Form("Telefone")))
				   vCelular =  LimpaLixo(ExpurgaApostrofe(REQUEST.Form("Celular")))
				   vAssunto = LimpaLixo(ExpurgaApostrofe(REQUEST.Form("Assunto"))) 
				   vMensagem = LimpaLixo(ExpurgaApostrofe(REQUEST.Form("Mensagem")))
				   
				   vSQL = "INSERT INTO [hinode].[dbo].[tbl_mw_contato_portal] " &_
							 "(Contato_portal_nome,Contato_portal_email,Contato_portal_tel,Contato_portal_cel,"&_
							  "Contato_portal_Assunto,Contato_portal_Msg,Contato_portal_ip,Contato_portal_Data) "&_ 
						   " VALUES ('" & vNome & "','" & vEmail & "','" & vTelefone &"'," &_
				           "'" & vCelular & "','" & vAssunto & "','"&vMensagem&"'," &_
				           "'"& Request.ServerVariables("REMOTE_ADDR")&"',GetDate());" 
					conexao.Execute(vSQL)
					conexao.Close
					Set conexao = Nothing
				   Response.Write "1|CONTATO ENVIADO COM SUCESSO!"
				   else
				   Response.Write "0|ERRO: " & Err.number & "-" & err.Description 
				   end if
				   
	           Set Mail = Nothing 
	           Set MailCon = Nothing 
	   
	   else
	   Response.Write "0|ERRO: AO TENTAR ENVIAR O CONTATO!"
			   
	end if		   
	
'XXXXXXXXXXXXXXXXXXX FIM - ENVIA NOTIFICAÇÃO POR E-MAIL - CONSULTOR XXXXXXXXXXXXXXXXXXXX

Exit Function
end function


Function InverteData(data,inverte)
 If isDate(data) then
	 vData = data
	else
	 vData = now()	
	End If
	temp = split(vData,"/")
	
	 If len(temp(0)) <> 2 Then
		 temp(0) = "0" & temp(0)
		End If
		
		If len(temp(1)) <> 2 Then
		 temp(1) = "0" & temp(1)
		End If
	
	If inverte = true Then			 	 	 
 	InverteData = temp(1) & "/" & temp(0) & "/" & temp(2)
	Else 
	 InverteData	= temp(0) & "/" & temp(1) & "/" & temp(2)
	End If	
End Function

Function FormataData(Data)
  If Data <> "" Then
    FormataData = Right("0" & DatePart("d", Data),2) & "/" & Right("0" & DatePart("m", Data),2) & "/" & DatePart("yyyy", Data)
  End If
End Function



'- Substituindo o apóstrofe(') pelo duplo apóstrofe ('')
Function ExpurgaApostrofe(texto)
    ExpurgaApostrofe = replace( texto , "'" , "''")
End function


'- Substituindo os caracteres e palavras maliciosas por vazio("").
Function LimpaLixo(input)
    dim lixo
    dim textoOK

    lixo = array ("SELECT","DROP","UPDATE",";","--","INSERT","DELETE","xp_","<","SCRIPT",">")

    textoOK = input

     for i = 0 to UBound(lixo)
          textoOK = replace( textoOK ,  lixo(i) , "")
     next

     LimpaLixo = textoOK

end Function


'- Rejeitando os dados maliciosos:
Function ValidaDados(input)
      lixo = array ( "SELECT" , "INSERT" , "UPDATE" , "DELETE" , "DROP" , "--" , "'")

      ValidaDados = true

      for i = LBound(lixo) to UBound(lixo)
            if ( instr(1 ,  input , lixo(i) , vbtextcompare ) <> 0 ) then
                  ValidaDados = False
                  exit function '}
            end if
      next
end function


%>