﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="../mw-conexao-db.asp"-->
<%

vacao = LimpaLixo(ExpurgaApostrofe(Request("acao")))


Select Case vacao
	Case "montar_rede_unilevel"	
		
		ciclo = LimpaLixo(ExpurgaApostrofe(Request("ciclo")))
		hierarquiaPai = LimpaLixo(ExpurgaApostrofe(Request("hierarquiaPai")))
		qualificacao = LimpaLixo(ExpurgaApostrofe(Request("qualificacao")))
		'nivel_arvore = LimpaLixo(ExpurgaApostrofe(Request("nivel_arvore")))
		
		
		strsql="select inicio from rede_parametros where ciclo=" & ciclo
		'Response.Write strsql
		set adors1=conexao_mw.execute(strsql)
		if adors1.eof then
			data = ""
		else
			data=adors1("inicio")
			data = InverteData(data,true)
		end if
		set adors1=nothing
		
		if data <> "" then
			strsql="exec rede_master '" & data & "'," & hierarquiaPai & "," & qualificacao & "," & hierarquiaPai & ", 1"
			'response.Write(strsql)
			montarRede = false
			set adors1=conexao_mw.execute(strsql)
			if not adors1.eof then
				hie			=adors1("hierarquia")
				integracao	=adors1("integracao")
				nome		=adors1("nome") & " " 	
				nivel		=adors1("profundidade_master")
				cor			=adors1("cor")
				estado		=adors1("estado")
				vg			=adors1("vol_grupo")
				
				thumbArray = retornaThumb(cor)
				color = thumbArray(0)
				imagem = thumbArray(1)
				titulo = thumbArray(2)
				montarRede = true
			end if	
			set adors1=nothing	
			
			if montarRede = true then
			%>
			<ul style='margin:0px; width:32000px;'>
				<li>
					<img style='position: relative;' src='imagens/bonequinhos/<%=imagem%>'>
					<p style="font-size:12px;">
						<span style="font-size:10px"><%=nome%></span>
						<br><%=titulo%>
					</p>
					<ul style="margin:0px;" id="container_arvore">  
						<%
						MontarRedeUnilevel data,hierarquiaPai,qualificacao,4
						%>
					</ul>
				 </li>
			</ul>
			<%
			else
			
			Response.Write(0)
			
			end if	
		end if
End Select




'Monta Rede Unilevel
Function MontarRedeUnilevel(data,hierarquiaPai,qualificacao,nivel_arvore)

	if nivel_arvore >= 1 then 'Enquanto o nivel for maior ou igual a 1
		
	nivel_arvore_sub = nivel_arvore - 1  
	
	strsql="exec rede_master '" & data & "'," & hierarquiaPai & "," & qualificacao & "," & hierarquiaPai & ", 2"
	
	'response.Write(strsql)
	
	set adors2=conexao_mw.execute(strsql)
	
		if not adors2.eof then
			do while not adors2.eof
				hieFilho			=adors2("hierarquia")
				integracao	=adors2("integracao")
				nome		=adors2("nome") & " " 	
				nivel		=adors2("profundidade_master")
				cor			= adors2("cor")
				estado		=adors2("estado")
				vg			=adors2("vol_grupo")
				
				thumbArray = retornaThumb(cor)

				color = thumbArray(0)
				imagem = thumbArray(1)
				titulo = thumbArray(2)
				
				
				p1	=instr(1,nome," ")
				p2	=instr(p1+1,nome," ")			
				nome2=mid(nome,1,p1-1)	
				if p2>0 then nome2=mid(nome,1,p2-1)	
			
				nome2	=nome2 & " - " & estado & " (" & integracao & ")"
				if cdbl(vg)>0 then nome2=nome2 & " - VG: " & formatnumber(vg,2)
				
				
				if nivel <> 1 then
				
%>

        <li>
            <a href="#" title="<%=nome2%>" class="montarArvoreConsultor tooltipEvent" data-placement="right" hierarquia="<%=hieFilho%>">
            	<img style='position: relative; z-index: 10; left: 0px;' src='imagens/bonequinhos/<%=imagem%>'>
            </a>
            <p style="font-size:12px; width:auto;">
            	<span style="font-size:10px"><%=titulo%><br>
            </p>
            <ul style='margin:0px;'>  
            	<%                                        	
                  MontarRedeUnilevel data,hieFilho,qualificacao,nivel_arvore_sub  
				 %>                                                                           
            </ul>                               
        </li>
<%
				
				end if
				
				adors2.movenext
			loop	
		
			set adors2=nothing
		
		end if 'not adors2.eof 
		
	end if 'nivel >= 1 then
End Function











Function retornaThumb(color_v)
	
	color_value = color_v
	
	colorArray = Array(0,0,0)
	colorArray(0) = ""
	colorArray(1) = ""
	colorArray(2) = ""
	
	color = false
	if  color_value = "#99ccff" then 
		colorArray(0) = "#ddeeff"
		colorArray(1) = "consultor.png"
		colorArray(2) = "Consultor"
		color = true
		
	elseif color_value = "#ccffcc" then
		colorArray(0) = "#e4fbe4"
		colorArray(1) = "consultor.png"
		colorArray(2) = "Consultor"
		color = true
		
	elseif color_value = "#cc6699" then
		colorArray(0) = "#fddbec" 
		colorArray(1) = "consultor.png"
		colorArray(2) = "Consultor"
		color = true
		
	elseif color_value = "#00cc66" then
		colorArray(0) = "#68A31C"
		colorArray(1) = "master.png"
		colorArray(2) = "Master"
		color = true
		
	elseif color_value = "#ff9933" then
		colorArray(0) = "#CD621E"
		colorArray(1) = "bronze.png"
		colorArray(2) = "Bronze"
		color = true
		
	elseif color_value = "#999999" then
		colorArray(0) = "#A6A6A6"
		colorArray(1) = "prata.png"
		colorArray(2) = "Prata"
		color = true
		
	elseif color_value ="#ffff66" then
		colorArray(0) = "#E7E03A" 
		colorArray(1) = "ouro.png"
		colorArray(2) = "Ouro"
		color = true
		
	elseif color_value ="#e1e1e1" then
		colorArray(0) = "#C9C9C9"
		colorArray(1) = "platina.png"
		colorArray(2) = "Platina"
		color = true
		
	elseif color_value ="#3366FF" then
		colorArray(0) = "#258BD4"
		colorArray(1) = "diamante.png"
		colorArray(2) = "Diamante"
		color = true
		
	elseif color_value ="#996633" then
		colorArray(0) = "#258BD4"
		colorArray(1) = "duplo_diamante.png"
		colorArray(2) = "Duplo Diamante"
		color = true
		
	elseif color_value ="#cccc00" then
		colorArray(0) = "#258BD4"
		colorArray(1) = "triplo_diamante.png"
		colorArray(2) = "Triplo Diamante"
		color = true
		
	end if
			
	if color = false then
		colorArray(0) = color_value
		colorArray(1) = "consultor.png"
		colorArray(2) = "Consultor"
	end if	
	
	retornaThumb = colorArray

Exit Function
End Function








Function InverteData(data,inverte)
 If isDate(data) then
	 vData = data
	else
	 vData = now()	
	End If
	temp = split(vData,"/")
	
	 If len(temp(0)) <> 2 Then
		 temp(0) = "0" & temp(0)
		End If
		
		If len(temp(1)) <> 2 Then
		 temp(1) = "0" & temp(1)
		End If
	
	If inverte = true Then			 	 	 
 	InverteData = temp(1) & "/" & temp(0) & "/" & temp(2)
	Else 
	 InverteData	= temp(0) & "/" & temp(1) & "/" & temp(2)
	End If	
End Function

Function FormataData(Data)
  If Data <> "" Then
    FormataData = Right("0" & DatePart("d", Data),2) & "/" & Right("0" & DatePart("m", Data),2) & "/" & DatePart("yyyy", Data)
  End If
End Function



'- Substituindo o apóstrofe(') pelo duplo apóstrofe ('')
Function ExpurgaApostrofe(texto)
    ExpurgaApostrofe = replace( Trim(texto) , "'" , "''")
End function


'- Substituindo os caracteres e palavras maliciosas por vazio("").
Function LimpaLixo(input)
    dim lixo
    dim textoOK

    lixo = array ("SELECT","DROP","UPDATE",";","--","INSERT","DELETE","xp_","<","SCRIPT",">")

    textoOK = Trim(input)

     for i = 0 to UBound(lixo)
          textoOK = replace( textoOK ,  lixo(i) , "")
     next

     LimpaLixo = textoOK

end Function


'- Rejeitando os dados maliciosos:
Function ValidaDados(input)
      lixo = array ( "SELECT" , "INSERT" , "UPDATE" , "DELETE" , "DROP" , "--" , "'")

      ValidaDados = true

      for i = LBound(lixo) to UBound(lixo)
            if ( instr(1 ,  input , lixo(i) , vbtextcompare ) <> 0 ) then
                  ValidaDados = False
                  exit function '}
            end if
      next
end function

%>