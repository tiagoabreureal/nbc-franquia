﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="../mw-conexao-db.asp"-->
<!--#include file="../json/JSON_2.0.4.asp"-->
<!--#include file="../json/JSON_UTIL_0.1.1.asp"-->

<%

vacao = Request("acao")
vid_pedido_oficial = Request.Form("id_pedido_oficial")
vidpatro = Request.Form("id_patro")
vidconsultor = Request.Form("idconsultor")
vdestinatario = Request("destinatario")
vss_pg = Request.Form("ss_pg")
vcpf = Request.Form("cpf")
vcnpj = Request.Form("cnpj")
vid_cad = Request.Form("id_cad")
vloc_cons_por = Request.Form("loc_cons_por")
vloc_prod = Request.Form("loc_prod")
vqtd = Request.Form("qtd_prod")
vid_car = Request.Form("id_car")
vpeso_ped = Request.Form("peso_ped")
vcep_entr = Request.Form("cep_entr")
vid_cdhret = Request.Form("id_cdhret")
vmodo_entrega = Request.Form("modo_entrega")

vtel = LimpaLixo(ExpurgaApostrofe(Request.Form("tel")))

vnome_cons = LimpaLixo(ExpurgaApostrofe(Request.Form("nome_cons"))) 
'vnome_cons = Request.Form("nome_cons") 
vid_cdhret_desc = Request.Form("id_cdhret_desc")
 
vendereco = LimpaLixo(ExpurgaApostrofe(Request.Form("endereco"))) 
vnumero = LimpaLixo(ExpurgaApostrofe(Request.Form("numero")))
vcompl = LimpaLixo(ExpurgaApostrofe(Request.Form("compl"))) 
vbairro = LimpaLixo(ExpurgaApostrofe(Request.Form("bairro"))) 
vcidade = LimpaLixo(ExpurgaApostrofe(Request.Form("cidade"))) 
vestado = LimpaLixo(ExpurgaApostrofe(Request.Form("estado"))) 

vid_forma_pag = Request.Form("id_forma_pag") 
vdesc_forma_pag = Request.Form("desc_forma_pag") 
vvl_frete = Request.Form("vl_frete") 
vvl_ped = Request.Form("vl_ped") 
vvl_sub_ped = Request.Form("vl_sub_ped") 
vpontos_ped = Request.Form("pontos_ped")
vl_credito_usado_ped = Request.Form("vl_credito_usado")
vPed_Juros = Request.Form("ped_juros")
vped_tipo = Request.Form("ped_tipo")
vregra_estoque = Request.Form("regra_estoque")
vstatus_ped = Request.Form("status_ped")
vemail_ped = Request.Form("email_ped")
vemail_cdh = Request.Form("email_cdh")
vcd_aprovacao_card = Request.Form("cd_aprovacao_card")
vqtd_parcela_card = Request.Form("qtd_parcela_card")
vqtd_parc_auto = Request.Form("qtd_parc_auto")
vprazo_transp_ped = Request.Form("prazo_transp_ped")
vid_transp = Request.Form("forma_envio_transp_ped")
vtransp = Request.Form("forma_envio_transp_desc_ped")
vidcat = Request.Form("idcat")
vatv_cons = Request.Form("atv_cons")
vatv_cad_cons = Request.Form("atv_cad_cons")
	
vciclo = Request.Form("ciclo")
	
'========================== HINODE 2.0 ===================================
valorDesconto = Request.Form("valor_desconto")
valorExcedente = Request.Form("valorExcedente")
codigoKit = Request.Form("codigoKit")
encontrou = false
i = 0
idlogcons = Request.Form("idlogcons")
'=========================================================================	


Select Case vacao

Case "atv_cons_ped"

if vid_pedido_oficial <> "" then
	Vretorno = Fc_Atv_Cons_ped(conexao_loja_mw,conexao_mw,vid_pedido_oficial)
	response.Write(Vretorno)
else
	response.Write("0|Erro: Ao Ativar O Consultor!")
end if 


Case "verifica_recibo"

if vdestinatario <> "" then
	Vretorno = Fc_Verifica_Recibo(conexao_mw,vdestinatario,vciclo)
	response.Write(Vretorno)
else
	response.Write("0|Erro: Consultor Não Encontrado!")
end if


Case "verifica_recibo_lideranca"

if vdestinatario <> "" then
	Vretorno = Fc_Verifica_Recibo_Lideranca(conexao_mw,vdestinatario,vciclo)
	response.Write(Vretorno)
else
	response.Write("0|Erro: Consultor Não Encontrado!")
end if 



Case "obter_parcelas"

if vvl_ped <> "" and vid_forma_pag<>"" then
	Vretorno = Fc_obter_parcelas(conexao_mw,vid_forma_pag,vvl_ped,vqtd_parc_auto)
	response.Write(Vretorno)
else
	response.Write("0|Erro: Ao Obter as Parcelas!!")
end if 


Case "lista_pedido"

if vid_pedido_oficial <> "" then
	Vretorno = Fc_lista_pedido(conexao_loja_mw,vid_pedido_oficial)
	response.Write(Vretorno)
else
	response.Write("0|Erro: Pedido não encontrado!")
end if 


Case "alt_pedido"

if vid_pedido_oficial <> "" then
	Vretorno = Fc_alt_pedido(conexao_loja_mw,vid_pedido_oficial,vid_forma_pag,vdesc_forma_pag,vPed_Juros,vqtd_parcela_card)
	response.Write(Vretorno)
else
	response.Write("0|Erro: Pedido não encontrado!")
end if 


Case "lista_pedido_item"

if vid_pedido_oficial <> "" then
	Vretorno = Fc_lista_pedido_item(conexao_loja_mw,vid_pedido_oficial)
	response.Write(Vretorno)
else
	response.Write("0|Erro: Item do Pedido não encontrado!")
end if 


Case "gera_pedido"

	if  vidconsultor <> "" and vss_pg <> "" and vid_forma_pag <> "" and vvl_ped <> "" then	
	    
		Vretorno = Fc_gera_pedido(conexao_loja_mw,vidconsultor,vss_pg,vmodo_entrega,vnome_cons,vid_cdhret,vid_cdhret_desc,vcep_entr,vendereco,vnumero,vcompl,vbairro,vcidade,vestado,vid_forma_pag,vdesc_forma_pag,vvl_frete,vvl_ped,vvl_sub_ped,vpontos_ped,vpeso_ped,vstatus_ped,vemail_ped,vcd_aprovacao_card,vqtd_parcela_card,vprazo_transp_ped,vid_transp,vtransp,vl_credito_usado_ped,vped_tipo,vemail_cdh,vPed_Juros,vqtd_parc_auto,valorDesconto,valorExcedente,codigoKit,vtel,idlogcons)
		
		response.Write(Vretorno)
	else
	    response.Write("0|Favor informe a forma de entrega!")
	end if
	


Case "calc_frete_list_transp"

	if  vss_pg <> "" and vid_cdhret<>"" then	
	    Vretorno = Fc_car_calc_frete_list_transp(conexao_loja_mw,vss_pg,vpeso_ped,vcep_entr,vid_cdhret)
		response.Write(Vretorno)
	else
	    response.Write("0|Favor informe a forma de entrega!")
	end if

Case "car_lista_hist"

	if  vidconsultor <> "" then	
		Vretorno = Fc_car_lista_hist(conexao_loja_mw,vidconsultor,vid_cdhret)
		response.Write(Vretorno)
	else
		response.Write("0|Não há histórico de Carrinho desse consultor!")
	end if
	

Case "credito_consultor"

	if  vidconsultor <> "" then	
		Vretorno = Fc_credito_consultor(conexao_loja_mw,vidconsultor)
		response.Write(Vretorno)
	else
		response.Write("0|Consultor não informado!")
	end if	


Case "car_lista_item"

	if  vss_pg <> "" and  vidconsultor <> "" then	
		'Vretorno = Fc_car_lista_item(conexao_loja_mw,vidconsultor,vss_pg,vid_cdhret)
		Vretorno = listaItems(conexao_loja_mw,vidconsultor,vss_pg,vatv_cad_cons,vid_cdhret,vatv_cons)
		response.Write(Vretorno)
	else
		response.Write("0|Carrinho está vazio!")
	end if
	
Case "lista_cat_subcat"
Vretorno = Fc_lista_cat_subcat(conexao_loja_mw,vidcat)	
response.Write(Vretorno)

Case "lista_prod_subcat"
Vretorno = Fc_lista_prod_subcat(conexao_loja_mw,vidcat)	
response.Write(Vretorno)	
	

Case "car_add_item"

	if  vloc_prod <> "" and  vidconsultor <> "" and vss_pg <> "" and vid_cdhret<>"" then	
		'Vretorno = Fc_car_add_item(conexao_loja_mw,vidconsultor,vloc_prod,vqtd,vss_pg,vid_cdhret,vregra_estoque,vatv_cons,vatv_cad_cons)
		Vretorno = addItem(conexao_loja_mw,vidconsultor,vloc_prod,vqtd,vss_pg,vid_cdhret,vregra_estoque,vatv_cons,vatv_cad_cons)
		response.Write(Vretorno)
	else
		response.Write("0|Favor informe o produto")
	end if
	

Case "car_del_item"

	if  vloc_prod <> "" and  vidconsultor <> "" and vss_pg <> "" and vatv_cad_cons <> "" then	
		'Vretorno = Fc_car_del_item(conexao_loja_mw,vidconsultor,vloc_prod,vss_pg,vid_car)
		Vretorno = deletaItem(conexao_loja_mw,vidconsultor,vloc_prod,vss_pg,vid_car,vatv_cad_cons,vqtd)
		response.Write(Vretorno)
	else
		response.Write("0|Favor informe o produto")
	end if	
	

Case "car_upd_item"

	if  vloc_prod <> "" and  vidconsultor <> "" and vss_pg <> "" and vid_car <> "" and vatv_cad_cons <> "" then	
		'Vretorno = Fc_car_upd_item(conexao_loja_mw,1,vidconsultor,vloc_prod,vqtd,vss_pg,vid_car)
		Vretorno = addItem(conexao_loja_mw,vidconsultor,vloc_prod,1,vss_pg,vid_cdhret,vregra_estoque,vatv_cons,vatv_cad_cons)
		response.Write(Vretorno)
	else
		response.Write("0|Favor informe o produto")
	end if	
	
	
Case "loc_consultor"

	if  vloc_cons_por <> "" and  vidconsultor <> "" then	
		Vretorno = Fc_loc_consultor(conexao_loja_mw,vidconsultor,vloc_cons_por)
		response.Write(Vretorno)
	else
		response.Write("0|Consultor Não Encontrado")
	end if
	
	
Case "sim_qualificacao"

	if  vidconsultor <> "" then	
		Vretorno = Fc_sim_qualificacao(conexao_mw,vidconsultor)
		response.Write(Vretorno)
	else
		response.Write("0|Consultor Não Encontrado")
	end if	
	
Case "ver_atv_consultor"	
	
	if  vidconsultor <> "" then	
		Vretorno = Fc_ver_atv_consultor(conexao_mw,vidconsultor)
		response.Write(Vretorno)
	else
		response.Write("0|Consultor Não Encontrado!")
	end if
	

Case "ped_pendente_consultor"	
	
	if  vidconsultor <> "" then	
		Vretorno = Fc_pedido_pendente(conexao_mw,vidconsultor)
		response.Write(Vretorno)
	else
		response.Write("0|Consultor Não Encontrado!")
	end if	
	

Case "lista_endereco"
	
	if vidconsultor <> "" then	
		Vretorno = Fc_lista_endereco(conexao_loja_mw,vidconsultor)
		response.Write(Vretorno)
	else
		response.Write("0|Consultor Não Encontrado")
	end if
	
	
Case "altera_endereco"
	
	if vidconsultor <> "" then	
		Vretorno = Fc_Altera_endereco(conexao_loja_mw,vidconsultor,vendereco,vnumero,vcompl,vbairro,vcidade,vestado,vcep_entr)
		response.Write(Vretorno)
	else
		response.Write("0|Consultor Não Encontrado")
	end if	


Case "obter_cdh"

Vretorno = Fc_obter_cdh(conexao_mw,vss_pg,vestado)
response.Write(Vretorno)


Case "lista_pedido_consulta"
	
	dataInicio = Request.Form("dataInicio")
	dataTermino = Request.Form("dataTermino")
	codigoPedido = Request.Form("codigoPedido")
		
	if vidconsultor <> "" then	
		Vretorno = Fc_Lista_Pedido_Consulta(conexao_loja_mw,vidconsultor,dataInicio,dataTermino,codigoPedido)
		response.Write(Vretorno)
	else
		response.Write("0|Consultor Não Encontrado")
	end if

Case "gravaXML"
	
	idPedido = Request.Form("idPedido")
	xml = Request.Form("xml")
	coluna = Request.Form("coluna")
	AcquirerTransactionId = Request.Form("AcquirerTransactionId")
	resposta = Request.Form("resposta")
	statusAntiFraude = Request.Form("statusAntiFraude")
	braspagTransactionId = Request.Form("braspagTransactionId")
	amount = Request.Form("amount")
	
	Vretorno = gravaXML(conexao_mw, idPedido, xml, coluna, AcquirerTransactionId, resposta, statusAntiFraude, braspagTransactionId, amount)
	response.Write(Vretorno)
	
Case "aprovaPedido"
	
	idPedido = Request.Form("id_pedido_oficial")
	codigoAprovacao = Request.Form("codigoAprovacao")
	AcquirerTransactionId = Request.Form("AcquirerTransactionId")
	AuthorizationCode = Request.Form("AuthorizationCode")
	
	Vretorno = gravaAprovacao(conexao_mw, idPedido, codigoAprovacao, AcquirerTransactionId, AuthorizationCode)
	response.Write(Vretorno)

Case "verificaPedidoExistente"
	idPedido = Request.Form("idPedido")
	
	Vretorno = verificaPedidoExistente(conexao_mw, idPedido)
	response.Write(Vretorno)
	
Case "listaPedidosPendentes"

	Vretorno = listaPedidosPendentes(conexao_loja_mw)
	response.Write(Vretorno)
	
Case "mudaStatusPedidoManual"
	idPedido = Request.Form("idPedido")
	
	Vretorno = mudaStatusPedidoManual(conexao_mw, idPedido)
	response.Write(Vretorno)

Case "mudaStatusPedido"

	login = Request.Form("login")
	senha = Request.Form("senha")
	
	if login = "hinode" and senha = "hinode@2013!" then
		idPedido = Request.Form("idPedido")
		novoStatus = Request.Form("novoStatus")
		
		Vretorno = novoStatus(conexao_mw, idPedido, novoStatus)
		response.Write(Vretorno)
	else	
		Response.Write("0")
	end if
	
Case "salvaEnderecoBoleto"

	idPedido = Request.Form("idPedido")
	endereco = Request.Form("endereco")
		
	Vretorno = salvaEnderecoBoleto(conexao_mw, idPedido, endereco)
	
	response.Write(Vretorno)
		
Case Else
	Response.Write("0")
		
End Select


'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX FUNCTION  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
Function salvaEnderecoBoleto(conexao, idPedido, endereco)
	
	vSQLUP = "UPDATE [hinode_loja].[dbo].[tbl_mw_gera_pedido] SET boleto = '" & endereco & "' WHERE IdPedido_oficial = '" & idPedido & "'"
			On Error Resume Next 
			conexao.Execute(vSQLUP)
	
	if err.number <> 0 then
		response.Write("0|Erro: ao atualizar o pedido, revise seus dados!")
	else
		response.Write(vid_pedido_oficial&"|Atualização Do Pedido Efetuado Com Sucesso!")
	end if
	
	EnviarEmail = EnviaPedidoEmail(idPedido, conexao, "")
		
Exit Function
end function

Function mudaStatusPedido(conexao, idPedido, novoStatus)
	
	vSQLUP = "UPDATE [hinode_loja].[dbo].[tbl_mw_gera_pedido] SET Ped_xml_antifraude_status = " & novoStatus & " WHERE IdPedido_oficial = '" & idPedido & "'"
			On Error Resume Next 
			conexao.Execute(vSQLUP)
	
	if err.number <> 0 then
		response.Write("0|Erro: ao atualizar o pedido, revise seus dados!")
	else
		response.Write(vid_pedido_oficial&"|Atualização Do Pedido Efetuado Com Sucesso!")
	end if
		
conexao.Close
set conexao=nothing
	
Exit Function
end function

Function mudaStatusPedidoManual(conexao, idPedido)
	
	vSQLUP = "UPDATE [hinode_loja].[dbo].[tbl_mw_gera_pedido] SET Ped_xml_antifraude_status = 501 WHERE IdPedido_oficial = '" & idPedido & "'"
			On Error Resume Next 
			conexao.Execute(vSQLUP)
	
	if err.number <> 0 then
		response.Write("0|Erro: ao atualizar o pedido, revise seus dados!")
	else
		response.Write(vid_pedido_oficial&"|Atualização Do Pedido Efetuado Com Sucesso!")
	end if
		
conexao.Close
set conexao=nothing
	
Exit Function
end function

Function listaPedidosPendentes(conexao)
	
	query = "SELECT * FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido] where Ped_xml_antifraude_status <> 0 ORDER BY CdPedido DESC"
	QueryToJSON(conexao, query).Flush
		
	conexao.Close
	set conexao = nothing
	
Exit Function
end function

Function verificaPedidoExistente(conexao, idPedido)
	
	query = "SELECT * FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido] where IdPedido_oficial = '" & idPedido & "' AND Ped_xml_pagador is not null"
	QueryToJSON(conexao, query).Flush
		
conexao.Close
set conexao=nothing
	
Exit Function
end function

Function gravaAprovacao(conexao, idPedido, codigoAprovacao, AcquirerTransactionId, AuthorizationCode)
	
	vSQLUP = "UPDATE [hinode_loja].[dbo].[tbl_mw_gera_pedido] set "&_
			"Ped_CodigoAprovacao = '" & codigoAprovacao & "', Ped_tid = '" & AcquirerTransactionId & "', Ped_arp = '" & AuthorizationCode & "'" &_ 
			" where IdPedido_oficial = '" & idPedido &"'"
			On Error Resume Next 
			conexao.Execute(vSQLUP)

	query = "exec [hinode_loja].[dbo].[loja_AtualizarCobranca_sp] '" & idPedido & "','" & AcquirerTransactionId & "'" & "','" & AuthorizationCode & "'"
	conexao.Execute(query)

	
	if err.number <> 0 then
		response.Write("0|Erro: ao atualizar o pedido, revise seus dados!")
	else
		response.Write(vid_pedido_oficial&"|Atualização Do Pedido Efetuado Com Sucesso!")
	end if
		
conexao.Close
set conexao=nothing
	
Exit Function
end function

Function gravaXML(conexao, idPedido, xml, coluna, AcquirerTransactionId, resposta, statusAntiFraude, braspagTransactionId, amount)
	
	vSQLUP = "UPDATE [hinode_loja].[dbo].[tbl_mw_gera_pedido] set "&_
			coluna & " = '" & xml & "', Ped_tid = '" & AcquirerTransactionId & "', Ped_xml_antifraude_response = '" & resposta & "', Ped_xml_antifraude_status = '" & statusAntiFraude & "', braspagTransactionId = '" & braspagTransactionId & "', amount = '" & amount & "'" &_ 
			" where IdPedido_oficial = '" & idPedido &"'"
			On Error Resume Next 
			conexao.Execute(vSQLUP)
	
	if err.number <> 0 then
		response.Write("0|Erro: ao atualizar o pedido, revise seus dados!")
	else
		response.Write(vid_pedido_oficial&"|Atualização Do Pedido Efetuado Com Sucesso!")
	end if
		
conexao.Close
set conexao=nothing
	
Exit Function
end function

Function Fc_sim_qualificacao(conexao,vidconsultor)
  
 		strsql= " select qtd_revendedores, qtd_master,vol_grupo,vol_destaque,atividade"&_
				" ,minimo_pontos, master_min,minimo_grupo, minimo_destaque"&_
				" ,qualificacao_atual,objetivo, vc.nome, vc.integracao"&_
				" from view_rede_desempenho vd , view_consultor vc"&_
				" where vd.hierarquia = vc.hierarquia "&_
				" and inicio = (select top 1 inicio from rede_parametros where fechado = 0 order by inicio) "&_
				" and vc.integracao = '"&vidconsultor&"'"
		QueryToJSON(conexao,strsql).Flush
		
conexao.Close
set conexao=nothing
	
Exit Function
end function


Function Fc_Verifica_Recibo(conexao,vdestinatario,vciclo)
  
    if vciclo <> "" then
		strsql= "exec [hinode].[dbo].[rede_Recibo_sp] '"&vdestinatario&"','"&vciclo&"'"
		QueryToJSON(conexao,strsql).Flush
	else
		strsql= "exec [hinode].[dbo].[rede_Recibo_sp] '"&vdestinatario&"'"
		QueryToJSON(conexao,strsql).Flush
	end if
	

conexao.Close
set conexao=nothing
	
Exit Function
end function

Function Fc_Verifica_Recibo_Lideranca(conexao,vdestinatario,vciclo)
  
    if vciclo <> "" then
		strsql= "exec [hinode].[dbo].[rede_Recibo_lideranca_sp] '"&vdestinatario&"','"&vciclo&"'"
		QueryToJSON(conexao,strsql).Flush
	else
		strsql= "exec [hinode].[dbo].[rede_Recibo_lideranca_sp] '"&vdestinatario&"'"
		QueryToJSON(conexao,strsql).Flush
	end if
	

conexao.Close
set conexao=nothing
	
Exit Function
end function



Function Fc_Lista_Pedido_Consulta(conexao, idConsultor, dataInicio, dataTermino, codigoPedido)

if dataInicio = "" and dataTermino = "" and codigoPedido = "" then

	vSQLLISTPED = " SELECT top 200 * " &_
	" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido] " &_
	" inner join [hinode].[dbo].[view_ListaPedido] on IdPedido_oficial = IdPedido " &_
	" WHERE Ped_idConsultor = '" & idConsultor & "' ORDER BY datapedido DESC "

elseif codigoPedido <> "" then

	vSQLLISTPED = " SELECT * " &_
	" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido] " &_
	" inner join [hinode].[dbo].[view_ListaPedido] on IdPedido_oficial = IdPedido " &_
	" WHERE Ped_idConsultor = '" & idConsultor & "' AND IdPedido_oficial = '" & codigoPedido & "'"

else

	vSQLLISTPED = " SELECT * " &_
	" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido] " &_
	" inner join [hinode].[dbo].[view_ListaPedido] on IdPedido_oficial = IdPedido " &_
	" WHERE Ped_idConsultor = '" & idConsultor & "' AND Ped_dt_cadastro >= '" & dataInicio & "' AND Ped_dt_cadastro <= '" & dataTermino & "' ORDER BY datapedido DESC "

end if

QueryToJSON(conexao, vSQLLISTPED).Flush				  

conexao.Close
set conexao=nothing
	
Exit Function
end function

function Fc_alt_pedido(conexao,vid_pedido_oficial,vid_forma_pag,vdesc_forma_pag,vPed_Juros,vqtd_parcela_card)

vSQLUP = " UPDATE [hinode_loja].[dbo].[tbl_mw_gera_pedido] set "&_
				   " Ped_idFormaPagamento = '" & vid_forma_pag & "',Ped_pagamento='"&vdesc_forma_pag&"',"&_ 
				   " Ped_QtdaParcela = '"& vqtd_parcela_card &"' ,Ped_Juros='"& vPed_Juros &"', "& _
				   " Ped_dt_alteracao = GetDate() "&_
				   " where IdPedido_oficial = '" & vid_pedido_oficial &"'"
				   On Error Resume Next 
				   conexao.Execute(vSQLUP)
				   
				   if err.number <> 0 then
	   					response.Write("0|Erro: ao atualizar o pedido, revise seus dados!")
				   else
						response.Write(vid_pedido_oficial&"|Atualização Do Pedido Efetuado Com Sucesso!")
				   end if
				   

conexao.Close
set conexao=nothing

Exit Function 
End Function



function Fc_gera_pedido(conexao,vidconsultor,vss_pg,vmodo_entrega,vnome_cons,vid_cdhret,vid_cdhret_desc,vcep_entr,vendereco,vnumero,vcompl,vbairro,vcidade,vestado,vid_forma_pag,vdesc_forma_pag,vvl_frete,vvl_ped,vvl_sub_ped,vpontos_ped,vpeso_ped,vstatus_ped,vemail_ped,vcd_aprovacao_card,vqtd_parcela_card,vprazo_transp_ped,vid_transp,vtransp,vl_credito_usado_ped,vped_tipo,vemail_cdh,vPed_Juros,vqtd_parc_auto,valorDesconto,valorExcedente,codigoKit,vtel,idlogcons)

vSQLCAR = " SELECT * " &_
" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
" WHERE Car_idConsultor='"&vidconsultor &"' and Car_session = '"&vss_pg&"'"&_
" order by CdCarrinho asc "

set adocar=conexao.execute(vSQLCAR)
			
			if not adocar.eof then

				vSQLPED = "INSERT INTO [hinode_loja].[dbo].[tbl_mw_gera_pedido] " &_
				"(Ped_idConsultor,Ped_idStatus,Ped_nome_consultor,Ped_email,Ped_session,Ped_idFormaPagamento,Ped_pagamento"&_
				",Ped_CodigoAprovacao,Ped_QtdaParcela,Ped_vl_sub_total,Ped_vl_frete,Ped_vl_total,Ped_pontos"&_
				",Ped_peso,Ped_modo_entr,Ped_prazo_dias,Ped_idDestinatario_local_retira"&_
				",Ped_destinatario_local_retira_nome,Ped_idTransporte,Ped_Transporte,Ped_cep,Ped_endereco,Ped_numero,Ped_compl"&_
				",Ped_bairro,Ped_cidade,Ped_estado,Ped_ip_remoto,Ped_dt_cadastro,Ped_Credito_usado,Ped_Juros,Ped_tipo,"&_
				" Ped_parcela_auto,Ped_desconto,Ped_excedente,Ped_kit_ativacao,Ped_tel,Ped_idConsultorLogin)" &_ 
				" VALUES ('" & vidconsultor & "','" & vstatus_ped & "','" & vnome_cons &"','" & vemail_ped & "'," &_
				"'" & vss_pg & "','" & vid_forma_pag & "','"&vdesc_forma_pag&"','"&vcd_aprovacao_card&"'," &_
				"'" & vqtd_parcela_card & "','"& vvl_sub_ped &"','"& vvl_frete &"','"& vvl_ped &"','" & vpontos_ped & "',"&_
				"'"& vpeso_ped &"','"& vmodo_entrega &"','"& vprazo_transp_ped &"',"&_
				"'"& vid_cdhret &"','"& vid_cdhret_desc &"','"& vid_transp &"','"& vtransp &"','"& vcep_entr &"','"& vendereco &"',"&_
				"'"&vnumero&"','"& vcompl &"','"& vbairro &"','"& vcidade &"','"&vestado&"',"&_ 
				"'"& Request.ServerVariables("REMOTE_ADDR")&"',GetDate(),'"&vl_credito_usado_ped&"','"&vPed_Juros&"',"&_
				"'"&vped_tipo&"',"&cint(vqtd_parc_auto)&",'"&valorDesconto&"','"&valorExcedente&"','"&codigoKit&"','"&vtel&"','"&idlogcons&"');" 

				On Error Resume Next 
				conexao.Execute(vSQLPED)
				
			if err.number <> 0 then	
				EnviarEmail = EnviaNotificaTecnico("Ped_idConsultor:"&vidconsultor&"-"& Err.number & "-" & err.Description,"GERANDO-PEDIDO",vSQLPED)
				response.Write("0|Erro ao gerar o pedido, revise seus dados!")
			else
				set ObjRs = conexao.execute("SELECT IDENT_CURRENT('[hinode_loja].[dbo].[tbl_mw_gera_pedido]') AS [ID]")
				IDPED = objRs("ID")
								
				objRs.Close
				Set objRs = Nothing
				
				'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX INSERIR PEDIDOS ITENS XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
				vSQLPEDITEM = ""
				
				do while not adocar.eof
				
				vPeso = replace(replace(adocar("Car_prod_peso"),".",""),",",".")
				vValor_unt = replace(replace(adocar("Car_prod_valor_unt"),".",""),",",".")
				'vValor_unt_desc = replace(replace(adocar("Car_prod_valor_desc"),".",""),",",".") 
				vValor_unt_desc = replace(replace(adocar("Car_prod_desconto"),".",""),",",".")
				vPontuacao = replace(replace(adocar("Car_prod_pontuacao"),".",""),",",".")
				
				if isNull(adocar("Car_class")) then 
				   vClass = 0 
				else  
				   vClass = adocar("Car_class") 
			    end if
				
				
				vSQLPEDITEM = vSQLPEDITEM & "INSERT INTO [hinode_loja].[dbo].[tbl_mw_gera_pedido_item] " &_
				"(PedItem_CdPedido,PedItem_idProduto,PedItem_prod_codigo,PedItem_prod_nome," &_
                "PedItem_prod_valor_unt,PedItem_prod_valor_desc,"&_
                "PedItem_prod_qtd,PedItem_prod_peso,PedItem_prod_pontuacao,PedItem_prod_qtd_encomendar, " &_
				"PediItem_prod_atv_consultor,PedItem_class) " &_					
        " VALUES ('" & IDPED & "','" & adocar("Car_idProduto") & "','" & adocar("Car_prod_codigo") &"'," &_
		"'" &  adocar("Car_prod_nome") & "','" & vValor_unt & "'," &_
		"'" & vValor_unt_desc & "','"& adocar("Car_prod_qtd") &"'," &_
		"'" & vPeso &"','"& vPontuacao &"','"&adocar("Car_prod_qtd_encomendar")&"',"&_
		"'"&adocar("Car_prod_atv_consultor")&"','"&vClass&"');" 
					
				adocar.movenext
				loop
				
				On Error Resume Next 
				conexao.Execute(vSQLPEDITEM)
				
				if err.number <> 0 then	
				    
					'------------------------- CANCELA O PEDIDO GERADO -----------------------
					vSQLUP = " UPDATE [hinode_loja].[dbo].[tbl_mw_gera_pedido] set "&_
				   " Ped_dt_alteracao = GetDate(),Ped_idStatus=0 "&_
				   " where CdPedido = '" & IDPED &"'"
				   conexao.Execute(vSQLUP)
				   '------------------------------------------------------------------vidconsultor--
				   response.Write("0|Erro:Ocorreu um erro ao inserir os itens do pedido, revise seus dados!")
				   
				else
				    
					if vidconsultor <> "00109466" then
						vSQLCARDEL = " DELETE FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
									 " WHERE Car_session='"&vss_pg&"' and Car_idConsultor='"& vidconsultor &"'"&_
						" and Car_id_cdh_retira = '"&vid_cdhret&"' AND Car_tipo_ped='CONSULTOR_HINODE' "
						conexao.execute(vSQLCARDEL)
					end if
					
					'XXXXXXXXXXXXXXXXXXXXXXXXX IMPORTA O PEDIDO XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
					
					strsql="exec [hinode_loja].[dbo].[loja_FechaPedido_sp] '"&vid_cdhret&"','"&IDPED&"' "
					On Error Resume Next
					set ado_proc=conexao.execute(strsql)
							
					if err.number <> 0 then
					
					EnviarEmail = EnviaNotificaTecnico("idErro:"&vId_cod&" - PedSite="&vId_pedido_oficial&" - CdPed="&IDPED&" - CDH="&vid_cdhret&" - ErroNumber="& Err.number & "-" & err.Description,"IMPORTA-PEDIDO-CONSULTOR-HINODE-0",strsql)
					
					else
					
					if ( not ado_proc.eof ) then
						
							vId_cod = ado_proc("cod")
							vmsg = ado_proc("msg")
							vId_pedido_oficial = ado_proc("idPedido") 
						    
							'if int(vId_cod)=0 and int(vId_pedido_oficial)>0 then
							if int(vId_pedido_oficial) > 0 then
								
								if int(vid_forma_pag) <> 5 then
									EnviarEmail = EnviaPedidoEmail(vId_pedido_oficial,conexao,vemail_cdh)
								end if
								
								response.Write(vId_pedido_oficial&"|Pedido gerado com sucesso...!")
									
							elseif int(vId_cod)>0 and int(vId_pedido_oficial)=0 then
	
	response.Write("0|Pedido Cancelado, Produto de Ativação esgotado!" & vId_cod & " - " & vmsg & " - " & IDPED)
	EnviarEmail = EnviaNotificaTecnico("idErro:"&vId_cod&" - PedSite="&vId_pedido_oficial&" - CdPed="&IDPED&" - CDH="&vid_cdhret&" - ErroNumber="& Err.number & "-" & err.Description,"IMPORTA-PEDIDO-CONSULTOR-HINODE-1",strsql)
	
							else
EnviarEmail = EnviaNotificaTecnico("idErro:"&vId_cod&" - PedSite="&vId_pedido_oficial&" - CdPed="&IDPED&" - CDH="&vid_cdhret&" - ErroNumber="& Err.number & "-" & err.Description,"IMPORTA-PEDIDO-CONSULTOR-HINODE-2",strsql)
								response.Write("0|Erro:"&vId_cod&"-"&vmsg&"-"&vId_pedido_oficial)
							end if
							
					else
						response.Write("0|Erro: Na Importação do Pedido gerado!")
					end if

                  end if
				  
					'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
					
				    
				end if
				'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX INSERIR PEDIDOS ITENS XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			end if
			
			
else
response.Write("0|Não foi possível localizar o carrinho desse consultor / seção!")
end if
			
adocar.Close	
set adocar=nothing

conexao.Close
set conexao=nothing

Exit Function 
End Function

function Fc_lista_pedido(conexao,vid_pedido_oficial)

vSQLLISTPED = " SELECT * " &_
" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido] " &_
" WHERE IdPedido_oficial='"&vid_pedido_oficial &"'"

QueryToJSON(conexao, vSQLLISTPED).Flush				  

conexao.Close
set conexao=nothing
	
Exit Function
end function


function Fc_lista_pedido_item(conexao,vid_pedido_oficial)

vSQLLISTPEDITEM = " SELECT item.* " &_
" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_item] item " &_
" inner join [hinode_loja].[dbo].[tbl_mw_gera_pedido] p ON p.CdPedido = item.PedItem_CdPedido"&_
" WHERE p.IdPedido_oficial='"& vid_pedido_oficial &"'"

QueryToJSON(conexao, vSQLLISTPEDITEM).Flush				  

conexao.Close
set conexao=nothing
	
Exit Function
end function




function Fc_car_calc_frete_list_transp(conexao,vss_pg,vpeso_ped,vcep_entr,vid_cdhret)

'===================== LISTA TRANSPORTE E CALCULA O FRETE ==========================
	strsqlfrete= "exec [hinode_loja].[dbo].[loja_ListaTransportadora_sp] '"&vss_pg&"','"& vid_cdhret &"'"
	''response.Write(strsqlfrete)
	QueryToJSON(conexao,strsqlfrete).Flush
	'response.Write(QueryToJSON(conexao,strsqlfrete).Flush)
'====================================================================================

conexao.Close
set conexao=nothing

Exit Function 
End Function


function Fc_car_lista_hist(conexao,vidconsultor,vid_cdhret)

vSQLCARLISTHIST = " SELECT top 1 Car_session " &_
" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
" WHERE Car_idConsultor='"&vidconsultor&"'"&_ 
" And Car_id_cdh_retira='"&vid_cdhret&"' AND Car_tipo_ped='CONSULTOR_HINODE' order by Car_DtCriacao desc "

set adohist=conexao.execute(vSQLCARLISTHIST)
			
			if not adohist.eof then
			response.Write("1|"&adohist("Car_session"))
			else
			response.Write("0|Não há histórico de Carrinho desse consultor!")
			end if
	
adohist.Close	
set adohist=nothing

conexao.Close
set conexao=nothing

Exit Function 
End Function




Function EnviaPedidoEmail(vid_pedido_oficial,conexao,vemail_cdh)

id_pedido_oficial = vid_pedido_oficial

if int(id_pedido_oficial) > 0 then

	Set adoPed = Server.CreateObject("ADODB.Recordset")
	
	vSQLLISTPED = " SELECT * " &_
	" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido] " &_
	" WHERE IdPedido_oficial='"&id_pedido_oficial &"'"
	
	adoPed.Open vSQLLISTPED, conexao
		
	if(not adoPed.eof)then
			
		vPed_idStatus = adoPed("Ped_idStatus")
		vPed_QtdaParcela = adoPed("Ped_QtdaParcela")
		vIdPedido_oficial = adoPed("IdPedido_oficial")
		vPed_nome_consultor = adoPed("Ped_nome_consultor")
		vPed_idConsultor = adoPed("Ped_idConsultor")
		vPed_pagamento = adoPed("Ped_pagamento")
		vPed_idFormaPagamento = adoPed("Ped_idFormaPagamento")
		vPed_vl_sub_total = FormatNumber(adoPed("Ped_vl_sub_total"),2)
		vPed_vl_frete = FormatNumber(adoPed("Ped_vl_frete"),2)
		vPed_vl_total = FormatNumber(adoPed("Ped_vl_total"),2)
		vPed_Credito_usado = FormatNumber(adoPed("Ped_Credito_usado"),2)
		vPed_modo_entr = adoPed("Ped_modo_entr")
		vPed_prazo_dias = adoPed("Ped_prazo_dias")
		vPed_Transporte = adoPed("Ped_Transporte")
		
		boleto = adoPed("boleto")
		
		vPed_cep = adoPed("Ped_cep")
		vPed_endereco = adoPed("Ped_endereco")
		vPed_numero = adoPed("Ped_numero")
		vPed_compl = adoPed("Ped_compl")
		vPed_bairro = adoPed("Ped_bairro")
		vPed_cidade = adoPed("Ped_cidade")
		vPed_estado = adoPed("Ped_estado")
		vPed_dt_cadastro = adoPed("Ped_dt_cadastro")
		vPed_email = adoPed("Ped_email")
	end if
		
	if vPed_email = "" then
		vPed_email = "ancelmo@hinode.com.br"
		'vPed_email = "leonardo@mediaw.com.br"
		'vPed_email = "rafael@mediaw.com.br"
	end if	 
	
	'XXXXXXXXXXXXXXXXX INICIO NOTIFICAÇÃO POR E-MAIL - CONSULTOR XXXXXXXXXXXXXXXXXXXX
	
	if vPed_email <> "" then
	
		vMsg = "<table bgcolor='#FFFFFF' border='0' style='padding:20px;'>"& _
		"<tbody>"& _
		"<tr>"& _
		"<td width='550'>"& _
		"<table align='center' border='0' cellpadding='0' cellspacing='0'>"& _
		"<tbody>"& _
		"<tr>"& _
		"<td style='padding-bottom:5px;' width='550'>"& _
		"<table style='background-color:#f1e3c8; border:1px solid #e1caa8; width:100%; padding:10px 15px;'>"& _
		"<tbody>"& _
		"<tr>"& _
		"<td width='180'>"& _
		"<img alt='Hinode.com.br' src='http://vo.hinode.com.br/images/logo-hinode-cosmeticos-email.png' /></td>"& _
		"<td align='center' style='font-family:arial; font-size:16px; font-weight:bold; color:#000000;'>"& _
		"PEDIDO REVENDEDOR > HINODE GERADO - "&vid_pedido_oficial&"</td>"& _
		"</tr>"& _
		"</tbody>"& _
		"</table>"& _
		"</td>"& _
		"</tr>"& _
		"<tr>"& _
		"<td valign='top' style='padding:15px; border:1px solid #e1caa8;'>"& _
		"<table align='center' border='0' cellpadding='0' cellspacing='0'>"& _
		"<tbody>"& _
		"<tr>"& _
		"<td style='font:13px Arial;' width='520'>"& _
		"<p>"& _
		"Olá, "&vPed_nome_consultor&", <br>"& _ 
		"À HINODE COSMÉTICOS, está enviando uma copia do pedido gerado.<br />"& _ 
		"O seu número de pedido é: <strong>"&vid_pedido_oficial&"</strong>.<br /><br />"& _
	
	
		"Dados do Pedido:<br />"&_
		""&vPed_nome_consultor&"<br />"&_
		"Sub total pedido:"&vPed_vl_sub_total&"<br />"&_
		"Frete:"&vPed_vl_frete&"<br />"&_
		"Crédito Usado:"&vPed_Credito_usado&"<br />"&_
		"Valor total pedido:"&vPed_vl_total&"<br />"&_
		"Forma de Pagamento:"&vPed_pagamento&" "&vPed_QtdaParcela&"x <br />"
		
		if cint(vPed_idFormaPagamento) = 5 then
			vMsg = vMsg & "<br /><br /><strong>"&_
			"<a href='" & boleto & "' target='_blank'>"&_
			"IMPRIMA SEU BOLETO BANCÁRIO, CLICANDO AQUI.</a></strong><br /><br />"
		end if
	
		vMsg = vMsg & "<strong>Tipo de Envio:</strong><br />"&_
		""&vPed_Transporte&"<br /><br />"&_
		
		"<strong>Itens do pedido:</strong><br />"&_
		"<table align='center' border='0' cellpadding='1' cellspacing='1' width='100%' bgcolor='#f1e3c8'>"&_
		"<tbody>"&_
		"<tr>"&_
		"<td>Código</td>"&_
		"<td>Produto</td>"&_
		"<td>Quant.</td>"&_
		"<td>Valor Unit.</td>"&_
		"<td>Valor Desconto</td>"&_
		"<td>Valor Total</td>"&_
		"</tr>"
	
		Set adoPeditem = Server.CreateObject("ADODB.Recordset")
	
		vSQLLISTPEDITEM = " SELECT item.* " &_
		" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_item] item " &_
		" inner join [hinode_loja].[dbo].[tbl_mw_gera_pedido] p ON p.CdPedido = item.PedItem_CdPedido"&_
		" WHERE p.IdPedido_oficial='"& vid_pedido_oficial &"'"
		
		adoPeditem.Open vSQLLISTPEDITEM, conexao
		
		if(not adoPeditem.eof) then
	
			items = ""
	
			do while not adoPeditem.eof
				
				if adoPeditem("PedItem_class") = 1 then
				
					vPedItem_prod_codigo = adoPeditem("PedItem_prod_codigo")
					vPedItem_prod_nome = adoPeditem("PedItem_prod_nome")
					vPedItem_prod_qtd_encomendar = adoPeditem("PedItem_prod_qtd_encomendar")
					vPedItem_prod_qtd = adoPeditem("PedItem_prod_qtd")
					vPedItem_prod_valor_unt = adoPeditem("PedItem_prod_valor_unt")
					valorDesconto = adoPeditem("PedItem_prod_valor_desc")
					vvalor_total_item = vPedItem_prod_qtd*vPedItem_prod_valor_unt - valorDesconto
					vPedItem_prod_valor_unt2 = FormatNumber(vPedItem_prod_valor_unt,2)
					vvalor_total_item2 = FormatNumber(vvalor_total_item,2)
		
					if vPedItem_prod_qtd_encomendar > 0 then
						encomendar_item = "</br><span class='texto_vermelho'>Item sob encomenda, quantidade encomendar:</span>"&vPedItem_prod_qtd_encomendar&""
					else
						encomendar_item = ""
					end if
					
					if adoPeditem("PediItem_prod_atv_consultor") = 1 then
						vMsg = vMsg & "<tr bgcolor='#FFC'>"&_
						"<td>"&vPedItem_prod_codigo&"</td>"&_
						"<td>"&vPedItem_prod_nome&""&encomendar_item&"</td>"&_
						"<td>"&vPedItem_prod_qtd&"</td>"&_
						"<td>R$"&vPedItem_prod_valor_unt2&"</td>"&_
						"<td>R$"&FormatNumber(valorDesconto,2)&"</td>"&_
						"<td>R$"&vvalor_total_item2&"</td>"&_
						"</tr>"
					else
						vMsg = vMsg & "<tr bgcolor='#FEFEDD'>"&_
						"<td>"&vPedItem_prod_codigo&"</td>"&_
						"<td>"&vPedItem_prod_nome&""&encomendar_item&"</td>"&_
						"<td>"&vPedItem_prod_qtd&"</td>"&_
						"<td>R$"&vPedItem_prod_valor_unt2&"</td>"&_
						"<td>R$"&FormatNumber(valorDesconto,2)&"</td>"&_
						"<td>R$"&vvalor_total_item2&"</td>"&_
						"</tr>"
					end if
										
				else
					
					idProduto = adoPeditem("PedItem_prod_codigo")
					
					arrayItems = Split(items,"|")
					encontrou = false
					
					For i = 0 to Ubound(arrayItems)  
						if arrayItems(i) = idProduto then
							encontrou = true
						end if
					Next
					
					if encontrou = false then
						
						quantidadeProduto = 0
						items = items + "|" + idProduto
						
						query = " SELECT item.* " &_
						" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_item] item " &_
						" inner join [hinode_loja].[dbo].[tbl_mw_gera_pedido] p ON p.CdPedido = item.PedItem_CdPedido"&_
						" WHERE p.IdPedido_oficial='"& vid_pedido_oficial & "' AND item.PedItem_prod_codigo='" & idProduto & "' AND item.PedItem_class = '0'"				
						
						Set adoCarregaTodosProdutos = Server.CreateObject("ADODB.Recordset")
						adoCarregaTodosProdutos.Open query, conexao_loja_mw
						
						if(not adoCarregaTodosProdutos.eof) then
							
							do while not adoCarregaTodosProdutos.eof
								quantidadeProduto = quantidadeProduto + 1
								adoCarregaTodosProdutos.movenext
							loop	
							
						end if
						
						vPedItem_prod_codigo = adoPeditem("PedItem_prod_codigo")
						vPedItem_prod_nome = adoPeditem("PedItem_prod_nome")
						vPedItem_prod_qtd_encomendar = adoPeditem("PedItem_prod_qtd_encomendar")
						vPedItem_prod_qtd = quantidadeProduto
						vPedItem_prod_valor_unt = adoPeditem("PedItem_prod_valor_unt")
						valorDesconto = adoPeditem("PedItem_prod_valor_desc")
						vvalor_total_item = vPedItem_prod_qtd*vPedItem_prod_valor_unt	
						vPedItem_prod_valor_unt2 = FormatNumber(vPedItem_prod_valor_unt,2)
						vvalor_total_item2 = FormatNumber(vvalor_total_item,2)
			
						if vPedItem_prod_qtd_encomendar > 0 then
							encomendar_item = "</br><span class='texto_vermelho'>Item sob encomenda, quantidade encomendar:</span>"&vPedItem_prod_qtd_encomendar&""
						else
							encomendar_item = ""
						end if
			
						vMsg = vMsg & "<tr bgcolor='#FFFFFF'>"&_
						"<td>"&vPedItem_prod_codigo&"</td>"&_
						"<td>"&vPedItem_prod_nome&""&encomendar_item&"</td>"&_
						"<td>"&vPedItem_prod_qtd&"</td>"&_
						"<td>R$"&vPedItem_prod_valor_unt2&"</td>"&_
						"<td>R$"&FormatNumber(valorDesconto,2)&"</td>"&_
						"<td>R$"&vvalor_total_item2&"</td>"&_
						"</tr>"
						
					
					end if
					
				end if
				
				adoPeditem.movenext
	
			loop
	
		end if
	
		vMsg = vMsg & "</tbody>"&_
		"</table>"&_
		"</td>"& _
		"</tr>"& _
		"</tbody>"& _
		"</table>"& _
		"</td>"& _
		"</tr>"& _
		"<tr>"& _
		"<td height='90' style='font:12px Arial; background:#f1e3c8; color:#000000; padding:10px 15px'>"& _
		"<table>"& _
		"<tbody>"& _
		"<tr>"& _
		"<td style='padding-left:10px'>"& _
		"<p style='font:12px arial; color:#000000'>"& _
		"Atenciosamente,<br />"& _
		"<span style='font:bold 20px arial;'>HINODE COSMÉTICOS</span><br />"& _
		"<a href='http://www.hinode.com.br' style='color:#000000' target='_blank' "& _ 
		"title='www.hinode.com.br'>www.hinode.com.br</a></p>"& _
		"</td>"& _
		"</tr>"& _
		"</tbody>"& _
		"</table>"& _
		"</td>"& _
		"</tr>"& _
		"</tbody>"& _
		"</table>"& _
		"</td>"& _
		"</tr>"& _
		"</tbody>"& _
		"</table>"
	
		if cstr(vPed_idConsultor) = "00101209" or cstr(vPed_idConsultor) = "05006444" then
			'vPed_email = "leonardo@mediaw.com.br"
			vPed_email = "rafael@mediaw.com.br"
		end if	 
	
		servidor="smtp.hinode.com.br"
		
		de  ="registro@hinode.com.br"
		
		user="registro@hinode.com.br"
		
		pass="hinode@2012"
		
		para = vPed_email
		'para = "rafael@mediaw.com.br"
		
		assunto	= "PEDIDO REVENDEDOR-HINODE [" & vid_pedido_oficial &"]"
		
		Set Mail = Server.CreateObject("CDO.Message") 
		
		Set MailCon = Server.CreateObject ("CDO.Configuration") 
	
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = servidor
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 587 
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 
		
		if len(user)>0 then
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = user
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = pass
		end if
	
		MailCon.Fields.update
	
		Set Mail.Configuration = MailCon 
		Mail.From = de
		
		if vemail_cdh <> "" then
			Mail.CC = vemail_cdh
		end if
	
		'Mail.BCC="fabio@hinode.com.br;isabel@hinode.com.br;leandro@hinode.com.br;ancelmo@hinode.com.br;toni@hinode.com.br"
		Mail.To = para
		Mail.Subject = assunto
		'id_Body	="Esse é um teste de email."
		Mail.HTMLBody=vMsg
		Mail.Fields.update
		Mail.Send
		Set Mail = Nothing 
		Set MailCon = Nothing 
		'Response.Write "Enviado com sucesso às " & now
	end if		   

end if

'XXXXXXXXXXXXXXXXXXX FIM - ENVIA NOTIFICAÇÃO POR E-MAIL - CONSULTOR XXXXXXXXXXXXXXXXXXXX

adoPed.Close
Set adoPed = Nothing

adoPeditem.Close
Set adoPeditem = Nothing

conexao.Close
Set conexao = Nothing


Exit Function	  
End Function


function Fc_credito_consultor(conexao,vidconsultor)

'vSQL_CRED_CONSULTOR = " SELECT top 1 valor " &_
'" FROM [hinode_loja].[dbo].[view_credito] " &_
'" WHERE integracao='"&vidconsultor&"'"

vSQL_CRED_CONSULTOR = " SELECT top 1 cred.valor, cons.integracao " &_
" FROM [hinode_loja].[dbo].[view_Consultor] cons " &_
" INNER JOIN [hinode_loja].[dbo].[view_credito] cred ON cred.destinatario = cons.destinatario " &_
" WHERE cons.integracao='"&vidconsultor&"'"


set adocred=conexao.execute(vSQL_CRED_CONSULTOR)
			
			if not adocred.eof then
			
				if CInt(adocred("valor")) > 0 then
					response.Write("1|"&replace(replace(adocred("valor"),".",""),",","."))
				else
					response.Write("0|Não há credito para este cdh!")
				end if
		
			else	
				response.Write("0|Consultor Não Encontrado!")
				'response.Write("1|40.00")
			end if
	
adocred.Close	
set adocred=nothing

conexao.Close
set conexao=nothing

Exit Function 
End Function


function Fc_pedido_pendente(conexao,vidconsultor)

vSQLPED = " SELECT pediCodigo " &_
" FROM [hinode].[dbo].[view_PedidoRevendedorKit] " &_
" WHERE integracao='"&vidconsultor &"'"

set adoped=conexao.execute(vSQLPED)
			
			if not adoped.eof then
			
				if CDbl(adoped("pediCodigo")) > 0 then
					response.Write("1|"&adoped("pediCodigo"))
				else
					response.Write("0|Não há pedido pendente!")
				end if
		
			else	
					response.Write("0|Não há pedido pendente!")
			end if
	
adoped.Close	
set adoped=nothing

conexao.Close
set conexao=nothing

Exit Function 
End Function

	
function Fc_lista_cat_subcat(conexao,idcat)

if cint(idcat)=0 then

vSQL = " SELECT idUso,Uso,ordem" &_
" FROM [hinode_loja].[dbo].[loja_ListaUso] " &_
" WHERE idUso > 0 order by ordem,Uso "

else

vSQL = " SELECT l.idLinha,l.Linha" &_
" FROM [hinode_loja].[dbo].[loja_ListaLinha] l " &_
" INNER JOIN [hinode_loja].[dbo].[loja_ListaUsoXLinha] uso ON uso.idLinha = l.idLinha " &_
" WHERE uso.idUso = "&cint(idcat)&" order by l.Linha "

end if

QueryToJSON(conexao,vSQL).Flush				  

conexao.Close
set conexao=nothing
	
Exit Function
end function


function Fc_lista_prod_subcat(conexao,idcat)

if int(idcat)<>0 then

vSQL = " SELECT DISTINCT Codigo,Nome,Descricao" &_
" FROM [hinode_loja].[dbo].[loja_ListaProduto] " &_
" WHERE idLinha = "&int(idcat)&" order by Nome "

end if

QueryToJSON(conexao,vSQL).Flush				  

conexao.Close
set conexao=nothing
	
Exit Function
end function	

function Fc_car_lista_item(conexao,vidconsultor,vss_pg,vid_cdhret)

vSQLCARLIST = " SELECT CdCarrinho,Car_idProduto,Car_prod_codigo,Car_prod_nome,Car_prod_valor_unt," &_
" Car_prod_qtd,Car_prod_peso,Car_prod_pontuacao,Car_prod_qtd_encomendar,Car_prod_atv_consultor,Car_qtd_minima_kitxcol "

'if cint(vatv_cons)=0 then
'vSQLCARLIST = vSQLCARLIST & " ,(Select codigo from [hinode].[dbo].[view_ListaProdutoAtivacao] " &_
'" where codigo = Car_prod_codigo) as vcod_prod_ativacao "
'end if

vSQLCARLIST = vSQLCARLIST & " FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_ 
" WHERE Car_session='"&vss_pg&"' and Car_idConsultor='"&vidconsultor &"' "&_
" and Car_id_cdh_retira='"&vid_cdhret&"' order by CdCarrinho desc "

QueryToJSON(conexao, vSQLCARLIST).Flush				  

conexao.Close
set conexao=nothing
	
Exit Function
end function


function Fc_car_del_item(conexao,vidconsultor,vloc_prod,vss_pg,vid_car)

vSQLCARDEL = " DELETE FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
				  " WHERE Car_prod_codigo = '" & vloc_prod & "'" &_
				  " and Car_session='"&vss_pg&"' and Car_idConsultor='"&vidconsultor &"' and CdCarrinho='"&vid_car&"'"
				conexao.execute(vSQLCARDEL)
			
			'if not adoudel.eof then
			response.Write("1|Item excluido com sucesso!")
			'else 
			'response.Write("0|Erro: ao deletar o item do carrinho, revise seus dados!")
			'End if  

End function



function Fc_car_upd_item(conexao,cl_conexao,vidconsultor,vloc_prod,vqtd,vss_pg,vid_car)



vSQLCARUP = " SELECT CdCarrinho FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
				  " WHERE Car_prod_codigo = '" & vloc_prod & "'" &_
				  " and Car_session='"&vss_pg&"' and Car_idConsultor='"&vidconsultor &"' and CdCarrinho='"&vid_car&"'"
				  
		set adoupd=conexao.execute(vSQLCARUP)
			
			if not adoupd.eof then
			
			vSQLUP = " UPDATE [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] set " &_
				   " Car_prod_qtd = '" & cint(vqtd) & "',"&_ 
				   " Car_DtAlteracao=GetDate() "& _
				   " where CdCarrinho = '" & adoupd("CdCarrinho") &"'"
				   On Error Resume Next 
				   conexao.Execute(vSQLUP)
				   
				   if err.number <> 0 then
	   					response.Write("0|Erro: ao atualizar o item no carrinho, revise seus dados!")
				   else
						response.Write("1|Atualização Do Item do Carrinho Efetuado Com Sucesso!")
				   end if
			
			else
			response.Write("0|Erro: item no carrinho indisponível, revise seus dados!")
			end if
			
adoupd.Close	
set adoupd=nothing

if cl_conexao = 1 then
	conexao.Close
	set conexao=nothing
end if


Exit Function 
End Function


function Fc_car_add_item(conexao,vidconsultor,vloc_prod,vqtd,vss_pg,vid_cdhret,vregra_estoque,vatv_cons,vatv_cad_cons)


vSQLCARDEL = " DELETE FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
				  " WHERE Car_prod_codigo = '" & vloc_prod & "'" &_
				  " and Car_session='"&vss_pg&"' and Car_idConsultor='"&vidconsultor &"' and Car_prod_atv_consultor=0"
				conexao.execute(vSQLCARDEL)


'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX VERIFICA ITEM KIT XXXXXXXXXXXXXXXXX	
  
vprod_atv_consultor = 0

if int(vloc_prod)=7002 OR int(vloc_prod)=7011 OR int(vloc_prod)=7012 OR int(vloc_prod)=7013 OR int(vloc_prod)=7014 OR int(vloc_prod)=7000 then

vSQL_Item_atv = " SELECT CdCarrinho FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
" WHERE Car_prod_codigo IN('007002','007011','007012','007013','007014','007000')" &_
" and Car_session='"&vss_pg&"' and Car_idConsultor='"&vidconsultor &"'"
		
			set adoitematv=conexao.execute(vSQL_Item_atv)
			
			if not adoitematv.eof then
				vloc_prod = 0
			elseif int(vatv_cad_cons) = 0 AND int(vloc_prod)=7000 then
				vloc_prod = 0
			elseif int(vatv_cad_cons) = 0 AND int(vloc_prod)<>7000 then
				vprod_atv_consultor = 1
			elseif int(vatv_cons) = 0 AND ( int(vloc_prod)=7002 OR int(vloc_prod)=7011 OR int(vloc_prod)=7012 OR int(vloc_prod)=7013 OR int(vloc_prod)=7014 OR int(vloc_prod)=7000) then
				vloc_prod = "007000"
				vprod_atv_consultor = 1
			end if
			
			adoitematv.Close	
			set adoitematv=nothing

end if


'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

	  
strSqlList = "SELECT top 1 IdProduto,Nome,Codigo,Valor_Consultor,Valor_Catalogo," &_
			  " Ativo,Peso,pontuacao,Qtd_Minima,Valor_Revendedor "&_
			  " from [homol_hinode_loja].[dbo].[loja_ListaProduto] " &_
			  " WHERE Codigo = '" & vloc_prod & "' and Ativo = 1 ORDER BY Nome ASC "
			  	  
set adoadd=conexao.execute(strSqlList)

if not adoadd.eof then

   'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX VERIFICA ESTOQUE XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		 
		 Valor_prod_unt = adoadd("Valor_Revendedor")
		 Vpontuacao = adoadd("pontuacao")
		 VQtd_Minima = adoadd("Qtd_Minima")
		 
		 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX VERIFICA SE O CONSULTOR ESTA INATIVO XXXXXXXXXXXXXXXXXXXXXXXXXXXX
		 

'if vprod_atv_consultor = 1 or (int(vatv_cons) = 0 and int(vatv_cad_cons)=1) then
	 
'		 			Set conexao_mw = Server.CreateObject("ADODB.Connection")
'		 				conexao_mw.Open "Provider=SQLOLEDB.1; Network Library=dbmssocn; Data Source=177.154.134.115,1433;" & _
'						  "User ID=hinode; Password=trymxc4!9pw; Initial Catalog=hinode; charset=utf8;"
		 
' strSqlAtv = "Select Valor from [hinode].[dbo].[view_ListaProdutoAtivacao] where codigo = '"& vloc_prod &"'"

'		 			set adoatv=conexao_mw.execute(strSqlAtv)
					
'					if not adoatv.eof then
'						Valor_prod_unt = adoatv("Valor")
'						Vpontuacao = 25
'						vprod_atv_consultor = 1
'					end if
			
'					adoatv.Close	
'					set adoatv=nothing

'					conexao_mw.Close
'					Set conexao_mw = Nothing

'		 end if
		 
		 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		 
	 strEstoque = "SELECT Estoque=SUM(Estoque) " &_
		" FROM [hinode_loja].[dbo].[view_ListaProdutoEstoque] " &_
		" WHERE IdProduto = '" &adoadd("IdProduto")& "'"
		
		if vid_cdhret="01001736" then ' --- pedido para alphaville	
			strEstoque = strEstoque & " and Codigo = '" & vloc_prod & "' and empresa = '00000849' and opcao<>5 "
	 	else
	 		strEstoque = strEstoque & " and Codigo = '" & vloc_prod & "' and empresa = '"&vid_cdhret&"'"
	 	end if
	  
	  set adoest=conexao.execute(strEstoque)
	 
	if not adoest.eof then
	
		 vEstoque = adoest("Estoque")
 		 
	     if isNull(vEstoque) then 
		    vEstoque = 0
		 end if
		 
		 
	  if vregra_estoque="0" and cint(vEstoque) <= 0 then ' Regra produto somente com estoque positivo
		 
		 response.Write("0|Produto Indisponível No Momento!")
		 
      else 
		 
		 if vregra_estoque="1" and cint(vEstoque) <= cint(vqtd) then 
		 'Regra de estoque, vende produto com estoque negativo
		 
		 		 vqtd_encomendar = vqtd - abs(vEstoque)
				 if vqtd_encomendar < 0  then vqtd_encomendar = vqtd end if  
		 
		 elseif vregra_estoque = "0" and cint(vEstoque) <= cint(vqtd) then
		 
		  		 vqtd = vEstoque
				 vqtd_encomendar = 0 
		 
		 end if

		'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX add item no carrinho XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		
		vPeso = replace(replace(adoadd("Peso"),".",""),",",".")
		vValor_unt = replace(replace(Valor_prod_unt,".",""),",",".") 
		vPontuacao = replace(replace(Vpontuacao,".",""),",",".") 
		vIdProduto = adoadd("IdProduto")
		vCodigo = adoadd("Codigo")
		
		 if vprod_atv_consultor=1 then 
			vqtd=1 
		 end if
		
		'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX VERIFICA SE O PROD JA TEM NO CARRINHO XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		
vSQLCAR = " SELECT CdCarrinho FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
" WHERE Car_idProduto = '"& vIdProduto &"' and Car_prod_codigo = '" & vCodigo & "'" &_
" and Car_session='"&vss_pg&"' and Car_idConsultor='"&vidconsultor &"' and Car_prod_atv_consultor=0"
		set adoprodcar=conexao.execute(vSQLCAR)
			
			if not adoprodcar.eof then
			
			'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX ATUALIZA O PROD NO CARRINHO XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			'vRetorno_up = Fc_car_upd_item(conexao,0,vidconsultor,vCodigo,vqtd,vss_pg,adoprodcar("CdCarrinho"))
			'response.Write(vRetorno)0
			
			vSQLUP = " UPDATE [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] set " &_
				   " Car_prod_qtd = '" & cint(vqtd) & "',Car_prod_qtd_encomendar='"&cint(vqtd_encomendar)&"',"&_ 
				   " Car_prod_valor_unt = '"& vValor_unt &"' ,Car_DtAlteracao='"& InverteData(Now(),True) &"', "& _
				   " Car_prod_atv_consultor = '"& cint(vprod_atv_consultor) &"' "&_
				   " where CdCarrinho = '" & adoprodcar("CdCarrinho") &"'"
				   On Error Resume Next 
				   conexao.Execute(vSQLUP)
				   
				   if err.number <> 0 then
	   					response.Write("0|Erro: ao atualizar o item no carrinho, revise seus dados!")
				   else
						response.Write("1|Atualização Do Item do Carrinho Efetuado Com Sucesso!")
				   end if
				   
			'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			
			else
			 
			   
			
vSQL = "INSERT INTO [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho]" &_
				" (Car_session,Car_idConsultor,Car_idProduto,Car_prod_codigo,Car_prod_nome,Car_prod_valor_unt,Car_prod_qtd," &_
		"Car_prod_peso,Car_prod_pontuacao,Car_prod_qtd_encomendar,Car_DtCriacao,Car_id_cdh_retira,Car_prod_atv_consultor,Car_qtd_minima_kitxcol)" &_ 
				" VALUES ('" & vss_pg & "','" & vidconsultor & "','" & vIdProduto &"','" & vCodigo & "'," &_
				"'" & adoadd("Nome") & "','" & vValor_unt & "','"&cint(vqtd)&"','"&vPeso&"'," &_
				"'" & vPontuacao & "','" & cint(vqtd_encomendar) & "','"& InverteData(Now(),True) &"'," &_
				"'" & vid_cdhret & "','" & cint(vprod_atv_consultor) & "','"&cint(VQtd_Minima)&"');" 
				On Error Resume Next 
				conexao.Execute(vSQL)
				if err.number <> 0 then	
					response.Write("0|Erro: Ao inserir item no carrinho, revise seus dados!")	
				else
					response.Write("1|Item adicionado no carrinho com sucesso!")
				end if
		 	
			end if
			
		adoprodcar.Close	
		set adoprodcar=nothing
						
		'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	
	end if
		
	else
	response.Write("0|Produto Indisponível No Momento!")
	end if
else
response.Write("0|Produto Não Encontrado..!")
end if


adoadd.Close	
set adoadd=nothing
conexao.Close
Set conexao = Nothing


Exit Function 
End Function



'============================================= Hinode 2.0 ===============================================

Function listaItems(conexao,vidconsultor,vss_pg,vatv_cad_cons,vid_cdhret,vatv_cons)

	vSQLCARLIST = " SELECT car.*,p.idLinha, p.idUso "&_
	" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] car " &_ 
	" LEFT JOIN [homol_hinode_loja].[dbo].[loja_ListaProduto] p ON p.idProduto=car.Car_idProduto "&_ 
	" and p.Codigo=car.Car_prod_codigo " &_
	" WHERE car.Car_session='"&vss_pg&"' and car.Car_idConsultor='"&vidconsultor &"' "&_
	" and car.Car_id_cdh_retira = '"&vid_cdhret&"' AND car.Car_tipo_ped='CONSULTOR_HINODE' "&_ 
	" ORDER BY car.CdCarrinho,car.Car_idProduto ASC "
	
	vatv_cad_consultor = cint(vatv_cad_cons)
	vatv_cons_consultor = cint(vatv_cons)
	
	'if vatv_cad_consultor > 0 and vatv_cons_consultor = 0 then
		
	'	set adoListaItems = conexao.execute(vSQLCARLIST)
	
	'	valorTotalMomento = 0	
	'	encontrou = false
	'	carClass = 0
	'	vprod_atv_consultor = 0
	'	vCar_prod_pontuacao = 0
	'	vCar_prod_pontuacao_total = 0
		
	'	if not adoListaItems.eof then
		
	'		do while not adoListaItems.eof 
				
	'		vCar_prod_pontuacao = cdbl(adoListaItems("Car_prod_qtd")) * cdbl(adoListaItems("Car_prod_pontuacao")) 
	'		vCar_prod_pontuacao_total = vCar_prod_pontuacao_total + vCar_prod_pontuacao 	
				
	'			vCar_prod_codigo = adoListaItems("Car_prod_codigo")
	'			valorPrecoConsultor = adoListaItems("Car_prod_valor_unt_con")
	'			valorPrecoCatalogo = adoListaItems("Car_prod_valor_unt_cat")
				
	'			vidLinha = adoListaItems("idLinha")
	'			vidUso = adoListaItems("idUso")
				
	'			if vidUso="" OR IsNull(vidUso) then
	'				vidUso = 4
	'			end if
				
	'			if vidLinha="" OR isNull(vidLinha) then
	'				vidLinha = 31
	'			end if
				
	'			if encontrou = false then
					
	'				vl_preco_unt = valorPrecoCatalogo
						
	'			  if int(vCar_prod_codigo) <> 7000 then
					
	'				 if cint(vidUso)=9 and cint(vidLinha)=15 then 
	'					valorTotalMomento = valorTotalMomento + valorPrecoCatalogo
	'					carClass = 1
	'				 elseif cint(vidUso)<>9 then
	'					valorTotalMomento = valorTotalMomento + valorPrecoCatalogo
	'					carClass = 1
	'				 else
	'					carClass = 0 	
	'				 end if
					
	'			  else	
	'			  carClass = 0
	'			  end if
					
	  
	'				if CDbl(valorTotalMomento) >= 100.0 AND CDbl(vCar_prod_pontuacao_total) >= 39.99999 then
	'					resto = CDbl(valorTotalMomento - 100)
	'					valorDescontoTemp = CDbl(resto * 0.5)
	'					valorDescontoTemp = FormatNumber(valorDescontoTemp,2)
	'					valorDescontoTemp = replace(replace(valorDescontoTemp,".",""),",",".")
	'					vprod_atv_consultor = 1
	'					encontrou = true
	'				else
	'					vprod_atv_consultor = 0
	'					valorDescontoTemp = 0.0
	'					valorDescontoTemp = FormatNumber(valorDescontoTemp,2)
	'					valorDescontoTemp = replace(replace(valorDescontoTemp,".",""),",",".")
	'				end if
	'			else
	'				carClass = 0
	'				vprod_atv_consultor = 0
	'				valorDescontoTemp = 0.0
	'				valorDescontoTemp = FormatNumber(valorDescontoTemp,2)
	'				valorDescontoTemp = replace(replace(valorDescontoTemp,".",""),",",".")	
	'				vl_preco_unt = valorPrecoConsultor		 
	'			end if
				
	'			query = " UPDATE [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] set " &_
	'			" Car_prod_desconto = '" & valorDescontoTemp & "'"&_ 
	'			" ,Car_prod_valor_unt = '" & replace(replace(vl_preco_unt,".",""),",",".") & "'"&_
	'			" ,Car_prod_atv_consultor = '"& cint(vprod_atv_consultor) &"' "&_
	'			" ,Car_class = '"& cint(carClass) &"' "&_
	'			" where CdCarrinho = '" & adoListaItems("CdCarrinho") &"'"
	'			conexao.execute(query)
						
	'			adoListaItems.movenext 
	'		loop
			
	'	end if	
	'		adoListaItems.Close	
	'		set adoListaItems=nothing    
	'else
					  
	'end if
	
	QueryToJSON(conexao, vSQLCARLIST).Flush
	
	conexao.Close
	set conexao=nothing

Exit Function
End Function

Function deletaItem(conexao,vidconsultor,vloc_prod,vss_pg,vid_car,vatv_cad_cons,vqtd)
	
	vqtd_prod = cint(vqtd)
	
	For i = 0 to vqtd_prod  
		
		If i=vqtd_prod Then Exit For
		
		vSQLCARDEL = " DELETE TOP (1) FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
		" WHERE Car_prod_codigo = '" & vloc_prod & "'" &_
		" and Car_session='"&vss_pg&"' and Car_idConsultor='"&vidconsultor &"' "&_
		" AND Car_tipo_ped='CONSULTOR_HINODE' "
		On Error Resume Next 
		conexao.Execute(vSQLCARDEL)
	Next
	
	if err.number <> 0 then	
		response.Write("0|Erro: Ao deletar item no carrinho, revise seus dados!")	
	else
		response.Write("1|Item excluido com sucesso!")
    end if
	

Exit Function
End Function

Function addItem(conexao,vidconsultor,vloc_prod,vqtd,vss_pg,vid_cdhret,vregra_estoque,vatv_cons,vatv_cad_cons)
	
	'Trazer saldo carrinho===============================================================================
	strSqlList_vlsub = " SELECT Car_vl_subtotal=SUM(Car_prod_qtd*Car_prod_valor_unt) " &_ 
	" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
	" WHERE Car_session='"&vss_pg&"' and Car_idConsultor='"&vidconsultor&"' and Car_tipo_ped='CONSULTOR_HINODE'"
	
	set adolistsub=conexao.execute(strSqlList_vlsub)
	if not adolistsub.eof then
		Carvlsubtotal = adolistsub("Car_vl_subtotal")
		
		if isNull(Carvlsubtotal) then 
			Carvlsubtotal = 0
		end if
		
	else
		Carvlsubtotal = 0
	end if
	
	adolistsub.Close	
	set adolistsub=nothing
	'=====================================================================================================
	
	'Regra para escolha do produto 7000(ativacao)=========================================================
	vprod_atv_consultor = 0
	
	vatv_consultor_alerta = 0
	
	if int(vloc_prod)=7002 OR int(vloc_prod)=7011 OR int(vloc_prod)=7012 OR int(vloc_prod)=7013 OR int(vloc_prod)=7014 OR int(vloc_prod)=7000 then
	
		vSQL_Item_atv = " SELECT CdCarrinho FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
		" WHERE Car_prod_codigo IN('007002','007011','007012','007013','007014','007000','007008')" &_
		" and Car_session='"&vss_pg&"' and Car_idConsultor='"&vidconsultor &"' and Car_tipo_ped='CONSULTOR_HINODE'"
	
		set adoitematv=conexao.execute(vSQL_Item_atv)
	
		if not adoitematv.eof then
			vloc_prod = 0
		elseif int(vatv_cad_cons) = 0 AND int(vloc_prod)=7000 then
			vloc_prod = 0
		elseif int(vatv_cad_cons) = 0 AND int(vloc_prod)<>7000 then
			vprod_atv_consultor = 0
		elseif int(vatv_cons) = 0 AND ( int(vloc_prod)=7002 OR int(vloc_prod)=7011 OR int(vloc_prod)=7012 OR int(vloc_prod)=7013 OR int(vloc_prod)=7014 OR int(vloc_prod)=7000 ) then
			vloc_prod = "007000"
		elseif int(vatv_cons) = 1 AND ( int(vloc_prod)=7002 OR int(vloc_prod)=7011 OR int(vloc_prod)=7012 OR int(vloc_prod)=7013 OR int(vloc_prod)=7014) then
			vloc_prod = "0"	
		    vatv_consultor_alerta = 0
		elseif int(vatv_cons) = 1 AND int(vloc_prod)=7000  then
			vloc_prod = "007000"
		end if
	
	adoitematv.Close	
	set adoitematv=nothing
	
	end if
	'====================================================================================================
	
	'============================== Buscar Informação do produto ========================================
	
	strSqlList = "SELECT top 1 IdProduto,Nome,Codigo,Valor_Consultor,Valor_Catalogo," &_
	" Ativo,Peso,pontuacao,Qtd_Minima,pontuacao_minima,Valor_Revendedor" &_
	" from [homol_hinode_loja].[dbo].[loja_ListaProduto] " &_
	" WHERE Codigo = '" & vloc_prod & "' and Ativo = 1 and Valor_Consultor > 0 ORDER BY Nome ASC "
	
	'response.write(strSqlList)
	
	set adoadd = conexao.execute(strSqlList)
	
	if not adoadd.eof then
		
		Valor_prod_unt = adoadd("Valor_Revendedor") 'adoadd("Valor_Consultor")
		Valor_prod_unt_cat = adoadd("Valor_Catalogo")
		Valor_prod_unt_con = adoadd("Valor_Revendedor") 'adoadd("Valor_Consultor")
		
		Vpontuacao = adoadd("pontuacao")
		VQtd_Minima = adoadd("Qtd_Minima")
		
		if cdbl(adoadd("pontuacao_minima")) > 0 then
			pontuacaoMinima = cdbl(adoadd("pontuacao_minima")) + cint(Vpontuacao)'Nova variável Hinode 2.0
		else
			pontuacaoMinima = cdbl(adoadd("pontuacao_minima"))
		end if
				
				
		'if int(vatv_cad_cons) = 1 and int(vatv_cons) = 0 and cdbl(Carvlsubtotal) < 100.0 then
	'		Valor_prod_unt = adoadd("Valor_Revendedor") 'adoadd("Valor_Catalogo")
	'	end if
		
		
		strEstoque = "SELECT Estoque=SUM(Estoque) " &_
		" FROM [hinode_loja].[dbo].[view_ListaProdutoEstoque] " &_
		" WHERE IdProduto = '" &adoadd("IdProduto")& "'"
		
		if vid_cdhret="01001736" then
		strEstoque = strEstoque & " and Codigo = '" & vloc_prod & "' and empresa = '00000849'"
		else
		strEstoque = strEstoque & " and Codigo = '" & vloc_prod & "' and empresa = '"&vid_cdhret&"'"
		end if
		
		'response.Write(strEstoque)
		
		set adoest = conexao.execute(strEstoque)
		
		if not adoest.eof then
		
			vEstoque = adoest("Estoque")
			'response.write("vEstoque:"&vEstoque)
			
	     	if isNull(vEstoque) then 
		    	vEstoque = 0
		 	end if
			
			if vregra_estoque="0" and cdbl(vEstoque) <= 0 then 
				'Regra produto somente com estoque positivo
				response.Write("0|Produto Indisponível No Momento!" & vregra_estoque & " " & cint(vEstoque))
			else
			
				if vregra_estoque="1" and cdbl(vEstoque) <= cdbl(vqtd) then 
				'Regra de estoque, vende produto com estoque negativo
					vqtd_encomendar = cdbl(vqtd) - abs(vEstoque)
					if vqtd_encomendar < 0 then 
						vqtd_encomendar = vqtd 
					end if  
				elseif vregra_estoque = "0" and cdbl(vEstoque) <= cdbl(vqtd) then
					vqtd = vEstoque
					vqtd_encomendar = 0 
				end if
				
				vPeso = replace(replace(adoadd("Peso"),".",""),",",".")
				vValor_unt = replace(replace(Valor_prod_unt,".",""),",",".")
				
				vValor_unt_cat = replace(replace(Valor_prod_unt_cat,".",""),",",".")
				vValor_unt_con = replace(replace(Valor_prod_unt_con,".",""),",",".")
				
				vPontuacao = replace(replace(Vpontuacao,".",""),",",".") 
				vIdProduto = adoadd("IdProduto")
				vCodigo = adoadd("Codigo")
				
				'vValortotal_item = (vqtd*Valor_prod_unt)
				'vValortotal_item = FormatNumber(vValortotal_item,2)
				
				vNomeProd = adoadd("Nome")
				
				'if int(vprod_atv_consultor) = 1 and int(vatv_cad_cons) = 0 then 
				'	vqtd = 1 
				'end if
				
				'================================= For para adicionar 1 item =========================
				for i = 1 to cdbl(vqtd)
					
					vSQL = "INSERT INTO [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho]" &_
				"(Car_session,Car_idConsultor,Car_idProduto,Car_prod_codigo,Car_prod_nome,Car_prod_valor_unt,Car_prod_qtd," &_
					"Car_prod_peso,Car_prod_pontuacao,Car_prod_qtd_encomendar,Car_DtCriacao,Car_id_cdh_retira," &_
				"Car_prod_atv_consultor,Car_qtd_minima_kitxcol, Car_prod_ponto_minimo,Car_prod_valor_unt_cat,"&_
					"Car_prod_valor_unt_con,Car_class,Car_tipo_ped)" &_ 
					" VALUES ('" & vss_pg & "','" & vidconsultor & "','" & vIdProduto &"','" & vCodigo & "'," &_
					"'" & vNomeProd & "','" & vValor_unt & "',1,'"&vPeso&"'," &_
					"'" & vPontuacao & "','" & int(vqtd_encomendar) & "','"& InverteData(Now(),True) &"'," &_
					"'" & vid_cdhret & "','" & int(vprod_atv_consultor) & "','"&int(VQtd_Minima)&"'," &_
					"'"&int(pontuacaoMinima)&"','" & vValor_unt_cat & "',"&_
					"'" & vValor_unt_con & "',0,'CONSULTOR_HINODE');" 
					
					On Error Resume Next 
					conexao.Execute(vSQL)
					
				next
				'=====================================================================================
				
			end if
		 
		else
			response.Write("0|Produto Indisponível No Momento...!")
		end if
		
	elseif int(vatv_consultor_alerta) = 1 then	
	
	response.Write("0|“Atenção! Você já é um consultor ativo. "&_
	               "Não é permitida a compra de kits Prata, Outo, "&_
				   "Platina e Diamante, é permitida somente a compra do "&_ 
				   "Kit Básico pelo código 7000 que você poderá transferir posteriormente”.!!!")	
		
	else
		response.Write("0|Produto Não Encontrado.. ..!")
	'===============================Fechar Buscar Informação do produto========================================
	end if
	'===============================Fechar Buscar Informação do produto========================================

	adoadd.Close	
	set adoadd=nothing
	conexao.Close
	Set conexao = Nothing

Exit Function
End Function
'========================================================================================================


Function Fc_loc_consultor(conexao,vidconsultor,loc_por)

if loc_por = "1" then
vloc_por = " nome LIKE '%" & vidconsultor & "%'"
else
vloc_por = " integracao = '" & vidconsultor & "'"
end if

strSqlList = "SELECT integracao,nome,email,ativosn,dt_cad from [hinode_loja].[dbo].[view_Consultor] "& _
" WHERE " & vloc_por & " ORDER BY nome ASC"
	  
set adoloc=conexao.execute(strSqlList)

if not adoloc.eof then

			DATA1 = Cdate(Now()) 'DATA ATUAL
			DATA2 = Cdate(adoloc("dt_cad")) 'DATA CADASTRO
			DIAS = DateDiff("d",DATA2,DATA1)

			IF int(adoloc("ativosn"))=0 and int(DIAS) > 30 then
			response.Write("0|O Cadastro do Consultor Expirou O Prazo de 30 dias Para Ativação, Realize um Novo Cadastro!")		
			else
			vid_consultor = adoloc("integracao")
			vretorno = adoloc("integracao")&"|"& _
			" " & replace(adoloc("nome"),"|","")&"|"& _
			" " & replace(adoloc("email"),"|","")&""
			response.Write(vid_consultor&"|"&vretorno)	
	        end if
else
	response.Write("0|O Consultor Não Foi Encontrado!")
end if

adoloc.Close	
set adoloc=nothing
conexao.Close
Set conexao = Nothing


Exit Function 
End Function


Function Fc_ver_atv_consultor(conexao,vidconsultor)

'strSqlAtvCad = "Select ativosn from [hinode].[dbo].[view_consultor] where integracao='"&vidconsultor&"'"
'set ado_atv_cad=conexao.execute(strSqlAtvCad)

'if not ado_atv_cad.eof then	
		
'	 if cint(ado_atv_cad("ativosn")) > 0 then
	   
strSqlList = " Select top 1 pediCodigo From [hinode].[dbo].[view_PrimeiroPedido] "&_
" where (MONTH(dataPedido) = MONTH(GETDATE())) and integracao='"&vidconsultor&"' order by dataPedido desc " 								 				
					
					set ado=conexao.execute(strSqlList)

					if not ado.eof then	
						response.Write("3|"&ado("pediCodigo"))	
					else
						response.Write("1|O Revendedor Não Está Ativo!")
					end if
					
					ado.Close	
					set ado=nothing

'	else
'	response.Write("1|O Cadastro do Consultor Não Está Ativo!")
'	end if	
		
'else
'	response.Write("0|O Consultor Não Encontrado!")
'end if

'ado_atv_cad.Close	
'set ado_atv_cad=nothing

'ado_atv_cad.Close	
'set ado_atv_cad =nothing

conexao.Close
Set conexao = Nothing

Exit Function 
End Function




Function Fc_Atv_Cons_ped(conexao_loja,conexao,vid_pedido_oficial)

strSqlList = "select top 1 con.destinatario,con.integracao,con.nome,con.email "&_ 
" from [hinode_loja].[dbo].[tbl_mw_gera_pedido] ped "&_
" INNER JOIN [hinode_loja].[dbo].[view_Consultor] con ON con.integracao=ped.Ped_idConsultor "&_
" where  ped.IdPedido_oficial ='" & vid_pedido_oficial & "'"	  

set adolist=conexao_loja.execute(strSqlList)

if not adolist.eof then
	
	strsql=" update a set ativosn=1 " & _
		" from [hinode_loja].[dbo].[tabclientes] a, [hinode_loja].[dbo].[tabpedidos] b " & _
		" where a.idcliente=b.idcliente and idpedido=" & vid_pedido_oficial
	conexao_loja.execute(strsql)
	
	strsql=" update [hinode].[dbo].[fat_destinatario] set ativosn=1,usuario_altera='site',dataatualizacao=getdate() "&_
	       " where destinatario='" & adolist("destinatario") & "'"
			
				On Error Resume Next 
				conexao.execute(strsql)
				if err.number <> 0 then	
					response.Write("0|Erro: Ao Tentar Reativar O Consultor!")	
				else
					response.Write("1| Ativação do Consultor Realizado Com Sucesso")
				end if
else
	response.Write("0|Erro: Pedido Do Consultor Não Encontrado!")
end if

adolist.Close	
set adolist=nothing

conexao_loja.Close
Set conexao_loja = Nothing

conexao.Close
Set conexao = Nothing

Exit Function 
End Function





Function Fc_Altera_endereco(conexao,vidconsultor,vendereco,vnumero,vcomplemento,vbairro,vcidade,vestado,vcep)
	
strsql="exec [hinode_loja].[dbo].[loja_AtualizarEnderecoEntrega_sp] '"&vidconsultor&"',"&_
"'"&vendereco&"','"&vnumero&"','"&vcomplemento&"','"&vbairro&"','"&vcidade&"',"&_
"'"&vestado&"','"&vcep&"'"

'response.Write(strsql)

set ado_proc=conexao.execute(strsql)
							
					if ( not ado_proc.eof ) then
						
							vId_cod = ado_proc("cod")
							vmsg = ado_proc("msg")
							
							if cint(vId_cod)=0 then
								response.Write("1|Dados do Endereço Alterado Com Sucesso!")	
							elseif cint(vId_cod)>0 then
								response.Write("1|Não Foi Possível Alterar Os Dados De Endereço!")
							else
								response.Write("0|Erro:"&vId_cod&"-"&vmsg)
							end if
							
					else
						response.Write("0|Erro: Na Alteração do Endereço!")
					end if

ado_proc.Close
Set ado_proc = Nothing

conexao.Close
Set conexao = Nothing

Exit Function 
End Function


Function Fc_lista_endereco(conexao,vidconsultor)
	
strSqlList = " select top 1 integracao,e_Endereco=b.endereco,e_Numero=b.numero,e_Complemento=b.complemento,"&_
" e_Bairro=b.bairro,e_Cidade=b.cidade,e_Estado=b.estado,e_Cep=b.cep,e_tel=t.telefone,e_ddd=t.ddd"&_
" from [hinode].[dbo].[view_Consultor] a inner join hinode.dbo.fat_LocalEntrega c on a.destinatario=c.destinatario_Compra"&_
" inner join [hinode].[dbo].[fat_Destinatario] b on c.destinatario_entrega=b.destinatario"&_
" left join [hinode].[dbo].[view_telefone] t on a.destinatario=t.destinatario" & _
" where a.integracao='" & vidconsultor & "' and b.endereco <>'' order by c.destinatario_Entrega desc"
	  
set adolist=conexao.execute(strSqlList)

if not adolist.eof then

		vid_consultor = adolist("integracao")
			vretorno = replace(adolist("e_Endereco"),"|","")&"|"& _
			"" & replace(adolist("e_Numero"),"|","")&"|"& _
			"" & replace(adolist("e_Complemento"),"|","")&"|"& _
			"" & replace(adolist("e_Bairro"),"|","")&"|"& _
			"" & replace(adolist("e_Cidade"),"|","")&"|"& _
			"" & replace(adolist("e_Estado"),"|","")&"|"& _
			"" & replace(adolist("e_Cep"),"|","")&"|"& _
			"" & replace(adolist("e_ddd"),"|","")&"|"& _
			"" & replace(adolist("e_tel"),"|","")&""
	response.Write(vid_consultor&"|"&vretorno)
else

	strSqlList2 = "select top 1 integracao,e_Endereco=b.endereco,e_Numero=b.numero,e_Complemento=b.complemento,"&_
			 " e_Bairro=b.bairro,e_Cidade=b.cidade,e_Estado=b.estado,e_Cep=b.cep,e_tel=t.telefone,e_ddd=t.ddd " & _
                  " from [hinode].[dbo].[view_Consultor] a "&_
				  " inner join [hinode].[dbo].[fat_Destinatario] b on a.destinatario=b.destinatario " & _
				  " left join [hinode].[dbo].[view_telefone] t on a.destinatario=t.destinatario" & _
				  " where a.integracao='" & vidconsultor & "'"

	set adolist2=conexao.execute(strSqlList2)
	
	if not adolist2.eof then
	
	vid_consultor = adolist2("integracao")
			
	 		
			vretorno = replace(adolist2("e_Endereco"),"|","")&"|"& _
			"" & replace(adolist2("e_Numero"),"|","")&"|"& _
			"" & replace(adolist2("e_Complemento"),"|","")&"|"& _
			"" & replace(adolist2("e_Bairro"),"|","")&"|"& _
			"" & replace(adolist2("e_Cidade"),"|","")&"|"& _
			"" & replace(adolist2("e_Estado"),"|","")&"|"& _
			"" & replace(adolist2("e_Cep"),"|","")&"|"& _
			"" & replace(adolist2("e_ddd"),"|","")&"|"& _
			"" & replace(adolist2("e_tel"),"|","")&""
				
			
	adolist2.Close	
	set adolist2=nothing
		
	response.Write(vid_consultor&"|"&vretorno)
	
	else
	response.Write("0|Endereço do Consultor Não Encontrado")
	end if

	
end if

adolist.Close	
set adolist=nothing
conexao.Close
Set conexao = Nothing

Exit Function 
End Function


Function Fc_obter_cdh(conexao,vss_pg,vestado)

strsql = " select destinatario,descricao,estado,email,bairro,cidade "&_ 
    " from [hinode].[dbo].[view_FranquiaCD] "

if vestado <> "" then	
strsql = strsql & " where destinatario not in('10143544') and estado = '"& vestado &"' and exibe_vo=1 " &_ 
" order by cidade,estado asc "
else
strsql = strsql & " where destinatario not in('10143544') and exibe_vo=1 "&_
" order by cidade,estado asc "
end if

QueryToJSON(conexao, strsql).Flush				  

conexao.Close
set conexao=nothing

	
Exit Function				  
End Function


Function Fc_obter_cdh_old(conexao,vss_pg,vestado)

strsql = " select b.destinatario,a.descricao,c.estado,c.email,c.bairro,c.cidade "&_ 
    " from [hinode].[dbo].[sys_atividade] a "&_ 
	" inner join [hinode].[dbo].[fat_filial] b on a.destinatario_matriz=b.destinatario " & _
	" inner join [hinode].[dbo].[fat_Destinatario] c on b.destinatario=c.destinatario "

if vestado <> "" then	
strsql = strsql & " where c.estado = '"& vestado &"' and b.cd in(1,2) and c.ativosn=1 " &_ 
" and b.destinatario not in('10143544') order by c.cidade,c.estado asc "
else
strsql = strsql & " where b.destinatario not in('10143544') and b.cd in(1,2) and c.ativosn=1 "&_
" order by c.cidade,c.estado asc "
end if

QueryToJSON(conexao, strsql).Flush				  

conexao.Close
set conexao=nothing

	
Exit Function				  
End Function


Function Fc_obter_parcelas(conexao,vid_forma_pag,vvl_ped,vqtd_parc_auto)

strsql_par = " select * from [hinode].[dbo].[view_ParametrosCartaoLoja] "
	 
	if Cint(vqtd_parc_auto)>0 then
strsql_par = strsql_par & " where IdFormaPagamento="&vid_forma_pag&""&_
							" and (parcela <="&cint(vqtd_parc_auto)&" OR Valor_Minimo <= "&vvl_ped&") "&_
							" order by parcela asc "
	else
strsql_par = strsql_par & " where IdFormaPagamento="&vid_forma_pag&" and Valor_Minimo <= "&vvl_ped&" order by parcela asc "
	end if

QueryToJSON(conexao, strsql_par).Flush				  

conexao.Close
set conexao=nothing

	
Exit Function				  
End Function



Function InverteData(data,inverte)
 If isDate(data) then
	 vData = data
	else
	 vData = now()	
	End If
	temp = split(vData,"/")
	
	 If len(temp(0)) <> 2 Then
		 temp(0) = "0" & temp(0)
		End If
		
		If len(temp(1)) <> 2 Then
		 temp(1) = "0" & temp(1)
		End If
	
	If inverte = true Then			 	 	 
 	InverteData = temp(1) & "/" & temp(0) & "/" & temp(2)
	Else 
	 InverteData	= temp(0) & "/" & temp(1) & "/" & temp(2)
	End If	
End Function

Function FormataData(Data)
  If Data <> "" Then
    FormataData = Right("0" & DatePart("d", Data),2) & "/" & Right("0" & DatePart("m", Data),2) & "/" & DatePart("yyyy", Data)
  End If
End Function

Function EnviaNotificaTecnico(Verro,vTela,Vquery)

vMsg = "TELA:"&vTela&" <BR />" &_
"Descrição do Erro:"&Verro&"<BR />"&_
"Query: "&Vquery&"<br />"

servidor="smtp.hinode.com.br"

		de  ="registro@hinode.com.br"

		user="registro@hinode.com.br"

		pass="hinode@2012"

		para = "suporte@mediaw.com.br"

		assunto	= "Notificação de Erro Hinode"
		
		Set Mail = Server.CreateObject("CDO.Message") 

               Set MailCon = Server.CreateObject ("CDO.Configuration") 

MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = servidor
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 587 
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 

               if len(user)>0 then

MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = user
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = pass

               end if

               MailCon.Fields.update
			   
			   Set Mail.Configuration = MailCon 
				   Mail.From = de
				   'Mail.CC="kleber@xcompany.com.br"
				   Mail.To = para
	               Mail.Subject = assunto
				   'id_Body	="Esse é um teste de email."
                   Mail.HTMLBody=vMsg
	               Mail.Fields.update
	               Mail.Send
	           Set Mail = Nothing 
	           Set MailCon = Nothing 
			   'Response.Write "Enviado com sucesso às " & now

Exit Function

end Function


'- Substituindo o apóstrofe(') pelo duplo apóstrofe ('')
Function ExpurgaApostrofe(texto)
    ExpurgaApostrofe = replace( Trim(texto) , "'" , "''")
End function

'- Substituindo os caracteres e palavras maliciosas por vazio("").
Function LimpaLixo(input)
    dim lixo
    dim textoOK

    lixo = array ("SELECT","DROP","UPDATE",";","--","INSERT","DELETE","xp_","<","SCRIPT",">")

    textoOK = Trim(input)

     for i = 0 to UBound(lixo)
          textoOK = replace( textoOK ,  lixo(i) , "")
     next

     LimpaLixo = textoOK

end Function


'- Rejeitando os dados maliciosos:
Function ValidaDados(input)
      lixo = array ( "SELECT" , "INSERT" , "UPDATE" , "DELETE" , "DROP" , "--" , "'")

      ValidaDados = true

      for i = LBound(lixo) to UBound(lixo)
            if ( instr(1 ,  input , lixo(i) , vbtextcompare ) <> 0 ) then
                  ValidaDados = False
                  exit function '}
            end if
      next
end function

%>