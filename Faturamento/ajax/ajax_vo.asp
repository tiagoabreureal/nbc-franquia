﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="../mw-conexao-db.asp"-->
<!--#include file="../json/JSON_2.0.4.asp"-->
<!--#include file="../json/JSON_UTIL_0.1.1.asp"-->

<%

vacao = LimpaLixo(ExpurgaApostrofe(Request("acao")))
vcampanha = LimpaLixo(ExpurgaApostrofe(Request("campanha")))
vidconsultor = LimpaLixo(ExpurgaApostrofe(Request("idconsultor")))

'cpf do cunsultor
arquivoPdf = LimpaLixo(ExpurgaApostrofe(Request("cpfConcultor")))

Select Case vacao




	Case "ativarIngresso"
		Vretorno = ativarIngresso(conexao_mw)
		
	Case "obterIngressosAtivos"
		Vretorno = obterIngressosAtivos(conexao_mw)
	
	Case "obterIngressosAdmin"
		Vretorno = obterIngressosAdmin(conexao_mw)
	
	Case "desativarIngresso"
		Vretorno = desativarIngresso(conexao_mw)


	Case "lista_cupom_consultor"
		if vidconsultor <> "" then
			Vretorno = Fc_lista_cupom(conexao_mw,vcampanha,vidconsultor)
			Response.Write(Vretorno)
		else
			Response.Write("0|Erro Consultor Não Encontrado!")
		end if
		
	Case "obterIngressosConsultor"
		Vretorno = obterIngressosConsultor(conexao_mw)	
			
	Case "obterConsultor"
		Vretorno = obterConsultor(conexao_mw)	
		
	Case "enviarIngresso"
		Vretorno = enviarIngresso(conexao_mw)
		
	Case "confirmarIngresso"
		Vretorno = confirmarIngresso(conexao_mw)
		
	Case "geraIngresso"
		Vretorno = geraIngresso(conexao_mw)
	
	Case "obterFranquias"
		Vretorno = obterFranquias(conexao_mw)
	
	Case "geraIngressos"
		Vretorno = geraIngressos(conexao_mw)
	
	Case "conf_recibo"
		Vretorno = confRecibo(conexao_mw)
	
	Case "conf_recibo_lideranca"
		Vretorno = confRecibo_lideranca(conexao_mw)	
	
	Case "confirmarEntrada"
		Vretorno = confirmarEntrada(conexao_mw)
	
	Case "geraIngressoPatrocinador"
		'Vretorno = geraIngressoPatrocinador(conexao_mw)
	
	Case "verificaIngressoPatrocinador"
		Vretorno = verificaIngressoPatrocinador(conexao_mw)
	
	Case "obtemIngressoPatrocinador"
		Vretorno = obtemIngressoPatrocinador(conexao_mw)
		
	Case "confirmarEntradaPatrocinador"
		Vretorno = confirmarEntradaPatrocinador(conexao_mw)
		
	Case "cupom_cons_qtd"
		Vretorno = Fc_cupom_cons_qtd(conexao_mw,vidconsultor)
		
	Case "cupom_cons_geral"
		Vretorno = Fc_cupom_cons_geral(conexao_mw,vidconsultor)
	
	Case "obterIngressosProprios"
		Vretorno = obterIngressosProprios(conexao_mw)
		
	Case "obterTodosIngressos"
		Vretorno = obterTodosIngressos(conexao_mw)
	
	Case "obterIngressosPatrocinador"
		Vretorno = obterIngressosPatrocinador(conexao_mw)
	
	Case "limparIngresso"
		Vretorno = limparIngresso(conexao_mw)
	
	Case "obterIngressosAdministracao"
		Vretorno = obterIngressosAdministracao(conexao_mw)
		
	Case "obterPdfConsultor"
		Vretorno = obterPdfConsultor(arquivoPdf)
	
	Case Else
			Response.Write("0")		
			
End Select

FUNCTION ativarIngresso(conexao)
	
	codigoIngresso = LimpaLixo(ExpurgaApostrofe(REQUEST("codigoIngresso")))
	idConsultor = LimpaLixo(ExpurgaApostrofe(REQUEST("idConsultor")))
	qtdIngressosAtivos = 0
	
	data = Split(NOW, " ")
	hora = data(1)
	dataSplit = Split(data(0),"/")
	data = 	dataSplit(2)&"-"&dataSplit(1)&"-"&dataSplit(0)
	dataHora = data&" "&hora
	
		query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingressos] WHERE codigo_ingresso = '" & codigoIngresso & "'"
		query2 = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingressos] WHERE idConsultor_ingresso = '" & idConsultor & "'"
		
		Set adoRecordset = Server.CreateObject("ADODB.Recordset")
		Set adoConvitesAtivos = Server.CreateObject("ADODB.Recordset")
		
		adoRecordset.Open query, conexao
		adoConvitesAtivos.Open query2, conexao		
		
		Set retorno = jsArray()		
		if not adoRecordset.eof then
			
			do while not adoConvitesAtivos.eof
				qtdIngressosAtivos = qtdIngressosAtivos + 1
				adoConvitesAtivos.movenext
			loop 		
			
			'response.Write(qtdIngressosAtivos)	
			'response.End()
			
			adoRecordset.movefirst
			
			Set retorno = jsObject()
			
			if int(adoRecordset("status_ingresso")) = 0 AND qtdIngressosAtivos <= 1 then
			
				query = "UPDATE [hinode].[dbo].[tbl_mw_ingressos] " &_
						"SET " &_
						"idConsultor_ingresso='" & idConsultor & "', " &_
						"status_ingresso = 1, " &_
						"dataAtivacao_ingresso = '"&dataHora&".000' " &_
						"WHERE codigo_ingresso = '" & codigoIngresso & "' "
						
						'response.Write(query)
						'response.End()
						
						conexao.Execute(query)
						
				retorno("status") = "success"
				retorno("mensagem") = "Ingresso Ativado! ("&codigoIngresso&") "&qtdIngressosAtivos&""
			else 
				retorno("status") = "error"
				'retorno("mensagem") = "O Convite ("&codigoIngresso&") Já esta Ativo"
				retorno("mensagem") = "Você já ativou 2 Convites!"
			end if
			
			retorno.Flush
			
		else
			
			Set retorno = jsObject()
			retorno("status") = "error"
			retorno("mensagem") = "O Convite ("&codigoIngresso&") Não Existe"
			retorno.Flush
		
		end if
	
	conexao.Close
	set conexao = nothing

EXIT FUNCTION
END FUNCTION

FUNCTION obterIngressosAtivos(conexao)
	
	idConsultor = LimpaLixo(ExpurgaApostrofe(REQUEST("idConsultor")))
	codigoIngresso = LimpaLixo(ExpurgaApostrofe(REQUEST("codigoIngresso")))
	
	if idConsultor <> "" then 
	
	
			query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingressos] WHERE idConsultor_ingresso = '"& idConsultor &"' AND status_ingresso = 1 "
	
		
		Set adoRecordset = Server.CreateObject("ADODB.Recordset")
		adoRecordset.Open query, conexao
		
		if(not adoRecordset.eof) then	
			
			Set retorno = jsArray()
	
			do while not adoRecordset.eof
			
				Set retorno(Null) = jsObject()
				retorno(Null)("codigo_ingresso") = adoRecordset("codigo_ingresso")
				retorno(Null)("status_ingresso") = adoRecordset("status_ingresso")
				retorno(Null)("idConsultor_ingresso") = adoRecordset("idConsultor_ingresso")
				retorno(Null)("data") = adoRecordset("dataAtivacao_ingresso")
				
				adoRecordset.movenext
			loop 
			
			retorno.Flush
		else
		
			Set retorno = jsObject()
			retorno("status") = "error"
			retorno("mensagem") = "Convite não encontrado!"
			retorno.Flush
		
		end if
	else
	
		Set retorno = jsObject()
			retorno("status") = "error"
			retorno("mensagem") = "Convite não encontrado!"
			retorno.Flush
	
	end if
	
	conexao.Close
	set conexao = nothing
	
EXIT FUNCTION
END FUNCTION




FUNCTION obterIngressosAdmin(conexao)
	
	idConsultor = LimpaLixo(ExpurgaApostrofe(REQUEST("idConsultor")))
	codigoIngresso = LimpaLixo(ExpurgaApostrofe(REQUEST("codigoIngresso")))
	
	codigoIngresso = trim(codigoIngresso)
	idConsultor = trim(idConsultor)
	
	
		if idConsultor <> "" AND codigoIngresso <> "" then
			query = "SELECT * FROM [hinode].[dbo].[view_consultor] A "&_
					"INNER JOIN [hinode].[dbo].[tbl_mw_ingressos] B ON A.integracao = B.idConsultor_ingresso "&_
					"WHERE B.idConsultor_ingresso = '"& idConsultor &"' "&_
					"AND B.codigo_ingresso = '"& codigoIngresso &"' "
			
		elseif idConsultor <> "" then
			query = "SELECT * FROM [hinode].[dbo].[view_consultor] A "&_
					"INNER JOIN [hinode].[dbo].[tbl_mw_ingressos] B ON A.integracao = B.idConsultor_ingresso "&_			
					"WHERE B.idConsultor_ingresso = '"& idConsultor &"' "
			
		elseif codigoIngresso <> "" then
			query = "SELECT * FROM [hinode].[dbo].[view_consultor] A "&_
					"INNER JOIN [hinode].[dbo].[tbl_mw_ingressos] B ON A.integracao = B.idConsultor_ingresso "&_	
					"WHERE B.codigo_ingresso = '"& codigoIngresso &"' "
			
		else
			query = "SELECT * FROM [hinode].[dbo].[view_consultor] A "&_
					"INNER JOIN [hinode].[dbo].[tbl_mw_ingressos] B ON A.integracao = B.idConsultor_ingresso "&_
					"WHERE B.status_ingresso = 1"
			
		end if

		
		Set adoRecordset = Server.CreateObject("ADODB.Recordset")
		adoRecordset.Open query, conexao
		
		if(not adoRecordset.eof) then	
			
			Set retorno = jsArray()
	
			do while not adoRecordset.eof
				
				Set retorno(Null) = jsObject()
				retorno(Null)("codigo_ingresso") = adoRecordset("codigo_ingresso")
				retorno(Null)("nome_consultor") = adoRecordset("nome")
				retorno(Null)("status_ingresso") = adoRecordset("status_ingresso")
				retorno(Null)("idConsultor_ingresso") = adoRecordset("idConsultor_ingresso")
				retorno(Null)("data") = adoRecordset("dataAtivacao_ingresso")
				
				adoRecordset.movenext
			loop 
			
			retorno.Flush
		else
		
			Set retorno = jsObject()
			retorno("status") = "error"
			retorno("mensagem") = "Convite não encontrado!"
			retorno.Flush
		
		end if
	
	
	conexao.Close
	set conexao = nothing
	
EXIT FUNCTION
END FUNCTION



FUNCTION desativarIngresso(conexao)
	
	codigoIngresso = LimpaLixo(ExpurgaApostrofe(REQUEST("codigoIngresso")))
	
	
		if codigoIngresso <> "" then
			query = " UPDATE [hinode].[dbo].[tbl_mw_ingressos] SET " &_
					" idPatrocinador_ingresso='', " &_
					" idConsultor_ingresso='', " &_
                    " status_ingresso = 0 "&_
					" WHERE codigo_ingresso = '"& codigoIngresso &"' "	
					
			conexao.Execute(query)
				
			Set retorno = jsObject()
			retorno("status") = "success"
			retorno.Flush
			
		else
			
			Set retorno = jsObject()
			retorno("status") = "error"
			retorno.Flush
			
		end if
	
	
	conexao.Close
	set conexao = nothing
	
EXIT FUNCTION
END FUNCTION



'#### OS METODOS ABAIXO SAO DO CONTROLE DE INGRESSOS VIA QR CODE
FUNCTION limparIngresso(conexao)
	
	idIngresso = LimpaLixo(ExpurgaApostrofe(REQUEST("idIngresso")))
	idAdmin = LimpaLixo(ExpurgaApostrofe(REQUEST("idAdmin")))
	
	if idAdmin = "05006444" then
				
		query = "UPDATE [hinode].[dbo].[tbl_mw_ingressos] SET " &_
		" utilizado_ingresso = '0'," &_
		" idConsultor_ingresso = NULL," &_
		" status_ingresso = '0'," &_
		" dataAtivacao_ingresso = NULL" &_
		" where id_ingresso = '" & idIngresso &"'"

		conexao.Execute(query)
		
		Set retorno = jsObject()
		retorno("status") = "success"
		retorno.Flush
		
	else
		
		Set retorno = jsObject()
		retorno("status") = "error"
		retorno.Flush
		
	end if

EXIT FUNCTION
END FUNCTION

FUNCTION obterTodosIngressos(conexao)
	
	query = "SELECT a.codigo_ingresso, a.status_ingresso, a.idPatrocinador_ingresso, a.idConsultor_ingresso, b.nome, b.integracao FROM [hinode].[dbo].[tbl_mw_ingressos] a INNER JOIN [hinode].[dbo].[view_consultor] b ON a.idPatrocinador_ingresso = b.integracao"
	
	strsql = " select Ciclo=B.nome_ciclo,a.integracao,a.nome,a.dt_cad,a.cupom,B.nome_ciclo "&_
      " from rede_campanha_patrocinios a inner join rede_parametros b on a.inicio=b.inicio "&_
      " where a.superior_integracao='"&vidconsultor&"'"&_
      " ORDER BY B.CICLO,a.dt_cad "
	
	Set adoRecordset = Server.CreateObject("ADODB.Recordset")
	adoRecordset.Open query, conexao
	
	if(not adoRecordset.eof) then	
		
		Set retorno = jsArray()

		do while not adoRecordset.eof
		
			Set retorno(Null) = jsObject()
			retorno(Null)("codigo_ingresso") = adoRecordset("codigo_ingresso")
			retorno(Null)("status_ingresso") = adoRecordset("status_ingresso")
			retorno(Null)("consultor") = adoRecordset("idConsultor_ingresso")
			retorno(Null)("nome") = adoRecordset("nome")
			
			adoRecordset.movenext
		loop 
		
		retorno.Flush
		
	end if
	
	conexao.Close
	set conexao = nothing
	
EXIT FUNCTION
END FUNCTION

FUNCTION obterIngressosProprios(conexao)
	
	idPatrocinador = LimpaLixo(ExpurgaApostrofe(REQUEST("id")))

	query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingressos] WHERE idConsultor_ingresso = '" & idPatrocinador & "' AND idPatrocinador_ingresso = '" & idPatrocinador & "'"
	
	Set adoRecordset = Server.CreateObject("ADODB.Recordset")
	adoRecordset.Open query, conexao
	
	if(not adoRecordset.eof) then	
		
		Set retorno = jsArray()
		
		Set retorno(Null) = jsObject()
		retorno(Null)("tipo") = "consultor"
		
		do while not adoRecordset.eof
		
			Set retorno(Null) = jsObject()
			retorno(Null)("id_ingresso") = adoRecordset("id_ingresso")
			retorno(Null)("codigo_ingresso") = adoRecordset("codigo_ingresso")
			retorno(Null)("idPatrocinador_ingresso") = adoRecordset("idPatrocinador_ingresso")
			retorno(Null)("idConsultor_ingresso") = adoRecordset("idConsultor_ingresso")
			retorno(Null)("status_ingresso") = adoRecordset("status_ingresso")
			
			query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & adoRecordset("idPatrocinador_ingresso") & "'"
			Set adoPatrocinador = Server.CreateObject("ADODB.Recordset")
			adoPatrocinador.Open query, conexao

			retorno(Null)("nome") = adoPatrocinador("nome")
			
			adoRecordset.movenext
		loop 
		
		retorno.Flush
	
	else
	
		Set retorno = jsObject()
		retorno("tipo") = "none"
		retorno.Flush
		
	end if
	
	conexao.Close
	set conexao = nothing
	
EXIT FUNCTION
END FUNCTION

FUNCTION obtemIngressoPatrocinador(conexao)
	
	idPatrocinador = LimpaLixo(ExpurgaApostrofe(REQUEST("idPatrocinador")))
	
	query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingresso_patrocinador] WHERE id_patrocinador = '" & idPatrocinador & "'"
	Set adoRecordset = Server.CreateObject("ADODB.Recordset")
	
	adoRecordset.Open query, conexao
	
	'response.write(query)

	if(not adoRecordset.eof) then

		Set retorno = jsObject()
		retorno("id") = adoRecordset("id")
		
		query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & idPatrocinador & "'"
		Set adoConsultor = Server.CreateObject("ADODB.Recordset")
		adoConsultor.Open query, conexao
		
		retorno("nome") = adoConsultor("nome")

		query = "SELECT * FROM [hinode].[dbo].[view_DadosIngresso] where integracao = '" & idPatrocinador & "'"

		Set adoEndereco = Server.CreateObject("ADODB.Recordset")
		adoEndereco.Open query, conexao
		
		retorno("cidade") = adoEndereco("cidade")
		retorno("estado") = adoEndereco("estado")
		retorno("segundoTitular") = adoEndereco("segundo_titular")
		
		retorno("status") = "success"
		
		retorno.Flush
		
	else
		Set retorno = jsObject()
		
		retorno("message") = "Este ingresso não pertence ao Consultor enviado!"
		retorno("status") = "error"
		
		retorno.Flush
	end if
	
	'response.write toJSON(retorno)
	  
	conexao.Close
	set conexao = nothing
	
EXIT FUNCTION
END FUNCTION

FUNCTION verificaIngressoPatrocinador(conexao)
	
	idPatrocinador = LimpaLixo(ExpurgaApostrofe(REQUEST("idPatrocinador")))
	
	query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingresso_patrocinador] WHERE id_patrocinador = '" & idPatrocinador & "'"
	Set adoRecordset = Server.CreateObject("ADODB.Recordset")
	
	adoRecordset.Open query, conexao
	
	Set retorno = jsArray()
	
	if not adoRecordset.eof then
		
		Set retorno = jsObject()
		retorno("status") = "success"
		retorno.Flush
		
	else
		
		Set retorno = jsObject()
		retorno("status") = "error"
		retorno.Flush
	
	end if

	conexao.Close
	set conexao = nothing

EXIT FUNCTION
END FUNCTION

FUNCTION geraIngressoPatrocinador(conexao)
	
	dados = "5448883;60|71924;10|1476;7|668940;5|19;15|670979;5|5424865;5|766540;5|149729;5|5382087;5|740551;5|738937;5|5407731;10|5407715;10|20;15|43;5|147379;5|5374431;5|731410;15|11412;5|5424881;5|5447733;5|770806;5|739155;5|5023130;5|5371054;5|51;5|42454;15|5006655;10|5090797;20|780529;10|27;15|1450;25|5402287;5|749290;20|5372460;15|7667;5|763550;20|77635;10|763569;20|786285;10|5411597;10|763251;5|5368023;10|759834;5|772887;10|5354810;20|761459;25|725440;5|5401102;10|5375910;5|5373130;10|70601;20|5394440;5|5009303;10|780406;15|69429;5|780545;5|74680;5|5398038;5|5441770;5|5410316;5|71702;5|5372410;5|5048142;5|5371003;10|851331;5|5409585;1|5043560;10|21266;5|5193294;5|90560;5|5327053;5|355320;5|749775;20|79345;5|197691;5|735138;5|82547;5|72156;5|5444225;5|76781;5|403635;5|850523;5|5440531;5|83164;20|5384250;5|5031842;5|112633;5|759551;5|725319;5|727402;15|711785;2|715760;1|49971;5|765222;10|49963;5|5403394;5|74633;5|727816;6|706257;5|69408;5|76148;5|5424179;10|82738;5|70191;5|5401639;5|71261;10|85716;5|79415;5|797603;5|70136;10|5385309;20|5110735;20|5446466;5|78876;10|5384680;15|5446167;10|70444;10|110597;20|77779;5|5410148;5|82902;15|72054;10|87164;5|5442959;5|5449499;5|72708;5|5424224;5|70470;5|5384031;5|5368023;5|85847;40|797590;5"
	
	arrayDados = Split(dados,"|")
	
	For i = 0 to Ubound(arrayDados)
		
		infoIngresso = Split(arrayDados(i), ";")
		
		idPatrocinador = infoIngresso(0)

		if Len(idPatrocinador) < 8 then
			
			resto = 7 - Len(idPatrocinador)
			
			zeroEsquerda = ""
			
			For j = 0 to resto
				
				zeroEsquerda = zeroEsquerda & "0"
				
			Next
			
		end if
		
		idPatrocinador = zeroEsquerda & idPatrocinador
		
		query = "INSERT INTO [hinode].[dbo].[tbl_mw_ingresso_patrocinador] " &_
		"(id_patrocinador, status)" &_
		" VALUES ('" & idPatrocinador & "', '0');"
		
		conexao.Execute(query)
		
	Next
	
	conexao.Close
	set conexao = nothing	
	
EXIT FUNCTION
END FUNCTION

FUNCTION confirmarEntrada(conexao)
	
	senha = LimpaLixo(ExpurgaApostrofe(REQUEST("senha")))
	idIngresso = LimpaLixo(ExpurgaApostrofe(REQUEST("idIngresso")))
	
	if senha = "3545" then
				
		query = "UPDATE [hinode].[dbo].[tbl_mw_ingressos] set " &_
		" utilizado_ingresso = '1'" & _
		" where id_ingresso = '" & idIngresso &"'"
	
		conexao.Execute(query)
		
		Set retorno = jsObject()
		retorno("status") = "success"
		retorno.Flush
		
	else
		
		Set retorno = jsObject()
		retorno("status") = "Senha incorreta!"
		retorno.Flush
		
	end if

EXIT FUNCTION
END FUNCTION

FUNCTION confirmarEntradaPatrocinador(conexao)
	
	senha = LimpaLixo(ExpurgaApostrofe(REQUEST("senha")))
	idIngresso = LimpaLixo(ExpurgaApostrofe(REQUEST("idIngresso")))
	
	if senha = "3545" then
				
		query = "UPDATE [hinode].[dbo].[tbl_mw_ingresso_patrocinador] set " &_
		" status = '1'" & _
		" where id = '" & idIngresso &"'"
	
		conexao.Execute(query)
		
		Set retorno = jsObject()
		retorno("status") = "success"
		retorno.Flush
		
	else
		
		Set retorno = jsObject()
		retorno("status") = "Senha incorreta!"
		retorno.Flush
		
	end if

EXIT FUNCTION
END FUNCTION

FUNCTION geraIngressos(conexao)
		
	idAdmin = LimpaLixo(ExpurgaApostrofe(REQUEST("idAdmin")))

	if idAdmin = "05006444" then
		
		idPatrocinador = LimpaLixo(ExpurgaApostrofe(REQUEST("id")))
		quantidade = LimpaLixo(ExpurgaApostrofe(REQUEST("quantidade")))
		
		if Len(idPatrocinador) < 8 then
			
			resto = 7 - Len(idPatrocinador)
			
			zeroEsquerda = ""
			
			For j = 0 to resto
				
				zeroEsquerda = zeroEsquerda & "0"
				
			Next
			
		end if
		
		idPatrocinador = zeroEsquerda & idPatrocinador
		
		For k = 1 To quantidade
		
			codigoIngresso = geraNumeroIngresso(conexao)
			
			query = "INSERT INTO [hinode].[dbo].[tbl_mw_ingressos] "&_
			"(codigo_ingresso, idPatrocinador_ingresso, status_ingresso)" &_
			" VALUES ('" & codigoIngresso & "', '" & idPatrocinador & "','0');"
			
			conexao.Execute(query)
			
		Next

		
		Set retorno = jsObject()
		retorno("status") = "success"
		retorno.Flush
		
		conexao.Close
		set conexao = nothing
		
	else
		
		Set retorno = jsObject()
		retorno("status") = "error"
		retorno.Flush
		
	end if
	
EXIT FUNCTION
END FUNCTION

FUNCTION geraIngressosBackup(conexao)
	
	'dados = "5448883;60|71924;10|1476;7|668940;5|19;15|670979;5|5424865;5|766540;5|149729;5|5382087;5|740551;5|738937;5|5407731;10|5407715;10|20;15|43;5|147379;5|5374431;5|731410;15|11412;5|5424881;5|5447733;5|770806;5|739155;5|5023130;5|5371054;5|51;5|42454;15|5006655;10|5090797;20|780529;10|27;15|1450;25|5402287;5|749290;20|5372460;15|7667;5|763550;20|77635;10|763569;20|786285;10|5411597;10|763251;5|5368023;10|759834;5|772887;10|5354810;20|761459;25|725440;5|5401102;10|5375910;5|5373130;10|70601;20|5394440;5|5009303;10|780406;15|69429;5|780545;5|74680;5|5398038;5|5441770;5|5410316;5|71702;5|5372410;5|5048142;5|5371003;10|851331;5|5409585;1|5043560;10|21266;5|5193294;5|90560;5|5327053;5|355320;5|749775;20|79345;5|197691;5|735138;5|82547;5|72156;5|5444225;5|76781;5|403635;5|850523;5|5440531;5|83164;20|5384250;5|5031842;5|112633;5|759551;5|725319;5|727402;15|711785;2|715760;1|49971;5|765222;10|49963;5|5403394;5|74633;5|727816;6|706257;5|69408;5|76148;5|5424179;10|82738;5|70191;5|5401639;5|71261;10|85716;5|79415;5|797603;5|70136;10|5385309;20|5110735;20|5446466;5|78876;10|5384680;15|5446167;10|70444;10|110597;20|77779;5|5410148;5|82902;15|72054;10|87164;5|5442959;5|5449499;5|72708;5|5424224;5|70470;5|5384031;5|5368023;5|85847;40|797590;5"
		
	'dados = "05006655;5|05009303;5"

	arrayDados = Split(dados,"|")
	
	For i = 0 to Ubound(arrayDados)
		
		response.write(arrayDados(i))
		
		infoIngresso = Split(arrayDados(i), ";")
		
		idPatrocinador = infoIngresso(0)
		quantidade = infoIngresso(1)
		
		if Len(idPatrocinador) < 8 then
			
			resto = 7 - Len(idPatrocinador)
			
			zeroEsquerda = ""
			
			For j = 0 to resto
				
				zeroEsquerda = zeroEsquerda & "0"
				
			Next
			
		end if
		
		idPatrocinador = zeroEsquerda & idPatrocinador
		
		For k = 1 To quantidade
		
			codigoIngresso = geraNumeroIngresso(conexao)
			
			query = "INSERT INTO [hinode].[dbo].[tbl_mw_ingressos] "&_
			"(codigo_ingresso, idPatrocinador_ingresso, status_ingresso)" &_
			" VALUES ('" & codigoIngresso & "', '" & idPatrocinador & "','0');"
			
			conexao.Execute(query)
			
		Next
		
	Next
	
	conexao.Close
	set conexao = nothing

	
EXIT FUNCTION
END FUNCTION


FUNCTION geraNumeroIngresso(conexao)
	
	retorno = RandomString() & " " & RandomString() & " " & RandomString()
	
	query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingressos] WHERE codigo_ingresso = '" & retorno & "'"
	Set adoRecordset = Server.CreateObject("ADODB.Recordset")
	
	adoRecordset.Open query, conexao
	
	if not adoRecordset.eof then
		
		encontrou = false
		
		do while encontrou = false
			
			retorno = RandomString() & " " & RandomString() & " " & RandomString()
			
			query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingressos] WHERE codigo_ingresso = '" & retorno & "'"
			Set adoRecordsetInterno = Server.CreateObject("ADODB.Recordset")
			
			adoRecordsetInterno.Open query, conexao
			
			if adoRecordsetInterno.eof then
			
				encontrou = true
				
			end if
			
		loop
	
	end if
	
	geraNumeroIngresso = retorno
		
EXIT FUNCTION
END FUNCTION

FUNCTION RandomString()

    Randomize()

    dim CharacterSetArray
    CharacterSetArray = Array(_
        Array(4, "ABCDEFGHIJKLMNOPQRSTUVXYWZ0123456789"), _
        Array(1, "0123456789") _
    )

    dim i
    dim j
    dim Count
    dim Chars
    dim Index
    dim Temp

    for i = 0 to UBound(CharacterSetArray)

        Count = CharacterSetArray(i)(0)
        Chars = CharacterSetArray(i)(1)

        for j = 1 to Count

            Index = Int(Rnd() * Len(Chars)) + 1
            Temp = Temp & Mid(Chars, Index, 1)

        next

    next

    dim TempCopy

    do until Len(Temp) = 0

        Index = Int(Rnd() * Len(Temp)) + 1
        TempCopy = TempCopy & Mid(Temp, Index, 1)
        Temp = Mid(Temp, 1, Index - 1) & Mid(Temp, Index + 1)

    loop

    RandomString = TempCopy

END FUNCTION

FUNCTION obterFranquias(conexao)
		
query = "SELECT * FROM [hinode].[dbo].[view_Franquia] WHERE cidade not in('BARUERI', 'SANTANA DE PARNAÍBA') ORDER BY estado,cidade ASC "
	Set adoRecordset = Server.CreateObject("ADODB.Recordset")
	
	adoRecordset.Open query, conexao
	
	Set retorno = jsArray()
	
	do while not adoRecordset.eof
	
		Set retorno(Null) = jsObject()
		
		retorno(Null)("estado") = adoRecordset("estado")
		retorno(Null)("cidade") = adoRecordset("cidade")
		retorno(Null)("investidor") = adoRecordset("investidor")
		retorno(Null)("endereco") = adoRecordset("endereco")
		retorno(Null)("numero") = adoRecordset("numero")
		retorno(Null)("telefone") = adoRecordset("Fone1")
		retorno(Null)("email") = adoRecordset("email")
		
		adoRecordset.movenext
		
	loop 
	
	retorno.Flush
		  
	conexao.Close
	set conexao = nothing
	
EXIT FUNCTION
END FUNCTION

FUNCTION confRecibo(conn1)
	
	ciclo = LimpaLixo(ExpurgaApostrofe(REQUEST("ciclo")))
	redeDestinatario = LimpaLixo(ExpurgaApostrofe(REQUEST("rede_destinatario")))
	
	if len(ciclo)=0 then ciclo=-1

	if len(redeDestinatario)=0 then
		destinatario	=replace(request("chave"),"'","")
		integracao		=replace(request("chave2"),"'","")
		hierarquia		=replace(request("chave3"),"'","")
		if len(hierarquia)=0 then hierarquia=-1
	
		strsql="select destinatario from [hinode].[dbo].[view_consultor] " & _
			"where destinatario='" & destinatario & "' and integracao='" & integracao & "' and hierarquia=" & hierarquia
			'Response.Write strsql
		set adoRs1=conn1.execute(strsql)
		if adors1.eof then
			Response.Write "Erro ao acessar a página."
			Response.end
		else
			redeDestinatario=adors1("destinatario")
		end if
		set adors1=nothing
	end if


	strSql = "exec [hinode].[dbo].[rede_Recibo_sp] '" & redeDestinatario & "'," & ciclo
	set adoRs1=conn1.Execute(strSql)
	'response.write strSql
	
	d= DAY(date)
	m= MONTH(date)
	a= YEAR(date)
	
	if not adoRs1.eof then
		valor = adors1("valor")
		inicio = adors1("inicio")
		fim = adors1("fim")
		nomecons    = adoRs1("nome")
		idCons      = adors1("integracao")
		cpfcons     = Trim(adoRs1("cgc_cpf"))
		rgcons      = adoRs1("inscricao_rg")
		valort      = formatnumber(adoRs1("valor"),2)
		valorir     = formatnumber(adoRs1("irrf"),2)
		valoroutros = formatnumber(adoRs1("outros"),2)
		valorliq    = formatnumber(adoRs1("liquido"),2)
		adoRs1.movenext
	end if
	
	
	textorecibo = "Recebi de HINODE FRANQUIAS  E LICEN&Ccedil;AS LTDA, estabelecida &agrave; Estrada da Represinha, n&ordm; 85, Itapecerica da Serra," &_
		" S&atilde;o Paulo, inscrita no C.N.P.J sob o n&ordm; 17.086.101/0001-20,a import&acirc;ncia de R$  "&valor&", referente a servi&ccedil;os prestados na divulga&ccedil;&atilde;o " &_
		"da &ldquo;Marca Hinode&rdquo;, no per&iacute;odo de "&inicio&" a "&fim&",  sem v&iacute;nculo empregat&iacute;cio."
	
	if m = 1 then mm = "janeiro"
	if m = 2 then mm = "fevereiro"
	if m = 3 then mm = "março"
	if m = 4 then mm = "abril"
	if m = 5 then mm = "maio"
	if m = 6 then mm = "junho"
	if m = 7 then mm = "julho"
	if m = 8 then mm = "agosto"
	if m = 9 then mm = "setembro"
	if m = 10 then mm = "outubro"
	if m = 11 then mm = "novembro"
	if m = 12 then mm = "dezembro"
	
	
	Dim vmsg
	
	vMsg = "<table width='650px' border='0' align='center' cellpadding='0' cellspacing='0' border='0'>"&_
            "<table width='650px' border='0' align='center' cellpadding='0' cellspacing='0'>"&_
                    "<tr><td align='center'><font face='Agency FB' size='6'>HINODE FRANQUIAS E LICEN&Ccedil;AS LTDA EPP</font></td></tr>"&_
                    "<tr><td align='center'><font face='Agency FB' size='5'>C.N.P.J 17.086.101/0001-20</font></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td align='center'><font face='Agency FB' size='4'><b>RECIBO PARA PAGAMENTO DE SERVI&Ccedil;OS DE DIVULGA&Ccedil;&Atilde;O DA MARCA HINODE</b></font></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>ID:"&idCons&"</font></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>"&nomecons&"</font></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>CPF: "&cpfcons&"</font></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td align=center><font face='Agency FB' size='6'><b>RECIBO</b></font></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>VALOR TOTAL&nbsp;&nbsp;R$&nbsp;"&valort&"</font></td>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>IRRF&nbsp;&nbsp;R$&nbsp;"&valorir&"</font></td>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>OUTROS&nbsp;&nbsp;R$&nbsp;"&valoroutros&"</font></td>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>VALOR LIQU&Iacute;DO&nbsp;&nbsp;R$&nbsp;"&valorliq&"</font></td>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td align=justify><font face='Agency FB' size='4'>"&textorecibo&"</font></td>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>S&atilde;o Paulo, "&d&" de "&mm&" de "&a&"</font></td>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>______________________________</font></td>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>Nome: "&nomecons&"</font></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>CPF: "&cpfcons&"</font></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>RG: "&rgcons&"</font></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                "</table>"&_         
    	"</table>"
	
	
	if Len(cpfcons) <= 12 then
	
		ipcons = Request.ServerVariables("REMOTE_ADDR")
		
strSql = "exec [hinode].[dbo].[rede_ConfirmarReciboBonus_sp] '" & idCons & "'," & ciclo & ",'"&ipcons&"'"
set adoRs1=conn1.Execute(strSql)
	
		
		servidor="smtp.hinode.com.br"

		'de  ="naoresponda@hinode.com.br"

		'user="naoresponda@hinode.com.br"

		'pass="druida@107"
		
		
		
		de  ="registro@hinode.com.br"

		user="registro@hinode.com.br"

		pass="hinode@2012"
		
		para = "suporte@mediaw.com.br"

		assunto	= "CONFIRMAÇÃO DE RECIBO"

				'Set Mail = Server.CreateObject("CDO.Message") 
				
				'			   Set MailCon = Server.CreateObject ("CDO.Configuration") 
				
				'MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = servidor
				'MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 587 
				'MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
				'MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 
				
				'			   if len(user)>0 then
				
				'MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
				'MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = user
				'MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = pass

               'end if

               'MailCon.Fields.update
			   
			   'Set Mail.Configuration = MailCon 
				'   Mail.From = de
				   'Mail.CC="registro@hinode.com.br"
				'   Mail.To = para
	            '   Mail.Subject = assunto
				   'id_Body	="Esse é um teste de email."
                '   Mail.HTMLBody=vMsg
	            '   Mail.Fields.update
	            '   Mail.Send
	          ' Set Mail = Nothing 
	          ' Set MailCon = Nothing
			   
			   
			   response.Write("Confirmação realizada com sucesso, favor imprimir o seu recíbo")
    else
	          response.Write("Caro consultor(a), favor enviar a nota-fiscal para o e-mail notafiscal@hinode.com.br.")
	
	end if			   
			   
	conn1.Close
	set conn1 = nothing
	
EXIT FUNCTION
END FUNCTION


FUNCTION confRecibo_lideranca(conn1)
	
	ciclo = LimpaLixo(ExpurgaApostrofe(REQUEST("ciclo")))
	redeDestinatario = LimpaLixo(ExpurgaApostrofe(REQUEST("rede_destinatario")))
	
	if len(ciclo)=0 then ciclo=-1

	if len(redeDestinatario)=0 then
		destinatario	=replace(request("chave"),"'","")
		integracao		=replace(request("chave2"),"'","")
		hierarquia		=replace(request("chave3"),"'","")
		if len(hierarquia)=0 then hierarquia=-1
	
		strsql="select destinatario from [hinode].[dbo].[view_consultor] " & _
			"where destinatario='" & destinatario & "' and integracao='" & integracao & "' and hierarquia=" & hierarquia
			'Response.Write strsql
		set adoRs1=conn1.execute(strsql)
		if adors1.eof then
			Response.Write "Erro ao acessar a página."
			Response.end
		else
			redeDestinatario=adors1("destinatario")
		end if
		set adors1=nothing
	end if


	strSql = "exec [hinode].[dbo].[rede_Recibo_lideranca_sp] '" & redeDestinatario & "'," & ciclo
	set adoRs1=conn1.Execute(strSql)
	'response.write strSql
	
	d= DAY(date)
	m= MONTH(date)
	a= YEAR(date)
	
	if not adoRs1.eof then
		valor = adors1("valor")
		inicio = adors1("inicio")
		fim = adors1("fim")
		nomecons    = adoRs1("nome")
		idCons      = adors1("integracao")
		cpfcons     = Trim(adoRs1("cgc_cpf"))
		rgcons      = adoRs1("inscricao_rg")
		valort      = formatnumber(adoRs1("valor"),2)
		valorir     = formatnumber(adoRs1("irrf"),2)
		valoroutros = formatnumber(adoRs1("outros"),2)
		valorliq    = formatnumber(adoRs1("liquido"),2)
		adoRs1.movenext
	end if
	
	
	textorecibo = "Recebi de HINODE FRANQUIAS  E LICEN&Ccedil;AS LTDA, estabelecida &agrave; Estrada da Represinha, n&ordm; 85, Itapecerica da Serra," &_
		" S&atilde;o Paulo, inscrita no C.N.P.J sob o n&ordm; 17.086.101/0001-20,a import&acirc;ncia de R$  "&valor&", referente a servi&ccedil;os prestados na divulga&ccedil;&atilde;o " &_
		"da &ldquo;Marca Hinode&rdquo;, no per&iacute;odo de "&inicio&" a "&fim&",  sem v&iacute;nculo empregat&iacute;cio."
	
	if m = 1 then mm = "janeiro"
	if m = 2 then mm = "fevereiro"
	if m = 3 then mm = "março"
	if m = 4 then mm = "abril"
	if m = 5 then mm = "maio"
	if m = 6 then mm = "junho"
	if m = 7 then mm = "julho"
	if m = 8 then mm = "agosto"
	if m = 9 then mm = "setembro"
	if m = 10 then mm = "outubro"
	if m = 11 then mm = "novembro"
	if m = 12 then mm = "dezembro"
	
	
	Dim vmsg
	
	vMsg = "<table width='650px' border='0' align='center' cellpadding='0' cellspacing='0' border='0'>"&_
            "<table width='650px' border='0' align='center' cellpadding='0' cellspacing='0'>"&_
                    "<tr><td align='center'><font face='Agency FB' size='6'>HINODE FRANQUIAS E LICEN&Ccedil;AS LTDA EPP</font></td></tr>"&_
                    "<tr><td align='center'><font face='Agency FB' size='5'>C.N.P.J 17.086.101/0001-20</font></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td align='center'><font face='Agency FB' size='4'><b>RECIBO PARA PAGAMENTO DE SERVI&Ccedil;OS DE DIVULGA&Ccedil;&Atilde;O DA MARCA HINODE</b></font></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>ID:"&idCons&"</font></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>"&nomecons&"</font></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>CPF: "&cpfcons&"</font></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td align=center><font face='Agency FB' size='6'><b>RECIBO</b></font></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>VALOR TOTAL&nbsp;&nbsp;R$&nbsp;"&valort&"</font></td>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>IRRF&nbsp;&nbsp;R$&nbsp;"&valorir&"</font></td>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>OUTROS&nbsp;&nbsp;R$&nbsp;"&valoroutros&"</font></td>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>VALOR LIQU&Iacute;DO&nbsp;&nbsp;R$&nbsp;"&valorliq&"</font></td>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td align=justify><font face='Agency FB' size='4'>"&textorecibo&"</font></td>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>S&atilde;o Paulo, "&d&" de "&mm&" de "&a&"</font></td>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>______________________________</font></td>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>Nome: "&nomecons&"</font></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>CPF: "&cpfcons&"</font></td></tr>"&_
                    "<tr><td align=left><font face='Agency FB' size='4'>RG: "&rgcons&"</font></td></tr>"&_
                    "<tr><td><br /></td></tr>"&_
                "</table>"&_         
    	"</table>"
	
	
'	if Len(cpfcons) <= 12 then
	
		ipcons = Request.ServerVariables("REMOTE_ADDR")
		
strSql = "exec [hinode].[dbo].[rede_ConfirmarReciboBonus_lideranca_sp] '" & idCons & "'," & ciclo & ",'"&ipcons&"'"
set adoRs1=conn1.Execute(strSql)
	
		
		servidor="smtp.hinode.com.br"

		'de  ="naoresponda@hinode.com.br"

		'user="naoresponda@hinode.com.br"

		'pass="druida@107"
		
		
		
		de  ="registro@hinode.com.br"

		user="registro@hinode.com.br"

		pass="hinode@2012"
		
		para = "suporte@mediaw.com.br"

		assunto	= "CONFIRMAÇÃO DE RECIBO"

				'Set Mail = Server.CreateObject("CDO.Message") 
				
				'			   Set MailCon = Server.CreateObject ("CDO.Configuration") 
				
				'MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = servidor
				'MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 587 
				'MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
				'MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 
				
				'			   if len(user)>0 then
				
				'MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
				'MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = user
				'MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = pass

               'end if

               'MailCon.Fields.update
			   
			   'Set Mail.Configuration = MailCon 
				'   Mail.From = de
				   'Mail.CC="registro@hinode.com.br"
				'   Mail.To = para
	            '   Mail.Subject = assunto
				   'id_Body	="Esse é um teste de email."
                '   Mail.HTMLBody=vMsg
	            '   Mail.Fields.update
	            '   Mail.Send
	          ' Set Mail = Nothing 
	          ' Set MailCon = Nothing
			   
	if Len(cpfcons) <= 12 then
			   response.Write("Confirmação realizada com sucesso, favor imprimir o seu recíbo")
    	else
	          response.Write("Caro consultor(a), favor enviar a nota-fiscal para o e-mail notafiscal@hinode.com.br.")
	
	end if			   
			   
	conn1.Close
	set conn1 = nothing
	
EXIT FUNCTION
END FUNCTION

FUNCTION geraIngresso(conexao)
	
	idIngresso = LimpaLixo(ExpurgaApostrofe(REQUEST("idIngresso")))
	idConsultor = LimpaLixo(ExpurgaApostrofe(REQUEST("idConsultor")))
	
	if idConsultor = "05006444" then
		query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingressos] WHERE id_ingresso = '" & idIngresso & "' AND status_ingresso = '1'"
		
		Set adoRecordset = Server.CreateObject("ADODB.Recordset")
	
		adoRecordset.Open query, conexao
		
		Set retorno = jsObject()
		
		if(not adoRecordset.eof) then
	
			Set retorno(Null) = jsObject()
			retorno(Null)("id_ingresso") = adoRecordset("id_ingresso")
			retorno(Null)("codigo_ingresso") = adoRecordset("codigo_ingresso")
			retorno(Null)("idPatrocinador_ingresso") = adoRecordset("idPatrocinador_ingresso")
			retorno(Null)("idConsultor_ingresso") = adoRecordset("idConsultor_ingresso")
			retorno(Null)("status_ingresso") = adoRecordset("status_ingresso")
			retorno(Null)("dataAtivacao") = adoRecordset("dataAtivacao_ingresso")
			
			query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & adoRecordset("idConsultor_ingresso") & "'"
			Set adoConsultor = Server.CreateObject("ADODB.Recordset")
			adoConsultor.Open query, conexao
			
			retorno(Null)("nome") = adoConsultor("nome")
	
			query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & adoRecordset("idPatrocinador_ingresso") & "'"
			Set adoConsultor = Server.CreateObject("ADODB.Recordset")
			adoConsultor.Open query, conexao
			
			retorno(Null)("ativadoPor") = adoConsultor("nome")
			
			query = "SELECT * FROM [hinode].[dbo].[view_DadosIngresso] where integracao = '" & adoRecordset("idConsultor_ingresso") & "'"
	
			Set adoEndereco = Server.CreateObject("ADODB.Recordset")
			adoEndereco.Open query, conexao
			
			retorno(Null)("cidade") = adoEndereco("cidade")
			retorno(Null)("estado") = adoEndereco("estado")
			retorno(Null)("segundoTitular") = adoEndereco("segundo_titular")
			
			retorno.Flush
			
		else
			retorno("message") = "Este ingresso não pertence ao Consultor enviado!"
			retorno("status") = "error"
			retorno.Flush
		end if
		
		'response.write toJSON(retorno)
		  
		conexao.Close
		set conexao = nothing
	
	else
		query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingressos] WHERE id_ingresso = '" & idIngresso & "' AND idConsultor_ingresso = '" & idConsultor & "' AND status_ingresso = '1'"
		
		Set adoRecordset = Server.CreateObject("ADODB.Recordset")
	
		adoRecordset.Open query, conexao
		
		Set retorno = jsObject()
		
		if(not adoRecordset.eof) then
	
			Set retorno(Null) = jsObject()
			retorno(Null)("id_ingresso") = adoRecordset("id_ingresso")
			retorno(Null)("codigo_ingresso") = adoRecordset("codigo_ingresso")
			retorno(Null)("idPatrocinador_ingresso") = adoRecordset("idPatrocinador_ingresso")
			retorno(Null)("idConsultor_ingresso") = adoRecordset("idConsultor_ingresso")
			retorno(Null)("status_ingresso") = adoRecordset("status_ingresso")
			retorno(Null)("dataAtivacao") = adoRecordset("dataAtivacao_ingresso")
			
			query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & idConsultor & "'"
			Set adoConsultor = Server.CreateObject("ADODB.Recordset")
			adoConsultor.Open query, conexao
			
			retorno(Null)("nome") = adoConsultor("nome")
	
			query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & adoRecordset("idPatrocinador_ingresso") & "'"
			Set adoConsultor = Server.CreateObject("ADODB.Recordset")
			adoConsultor.Open query, conexao
			
			retorno(Null)("ativadoPor") = adoConsultor("nome")
			
			query = "SELECT * FROM [hinode].[dbo].[view_DadosIngresso] where integracao = '" & idConsultor & "'"
	
			Set adoEndereco = Server.CreateObject("ADODB.Recordset")
			adoEndereco.Open query, conexao
			
			retorno(Null)("cidade") = adoEndereco("cidade")
			retorno(Null)("estado") = adoEndereco("estado")
			retorno(Null)("segundoTitular") = adoEndereco("segundo_titular")
			
			retorno.Flush
			
		else
			retorno("message") = "Este ingresso não pertence ao Consultor enviado!"
			retorno("status") = "error"
			retorno.Flush
		end if
		
		'response.write toJSON(retorno)
		  
		conexao.Close
		set conexao = nothing
		
	end if
	
	
EXIT FUNCTION
END FUNCTION

FUNCTION confirmarIngresso(conexao)
	
	idIngresso = LimpaLixo(ExpurgaApostrofe(REQUEST("idIngresso")))
	idConsultor = LimpaLixo(ExpurgaApostrofe(REQUEST("idConsultor")))
	
	query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingressos] WHERE id_ingresso = '" & idIngresso & "' AND idConsultor_ingresso = '" & idConsultor & "'"
	Set adoRecordset = Server.CreateObject("ADODB.Recordset")
	
	adoRecordset.Open query, conexao
	
	Set retorno = jsObject()
	
	if(not adoRecordset.eof) then
		
		data = Now
		
		query = "UPDATE [hinode].[dbo].[tbl_mw_ingressos] set " &_
		" dataAtivacao_ingresso = GETDATE(), " & _
		" status_ingresso = '1'" & _
		" where id_ingresso = '" & idIngresso &"'"
	
		On Error Resume Next 
		conexao.Execute(query)
			
		if err.number <> 0 then
			ErroRt = EnviaNotificaTecnico("" & Err.number & "-" & err.Description, "UpdateIngresso", query)
			retorno("message") = "Technical error!"
			retorno("status") = "error"
			retorno.Flush
		else
			retorno("message") = "Ingresso confirmado com sucesso!"
			retorno("status") = "success"
			retorno.Flush
			
			codigoConvite = adoRecordset("codigo_ingresso")
			idPatrocinador = adoRecordset("idPatrocinador_ingresso")

			query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & idConsultor & "'"
			Set adoConsultor = Server.CreateObject("ADODB.Recordset")
			
			adoConsultor.Open query, conexao
			
			nomeConsultor = adoConsultor("nome")
			emailConsultor = adoConsultor("email")
			
			query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & idPatrocinador & "'"
			Set adoPatrocinador = Server.CreateObject("ADODB.Recordset")
			
			adoPatrocinador.Open query, conexao
			
			nomePatrocinador = adoPatrocinador("nome")
			emailPatrocinador = adoPatrocinador("email")
			
			query = "SELECT * FROM [hinode].[dbo].[view_DadosIngresso] where integracao = '" & idConsultor & "'"

			Set adoEndereco = Server.CreateObject("ADODB.Recordset")
			adoEndereco.Open query, conexao
			
			query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingressos] where id_ingresso = '" & idIngresso & "'"

			Set adoIngresso = Server.CreateObject("ADODB.Recordset")
			adoIngresso.Open query, conexao
			
			dataAtivacao = Split(NOW, " ")
			
			codigoConvite = Replace(codigoConvite, " ", "-")
			
			mensagemConsultor = "<html>" &_
			"<head>" &_
			"<title>ingresso</title>" &_
			"</head>" &_
			"<body bgcolor='#FFFFFF' leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>" &_
			"<table id='Tabela_01' width='595' border='0' cellpadding='0' cellspacing='0'>" &_
			"<tr>" &_
			"<td rowspan='2'>" &_
			"<img src='http://vo.hinode.com.br/vo/ingressos/imagens/ingresso_01.gif' width='269' height='391' alt=''></td>" &_
			"<td>" &_
			"<img src='http://vo.hinode.com.br/vo/ingressos/imagens/ingresso_02.gif' width='326' height='169' alt=''></td>" &_
			"</tr>" &_
			"<tr>" &_
			"<td height='222' align='center' valign='top'><img src='http://chart.apis.google.com/chart?chs=200x200&cht=qr&chld=|0&chl=http://vo.hinode.com.br/vo/ingressos/confirma-ingresso.asp?id=" & codigoConvite & "' /></td>" &_
			"</tr>" &_
			"</table>" &_
			"<br>" &_
			"<table width='595' border='1' bordercolor='black' cellspacing='0' cellpadding='0'>" &_
			"<tr>" &_
			"<td height='220'><table width='100%' style='font-family:Arial, Helvetica;color:black;font-size:12px' border='0' cellspacing='0' cellpadding='0'>" &_
			"<tr>" &_
			"<td width='49%' style='font-size:24px'><b>Nome:</b></td>" &_
			"<td width='51%' style='font-size:24px'><b>ID:</b></td>" &_
			"</tr>" &_
			"<tr>" &_
			"<td height='48' style='font-size:16px' valign='top'>" & nomeConsultor & "</td>" &_
			"<td style='font-size:16px' valign='top'>" & idConsultor  & "</td>" &_
			"</tr>" &_
			"<tr>" &_
			"<td style='font-size:16px'><strong>Segundo Titular:</strong></td>" &_
			"<td style='font-size:16px'><strong>Cidade:</strong></td>" &_
			"</tr>" &_
			"<tr>" &_
			"<td height='31'>" & adoEndereco("segundo_titular")  & "</td>" &_
			"<td>" & adoEndereco("cidade")  & "</td>" &_
			"</tr>" &_
			"<tr>" &_
			"<td style='font-size:16px'><strong>Ativado por:</strong></td>" &_
			"<td style='font-size:16px'><strong>Estado</strong></td>" &_
			"</tr>" &_
			"<tr>" &_
			"<td height='28'>" & nomePatrocinador  & "</td>" &_
			"<td>" & adoEndereco("estado")  & "</td>" &_
			"</tr>" &_
			"<tr>" &_
			"<td style='font-size:16px' ><strong>Data ativação:</strong></td>" &_
			"<td></td>" &_
			"</tr>" &_
			"<tr>" &_
			"<td height='25'>" & dataAtivacao(0)  & "</td>" &_
			"<td></td>" &_
			"</tr>" &_
			"</table></td>" &_
			"</tr>" &_
			"</table>" &_
			"<br>" &_
			"<table width='595' border='0' cellspacing='0' cellpadding='0'>" &_
			"<tr>" &_
			"<td height='73' align='center' bgcolor='#000000' style='font-family:Arial, Helvetica;color:white;font-size:16px'>ESSA ATIVAÇÃO É DE USO PESSOAL E INTRANSFERÍVEL<br>" &_
			"OBRIGATÓRIO A APRESENTAÇÃO NO DIA DO EVENTO</td>" &_
			"</tr>" &_
			"<tr>" &_
			"<td height='26' align='center' bgcolor='#FFFFFF' style='font-family:Arial, Helvetica;color:white;font-size:16px'>&nbsp;</td>" &_
			"</tr>" &_
			"<tr>" &_
			"<td></td>" &_
			"</tr>" &_
			"<tr>" &_
			"<td height='19'><table width='595' border='1' bordercolor='black' cellspacing='0' cellpadding='0'>" &_
			"<tr>" &_
			"<td height='29'><table width='100%' style='font-family:Arial, Helvetica;color:black;font-size:12px' border='0' cellspacing='0' cellpadding='0'>" &_
			"<tr>" &_
			"<td colspan='2' style='font-size:16px'><strong>Informações Importantes:</strong></td>" &_
			"</tr>" &_
			"<tr>" &_
			"<td height='32' colspan='2' style='font-size:14px'><ul>" &_
			"<li>Obrigatório a apresentação deste impresso no dia do evento</li>" &_
			"</ul></td>" &_
			"</tr>" &_
			"</table></td>" &_
			"</tr>" &_
			"</table></td>" &_
			"</tr>" &_
			"<tr>" &_
			"<td></td>" &_
			"</tr>" &_
			"<tr>" &_
			"<td align='right'><img src='http://vo.hinode.com.br/vo/ingressos/imagens/logo.jpg' width='160' height='71'></td>" &_
			"</tr>" &_
			"</table>"

			mensagemPatrocinador = "<table bgcolor='#FFFFFF' border='0' style='padding:20px;'><tbody>" &_
					"<tr><td width='557' height='300' valign='top'><table align='center' border='0' cellpadding='0' cellspacing='0'>" &_
					"<tbody><tr>" &_
					"<td style='padding-bottom:5px;' width='550'>" &_
					"<table style='background-color:#f1e3c8; border:1px solid #e1caa8; width:100%; padding:10px 15px;'>" &_
					"<tbody><tr><td width='180'><img alt='Hinode.com.br' src='http://vo.hinode.com.br/images/logo-hinode-cosmeticos-email.png' /></td>" &_
					"<td align='center' style='font-family:arial; font-size:16px; font-weight:bold; color:#000000;'>CONVENÇÃO NACIONAL HINODE</td></tr>" &_
					"</tbody></table></td></tr><tr><td valign='top' style='padding:15px; border:1px solid #e1caa8;'>" &_
					"<table align='center' border='0' cellpadding='0' cellspacing='0'><tbody>" &_
					"<tr>" &_
					"<td height='159' align='center' style='font:13px Arial;'>" &_
					"<img src='http://vo.hinode.com.br/images/globo.png' width='257' height='191'></td>" &_
					"</tr>" &_
					"<tr>" &_
					"<td height='159' style='font:13px Arial;'><p>Olá, <strong>" & nomePatrocinador & "</strong>!" &_
					"<p>Você acaba de receber a confirmação de ativação do convite de <strong>" & nomeConsultor & "</strong>. <br>" &_
					"<br>Código do convite: " & codigoConvite & "<br>" &_
					"</tr>" &_
					"</tbody></table></td></tr><tr>" &_
					"<td height='90' style='font:12px Arial; background:#f1e3c8; color:#000000; padding:10px 15px'>" &_
					"<table><tbody>" &_
					"<tr><td style='padding-left:10px'><p style='font:12px arial; color:#000000'>Atenciosamente,<br />" &_
					"<span style='font:bold 20px arial;'>HINODE COSMÉTICOS</span><br />" &_
					"<a href='http://www.hinode.com.br' style='color:#000000' target='_blank' title='www.hinode.com.br'>www.hinode.com.br</a>" &_
					"</p></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>"
			
			servidor = "smtp.hinode.com.br"		
			de = "HINODE<registro@hinode.com.br>"
			user = "registro@hinode.com.br"
			pass = "hinode@2012"
			
			paraConsultor = emailConsultor
			paraPatrocinador = emailPatrocinador
			
			Set Mail = Server.CreateObject("CDO.Message") 
			
			Set MailCon = Server.CreateObject ("CDO.Configuration") 
			
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = servidor
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 587 
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 
			
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = user
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = pass
			
			MailCon.Fields.update
			
			Set Mail.Configuration = MailCon 
			Mail.From = de
			Mail.BCC = "rafael@mediaw.com.br"
			Mail.To = LCASE(paraConsultor)
			Mail.Subject = "CONVENÇÃO NACIONAL HINODE - Seu Ingresso foi gerado!"
			Mail.HTMLBody = mensagemConsultor
			Mail.Fields.update
			Mail.Send
			
			Set Mail.Configuration = MailCon 
			Mail.From = de
			Mail.BCC = "rafael@mediaw.com.br"
			Mail.To = LCASE(paraPatrocinador)
			Mail.Subject = "CONVENÇÃO NACIONAL HINODE - Confirmação de convite"
			Mail.HTMLBody = mensagemPatrocinador
			Mail.Fields.update
			Mail.Send
			
			Set Mail = Nothing 
			Set MailCon = Nothing 
			
			storedProcedure = "exec rede_gerar_cupom_2013 '" & idIngresso & "'"
			set adoStoredProcedure = conexao.execute(storedProcedure)
			
		end if
		
	else
		retorno("message") = "Este ingresso não pertence ao Consultor enviado!"
		retorno("status") = "error"
		retorno.Flush
	end if
	
	'response.write toJSON(retorno)
	  
	conexao.Close
	set conexao = nothing
	
EXIT FUNCTION
END FUNCTION


FUNCTION enviarIngresso(conexao)
	
	idIngresso = LimpaLixo(ExpurgaApostrofe(REQUEST("idIngresso")))
	idConsultor = LimpaLixo(ExpurgaApostrofe(REQUEST("idConsultor")))
	idPatrocinador = LimpaLixo(ExpurgaApostrofe(REQUEST("idPatrocinador")))
	
	query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingressos] WHERE id_ingresso = '" & idIngresso & "' AND idPatrocinador_ingresso = '" & idPatrocinador & "'"
	Set adoRecordset = Server.CreateObject("ADODB.Recordset")
	
	adoRecordset.Open query, conexao
	
	Set retorno = jsObject()
	
	if(not adoRecordset.eof) then
		
		if Len(idConsultor) < 8 then
			
			resto = 7 - Len(idConsultor)
			
			zeroEsquerda = ""
			
			For j = 0 to resto
				
				zeroEsquerda = zeroEsquerda & "0"
				
			Next
			
		end if
		
		idConsultor = zeroEsquerda & idConsultor
	
		query = "UPDATE [hinode].[dbo].[tbl_mw_ingressos] set " &_
		" idConsultor_ingresso = '" & idConsultor & "'" & _
		" where id_ingresso = '" & idIngresso &"'"
	
		On Error Resume Next 
		conexao.Execute(query)
			
		if err.number <> 0 then
			ErroRt = EnviaNotificaTecnico("" & Err.number & "-" & err.Description, "UpdateIngresso", query)
			retorno("message") = "Technical error!"
			retorno("status") = "error"
			retorno.Flush
		else
			retorno("message") = "Ingresso enviado com sucesso!"
			retorno("status") = "success"
			retorno.Flush
			
			query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & idConsultor & "'"
			Set adoConsultor = Server.CreateObject("ADODB.Recordset")
			
			adoConsultor.Open query, conexao
			
			nomeConsultor = adoConsultor("nome")
			emailConsultor = adoConsultor("email")
			
			query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & idPatrocinador & "'"
			Set adoPatrocinador = Server.CreateObject("ADODB.Recordset")
			
			adoPatrocinador.Open query, conexao
			
			nomePatrocinador = adoPatrocinador("nome")
			
			vMsg = "<table bgcolor='#FFFFFF' border='0' style='padding:20px;'><tbody>" &_
					"<tr><td width='557' height='300' valign='top'><table align='center' border='0' cellpadding='0' cellspacing='0'>" &_
					"<tbody><tr>" &_
					"<td style='padding-bottom:5px;' width='550'>" &_
					"<table style='background-color:#f1e3c8; border:1px solid #e1caa8; width:100%; padding:10px 15px;'>" &_
					"<tbody><tr><td width='180'><img alt='Hinode.com.br' src='http://vo.hinode.com.br/images/logo-hinode-cosmeticos-email.png' /></td>" &_
					"<td align='center' style='font-family:arial; font-size:16px; font-weight:bold; color:#000000;'>CONVENÇÃO NACIONAL HINODE</td></tr>" &_
					"</tbody></table></td></tr><tr><td valign='top' style='padding:15px; border:1px solid #e1caa8;'>" &_
					"<table align='center' border='0' cellpadding='0' cellspacing='0'><tbody>" &_
					"<tr>" &_
					"<td height='159' align='center' style='font:13px Arial;'>" &_
					"<img src='http://vo.hinode.com.br/images/globo.png' width='257' height='191'></td>" &_
					"</tr>" &_
					"<tr>" &_
					"<td height='159' style='font:13px Arial;'><p>Olá, <strong>" & nomeConsultor & "</strong>!" &_
					"<p>Você acaba de receber de <strong>" & nomePatrocinador & "</strong> um convite para a <em>CONVENÇÃO NACIONAL HINODE</em>. <br>" &_
					"<br>" &_
					"Agora você precisa confirmar o recebimento deste convite, utilize o link abaixo para ser enviado a página de confirmação.</td>" &_
					"</tr>" &_
					"<tr>" &_
					"<td width='520' height='43' align='center' style='font:13px Arial;'><p><a href='https://vo.hinode.com.br/vo/index.asp'>" &_
					"<img src='http://vo.hinode.com.br/images/ir.jpg' width='197' height='43'></a>" &_
					"</td></tr></tbody></table></td></tr><tr>" &_
					"<td height='90' style='font:12px Arial; background:#f1e3c8; color:#000000; padding:10px 15px'>" &_
					"<table><tbody>" &_
					"<tr><td style='padding-left:10px'><p style='font:12px arial; color:#000000'>Atenciosamente,<br />" &_
					"<span style='font:bold 20px arial;'>HINODE COSMÉTICOS</span><br />" &_
					"<a href='http://www.hinode.com.br' style='color:#000000' target='_blank' title='www.hinode.com.br'>www.hinode.com.br</a>" &_
					"</p></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>"
		
			servidor = "smtp.hinode.com.br"		
			de = "HINODE<registro@hinode.com.br>"
			user = "registro@hinode.com.br"
			pass = "hinode@2012"
			para = emailConsultor
			assunto	= "CONVENÇÃO NACIONAL HINODE - Convite"
			
			Set Mail = Server.CreateObject("CDO.Message") 
			
			Set MailCon = Server.CreateObject ("CDO.Configuration") 
			
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = servidor
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 587 
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 
			
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = user
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = pass
			
			MailCon.Fields.update
			
			Set Mail.Configuration = MailCon 
			Mail.From = de
			Mail.BCC = "rafael@mediaw.com.br"
			Mail.To = LCASE(para)
			Mail.Subject = assunto
			Mail.HTMLBody = vMsg
			Mail.Fields.update
			Mail.Send
			Set Mail = Nothing 
			Set MailCon = Nothing 
	
		end if
		
	else
		retorno("message") = "Este ingresso não pertence ao Patrocinador enviado!"
		retorno("status") = "error"
		retorno.Flush
	end if
	
	'response.write toJSON(retorno)
	  
	conexao.Close
	set conexao = nothing
	
EXIT FUNCTION
END FUNCTION

FUNCTION obterConsultor(conexao)
	
	idConsultor = LimpaLixo(ExpurgaApostrofe(REQUEST("idConsultor")))
	
	if Len(idConsultor) < 8 then
			
		resto = 7 - Len(idConsultor)
		
		zeroEsquerda = ""
		
		For j = 0 to resto
			
			zeroEsquerda = zeroEsquerda & "0"
			
		Next
		
	end if

	query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & zeroEsquerda & idConsultor & "'"
	
	QueryToJSON(conexao, query).Flush				  
	
	conexao.Close
	set conexao = nothing
	
EXIT FUNCTION
END FUNCTION

FUNCTION obterIngressosConsultor(conexao)
	
	idPatrocinador = LimpaLixo(ExpurgaApostrofe(REQUEST("id")))

	query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingressos] WHERE idPatrocinador_ingresso = '" & idPatrocinador & "'"
	
	Set adoRecordset = Server.CreateObject("ADODB.Recordset")
	adoRecordset.Open query, conexao
	
	if(not adoRecordset.eof) then	
		
		Set retorno = jsArray()
		
		Set retorno(Null) = jsObject()
		retorno(Null)("tipo") = "patrocinador"
		
		do while not adoRecordset.eof
		
			Set retorno(Null) = jsObject()
			retorno(Null)("id_ingresso") = adoRecordset("id_ingresso")
			retorno(Null)("codigo_ingresso") = adoRecordset("codigo_ingresso")
			retorno(Null)("idPatrocinador_ingresso") = adoRecordset("idPatrocinador_ingresso")
			retorno(Null)("idConsultor_ingresso") = adoRecordset("idConsultor_ingresso")
			retorno(Null)("status_ingresso") = adoRecordset("status_ingresso")
			
			query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & adoRecordset("idConsultor_ingresso") & "'"
			Set adoConsultor = Server.CreateObject("ADODB.Recordset")
			adoConsultor.Open query, conexao

			retorno(Null)("nome") = adoConsultor("nome")
			
			adoRecordset.movenext
		loop 
		
		retorno.Flush
		
	else
		
		query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingressos] WHERE idConsultor_ingresso = '" & idPatrocinador & "'"
		
		Set adoRecordset = Server.CreateObject("ADODB.Recordset")
		adoRecordset.Open query, conexao
		
		if(not adoRecordset.eof) then	
		
			Set retorno = jsArray()
			
			Set retorno(Null) = jsObject()
			retorno(Null)("tipo") = "consultor"
			
			do while not adoRecordset.eof
			
				Set retorno(Null) = jsObject()
				retorno(Null)("id_ingresso") = adoRecordset("id_ingresso")
				retorno(Null)("codigo_ingresso") = adoRecordset("codigo_ingresso")
				retorno(Null)("idPatrocinador_ingresso") = adoRecordset("idPatrocinador_ingresso")
				retorno(Null)("idConsultor_ingresso") = adoRecordset("idConsultor_ingresso")
				retorno(Null)("status_ingresso") = adoRecordset("status_ingresso")
				
				query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & adoRecordset("idPatrocinador_ingresso") & "'"
				Set adoPatrocinador = Server.CreateObject("ADODB.Recordset")
				adoPatrocinador.Open query, conexao

				retorno(Null)("nome") = adoPatrocinador("nome")
				
				adoRecordset.movenext
			loop 
			
			retorno.Flush
		
		else
		
			Set retorno = jsObject()
			retorno("tipo") = "none"
			retorno.Flush
			
		end if
		
	end if 
	
	conexao.Close
	set conexao = nothing
	
EXIT FUNCTION
END FUNCTION


FUNCTION obterIngressosAdministracao(conexao)
	
	idPatrocinador = LimpaLixo(ExpurgaApostrofe(REQUEST("id")))
	
	if Len(idConsultor) < 8 then
			
		resto = 7 - Len(idPatrocinador)
		
		zeroEsquerda = ""
		
		For j = 0 to resto
			
			zeroEsquerda = zeroEsquerda & "0"
			
		Next
		
	end if

	idPatrocinador = zeroEsquerda & idPatrocinador
	
	query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingressos] WHERE idPatrocinador_ingresso = '" & idPatrocinador & "' OR idConsultor_ingresso = '" & idPatrocinador & "'"
	
	Set adoRecordset = Server.CreateObject("ADODB.Recordset")
	adoRecordset.Open query, conexao
	
	if(not adoRecordset.eof) then	
		
		Set retorno = jsArray()
		
		Set retorno(Null) = jsObject()
		retorno(Null)("tipo") = "patrocinador"
		
		do while not adoRecordset.eof
		
			Set retorno(Null) = jsObject()
			retorno(Null)("id_ingresso") = adoRecordset("id_ingresso")
			retorno(Null)("codigo_ingresso") = adoRecordset("codigo_ingresso")
			retorno(Null)("idPatrocinador_ingresso") = adoRecordset("idPatrocinador_ingresso")
			retorno(Null)("idConsultor_ingresso") = adoRecordset("idConsultor_ingresso")
			retorno(Null)("status_ingresso") = adoRecordset("status_ingresso")
			
			query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & adoRecordset("idConsultor_ingresso") & "'"
			Set adoConsultor = Server.CreateObject("ADODB.Recordset")
			adoConsultor.Open query, conexao

			retorno(Null)("nome") = adoConsultor("nome")
			
			adoRecordset.movenext
		loop 
		
		retorno.Flush
	
	else
		
		Set retorno = jsObject()
		retorno("tipo") = "none"
		retorno.Flush

	end if 
	
	conexao.Close
	set conexao = nothing
	
EXIT FUNCTION
END FUNCTION

FUNCTION obterIngressosPatrocinador(conexao)
	
	idPatrocinador = LimpaLixo(ExpurgaApostrofe(REQUEST("id")))

	query = "SELECT * FROM [hinode].[dbo].[tbl_mw_ingressos] WHERE idConsultor_ingresso = '" & idPatrocinador & "' AND idPatrocinador_ingresso <> '" & idPatrocinador & "'"
	
	Set adoRecordset = Server.CreateObject("ADODB.Recordset")
	adoRecordset.Open query, conexao
	
	if(not adoRecordset.eof) then	

		Set retorno = jsArray()
		
		do while not adoRecordset.eof
		
			Set retorno(Null) = jsObject()
			retorno(Null)("id_ingresso") = adoRecordset("id_ingresso")
			retorno(Null)("codigo_ingresso") = adoRecordset("codigo_ingresso")
			retorno(Null)("idPatrocinador_ingresso") = adoRecordset("idPatrocinador_ingresso")
			retorno(Null)("idConsultor_ingresso") = adoRecordset("idConsultor_ingresso")
			retorno(Null)("status_ingresso") = adoRecordset("status_ingresso")
			
			query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & adoRecordset("idPatrocinador_ingresso") & "'"
			Set adoPatrocinador = Server.CreateObject("ADODB.Recordset")
			adoPatrocinador.Open query, conexao

			retorno(Null)("nome") = adoPatrocinador("nome")
			
			adoRecordset.movenext
		loop 
		
		retorno.Flush
				
	end if 
	
	conexao.Close
	set conexao = nothing
	
EXIT FUNCTION
END FUNCTION

Function Fc_lista_cupom(conexao,vcampanha,vidconsultor)

strsql = " select Ciclo=B.nome_ciclo,a.integracao,a.nome,a.dt_cad,a.cupom,B.nome_ciclo "&_
      " from rede_campanha_patrocinios a inner join rede_parametros b on a.inicio=b.inicio "&_
      " where a.superior_integracao='"&vidconsultor&"'"&_
      " ORDER BY B.CICLO,a.dt_cad "
	
QueryToJSON(conexao, strsql).Flush				  

conexao.Close
set conexao=nothing

Exit Function				  
End Function



Function Fc_cupom_cons_geral(conexao,vidconsultor)
strsql= "exec [hinode].[dbo].[cupom_consulta_geral] '"&vidconsultor&"'"
		QueryToJSON(conexao,strsql).Flush			  

conexao.Close
set conexao=nothing

Exit Function				  
End Function


Function Fc_cupom_cons_qtd(conexao,vidconsultor)
strsql= "exec [hinode].[dbo].[cupom_consulta_qtd] '"&vidconsultor&"'"
		QueryToJSON(conexao,strsql).Flush			  

conexao.Close
set conexao=nothing

Exit Function				  
End Function

Function Fc_cupom_cons_qtd(conexao,vidconsultor)
strsql= "exec [hinode].[dbo].[cupom_consulta_qtd] '"&vidconsultor&"'"
		QueryToJSON(conexao,strsql).Flush			  

conexao.Close
set conexao=nothing

Exit Function				  
End Function


'--------- LISTA PDF CONSULTOR ---------------
Function obterPdfConsultor(arquivoPdf)

	tamanho = Len(arquivoPdf)
	zeroEsc = ""
	
	For i = tamanho to 10
	 	zeroEsc = "0"&zeroEsc
	Next
	
	arquivoPdf2 = zeroEsc&arquivoPdf
	
	'response.Write arquivoPdf2
	
	diretorio = "D:\inetpub\Sites\www_hinode_com_br\portalhinode\vo\mw-arquivos\rendimentos" ' coloque a pasta principal
	Dim objFSO, conf_diretorio
	
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	if objFSO.FileExists(diretorio & "\"&arquivoPdf2&"0001.pdf") = False then'Verifica se o arquivo existe
		response.Write "0"
	else
		response.Write arquivoPdf2&"0001.pdf"
	end if
End Function





Function InverteData(data,inverte)
 If isDate(data) then
	 vData = data
	else
	 vData = now()	
	End If
	temp = split(vData,"/")
	
	 If len(temp(0)) <> 2 Then
		 temp(0) = "0" & temp(0)
		End If
		
		If len(temp(1)) <> 2 Then
		 temp(1) = "0" & temp(1)
		End If
	
	If inverte = true Then			 	 	 
 	InverteData = temp(1) & "/" & temp(0) & "/" & temp(2)
	Else 
	 InverteData	= temp(0) & "/" & temp(1) & "/" & temp(2)
	End If	
End Function

Function FormataData(Data)
  If Data <> "" Then
    FormataData = Right("0" & DatePart("d", Data),2) & "/" & Right("0" & DatePart("m", Data),2) & "/" & DatePart("yyyy", Data)
  End If
End Function



'- Substituindo o apóstrofe(') pelo duplo apóstrofe ('')
Function ExpurgaApostrofe(texto)
    ExpurgaApostrofe = replace( texto , "'" , "''")
End function


'- Substituindo os caracteres e palavras maliciosas por vazio("").
Function LimpaLixo(input)
    dim lixo
    dim textoOK

    lixo = array ("SELECT","DROP","UPDATE",";","--","INSERT","DELETE","xp_","<","SCRIPT",">")

    textoOK = input

     for i = 0 to UBound(lixo)
          textoOK = replace( textoOK ,  lixo(i) , "")
     next

     LimpaLixo = textoOK

end Function


'- Rejeitando os dados maliciosos:
Function ValidaDados(input)
      lixo = array ( "SELECT" , "INSERT" , "UPDATE" , "DELETE" , "DROP" , "--" , "'")

      ValidaDados = true

      for i = LBound(lixo) to UBound(lixo)
            if ( instr(1 ,  input , lixo(i) , vbtextcompare ) <> 0 ) then
                  ValidaDados = False
                  exit function '}
            end if
      next
end function


%>