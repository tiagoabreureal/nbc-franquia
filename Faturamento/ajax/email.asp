﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="../mw-conexao-db.asp"-->
<!--#include file="../json/JSON_2.0.4.asp"-->
<!--#include file="../json/JSON_UTIL_0.1.1.asp"-->

<%

'Function EnviaPedidoEmail(vid_pedido_oficial,conexao,vemail_cdh)
vid_pedido_oficial = 418580
vemail_cdh = "rafael@mediaw.com.br"

if vid_pedido_oficial > 0 then

	Set adoPed = Server.CreateObject("ADODB.Recordset")
	
	vSQLLISTPED = " SELECT * " &_
	" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido] " &_
	" WHERE IdPedido_oficial='"&vid_pedido_oficial &"'"
	
	adoPed.Open vSQLLISTPED, conexao_loja_mw
	
	if(not adoPed.eof)then
		do while not adoPed.eof
			vPed_idStatus = adoPed("Ped_idStatus")
			vPed_QtdaParcela = adoPed("Ped_QtdaParcela")
			vIdPedido_oficial = adoPed("IdPedido_oficial")
			vPed_nome_consultor = adoPed("Ped_nome_consultor")
			vPed_idConsultor = adoPed("Ped_idConsultor")
			vPed_pagamento = adoPed("Ped_pagamento")
			vPed_idFormaPagamento = adoPed("Ped_idFormaPagamento")
			vPed_vl_sub_total = FormatNumber(adoPed("Ped_vl_sub_total"),2)
			vPed_vl_frete = FormatNumber(adoPed("Ped_vl_frete"),2)
			vPed_vl_total = FormatNumber(adoPed("Ped_vl_total"),2)
			vPed_Credito_usado = FormatNumber(adoPed("Ped_Credito_usado"),2)
			vPed_modo_entr = adoPed("Ped_modo_entr")
			vPed_prazo_dias = adoPed("Ped_prazo_dias")
			vPed_Transporte = adoPed("Ped_Transporte")
			
			vPed_cep = adoPed("Ped_cep")
			vPed_endereco = adoPed("Ped_endereco")
			vPed_numero = adoPed("Ped_numero")
			vPed_compl = adoPed("Ped_compl")
			vPed_bairro = adoPed("Ped_bairro")
			vPed_cidade = adoPed("Ped_cidade")
			vPed_estado = adoPed("Ped_estado")
			vPed_dt_cadastro = adoPed("Ped_dt_cadastro")
			vPed_email = adoPed("Ped_email")
			
			adoPed.movenext
		loop
	end if
	
	if vPed_email = "" then
		'vPed_email = "ancelmo@hinode.com.br"
		'vPed_email = "leonardo@mediaw.com.br"
		vPed_email = "rafael@mediaw.com.br"
	end if	 
	
	'XXXXXXXXXXXXXXXXX INICIO NOTIFICAÇÃO POR E-MAIL - CONSULTOR XXXXXXXXXXXXXXXXXXXX
	
	if vPed_email <> "" then
	
		vMsg = "<table bgcolor='#FFFFFF' border='0' style='padding:20px;'>"& _
		"<tbody>"& _
		"<tr>"& _
		"<td width='550'>"& _
		"<table align='center' border='0' cellpadding='0' cellspacing='0'>"& _
		"<tbody>"& _
		"<tr>"& _
		"<td style='padding-bottom:5px;' width='550'>"& _
		"<table style='background-color:#f1e3c8; border:1px solid #e1caa8; width:100%; padding:10px 15px;'>"& _
		"<tbody>"& _
		"<tr>"& _
		"<td width='180'>"& _
		"<img alt='Hinode.com.br' src='http://vo.hinode.com.br/images/logo-hinode-cosmeticos-email.png' /></td>"& _
		"<td align='center' style='font-family:arial; font-size:16px; font-weight:bold; color:#000000;'>"& _
		"PEDIDO CONSULTOR > HINODE GERADO - "&vid_pedido_oficial&"</td>"& _
		"</tr>"& _
		"</tbody>"& _
		"</table>"& _
		"</td>"& _
		"</tr>"& _
		"<tr>"& _
		"<td valign='top' style='padding:15px; border:1px solid #e1caa8;'>"& _
		"<table align='center' border='0' cellpadding='0' cellspacing='0'>"& _
		"<tbody>"& _
		"<tr>"& _
		"<td style='font:13px Arial;' width='520'>"& _
		"<p>"& _
		"Olá, "&vPed_nome_consultor&", <br>"& _ 
		"À HINODE COSMÉTICOS, está enviando uma copia do pedido gerado.<br />"& _ 
		"O seu número de pedido é: <strong>"&vid_pedido_oficial&"</strong>.<br /><br />"& _
	
	
		"Dados do Pedido:<br />"&_
		""&vPed_nome_consultor&"<br />"&_
		"Sub total pedido:"&vPed_vl_sub_total&"<br />"&_
		"Frete:"&vPed_vl_frete&"<br />"&_
		"Crédito Usado:"&vPed_Credito_usado&"<br />"&_
		"Valor total pedido:"&vPed_vl_total&"<br />"&_
		"Forma de Pagamento:"&vPed_pagamento&" "&vPed_QtdaParcela&"x <br />"
		
		if cint(vPed_idFormaPagamento) = 5 then
			vMsg = vMsg & "<br /><br /><strong>"&_
			"<a href='https://vo.hinode.com.br/vo/gera_pedido/"&_ 
			"mw-gera-pedido-boleto.asp?id_pedido_oficial="&vIdPedido_oficial&"' target='_blank'>"&_
			"IMPRIMA SEU BOLETO BANCÁRIO, CLICANDO AQUI.</a></strong><br /><br />"
		end if
	
		vMsg = vMsg & "<strong>Tipo de Envio:</strong><br />"&_
		""&vPed_Transporte&"<br /><br />"&_
		
		"<strong>Itens do pedido:</strong><br />"&_
		"<table align='center' border='0' cellpadding='1' cellspacing='1' width='100%' bgcolor='#f1e3c8'>"&_
		"<tbody>"&_
		"<tr>"&_
		"<td>Código</td>"&_
		"<td>Produto</td>"&_
		"<td>Quant.</td>"&_
		"<td>Valor Unit.</td>"&_
		"<td>Valor Desconto</td>"&_
		"<td>Valor Total</td>"&_
		"</tr>"
	
		Set adoPeditem = Server.CreateObject("ADODB.Recordset")
	
		vSQLLISTPEDITEM = " SELECT item.* " &_
		" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_item] item " &_
		" inner join [hinode_loja].[dbo].[tbl_mw_gera_pedido] p ON p.CdPedido = item.PedItem_CdPedido"&_
		" WHERE p.IdPedido_oficial='"& vid_pedido_oficial &"'"
		
		adoPeditem.Open vSQLLISTPEDITEM, conexao_loja_mw
		
		if(not adoPeditem.eof) then
	
			items = ""
	
			do while not adoPeditem.eof
				
				if adoPeditem("PedItem_class") = 1 then
				
					vPedItem_prod_codigo = adoPeditem("PedItem_prod_codigo")
					vPedItem_prod_nome = adoPeditem("PedItem_prod_nome")
					vPedItem_prod_qtd_encomendar = adoPeditem("PedItem_prod_qtd_encomendar")
					vPedItem_prod_qtd = adoPeditem("PedItem_prod_qtd")
					vPedItem_prod_valor_unt = adoPeditem("PedItem_prod_valor_unt")
					valorDesconto = adoPeditem("PedItem_prod_valor_desc")
					vvalor_total_item = vPedItem_prod_qtd*vPedItem_prod_valor_unt - valorDesconto
					vPedItem_prod_valor_unt2 = FormatNumber(vPedItem_prod_valor_unt,2)
					vvalor_total_item2 = FormatNumber(vvalor_total_item,2)
					
					itemAtivacao = adoPeditem("PediItem_prod_atv_consultor")
					
					if vPedItem_prod_qtd_encomendar > 0 then
						encomendar_item = "</br><span class='texto_vermelho'>Item sob encomenda, quantidade encomendar:</span>"&vPedItem_prod_qtd_encomendar&""
					else
						encomendar_item = ""
					end if
		
					if itemAtivacao = int(1) then
						vMsg = vMsg & "<tr bgcolor='#FFC'>"&_
						"<td>"&vPedItem_prod_codigo&"</td>"&_
						"<td>"&vPedItem_prod_nome&""&encomendar_item&"</td>"&_
						"<td>"&vPedItem_prod_qtd&"</td>"&_
						"<td>R$"&vPedItem_prod_valor_unt2&"</td>"&_
						"<td>R$"&FormatNumber(valorDesconto,2)&"</td>"&_
						"<td>R$"&vvalor_total_item2&"</td>"&_
						"</tr>"
					else
						vMsg = vMsg & "<tr bgcolor='#FEFEDD'>"&_
						"<td>"&vPedItem_prod_codigo&"</td>"&_
						"<td>"&vPedItem_prod_nome&""&encomendar_item&"</td>"&_
						"<td>"&vPedItem_prod_qtd&"</td>"&_
						"<td>R$"&vPedItem_prod_valor_unt2&"</td>"&_
						"<td>R$"&FormatNumber(valorDesconto,2)&"</td>"&_
						"<td>R$"&vvalor_total_item2&"</td>"&_
						"</tr>"
					end if
					
				else
					
					idProduto = adoPeditem("PedItem_prod_codigo")
					
					arrayItems = Split(items,"|")
					encontrou = false
					
					For i = 0 to Ubound(arrayItems)  
						if arrayItems(i) = idProduto then
							encontrou = true
						end if
					Next
					
					if encontrou = false then
						
						quantidadeProduto = 0
						items = items + "|" + idProduto
						
						query = " SELECT item.* " &_
						" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_item] item " &_
						" inner join [hinode_loja].[dbo].[tbl_mw_gera_pedido] p ON p.CdPedido = item.PedItem_CdPedido"&_
						" WHERE p.IdPedido_oficial='"& vid_pedido_oficial & "' AND item.PedItem_prod_codigo='" & idProduto & "' AND item.PedItem_class = '0'"				
						
						Set adoCarregaTodosProdutos = Server.CreateObject("ADODB.Recordset")
						adoCarregaTodosProdutos.Open query, conexao_loja_mw
						
						if(not adoCarregaTodosProdutos.eof) then
							
							do while not adoCarregaTodosProdutos.eof
								quantidadeProduto = quantidadeProduto + 1
								adoCarregaTodosProdutos.movenext
							loop	
							
						end if
						
						vPedItem_prod_codigo = adoPeditem("PedItem_prod_codigo")
						vPedItem_prod_nome = adoPeditem("PedItem_prod_nome")
						vPedItem_prod_qtd_encomendar = adoPeditem("PedItem_prod_qtd_encomendar")
						vPedItem_prod_qtd = quantidadeProduto
						vPedItem_prod_valor_unt = adoPeditem("PedItem_prod_valor_unt")
						valorDesconto = adoPeditem("PedItem_prod_valor_desc")
						vvalor_total_item = vPedItem_prod_qtd*vPedItem_prod_valor_unt	
						vPedItem_prod_valor_unt2 = FormatNumber(vPedItem_prod_valor_unt,2)
						vvalor_total_item2 = FormatNumber(vvalor_total_item,2)
			
						if vPedItem_prod_qtd_encomendar > 0 then
							encomendar_item = "</br><span class='texto_vermelho'>Item sob encomenda, quantidade encomendar:</span>"&vPedItem_prod_qtd_encomendar&""
						else
							encomendar_item = ""
						end if
			
						vMsg = vMsg & "<tr bgcolor='#FFFFFF'>"&_
						"<td>"&vPedItem_prod_codigo&"</td>"&_
						"<td>"&vPedItem_prod_nome&""&encomendar_item&"</td>"&_
						"<td>"&vPedItem_prod_qtd&"</td>"&_
						"<td>R$"&vPedItem_prod_valor_unt2&"</td>"&_
						"<td>R$"&FormatNumber(valorDesconto,2)&"</td>"&_
						"<td>R$"&vvalor_total_item2&"</td>"&_
						"</tr>"
						
					
					end if
					
				end if
				
				adoPeditem.movenext
	
			loop
	
		end if
	
		vMsg = vMsg & "</tbody>"&_
		"</table>"&_
		"</td>"& _
		"</tr>"& _
		"</tbody>"& _
		"</table>"& _
		"</td>"& _
		"</tr>"& _
		"<tr>"& _
		"<td height='90' style='font:12px Arial; background:#f1e3c8; color:#000000; padding:10px 15px'>"& _
		"<table>"& _
		"<tbody>"& _
		"<tr>"& _
		"<td style='padding-left:10px'>"& _
		"<p style='font:12px arial; color:#000000'>"& _
		"Atenciosamente,<br />"& _
		"<span style='font:bold 20px arial;'>HINODE COSMÉTICOS</span><br />"& _
		"<a href='http://www.hinode.com.br' style='color:#000000' target='_blank' "& _ 
		"title='www.hinode.com.br'>www.hinode.com.br</a></p>"& _
		"</td>"& _
		"</tr>"& _
		"</tbody>"& _
		"</table>"& _
		"</td>"& _
		"</tr>"& _
		"</tbody>"& _
		"</table>"& _
		"</td>"& _
		"</tr>"& _
		"</tbody>"& _
		"</table>"

	response.write vMsg
	
		if cstr(vPed_idConsultor)="00101209" then
			'vPed_email = "leonardo@mediaw.com.br"
			vPed_email = "rafael@mediaw.com.br"
		end if	 
	
		servidor="smtp.hinode.com.br"
		
		de  ="naoresponda@hinode.com.br"
		
		user="naoresponda@hinode.com.br"
		
		pass="druida@107"
		
		'para = vPed_email
		para = "rafael@mediaw.com.br"
		
		assunto	= "PEDIDO CONSULTOR-HINODE [" & vid_pedido_oficial &"]"
		
		Set Mail = Server.CreateObject("CDO.Message") 
		
		Set MailCon = Server.CreateObject ("CDO.Configuration") 
	
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = servidor
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 587 
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 
		
		if len(user)>0 then
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = user
			MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = pass
		end if
	
		MailCon.Fields.update
	
		Set Mail.Configuration = MailCon 
		Mail.From = de
		
		if vemail_cdh <> "" then
			Mail.CC = vemail_cdh
		end if
	
		'Mail.BCC="fabio@hinode.com.br;isabel@hinode.com.br;leandro@hinode.com.br;ancelmo@hinode.com.br;toni@hinode.com.br"
		Mail.To = para
		Mail.Subject = assunto
		'id_Body	="Esse é um teste de email."
		Mail.HTMLBody=vMsg
		Mail.Fields.update
		Mail.Send
		Set Mail = Nothing 
		Set MailCon = Nothing 
		'Response.Write "Enviado com sucesso às " & now
	end if		   

end if

'XXXXXXXXXXXXXXXXXXX FIM - ENVIA NOTIFICAÇÃO POR E-MAIL - CONSULTOR XXXXXXXXXXXXXXXXXXXX

adoPed.Close
Set adoPed = Nothing

adoPeditem.Close
Set adoPeditem = Nothing

conexao_loja_mw.Close
Set conexao_loja_mw = Nothing


%>