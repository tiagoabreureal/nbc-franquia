﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="../mw-conexao-db.asp"-->
<%

vacao = LimpaLixo(ExpurgaApostrofe(Request("acao")))
vidconsultor = LimpaLixo(ExpurgaApostrofe(Request.Form("idconsultor")))
vidpergunta = LimpaLixo(ExpurgaApostrofe(Request.Form("idpergunta")))
vresposta_consultor = LimpaLixo(ExpurgaApostrofe(Request.Form("resposta_consultor")))
vdescricao_pergunta = LimpaLixo(ExpurgaApostrofe(Request.Form("descricao_pergunta")))

Select Case vacao
	
	Case "salvar-resposta-questionario"		
		Vretorno = Fc_salvar_resposta_questionario(vidconsultor,vidpergunta,vresposta_consultor,vdescricao_pergunta)
		response.Write(Vretorno)
		
	Case "verifica-consultor-respondeu"		
		Vretorno = Fc_verificaConsultorRespondeu(vidconsultor)
		response.Write(Vretorno)
		
	Case "altera-email-consultor"		
		Vretorno = Fc_alteraEmailConsultor()
		response.Write(Vretorno)

Case Else
	Response.Write("0")
		
End Select

'XXXXXXXXXXXXXXXXXXXXXXXXXX FUNCTION  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Function Fc_salvar_resposta_questionario(vidconsultor,vidpergunta,vresposta_consultor,vdescricao_pergunta)

	
	perguntas = Split(vidpergunta,"|")
	respostas = Split(vresposta_consultor,"|")
	descricao = Split(vdescricao_pergunta,"|")
	'response.Write(perguntas(0)&" - "&respostas(0)&" - "&descricao(0))	
	horaAtual = Split(Time," ")
	data = Split(date,"/")
	dataHora = data(2)&"-"&data(1)&"-"&data(0)&" "&horaAtual(0)


	if vidconsultor <> "" AND vidpergunta <> "" AND vresposta_consultor <> "" then
	
		For i = 0 to 6 'use i as a counter
			strsql = " INSERT INTO [hinode].[dbo].[tbl_mw_questionario] "&_
                 " ([id_pergunta] "&_
                 ",[id_consultor] "&_
                 ",[resposta_consultor]"&_
				 ",[descricao]"&_
				 ",[data_resposta]) "&_
                 "VALUES "&_
                 "( '"&perguntas(i)&"' "&_
                 ", '"&vidconsultor&"' "&_
				 ", '"&respostas(i)&"' "&_
				 ", '"&descricao(i)&"' "&_
                 ", '"&dataHora&"' ) "
		
			'response.Write(strsql&"<br>")
			set adors2 = conexao_mw.execute(strsql)
			'response.Write("1|"&vidpergunta&"|"&vresposta_consultor) 
		Next
		
		response.Write("1|Questionário salvo com sucesso!")
		
	else
		response.Write("0|Erro ao tentar alterar o lado, tente novamente!")
	end if
	
End Function


Function Fc_verificaConsultorRespondeu(vidconsultor)
	sql = " SELECT TOP 1 * FROM [hinode].[dbo].[tbl_mw_questionario] "&_
			" WHERE id_consultor = '"&vidconsultor&"' "
			
	set adors1 = conexao_mw.execute(sql)
	
	if ( not adors1.eof ) then		
		response.Write("1|Consultor já Respondeu o questionario!")
	else
		response.Write("0|Consultor ainda não Respondeu o questionario!")
	end if	
			
End Function


Function Fc_alteraEmailConsultor()

	destinatarioConsultor = LimpaLixo(ExpurgaApostrofe(Request.Form("destinatarioConsultor")))
	novoEmail = LimpaLixo(ExpurgaApostrofe(Request.Form("email")))

	if destinatarioConsultor <> "" then
	
		strsql=" UPDATE fat_destinatario SET " & _
			" email = '" & novoEmail & "' " & _
			"WHERE destinatario = '" & destinatarioConsultor & "'"
				
		set adors1 = conexao_mw.execute(strsql)
		
		response.Write("1|E-mail alterado com sucesso!")
	
	else
		
		response.Write("0|Consultor não encontrado!")
		
	end if	
			
End Function



'#######################################
'#######################################
'#######################################
'#######################################

'- Substituindo o apóstrofe(') pelo duplo apóstrofe ('')
Function ExpurgaApostrofe(texto)
    ExpurgaApostrofe = replace( Trim(texto) , "'" , "''")
End function

'- Substituindo os caracteres e palavras maliciosas por vazio("").
Function LimpaLixo(input)
    dim lixo
    dim textoOK

    lixo = array ("SELECT","DROP","UPDATE",";","--","INSERT","DELETE","xp_","<","SCRIPT",">")

    textoOK = Trim(input)

     for i = 0 to UBound(lixo)
          textoOK = replace( textoOK ,  lixo(i) , "")
     next

     LimpaLixo = textoOK

end Function


'- Rejeitando os dados maliciosos:
Function ValidaDados(input)
      lixo = array ( "SELECT" , "INSERT" , "UPDATE" , "DELETE" , "DROP" , "--" , "'")

      ValidaDados = true

      for i = LBound(lixo) to UBound(lixo)
            if ( instr(1 ,  input , lixo(i) , vbtextcompare ) <> 0 ) then
                  ValidaDados = False
                  exit function '}
            end if
      next
end function


Function InverteData(data,inverte)
 If isDate(data) then
	 vData = data
	else
	 vData = now()	
	End If
	temp = split(vData,"/")
	
	 If len(temp(0)) <> 2 Then
		 temp(0) = "0" & temp(0)
		End If
		
		If len(temp(1)) <> 2 Then
		 temp(1) = "0" & temp(1)
		End If
	
	If inverte = true Then			 	 	 
 	InverteData = temp(1) & "/" & temp(0) & "/" & temp(2)
	Else 
	 InverteData	= temp(0) & "/" & temp(1) & "/" & temp(2)
	End If	
End Function

%>