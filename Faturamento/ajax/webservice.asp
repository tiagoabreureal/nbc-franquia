﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="../mw-conexao-db.asp"-->
<!--#include file="../json/JSON_2.0.4.asp"-->
<!--#include file="../json/JSON_UTIL_0.1.1.asp"-->

<%

action = REQUEST("action")

SELECT CASE action

	CASE "retornaConsultor"
		retornaConsultor()
		
	CASE "obterConsultor"
		obterConsultor()
		
	CASE "retornaListaExcecoes"
		retornaListaExcecoes()
	
	CASE "excluirConsultorListaExcecoes"
		excluirConsultorListaExcecoes()
		
	CASE "adicionarConsultorListaExcecoes"
		adicionarConsultorListaExcecoes()
		
	CASE "isExcecao"
		isExcecao()
		
END SELECT

FUNCTION isExcecao()
	
	idConsultor = REQUEST("idConsultor")
	
	query = "SELECT * FROM [hinode].[dbo].[tbl_mw_excecao_antifraude] WHERE idConsultor = '" & idConsultor & "'"
	Set adoRecordset = Server.CreateObject("ADODB.Recordset")
	
	adoRecordset.Open query, conexao_loja_mw
	
	if adoRecordset.eof then
	
		Set retorno = jsObject()
		retorno("status") = false
		retorno.Flush
		
	else
		
		Set retorno = jsObject()
		retorno("status") = true
		retorno.Flush
	
	end if
	
	conexao_loja_mw.Close
	set conexao_loja_mw = nothing
	
EXIT FUNCTION
END FUNCTION

FUNCTION adicionarConsultorListaExcecoes()
	
	idConsultor = REQUEST("idConsultor")
	nomeConsultor = REQUEST("nomeConsultor")
	
	if Len(idConsultor) < 8 then
			
		resto = 7 - Len(idConsultor)
		
		zeroEsquerda = ""
		
		For j = 0 to resto
			
			zeroEsquerda = zeroEsquerda & "0"
			
		Next
		
	end if
	
	idConsultor = zeroEsquerda & idConsultor
	
	query = "SELECT * FROM [hinode].[dbo].[tbl_mw_excecao_antifraude] WHERE idConsultor = '" & idConsultor & "'"
	Set adoRecordset = Server.CreateObject("ADODB.Recordset")
	
	adoRecordset.Open query, conexao_loja_mw
	
	if adoRecordset.eof then
		
		query = "INSERT INTO [hinode].[dbo].[tbl_mw_excecao_antifraude] (idConsultor, nomeConsultor, data) VALUES ('"  & idConsultor & "', '" & nomeConsultor & "', GetDate())"
		conexao_loja_mw.Execute(query)	
		
		Set retorno = jsObject()
		retorno("status") = true
		retorno("mensagem") = "Consultor cadastrado com sucesso!"
		retorno.Flush
		
	else
		
		Set retorno = jsObject()
		retorno("status") = false
		retorno("mensagem") = "Consultor já está cadastrado!"
		retorno.Flush
	
	end if
	
	conexao_loja_mw.Close
	set conexao_loja_mw = nothing
	
EXIT FUNCTION
END FUNCTION

FUNCTION excluirConsultorListaExcecoes()
	
	idConsultor = REQUEST("idConsultor")
	
	if Len(idConsultor) < 8 then
			
		resto = 7 - Len(idConsultor)
		
		zeroEsquerda = ""
		
		For j = 0 to resto
			
			zeroEsquerda = zeroEsquerda & "0"
			
		Next
		
	end if
	
	idConsultor = zeroEsquerda & idConsultor
	
	query = "DELETE FROM [hinode].[dbo].[tbl_mw_excecao_antifraude] WHERE idConsultor = '" & idConsultor & "'"
		
	conexao_loja_mw.Execute(query)			  
	
	conexao_loja_mw.Close
	set conexao_loja_mw = nothing
	
EXIT FUNCTION
END FUNCTION

FUNCTION retornaListaExcecoes()
	
	query = "SELECT * FROM [hinode].[dbo].[tbl_mw_excecao_antifraude] ORDER BY nomeConsultor ASC"
		
	QueryToJSON(conexao_loja_mw, query).Flush				  
	
	conexao_loja_mw.Close
	set conexao_loja_mw = nothing
	
EXIT FUNCTION
END FUNCTION

FUNCTION retornaConsultor()
	
	idConsultor = REQUEST("idConsultor")
	
	if Len(idConsultor) < 8 then
			
		resto = 7 - Len(idConsultor)
		
		zeroEsquerda = ""
		
		For j = 0 to resto
			
			zeroEsquerda = zeroEsquerda & "0"
			
		Next
		
	end if
	
	idConsultor = zeroEsquerda & idConsultor
	
	query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & idConsultor & "'"
		
	QueryToJSON(conexao_loja_mw, query).Flush				  
	
	conexao_loja_mw.Close
	set conexao_loja_mw = nothing
	
EXIT FUNCTION
END FUNCTION

FUNCTION obterConsultor()
	
	idConsultor = LimpaLixo(ExpurgaApostrofe(REQUEST("idConsultor")))
	
	if Len(idConsultor) < 8 then
			
		resto = 7 - Len(idConsultor)
		
		zeroEsquerda = ""
		
		For j = 0 to resto
			
			zeroEsquerda = zeroEsquerda & "0"
			
		Next
		
	end if

	query = "SELECT * FROM [hinode].[dbo].[view_consultor] WHERE integracao = '" & zeroEsquerda & idConsultor & "'"
	
	QueryToJSON(conexao_loja_mw, query).Flush				  
	
	conexao_loja_mw.Close
	set conexao_loja_mw = nothing
	
EXIT FUNCTION
END FUNCTION

Function ExpurgaApostrofe(texto)
    ExpurgaApostrofe = replace( texto , "'" , "''")
End function

'- Substituindo os caracteres e palavras maliciosas por vazio("").
Function LimpaLixo(input)
    dim lixo
    dim textoOK

    lixo = array ("SELECT","DROP","UPDATE",";","--","INSERT","DELETE","xp_","<","SCRIPT",">")

    textoOK = input

     for i = 0 to UBound(lixo)
          textoOK = replace( textoOK ,  lixo(i) , "")
     next

     LimpaLixo = textoOK

end Function

%>