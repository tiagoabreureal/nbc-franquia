<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->

<%
pedido          = request.Form("pedido")
ncartao         = request.Form("ncartao")
titular         = request.Form("titular")
cvc             = request.Form("cvc")
mesval          = request.Form("mesval")
anoval          = request.Form("anoval")
parcelas        = request.Form("parcelas")
valorcartao     = request.Form("valorcartao")
valorvista      = request.Form("valorvista")
valor           = request.Form("valor")
juros           = request.Form("juros")
valorfinal      = request.Form("valorfinal")
qtd_cartoes     = request.Form("qtdcartoes")
ip              = replace(replace(Request.ServerVariables("REMOTE_ADDR"),".",""),"-","")
'Origem e url deixar fixo por enquanto pq s� a franquia vai usar essa integra��o
origem          = "LiderFranquia"

''evitar sql injection
pedido = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(pedido,"'",""),"select",""),"delete",""),"update",""),"injection",""),"--",""),"drop",""),"insert",""),";",""),"xp_",""),"<",""),">",""),"script","")
ncartao = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(ncartao,"'",""),"select",""),"delete",""),"update",""),"injection",""),"--",""),"drop",""),"insert",""),";",""),"xp_",""),"<",""),">",""),"script","")
titular = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(titular,"'",""),"select",""),"delete",""),"update",""),"injection",""),"--",""),"drop",""),"insert",""),";",""),"xp_",""),"<",""),">",""),"script","")
cvc = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(cvc,"'",""),"select",""),"delete",""),"update",""),"injection",""),"--",""),"drop",""),"insert",""),";",""),"xp_",""),"<",""),">",""),"script","")
mesval = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(mesval,"'",""),"select",""),"delete",""),"update",""),"injection",""),"--",""),"drop",""),"insert",""),";",""),"xp_",""),"<",""),">",""),"script","")
anoval = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(anoval,"'",""),"select",""),"delete",""),"update",""),"injection",""),"--",""),"drop",""),"insert",""),";",""),"xp_",""),"<",""),">",""),"script","")
parcelas = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(parcelas,"'",""),"select",""),"delete",""),"update",""),"injection",""),"--",""),"drop",""),"insert",""),";",""),"xp_",""),"<",""),">",""),"script","")
valorcartao = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(valorcartao,"'",""),"select",""),"delete",""),"update",""),"injection",""),"--",""),"drop",""),"insert",""),";",""),"xp_",""),"<",""),">",""),"script","")
valorvista = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(valorvista,"'",""),"select",""),"delete",""),"update",""),"injection",""),"--",""),"drop",""),"insert",""),";",""),"xp_",""),"<",""),">",""),"script","")
valor = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(valor,"'",""),"select",""),"delete",""),"update",""),"injection",""),"--",""),"drop",""),"insert",""),";",""),"xp_",""),"<",""),">",""),"script","")
juros = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(juros,"'",""),"select",""),"delete",""),"update",""),"injection",""),"--",""),"drop",""),"insert",""),";",""),"xp_",""),"<",""),">",""),"script","")
valorfinal = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(valorfinal,"'",""),"select",""),"delete",""),"update",""),"injection",""),"--",""),"drop",""),"insert",""),";",""),"xp_",""),"<",""),">",""),"script","")
qtd_cartoes = replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(qtd_cartoes,"'",""),"select",""),"delete",""),"update",""),"injection",""),"--",""),"drop",""),"insert",""),";",""),"xp_",""),"<",""),">",""),"script","")

'Proc com algumas valida��es 
sqllnegra = "exec valida_listanegra_sp '"&pedido&"','"&ip&"'"
set adolista = conn1.execute(sqllnegra)
if not adolista.eof then
    cod = adolista("cod")
    msg = adolista("msg")
end if
set adolista = nothing
        
if cod = 10 then
    session("adyen_msg")="Lista Negra"
    response.Redirect "vo3-gera-pedido-forma-pagamento_mult.asp?id_pedido_site="&pedido&""
end if


'response.Write  replace(replace(formatNumber(replace(valorFinal,".",","),2),",",""),".","") & "<br>"
'response.Write 5000
'response.End

'Validar se o valor final � maior que 50 reais
if replace(replace(formatNumber(replace(valorFinal,".",","),2),",",""),".","") < 5000 then
    'sqllist = "exec lista_negra_sp '"&idpedido&"','"&ip&"'"
    'conn1.execute(sqllist)
    session("adyen_msg") = "Recusado devido ao valor ser inferior a 50 reais"
    response.Redirect "vo3-gera-pedido-forma-pagamento_mult.asp?id_pedido_site="&pedido&""
end if

'Verificar se o pedido realmente existe  no sistema
sqlinexistente = "Select count(0) cont from ped_pedidoTotal where pedicodigo = '"&pedido&"' "
set adoinexist = conn1.execute(sqlinexistente)
if not adoinexist.eof then
    existe = adoinexist("cont") 
end if
if existe <= 0 then
    session("adyen_msg") = "Pedido N�o Encontrado!"
    response.Redirect "vo3-gera-pedido-forma-pagamento_mult.asp"
end if
set adoinexist = nothing
'Verificar se o pedido esta com status (10) pagamento efetuado ou passou pelos status (1) Nota gerada (97) Retirado na Franquia
sqlst = "select count (0) as val from ped_pedidoTotal a " &_
                "inner join ped_movimentoStatus b on a.pedicodigo = b.pedicodigo " &_
                "where nfreferencia = '"&idpedido&"' and (b.stnfCodigo in (1,97) or a.stnfCodigo in (10,20)) " 
set adost = conn1.execute(sqlst)
if not adost.eof then
    val = adost("val")
end if
if val > 0 then
    session("adyen_msg") = "Pedido J� Faturado!"
    response.Redirect "vo3-gera-pedido-forma-pagamento_mult.asp?id_pedido_site="&pedido&""
end if
set adost = nothing

''Validar estoque antes de realizar transa��o
'sqlEstoque = "exec ped_ValidarEstoque_sp '"&session("empresa_id")&"',"&pedido&",1"
'response.Write sqlEstoque
'response.End
'adoEstoque = conn1.execute(sqlEstoque)

'if adoEstoque("cod") <> "0" then
    'session("adyen_msg") = "Pedido contem itens sem estoque!"
    'response.Redirect "vo3-gera-pedido-forma-pagamento_mult.asp?id_pedido_site="&pedido&""
'end if



'Fazer os splits para pegar os dados de cada cart�o separado
array_ncartao = split(ncartao,"|")
array_titular = split(titular,"|")
array_cvc = split(cvc,"|")
array_mesval = split(mesval,"|")
array_anoval = split(anoval,"|")
array_valorcartao = split(valorcartao,"|")

'response.write "pedido:" & pedido & "<br>"
'response.write "ncartao:" & ncartao & "<br>"
'response.write "parcelas:" & parcelas & "<br>"
'response.write "valorvista:" & valorvista & "<br>"
'response.write "juros:" & juros & "<br>"
'response.write "valorfinal:" & valorfinal & "<br>"
'response.write "qtd_cartoes:" & qtd_cartoes &"<br>"
'response.write "valor:" & valor & "<br>"

'Usar a Variavel total para calcular os valores para compara��o com valor total enviado via form
'E variavel psp Referencia que � o codigo da transa��o na adyen , logTid � usado pra guardar as transa��es caso aja necessidade de extorno e
'tamb�m sera usado para gerar log na ped_MovimentoStatus
total = 0
pspReference = ""
LogTid = ""
cont = Ubound(array_ncartao)
for x=0 to cont


    total = total + array_valorcartao(x)
    'response.write "<b>Dado do cart�o "&(x)+1&":</b> <br>"
    'response.write "ncartao:" & array_ncartao(x) & "<br>"
    'response.write "titular:" & array_titular(x) & "<br>"
    'response.write "cvc:" & array_cvc(x) & "<br>"
    'response.write "mesval:" & array_mesval(x) & "<br>"
    'response.write "anoval:" & array_anoval(x) & "<br>"
    'response.write "valorcartao:" & array_valorcartao(x) & "<br>"

    if array_ncartao(x) = "" then
        session("adyen_msg") =  "Favor preencher o numero cart�o "&(x)+1
        response.Redirect "vo3-gera-pedido-forma-pagamento_mult.asp"
    end if

    if array_titular(x) = "" then
        session("adyen_msg") = "Favor preencher o titular cart�o "&(x)+1
        response.Redirect "vo3-gera-pedido-forma-pagamento_mult.asp"
    end if
        
    if array_cvc(x) = "" or len(array_cvc(x)) < 3 then
        session("adyen_msg") ="Favor preencher o codigo de seguran�a cart�o "&(x)+1
        response.Redirect "vo3-gera-pedido-forma-pagamento_mult.asp"
    end if

    if array_mesval(x) = "" or len(array_mesval(x)) <> 2 then
        response.Write "Favor preencher o m�s de seguran�a cart�o "&(x)+1
        response.Redirect "vo3-gera-pedido-forma-pagamento_mult.asp"
    end if
        
        if array_anoval(x) = "" or len(array_anoval(x)) <> 4 then
        session("adyen_msg") = "Favor preencher o m�s de seguran�a cart�o "&(x)+1
        response.Redirect "vo3-gera-pedido-forma-pagamento_mult.asp"
    end if

    'response.Write  cdbl(array_valorcartao(x)) &"<br>"
    'response.Write cdbl(50)
    'response.End

    if array_valorcartao(x) = "" or replace(replace(formatNumber(replace(array_valorcartao(x),".",","),2),",",""),".","") < 5000 then
        session("adyen_msg") = "Favor preencher um valor superior a 50 do cart�o "&(x)+1
        response.Redirect "vo3-gera-pedido-forma-pagamento_mult.asp"
    end if    

    if x + 1 = qtd_cartoes then
        if cdbl(total) <> cdbl(valorFinal) then
            session("adyen_msg") = "Valores informados diferentes do total do pedido"    
            response.Redirect "vo3-gera-pedido-forma-pagamento_mult.asp"
        end if        
    end if


    'Por causa da configura��o no sistema da adyen um pedido aprovado n�o realiza mais de um transa��o e como no caso de
    'multiplos cart�es v�o haver diversas transa��o acrescentar um prefixo ao pedido
    prefixo = ""
    if qtd_cartoes > 1 then
        Select Case x
            Case 0:prefixo = "A"

            Case 1:prefixo="B"

            Case 2:prefixo="C"

            Case 3:prefixo="D"

            Case 4:prefixo="E"

            Case 5:prefixo="F"
    
            Case else prefixo="" 

        end Select

    end if



    'x = 2
    'sql = "adyenxml_sp "&replace(replace(formatNumber(array_valorcartao(x),2),".",""),",","")&",'"&array_mesval(x)&"','"&array_anoval(x)&"','"&array_titular(x)&"','"&array_ncartao(x)&"','"&array_cvc(x)&"',"&parcelas&",'','','',1,'"&cstr(pedido&prefixo)&"','"&origem&"','"&ip&"'"
    'response.Write sql & "<br>"
    'sql = "adyenxml_sp "&replace(replace(formatNumber(replace(array_valorcartao(x),".",","),2),".",""),",","")&",'"&array_mesval(x)&"','"&array_anoval(x)&"','"&array_titular(x)&"','"&array_ncartao(x)&"','"&array_cvc(x)&"',"&parcelas&",'','','',1,'"&cstr(pedido&prefixo)&"','"&origem&"','"&ip&"'"
    'response.Write sql
    'response.Write  replace(replace(formatNumber(replace(array_valorcartao(x),".",","),2),",",""),".","")
    'response.End
    



    'Passou por todas as valida��es montar o xml para envio
    sql = "adyenxml_sp "&replace(replace(formatNumber(replace(array_valorcartao(x),".",","),2),",",""),".","")&",'"&array_mesval(x)&"','"&array_anoval(x)&"','"&array_titular(x)&"','"&array_ncartao(x)&"','"&array_cvc(x)&"',"&parcelas&",'','','',1,'"&cstr(pedido&prefixo)&"','"&origem&"','"&ip&"'"
    'response.Write sql
    'response.End
    set adors1  = conn1.execute(sql)
    ' response.Write sql
    ' response.End
    if not adors1.eof then
        xml = adors1("xml")
    end if    
    set adors1 = nothing    
    'response.Write "<br><b> xml cartao "&(x)&"</b><br><br>"&xml& "<br><br>"
    'response.End

    'Parte do envio do Xml
    urlws = "https://pal-live.adyen.com/pal/servlet/soap/Payment" 
    urlws_wsdl = "https://pal-live.adyen.com/pal/Payment.wsdl"
    Set objhttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
    
        objhttp.open "post", urlws, false, "ws_112835@Company.Hinode", "2-VCuXhVQq\La(Tc+w@7+3qRN"
        objhttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
        objhttp.setRequestHeader "SOAPAction", urlws_wsdl
        objhttp.send xml
        retorno = objhttp.responsetext
        'response.Write retorno
        'response.End
        Set objXML = Server.CreateObject("MSXML2.DOMDocument")
        objXML.async = False
        ReturnValue = objXML.LoadXML(retorno)
        if ReturnValue = False Then
            'Se n�o for valido Erro!
	        Set oXMLError = objXML.ParseError
	        Response.Write "&#xa0;&#xa0;" & oXMLError.ErrorCode & " - " & oXMLError.Reason & "  URL=" & oXMLError.URL & "<br>"
	        Set oXMLError = Nothing
            Response.End
        End If

        'If not objXML.loadXML(retorno) then 
        ' report loading error
        'response.Write ("ERRO")
        'end if
    
        Set Root = objXML.documentElement
    
    
        'O status sempre sera retornado!.
        'Pegando a estrutura do XML de resposta. E Validado se cada item existe.
        If (objXML.GetElementsByTagName("resultCode").length <> 0) Then
            Set status = objXML.getElementsByTagName("resultCode")(0)
            var_status = status.text
            set cod = objXML.getElementsByTagName("pspReference")(0)
            var_cod = cod.text ''codigo 
        Else
            var_status = ""
            var_cod = ""
        end if
        
          Select Case var_status
            Case "Refused":var_status = "Recusado"

            Case "Authorised":var_status="Autorizado"

            Case "CANCELLED":var_status="Cancelado"

            Case "PENDING":var_status="Pendente"

            Case "ERROR":var_status="Erro"

            Case else var_status="Erro" 

        end Select
        
        'Passar o primeiro cart�o como autorizado para fins de teste
        'if x = 0 then
            'var_status = "Autorizado"
        'end if

        'Alimentando o log com o codigo das transa��es
        if LogTid = "" then
            LogTid = var_cod
        else
            LogTid = LogTid&","& var_cod
        end if
        'Atualizar e gerar logs
        sqlidlog = "select max(isnull(idlog,0)) as idlog from logxmladyen where idpedido = '"&pedido&prefixo&"'"   
        set adolog = conn1.execute(sqlidlog)
        if not adolog.eof then
            idlog = adolog("idlog")
        end if
        set adolog = nothing
        sql1 = "update logxmladyen set pspreference = '"&var_cod&"', status = '"&var_status&"' where idlog = "&idlog
        'response.Write sql1
        conn1.execute(sql1)
        
        'Caso for mais de um cart�o inserir no log de multiplos cart�es
        if qtd_cartoes > 1 then
        sqlLog2 = "insert into log_adyenMultiplos (totCartoes,Pedido,PedPref,nCartao,pspreference,mensagem,data,usuario,valor) "&_
                  "values("&qtd_cartoes&",'"&pedido&"','"&pedido&prefixo&"',"&x+1&",'"&var_cod&"','"&var_status&"',getdate(),'"&session("usuario")&"','"&replace(replace(formatNumber(array_valorcartao(x),2),".",""),",","")&"')"
        conn1.execute(sqlLog2)
        end if

        'Gerar informa��es de retorno na ficha do pedido para o franquiado saber qual cart�o foi recusado
        sqllog3 = "insert into ped_observacao (pedicodigo,emitente,observacao,complemento,usuario,data) values ('"&pedido&"','"&session("empresa_id")&"',20004,'Cart�o :"&right(array_ncartao(x),4)&"- Retorno :"&var_status&"','"&session("usuario")&"',getdate())"
        'response.Write sqllog3
        conn1.execute(sqllog3)

        if  var_status = "Recusado" or var_status = "Erro" then
            'Caso esteja no segundo cart�o para frente entrar no if
            if x > 0 then
                'Verificar e pegar valor e codigo das transa��es que necessitam ser extornadas
                strTid = "select pspreference , mensagem, valor from log_adyenMultiplos where pspreference in ("&LogTid&") and mensagem like '%Autorizado%' and pedido = '"&pedido&"' "
                set adoTid = conn1.execute(strTid)
                do while not adoTid.eof
                    'Gerar Xml de extorno
                    StrRefund = "exec refund_adyenxml_sp '"&origem&"','"&adoTid("valor")&"','"&adoTid("pspreference")&"' "
                    'StrRefund = "exec refund_adyenxml_sp 'DinamicEscritorio','43424','4414393107927123' "
                    'response.Write strRefund
                    'response.End
                    set adoRefund = conn1.execute(strRefund)
                    
                    if not adoRefund.eof then
                        xmlRefund = adoRefund("xml")
                    end if
                    set adoRefund = nothing
                    'Envio do xml de extorno
                    urlws = "https://pal-live.adyen.com/pal/servlet/soap/Payment" 
                    urlws_wsdl = "https://pal-live.adyen.com/pal/Payment.wsdl"
                    Set objhttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
    
                    objhttp.open "post", urlws, false, "ws_112835@Company.Hinode", "2-VCuXhVQq\La(Tc+w@7+3qRN"
                    objhttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
                    objhttp.setRequestHeader "SOAPAction", urlws_wsdl
                    objhttp.send xmlRefund
                    retorno = objhttp.responsetext

                    response.Write retorno    

                    Set objXML = Server.CreateObject("MSXML2.DOMDocument")
                    objXML.async = False
                    ReturnValue = objXML.LoadXML(retorno)
                    if ReturnValue = False Then
                        'Se n�o for valido Erro!
	                    Set oXMLError = objXML.ParseError
	                    Response.Write "&#xa0;&#xa0;" & oXMLError.ErrorCode & " - " & oXMLError.Reason & "  URL=" & oXMLError.URL & "<br>"
	                    Set oXMLError = Nothing
                        Response.End
                    End If

                    'If not objXML.loadXML(retorno) then 
                    ' report loading error
                    'response.Write ("ERRO")
                    'end if
    
                    Set Root = objXML.documentElement
                    
                    'response.Write xmlRefund
                    'O status sempre sera retornado!.
                    'Pegando a estrutura do XML de resposta. E Validado se cada item existe.
                    If (objXML.GetElementsByTagName("response").length <> 0) Then
                    Set status = objXML.getElementsByTagName("response")(0)
                    refund_status = status.text
                    set cod = objXML.getElementsByTagName("pspReference")(0)
                    refund_cod = cod.text ''codigo 
                    Else
                    refund_status = ""
                    refund_cod = ""
                    end if
                    'Tratando o retorno do extorno
                    Select Case refund_status
                        Case "Refused":refund_status = "Recusado"

                        Case "[refund-received]":refund_status="Extorno Recebido"

                        Case "CANCELLED":refund_status="Cancelado"

                        Case "PENDING":refund_status="Pendente"

                        Case "ERROR":refund_status="Erro"

                        Case else refund_status=refund_status

                    end Select
                    'Inserir um log do xml e da resposta do extorno da transa��o.
                    strRef = "insert into log_adyenXml_Refund (idPedido,xml,data,pspreference,status) values ('"&pedido&"','"&xmlRefund&"',getdate(),'"&adoTid("pspreference")&"','"&refund_status&"') "
                    conn1.execute(strRef)

                    
                    adoTid.movenext
                loop
                set adoTid = nothing
                'Sair do for pois um dos cart�es foi recusado
                exit for
            else
                'Sair do for pois a compra foi recusada no primeiro cart�o
                exit for 
            end if
        
        
        end if    
        
        


next

'Proc que valida o pedido 
sqlFim = "exec ped_AprovarPedidoFranquia2_adyen_sp '"&session("empresa_id")&"','"&pedido&"','"&replace(LogTid,",","|")&"','','"&session("usuario")&"'"
'response.Write sqlFim
'response.End
set adors1 = conn1.execute(sqlFim)

if not adors1.eof then
    pstatus = adors1("status")
end if
set adors1 = nothing
'pstatus = "10"

if pstatus = "10" then
    sqlPed = "exec ped_atualizarDados_sp '"&pedido&"',"&valorvista&","&juros&", "&qtd_cartoes&" "
    conn1.execute(sqlPed)
end if

conn1.close
Response.Redirect("vo3-gera-pedido-finalizado-cdh.asp?id_pedido_oficial="&pedido&"")


%>

<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->




