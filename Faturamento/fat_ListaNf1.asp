<%'@LANGUAGE = VBScript.Encode %>
<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10535)
%>

<script language="javascript">

    function gerarXml(emitente, nota, serie) {
        path = document.frmGerarArquivoXml;

        path.emitenteXml.value = emitente;
        path.notaXml.value = nota;
        path.serieXml.value = serie;
        path.submit();
        //window.iframeGerarArquivoXml.location.href = "nfe_GerarArquivoXml.asp";
        //window.open("nfe_GerarArquivoXml.asp", "gerarArquivoXml");
    }
    
    function gerarXmlCanc(emitente, nota, serie) {
        path = document.frmGerarArquivoXmlCanc;

        path.emitenteXml.value = emitente;
        path.notaXml.value = nota;
        path.serieXml.value = serie;        
        path.submit();
        //window.iframeGerarArquivoXml.location.href = "nfe_GerarArquivoXml.asp";
        //window.open("nfe_GerarArquivoXml.asp", "gerarArquivoXml");
    }

</script>



<%

call form_criar("frmGerarArquivoXml","../nfe/nfe_GerarArquivoXml.asp")
	call txtOculto_criar("emitenteXml","")
	call txtOculto_criar("notaXml","")
	call txtOculto_criar("serieXml","")	
call form_fim()	

call form_criar("frmGerarArquivoXmlCanc","../nfe/nfe_GerarArquivoXmlCanc.asp")
	call txtOculto_criar("emitenteXml","")
	call txtOculto_criar("notaXml","")
	call txtOculto_criar("serieXml","")	
call form_fim()	

empresa_nfe=trim(request("empresa_nfe"))
if len(empresa_nfe)=0 then
	s1="selected"
else
	temp=split(empresa_nfe,"-")
	empresa	=temp(0)
	serie	=temp(1)
end if

status	=trim(request("status"))
strIni	=trim(request("strIni"))
strFim	=trim(request("strFim"))
nota	=trim(request("nota"))
nota2	=trim(request("nota2"))

situacao1	=request("situacao1")
situacao2	=request("situacao2")

email	=trim(request("email"))

if len(strIni)=0 then strIni=date
if len(strFim)=0 then strFim=date
if len(status)=0 then status=0

lbl3	="Emiss�o:"
txt3	=strIni & " - " & strFim
impcab = 1
impressao_tipo="P"

if len(session("libera_impressao_nfe"))=0 then session("libera_impressao_nfe")=0

strsql="select a.situacao,a.emitente,e_nome_abreviado,nota,a.serie,a.nome,referencia,emissao,a.integracao,a.destinatario" & _
		",chave_autenticacao,statusNf,statusNfCor,statusNfDescricao,xMotivo=isnull(xmotivo,'') + isnull(a.responsavel,'')" & _
		",sum(valor_total) total " & _
		",i=case when (statusNf = '1' or statusNf='99' or (" & session("libera_impressao_nfe") & "=1 and statusNf = '15')) and len(chave_autenticacao)>0 then '<input type=''checkbox'' name=''nota'' value=''' + convert(varchar(8),a.emitente) + '$' + convert(varchar(6),nota) + '$' + a.serie + '''>' else '' end " & _		
		",b.cod_grupo,a.usuario,log='log',pedido_edi=isnull(pedido_edi,'') " & _
		",xml_email=isnull(d.xml_email,''),transporte_email=isnull(a.transporte_email,'') " & _
		",impresso=isnull((select top 1 1 from fat_nota_status x where x.emitente=a.emitente and x.nota=a.nota and x.serie=a.serie and status=60),0) " & _
		"from view_notas_resumo a inner join view_depto b on a.emitente=b.destinatario " & _
		"inner join fat_Nfe c on a.emitente=c.emitente and a.serie=c.serie " & _
		"inner join fat_Comercio d on a.destinatario=d.destinatario " & _
		"where a.emitente='" & session("empresa_id") & "' " & _
				" and (a.statusNf=" & status & " or " & status & "=0) " & _
				" and (a.serie='" & serie & "' or " & len(serie) & "=0) "	

if len(situacao1)>0 then
	strsql=strsql & " and a.statusnf between " & situacao1 & " and " & situacao2
end if		
		
if len(nota)>0 then
	strsql=strsql & " and nota>=" & nota
	if len(nota2)>0 then 
		strsql=strsql & " and nota<=" & nota2
	end if
else
	strsql=strsql & " and convert(datetime,convert(varchar(10),emissao,120)) between '" & dt_fim(strIni) & "' and '" & dt_fim(strFim) & "' "
end if
strsql=strsql & "group by a.transporte_email,d.xml_email,emissao,a.responsavel,a.pedido_edi,a.situacao,a.usuario,xmotivo,a.emitente,b.cod_grupo,statusNfCor,statusNfDescricao,chave_autenticacao,statusNf,e_nome_abreviado,nota,a.serie,a.nome,referencia,a.integracao,a.destinatario " & _
		"order by e_nome_abreviado,referencia,nota"
	
'call rw(strsql,0)
set adors1=locConnVenda.execute(strsql)
if len(trim(request("btn2"))) > 0 then call Redi_Arquivo(strSql,"")
soma=0
if not adors1.eof then grupo=adors1("cod_grupo")

p="1"
'target="target='_new'"
opcao_link="1"
txt4="<input type='button' name='btnAtualizar' style='font-family: Verdana; font-size: 8pt' value='Atualizar P�gina' onclick='atualizar(0);'>"


call define_coluna ("Nota","nota","5","c","t","n","s","","")
call define_coluna ("","serie","2","c","t","n","s","","")
call define_coluna ("ID","integracao","6","c","t","n","s","","")
call define_coluna ("nome","nome","*","e","t","n","s","","")
'call define_coluna ("Opera��o","descricao_operacao","12","e","t","n","s","","")
call define_coluna ("total","total","7","d","2","s","s","","")
call define_coluna ("Status","statusNfDescricao","13","c","t","n","s","$statusNfCor","")
call define_coluna ("Mensagem","xMotivo","13","e","t","n","s","$statusNfCor","")
call define_coluna ("Usu�rio","usuario","10","c","t","n","s","","")
call define_coluna ("Danfe<br><input type='checkbox' name='TODOS' value='1' onclick='javascript:CHECKPED()'>","i","3","c","t","n","s","","")
call define_coluna ("Log","1","3","c","t","n","s","","")
call define_coluna ("Xml","2","3","c","t","n","s","","")
call define_coluna ("Xml Canc","3","3","c","t","n","s","","")

if cstr(email)="1" then
	call define_coluna ("E-mail","4","5","c","t","n","s","","")
end if



'link="../nfe/nfe_danfe.asp?nota=[nota]$[serie]"
link="../nfe/nfe_Nf.asp?nf=[nota]&se=[serie]&filial=[emitente]"
call define_link(soma,link)
call define_link(1+soma,link)

link2="../cliente/cli_dados.asp?dest=[destinatario]"
call define_link(3+soma,link2)
call define_link(4+soma,link2)

call define_link(6+soma,"javascript:AbreStatus('[emitente]',[nota],'[serie]')")

link="../nfe/nfe_LogErro.asp?emitente=[emitente]&nota=[nota]&serie=[serie]&strini=" & strini & "&strfim=" & strfim & "&filial=" & filial & "&status=" & status
call define_link(7+soma,link)

if cstr(email)="1" then
	link="mailto:[xml_email];[transporte_email]"
	call define_link (12,link)
end if

call define_quebra ("total Movimento ", "referencia","s","referencia")
'call define_quebra ("total Emiss�o ", "emissao","p","emissao")
call define_quebra ("", "e_nome_abreviado","s","e_nome_abreviado")
	
'call exibe_mensagem_nfe(0)

%>
<form name="frmNota" method="post" action="../nfe/nfe_danfe.asp?print=<%=p%>" <%=target%>>
<%
call txtOculto_criar("grupo",grupo)
if grupo="005" then strParametroTab1=strParametroTab1 & "<tr><td colspan=20 align='right'><input type='checkbox' name='imprime_saida' value='1' checked><font face='verdana' size='1'>Imprime data de Sa�da</font></td></tr>"


call fun_rel_cab()
strParametrotab1=""
'call form_criar("frmNota","../nfe/nfe_danfe.asp")


	call txtOculto_criar("strIni",strini)
	call txtOculto_criar("strFim",strFim)
	'call txtOculto_criar("nota",nota)

conta=0
conta2=0

variavel(4)="[e-mail]"

do while not adoRs1.eof	
	if adors1("statusNf")=1 or (cdbl(session("libera_impressao_nfe"))=cdbl(1) and adors1("statusNf")=15) then 
		conta=conta+1
		variavel(1)=""
	else
		variavel(1)="log"
	    
	end if
	
	if cstr(adors1("statusNf"))="1" OR cstr(adors1("statusNf"))="99" then
	    variavel(2) = "<img src=""../Imagem/Icones/xml.gif"" id=""btnGerarXml"" onclick=""javascript:gerarXml('"&adoRs1("emitente")&"','"&adoRs1("nota")&"','"&adoRs1("serie")&"');"" style=""cursor:hand"" /> "
	    if cstr(adors1("statusNf"))="99" then
			variavel(3) = "<img src=""../Imagem/Icones/xml.gif"" id=""btnGerarXmlCanc"" onclick=""javascript:gerarXmlCanc('"&adoRs1("emitente")&"','"&adoRs1("nota")&"','"&adoRs1("serie")&"');"" style=""cursor:hand"" /> "
	    else
			variavel(3) = ""
	    end if
	else
	    variavel(2) = ""
	    variavel(3) = ""
	end if
		
	intTotal=intTotal+1
	
	corColunaFundo(8)=""
	''verificar se j� foi impresso.
	if adors1("impresso")=1 then corColunaFundo(8)="#red"	
		
	call fun_rel_det()
	adoRs1.movenext
loop
'for i=1 to icoluna
'	formatofont(i)=""
'next
call fun_fim_rel()
call Func_ImpPag(1,iColuna)

'INCLUDES OBRIGAT�RIOS%>
<tr><td colspan="<%=3+soma%>" align="left">
	<%
	if cdbl(conta2)>0 then
		call button_criar("submit","btn2","Gerar NFe","")		
	end if%></td>
		  <td colspan="5" align="left"></td>
		 <td colspan="3" align="right">
	<%
	if cdbl(conta)>0 then
		call button_criar("submit","btn1","Imprimir Danfe","")		
	end if%></td>
	</tr>

</table></td></tr></table>
<%
call form_fim()
%>

<script language="JavaScript">
ok=false;
function CHECKPED() {	
	if(!ok){
	  for (var i=0;i<document.frmNota.elements.length;i++) {
	    var x = document.frmNota.elements[i];
	    if (x.name == 'nota') {		
		    	x.checked = true;
				ok=true;
			}
	    }
	}
	else{
	for (var i=0;i<document.frmNota.elements.length;i++) {
	    var x = document.frmNota.elements[i];
	    if (x.name == 'nota') {		
		    	x.checked = false;
				ok=false;
			}
	    }	
	}
}

function CHECKPED2() {	
	if(!ok){
	  for (var i=0;i<document.frmNota.elements.length;i++) {
	    var x = document.frmNota.elements[i];
	    if (x.name == 'notagerar') {		
		    	x.checked = true;
				ok=true;
			}
	    }
	}
	else{
	for (var i=0;i<document.frmNota.elements.length;i++) {
	    var x = document.frmNota.elements[i];
	    if (x.name == 'notagerar') {		
		    	x.checked = false;
				ok=false;
			}
	    }	
	}
}
</script>

<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->


<script>
function AbreStatus(filial,nota,serie)	{
	window.open("fat_ListaStatus.asp?nota=" + nota + "&serie=" + serie + "&filial=" + filial,"Status","scrollbars=yes,toobar=no,width=600,height=320,screenX=0,screenY=0,top=100,left=100");	
						}
function atualizar(opcao) {
	if (opcao==0) {		
		window.location.reload();	
		}
	if (opcao==1) {		
		location.href='../comercio/faturamento/emissao/fat_inicio.asp';
		}
	if (opcao==2) {		
		location.href='../comercio/faturamento/emissao/fat_inicio.asp';		
		}
	
}						
</script>