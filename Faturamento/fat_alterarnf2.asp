<%@LANGUAGE = VBScript.Encode %>
<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10460)

nota		=request("nota")
serie		=request("serie")
emitente	=request("emitente")
destinatario=request("destinatario")
pedido		=request("pedido")
opcao		=request("opcao")
tabela		=request("tabela")


if opcao="C" then ''mudar destinatario
	novodest	=trim(request("novodest"))
	if len(novodest)<>8 then 
		session("msg")="C�digo de cliente inv�lido"
		Response.Redirect "fat_Alterarnf.asp"
	end if

	strsql=	"update " & tabela & " set destinatario='" & novodest & "',destinatario_entrega='" & novodest & "' " & _
			"where emitente='" & emitente & "' and nota=" & nota & " and serie='" & serie & "'"
	'call rw(strsql,1)
	locconnvenda.execute(strsql)
	
	strsql="delete fat_saida_endereco where emitente='" & emitente & "' and nota=" & nota & " and serie='" & serie & "'"
	locconnvenda.execute(strsql)
	
	strsql="insert into fat_saida_endereco (emitente,nota,serie,pedido,nome,cgc_cpf,inscricao_rg,logradouro,endereco " & _
		",numero,complemento,bairro,cidade,estado,cep,pais " & _
		",e_nome,e_cgc_cpf,e_inscricao_rg,e_logradouro,e_endereco " & _
		",e_numero,e_complemento,e_bairro,e_cidade,e_estado,e_cep,e_pais) " & _
		"select '" & emitente & "'," & nota & ",'" & serie & "'," & pedido & _
		",nome,cgc_cpf,inscricao_rg,logradouro,endereco " & _
		",numero,complemento,bairro,cidade,estado,cep,pais " & _
		",nome,cgc_cpf,inscricao_rg,logradouro,endereco " & _
		",numero,complemento,bairro,cidade,estado,cep,pais " & _
		"from fat_destinatario where destinatario='" & novodest & "'"
	locconnvenda.execute(strsql)

	strsql= "insert into ped_movimentoStatus (emitente,pedicodigo,stnfcodigo,datamovimento,obs,usuario) " & _
			"select emitente,pedicodigo,stnfcodigo,getdate(),'NF Alterada (Destinat�rio). De " & destinatario & " para " & novodest & "','" & session("usuario") & "' "  & _
			"from ped_pedidoTotal " & _
			"where pedicodigo=" & pedido
	'call rw(strsql,1)
	locconnvenda.execute(strsql)
	
elseif opcao="D" then ''mudar data
	data		=trim(request("data"))
	referencia	=trim(request("referencia"))
	if len(data)=0 then 
		session("msg")="Data inv�lida, verifique."
		Response.Redirect "fat_Alterarnf.asp"
	end if
	
	strsql=	"update " & tabela & " set referencia='" & dt_fim(data) & "',emissao='" & dt_fim(data) & "',dtFaturamento='" & dt_fim(data) & "' " & _
			"where emitente='" & emitente & "' and nota=" & nota & " and serie='" & serie & "'"
	'call rw(strsql,1)			
	locconnvenda.execute(strsql)

	strsql= "insert into ped_movimentoStatus (emitente,pedicodigo,stnfcodigo,datamovimento,obs,usuario) " & _
			"select emitente,pedicodigo,stnfcodigo,getdate(),'NF Alterada (Data). De " & referencia & " para " & data & "','" & session("usuario") & "' "  & _
			"from ped_pedidoTotal " & _
			"where pedicodigo=" & pedido
	'call rw(strsql,1)
	locconnvenda.execute(strsql)
elseif opcao="OP" then ''operacao
	operacao	=request("operacao")
	if len(operacao)=0 then 
		session("msg")="Opera��o inv�lida, verifique."
		Response.Redirect "fat_Alterarnf.asp"
	end if
	
	strsql=	"update " & tabela & " set operacao=" & operacao & " " & _
			"where emitente='" & emitente & "' and nota=" & nota & " and serie='" & serie & "' and produto<>'999999'"
	'call rw(strsql,1)			
	locconnvenda.execute(strsql)
	
	strsql= "update a set cfo=case when b.estado='EX' then cfopais when b.estado=d.estado then c.cfo when b.estado<>d.estado then c.cfofora end " & _
			"from " & tabela & " a, fat_destinatario b, fat_operacao c, fat_Destinatario d " & _
			"where emitente='" & emitente & "' and nota=" & nota & " and serie='" & serie & "' " & _
			"and a.destinatario=b.destinatario and a.operacao=c.operacao and a.emitente=d.destinatario "
	locconnvenda.execute(strsql)	

	strsql= "insert into ped_movimentoStatus (emitente,pedicodigo,stnfcodigo,datamovimento,obs,usuario) " & _
			"select emitente,pedicodigo,stnfcodigo,getdate(),'NF Alterada (Opera��o). " & operacao & "','" & session("usuario") & "' "  & _
			"from ped_pedidoTotal " & _
			"where pedicodigo=" & pedido
	'call rw(strsql,1)
	locconnvenda.execute(strsql)	
end if

session("msg")="Alterado com sucesso."
Response.Redirect "fat_Alterarnf.asp"
%>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->