<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%
'C�DIGO DA APLICA��O (SYS_APLICA��O)
call Verifica_Permissao(11302)
%>
<!--#include virtual='public/pub_Body.asp'-->
<style type="text/css">
    .select_menor {
        width: 40%;
    }
    input:disabled {
        background-color: #dcdcdc;
    }
    input[name="integracao"]:disabled {
        font-weight: bold;
    }
</style>
<script type="text/javascript" src="../js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="../js/jquery.maskMoney.js"></script>
<script type="text/javascript">
    $(function () {
        $("input[name='valor']").maskMoney({ prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false });
    })
</script>
<%
call rel_Criar(70,"frm1","fr_gravarDeposito1.asp")

    nomeBotao = "Gravar"

    strSqlFranquiaID = "select integracao as ID from fat_comercio where destinatario = '" & session("Empresa") & "'"
    set adoFranquiaID = conn1.execute(strSqlFranquiaID)

    if not adoFranquiaID.eof then
        franquia_id = adoFranquiaID("ID")
    else
        franquia_id = 0
    end if
    set adoFranquiaID = nothing

    rqr = "<font color='red'><b>*</b></font>"

    call Rel_Item_Texto("<center>Identifica��o dep�sito","")

    call Rel_item_txt(40,60,"Seu ID","texto","integracao",15,8,franquia_id&"' disabled ")

    call Rel_Item_Texto("<center>Dados dep�sito","")
	
	call Rel_Item_SelectINI(50,50,"Conta Corrente","conta_corrente","")
	    strSqlBanco="exec sys_listaBanco_sp"
	    set adoBanco=locconnvenda.execute(strSqlBanco)
		do while not adoBanco.eof
			call Select_Item(adoBanco("conta_corrente"), adoBanco("nomeconta"),"")
			adoBanco.movenext
		loop
		set adoBanco=nothing
	call Rel_Item_SelectFIM()

    call Rel_item_txt(40,60,rqr & "Documento","texto","documento",15,12,"")

    call Rel_item_txt(40,60,rqr & "Data do Dep�sito","data","dt_deposito",15,10,date)

    call Rel_item_txt(40,60,rqr & "Valor","texto","valor",15,15,"")

    call Rel_item_txt(40,60,"Data do Movimento","data","dt_movimento",15,10,date & "' disabled ")

	
    ''Finalidade - definido como dep�sito
	strsql="select finalidade, descricao from sys_finalidade where finalidade = 25"
	'call rw(strsql,0)
	set adoRs1=locconnvenda.execute(strSql)
	
	call Rel_Item_SelectINI(50,50,"Finalidade","finalidade","class='select_menor'")
		do while not adoRs1.eof
			call Select_Item(adoRs1("finalidade"),adoRs1("descricao"),"selected")			
			adoRs1.movenext
		loop
		set adoRs1=nothing		
	call Rel_Item_SelectFIM()

    ''Observa��o
    call Rel_TextArea_Criar(40,60,"Observa��o","observacao' placeholder='M�ximo 80 caracteres!'",3,40,"")

    call Rel_Item_Texto("&nbsp;&nbsp;&nbsp;" & rqr & " Campos obrigat�rios!","")
	
call rel_Fim(0)
%>

<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->
