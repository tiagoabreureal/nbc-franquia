<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<!--#include virtual="public/pub_Componentes.asp"-->
<%

id_pedido_oficial = REQUEST("id_pedido_site")


'Caso o pedido seja capturado pela hinode e o usuário do cdh tentar faturar o pedido novamente entrar na validação Nilson 15/01/2015 Solicitado por Andre
strSql = "select stnfCodigo from ped_pedidoTotal where nfreferencia = "&id_pedido_oficial&" "
set adorsc = locConnVenda.execute(strSql)

status = adorsc("stnfCodigo")

if status = 10 then
    call redi_aviso("Captura do Pedido","Esse pedido já esta com status pagamento efetuado.")
end if


%>
<!----------- //////////////////// NOVO VO ABAIXO /////////////////////////////////// --->           
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Hinode - VO</title>

        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- basic styles -->

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/css/font-awesome.min.css" />

        <!--[if IE 7]>
          <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
        <![endif]-->

        <!-- page specific plugin styles -->

        <!-- fonts -->

        <link rel="stylesheet" href="assets/css/ace-fonts.css" />

        <!-- ace styles -->

        <link rel="stylesheet" href="assets/css/ace.min.css" />
        <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />

        <!--[if lte IE 8]>
          <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->

        <script src="assets/js/ace-extra.min.js"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->


        <!-- Hinode -->
        <link rel="stylesheet" href="assets/css/hinode.css" />
        
<!-- basic scripts -->

<!--[if !IE]> -->

<script type="text/javascript">
    window.jQuery || document.write("<scr" + "ipt src='assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
window.jQuery || document.write("<scr"+"ipt src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

<script type="text/javascript">
    if ("ontouchend" in document)
        document.write("<scr" + "ipt src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/typeahead-bs2.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="assets/js/excanvas.min.js"></script>
<![endif]-->

<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<script src="assets/js/jquery.easy-pie-chart.min.js"></script>
<script src="assets/js/jquery.sparkline.min.js"></script>
<script src="assets/js/flot/jquery.flot.min.js"></script>
<script src="assets/js/flot/jquery.flot.pie.min.js"></script>
<script src="assets/js/flot/jquery.flot.resize.min.js"></script>

<!-- ace scripts -->

<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>

<!-- inline scripts related to this page -->

<!-- Cycle -->
<!--<script src="hinode/js/jquery.cycle.all.js" ></script>
<script type="text/javascript">
$('#slide').cycle({ 
    fx:     'scrollHorz', 
    speed:  1000, 
    timeout: 200, 
    next:   '#next_slide', 
    prev:   '#prev_slide' 
});

</script>-->       
       
        
 
 <style>
.navbar {
background: #ffffff; /* Old browsers */
background: -moz-linear-gradient(left,  #ffffff 0%, #fff8e2 51%, #ffcb07 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, right top, color-stop(0%,#ffffff), color-stop(51%,#fff8e2), color-stop(100%,#ffcb07)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(left,  #ffffff 0%,#fff8e2 51%,#ffcb07 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(left,  #ffffff 0%,#fff8e2 51%,#ffcb07 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(left,  #ffffff 0%,#fff8e2 51%,#ffcb07 100%); /* IE10+ */
background: linear-gradient(to right,  #ffffff 0%,#fff8e2 51%,#ffcb07 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffcb07',GradientType=1 ); /* IE6-9 */

}
 
 
.ace-nav > li.light-blue > a {
    background-color: #ffcb07;
		background:none;
}

.ace-nav > li.open.light-blue > a {
    background-color: #ffcb07 !important;
	background:none;

}

.ace-nav > li.light-blue > a:hover{
	background:none;
	color: #d15b47;
}
.ace-nav > li.light-blue > a:focus {
    background-color: #ffcb07;
	background:none;
	color: #d15b47;
}

.ace-nav > li > a {
    color: #d15b47;
}



.ace-nav > li.amarelo > a {
    /*background-color: #ffcb07;*/
	background:none;
}

.ace-nav > li.open > a {
    background-color: #ffcb07 !important;
}

.ace-nav > li > a > [class*="icon-"] {
    color: #d15b47;
}

.ace-nav > li.open > a > [class*="icon-"] {
    color: #fff;
}


.ace-nav > li {
    border-left: 1px solid #ffcb07;
    float: left !important;
    height: 45px;
    line-height: 45px;
    margin-top: 24px;
    position: relative;
}
 
 
 .navbar .navbar-header {
    float: right;
    margin: 0 !important;
}
 
 </style>

 
 
 
 
 
    </head>


<div class="main-container" id="main-container">

    <script type="text/javascript">

        try {
            ace.settings.check('main-container', 'fixed');
        }

        catch (e) {
        }


        $(document).ready(function () {


            //Carrega Pedido
            //var vid_pedido_oficial = "921236";
            var vid_pedido_oficial = "<%=id_pedido_oficial%>";


            if (vid_pedido_oficial != "") {

                $.ajax({
                    url: 'ajax/vo3_ajax_consultor_gera_pedido.asp',
                    type: 'POST',
                    data: { acao: 'lista_pedido', id_pedido_oficial: vid_pedido_oficial },
                    dataType: "json",

                    beforeSend: function () {
                        $('#carregando').show(100);
                    },

                    complete: function () {
                        $('#carregando').hide(100);
                    },

                    success: function (retorno) {

                        $.each(retorno, function (i, item) {

                            //Nova Regra de Parcelamento para Kits Upgrade - Ewerton - 22/04/2015
                            if (item.upgrade !== null) {
                                $("#upgrade").val(item.upgrade);
                                $("#upgrade_produto_anterior").val(item.upgrade_produto_anterior);
                            }

                            $("#vl_total_pedido").val(item.Ped_vl_total);
                            $("#vl_total_pedido_bkp").val(item.Ped_vl_total);
                            $('#txt_vl_total_pedido').html('R$ ' + number_format(item.Ped_vl_total, 2, ',', '.'));
                            $('#txt_pontos_total_pedido').html('' + number_format(item.Ped_pontos, 2, ',', '.'));
                            $("#id_pedido_oficial").val(item.IdPedido_oficial)
                            //alert($("#vl_total_pedido").val());						
                        });

                        Fc_pedido_item(vid_pedido_oficial);

                    }, //success

                    error: function (retorno) {
                        //console.log(retorno);
                        if (retorno.status == 404) {
                            //$('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
                            bootbox.alert("Houve um erro: " + retorno.status + " " + retorno.statusText, function () { });
                        }

                        else {
                            //$('#retorno').html('Houve um erro desconhecido entre em contato com o administrador...');
                            bootbox.alert("Houve um erro desconhecido entre em contato com o administrador...", function () { });
                        }

                    } //error

                }); //ajax

            } //if

            else {
                bootbox.alert("Pedido não encontrado!", function () { });
            } //else


            //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


            function Fc_pedido_item(vid_pedido_oficial) {

                //XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                $.ajax({
                    url: 'ajax/vo3_ajax_consultor_gera_pedido.asp',
                    type: 'POST',
                    data: { acao: 'lista_pedido_item', id_pedido_oficial: vid_pedido_oficial },
                    dataType: "json",

                    beforeSend: function () {
                        $('#carregando').show(100);
                    },

                    complete: function () {
                        $('#carregando').hide(100);
                    },

                    success: function (retorno) {

                        if (retorno.length > 0) {

                            $.each(retorno, function (i, item) {

                                //Inclusão Novos Kits - Ewerton - 14/04/2015
                                if (parseInt(item.PedItem_prod_codigo, 10) == 7250 || parseInt(item.PedItem_prod_codigo, 10) == 7251 || parseInt(item.PedItem_prod_codigo, 10) == 7252 || parseInt(item.PedItem_prod_codigo, 10) == 7253) {

                                    $("#codigokit").val(parseInt(item.PedItem_prod_codigo, 10));

                                };

                            }); //$.each

                        } //if

                        else {
                            bootbox.alert("<h4>Item do Pedido Não Encontrado!</h4>", function () { });
                        }

                    }, //success

                    error: function (retorno) {

                        if (retorno.status == 404) {
                            bootbox.alert("Houve um erro: " + retorno.status + " " + retorno.statusText, function () { });
                        }

                        else {
                            bootbox.alert("Houve um erro desconhecido entre em contato com o administrador...", function () { });
                        }

                    } //error

                })//AJAX

            } //Fim da function Fc_pedido_item()


            //Direciona para o pagamento
            $("#bt_continuar").click(function (event) {
                var valorExcedente = "0";

                var codigoKit = $("#codigokit").val();
                var vid_pedido_oficial = parseInt($("#id_pedido_oficial").val(), 10);
                var vss_pg = $("#ss_pg").val();
                var vid_cons = $("#id_cons").val();
                var vmodo_entrega = $("#modo_entrega").val();
                var vnome_cons = $("#nome_cons").val();
                var vid_cdh_retira = $("#id_cdh_retira").val();
                var vid_cdh_retira_desc = $("#id_cdh_retira_desc").val();
                var vemail_ped = $("#email_ped").val();
                var vcdh_retira_email = $("#cdh_retira_email").val();

                var vcep = $("#cep").val();
                var vendereco = $("#endereco").val();
                var vnumero = $("#numero").val();
                var vcomplemento = $("#complemento").val();
                var vbairro = $("#bairro").val();
                var vcidade = $("#cidade").val();
                var vestado = $("#estado").val();

                var vid_forma_pag = $("#id_forma_pag").val();
                var vdesc_forma_pag = $("#desc_forma_pag").val();

                var vdesc_forma_pag_verf = $("#desc_forma_pag_verf").val();

                var vvl_total_pedido_frete = $("#vl_total_pedido_frete").val();
                var vvl_total_pedido = $("#vl_total_pedido").val();
                var vvl_sub_total_pedido = $("#vl_sub_total_pedido").val();
                var vpontos_total_pedido = $("#pontos_total_pedido").val();
                var vpeso_total_pedido = $("#peso_total_pedido").val();
                var vforma_envio_transp = $("#forma_envio_transp_ped").val();
                var vforma_envio_transp_desc_ped = $("#forma_envio_transp_desc_ped").val();
                var vprazo_transp_ped = $("#prazo_transp_ped").val();

                var vvl_credito_usado = $("#vl_credito_usado").val();

                var vqtd_parc = $("#qtd_parc").val();
                var vped_juros = 0; //Alterado 29/11/13 $("#ped_juros").val();
                var vqtd_parc_auto = $("#qtd_parc_auto").val();

                if (vqtd_parc_auto == '') { vqtd_parc_auto = 0; }
                var vvbin6 = $("#vbin6").val();
                var vcnome = $("#cnome").val();
                var vcseg = $("#cseg").val();
                var vcmes = $("#cmes").val();
                var vcano = $("#cano").val();

                //alert("vid_forma_pag:"+vid_forma_pag+" | vid_forma_pag:"+vid_forma_pag);
                //alert("Bin:"+vvbin6+" | Nome:"+vcnome+" | Seg:"+vcseg+" | Mes:"+vcmes+" | Ano:"+vcano);

                var VvalorDesconto = $("#valorDesconto").val();

                var valorTotalPedidoJuros = $("#vl_total_pedido_juros").val();
                var vtel = $("#tel").val();

                if ((parseFloat(valorTotalPedidoJuros) - parseFloat(vvl_total_pedido)) > 0) {
                    vped_juros = parseFloat(valorTotalPedidoJuros) - parseFloat(vvl_total_pedido);
                }

                //if(vdesc_forma_pag_verf=="2" || vdesc_forma_pag_verf=="3"){
                //  vqtd_parc = 0;
                //}




                if (vid_forma_pag == "") {
                    bootbox.alert("<h4>Favor selecione uma forma de pagamento.</h4>", function () { });
                }

                else if (vdesc_forma_pag_verf == "1" && vqtd_parc == "") {
                    bootbox.alert("<h4>Favor selecione uma quantidade de parcela.</h4>", function () { });
                    $("#qtd_parc").focus();
                }

                else if (vdesc_forma_pag_verf == "1" && vvbin6 == "") {
                    bootbox.alert("<h4>Favor informar os números do cartão.</h4>", function () { });
                    $("#vbin6").focus();
                }

                else if (vid_forma_pag != 7 && vid_forma_pag != 10 && vdesc_forma_pag_verf == "1" && vvbin6.length <= 5) {
                    bootbox.alert("<h4>Favor informar os 6 primeiros números do cartão.</h4>", function () { });
                    $("#vbin6").focus();
                }

                else if ((vid_forma_pag == 7 || vid_forma_pag == 10) && vdesc_forma_pag_verf == "1" && vvbin6.length <= 15) {
                    bootbox.alert("<h4>Favor informar os 16 números do cartão.</h4>", function () { });
                    $("#vbin6").focus();
                }

                else if ((vid_forma_pag == 7 || vid_forma_pag == 10) && vcnome == "") {
                    bootbox.alert("<h4>Favor informe o nome do titular que está impresso no cartão.</h4>", function () { });
                    $("#cnome").focus();
                }

                else if ((vid_forma_pag == 7 || vid_forma_pag == 10) && vcseg == "") {
                    bootbox.alert("<h4>Favor informe o código de segurança do cartão.</h4>", function () { });
                    $("#cseg").focus();
                }

                else if ((vid_forma_pag == 7 || vid_forma_pag == 10) && vcmes == "") {
                    bootbox.alert("<h4>Favor selecione o mês de validade do cartão.</h4>", function () { });
                    $("#cmes").focus();
                }

                else if ((vid_forma_pag == 7 || vid_forma_pag == 10) && vcano == "") {
                    bootbox.alert("<h4>Favor selecione o ano de validade do cartão.</h4>", function () { });
                    $("#cano").focus();
                }

                else {

                    //alert("id_pedido_oficial="+vid_pedido_oficial+"&vbin6="+vvbin6+"&cnome="+vcnome+"&cseg="+vcseg+"&cmes="+vcmes+"&cano="+vcano+"&valorTotalPedidoJuros="+valorTotalPedidoJuros);	

                    //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX MODIFICA A FORMA DE PAGAMENTO XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                    $.ajax({
                        url: 'ajax/vo3_ajax_consultor_gera_pedido.asp',
                        type: 'POST',
                        data: { acao: 'alt_pedido', id_pedido_oficial: vid_pedido_oficial, id_forma_pag: vid_forma_pag, desc_forma_pag: vdesc_forma_pag,
                            ped_juros: vped_juros, qtd_parcela_card: vqtd_parc, ped_tipo: 'CDH>CLIENTE'
                        },

                        dataType: "text",

                        beforeSend: function () {
                            $('#carregando').show(100);
                        },

                        complete: function () {
                            $('#carregando').hide(100);
                        },

                        success: function (retorno) {

                            var Vrs = retorno.split('|');

                            if (parseInt(Vrs[0], 10) > 0) {
                                window.location.href = "mw-anti-fraude-cdh.asp?id_pedido_oficial=" + vid_pedido_oficial + "&vbin6=" + vvbin6 + "&cnome=" + vcnome + "&cseg=" + vcseg + "&cmes=" + vcmes + "&cano=" + vcano + "&valorTotalPedidoJuros=" + valorTotalPedidoJuros;
                            }

                            else {
                                bootbox.alert("Houve um erro ao atualizar o pedido, revise seus dados", function () { });
                            }

                        }, //success

                        error: function (retorno) {
                            //console.log(retorno);
                            if (retorno.status == 404) {
                                //$('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
                                bootbox.alert("Houve um erro: " + retorno.status + " " + retorno.statusText, function () { });
                            }

                            else {
                                //$('#retorno').html('Houve um erro desconhecido entre em contato com o administrador...');
                                bootbox.alert("Houve um erro desconhecido entre em contato com o administrador !" + retorno, function () { });
                            }

                        } //error

                    }); //ajax

                } //else

            })//$("#bt_continuar").click


            //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX			


            $("#visa").click(function () {
                //if($("#id_cons").val() == '00120698')
                //{
                $('.reset').hide();
                $("#cartao_vazio").hide('slow');
                $("#cartao_dados").show('slow');
                $("#cartao-visa").show('slow');
                $("#desc_forma_pag").val('Visa');
                $("#desc_forma_pag_verf").val('1'); //Cartão
                $("#id_forma_pag").val('2');

                $("#tr_cnome").show('slow');
                $("#tr_cnome_info").show('slow');
                $("#tr_cseg").show('slow');
                $("#tr_cmes_cano").show('slow');
                $("#txtnum").html("digite os números do cartão");
                $("#vbin6").attr("maxlength", "16");
                $("#txt_vl_parcela").html("");
                vvl_total_pedido = $("#vl_total_pedido_bkp").val();
                $("#vl_total_pedido").val(vvl_total_pedido);
                $("#qtd_parc").val("");

                Fc_obter_parcelas();
            }); //visa


            $("#mastercard").click(function () {
                /*if($("#id_cons").val() == '00120698')
                {*/
                $('.reset').hide();
                $("#cartao_vazio").hide('slow');
                $("#cartao_dados").show('slow');
                $("#cartao-master").show('slow');
                $("#desc_forma_pag").val('Master Card');
                $("#desc_forma_pag_verf").val('1'); //Cartão
                $("#id_forma_pag").val('1');

                $("#tr_cnome").show('slow');
                $("#tr_cnome_info").show('slow');
                $("#tr_cseg").show('slow');
                $("#tr_cmes_cano").show('slow');
                $("#txtnum").html("digite os números do cartão");
                $("#vbin6").attr("maxlength", "16");
                $("#txt_vl_parcela").html("");
                vvl_total_pedido = $("#vl_total_pedido_bkp").val();
                $("#vl_total_pedido").val(vvl_total_pedido);
                $("#qtd_parc").val("");

                Fc_obter_parcelas();
            }); //mastercard


            $("#aura").click(function () {

                $('.reset').hide();
                $("#cartao_vazio").hide('slow');
                $("#cartao_dados").show('slow');
                $("#cartao-aura").show('slow');
                $("#desc_forma_pag").val('Crédito Aura');
                $("#desc_forma_pag_verf").val('1'); //Cartão
                $("#id_forma_pag").val('10');

                $("#tr_cnome").show('slow');
                $("#tr_cnome_info").show('slow');
                $("#tr_cseg").show('slow');
                $("#tr_cmes_cano").show('slow');
                $("#txtnum").html("digite os números do cartão");
                $("#vbin6").attr("maxlength", "16");
                $("#txt_vl_parcela").html("");
                vvl_total_pedido = $("#vl_total_pedido_bkp").val();
                $("#vl_total_pedido").val(vvl_total_pedido);
                $("#qtd_parc").val("");

                Fc_obter_parcelas();
            }); //aura


            $("#american").click(function () {
                /*if($("#id_cons").val() == '00120698')
                {*/
                $('.reset').hide();
                $("#cartao_vazio").hide('slow');
                $("#cartao_dados").show('slow');
                $("#cartao-american").show('slow');
                $("#desc_forma_pag").val('American Express');
                $("#desc_forma_pag_verf").val('1'); //Cartão
                $("#id_forma_pag").val('4');

                $("#tr_cnome").show('slow');
                $("#tr_cnome_info").show('slow');
                $("#tr_cseg").show('slow');
                $("#tr_cmes_cano").show('slow');
                $("#txtnum").html("digite os números do cartão");
                $("#vbin6").attr("maxlength", "16");
                $("#txt_vl_parcela").html("");
                vvl_total_pedido = $("#vl_total_pedido_bkp").val();
                $("#vl_total_pedido").val(vvl_total_pedido);
                $("#qtd_parc").val("");

                Fc_obter_parcelas();
            }); //american


            $("#diners").click(function () {
                /*if($("#id_cons").val() == '00120698')
                {*/
                $('.reset').hide();
                $("#cartao_vazio").hide('slow');
                $("#cartao_dados").show('slow');
                $("#cartao-diners").show('slow');
                $("#desc_forma_pag").val('Crédito Diners');
                $("#desc_forma_pag_verf").val('1'); //Cartão
                $("#id_forma_pag").val('3');

                $("#tr_cnome").show('slow');
                $("#tr_cnome_info").show('slow');
                $("#tr_cseg").show('slow');
                $("#tr_cmes_cano").show('slow');
                $("#txtnum").html("digite os números do cartão");
                $("#vbin6").attr("maxlength", "16");
                $("#txt_vl_parcela").html("");
                vvl_total_pedido = $("#vl_total_pedido_bkp").val();
                $("#vl_total_pedido").val(vvl_total_pedido);
                $("#qtd_parc").val("");

                Fc_obter_parcelas();
            }); //diners


            $("#hipercard").click(function () {

                $('.reset').hide();
                $("#cartao_vazio").hide('slow');
                $("#cartao_dados").show('slow');
                $("#cartao-hipercard").show('slow');
                $("#desc_forma_pag").val('Crédito Hipercard');
                $("#desc_forma_pag_verf").val('1'); //Cartão
                $("#id_forma_pag").val('7');

                $("#tr_cnome").show('slow');
                $("#tr_cnome_info").show('slow');
                $("#tr_cseg").show('slow');
                $("#tr_cmes_cano").show('slow');
                $("#txtnum").html("digite os números do cartão");
                $("#vbin6").attr("maxlength", "16");
                $("#txt_vl_parcela").html("");
                $("#qtd_parc").val("");

                vvl_total_pedido = $("#vl_total_pedido_bkp").val();
                $("#vl_total_pedido").val(vvl_total_pedido);

                Fc_obter_parcelas();
            }); //hipercard


            $("#boleto-banc").click(function () {

                $('.reset').hide();
                $("#cartao_vazio").hide('slow');
                $("#cartao_dados").hide();
                $("#boleto-bancario").show('slow');
                $("#desc_forma_pag").val('Boleto Bancário');
                $("#desc_forma_pag_verf").val('2'); //Boleto
                $("#id_forma_pag").val('5');

                $("#tr_cnome").hide();
                $("#tr_cnome_info").hide();
                $("#tr_cseg").hide();
                $("#tr_cmes_cano").hide();
                $("#qtd_parc").val("1");
                $("#ped_juros").val("0");

                if ($("#vl_total_pedido_bkp").val() > 0) {
                    vvl_total_pedido = eval($("#vl_total_pedido_bkp").val()) + 1.50;
                }

                else {
                    vvl_total_pedido = eval($("#vl_total_pedido_bkp").val());
                }

                $('#txt_vl_parcela').html('R$ ' + number_format(vvl_total_pedido, 2, ',', '.'));
                $("#vl_total_pedido").val(vvl_total_pedido);
            }); //boleto-banc


            $("#dep-id").click(function () {

                $('.reset').hide();
                $("#cartao_vazio").hide('slow');
                $("#cartao_dados").hide();
                $("#dep-identificado").show('slow');
                $("#desc_forma_pag").val('Depósito Identificado');
                $("#desc_forma_pag_verf").val('2'); //Boleto
                $("#id_forma_pag").val('6');

                $("#tr_cnome").hide();
                $("#tr_cnome_info").hide();
                $("#tr_cseg").hide();
                $("#tr_cmes_cano").hide();
                $("#txt_vl_parcela").html("");
                $("#qtd_parc").val("1");
                $("#ped_juros").val("0");
                vvl_total_pedido = $("#vl_total_pedido_bkp").val();
                $("#vl_total_pedido").val(vvl_total_pedido);
            }); //dep-id


            $("#pag-cd-cdh").click(function () {

                $('.reset').hide();
                $("#cartao_vazio").hide('slow');
                $("#cartao_dados").hide();
                $("#pag-cd-cdh-div").show('slow');
                $("#desc_forma_pag").val('PAGAMENTO NO CDH');
                $("#desc_forma_pag_verf").val('2'); //Boleto
                $("#id_forma_pag").val('17');

                $("#tr_cnome").hide();
                $("#tr_cnome_info").hide();
                $("#tr_cseg").hide();
                $("#tr_cmes_cano").hide();
                $("#txt_vl_parcela").html("");
                $("#qtd_parc").val("1");
                $("#ped_juros").val("0");
                vvl_total_pedido = $("#vl_total_pedido_bkp").val();
                $("#vl_total_pedido").val(vvl_total_pedido);
            }); //pag-cd-cdh


            $("#pag-brad").click(function () {

                $('.reset').hide();
                $("#cartao_vazio").hide('slow');
                $("#cartao_dados").hide();
                $("#pag-online-brad").show('slow');
                $("#desc_forma_pag").val('Pagamento Bradesco');
                $("#desc_forma_pag_verf").val('3'); //PagOnline
                $("#id_forma_pag").val('9');

                $("#tr_cnome").hide();
                $("#tr_cnome_info").hide();
                $("#tr_cseg").hide();
                $("#tr_cmes_cano").hide();
                $("#txt_vl_parcela").html("");
                $("#qtd_parc").val("1");
                $("#ped_juros").val("0");

                vvl_total_pedido = $("#vl_total_pedido_bkp").val();
                $("#vl_total_pedido").val(vvl_total_pedido);
            }); //pag-brad


            $("#pag-itau").click(function () {

                $('.reset').hide();
                $("#cartao_vazio").hide('slow');
                $("#cartao_dados").hide();
                $("#pag-online-itau").show('slow');
                $("#desc_forma_pag").val('Itaú Shopline');
                $("#desc_forma_pag_verf").val('3'); //PagOnline
                $("#id_forma_pag").val('14');

                $("#tr_cnome").hide();
                $("#tr_cnome_info").hide();
                $("#tr_cseg").hide();
                $("#tr_cmes_cano").hide();
                $("#txt_vl_parcela").html("");
                $("#qtd_parc").val("1");
                $("#ped_juros").val("0");

                vvl_total_pedido = $("#vl_total_pedido_bkp").val();
                $("#vl_total_pedido").val(vvl_total_pedido);
            }); //pag-itau


        });    //$(document).ready



        function Fc_obter_parcelas() {

            var vid_forma_pag = $("#id_forma_pag").val();
            var vvl_total_pedido = $("#vl_total_pedido").val();

            vvl_total_pedido = parseFloat(vvl_total_pedido);

            var vvl_total_pedido_bkp = vvl_total_pedido;
            var vqtd_parc_auto = $("#qtd_parc_auto").val() == "" ? "0" : parseInt($("#qtd_parc_auto").val(), 10);

            var valorExcedente = "0";
            var codigoKit = $("#codigokit").val();
            var upgrade = $("#upgrade").val();
            var upgrade_produto_anterior = $("#upgrade_produto_anterior").val();

            valorExcedente = parseFloat(valorExcedente);

            /*
            if (codigoKit == 7103) {
            vqtd_parc_auto = 3;
            }
            
            else if(codigoKit == 7102) {
            vqtd_parc_auto = 2;
            }
            
            else if(codigoKit == 7101) {
            vqtd_parc_auto = 1;
            }
            */



            // Inclusão Regra Parcelamento Novos Kits - 13/04/2015 - Ewerton
            if (codigoKit == 7250 || codigoKit == 7251 || codigoKit == 7252 || codigoKit == 7253 || upgrade == 1 || upgrade == 2) {


                /*
                * Pedidos de Upgrade tipo 2 - do kit 7250 para 7251,7252,7253 já caem na regra de Kit Normal,
                * pois os kits constam no pedido e são gravados na variável codigoKit, validados na regra abaixo.
                *
                * Pedidos de Upgrade tipo 1 precisam da segunda validação abaixo, pois o Kit 7253 não consta no pedido,
                * porque não possui valor, e o parcelamento deve ser validado de acordo com o Kit Antigo (7101 ou 7102). - Ewerton
                */

                // Pedidos de Kit Normal - 13/04/2015 - Ewerton
                if (codigoKit == 7252) {
                    vqtd_parc_auto = 2;
                }

                else if (codigoKit == 7253) {
                    vqtd_parc_auto = 3;
                }

                else {
                    vqtd_parc_auto = 1;
                }

                // Pedidos de Upgrade tipo 1 (Kits Antigos) - 22/04/2015 - Ewerton
                if (upgrade == 1) {

                    if (upgrade_produto_anterior == 7101) {
                        vqtd_parc_auto = 2;
                    }

                    else if (upgrade_produto_anterior == 7102) {
                        vqtd_parc_auto = 3;
                    }

                } // if(upgrade)


            } // if(codigoKit)



            //alert("id_forma_pag:"+vid_forma_pag+",vl_ped:"+vvl_total_pedido+",qtd_parc_auto:"+vqtd_parc_auto);

            if (vid_forma_pag > 0 && vvl_total_pedido > 0) {

                //XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                $.ajax({
                    url: 'ajax/vo3_ajax_consultor_gera_pedido.asp',
                    type: 'POST',
                    data: { acao: 'obter_parcelas', id_forma_pag: vid_forma_pag, vl_ped: vvl_total_pedido, qtd_parc_auto: vqtd_parc_auto },
                    dataType: "json",

                    beforeSend: function () {
                        $('#carregando').show();
                    },

                    complete: function () {
                        $('#carregando').hide();
                    },

                    success: function (retorno) {

                        if (retorno.length > 0) {

                            var vIdFormaPagamento = '';
                            var vPagamento = '';
                            var vValor_Minimo = '';
                            var vparcela = '';
                            var vjuros = '';
                            var vjuros_mes_parc = '';
                            var vfator = '';
                            var vMontaCombo = '';
                            var meusItens = '';
                            //var vcount = 0;

                            var vValorparcela = 0.00;

                            var valorTotalPedido = vvl_total_pedido;

                            $.each(retorno, function (i, item) {

                                if (parseInt(item.parcela, 10) < 4) {

                                    vIdFormaPagamento = item.IdFormaPagamento;
                                    vPagamento = item.Pagamento;
                                    vValor_Minimo = item.Valor_Minimo;
                                    vparcela = item.parcela;
                                    vjuros = item.juros;

                                    vValorparcela = eval(valorTotalPedido / vparcela);

                                    vvl_total_pedido = valorTotalPedido;


                                    //Validar parcelamento + Juros a partir do código do Kit
                                    intCodigoKit = parseInt(codigoKit, 10)

                                    //Validar se é pedido de Upgrade tipo 1
                                    if ((parseInt(upgrade_produto_anterior, 10) == 7101 || parseInt(upgrade_produto_anterior, 10) == 7102) && upgrade == 1) {
                                        intCodigoKit = parseInt(upgrade_produto_anterior, 10)
                                    }

                                    switch (intCodigoKit) {
                                        /*
                                        case 7011:
                                        if(parseInt(vjuros, 10) != 0) {
                                        vjuros_mes_parc = vjuros / 100;
                                        var i;
							
                                        vvl_total_pedido -= parseFloat(valorExcedente);
	
                                        var valor = parseFloat(valorExcedente);
										
                                        valor = valor * (Math.pow((1+(vjuros/100)),vparcela)*(vjuros/100))/(Math.pow((1+(vjuros/100)),vparcela)-1);
                                        vValorparcela = vparcela * valor;
										
                                        vValorparcela = (vvl_total_pedido + vValorparcela) / vparcela;
										
                                        meusItens += '<option value="'+vparcela+'|'+vjuros+'|'+vValorparcela+'">'+vparcela+' x '+number_format(vValorparcela,2,',','.')+'</div>';
                                        }

                                        else {
                                        vvl_total_pedido =  parseFloat(vvl_total_pedido);
                                        meusItens += '<option value="'+vparcela+'|'+vjuros+'|'+vValorparcela+'">'+vparcela+' x '+number_format(vvl_total_pedido,2,',','.')+'</div>';	
                                        }
									
                                        break;
								
                                        case 7002:
									

                                        if(parseInt(vjuros, 10) != 0) {
                                        vjuros_mes_parc = vjuros / 100;
										
                                        var i;
                                        var valor = parseFloat(vvl_total_pedido);
                                        valor = valor * (Math.pow((1+(vjuros/100)),vparcela)*(vjuros/100))/(Math.pow((1+(vjuros/100)),vparcela)-1);
	
                                        vValorparcela = vparcela * valor;
                                        vValorparcela = vValorparcela / vparcela;
										
                                        meusItens += '<option value="'+vparcela+'|'+vjuros+'|'+vValorparcela+'">'+vparcela+' x '+number_format(vValorparcela,2,',','.')+'</div>';
                                        }

                                        else {
                                        meusItens += '<option value="'+vparcela+'|'+vjuros+'|'+vValorparcela+'">'+vparcela+' x '+number_format(vvl_total_pedido,2,',','.')+'</div>';
                                        }


                                        break;
								
                                        case 7012:
                                        case 7013:								
                                
                                        case 7101:
                                        if(parseInt(vparcela) == 1) {
                                        meusItens += '<option value="'+vparcela+'|0">'+vparcela+'x'+number_format((vValorparcela),2,',','.')+'</div>';
                                        }
										
                                        break;
									
                                        case 7102:
                                        if(parseInt(vparcela) <= 2) {
                                        meusItens += '<option value="'+vparcela+'|0">'+vparcela+'x'+number_format((vValorparcela),2,',','.')+'</div>';
                                        }
										
                                        break;
								
                                        case 7103:
                                        if(parseInt(vparcela) <= 3) {
                                        meusItens += '<option value="'+vparcela+'|0">'+vparcela+'x'+number_format((vValorparcela),2,',','.')+'</div>';
                                        }
										
                                        break; 
								
                                        case 7014:
									
                                        //vValorparcela = vValorparcela / vparcela;
                                        meusItens += '<option value="'+vparcela+'|0">'+vparcela+'x'+number_format((vValorparcela),2,',','.')+'</div>';

                                        break;
                                        */



                                        //Inclusão novos Kits 13/04/2015 - Ewerton      

                                        //KIT Revendedor    
                                        case 7250:

                                            if (parseInt(vparcela) <= 1) {
                                                meusItens += '<option class="7250" value="' + vparcela + '|0">' + vparcela + ' x ' + number_format((vValorparcela), 2, ',', '.') + '</div>';
                                            }

                                            break;

                                        //KIT Executivo Básico    
                                        case 7251:

                                            if (parseInt(vparcela) <= 1) {
                                                meusItens += '<option class="7251" value="' + vparcela + '|0">' + vparcela + ' x ' + number_format((vValorparcela), 2, ',', '.') + '</div>';
                                            }

                                            break;

                                        //KIT Executivo Plus      
                                        case 7252:

                                            if (parseInt(vparcela) <= 2) {
                                                meusItens += '<option class="7252" value="' + vparcela + '|0">' + vparcela + ' x ' + number_format((vValorparcela), 2, ',', '.') + '</div>';
                                            }

                                            break;

                                        //KIT Executivo TOP      
                                        case 7253:

                                            if (parseInt(vparcela) <= 3) {
                                                meusItens += '<option class="7253" value="' + vparcela + '|0">' + vparcela + ' x ' + number_format((vValorparcela), 2, ',', '.') + '</div>';
                                            }

                                            break;



                                        //Pedidos de Upgrade tipo 1 - ver comentário no começo - 22/04/2015 - Ewerton      

                                        //Upgrade KIT Básico
                                        case 7101:

                                            if (parseInt(vparcela) <= 3) {
                                                meusItens += '<option class="7101" value="' + vparcela + '|0">' + vparcela + ' x ' + number_format((vValorparcela), 2, ',', '.') + '</div>';
                                            }

                                            break;

                                        //Upgrade KIT Plus
                                        case 7102:

                                            if (parseInt(vparcela) <= 2) {
                                                meusItens += '<option class="7102" value="' + vparcela + '|0">' + vparcela + ' x ' + number_format((vValorparcela), 2, ',', '.') + '</div>';
                                            }

                                            break;


                                        //Se não for nenhuma das condições acima
                                        default:

                                            if (parseInt(vjuros, 10) != 0) {
                                                var i;
                                                var valor = parseFloat(vvl_total_pedido);
                                                valor = valor * (Math.pow((1 + (vjuros / 100)), vparcela) * (vjuros / 100)) / (Math.pow((1 + (vjuros / 100)), vparcela) - 1);


                                                vValorparcela = vparcela * valor;
                                                vValorparcela = vValorparcela / vparcela;

                                                meusItens += '<option class="default" value="' + vparcela + '|' + vjuros + '|' + vValorparcela + '">' + vparcela + ' x ' + number_format(vValorparcela, 2, ',', '.') + '</div>';
                                            }

                                            else {
                                                meusItens += '<option class="default" value="' + vparcela + '|' + vjuros + '|' + vValorparcela + '">' + vparcela + ' x ' + number_format(vvl_total_pedido, 2, ',', '.') + '</div>';
                                            }

                                            break;

                                    } //switch
                                    //class nas options serve apenas para teste - identificar qual a condição


                                } //if (parseInt(item.parcela, 10) < 4)

                            }) //$.each

                        } //if (retorno.length > 0)

                        vMontaCombo += '<select size="1" name="qtd_parc_sel" id="qtd_parc_sel" class="imput-formata" style=" width: 105px;">';
                        vMontaCombo += '<option selected="selected" value="">Selecione</option>';
                        vMontaCombo += meusItens + '</select>';

                        $("#div_parcelas").html(vMontaCombo);


                        $("#qtd_parc_sel").on('change', function (event) {

                            var rs = $("#qtd_parc_sel").val().split('|');
                            $("#qtd_parc").val(rs[0]);
                            //$("#ped_juros").val(rs[1]);
                            var vqtd_parc = rs[0];

                            $("#vl_total_pedido_juros").val(0);

                            if (rs[1] > 0) {
                                var vjuros_mes = rs[1] / 100;
                                var vfator_finan = (vjuros_mes / (1 - (1 / (Math.pow(1 + vjuros_mes, vqtd_parc))))).toFixed(5);

                                //ref http://www.matematicadidatica.com.br/CalculoPrestacao.aspx
                                //$("#fator_parcela").val(vfator_finan);
                                $("#vl_total_pedido_juros").val(number_format((parseFloat(rs[0]) * parseFloat(rs[2])), 2, '.', ''));
                                $("#txt_vl_parcela").html("R$ " + number_format(rs[2], 2, ',', '.'));
                            }

                            else {
                                $("#txt_vl_parcela").html("R$ " + number_format(vvl_total_pedido / vqtd_parc, 2, ',', '.'));
                            }

                        }); //$("#qtd_parc_sel").on('change', function(event))				


                    }, //success


                    error: function (retorno) {

                        if (retorno.status == 404) {
                            $('#retorno').html('Houve > um erro: ' + retorno.status + ' ' + retorno.statusText);
                        }

                        else {
                            $('#retorno').html('Houve < um erro desconhecido entre em contato com o administrador...');
                        }

                    } //error

                })//ajax

            } //if (vid_forma_pag > 0 && vvl_total_pedido > 0)

        } // Fim da function Fc_obter_parcelas()


        function validateNumber(event) {
            var key = window.event ? event.keyCode : event.which;

            if (event.keyCode == 8 || event.keyCode == 46
             || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }

            else if (key < 48 || key > 57) {
                return false;
            }

            else return true;
        }; //Fim da function validateNumber(event)

        function getNum(val) {

            if (isNaN(val))
                return 0;

            else
                return val;
        } //function getNum(val)
	
    </script>


    <div class="main-container-inner">
        

    <div class="page-content">
    
        <div class="page-header">
            <h1>
               Pagamento
            </h1>
            <br>
            <a  type="button" class="btn btn-lg " href="fat_GestaoFaturamento1.asp">
                Voltar <i class="icon-arrow-left align-top bigger-125"></i>
            </a>
        </div><!-- /.page-header -->
    
    
    
        <div class="row">
            <div class="col-xs-12"> 
                       
                <div class="row">
                    <div class="col-sm-3">
                    
                    	<!--####### USO ###########--->
                        <div class="widget-box">
                        
                        	<div class="widget-header widget-header-flat widget-header-small">
                                <h5>Detalhes do Pedido</h5>
                            </div>
                        
                            <div class="widget-body">
                                <div class="widget-main barraDeRolagemUso" style="min-height:300px;">
                                    
                                    <table class="table table-bordered table-striped">    
                                        <tbody>
                                              <tr>
                                              	<td><h4 style="margin:0;"><b>Valor do Pedido:</b></h4></td>
                                              </tr>
                                              <tr>
                                              	<td><h2 style="margin:0;" id="txt_vl_total_pedido">R$ 0,00</h2></td>
                                              </tr>                                          
                                        </tbody>
                                    </table>
                                    
                                    <hr/>
                                    
                                    <table class="table table-bordered table-striped">    
                                        <tbody>
                                              <tr>
                                              	<td><h4 style="margin:0;"><b>Valor da Parcela:</b></h4></td>
                                              </tr>
                                              <tr>
                                              	<td><h2 style="margin:0;" id="txt_vl_parcela">-</h2></td>
                                              </tr>                                          
                                        </tbody>
                                    </table>
                                    
                                    <hr/>
                                    
                                    <table class="table table-bordered table-striped">    
                                        <tbody>
                                              <tr>
                                              	<td><h4 style="margin:0;"><b>Total em Pontos:</b></h4></td>
                                              </tr> 
                                              <tr>
                                              	<td><h2 style="margin:0;" id="txt_pontos_total_pedido">0,00</h2></td>
                                              </tr>                                          
                                        </tbody>
                                    </table>
                                    
                                    <hr/>
                                    
                                    <table class="table table-bordered table-striped">    
                                        <tbody>
                                              <tr>
                                              	<td><h4 style="margin:0;"><b>Pagamento Selecionado:</b></h4></td>
                                              </tr> 
                                              <tr>
                                              	<td>
                                                
                        <h4 style="margin:0;" id="cartao_vazio">-</h4>
                        <h4 style="margin:0; display:none;" id="cartao-visa" class="reset">Cartão de Crédito Visa</h4>
                        <h4 style="margin:0; display:none;" id="cartao-master" class="reset">Cartão de Crédito Master Card</h4>
                        <h4 style="margin:0; display:none;" id="cartao-aura" class="reset">Cartão de Crédito Aura</h4>
                        <h4 style="margin:0; display:none;" id="cartao-american" class="reset">Cartão de Crédito American Express</h4>
                        <h4 style="margin:0; display:none;" id="cartao-diners" class="reset">Cartão de Crédito Diners</h4>
                        <h4 style="margin:0; display:none;" id="cartao-hipercard" class="reset">Cartão de Crédito Hipercard</h4>
                        <h4 style="margin:0; display:none;" id="boleto-bancario" class="reset">Boleto Bancário</h4>
                        <h4 style="margin:0; display:none;" id="dep-identificado" class="reset">Depósito Identificado</h4>
                        <h4 style="margin:0; display:none;" id="pag-online-brad" class="reset">Pagamento Fácil Bradesco</h4>
                        <h4 style="margin:0; display:none;" id="pag-online-itau" class="reset">Itaú Shopline</h4>
                        <h4 style="margin:0; display:none;" id="pag-cd-cdh-div" class="reset">Pagamento no CD-CDH</h4>
                                                </td>
                                              </tr>                                          
                                        </tbody>
                                    </table>                                                          
                                    
                                    
                                </div><!-- /widget-main -->
                            </div><!-- /widget-body -->
                        </div>
                          
                    </div>
                    <div class="col-sm-9">
                        <!--####### PRODUTOS ###########--->
                        <div class="widget-box">
                            <div class="widget-header widget-header-flat widget-header-small">
                                <h5> Formas de Pagamento </h5>
                            </div>

                            <div class="widget-body">
                                
                                <div class="widget-main" style="min-height:300px;">
                                
                                	<div class="widget-box transparent" id="forma_pag_cartao">
                                        <div class="widget-header">
                                            <h4 class="lighter">Cartão de Crédito</h4>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main padding-6 no-padding-left no-padding-right">
                                                <div class="row">
                                                	<div class="col-sm-6">
                                                    	<div class="radio">
                                                            <label>
                                                                <input type="radio" class="ace" id="visa" value="2" name="pagamento">
                                                                <span class="lbl">
                                                                	<img width="56" height="36" src="assets/imagens/iico_visa.gif"> Visa
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" class="ace" id="mastercard" value="1" name="pagamento">
                                                                <span class="lbl">
                                                                    <img width="57" height="36" src="assets/imagens/iico_mastercard.gif"> Mastercard
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                	<div class="col-sm-6">
                                                    	<div class="radio">
                                                            <label>
                                                                <input type="radio" class="ace" id="aura" value="10" name="pagamento">
                                                                <span class="lbl">
                                                                    <img width="43" height="30" src="assets/imagens/iico_aura.gif"> Aura
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                    	<div class="radio">
                                                            <label>
                                                                <input type="radio" class="ace" id="american" value="4" name="pagamento">
                                                                <span class="lbl">
                                                                    <img width="55" height="36" src="assets/imagens/iico_american_express.gif"> American Express
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                	<div class="col-sm-6">
                                                    	<div class="radio">
                                                            <label>
                                                                <input type="radio" class="ace" id="diners" value="3" name="pagamento">
                                                                <span class="lbl">
                                                                    <img width="57" height="36" src="assets/imagens/iico_diners.gif"> Diners
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <!--<div class="col-sm-6">
                                                    	<div class="radio">
                                                            <label>
                                                                <input type="radio" class="ace" id="hipercard" value="7" name="pagamento">
                                                                <span class="lbl">
                                                                    <img width="55" height="36" src="assets/imagens/icon_hipercard.jpg"> Hipercard
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>-->
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="widget-box transparent" id="cartao_dados" style="display:none;">
                                        <div class="widget-header">
                                            <h4 class="lighter">Informe os dados do cartão de crédito</h4>
                                        </div>
                                        
                                        <div class="widget-body">
                                            <div class="widget-main padding-6 no-padding-left no-padding-right">
                                                <div class="row">
                                                	<div class="col-sm-12">
                                                        <form class="form-horizontal" role="form">
                                                        <input type="hidden" id="codigokit" value="" name="codigokit">
                                                        <input type="hidden" id="upgrade" value="" name="upgrade">
                                                        <input type="hidden" id="upgrade_produto_anterior" value="" name="upgrade_produto_anterior">
                                                        
                                                            <div class="form-group">
                                             					<label class="col-sm-3 control-label no-padding-right"> * Qtd de Parcela(s) : </label>
                        
                                                                <div class="col-sm-9">
                                                                    <div id="div_parcelas" style="display:inline">                                                                  
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                             					<label class="col-sm-3 control-label no-padding-right"> * Número do cartão: </label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" id="vbin6" name="vbin6" class="col-xs-10 col-sm-5 limited" placeholder="Número do cartão" maxlength="16">
                                                                    <span class="help-inline col-xs-12 col-sm-7">
                                                                        <span class="middle">Digite os números do cartão</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                             					<label class="col-sm-3 control-label no-padding-right"> * Nome do titular:  </label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" id="cnome" name="cnome" class="col-xs-10 col-sm-5 limited" placeholder="Nome do titular" maxlength="50">
                                                                    <span class="help-inline col-xs-12 col-sm-7">
                                                                        <span class="middle">(Como está impresso no cartão)</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                             					<label class="col-sm-3 control-label no-padding-right"> * Código de segurança:   </label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" id="cseg" name="cseg" class="col-xs-10 col-sm-5 limited" placeholder="Código de segurança" maxlength="4">
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                             					<label class="col-sm-3 control-label no-padding-right"> * Validade mês/ano:   </label>
                                                                <div class="col-sm-9">
                                                                
																	<select name="cmes" id="cmes" style="padding-right:4px; width:60px;">
                                                                         <option selected="selected" value=""></option>
                                                                        <option value="01">01</option>
                                                                        <option value="02">02</option>
                                                                        <option value="03">03</option>
                                                                        <option value="04">04</option>
                                                                        <option value="05">05</option>
                                                                        <option value="06">06</option>
                                                                        <option value="07">07</option>
                                                                        <option value="08">08</option>
                                                                        <option value="09">09</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                    /
                                                                    <select name="cano" id="cano" style="padding-right:4px; width:80px;">
                                                                        <option selected="selected" value=""> </option>
                                                                        <option value="2015">2015</option>
                                                                        <option value="2016">2016</option>
                                                                        <option value="2017">2017</option>
                                                                        <option value="2018">2018</option>
                                                                        <option value="2019">2019</option>
                                                                        <option value="2020">2020</option>
                                                                        <option value="2021">2021</option>
                                                                        <option value="2022">2022</option>
                                                                        <option value="2023">2023</option>
                                                                        <option value="2024">2024</option>
                                                                        <option value="2025">2025</option>
                                                                        <option value="2026">2026</option>
                                                                        <option value="2027">2027</option>
                                                                        <option value="2028">2028</option>
                                                                        <option value="2029">2029</option>
                                                                        <option value="2030">2030</option>
                                                                    </select>
                                                                    
                                                                </div>
                                                            </div>
                                                            
                                                        </form>
                                                    </div>
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="widget-box transparent" id="forma_pag_bo_de" style="display:none;">
                                        <div class="widget-header">
                                            <h4 class="lighter">Cartão de Crédito</h4>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main padding-6 no-padding-left no-padding-right">
                                                <div class="row">
                                                	<div class="col-sm-6">
                                                    	<div class="radio">
                                                            <label>
                                                                <input type="radio" class="ace" id="boleto-banc" value="5" name="pagamento">
                                                                <span class="lbl">
                                                                	<img width="57" height="34" src="imagens/iico_boleto.gif">
                                                                    <span style="color:#F00;float: right;width: 200px;">
                                                                    	Boleto Bancário
                                                                        <br>
                                                                        com tarifa de R$ 1,50
                                                                    </span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" class="ace" id="dep-id" value="6" name="pagamento">
                                                                <span class="lbl">
                                                                    <img width="55" height="34" src="imagens/iico_identificado.gif">
                                                                    Depósito Identificado
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div> <!-- row -->                                               
                                                                                              
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="widget-box transparent" id="forma_pag_cd_cdh" style="display:none;">
                                        <div class="widget-header">
                                            <h4 class="lighter">Pagamento no CD - CDH </h4>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main padding-6 no-padding-left no-padding-right">
                                                <div class="row">
                                                	<div class="col-sm-6">
                                                    	<div class="radio">
                                                            <label>
                                                                <input type="radio" class="ace" name="pagamento" id="pag-cd-cdh" value="17">
                                                                <span class="lbl">
                                                                	<img src="imagens/icon_cd_cdh.jpg"/>
                                                                    <span style="color:#F00;float: right;width: 200px;">
                                                                    	Pagamento No <%= vid_cdh_retira_desc %>
                                                                    </span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>                                                    
                                                </div> <!-- row -->                                               
                                                                                              
                                            </div>
                                        </div>
                                    </div>
                                    

                                </div><!-- /widget-main -->
                            </div><!-- /widget-body -->
                            
                            
                        </div>
                        
                        
                        <hr/>
                        
                        <div class="row">
                             <div class="col-sm-12" style="text-align:right;">
                                <button class="btn btn-lg btn-success" type="button" id="bt_continuar" name="bt_continuar">
                                	Efetuar Pagamento <i class="icon-shopping-cart align-top bigger-125"></i>
                                </button>
                                
                                <form name="form-concluir-compra" id="form-concluir-compra" method="post" action="vo3-gera-pedido-forma-pagamento.asp">
                                    <input type="hidden" name="ss_pg" id="ss_pg" value="<%=vss_pg%>"/>
                                    
                                    <input type="hidden" name="id_cons" id="id_cons" value="<%=vid_cons%>"/>
                                    <input type="hidden" name="modo_entrega" id="modo_entrega" value="<%=vmodo_entrega%>"/>
                                    <input type="hidden" name="nome_cons" id="nome_cons" value="<%=vnome_cons%>"/>
                                    <input type="hidden" name="id_cdh_retira" id="id_cdh_retira" value="<%=vid_cdh_retira%>"/>
                                    <input type="hidden" name="id_cdh_retira_desc" id="id_cdh_retira_desc" value="<%=vid_cdh_retira_desc%>"/>
                                    <input type="hidden" name="email_ped" id="email_ped" value="<%=vemail_ped%>" />
                                    <input type="hidden" name="cdh_retira_email" id="cdh_retira_email" value="<%=vcdh_retira_email%>" />
                            
                                    <input type="hidden" name="cep" id="cep" value="<%=vcep%>"  />
                                    <input type="hidden" name="endereco" id="endereco" value="<%=vendereco%>" />
                                    <input type="hidden" name="numero" id="numero" value="<%=vnumero%>" />
                                    <input type="hidden" name="complemento" id="complemento" value="<%=vcomplemento%>" />
                                    <input type="hidden" name="bairro" id="bairro" value="<%=vbairro%>" />
                                    <input type="hidden" name="cidade" id="cidade" value="<%=vcidade%>" />
                                    <input type="hidden" name="estado" id="estado" value="<%=vestado%>" />
                                    
                                    <input type="hidden" name="id_forma_pag" id="id_forma_pag" value="<%=vid_forma_pag%>" />
                                    <input type="hidden" name="desc_forma_pag" id="desc_forma_pag" value="<%=vdesc_forma_pag%>" />
                                    <input type="hidden" name="desc_forma_pag_verf" id="desc_forma_pag_verf" value="" />
                                    
                                    <input type="hidden" name="vl_total_pedido_frete" id="vl_total_pedido_frete" value="<%=vvl_total_pedido_frete%>" />
                                    <input type="hidden" name="vl_total_pedido" id="vl_total_pedido" value="<%=vvl_total_pedido%>" />
                                    <input type="hidden" name="vl_total_pedido_juros" id="vl_total_pedido_juros" value="0" />
                                    <input type="hidden" name="vl_total_pedido_bkp" id="vl_total_pedido_bkp" value="<%=vvl_total_pedido%>" />
                                    <input type="hidden" name="vl_sub_total_pedido" id="vl_sub_total_pedido" value="<%=vvl_sub_total_pedido%>" />
                                    <input type="hidden" name="pontos_total_pedido" id="pontos_total_pedido" value="<%=vpontos_total_pedido%>" />
                                    <input type="hidden" name="peso_total_pedido" id="peso_total_pedido" value="<%=vpeso_total_pedido%>" />
                                    <input type="hidden" name="forma_envio_transp_ped" id="forma_envio_transp_ped" value="<%=vforma_envio_transp_ped%>" />
                                    <input type="hidden" name="forma_envio_transp_desc_ped" id="forma_envio_transp_desc_ped" value="<%=vforma_envio_transp_desc_ped%>" />
                                    <input type="hidden" name="prazo_transp_ped" id="prazo_transp_ped" value="<%=vprazo_transp_ped%>" />
                                    <input type="hidden" name="vl_credito_usado" id="vl_credito_usado" value="<%=vvl_credito_usado%>" />
                                    <input type="hidden" name="qtd_parc_auto" id="qtd_parc_auto" value="<%=vqtd_parc_auto%>" />
                                    
                                    <input type="hidden" name="qtd_parc" id="qtd_parc" value="" />
                                    <input type="hidden" name="ped_juros" id="ped_juros" value="" />
                                    <input type="hidden" name="atv_cons" id="atv_cons" value="<%=vatv_cons%>" />
                                    <input type="hidden" name="atv_cad_cons_bkp" id="atv_cad_cons_bkp" value="<%=vatv_cad_cons_bkp%>" />
                                    <input type="hidden" name="fator_parcela" id="fator_parcela" value="" />
                                    <input type="hidden" name="id_pedido_oficial" id="id_pedido_oficial" value="<%=vid_pedido_oficial%>" />
                                    <input type="hidden" name="valorDesconto" id="valorDesconto" value="<%=VvalorDesconto%>" />
                                    <input type="hidden" name="tel" id="tel" value="<%=vtel%>" />   
                                    
                                    <input type="hidden" name="pontuacao_minima" id="pontuacao_minima" value="<%=vpontuacao_minima%>" />
                                 </form>
                                
                            </div>
                        </div>
                        
                          
                    </div>
                </div>  <!-- Row -->
                
                <hr/>
                
                <div class="row">
                    <div class="col-sm-2">                   	
                                   
                    </div>
                    <div class="col-sm-2">    
                    	
                    </div>
                    <div class="col-sm-2">   
                    	
                    </div>
                    <div class="col-sm-6" style="text-align:right; padding-bottom:50px;"> 
                    	<!--<div>
                    	<a class="btn btn-lg" href="vo3-gera-pedido.asp">
                            <i class="icon-undo align-top bigger-125"></i>
                            Recomeçar
                        </a>
                      
                    	<button class="btn btn-lg btn-success" type="button">
                        	Continuar <i class="icon-arrow-right align-top bigger-125"></i>
                        </button>
                        </div>-->
                    </div>
                </div> <!-- Row -->
                
                              
            </div><!-- /.col -->
        </div><!-- /.row -->
        
        
    </div><!-- /.page-content -->
</div><!-- /.main-content -->
<!--- FIM CONTEUDO ///////////////////////////////////////////////////////-----------> 




<script language="javascript" type="text/javascript" src="assets/js/number_format.js"></script>

<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/typeahead-bs2.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
  <script src="assets/js/excanvas.min.js"></script>
<![endif]-->

<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="assets/js/chosen.jquery.min.js"></script>
<script src="assets/js/fuelux/fuelux.spinner.min.js"></script>
<script src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="assets/js/date-time/bootstrap-timepicker.min.js"></script>
<script src="assets/js/date-time/moment.min.js"></script>
<script src="assets/js/date-time/daterangepicker.min.js"></script>
<script src="assets/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/js/jquery.knob.min.js"></script>
<script src="assets/js/jquery.autosize.min.js"></script>
<script src="assets/js/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="assets/js/jquery.maskedinput.min.js"></script>
<script src="assets/js/bootstrap-tag.min.js"></script>
<script src="assets/js/bootbox.min.js"></script>

<!-- ace scripts -->

<script src="assets/js/ace-elements.min.js"></script>


<!-- inline scripts related to this page -->

<script type="text/javascript">

    jQuery(function ($) {

        $('.limited').inputlimiter({
            remText: '%n Caracteres restantes...',
            limitText: 'Max permitido : %n.'
        });



    });
	
</script>


<div id="carregando" style="display:none; position:fixed; top:0; right:0; bottom:0; left:0; background:rgba(100,100,100,0.7); z-index:9999;">

	<span style="color: #FFFFFF;
    display: block;
    font-size: 16px;
    margin: 10% auto;
    text-align: center;
    width: 200px;" id="carregando">
    
        <img  src="https://vo.hinode.com.br/vo-2/assets/images/ajax-loader.gif" style="margin-right:10px;">Carregando...
        
    </span>

</div>


</body>
</html>
