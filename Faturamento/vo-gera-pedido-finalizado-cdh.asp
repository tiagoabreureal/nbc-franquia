<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Hinode - VO</title>

        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- basic styles -->

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/css/font-awesome.min.css" />

        <!--[if IE 7]>
          <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
        <![endif]-->

        <!-- page specific plugin styles -->

        <!-- fonts -->

        <link rel="stylesheet" href="assets/css/ace-fonts.css" />

        <!-- ace styles -->

        <link rel="stylesheet" href="assets/css/ace.min.css" />
        <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />

        <!--[if lte IE 8]>
          <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->

        <script src="assets/js/ace-extra.min.js"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->


        <!-- Hinode -->
        <link rel="stylesheet" href="assets/css/hinode.css" />
        
<!-- basic scripts -->

<!--[if !IE]> -->

<script type="text/javascript">
    window.jQuery || document.write("<scr"+"ipt src='assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
window.jQuery || document.write("<scr"+"ipt src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

<script type="text/javascript">
    if ("ontouchend" in document)
        document.write("<scr"+"ipt src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/typeahead-bs2.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="assets/js/excanvas.min.js"></script>
<![endif]-->

<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<script src="assets/js/jquery.easy-pie-chart.min.js"></script>
<script src="assets/js/jquery.sparkline.min.js"></script>
<script src="assets/js/flot/jquery.flot.min.js"></script>
<script src="assets/js/flot/jquery.flot.pie.min.js"></script>
<script src="assets/js/flot/jquery.flot.resize.min.js"></script>

<!-- ace scripts -->

<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>

<!-- inline scripts related to this page -->

<!-- Cycle -->
<!--<script src="hinode/js/jquery.cycle.all.js" ></script>
<script type="text/javascript">
$('#slide').cycle({ 
    fx:     'scrollHorz', 
    speed:  1000, 
    timeout: 200, 
    next:   '#next_slide', 
    prev:   '#prev_slide' 
});

</script>-->       
       
        
 
 <style>
.navbar {
background: #ffffff; /* Old browsers */
background: -moz-linear-gradient(left,  #ffffff 0%, #fff8e2 51%, #ffcb07 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, right top, color-stop(0%,#ffffff), color-stop(51%,#fff8e2), color-stop(100%,#ffcb07)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(left,  #ffffff 0%,#fff8e2 51%,#ffcb07 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(left,  #ffffff 0%,#fff8e2 51%,#ffcb07 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(left,  #ffffff 0%,#fff8e2 51%,#ffcb07 100%); /* IE10+ */
background: linear-gradient(to right,  #ffffff 0%,#fff8e2 51%,#ffcb07 100%); /* W3C */

filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffcb07',GradientType=1 ); /* IE6-9 */

}
 
 
.ace-nav > li.light-blue > a {
    background-color: #ffcb07;
		background:none;
}

.ace-nav > li.open.light-blue > a {
    background-color: #ffcb07 !important;
	background:none;

}

.ace-nav > li.light-blue > a:hover{
	background:none;
	color: #d15b47;
}
.ace-nav > li.light-blue > a:focus {
    background-color: #ffcb07;
	background:none;
	color: #d15b47;
}

.ace-nav > li > a {
    color: #d15b47;
}



.ace-nav > li.amarelo > a {
    /*background-color: #ffcb07;*/
	background:none;
}

.ace-nav > li.open > a {
    background-color: #ffcb07 !important;
}

.ace-nav > li > a > [class*="icon-"] {
    color: #d15b47;
}

.ace-nav > li.open > a > [class*="icon-"] {
    color: #fff;
}


.ace-nav > li {
    border-left: 1px solid #ffcb07;
    float: left !important;
    height: 45px;
    line-height: 45px;
    margin-top: 24px;
    position: relative;
}
 
 
 .navbar .navbar-header {
    float: right;
    margin: 0 !important;
}
 
 </style>

 
 
 
 
 
</head>

<%

session("ss_pg_session") = ""
vIdPedido_oficial = REQUEST("id_pedido_oficial")
vbin6 = REQUEST("vbin6")
vcnome = REQUEST("cnome")
vcseg = REQUEST("cseg")
vcmes = REQUEST("cmes")
vcano = REQUEST("cano")
valorTotalPedidoJuros = REQUEST("valorTotalPedidoJuros")

'session("ss_pg_session") = ""
'vIdPedido_oficial = "860212"
'vbin6 = ""
'vcnome = ""
'vcseg = ""
'vcmes = ""
'vcano = ""
'valorTotalPedidoJuros = ""
	
%>

<script language="javascript" type="text/javascript" src="../js/jquery-1.8.3.min.js"></script>

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="../fancyBox/source/jquery.fancybox.js?v=2.1.0"></script>
<link rel="stylesheet" type="text/css" href="../fancyBox/source/jquery.fancybox.css?v=2.1.0" media="screen" />

<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="../fancyBox/source/helpers/jquery.fancybox-buttons.css?v=1.0.3" />
<script type="text/javascript" src="../fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.3"></script>

<script language="javascript" type="text/javascript" src="gera_pedido/js/jquery.printElement.js"></script>

<div class="main-container" id="main-container">
<script type="text/javascript">
	try {
		ace.settings.check('main-container', 'fixed');
	} catch (e) {
	}
</script>
	
	
	
<script>

	 $(document).ready(function(){
			
		$("#bt_print").click(function ( event ) {	
		
			$("#bt_print").hide();
		
			event.preventDefault();
			$('#tela').printElement({});
			
			$("#bt_print").show();	
				
		})
		
		
			
		/* LISTA CARRINHO */
		var vid_pedido_oficial = $("#IdPedido_oficial").val();
		if(vid_pedido_oficial!='' && parseInt(vid_pedido_oficial,10) > 0){
			
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			$.ajax({
        		url : 'ajax/vo2_ajax_consultor_gera_pedido.asp',
        		type :'POST',
				data : {acao:'lista_pedido',id_pedido_oficial:vid_pedido_oficial}, 
				dataType:"json",
				beforeSend : function(){
            		$('#carregando').show(100);
        		},
        		complete : function(){
            		$('#carregando').hide(100);
        		},
        		success : function(retorno){
					
					//console.log(retorno);
					
					if(retorno.length > 0 ) {
						
						var vStatus_ped = "";
						var vqtd_parc = "";
						
						$.each(retorno, function (i, item) {
					    
						if(item.Ped_idStatus=='1'){
						vStatus_ped = "Aguardando Compensação";
						}else if(item.Ped_idStatus=='2'){
						vStatus_ped = "Pagamento Realizado";
						}else{
						vStatus_ped = "Cancelado";
						}
						
						vqtd_parc = " - "+item.Ped_QtdaParcela+"x";
						
						if(parseInt(item.Ped_idFormaPagamento,10)==5 && parseFloat(item.Ped_vl_total)>0 ){
							$("#imprimir_boleto").show("slow");
							vqtd_parc = " ";
						}
						
		$("#vl_CdPedido").html("NÚMERO DO PEDIDO: "+item.IdPedido_oficial);
		$("#vl_Ped_idStatus").html(vStatus_ped);
		$("#vl_Ped_nome_consultor").html(item.Ped_nome_consultor);
						
		//item.Ped_email
		
		$("#vl_Ped_pagamento").html(item.Ped_pagamento+''+vqtd_parc);
		//item.Ped_CodigoAprovacao
		//$("#vl_Ped_QtdaParcela").html(item.Ped_QtdaParcela);
		$("#vl_Ped_vl_sub_total").html("R$ "+number_format(item.Ped_vl_sub_total,2,',','.'));
		
		$("#vl_Ped_vl_frete").html("R$ "+number_format(item.Ped_vl_frete,2,',','.'));
		
		var valorTotalPedidoJuros = "<% response.write(valorTotalPedidoJuros) %>";
		if(valorTotalPedidoJuros != 0)
		{
			$("#vl_Ped_vl_total").html("R$ "+number_format(valorTotalPedidoJuros,2,',','.'));
		}
		else
		{
			$("#vl_Ped_vl_total").html("R$ "+number_format(item.Ped_vl_total,2,',','.'));
		}
		$("#vl_credito_user").html("R$ "+number_format(item.Ped_Credito_usado,2,',','.'));
						
		$("#vl_Ped_pontos").html(number_format(item.Ped_pontos,2,',','.'));
		//item.Ped_peso
		
		//$("#vl_Ped_modo_entr").html(item.Ped_modo_entr);
		$("#vl_Ped_prazo_dias").html(item.Ped_prazo_dias);
		//item.Ped_idDestinatario_local_retira
		
		if(parseInt(item.Ped_modo_entr,10)==1){
			$("#vl_Ped_local_entr").html("");
			$("#vl_Ped_modo_entr").html("");
		}else{
		$("#vl_Ped_local_entr").html(item.Ped_destinatario_local_retira_nome);
		}
		
		$("#vl_Ped_Transporte").html(item.Ped_Transporte);
						
						$("#vl_Ped_cep").html(item.Ped_cep);
						$("#vl_Ped_endereco").html(item.Ped_endereco+' , '+item.Ped_numero);
						//$("#vl_Ped_numero").html(item.Ped_numero);
						$("#vl_Ped_compl").html(item.Ped_compl);
						$("#vl_Ped_bairro").html(item.Ped_bairro);
						$("#vl_Ped_cidade").html(item.Ped_cidade+' / '+ item.Ped_estado);
						//$("#vl_Ped_estado").html(item.Ped_estado);
						
						
						$("#vl_Ped_dt_cadastro").html(item.Ped_dt_cadastro);
						//item.IdPedido_oficial
					   
					    $("#Ped_vl_total").val(number_format(item.Ped_vl_total,2,',','.'));
						$("#Ped_vl_frete").val(number_format(item.Ped_vl_frete,2,',','.'));
						$("#Ped_vl_sub_total").val(number_format(item.Ped_vl_sub_total,2,',','.'));
						$("#Ped_nome_consultor").val(item.Ped_nome_consultor);
						$("#Ped_idConsultor").val(item.Ped_idConsultor);
						
						$("#Ped_estado").val(item.Ped_estado);
						$("#Ped_cidade").val(item.Ped_cidade);
						$("#Ped_bairro").val(item.Ped_bairro);
						$("#Ped_compl").val(item.Ped_compl);
						$("#Ped_numero").val(item.Ped_numero);
						$("#Ped_endereco").val(item.Ped_endereco);
						$("#Ped_cep").val(item.Ped_cep);
						$("#Ped_idFormaPagamento").val(item.Ped_idFormaPagamento);
						$("#Ped_QtdaParcela").val(item.Ped_QtdaParcela);
						

					    })
						
						Fc_pedido_item(vid_pedido_oficial);
				    
					}else{
					 //$("#retorno").html("Pedido Não Encontrado!");
					 bootbox.alert("Pedido Não Encontrado!", function() {});
				    }
					
				},
				error : function(retorno){
					 //console.log(retorno);
					if ( retorno.status == 404 ){
        				//$('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
						bootbox.alert("Houve um erro: " + retorno.status + " " + retorno.statusText, function() {});
    				}else{
      					//$('#retorno').html('Houve um erro desconhecido entre em contato com o administrador...');
						bootbox.alert("Houve um erro desconhecido entre em contato com o administrador...", function() {});
        			}
				}
			})
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		}else{
			//$("#retorno").html("Pedido Não Encontrado!!");
			bootbox.alert("Pedido Não Encontrado!", function() {});
		}

       
})


function Fc_pedido_item(vid_pedido_oficial){
			
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			$.ajax({
        		url : 'ajax/vo2_ajax_consultor_gera_pedido.asp',
        		type :'POST',
				data : {acao:'lista_pedido_item',id_pedido_oficial:vid_pedido_oficial}, 
				dataType:"json",
				beforeSend : function(){
            		$('#carregando').show(100);
        		},
        		complete : function(){
            		$('#carregando').hide(100);
        		},
        		success : function(retorno)
				{
					//console.log(retorno);
					if(retorno.length > 0 ) 
					{
						
						var meusItens = '';
						var valor_total_item = 0;
						var pontos_total_item = 0;
						var encomendar_item = '';
						var Status_item = '';
						var bg_item = '';
						
						var arrayProdutos = [];
						var mostrouItem = [];
						
						$.each(retorno, function (i, item) 
						{
							if(item.PedItem_class == 1)
							{
								if(item.PedItem_prod_qtd_encomendar>0)
								{
									encomendar_item = '</br><span class="texto_vermelho">Item sob encomenda, quantidade encomendar:</span>'+item.PedItem_prod_qtd_encomendar;
								}
								else
								{
									encomendar_item = '';
								}
								
								if(parseInt(item.PedItem_class,10)==1)
								{ 
									bg_item = 'background-color:#fefedd"';
								}
								
								if(parseInt(item.PediItem_prod_atv_consultor,10)==1)
								{ 
									bg_item = 'background-color:#FFC"';
								}
								
								if(parseInt(item.PedItem_prod_status,10)==0)
								{ 
									Status_item = 'Cancelado'; bg_item = 'background-color:#fecdb8"';
								}
								
								valor_total_item = eval((item.PedItem_prod_qtd*item.PedItem_prod_valor_unt)-item.PedItem_prod_valor_desc);	
								pontos_total_item = eval(item.PedItem_prod_qtd*item.PedItem_prod_pontuacao);
								
								vnome_prod = item.PedItem_prod_nome.replace(new RegExp( "\\n", "g" ),"").replace(new RegExp( "\\<br>", "g" )," ")
								
								/*meusItens += '<li style="height:auto; padding-bottom:5px; padding-top:5px; '+bg_item+'">';
								// += '<div class="item">'+ item.PedItem_idProduto+'</div>';
								meusItens += '<div class="codigo">'+ item.PedItem_prod_codigo+'</div>';
								meusItens += '<div class="produto">'+ vnome_prod+''+encomendar_item+'</div>';
								meusItens += '<div class="quant">'+ 1 +'</div>';
								meusItens += '<div class="valor">R$ '+ number_format(item.PedItem_prod_valor_unt,2,',','.')+'</div>';
								meusItens += '<div class="desconto">R$ '+ number_format(item.PedItem_prod_valor_desc,2,',','.')+'</div>';
								meusItens += '<div class="valor-total">R$ '+ number_format(1 * valor_total_item,2,',','.')+'</div>';
								meusItens += '<div class="pontos">'+number_format(1 * item.PedItem_prod_pontuacao,2,',','.')+'</div>'; //number_format(pontos_total_item,2,',','.')
								meusItens += '<div class="status">'+Status_item+'</div>'; //number_format(pontos_total_item,2,',','.')
								meusItens += '</li>';*/
								
								
								meusItens += '<tr>';	
								meusItens += '	<td class="codigo">'+ item.PedItem_prod_codigo+'</td>	';
								meusItens += '	<td class="produto">'+ vnome_prod+''+encomendar_item+'</td>';	
								meusItens += '	<td class="quant">'+ 1 +'</td>	';
								meusItens += '	<td class="valor">R$ '+ number_format(item.PedItem_prod_valor_unt,2,',','.')+'</td>';	
								meusItens += '	<td class="desconto">R$ '+ number_format(item.PedItem_prod_valor_desc,2,',','.')+'</td>';	
								meusItens += '	<td class="valor-total">R$ '+ number_format(1 * valor_total_item,2,',','.')+'</td>';	
								meusItens += '	<td class="pontos">'+number_format(1 * item.PedItem_prod_pontuacao,2,',','.')+'</td>';
								meusItens += '	<td class="status">'+Status_item+'</td>';	
								meusItens += '</tr>';
								
							}
							else
							{
								if(!$.isNumeric(mostrouItem[item.PedItem_prod_codigo]))
								{
									var quantidadeItem = item.PedItem_prod_qtd;
									
									if(item.PedItem_class == 0)
									{									
										var idProduto = item.PedItem_prod_codigo;
										var idPedido = item.CdPedItem;
										
										$.each(retorno, function (i, item)
										{	
											//alert(item.PedItem_prod_codigo + " == " + idProduto);									
											if(item.PedItem_prod_codigo == idProduto && item.CdPedItem != idPedido && item.PedItem_class == 0)
											{
												quantidadeItem += 1;
											}
										});
									}
		
									if(item.PedItem_prod_qtd_encomendar>0)
									{
										encomendar_item = '</br><span class="texto_vermelho">Item sob encomenda, quantidade encomendar:</span>'+item.PedItem_prod_qtd_encomendar;
									}
									else
									{
										encomendar_item = '';
									}
									
									if(parseInt(item.PedItem_class,10)==1)
									{ 
										bg_item = 'background-color:#f3f3e4"';
									}
									
									if(parseInt(item.PediItem_prod_atv_consultor,10)==1)
									{ 
										bg_item = 'background-color:#FFC"';
									}
									
									if(parseInt(item.PedItem_prod_status,10)==0)
									{ 
										Status_item = 'Cancelado'; bg_item = 'background-color:#fecdb8"';
									}
									else
									{
										Status_item = ''; bg_item = '';
									}
									
									valor_total_item = eval((item.PedItem_prod_qtd*item.PedItem_prod_valor_unt)-item.PedItem_prod_valor_desc);	
									pontos_total_item = eval(item.PedItem_prod_qtd*item.PedItem_prod_pontuacao);
									
									vnome_prod = item.PedItem_prod_nome.replace(new RegExp( "\\n", "g" ),"").replace(new RegExp( "\\<br>", "g" )," ")
									
									/*meusItens += '<li style="height:auto; padding-bottom:5px; padding-top:5px; '+bg_item+'">';
									// += '<div class="item">'+ item.PedItem_idProduto+'</div>';
									meusItens += '<div class="codigo">'+ item.PedItem_prod_codigo+'</div>';
									meusItens += '<div class="produto">'+ vnome_prod+''+encomendar_item+'</div>';
									meusItens += '<div class="quant">'+ quantidadeItem +'</div>';
									meusItens += '<div class="valor">R$ '+ number_format(item.PedItem_prod_valor_unt,2,',','.')+'</div>';
									meusItens += '<div class="desconto">R$ '+ number_format(item.PedItem_prod_valor_desc,2,',','.')+'</div>';
									meusItens += '<div class="valor-total">R$ '+ number_format(quantidadeItem * valor_total_item,2,',','.')+'</div>';
									meusItens += '<div class="pontos">'+number_format(quantidadeItem * item.PedItem_prod_pontuacao,2,',','.')+'</div>'; //number_format(pontos_total_item,2,',','.')
									meusItens += '<div class="status">'+Status_item+'</div>'; //number_format(pontos_total_item,2,',','.')
									meusItens += '</li>';*/
									
									
									meusItens += '<tr>';	
									meusItens += '	<td class="codigo">'+ item.PedItem_prod_codigo+'</td>	';
									meusItens += '	<td class="produto">'+ vnome_prod+''+encomendar_item+'</td>';	
									meusItens += '	<td class="quant">'+ 1 +'</td>	';
									meusItens += '	<td class="valor">R$ '+ number_format(item.PedItem_prod_valor_unt,2,',','.')+'</td>';	
									meusItens += '	<td class="desconto">R$ '+ number_format(item.PedItem_prod_valor_desc,2,',','.')+'</td>';	
									meusItens += '	<td class="valor-total">R$ '+ number_format(1 * valor_total_item,2,',','.')+'</td>';	
									meusItens += '	<td class="pontos">'+number_format(1 * item.PedItem_prod_pontuacao,2,',','.')+'</td>';
									meusItens += '	<td class="status">'+Status_item+'</td>';	
									meusItens += '</tr>';
									
									
									mostrouItem[item.PedItem_prod_codigo] = 1;
								}
							}
						});
						
						$("#carrinho_lista").html( meusItens );
							
					}
					else
					{
						//$("#retorno").html("Item do Pedido Não Encontrado!");
						bootbox.alert("<h4>Item do Pedido Não Encontrado!</h4>", function() {});
					}
					
				},
				error : function(retorno)
				{
					//console.log(retorno);
					if ( retorno.status == 404 )
					{
						//$('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
						bootbox.alert("Houve um erro: " + retorno.status + " " + retorno.statusText, function() {});
					}
					else
					{
						//$('#retorno').html('Houve um erro desconhecido entre em contato com o administrador...');
						bootbox.alert("Houve um erro desconhecido entre em contato com o administrador...", function() {});
					}
				}
			})
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		
		}
		
		
		function Fc_redirect_pag(){
		
		$('#carregando').show(100);
		
		var vid_pedido_oficial = $("#IdPedido_oficial").val();
		var vPed_idConsultor = $("#Ped_idConsultor").val();
		var vPed_nome_consultor = $("#Ped_nome_consultor").val();
		var vPed_nome_consultor = $("#Ped_nome_consultor").val();
		var vPed_vl_sub_total = $("#Ped_vl_sub_total").val();		
		var vPed_vl_frete = $("#Ped_vl_frete").val();
		var vPed_vl_total = $("#Ped_vl_total").val();
		var vPed_cep = $("#Ped_cep").val();
		var vPed_endereco = $("#Ped_endereco").val();
		var vPed_numero = $("#Ped_numero").val();
		var vPed_compl = $("#Ped_compl").val();

		var vPed_bairro = $("#Ped_bairro").val();
		var vPed_cidade = $("#Ped_cidade").val();
		var vPed_estado = $("#Ped_estado").val();
		var vPed_idFormaPagamento = $("#Ped_idFormaPagamento").val();
		var vPed_QtdaParcela = $("#Ped_QtdaParcela").val();
		var vbin6 = $("#vbin6").val();
		var vcnome = $("#cnome").val();
		var vcseg = $("#cseg").val();
		var vcmes = $("#cmes").val();
		var vcano = $("#cano").val();
		
		//alert(parseInt(vPed_idFormaPagamento,10));

		if(parseInt(vPed_idFormaPagamento,10) == 5 && parseFloat(vPed_vl_total) > 0)
		{ 
			var idPedido = vid_pedido_oficial;
			
			if($("#endereco_boleto").val() == "")
			{
				$.ajax({
					url : 'https://cartoes.hinode.com.br/index/gera-pedido-boleto',
					type: 'POST',
					data: 
					{
						idPedido: idPedido,
					},
					dataType: "json",
					beforeSend: function() { $("#status").html("Processando..."); },
					complete: function() {},
					success: function(json) 
					{
						window.$.fancybox.open({href:json.endereco,
							 type : 'iframe',
							 width : '100%', 
							 height: '500'
						});
						
						
						$("#endereco_boleto").val(json.endereco);
						
						$("#vl_Ped_idStatus").html("Boleto gerado com sucesso!");
						$("#vl_Ped_idStatus").css("color","#0F0");
						
						$('#carregando').hide(100);
					},
					error: function(retorno) {}
				});	
			}
			else
			{
				window.$.fancybox.open({href:$("#endereco_boleto").val(),
					 type : 'iframe',
					 width : '100%', 
					 height: '500'
				});
				
				$('#carregando').hide(100);
			}
		}
		
		else if(parseInt(vPed_idFormaPagamento,10) == 6)
		{
			window.$.fancybox.open({href:'gera_pedido/mw-deposito-bancario.asp?id_pedido_oficial='+vid_pedido_oficial,
							 type : 'iframe',
							 width : '100%', 
							 height: '500'
						});
		$('#carregando').hide(100);				
			
		}
		else if(parseInt(vPed_idFormaPagamento,10) == 7)
		{
window.$.fancybox.open({href:'gera_pedido/cartao.asp?id_pedido_oficial='+vid_pedido_oficial+'&Ped_vl_total='+vPed_vl_total+'&Ped_idFormaPagamento='+vPed_idFormaPagamento+'&Ped_QtdaParcela='+vPed_QtdaParcela+'&vbin6='+vbin6+'&cmes='+vcmes+'&cano='+vcano+'&cseg='+vcseg+'&cnome='+vcnome,
							 type : 'iframe',
							 width : '100%', 
							 height: '500'
						});
		$('#carregando').hide(100);				
			
		}
else if(parseInt(vPed_idFormaPagamento,10) != 5  && parseInt(vPed_idFormaPagamento,10) != 6 && parseInt(vPed_idFormaPagamento,10) != 7)
		{ 

			var valorTotalPedidoJuros = "<% response.write(valorTotalPedidoJuros) %>";

			if(valorTotalPedidoJuros != 0)
			{
				//vPed_vl_total = valorTotalPedidoJuros;
				vPed_vl_total = number_format(valorTotalPedidoJuros,2,',','.');
				//$("#retorno").html(vPed_vl_total);
			}

			var idPedido = vid_pedido_oficial;
			var numeroCartao = vbin6;
			var nomeComprador = vcnome;
			var dataExpiracao = vcmes + "/" + vcano;
			var codigoSeguranca = vcseg;
			var valorPedido = vPed_vl_total;
			
			$.ajax({
				url : 'https://cartoes.hinode.com.br/index/gera-pedido-cartao',
				type: 'POST',
				data: 
				{
					idPedido: idPedido,
					numeroCartao: numeroCartao,
					nomeComprador: nomeComprador,
					dataExpiracao: dataExpiracao,
					codigoSeguranca: codigoSeguranca,
					valorPedido: valorPedido,
				},
				dataType: "json",
				beforeSend: function() 
				{ 
					$("#carregando").css("display","block");
					$("#vl_Ped_idStatus").html("Processando pagamento...");
					$("#vl_Ped_idStatus").css("color","#00F");
				},
				complete: function() {},
				success: function(retorno) 
				{
					$("#carregando").css("display","none");
					if(retorno.status == 'success')
					{	
						$("#vl_Ped_idStatus").html("Pedido aguardando análise na Hinode.");
						$("#vl_Ped_idStatus").css("color","#25b116");
					}
					else
					{
						$("#vl_Ped_idStatus").html("Venda não autorizada!");
						$("#vl_Ped_idStatus").css("color","#F00");
						//$("#vl_Ped_idStatus").append("<br><a href='javascript:history.back()' style=\"text-decoration:underline\"'>Tentar novamente!</a>");
					}
					//alert("Retorno BrasPag\n\nStatus: " + retorno.status + "\nPagador: " + retorno.pagador + "\nAntiFraude: " + retorno.antifraude);
				},
				error: function(retorno) {}
			});
		 }
		 else
		 {
			 $('#carregando').hide(100);
		 }
	}	
	
</script>


<div class="main-container-inner">


<!--- CONTEUDO ///////////////////////////////////////////////////////----------->      

    <div class="page-content">
    
        <div class="page-header">
            <h1>
                Pagamento
            </h1>
            <br>
            <a  type="button" class="btn btn-lg " href="https://cdh.hinode.com.br/faturamento/fat_GestaoFaturamento1.asp">
                Voltar <i class="icon-arrow-left align-top bigger-125"></i>
            </a>
        </div><!-- /.page-header -->
    
    	<form name="form1" id="form1" method="post">
            <input type="hidden" id="endereco_boleto" value=""/>
            <input type="hidden" name="IdPedido_oficial" id="IdPedido_oficial" value="<%=vIdPedido_oficial%>"/>
            <input type="hidden" name="vbin6" id="vbin6" value="<%=vbin6%>"/>
            <input type="hidden" name="Ped_vl_total" id="Ped_vl_total" />
            <input type="hidden" name="Ped_vl_frete" id="Ped_vl_frete"/>
            <input type="hidden" name="Ped_vl_sub_total" id="Ped_vl_sub_total"/>
            <input type="hidden" name="Ped_nome_consultor" id="Ped_nome_consultor"/>
            <input type="hidden" name="Ped_idConsultor" id="Ped_idConsultor"/>
            <input type="hidden" name="Ped_estado" id="Ped_estado"/>
            <input type="hidden" name="Ped_cidade" id="Ped_cidade"/>
            <input type="hidden" name="Ped_bairro" id="Ped_bairro"/>
            <input type="hidden" name="Ped_compl" id="Ped_compl"/>
            <input type="hidden" name="Ped_numero" id="Ped_numero"/>
            <input type="hidden" name="Ped_endereco" id="Ped_endereco"/>
            <input type="hidden" name="Ped_cep" id="Ped_cep"/>
            <input type="hidden" name="Ped_idFormaPagamento" id="Ped_idFormaPagamento"/>
            <input type="hidden" name="Ped_QtdaParcela" id="Ped_QtdaParcela"/>
            
            <input type="hidden" name="cnome" id="cnome" value="<%=vcnome%>"/>
            <input type="hidden" name="cseg" id="cseg" value="<%=vcseg%>"/>
            <input type="hidden" name="cmes" id="cmes" value="<%=vcmes%>"/>
            <input type="hidden" name="cano" id="cano" value="<%=vcano%>"/>
        </form>
    
        <div class="row"  id="tela">
            <div class="col-xs-12"> 
				<div class="widget-box">                
                    <div class="widget-header widget-header-flat widget-header-small">
                        <h5> Pedido Finalizado </h5>
                    </div>
                    <div class="widget-body">                        
                        <div class="widget-main">   
   							<div class="row">
                            	<div class="col-xs-6">
                                	<table class="table table-bordered table-striped" style="margin:0;">
                                        <tbody>
                                              <tr>
                                                <td style="width:200px;" ><h4 style="margin:0;"><b>Número do Pedido</b></h4></td>
                                                <td><h4 style="margin:0;" id="vl_CdPedido" class="titulo-int"></h4></td>
                                              </tr>  
                                              <tr>
                                                <td><h5 style="margin:0;"><b>Nome:</b></h5></td>
                                                <td><h5 style="margin:0;" id="vl_Ped_nome_consultor"></h5></td>
                                              </tr>
                                              <tr>
                                                <td><h5 style="margin:0;"><b>Telefone:</b></h5></td>
                                                <td><h5 style="margin:0;" id="vl_tel"></h5></td>
                                              </tr>  
                                              <tr>
                                                <td><h5 style="margin:0;"><b>Tipo:</b></h5></td>
                                                <td><h5 style="margin:0;" id="vl_tipo"></h5></td>
                                              </tr> 
                                              <tr>
                                                <td><h5 style="margin:0;"><b>Data:</b></h5></td>
                                                <td><h5 style="margin:0;" id="vl_Ped_dt_cadastro"></h5></td>
                                              </tr>
                                              <tr>
                                                <td><h5 style="margin:0;"><b>Local de Retira:</b></h5></td>
                                                <td><h5 style="margin:0;"  id="vl_Ped_local_entr"></h5></td>
                                              </tr>
                                              <tr>
                                                <td><h5 style="margin:0;"><b>Status do Pedido:</b></h5></td>
                                                <td><h5 style="margin:0;" id="vl_Ped_idStatus"></h5></td>
                                              </tr>
                                              
                                              <tr>
                                              	<td colspan="2">  
                                                     <a href="#" class="btn btn-sm btn-success" onclick="Fc_redirect_pag();" id="imprimir_boleto" style="display:none;">
                                                     	IMPRIMIR O BOLETO <i class="icon-barcode align-top bigger-160"> </i>
                                                     </a>
                                                </td>
                                              </tr>
                                              
                                        </tbody>
                                    </table>
                                </div>
                                
                                <div class="col-xs-6">
                                	
                                	<table class="table table-bordered table-striped" style="margin:0;">
                                        <tbody>   
                                        	  <tr>
                                                <td colspan="2" style="padding:0px;">
                                                	<button id="bt_print" type="button" class="btn btn-sm btn-success" style="float:right; margin-bottom:0px;">
                                        Imprimir Pedido <i class="icon-print align-top"></i>
                                    </button>
                                                </td>
                                              </tr>                                    
                                              <tr>
                                                <td style="width:200px;"><h5 style="margin:0;"><b>Sub total pedido :</b></h5></td>
                                                <td><h5 style="margin:0;"  id="vl_Ped_vl_sub_total"></h5></td>
                                              </tr>  
                                              <tr>
                                                <td><h5 style="margin:0;"><b>Frete:</b></h5></td>
                                                <td><h5 style="margin:0;" id="vl_Ped_vl_frete"></h5></td>
                                              </tr>
                                              <tr>
                                                <td><h5 style="margin:0;"><b>Crédito Usado:</b></h5></td>
                                                <td><h5 style="margin:0;" id="vl_credito_user" style="color:#C00">R$ 0,00</h5></td>
                                              </tr>  
                                              <tr>
                                                <td><h5 style="margin:0;"><b>Valor total pedido:</b></h5></td>
                                                <td><h5 style="margin:0;" id="vl_Ped_vl_total"></h5></td>
                                              </tr> 
                                              <tr>
                                                <td><h5 style="margin:0;"><b>Forma de Pagto:</b></h5></td>
                                                <td><h5 style="margin:0;" id="vl_Ped_pagamento"></h5></td>
                                              </tr>
                                              <tr>
                                                <td><h5 style="margin:0;"><b>Pontos:</b></h5></td>
                                                <td><h5 style="margin:0;"  id="vl_Ped_pontos"></h5></td>
                                              </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
   
                        </div><!-- /widget-main -->
                    </div><!-- /widget-body -->                    
                    
                </div> 
                
                <hr/>
                
                <div class="widget-box">                
                    <div class="widget-header widget-header-flat widget-header-small">
                        <h5> Endereço de entrega </h5>
                    </div>
                    <div class="widget-body">                        
                        <div class="widget-main">   
   							<div class="row">
                            
                            	<div class="col-xs-6">
                                	<table class="table table-bordered table-striped" style="margin:0;">
                                        <tbody>
                                          <tr>
                                            <td style="width:200px;"><h5 style="margin:0;"><b>Tipo de envio:</b></h5></td>
                                            <td><h5 style="margin:0;"  id="vl_Ped_Transporte"></h5></td>
                                          </tr>
                                          <tr>
                                            <td><h5 style="margin:0;"><b>Rua:</b></h5></td>
                                            <td><h5 style="margin:0;" id="vl_Ped_endereco"></h5></td>
                                          </tr>
                                          <tr>
                                            <td><h5 style="margin:0;"><b>Complemento:</b></h5></td>
                                            <td><h5 style="margin:0;" id="vl_Ped_compl"></h5></td>
                                          </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                                <div class="col-xs-6">                                    
                                	<table class="table table-bordered table-striped" style="margin:0;">
                                        <tbody>
                                          <tr>
                                            <td style="width:200px;"><h5 style="margin:0;"><b>Cep:</b></h5></td>
                                            <td><h5 style="margin:0;" id="vl_Ped_cep"></h5></td>
                                          </tr>
                                          <tr>
                                            <td><h5 style="margin:0;"><b>Bairro:</b></h5></td>
                                            <td><h5 style="margin:0;"  id="vl_Ped_bairro"></h5></td>
                                          </tr>
                                          <tr>
                                            <td><h5 style="margin:0;"><b>Cidade:</b></h5></td>
                                            <td><h5 style="margin:0;"  id="vl_Ped_cidade"></h5></td>
                                          </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
   
                        </div><!-- /widget-main -->
                    </div><!-- /widget-body -->                  
                </div> 
                
                <hr/>
                
                <div class="widget-box">                
                    <div class="widget-header widget-header-flat widget-header-small">
                        <h5> Itens do Pedido </h5>
                    </div>
                    <div class="widget-body">                        
                        <div class="widget-main">   
   							<div class="row">                            
                            	<div class="col-xs-12">
                                	<div class="table-responsive">
                                        <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Produto</th>
                                                    <th>Quant.</th>
                                                    <th>Valor Unit.</th>
                                                    <th>Desc.atv</th>
                                                    <th>Valor Total</th>
                                                    <th>Pontos</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
            
                                            <tbody id="carrinho_lista">
                                            	
                                            </tbody>
                                        </table>
                                    </div>
                                </div>                                
                            </div>   
                        </div><!-- /widget-main -->
                    </div><!-- /widget-body -->                  
                </div>
                
                              
            </div><!-- /.col -->
        </div><!-- /.row -->
        
        
    </div><!-- /.page-content -->
</div><!-- /.main-content -->
<!--- FIM CONTEUDO ///////////////////////////////////////////////////////-----------> 

 


<script language="javascript" type="text/javascript" src="assets/js/number_format.js"></script>

<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/typeahead-bs2.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
  <script src="assets/js/excanvas.min.js"></script>
<![endif]-->

<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="assets/js/chosen.jquery.min.js"></script>
<script src="assets/js/fuelux/fuelux.spinner.min.js"></script>
<script src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="assets/js/date-time/bootstrap-timepicker.min.js"></script>
<script src="assets/js/date-time/moment.min.js"></script>
<script src="assets/js/date-time/daterangepicker.min.js"></script>
<script src="assets/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/js/jquery.knob.min.js"></script>
<script src="assets/js/jquery.autosize.min.js"></script>
<script src="assets/js/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="assets/js/jquery.maskedinput.min.js"></script>
<script src="assets/js/bootstrap-tag.min.js"></script>
<script src="assets/js/bootbox.min.js"></script>

<!-- ace scripts -->

<script src="assets/js/ace-elements.min.js"></script>


<!-- inline scripts related to this page -->

<script type="text/javascript">

jQuery(function($) {

$('.limited').inputlimiter({
	remText: '%n Caracteres restantes...',
	limitText: 'Max permitido : %n.'
});	
	
	
	
});
	
</script>

<script>
	//Fc_redirect_pag();
	setTimeout(function() {
	  // Do something every 2 seconds
	  Fc_redirect_pag();
	}, 2000);
</script>  


<div id="carregando" style="display:none; position:fixed; top:0; right:0; bottom:0; left:0; background:rgba(100,100,100,0.7); z-index:9999;">

	<span style="color: #FFFFFF;
    display: block;
    font-size: 16px;
    margin: 10% auto;
    text-align: center;
    width: 200px;" id="carregando">
    
        <img  src="https://vo.hinode.com.br/vo-2/assets/images/ajax-loader.gif" style="margin-right:10px;">Carregando...
        
    </span>

</div>


</body>
</html>
