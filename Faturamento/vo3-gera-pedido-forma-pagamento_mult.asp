<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<!--#include virtual="public/pub_Componentes.asp"-->
<%
call Verifica_Permissao(11230)

id_pedido_oficial = REQUEST("id_pedido_site")

'Localizar pedido
strSql = "select pediCodigo from ped_pedidoTotal where nfreferencia = '" & id_pedido_oficial & "'"
set adoRs1 = conn1.execute(strSql)
if adoRs1.eof then
    call redi_aviso("Captura do pedido","Pedido não encontrado!")
end if

'Caso o pedido seja capturado pela hinode e o usuário do cdh tentar faturar o pedido novamente entrar na validação Nilson 15/01/2015 Solicitado por Andre
strSql = "select stnfCodigo from ped_pedidoTotal where nfreferencia = '" & id_pedido_oficial & "'"
set adoCodigo = conn1.execute(strSql)

status = adoCodigo("stnfCodigo")

if status = 10 then
    call redi_aviso("Captura do pedido","Esse pedido já esta com status pagamento efetuado.")
end if

if len(session("adyen_msg")) > 0 then
    adyen_msg = session("adyen_msg")
    session("adyen_msg") = ""
end if

strSql="select b.stnfVerificaPendencia from ped_PedidoTotal a inner join ped_StatusNf b on a.stnfCodigo=b.stnfCodigo where a.nfReferencia = '" & id_pedido_oficial & "'"
set adoVerificaPedido=conn1.execute(strsql)
if not adoVerificaPedido.eof then
   
    soma=0 ''' quando está mudando status (empenhado) para outro status empenhado, deve-se somar a qtd do item pq a view de estoque já está descontando esse item.
    if adoVerificaPedido("stnfVerificaPendencia")=true then soma=1

    Erro="0"
    strSql="exec ped_ValidarEstoque_sp '" & session("empresa_id") & "'," & id_pedido_oficial & "," & soma
    set adoEstoque=locConnvenda.execute(strsql)
    if not adoEstoque.eof then
        Erro=adoEstoque("cod")
        msg =adoEstoque("msg")
    end if
    set adoEstoque=nothing

    if cstr(Erro)<>"0" then call redi_Aviso("Estoque",msg)

end if

set adoVerificaPedido=nothing
set adoRs1 = nothing
set adoCodigo = nothing
%>
<!----------- //////////////////// NOVO VO ABAIXO /////////////////////////////////// --->           
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>HINODE - FRANQUIA</title>

    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- basic styles -->

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />

    <!--[if IE 7]>
        <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
    <![endif]-->

    <!-- page specific plugin styles -->

    <!-- fonts -->

    <link rel="stylesheet" href="assets/css/ace-fonts.css" />

    <!-- ace styles -->

    <link rel="stylesheet" href="assets/css/ace.min.css" />
    <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="assets/css/ace-skins.min.css" />

    <!--[if lte IE 8]>
          <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->

    <!-- inline styles related to this page -->
    <style type="text/css">
        .navbar {
            background: #ffffff; /* Old browsers */
            background: -moz-linear-gradient(left, #ffffff 0%, #fff8e2 51%, #ffcb07 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, right top, color-stop(0%,#ffffff), color-stop(51%,#fff8e2), color-stop(100%,#ffcb07)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(left, #ffffff 0%,#fff8e2 51%,#ffcb07 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(left, #ffffff 0%,#fff8e2 51%,#ffcb07 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(left, #ffffff 0%,#fff8e2 51%,#ffcb07 100%); /* IE10+ */
            background: linear-gradient(to right, #ffffff 0%,#fff8e2 51%,#ffcb07 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffcb07',GradientType=1 ); /* IE6-9 */
        }

        .ace-nav > li.light-blue > a {
            background-color: #ffcb07;
            background: none;
        }

        .ace-nav > li.open.light-blue > a {
            background-color: #ffcb07 !important;
            background: none;
        }

        .ace-nav > li.light-blue > a:hover {
            background: none;
            color: #d15b47;
        }

        .ace-nav > li.light-blue > a:focus {
            background-color: #ffcb07;
            background: none;
            color: #d15b47;
        }

        .ace-nav > li > a {
            color: #d15b47;
        }

        .ace-nav > li.amarelo > a {
            /*background-color: #ffcb07;*/
            background: none;
        }

        .ace-nav > li.open > a {
            background-color: #ffcb07 !important;
        }

        .ace-nav > li > a > [class*="icon-"] {
            color: #d15b47;
        }

        .ace-nav > li.open > a > [class*="icon-"] {
            color: #fff;
        }

        .ace-nav > li {
            border-left: 1px solid #ffcb07;
            float: left !important;
            height: 45px;
            line-height: 45px;
            margin-top: 24px;
            position: relative;
        }

        .navbar .navbar-header {
            float: right;
            margin: 0 !important;
        }

        .forma_pag_cartao {
            display: none;
        }

        .vvalor {
            text-align: right;
        }

        .tabela-pagamento {
            width: 400px;
        }

        .expandir, .pagamento-vista {
            cursor:pointer;
        }

        .select-pagamento {
            display: inline;
            padding-right: 4px;
            width: 100px;
        }
        
        .div_parcelas {
            text-align: right;
        }

        .pagamento-vista {
             color:#669fc7;
        }

        #forma_pag_vista {
            display: none;
        }

        #exibir-pagamento-vista {
            display: none;
        }

    </style>

    <!-- ace settings handler -->

    <script src="assets/js/ace-extra.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->

    <!-- Hinode -->
    <link rel="stylesheet" href="assets/css/hinode.css" />

    <!-- basic scripts -->

    <!--[if !IE]> -->
    <script type="text/javascript">
        window.jQuery || document.write("<scr" + "ipt src='assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
    </script>
    <!-- <![endif]-->

    <!--[if IE]>
        <script type="text/javascript">
            window.jQuery || document.write("<scr"+"ipt src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
        </script>
    <![endif]-->

    <script type="text/javascript">
    if ("ontouchend" in document)
        document.write("<scr" + "ipt src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/typeahead-bs2.min.js"></script>

    <!-- page specific plugin scripts -->

    <!--[if lte IE 8]>
        <script src="assets/js/excanvas.min.js"></script>
    <![endif]-->

    <script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="assets/js/jquery.ui.touch-punch.min.js"></script>
    <script src="assets/js/jquery.slimscroll.min.js"></script>
    <script src="assets/js/jquery.easy-pie-chart.min.js"></script>
    <script src="assets/js/jquery.sparkline.min.js"></script>
    <script src="assets/js/flot/jquery.flot.min.js"></script>
    <script src="assets/js/flot/jquery.flot.pie.min.js"></script>
    <script src="assets/js/flot/jquery.flot.resize.min.js"></script>

    <!-- ace scripts -->

    <script src="assets/js/ace-elements.min.js"></script>
    <script src="assets/js/ace.min.js"></script>

    <!-- inline scripts related to this page -->

    <script type="text/javascript">
        var vid_pedido_oficial = "<%=id_pedido_oficial%>";
        var adyen_msg = '<%=adyen_msg%>';
    </script>
    <script type="text/javascript" src="vo3-gera-pedido-forma-pagamento_mult.js"></script>

    <!-- Cycle -->
    <!--
    <script src="hinode/js/jquery.cycle.all.js" ></script>
    <script type="text/javascript">
        $('#slide').cycle({ 
            fx:     'scrollHorz', 
            speed:  1000, 
            timeout: 200, 
            next:   '#next_slide', 
            prev:   '#prev_slide' 
        });
    </script>
    -->

</head>
<body>

    <div class="main-container" id="main-container">


        <div class="main-container-inner">


            <div class="page-content">

                <div class="page-header">
                    <h1>Pagamento
                    </h1>
                    <br>
                    <a type="button" class="btn btn-lg " href="fat_GestaoFaturamento1.asp">Voltar <i class="icon-arrow-left align-top bigger-125"></i>
                    </a>
                </div>
                <!-- /.page-header -->



                <div class="row">
                    <div class="col-xs-12">

                        <div class="row">
                            <div class="col-sm-3">

                                <!--####### USO ###########--->
                                <div class="widget-box">

                                    <div class="widget-header widget-header-flat widget-header-small">
                                        <h5>Detalhes do Pedido</h5>
                                    </div>

                                    <div class="widget-body">
                                        <div class="widget-main barraDeRolagemUso">

                                            <table class="table table-bordered table-striped">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <h4 style="margin: 0;"><b>Valor do pedido:</b></h4>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <h2 style="margin: 0;" id="txt_vl_final">R$ 0,00</h2>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <hr />

                                            <table class="table table-bordered table-striped">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <h4 style="margin: 0;"><b>Valor da Parcela:</b></h4>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <h2 style="margin: 0;" id="txt_vl_parcela">-</h2>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <hr />

                                            <table class="table table-bordered table-striped">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <h4 style="margin: 0;"><b>Valor restante:</b></h4>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <h2 style="margin: 0;" id="txt_valor_restante">0,00</h2>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <hr />

                                            <table class="table table-bordered table-striped">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <h4 style="margin: 0;"><b>Total em pontos:</b></h4>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <h2 style="margin: 0;" id="txt_pontos_total_pedido">0,00</h2>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <!--
                                            <hr />

                                            <table class="table table-bordered table-striped">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <h4 style="margin: 0;">
                                                                <b>Pagamento Selecionado:</b>
                                                            </h4>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <h4 style="margin: 0;" id="cartao_vazio">-</h4>
                                                            <h4 style="margin: 0; display: none;" id="cartao-visa" class="reset">Cartão de Crédito Visa</h4>
                                                            <h4 style="margin: 0; display: none;" id="cartao-master" class="reset">Cartão de Crédito Master Card</h4>
                                                            <h4 style="margin: 0; display: none;" id="cartao-aura" class="reset">Cartão de Crédito Aura</h4>
                                                            <h4 style="margin: 0; display: none;" id="cartao-american" class="reset">Cartão de Crédito American Express</h4>
                                                            <h4 style="margin: 0; display: none;" id="cartao-diners" class="reset">Cartão de Crédito Diners</h4>
                                                            <h4 style="margin: 0; display: none;" id="cartao-hipercard" class="reset">Cartão de Crédito Hipercard</h4>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            -->

                                        </div>
                                        <!-- /widget-main -->
                                    </div>
                                    <!-- /widget-body -->
                                </div>

                            </div>
                            <div class="col-sm-9">
                                <!--####### PRODUTOS ###########--->
                                <div class="widget-box">
                                    <div class="widget-header widget-header-flat widget-header-small">
                                        <h5>Formas de Pagamento </h5>
                                    </div>

                                    <div class="widget-body">

                                        <div class="widget-main">

                                            <div class="widget-box transparent" id="escolha-qtd-cartao">
                                                <div class="widget-header">
                                                    <h4 class="lighter">Escolha a quantidade de cartões para pagamento</h4>
                                                </div>
                                                <div class="widget-body">
                                                    <div class="widget-main padding-6 no-padding-left no-padding-right">
                                                        <div class="row">
                                                            <div class="col-sm-12 text-center">
                                                                <div class="radio" align="center">
                                                                    <table class="table table-bordered tabela-pagamento">
                                                                        <thead>
                                                                            <th colspan="2" class="text-center">Selecione a forma de pagamento, por favor!</th>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="text-right" width="50%">
                                                                                    <span>Quantidade de cartões:</span>
                                                                                </td>
                                                                                <td class="text-left" width="50%">
                                                                                    <select name="qtd-cartao-select" id="qtd-cartao-select" class="select-pagamento">
                                                                                        <option selected="selected" value="01">01</option>
                                                                                        <option value="02">02</option>
                                                                                        <option value="03">03</option>
                                                                                        <option value="04">04</option>
                                                                                        <option value="05">05</option>
                                                                                    </select>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-right" width="50%">
                                                                                    <span>Quantidade de parcelas:</span>
                                                                                </td>
                                                                                <td class="text-left" width="50%">
                                                                                    <div id="div_parcelas" class="select-pagamento"></div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="exibir-pagamento-vista">
                                                                                <td class="text-center" colspan="2">
                                                                                    <button id="adicionar-pagamento-vista" value="1" class="btn btn-info btn-xs">Adicionar pagamento à vista</button>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="widget-box transparent forma_pag_cartao" id="forma_pag_vista">
                                                <div class="widget-header">
                                                    <h4 class="lighter">Pagamento à vista</h4>
                                                    <span class="expandir" id="remover-pagamento-vista">[remover]</span>
                                                </div>
                                                <div class="widget-box transparent">
                                                    <div class="widget-body">
                                                        <div class="widget-main padding-6 no-padding-left no-padding-right">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <form class="form-horizontal" role="form">
                                                                        <div class="form-group">
                                                                            <label class="col-sm-3 control-label no-padding-right">* Valor a pagar: </label>
                                                                            <div class="col-sm-9">
                                                                                <input type="text" class="vvalor limited" id="vvalor-vista" name="vvalor-vista" placeholder="Valor a pagar" />
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="widget-box transparent forma_pag_cartao" id="forma_pag_cartao-1">
                                                <div class="widget-header">
                                                    <h4 class="lighter">Cartão de Crédito 1</h4>
                                                    <span class="expandir" id="expandir-1">[esconder]</span>
                                                </div>
                                            </div>


                                            <div class="widget-box transparent cartao_dados" id="cartao_dados-1">
                                                <div class="widget-body">
                                                    <div class="widget-main padding-6 no-padding-left no-padding-right">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <form class="form-horizontal" role="form">

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Valor a pagar: </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="vvalor limited" id="vvalor-1" name="vvalor" placeholder="Valor a pagar" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                             					        <label class="col-sm-3 control-label no-padding-right"> * Parcelas: </label>                                                                        
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="div_parcelas" name="div_parcelas-1" id="div_parcelas-1" value="Selecione uma parcela!" readonly/>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Número do cartão: </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" id="vbin6-1" name="vbin6-1" class="col-xs-10 col-sm-5 limited vbin6" placeholder="Número do cartão" maxlength="16" />
                                                                            <span class="help-inline col-xs-12 col-sm-7">
                                                                                <span class="middle">Digite os números do cartão</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Nome do titular:  </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" id="cnome-1" name="cnome-1" class="col-xs-10 col-sm-5 limited" placeholder="Nome do titular" maxlength="50" />
                                                                            <span class="help-inline col-xs-12 col-sm-7">
                                                                                <span class="middle">(Como está impresso no cartão)</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Código de segurança:   </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" id="cseg-1" name="cseg-1" class="col-xs-10 col-sm-5 limited cseg" placeholder="Código de segurança" maxlength="4" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Validade mês/ano:   </label>
                                                                        <div class="col-sm-9">

                                                                            <select name="cmes-1" id="cmes-1" style="padding-right: 4px; width: 60px;">
                                                                                <option selected="selected" value=""></option>
                                                                                <option value="01">01</option>
                                                                                <option value="02">02</option>
                                                                                <option value="03">03</option>
                                                                                <option value="04">04</option>
                                                                                <option value="05">05</option>
                                                                                <option value="06">06</option>
                                                                                <option value="07">07</option>
                                                                                <option value="08">08</option>
                                                                                <option value="09">09</option>
                                                                                <option value="10">10</option>
                                                                                <option value="11">11</option>
                                                                                <option value="12">12</option>
                                                                            </select>
                                                                            <span>/</span>
                                                                            <select name="cano-1" id="cano-1" style="padding-right: 4px; width: 80px;">
                                                                                <option selected="selected" value=""></option>
                                                                                <option value="2015">2015</option>
                                                                                <option value="2016">2016</option>
                                                                                <option value="2017">2017</option>
                                                                                <option value="2018">2018</option>
                                                                                <option value="2019">2019</option>
                                                                                <option value="2020">2020</option>
                                                                                <option value="2021">2021</option>
                                                                                <option value="2022">2022</option>
                                                                                <option value="2023">2023</option>
                                                                                <option value="2024">2024</option>
                                                                                <option value="2025">2025</option>
                                                                                <option value="2026">2026</option>
                                                                                <option value="2027">2027</option>
                                                                                <option value="2028">2028</option>
                                                                                <option value="2029">2029</option>
                                                                                <option value="2030">2030</option>
                                                                            </select>

                                                                        </div>
                                                                    </div>

                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />
                                            </div>

                                            <div class="widget-box transparent forma_pag_cartao" id="forma_pag_cartao-2" style="display: none;">
                                                <div class="widget-header">
                                                    <h4 class="lighter">Cartão de Crédito 2</h4>
                                                    <span class="expandir" id="expandir-2">[esconder]</span>
                                                </div>
                                            </div>

                                            <div class="widget-box transparent cartao_dados" id="cartao_dados-2" style="display: none;">
                                                <div class="widget-body">
                                                    <div class="widget-main padding-6 no-padding-left no-padding-right">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <form class="form-horizontal" role="form">

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Valor a pagar: </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="vvalor limited" id="vvalor-2" name="vvalor" placeholder="Valor a pagar" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                             					        <label class="col-sm-3 control-label no-padding-right"> * Parcelas: </label>                                                                        
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="div_parcelas" name="div_parcelas-2" id="div_parcelas-2" value="Selecione uma parcela!" readonly/>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Número do cartão: </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" id="vbin6-2" name="vbin6-2" class="col-xs-10 col-sm-5 limited vbin6" placeholder="Número do cartão" maxlength="16" />
                                                                            <span class="help-inline col-xs-12 col-sm-7">
                                                                                <span class="middle">Digite os números do cartão</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Nome do titular:  </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" id="cnome-2" name="cnome-2" class="col-xs-10 col-sm-5 limited" placeholder="Nome do titular" maxlength="50" />
                                                                            <span class="help-inline col-xs-12 col-sm-7">
                                                                                <span class="middle">(Como está impresso no cartão)</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Código de segurança:   </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" id="cseg-2" name="cseg-2" class="col-xs-10 col-sm-5 limited cseg" placeholder="Código de segurança" maxlength="4" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Validade mês/ano:   </label>
                                                                        <div class="col-sm-9">

                                                                            <select name="cmes-2" id="cmes-2" style="padding-right: 4px; width: 60px;">
                                                                                <option selected="selected" value=""></option>
                                                                                <option value="01">01</option>
                                                                                <option value="02">02</option>
                                                                                <option value="03">03</option>
                                                                                <option value="04">04</option>
                                                                                <option value="05">05</option>
                                                                                <option value="06">06</option>
                                                                                <option value="07">07</option>
                                                                                <option value="08">08</option>
                                                                                <option value="09">09</option>
                                                                                <option value="10">10</option>
                                                                                <option value="11">11</option>
                                                                                <option value="12">12</option>
                                                                            </select>
                                                                            <span>/</span>
                                                                            <select name="cano-2" id="cano-2" style="padding-right: 4px; width: 80px;">
                                                                                <option selected="selected" value=""></option>
                                                                                <option value="2015">2015</option>
                                                                                <option value="2016">2016</option>
                                                                                <option value="2017">2017</option>
                                                                                <option value="2018">2018</option>
                                                                                <option value="2019">2019</option>
                                                                                <option value="2020">2020</option>
                                                                                <option value="2021">2021</option>
                                                                                <option value="2022">2022</option>
                                                                                <option value="2023">2023</option>
                                                                                <option value="2024">2024</option>
                                                                                <option value="2025">2025</option>
                                                                                <option value="2026">2026</option>
                                                                                <option value="2027">2027</option>
                                                                                <option value="2028">2028</option>
                                                                                <option value="2029">2029</option>
                                                                                <option value="2030">2030</option>
                                                                            </select>

                                                                        </div>
                                                                    </div>

                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />
                                            </div>

                                            <div class="widget-box transparent forma_pag_cartao" id="forma_pag_cartao-3" style="display: none;">
                                                <div class="widget-header">
                                                    <h4 class="lighter">Cartão de Crédito 3</h4>
                                                    <span class="expandir" id="expandir-3">[esconder]</span>
                                                </div>
                                            </div>

                                            <div class="widget-box transparent cartao_dados" id="cartao_dados-3" style="display: none;">
                                                <div class="widget-body">
                                                    <div class="widget-main padding-6 no-padding-left no-padding-right">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <form class="form-horizontal" role="form">

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Valor a pagar: </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="vvalor limited" id="vvalor-3" name="vvalor" placeholder="Valor a pagar" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                             					        <label class="col-sm-3 control-label no-padding-right"> * Parcelas: </label>                                                                        
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="div_parcelas" name="div_parcelas-3" id="div_parcelas-3" value="Selecione uma parcela!" readonly/>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Número do cartão: </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" id="vbin6-3" name="vbin6-3" class="col-xs-10 col-sm-5 limited vbin6" placeholder="Número do cartão" maxlength="16" />
                                                                            <span class="help-inline col-xs-12 col-sm-7">
                                                                                <span class="middle">Digite os números do cartão</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Nome do titular:  </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" id="cnome-3" name="cnome-3" class="col-xs-10 col-sm-5 limited" placeholder="Nome do titular" maxlength="50" />
                                                                            <span class="help-inline col-xs-12 col-sm-7">
                                                                                <span class="middle">(Como está impresso no cartão)</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Código de segurança:   </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" id="cseg-3" name="cseg-3" class="col-xs-10 col-sm-5 limited cseg" placeholder="Código de segurança" maxlength="4" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Validade mês/ano:   </label>
                                                                        <div class="col-sm-9">

                                                                            <select name="cmes-3" id="cmes-3" style="padding-right: 4px; width: 60px;">
                                                                                <option selected="selected" value=""></option>
                                                                                <option value="01">01</option>
                                                                                <option value="02">02</option>
                                                                                <option value="03">03</option>
                                                                                <option value="04">04</option>
                                                                                <option value="05">05</option>
                                                                                <option value="06">06</option>
                                                                                <option value="07">07</option>
                                                                                <option value="08">08</option>
                                                                                <option value="09">09</option>
                                                                                <option value="10">10</option>
                                                                                <option value="11">11</option>
                                                                                <option value="12">12</option>
                                                                            </select>
                                                                            <span>/</span>
                                                                            <select name="cano-3" id="cano-3" style="padding-right: 4px; width: 80px;">
                                                                                <option selected="selected" value=""></option>
                                                                                <option value="2015">2015</option>
                                                                                <option value="2016">2016</option>
                                                                                <option value="2017">2017</option>
                                                                                <option value="2018">2018</option>
                                                                                <option value="2019">2019</option>
                                                                                <option value="2020">2020</option>
                                                                                <option value="2021">2021</option>
                                                                                <option value="2022">2022</option>
                                                                                <option value="2023">2023</option>
                                                                                <option value="2024">2024</option>
                                                                                <option value="2025">2025</option>
                                                                                <option value="2026">2026</option>
                                                                                <option value="2027">2027</option>
                                                                                <option value="2028">2028</option>
                                                                                <option value="2029">2029</option>
                                                                                <option value="2030">2030</option>
                                                                            </select>

                                                                        </div>
                                                                    </div>

                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />
                                            </div>

                                            <div class="widget-box transparent forma_pag_cartao" id="forma_pag_cartao-4" style="display: none;">
                                                <div class="widget-header">
                                                    <h4 class="lighter">Cartão de Crédito 4</h4>
                                                    <span class="expandir" id="expandir-4">[esconder]</span>
                                                </div>
                                            </div>

                                            <div class="widget-box transparent cartao_dados" id="cartao_dados-4" style="display: none;">
                                                <div class="widget-body">
                                                    <div class="widget-main padding-6 no-padding-left no-padding-right">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <form class="form-horizontal" role="form">

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Valor a pagar: </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="vvalor limited" id="vvalor-4" name="vvalor" placeholder="Valor a pagar" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                             					        <label class="col-sm-3 control-label no-padding-right"> * Parcelas: </label>                                                                        
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="div_parcelas" name="div_parcelas-4" id="div_parcelas-4" value="Selecione uma parcela!" readonly/>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Número do cartão: </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" id="vbin6-4" name="vbin6-4" class="col-xs-10 col-sm-5 limited vbin6" placeholder="Número do cartão" maxlength="16" />
                                                                            <span class="help-inline col-xs-12 col-sm-7">
                                                                                <span class="middle">Digite os números do cartão</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Nome do titular:  </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" id="cnome-4" name="cnome-4" class="col-xs-10 col-sm-5 limited" placeholder="Nome do titular" maxlength="50" />
                                                                            <span class="help-inline col-xs-12 col-sm-7">
                                                                                <span class="middle">(Como está impresso no cartão)</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Código de segurança:   </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" id="cseg-4" name="cseg-4" class="col-xs-10 col-sm-5 limited cseg" placeholder="Código de segurança" maxlength="4" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Validade mês/ano:   </label>
                                                                        <div class="col-sm-9">

                                                                            <select name="cmes-4" id="cmes-4" style="padding-right: 4px; width: 60px;">
                                                                                <option selected="selected" value=""></option>
                                                                                <option value="01">01</option>
                                                                                <option value="02">02</option>
                                                                                <option value="03">03</option>
                                                                                <option value="04">04</option>
                                                                                <option value="05">05</option>
                                                                                <option value="06">06</option>
                                                                                <option value="07">07</option>
                                                                                <option value="08">08</option>
                                                                                <option value="09">09</option>
                                                                                <option value="10">10</option>
                                                                                <option value="11">11</option>
                                                                                <option value="12">12</option>
                                                                            </select>
                                                                            <span>/</span>
                                                                            <select name="cano-4" id="cano-4" style="padding-right: 4px; width: 80px;">
                                                                                <option selected="selected" value=""></option>
                                                                                <option value="2015">2015</option>
                                                                                <option value="2016">2016</option>
                                                                                <option value="2017">2017</option>
                                                                                <option value="2018">2018</option>
                                                                                <option value="2019">2019</option>
                                                                                <option value="2020">2020</option>
                                                                                <option value="2021">2021</option>
                                                                                <option value="2022">2022</option>
                                                                                <option value="2023">2023</option>
                                                                                <option value="2024">2024</option>
                                                                                <option value="2025">2025</option>
                                                                                <option value="2026">2026</option>
                                                                                <option value="2027">2027</option>
                                                                                <option value="2028">2028</option>
                                                                                <option value="2029">2029</option>
                                                                                <option value="2030">2030</option>
                                                                            </select>

                                                                        </div>
                                                                    </div>

                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="widget-box transparent forma_pag_cartao" id="forma_pag_cartao-5" style="display: none;">
                                                <div class="widget-header">
                                                    <h4 class="lighter">Cartão de Crédito 5</h4>
                                                    <span class="expandir" id="expandir-5">[esconder]</span>
                                                </div>
                                            </div>

                                            <div class="widget-box transparent cartao_dados" id="cartao_dados-5" style="display: none;">
                                                <div class="widget-body">
                                                    <div class="widget-main padding-6 no-padding-left no-padding-right">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <form class="form-horizontal" role="form">

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Valor a pagar: </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="vvalor limited" id="vvalor-5" name="vvalor" placeholder="Valor a pagar" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                             					        <label class="col-sm-3 control-label no-padding-right"> * Parcelas: </label>                                                                        
                                                                        <div class="col-sm-9">
                                                                            <input type="text" class="div_parcelas" name="div_parcelas-5" id="div_parcelas-5" value="Selecione uma parcela!" readonly/>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Número do cartão: </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" id="vbin6-5" name="vbin6-5" class="col-xs-10 col-sm-5 limited vbin6" placeholder="Número do cartão" maxlength="16" />
                                                                            <span class="help-inline col-xs-12 col-sm-7">
                                                                                <span class="middle">Digite os números do cartão</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Nome do titular:  </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" id="cnome-5" name="cnome-5" class="col-xs-10 col-sm-5 limited" placeholder="Nome do titular" maxlength="50" />
                                                                            <span class="help-inline col-xs-12 col-sm-7">
                                                                                <span class="middle">(Como está impresso no cartão)</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Código de segurança:   </label>
                                                                        <div class="col-sm-9">
                                                                            <input type="text" id="cseg-5" name="cseg-5" class="col-xs-10 col-sm-5 limited cseg" placeholder="Código de segurança" maxlength="4" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label no-padding-right">* Validade mês/ano:   </label>
                                                                        <div class="col-sm-9">

                                                                            <select name="cmes-5" id="cmes-5" style="padding-right: 4px; width: 60px;">
                                                                                <option selected="selected" value=""></option>
                                                                                <option value="01">01</option>
                                                                                <option value="02">02</option>
                                                                                <option value="03">03</option>
                                                                                <option value="04">04</option>
                                                                                <option value="05">05</option>
                                                                                <option value="06">06</option>
                                                                                <option value="07">07</option>
                                                                                <option value="08">08</option>
                                                                                <option value="09">09</option>
                                                                                <option value="10">10</option>
                                                                                <option value="11">11</option>
                                                                                <option value="12">12</option>
                                                                            </select>
                                                                            <span>/</span>
                                                                            <select name="cano-5" id="cano-5" style="padding-right: 4px; width: 80px;">
                                                                                <option selected="selected" value=""></option>
                                                                                <option value="2015">2015</option>
                                                                                <option value="2016">2016</option>
                                                                                <option value="2017">2017</option>
                                                                                <option value="2018">2018</option>
                                                                                <option value="2019">2019</option>
                                                                                <option value="2020">2020</option>
                                                                                <option value="2021">2021</option>
                                                                                <option value="2022">2022</option>
                                                                                <option value="2023">2023</option>
                                                                                <option value="2024">2024</option>
                                                                                <option value="2025">2025</option>
                                                                                <option value="2026">2026</option>
                                                                                <option value="2027">2027</option>
                                                                                <option value="2028">2028</option>
                                                                                <option value="2029">2029</option>
                                                                                <option value="2030">2030</option>
                                                                            </select>

                                                                        </div>
                                                                    </div>

                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- /widget-main -->
                                    </div>
                                    <!-- /widget-body -->

                                </div>

                                <hr />

                                <div class="row">
                                    <div class="col-sm-12" style="text-align: right;">
                                        <button class="btn btn-lg btn-success" type="button" id="bt_continuar" name="bt_continuar">
                                            Efetuar Pagamento <i class="icon-shopping-cart align-top bigger-125"></i>
                                        </button>

                                        <form name="form-concluir-compra" id="form-concluir-compra" method="post" action="vo3-gera-pedido-forma-pagamento.asp">
                                            <input type="hidden" name="ss_pg" id="ss_pg" value="<%=vss_pg%>" />

                                            <input type="hidden" name="id_cons" id="id_cons" value="<%=vid_cons%>" />
                                            <input type="hidden" name="modo_entrega" id="modo_entrega" value="<%=vmodo_entrega%>" />
                                            <input type="hidden" name="nome_cons" id="nome_cons" value="<%=vnome_cons%>" />
                                            <input type="hidden" name="id_cdh_retira" id="id_cdh_retira" value="<%=vid_cdh_retira%>" />
                                            <input type="hidden" name="id_cdh_retira_desc" id="id_cdh_retira_desc" value="<%=vid_cdh_retira_desc%>" />
                                            <input type="hidden" name="email_ped" id="email_ped" value="<%=vemail_ped%>" />
                                            <input type="hidden" name="cdh_retira_email" id="cdh_retira_email" value="<%=vcdh_retira_email%>" />

                                            <input type="hidden" name="cep" id="cep" value="<%=vcep%>" />
                                            <input type="hidden" name="endereco" id="endereco" value="<%=vendereco%>" />
                                            <input type="hidden" name="numero" id="numero" value="<%=vnumero%>" />
                                            <input type="hidden" name="complemento" id="complemento" value="<%=vcomplemento%>" />
                                            <input type="hidden" name="bairro" id="bairro" value="<%=vbairro%>" />
                                            <input type="hidden" name="cidade" id="cidade" value="<%=vcidade%>" />
                                            <input type="hidden" name="estado" id="estado" value="<%=vestado%>" />

                                            <input type="hidden" name="id_forma_pag" id="id_forma_pag" value="<%=vid_forma_pag%>" />
                                            <input type="hidden" name="desc_forma_pag" id="desc_forma_pag" value="<%=vdesc_forma_pag%>" />
                                            <input type="hidden" name="desc_forma_pag_verf" id="desc_forma_pag_verf" value="" />
                                            <input type="hidden" name="qtd_parc" id="qtd_parc" value="" />

                                            <input type="hidden" name="vl_total_pedido_frete" id="vl_total_pedido_frete" value="<%=vvl_total_pedido_frete%>" />
                                            <input type="hidden" name="vl_total_pedido" id="vl_total_pedido" value="<%=vvl_total_pedido%>" />
                                            <input type="hidden" name="vl_total_pedido_juros" id="vl_total_pedido_juros" value="0" />
                                            <input type="hidden" name="vl_total_pedido_bkp" id="vl_total_pedido_bkp" value="<%=vvl_total_pedido%>" />
                                            <input type="hidden" name="vl_final" id="vl_final" value="0" />

                                            <input type="hidden" name="vl_sub_total_pedido" id="vl_sub_total_pedido" value="<%=vvl_sub_total_pedido%>" />
                                            <input type="hidden" name="pontos_total_pedido" id="pontos_total_pedido" value="<%=vpontos_total_pedido%>" />
                                            <input type="hidden" name="peso_total_pedido" id="peso_total_pedido" value="<%=vpeso_total_pedido%>" />
                                            <input type="hidden" name="forma_envio_transp_ped" id="forma_envio_transp_ped" value="<%=vforma_envio_transp_ped%>" />
                                            <input type="hidden" name="forma_envio_transp_desc_ped" id="forma_envio_transp_desc_ped" value="<%=vforma_envio_transp_desc_ped%>" />
                                            <input type="hidden" name="prazo_transp_ped" id="prazo_transp_ped" value="<%=vprazo_transp_ped%>" />
                                            <input type="hidden" name="vl_credito_usado" id="vl_credito_usado" value="<%=vvl_credito_usado%>" />
                                            <input type="hidden" name="qtd_parc_auto" id="qtd_parc_auto" value="<%=vqtd_parc_auto%>" />

                                            <input type="hidden" name="ped_juros" id="ped_juros" value="" />
                                            <input type="hidden" name="atv_cons" id="atv_cons" value="<%=vatv_cons%>" />
                                            <input type="hidden" name="atv_cad_cons_bkp" id="atv_cad_cons_bkp" value="<%=vatv_cad_cons_bkp%>" />
                                            <input type="hidden" name="fator_parcela" id="fator_parcela" value="" />
                                            <input type="hidden" name="id_pedido_oficial" id="id_pedido_oficial" value="<%=vid_pedido_oficial%>" />
                                            <input type="hidden" name="valorDesconto" id="valorDesconto" value="<%=VvalorDesconto%>" />
                                            <input type="hidden" name="tel" id="tel" value="<%=vtel%>" />

                                            <input type="hidden" name="pontuacao_minima" id="pontuacao_minima" value="<%=vpontuacao_minima%>" />

                                            <input type="hidden" id="codigokit" value="" name="codigokit">
                                            <input type="hidden" id="upgrade" value="" name="upgrade">
                                            <input type="hidden" id="upgrade_produto_anterior" value="" name="upgrade_produto_anterior">
                                        </form>
                                        <%'Form para enviar os dados para pagamento Adyen Nilson 20/07/2015%>
                                        <form name="Finalizar_pagamento" id="fin_pag" method="post" action="cdh-adyen_mult.asp">
                                            <input type="hidden" name="pedido" id="pedido"/>
                                            <input type="hidden" name="ncartao" id="ncartao"/>
                                            <input type="hidden" name="titular" id="titular"/>
                                            <input type="hidden" name="cvc" id="cvc"/>
                                            <input type="hidden" name="mesval" id="mesval"/>
                                            <input type="hidden" name="anoval" id="anoval"/>
                                            <input type="hidden" name="parcelas" id="parcelas"/>
                                            <input type="hidden" name="valorcartao" id="valorcartao"/>
                                            <input type="hidden" name="valorvista" id="valorvista"/>
                                            <input type="hidden" name="valor" id="valor"/>
                                            <input type="hidden" name="juros" id="juros"/>
                                            <input type="hidden" name="valorfinal" id="valorfinal">
                                            <input type="hidden" name="qtdcartoes" id="qtd-cartoes" value="" />
                                        </form>

                                    </div>
                                </div>


                            </div>
                        </div>
                        <!-- Row -->

                        <hr />

                        <div class="row">
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-6" style="text-align: right; padding-bottom: 50px;">
                                <!--<div>
                    	<a class="btn btn-lg" href="vo3-gera-pedido.asp">
                            <i class="icon-undo align-top bigger-125"></i>
                            Recomeçar
                        </a>
                      
                    	<button class="btn btn-lg btn-success" type="button">
                        	Continuar <i class="icon-arrow-right align-top bigger-125"></i>
                        </button>
                        </div>-->
                            </div>
                        </div>
                        <!-- Row -->


                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->


            </div>
            <!-- /.page-content -->
        </div>

    </div><!-- /.main-content -->
    <!--- FIM CONTEUDO ///////////////////////////////////////////////////////----------->

    <script type="text/javascript" src="assets/js/number_format.js"></script>

    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/typeahead-bs2.min.js"></script>

    <!-- page specific plugin scripts -->

    <!--[if lte IE 8]>
        <script type="text/javascript" src="assets/js/excanvas.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="assets/js/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/fuelux/fuelux.spinner.min.js"></script>
    <script type="text/javascript" src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="assets/js/date-time/bootstrap-timepicker.min.js"></script>
    <script type="text/javascript" src="assets/js/date-time/moment.min.js"></script>
    <script type="text/javascript" src="assets/js/date-time/daterangepicker.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.knob.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.autosize.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.inputlimiter.1.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.maskMoney.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-tag.min.js"></script>
    <script type="text/javascript" src="assets/js/bootbox.min.js"></script>

    <!-- ace scripts -->

    <script src="assets/js/ace-elements.min.js"></script>


    <!-- inline scripts related to this page -->

    <script type="text/javascript">

        jQuery(function ($) {
            $('.limited').inputlimiter({
                remText: '%n Caracteres restantes...',
                limitText: 'Max permitido : %n.'
            });
        });

    </script>


    <div id="carregando-pedido" style="display: none; position: fixed; top: 0; right: 0; bottom: 0; left: 0; background: rgba(100,100,100,0.7); z-index: 9999;">

        <span style="color: #FFFFFF; display: block; font-size: 16px; margin: 10% auto; text-align: center; width: 200px;">

            <img src="https://vo.hinode.com.br/vo-2/assets/images/ajax-loader.gif" style="margin-right: 10px;"/>
            <br>
            <span>Carregando informações do pedido, aguarde por favor!...</span>
        
        </span>

    </div>

    <div id="carregando" style="display: none; position: fixed; top: 0; right: 0; bottom: 0; left: 0; background: rgba(100,100,100,0.7); z-index: 9999;">

        <span style="color: #FFFFFF; display: block; font-size: 16px; margin: 10% auto; text-align: center; width: 200px;">

            <img src="https://vo.hinode.com.br/vo-2/assets/images/ajax-loader.gif" style="margin-right: 10px;"/>
            <br>
            <span>Carregando...</span>
        
        </span>

    </div>
</body>
</html>
