try {
    ace.settings.check('main-container', 'fixed');
}
catch (e) {
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
$(document).ready(function () {

    if (adyen_msg != "") {
        bootbox.alert("<h4>" + adyen_msg + "</h4>", function () { });
    }

    if (vid_pedido_oficial != "") {
        $('#carregando-pedido').show();
        carregarPedido(vid_pedido_oficial);
    }
    else {
        bootbox.alert("Pedido não encontrado!", function () { });
    }


    $("#bt_continuar").click(function (event) {
        var valorExcedente = "0";

        var codigoKit = $("#codigokit").val();
        var vid_pedido_oficial = parseInt($("#id_pedido_oficial").val(), 10);
        var vss_pg = $("#ss_pg").val();
        var vid_cons = $("#id_cons").val();
        var vmodo_entrega = $("#modo_entrega").val();
        var vnome_cons = $("#nome_cons").val();
        var vid_cdh_retira = $("#id_cdh_retira").val();
        var vid_cdh_retira_desc = $("#id_cdh_retira_desc").val();
        var vemail_ped = $("#email_ped").val();
        var vcdh_retira_email = $("#cdh_retira_email").val();
        var vcep = $("#cep").val();
        var vendereco = $("#endereco").val();
        var vnumero = $("#numero").val();
        var vcomplemento = $("#complemento").val();
        var vbairro = $("#bairro").val();
        var vcidade = $("#cidade").val();
        var vestado = $("#estado").val();


        var vid_forma_pag = "1";
        var vdesc_forma_pag = "MASTER CARD";

        var vdesc_forma_pag_verf = $("#desc_forma_pag_verf").val();

        var vvl_total_pedido_frete = $("#vl_total_pedido_frete").val();
        var vvl_total_pedido = $("#vl_total_pedido").val();
        var vvl_sub_total_pedido = $("#vl_sub_total_pedido").val();
        var vpontos_total_pedido = $("#pontos_total_pedido").val();
        var vpeso_total_pedido = $("#peso_total_pedido").val();
        var vforma_envio_transp = $("#forma_envio_transp_ped").val();
        var vforma_envio_transp_desc_ped = $("#forma_envio_transp_desc_ped").val();
        var vprazo_transp_ped = $("#prazo_transp_ped").val();
        var vvl_credito_usado = $("#vl_credito_usado").val();

        var vqtd_parc = $("#qtd_parc").val();
        var vped_juros = 0;
        var vqtd_parc_auto = $("#qtd_parc_auto").val();

        if (vqtd_parc_auto == '') {
            vqtd_parc_auto = 0;
        }
        var vvalorcartao = getValorCartao(1);
        var vvbin6 = $("#vbin6-1").val();
        var vcnome = $("#cnome-1").val();
        var vcseg = $("#cseg-1").val();
        var vcmes = $("#cmes-1").val();
        var vcano = $("#cano-1").val();

        var VvalorDesconto = $("#valorDesconto").val();

        var valorTotalPedidoJuros = $("#vl_total_pedido_juros").val();
        var vtel = $("#tel").val();

        if ((parseFloat(valorTotalPedidoJuros) - parseFloat(vvl_total_pedido)) > 0) {
            vped_juros = parseFloat(valorTotalPedidoJuros) - parseFloat(vvl_total_pedido);
        }

        var contaCartao = ""
        var qtdCartao = getQtd();

        if (qtdCartao == 1) {
            contaCartao = "do cartão";
        }
        else {
            contaCartao = "dos " + qtdCartao + " cartões";
        }

        if (vdesc_forma_pag_verf != "1") {
            bootbox.alert("<h4>Forma de pagamento inválida!</h4>", function () { });
        }
        else if (validarParcelas() == false) {
            bootbox.alert("<h4>Favor selecionar uma quantidade de parcelas.</h4>", function () { });
            $("#qtd_parc").focus();
        }
        else if (validarValoresPreenchidos('cartao') == false) {
            bootbox.alert("<h4>O valor " + contaCartao + " deve ser preenchido!</h4>", function () { });
        }
        else if (validarValoresPreenchidos('vista') == false) {
            bootbox.alert("<h4>O valor à vista deve ser preenchido!</h4>", function () { });
        }
        else if (validarValoresMinimos() == false) {
            bootbox.alert("<h4>O valor " + contaCartao + " não pode ser menor que R$ 50,00!</h4>", function () { });
        }
        else if (validarDadosCartoes('num') == false) {
            bootbox.alert("<h4>Favor informar números correto " + contaCartao + ".</h4>", function () { });
        }
        else if (validarDadosCartoes('nome') == false) {
            bootbox.alert("<h4>Favor informar o nome impresso do titular " + contaCartao + ".</h4>", function () { });
        }
        else if (validarDadosCartoes('codigo') == false) {
            bootbox.alert("<h4>Favor informar o código de segurança " + contaCartao + ".</h4>", function () { });
        }
        else if (validarDadosCartoes('mes') == false) {
            bootbox.alert("<h4>Favor selecionar o mês de validade " + contaCartao + ".</h4>", function () { });
        }
        else if (validarDadosCartoes('ano') == false) {
            bootbox.alert("<h4>Favor selecionar o ano de validade " + contaCartao + ".</h4>", function () { });
        }
        else if (validarValorTotal() == false) {
            bootbox.alert("<h4>Valores informados não correspondem ao valor total do pedido!</h4>", function () { });
        }
        else {

            $.ajax({
                url: 'ajax/vo3_ajax_consultor_gera_pedido.asp',
                type: 'POST',
                data: {
                    acao: 'alt_pedido', id_pedido_oficial: vid_pedido_oficial, id_forma_pag: vid_forma_pag, desc_forma_pag: vdesc_forma_pag,
                    ped_juros: vped_juros, qtd_parcela_card: vqtd_parc, ped_tipo: 'CDH>CLIENTE'
                },
                dataType: "text",
                beforeSend: function () {
                    $('#carregando').show(100);
                },
                complete: function () {
                    $('#carregando').hide(100);
                },
                success: function (retorno) {

                    var result = retorno.split('|');

                    if (parseInt(result[0], 10) > 0) {
                        if (qtdCartao > 1) {
                            for (i = 2; i <= qtdCartao; i++) {
                                vvalorcartao += '|' + getValorCartao(i);
                                vvbin6 += '|' + getNumeroCartao(i);
                                vcnome += '|' + getNomeCartao(i);
                                vcseg += '|' + getCodigoCartao(i);
                                vcmes += '|' + getMesCartao(i);
                                vcano += '|' + getAnoCartao(i);
                            }
                        }

                        $("#pedido").val(vid_pedido_oficial);
                        $("#valorcartao").val(vvalorcartao);
                        $("#valorvista").val(getValorVista());
                        $("#ncartao").val(vvbin6);
                        $("#titular").val(vcnome);
                        $("#cvc").val(vcseg);
                        $("#mesval").val(vcmes);
                        $("#anoval").val(vcano);
                        $("#parcelas").val(vqtd_parc);
                        $("#valor").val(parseFloat(vvl_total_pedido));
                        $("#juros").val(parseFloat(vped_juros));
                        $("#valorfinal").val(getValorFinal());
                        $("#qtd-cartoes").val(qtdCartao);

                        $("#fin_pag").submit();
                        return;
                    }
                    else {
                        bootbox.alert("Houve um erro ao atualizar o pedido, revise seus dados", function () { });
                    }

                },
                error: function (retorno) {
                    if (retorno.status == 404) {
                        bootbox.alert("Houve um erro: " + retorno.status + " " + retorno.statusText, function () { });
                    }
                    else {
                        bootbox.alert("Houve um erro desconhecido entre em contato com o administrador !" + retorno, function () { });
                    }

                }

            }); //ajax

        }

    });//$("#bt_continuar").click


    $(".pagamento").click(function () {
        $("#carregando").show();

        $('.reset').hide();
        $("#cartao_vazio").hide('slow');
        $("#txt_vl_parcela").html("");

        $("#carregando").hide();
    });

    $("#qtd-cartao-select").change(function () {
        $("#carregando").show();

        var qtd = getQtd();
        $("#qtd-cartoes").val(qtd);

        for (i = 1; i <= 5; i++) {
            if (i <= qtd) {
                $("#forma_pag_cartao-" + i).show('slow');
                $("#cartao_dados-" + i).show('slow');

                if (qtd == 1) {
                    $("#vvalor-" + i).val(number_format(getValorTotal(), 2, ',', '.'));
                    exibirParcelas();
                }
                else {
                    $("#vvalor-" + i).val("");
                }
            }
            else {
                $("input[name=pagamento-" + i + "]").prop("checked", null);
                $("#forma_pag_cartao-" + i).hide('slow');
                $("#cartao_dados-" + i).hide('slow');
                $("#vvalor-" + i).val("");
                $("#expandir-" + i).text("[esconder]");
            }
        }

        $("#carregando").hide();
    });


    $("#expandir-1").click(function () {
        var status = $(this).text();
        if (status == "[expandir]") {
            $(this).text("[esconder]");
            $("#cartao_dados-1").show('slow');
        }
        else if (status == "[esconder]") {
            $(this).text("[expandir]");
            $("#cartao_dados-1").hide('slow');
        }
    });
    $("#expandir-2").click(function () {
        var status = $(this).text();
        if (status == "[expandir]") {
            $(this).text("[esconder]");
            $("#cartao_dados-2").show('slow');
        }
        else if (status == "[esconder]") {
            $(this).text("[expandir]");
            $("#cartao_dados-2").hide('slow');
        }
    });
    $("#expandir-3").click(function () {
        var status = $(this).text();
        if (status == "[expandir]") {
            $(this).text("[esconder]");
            $("#cartao_dados-3").show('slow');
        }
        else if (status == "[esconder]") {
            $(this).text("[expandir]");
            $("#cartao_dados-3").hide('slow');
        }
    });
    $("#expandir-4").click(function () {
        var status = $(this).text();
        if (status == "[expandir]") {
            $(this).text("[esconder]");
            $("#cartao_dados-4").show('slow');
        }
        else if (status == "[esconder]") {
            $(this).text("[expandir]");
            $("#cartao_dados-4").hide('slow');
        }
    });
    $("#expandir-5").click(function () {
        var status = $(this).text();
        if (status == "[expandir]") {
            $(this).text("[esconder]");
            $("#cartao_dados-5").show('slow');
        }
        else if (status == "[esconder]") {
            $(this).text("[expandir]");
            $("#cartao_dados-5").hide('slow');
        }
    });

    $("#adicionar-pagamento-vista").click(function () {
        $("#forma_pag_vista").show('slow');
        $("#exibir-pagamento-vista").hide('slow');
        $(this).val(1);
        $("#vvalor-vista").val("");
    });

    $("#remover-pagamento-vista").click(function () {
        $("#forma_pag_vista").hide('slow');
        $("#exibir-pagamento-vista").show('slow');
        $("#adicionar-pagamento-vista").val(0);
    });

    $("input[name=vvalor]").blur(function () {
        exibirParcelas();
    });

    $(".vbin6").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            bootbox.alert("<h4>Por favor, digite apenas números no campo \"Cartão\"!</h4>", function () { });
            return false;
        }
    });

    $(".cseg").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            bootbox.alert("<h4>Por favor, digite apenas números no campo \"Código de segurança\"!</h4>", function () { });
            return false;
        }
    });

    $("#vvalor-vista").blur(function () {
        if (this.value != "") {
            $("#carregando-pedido").show();
            obterParcelas();
        }
    });

    $("#vvalor-1").blur(function () {
        validarValorMinimo(this.value, 1);
    });
    $("#vvalor-2").blur(function () {
        validarValorMinimo(this.value, 2);
    });
    $("#vvalor-3").blur(function () {
        validarValorMinimo(this.value, 3);
    });
    $("#vvalor-4").blur(function () {
        validarValorMinimo(this.value, 4);
    });
    $("#vvalor-5").blur(function () {
        validarValorMinimo(this.value, 5);
    });

});//$(document).ready

function carregarPedido(vid_pedido_oficial) {

    //XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    $.ajax({
        url: 'ajax/vo3_ajax_consultor_gera_pedido.asp',
        type: 'POST',
        data: { acao: 'lista_pedido_item', id_pedido_oficial: vid_pedido_oficial },
        dataType: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (retorno) {

            if (retorno.length > 0) {

                $.each(retorno, function (i, item) {

                    if (parseInt(item.PedItem_prod_codigo, 10) == 7250 || parseInt(item.PedItem_prod_codigo, 10) == 7251 || parseInt(item.PedItem_prod_codigo, 10) == 7252 || parseInt(item.PedItem_prod_codigo, 10) == 7253) {

                        $("#codigokit").val(parseInt(item.PedItem_prod_codigo, 10));

                    };

                });

                listarPedido(vid_pedido_oficial);

            }
            else {
                $('#carregando-pedido').hide();
                bootbox.alert("<h4>Item do Pedido Não Encontrado!</h4>", function () {
                    location.href = "fat_GestaoFaturamento1.asp";
                });
            }

        },

        error: function (retorno) {

            if (retorno.status == 404) {
                bootbox.alert("Houve um erro: " + retorno.status + " " + retorno.statusText, function () { });
            }

            else {
                bootbox.alert("Houve um erro desconhecido entre em contato com o administrador...", function () { });
            }

        }

    });

}//Fim da function carregarPedido()


function listarPedido(vid_pedido_oficial) {

    $.ajax({
        url: 'ajax/vo3_ajax_consultor_gera_pedido.asp',
        type: 'POST',
        data: { acao: 'lista_pedido', id_pedido_oficial: vid_pedido_oficial },
        dataType: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (retorno) {

            $.each(retorno, function (i, item) {

                if (item.upgrade != null) {
                    $("#upgrade").val(item.upgrade);
                    $("#upgrade_produto_anterior").val(item.upgrade_produto_anterior);
                }

                $("#vl_total_pedido").val(item.Ped_vl_total);
                $("#vvalor-1").val(number_format(item.Ped_vl_total, 2, ',', '.'));
                $("#vl_total_pedido_bkp").val(item.Ped_vl_total);
                $("#txt_vl_final").html('R$ ' + number_format(item.Ped_vl_total, 2, ',', '.'));
                $("#txt_valor_restante").html("R$ 0,00");
                $("#vl_final").val(item.Ped_vl_total);
                $("#txt_pontos_total_pedido").html('' + number_format(item.Ped_pontos, 2, ',', '.'));
                $("#id_pedido_oficial").val(item.IdPedido_oficial);

                $("#desc_forma_pag_verf").val('1');
                $("input[name=vvalor]").maskMoney({ prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false });
                $("input[name=vvalor-vista]").maskMoney({ prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false });

            });

            $("#forma_pag_vista").show('slow');
            $("#forma_pag_cartao-1").show('slow');
            obterParcelas();
        },
        error: function (retorno) {
            if (retorno.status == 404) {
                bootbox.alert("Houve um erro: " + retorno.status + " " + retorno.statusText, function () { });
            }
            else {
                bootbox.alert("Houve um erro desconhecido entre em contato com o administrador...", function () { });
            }
            $('#carregando-pedido').hide();
        }
    });

}//Fim da function listarPedido()
        

function obterParcelas() {

    var vvl_total_pedido = getValorTotal();
    var vvl_vista = getValorVista();

    vvl_total_pedido = vvl_total_pedido - vvl_vista;

    vvl_total_pedido = parseFloat(vvl_total_pedido);

    var vvl_total_pedido_bkp = vvl_total_pedido;
    var vqtd_parc_auto = $("#qtd_parc_auto").val() == "" ? "0" : parseInt($("#qtd_parc_auto").val(), 10);

    var valorExcedente = "0";
    var codigoKit = $("#codigokit").val();
    var upgrade = $("#upgrade").val();
    var upgrade_produto_anterior = $("#upgrade_produto_anterior").val();

    valorExcedente = parseFloat(valorExcedente);
            
    if (codigoKit == 7250 || codigoKit == 7251 || codigoKit == 7252 || codigoKit == 7253 || upgrade == 1 || upgrade == 2) {
        if (codigoKit == 7252) {
            vqtd_parc_auto = 2;
        }
        else if (codigoKit == 7253) {
            vqtd_parc_auto = 3;
        }
        else {
            vqtd_parc_auto = 1;
        }
    }

    //Juros é o mesmo para todos os cartões.
    vid_forma_pagamento_geral = 1; //Mastercard

    calcularParcelas(vid_forma_pagamento_geral, vvl_total_pedido, vqtd_parc_auto, codigoKit);

} // Fim da function obterParcelas()


function calcularParcelas(vid_forma_pag, vvl_total_pedido, vqtd_parc_auto, codigoKit) {

    $.ajax({
        url: 'ajax/vo3_ajax_consultor_gera_pedido.asp',
        type: 'POST',
        data: { acao: 'obter_parcelas', id_forma_pag: vid_forma_pag, vl_ped: vvl_total_pedido, qtd_parc_auto: vqtd_parc_auto },
        dataType: "json",
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (retorno) {

            if (retorno.length > 0) {

                var vIdFormaPagamento = '';
                var vPagamento = '';
                var vValor_Minimo = '';
                var vparcela = '';
                var vjuros = '';
                var vjuros_mes_parc = '';
                var vfator = '';
                var vMontaCombo = '';
                var vParcelas = '';
                var vParcelasValores = '';
                var intCodigoKit = 0;

                var vValorparcela = 0.00;

                var valorTotalPedido = vvl_total_pedido;
                                                
                $.each(retorno, function (i, item) {

                    if (parseInt(item.parcela, 10) < 4) {

                        vIdFormaPagamento = item.IdFormaPagamento;
                        vPagamento = item.Pagamento;
                        vValor_Minimo = item.Valor_Minimo;
                        vparcela = item.parcela;
                        vjuros = item.juros;

                        vValorparcela = eval(valorTotalPedido / vparcela);

                        vvl_total_pedido = valorTotalPedido;

                        intCodigoKit = parseInt(codigoKit, 10);

                        if ((parseInt(upgrade_produto_anterior, 10) == 7101 || parseInt(upgrade_produto_anterior, 10) == 7102) && upgrade == 1) {
                            intCodigoKit = parseInt(upgrade_produto_anterior, 10)
                        }

                                
                        switch (intCodigoKit) {

                            case 7250:

                                if (parseInt(vparcela) <= 1) {
                                    vParcelas += '<option class="7250" value="' + vparcela + '|0">' + vparcela + ' x ' + number_format((vValorparcela), 2, ',', '.') + '</div>';
                                    vParcelasValores += '<option class="7250" value="' + vparcela + '|0">' + vparcela + ' x ' + number_format((vValorparcela), 2, ',', '.') + '</div>';
                                }

                                break;

                            case 7251:

                                if (parseInt(vparcela) <= 1) {
                                    vParcelas += '<option class="7251" value="' + vparcela + '|0">' + vparcela + ' x ' + number_format((vValorparcela), 2, ',', '.') + '</div>';
                                    vParcelasValores += '<option class="7251" value="' + vparcela + '|0">' + vparcela + ' x ' + number_format((vValorparcela), 2, ',', '.') + '</div>';
                                }

                                break;

                            case 7252:

                                if (parseInt(vparcela) <= 2) {
                                    vParcelas += '<option class="7252" value="' + vparcela + '|0">' + vparcela + ' x ' + number_format((vValorparcela), 2, ',', '.') + '</div>';
                                    vParcelasValores += '<option class="7252" value="' + vparcela + '|0">' + vparcela + ' x ' + number_format((vValorparcela), 2, ',', '.') + '</div>';
                                }

                                break;

                            case 7253:

                                if (parseInt(vparcela) <= 3) {
                                    vParcelas += '<option class="7253" value="' + vparcela + '|0">' + vparcela + ' x ' + number_format((vValorparcela), 2, ',', '.') + '</div>';
                                    vParcelasValores += '<option class="7253" value="' + vparcela + '|0">' + vparcela + ' x ' + number_format((vValorparcela), 2, ',', '.') + '</div>';
                                }

                                break;

                            default:

                                if (parseInt(vjuros, 10) != 0) {
                                    var i;
                                    var valor = parseFloat(vvl_total_pedido);
                                    valor = valor * (Math.pow((1 + (vjuros / 100)), vparcela) * (vjuros / 100)) / (Math.pow((1 + (vjuros / 100)), vparcela) - 1);

                                    vValorparcela = vparcela * valor;
                                    vValorparcela = vValorparcela / vparcela;

                                    vParcelas += '<option class="default" value="' + vparcela + '|' + vjuros + '|' + vValorparcela + '">' + vparcela + 'x</div>';
                                    vParcelasValores += '<option class="default" value="' + vparcela + '|' + vjuros + '|' + vValorparcela + '">' + vparcela + ' x ' + number_format(vValorparcela, 2, ',', '.') + '</div>';
                                }
                                else {
                                    vParcelas += '<option class="default" value="' + vparcela + '|' + vjuros + '|' + vValorparcela + '">' + vparcela + 'x</div>';
                                    vParcelasValores += '<option class="default" value="' + vparcela + '|' + vjuros + '|' + vValorparcela + '">' + vparcela + ' x ' + number_format(vvl_total_pedido, 2, ',', '.') + '</div>';
                                }

                                break;

                        }//switch

                    }//if (parseInt(item.parcela, 10) < 4)

                });//$.each

            }//if (retorno.length > 0)

            vMontaCombo += '<select size="1" name="qtd_parc_sel" id="qtd_parc_sel" class="imput-formata" style=" width: 100px;">';
            vMontaCombo += '    <option selected="selected" value="">Selecione</option>';
            vMontaCombo +=      vParcelas;
            vMontaCombo += '</select>';

            $("#div_parcelas").html(vMontaCombo);

            $("#qtd_parc_sel").change(function () {

                var rs = $("#qtd_parc_sel").val().split('|');

                var vqtd_parc   = rs[0];
                var vjuros = rs[1];
                var vvalor_parc = rs[2];
                $("#qtd_parc").val(vqtd_parc);

                $("#vl_total_pedido_juros").val(0);

                if (vjuros > 0) {
                    var vjuros_mes = vjuros / 100;
                    var vfator_finan = (vjuros_mes / (1 - (1 / (Math.pow(1 + vjuros_mes, vqtd_parc))))).toFixed(5);
                            
                    $("#vl_total_pedido_juros").val(number_format((parseFloat(vqtd_parc) * parseFloat(vvalor_parc)), 2, '.', ''));
                    $("#txt_vl_parcela").html("R$ " + number_format(vvalor_parc, 2, ',', '.'));
                }

                else {
                    $("#txt_vl_parcela").html("R$ " + number_format(vvl_total_pedido / vqtd_parc, 2, ',', '.'));
                }

                var valorFinal = $("#vl_total_pedido_juros").val();
                if (valorFinal == 0) {
                    valorFinal = $("#vl_total_pedido").val();
                }

                $("#vl_final").val(valorFinal);
                $("#txt_vl_final").html("R$ " + number_format(valorFinal, 2, ',', '.'));

                verificarRestante();
                exibirParcelas();

            });

            verificarRestante();
            exibirParcelas();

            $('#carregando-pedido').hide();
        },//success
        error: function (retorno) {

            if (retorno.status == 404) {
                $('#retorno').html('Houve > um erro: ' + retorno.status + ' ' + retorno.statusText);
            }

            else {
                $('#retorno').html('Houve < um erro desconhecido entre em contato com o administrador...');
            }

            $('#carregando-pedido').hide();
        }//error

    });//ajax

}//fim da function calcularParcelas()


function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;

    if (event.keyCode == 8 || event.keyCode == 46
     || event.keyCode == 37 || event.keyCode == 39) {
        return true;
    }

    else if (key < 48 || key > 57) {
        return false;
    }

    else return true;
}; //Fim da function validateNumber(event)

function getNum(val) {

    if (isNaN(val))
        return 0;

    else
        return val;
} //function getNum(val)

function converterValor(valor) {
    return (valor != "") ? parseFloat(valor.replace("R$", "").replace(/\./g, "").replace(",", ".").trim()) : 0.00;
}

function getQtd() {
    return ($("#qtd-cartao-select").val() != "") ? parseInt($("#qtd-cartao-select").val(), 10) : 0;
}

function getValorCartao(cartao) {
    return ($("#vvalor-" + cartao).val() != "") ? converterValor($("#vvalor-" + cartao).val()) : 0.00;
}

function getNumeroCartao(cartao) {
    return ($("#vbin6-" + cartao).val() != "") ? $("#vbin6-" + cartao).val().trim() : 0;
}

function getNomeCartao(cartao) {
    return ($("#cnome-" + cartao).val() != "") ? $("#cnome-" + cartao).val().trim() : 0;
}

function getCodigoCartao(cartao) {
    return ($("#cseg-" + cartao).val() != "") ? $("#cseg-" + cartao).val().trim() : 0;
}

function getMesCartao(cartao) {
    return ($("#cmes-" + cartao).val() != "") ? $("#cmes-" + cartao).val().trim() : 0;
}

function getAnoCartao(cartao) {
    return ($("#cano-" + cartao).val() != "") ? $("#cano-" + cartao).val().trim() : 0;
}

function getValorTotal() {
    return $("#vl_total_pedido").val();
}

function getValorFinal() {
    return $("#vl_final").val();
}

function getParcelas() {
    return ($("#qtd_parc").val() != "") ? parseInt($("#qtd_parc").val()) : 0;
}

function getPagVista() {
    return ($("#adicionar-pagamento-vista")).val();
}

function getValorVista() {
    return (($("#vvalor-vista").val() != "") && (getPagVista()==1)) ? converterValor($("#vvalor-vista").val()) : 0.00;
}

function validarValorMinimo(valor, i) {
    valor = converterValor(valor);
    if (valor < 50) {
        bootbox.alert("<h4>Valor do cartão " + i + " não pode ser menor que R$ 50,00!</h4>", function () { });
        $("#vvalor-" + i).val("");
    }
    else {
        verificarRestante();
    }
}

function validarValorTotal() {

    var valor1 = getValorCartao(1);
    var valor2 = getValorCartao(2);
    var valor3 = getValorCartao(3);
    var valor4 = getValorCartao(4);
    var valor5 = getValorCartao(5);
    var valorVista = getValorVista();

    var valorFinal = parseFloat(getValorFinal().replace(",", "."));
    var valorTotal = ((valor1 + valor2 + valor3 + valor4 + valor5 + valorVista).toFixed(2));

    if (valorTotal == valorFinal) {
        return true;
    }
    else {
        return false;
    }
}

function validarValoresPreenchidos(tipo) {
    var qtd = getQtd();

    if(tipo=="cartao") {
        for (i = 1; i <= qtd; i++) {
            if (qtd > 1) {
                if (getValorCartao(i) == 0) {
                    return false;
                }
            }
        }
        return true;
    }
    else if(tipo == "vista") {
        if (getPagVista() == 1) {
            if (getValorVista() == 0) {
                return false;
            }
        }
        return true;
    }
}

function validarValoresMinimos() {

    var qtd = getQtd();

    if (qtd > 1) {
        for (i = 1; i <= qtd; i++) {
            if (getValorCartao(i) < 50) {
                return false;
            }
        }
    }
    return true;
}

function validarDadosCartoes(campo) {

    var qtd = getQtd();

    if (campo == "num") {
        for (i = 1; i <= qtd; i++) {
            if ((getNumeroCartao(i) == 0) || (getNumeroCartao(i).length != 16) && (getNumeroCartao(i).length != 14)) {
                return false;
            }
        }
    }
    else if (campo == "nome") {
        for (i = 1; i <= qtd; i++) {
            if (getNomeCartao(i) == 0) {
                return false;
            }
        }
    }
    else if (campo == "codigo") {
        for (i = 1; i <= qtd; i++) {
            if (getCodigoCartao(i) == 0) {
                return false;
            }
        }
    }
    else if (campo == "mes") {
        for (i = 1; i <= qtd; i++) {
            if (getMesCartao(i) == 0) {
                return false;
            }
        }
    }
    else if (campo == "ano") {
        for (i = 1; i <= qtd; i++) {
            if (getAnoCartao(i) == 0) {
                return false;
            }
        }
    }            

    return true;
}

function validarParcelas() {
    if (getParcelas() == 0) {
        return false;
    }

    return true;
}

function exibirParcelas() {

    var qtd = getQtd();
    var parcelas = getParcelas();
    var valorCartao = getValorFinal();
    var valorParcela = getValorFinal();
    var textoParcela = "";
    var textoSelecioneParcela = "Selecione uma parcela!"
    var textoDigiteValor = "Digite um valor!"

    if (parcelas!=0) {
        for (j = 1; j <= qtd; j++) {
            valorCartao = getValorCartao(j);
            valorParcela = (valorCartao / parcelas);

            textoParcela = parcelas + " x " + number_format(valorParcela, 2, ',', '.');
            if (valorParcela != 0) {
                $("#div_parcelas-" + j).val(textoParcela);
            }
            else {
                $("#div_parcelas-" + j).val(textoDigiteValor);
            }
        }
    }
    else {
        for (j = 1; j <= qtd; j++) {
            $("#div_parcelas-" + j).val(textoSelecioneParcela);
        }
    }
}

function verificarRestante() {
    var valor1 = getValorCartao(1);
    var valor2 = getValorCartao(2);
    var valor3 = getValorCartao(3);
    var valor4 = getValorCartao(4);
    var valor5 = getValorCartao(5);
    var valorVista = getValorVista();

    var valorFinal = parseFloat(getValorFinal().replace(",", ".")).toFixed(2);
    var valorTotal = ((valor1 + valor2 + valor3 + valor4 + valor5 + valorVista).toFixed(2));

    var valorRestante = valorFinal - valorTotal;

    if(valorRestante >= 0) {
        $("#txt_valor_restante").html("R$ " + number_format(valorRestante, 2, ',', '.'));
    }
}