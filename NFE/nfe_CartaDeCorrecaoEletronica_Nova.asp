<%'@LANGUAGE = VBScript.Encode %>
<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%call Verifica_Permissao(10894)


emitente = Request.Form("emitente")
nota     = Request.Form("nota")
serie    = Request.Form("serie")

strSql = "select valor from fat_nota_parametro where parametro='HP01_xCondUso'"
set adoA = locconnvenda.execute(strSql)

regra = adoA("valor")

if(Request.Form("acao")="gravar") then

 xCorrecao = Replace(Request.Form("xCorrecao"),"'","")

 if(len(xCorrecao)=0) then
    msg = "Aten��o: A corre��o deve ser preenchida!"
 end if

 if(len(xCorrecao)>1000) then
    msg = "Aten��o: A corre��o deve ter no m�ximo 1000 caracteres!!"
 end if

 if(msg="") then

    strSql = " EXEC nfe_CartaDeCorrecao " & _
             "    @emitente='"&emitente&"' " & _
             "   ,@nota='"&nota&"' " & _
             "   ,@serie='"&serie&"' " & _
             "   ,@xCorrecao='"&xCorrecao&"' " & _
             "   ,@usuario='"&session("usuario")&"' "
    'call rw(strSql,1)
    locconnvenda.execute(strSql)
    Response.write("<script>alert('Carta de corre��o registrada. Aguarde a aprova��o pelo SEFAZ para gerar o arquivo XML.'); window.close();</script>")


 else
    Response.write("<script>alert('"&msg&"')</script>")
 end if


end if

%>
<!--#include virtual="public/pub_BodyOculto.asp"-->
<style type="text/css">
    #tbl .regra
    {
        font-style: italic;
        color: Red;
        text-align: justify;
        padding: 3px;
    }
    
      #tbl .regra2
    {
        color: Blue;
        text-align: justify;
        padding: 3px;
    }
    
    #tbl
    {
        font-size: 12px;
        font-family: Verdana;
        border-collapse: collapse;
    }
    
    #tbl .b
    {
        font-weight: bold;
    }
    
    #tbl th
    {
        font-weight: bold;
    }
    
    
    
</style>

<form action="nfe_CartaDeCorrecaoEletronica_Nova.asp" method="post">
<input type="hidden" name="acao" value="gravar" />
<input type="hidden" name="emitente" value="<%=emitente%>" />
<input type="hidden" name="nota" value="<%=nota%>" />
<input type="hidden" name="serie" value="<%=serie%>" />
<table id="tbl" cellpadding="2" cellspacing="2" border="1">
    <tr>
        <td colspan="2" align="left">
            <label class="b">
                Nota:
            </label>
            <label>
                <%=nota%></label>
            <label class="b">
                S�rie:
            </label>
            <label>
                <%=serie%></label>
        </td>
    </tr>
    <tr>
    <th>
    <label>Aten��o:</label>
    </th>
        <td >
            <label class="regra">
                <%=regra%></label>
        </td>
    </tr>
    <tr>
        <th valign="top">
            <label>
                Corre��o:</label>
        </th>
        <td>
            <textarea name="xCorrecao" cols="60" rows="10"></textarea>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <label class="regra2">
                Essa carta de corre��o ir� substituir todas as outras j� feitas e aprovadas para
                essa nota fiscal.
            </label>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <input type="submit" value=" Gravar " />
        </td>
    </tr>
</table>
</form>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->
