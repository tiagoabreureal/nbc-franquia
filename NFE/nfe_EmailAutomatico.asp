<%
'----------------------------------------------------------------------------------------------------------
' ### E-MAIL COM XML ###
'----------------------------------------------------------------------------------------------------------

''Par�metros Gerais
email_assunto		="NF-e"
email_assuntoCanc	="Cancelamento de NF-e"

email_texto			="Notifica��o de Nota Fiscal Eletr�nica emitida para:"
email_textoCanc		="Cancelamento de Nota Fiscal Eletr�nica:"


''======================================================================================
''DISTRIBUIDOR
''======================================================================================
set connDistribuidor=createobject("ADODB.Connection")
connDistribuidor.Open "Provider=SQLOLEDB.1;Persist Security Info=False;Data Source=187.45.208.159;Initial Catalog=DISTRIBUIDOR;User Id=distribuidor;Password=r@T053axWuO"
connDistribuidor.CommandTimeout=600

strsql=	"select b.nome,b.cgc_cpf,a.emitente,a.nota,a.serie,c.xml_Email,d.nome nome_cliente,d.cgc_cpf cgc_cpf_cliente" & _
		",xmlNota=isnull(xmlNota,''),a.chave_autenticacao,a.nProt,a.data_autorizacao " & _
		"FROM fat_Nota_B01 a INNER JOIN fat_DESTINATARIO b ON (a.emitente=b.destinatario) " & _
		"inner join fat_Comercio c on a.destinatario=c.destinatario " & _
		"inner join fat_Destinatario d on a.destinatario=d.destinatario " & _
		"inner join fat_Hierarquia e on a.emitente=e.destinatario " & _
        "where c.xml_Email_Ativo=1 and emissao>='" & dt_fim(date-2) & "' " & _
        " and nota not in(select nota from fat_saida_observacao d where d.emitente=a.emitente and d.serie=a.serie and tipo=10010) " & _
        " and len(chave_autenticacao)>0 and a.situacao=1 and a.ambiente=1 and e.nivel_superior in(10001,10002) and isnull(c.xml_email,'')<>'' " & _
        "order by b.cgc_cpf,a.nota"
'Response.Write strsql
set adors1=connDistribuidor.execute(strsql)
conta=0
do while not adors1.eof
	conta=conta+1			
	''dados do emitente
	emitente	=adors1("emitente")
	nota		=adors1("nota")
	serie		=adors1("serie")
	nome		=adors1("nome")
	cgc_cpf		=adors1("cgc_cpf")
	cgc_cpf2	=mid(cgc_cpf,1,2) & "." & mid(cgc_cpf,3,3) & "." & mid(cgc_cpf,6,3) & "/" & mid(cgc_cpf,9,4) & "-" & mid(cgc_cpf,13,2)
	if len(trim(cgc_cpf2))=0 then cgc_cpf2=cgc_cpf	
	xml					=adors1("xmlNota")
	nProt				=adors1("nProt")
	data_autorizacao	=adors1("data_autorizacao")
	
	''dados do destinat�rio
	cgc_cpf_cliente	=adors1("cgc_cpf_cliente")
	if len(cgc_cpf_cliente)=14 then
		cgc_cpf_cliente2="CNPJ: " & mid(cgc_cpf_cliente,1,2) & "." & mid(cgc_cpf_cliente,3,3) & "." & mid(cgc_cpf_cliente,6,3) & "/" & mid(cgc_cpf_cliente,9,4) & "-" & mid(cgc_cpf_cliente,13,2)
	else
		cgc_cpf_cliente2="CPF: " & cgc_cpf_cliente
	end if	
	if len(trim(cgc_cpf_cliente2))=0 then cgc_cpf_cliente2=cgc_cpf_cliente
		
	nome_cliente	=adors1("nome_cliente")
	email			=adors1("xml_Email")
		
	chave_autenticacao	=adors1("chave_autenticacao")
	chave_autenticacao2 =mid(chave_autenticacao,1,4) & " " & mid(chave_autenticacao,5,4) & " " & mid(chave_autenticacao,9,4) & " " & _
						 mid(chave_autenticacao,13,4) & " " & mid(chave_autenticacao,17,4) & " " & mid(chave_autenticacao,21,4) & " " & _
						 mid(chave_autenticacao,25,4) & " " & mid(chave_autenticacao,29,4) & " " & mid(chave_autenticacao,33,4) & " " & _
						 mid(chave_autenticacao,37,4) & " " & mid(chave_autenticacao,41,4)																						

	assunto	=email_assunto & ": " & nota & "-" & serie 
	texto	=email_texto & _
			"<br>" & nome_cliente & _
			"<br>" & cgc_cpf_cliente2 & _
			"<br><br>Nota Fiscal: " & nota & " - " & serie & _
			"<br>Chave de Acesso: " & chave_autenticacao2 & _
			"<br>Protocolo de Autoriza��o: " & nProt & _
			"<br>Data de Autoriza��o: " & data_autorizacao & _
			"<br><br>Visualizar Danfe: <a href='http://www.xc1.com.br/erp/danfe/danfe_Consulta.asp?Chave=" & chave_autenticacao & "'>[clique aqui]</a>" & _
			"<br><br>Atenciosamente," & _
			"<br>" & nome & _
			"<br>CNPJ: " & cgc_cpf2 & _
			"<br><br><br><i>## E-mail enviado automaticamente, favor n�o responder ##</i>" & _
			"<br><br>--------------------------------------------------------------" & _						
			"        <br><b><font face='verdana' size='1'>XC<font color=red>1</font><font color='black'> - Nota Fiscal Eletr�nica</b>" & _			
			"        <br>Acesse <a href='http://www.xc1.com.br'>www.xc1.com.br</a> ou entre em contato para maiores informa��es." & _
			"		 <br>Fone: (11) 5584-4880</font>" & _			
			"<br>--------------------------------------------------------------"
		
	
	posicao1	=instr(1,xml,"<NFe")
	posicao2	=instr(1,xml,"</NFe>")				
	if posicao1=0 then posicao1=1
	if posicao2=0 then posicao2=len(xml)	
	xml = mid(xml,posicao1,(posicao2-posicao1+6))    
    xml	= EncodeUTF8(xml)
	
	arq			= chave_autenticacao & ".xml" 'cgc_cpf & "_" & emitente & "_" & nota & ".xml"
	arquivoXml	= "xml\" & arq
	set objArquivo = createobject("scripting.filesystemobject")	
	Set arquivo = objArquivo.CreateTextFile(server.mappath(arquivoXml), true) 
    arquivo.writeline xml
    set objArquivo=nothing
    
    arquivo_ftp	="http://xc1.tempsite.ws/erp/nfe/xml/" & arq
   'Response.Write arquivo_ftp
    'Response.end
	
	call Email_Enviar(email,assunto,texto,"","","","","TRUE","TRUE",de,"","","","1",arquivo_ftp)
	
	strsql="insert into sys_email_log (email,email_assunto,texto,data,usuario,destinatario,status) values " & _
		"('" & email & "','" & assunto & "','" & replace(texto,"'","") & "',getdate(),'admin','" & destinatario & "','1')"
	connDistribuidor.execute(strsql)
	
	strsql= "insert into fat_saida_observacao (emitente,nota,serie,linha,tipo,complemento,concessorio) values " & _
			"('" & emitente & "'," & nota & ",'" & serie & "',1,10010,'" & email & "',0)"
	connDistribuidor.execute(strsql)			
	
	adors1.movenext
loop 
set adors1=nothing
set connDistribuidor=nothing

'Response.Write "<br>Enviado Distribuidor - " & now()



''======================================================================================
''KENKO
''======================================================================================
set connKenko=createobject("ADODB.Connection")
connKenko.Open "Provider=SQLOLEDB.1;Persist Security Info=False;Data Source=sqlserver01.xcompany.com.br;Initial Catalog=KENKO;User Id=kenko;Password=qR@t0xl4dAg"
connKenko.CommandTimeout=600

strsql=	"select b.nome,b.cgc_cpf,a.emitente,a.nota,a.serie,c.xml_Email,d.nome nome_cliente,d.cgc_cpf cgc_cpf_cliente" & _
		",xmlNota=isnull(xmlNota,''),a.chave_autenticacao,a.nProt,a.data_autorizacao " & _
		"FROM fat_Nota_B01 a INNER JOIN fat_DESTINATARIO b ON (a.emitente=b.destinatario) " & _
		"inner join fat_Comercio c on a.destinatario=c.destinatario " & _
		"inner join fat_Destinatario d on a.destinatario=d.destinatario " & _
        "where c.xml_Email_Ativo=1 and emissao>='" & dt_fim(date) & "' " & _
        " and nota not in(select nota from fat_saida_observacao d where d.emitente=a.emitente and d.serie=a.serie and tipo=10010) " & _
        " and len(chave_autenticacao)>0 and a.situacao=1 and isnull(c.xml_email,'')<>'' " & _
        "order by b.cgc_cpf,a.nota"
'Response.Write strsql
set adors1=connKenko.execute(strsql)
conta=0
do while not adors1.eof
	conta=conta+1			
	''dados do emitente
	emitente	=adors1("emitente")
	nota		=adors1("nota")
	serie		=adors1("serie")
	nome		=adors1("nome")
	cgc_cpf		=adors1("cgc_cpf")
	cgc_cpf2	=mid(cgc_cpf,1,2) & "." & mid(cgc_cpf,3,3) & "." & mid(cgc_cpf,6,3) & "/" & mid(cgc_cpf,9,4) & "-" & mid(cgc_cpf,13,2)
	if len(trim(cgc_cpf2))=0 then cgc_cpf2=cgc_cpf
	xml					=adors1("xmlNota")
	nProt				=adors1("nProt")
	data_autorizacao	=adors1("data_autorizacao")
	
	''dados do destinat�rio
	cgc_cpf_cliente	=adors1("cgc_cpf_cliente")
	if len(cgc_cpf_cliente)=14 then
		cgc_cpf_cliente2="CNPJ: " & mid(cgc_cpf_cliente,1,2) & "." & mid(cgc_cpf_cliente,3,3) & "." & mid(cgc_cpf_cliente,6,3) & "/" & mid(cgc_cpf_cliente,9,4) & "-" & mid(cgc_cpf_cliente,13,2)
	else
		cgc_cpf_cliente2="CPF: " & cgc_cpf_cliente
	end if	
	if len(trim(cgc_cpf_cliente2))=0 then cgc_cpf_cliente2=cgc_cpf_cliente
	
	nome_cliente	=adors1("nome_cliente")
	email			=adors1("xml_Email")	
		
	chave_autenticacao	=adors1("chave_autenticacao")
	chave_autenticacao2 =mid(chave_autenticacao,1,4) & " " & mid(chave_autenticacao,5,4) & " " & mid(chave_autenticacao,9,4) & " " & _
						 mid(chave_autenticacao,13,4) & " " & mid(chave_autenticacao,17,4) & " " & mid(chave_autenticacao,21,4) & " " & _
						 mid(chave_autenticacao,25,4) & " " & mid(chave_autenticacao,29,4) & " " & mid(chave_autenticacao,33,4) & " " & _
						 mid(chave_autenticacao,37,4) & " " & mid(chave_autenticacao,41,4)																						

	assunto	=email_assunto & ": " & nota & "-" & serie 
	texto	=email_texto & _
			"<br>" & nome_cliente & _
			"<br>" & cgc_cpf_cliente2 & _
			"<br><br>Nota Fiscal: " & nota & " - " & serie & _
			"<br>Chave de Acesso: " & chave_autenticacao2 & _
			"<br>Protocolo de Autoriza��o: " & nProt & _
			"<br>Data de Autoriza��o: " & data_autorizacao & _
			"<br><br>Atenciosamente," & _
			"<br>" & nome & _
			"<br>CNPJ: " & cgc_cpf2 & _
			"<br><br><br><i>## E-mail enviado automaticamente, favor n�o responder ##</i>" & _
			"<br><br>--------------------------------------------------------------" & _						
			"        <br><b><font face='verdana' size='1'>XC<font color=red>1</font><font color='black'> - Nota Fiscal Eletr�nica</b>" & _			
			"        <br>Acesse <a href='http://www.xc1.com.br'>www.xc1.com.br</a> ou entre em contato para maiores informa��es." & _
			"		 <br>Fone: (11) 5584-4880</font>" & _			
			"<br>--------------------------------------------------------------"
		
	
	posicao1	=instr(1,xml,"<NFe")
	posicao2	=instr(1,xml,"</NFe>")				
	if posicao1=0 then posicao1=1
	if posicao2=0 then posicao2=len(xml)	
	xml = mid(xml,posicao1,(posicao2-posicao1+6))    
    xml	= EncodeUTF8(xml)
	
	arq			= chave_autenticacao & ".xml" 'cgc_cpf & "_" & emitente & "_" & nota & ".xml"
	arquivoXml	= "xml\" & arq
	set objArquivo = createobject("scripting.filesystemobject")	
	Set arquivo = objArquivo.CreateTextFile(server.mappath(arquivoXml), true) 
    arquivo.writeline xml
    set objArquivo=nothing
    
    arquivo_ftp	="http://xc1.tempsite.ws/erp/nfe/xml/" & arq
   'Response.Write arquivo_ftp
    'Response.end
	
	call Email_EnviarKenko(email,assunto,texto,"","","","","TRUE","TRUE",de,"","","","1",arquivo_ftp)
	
	strsql="insert into sys_email_log (email,email_assunto,texto,data,usuario,destinatario,status) values " & _
		"('" & email & "','" & assunto & "','" & replace(texto,"'","") & "',getdate(),'admin','" & destinatario & "','1')"
	connKenko.execute(strsql)
	
	strsql= "insert into fat_saida_observacao (emitente,nota,serie,linha,tipo,complemento,concessorio) values " & _
			"('" & emitente & "'," & nota & ",'" & serie & "',1,10010,'" & email & "',0)"
	connKenko.execute(strsql)			
	
	adors1.movenext
loop 
set adors1=nothing
set connKenko=nothing



''======================================================================================
''MINUANO
''alterado em 11/10/11 (Kleber).
''======================================================================================
set connMinuano=createobject("ADODB.Connection")
connMinuano.Open "Provider=SQLOLEDB.1;Persist Security Info=False;Data Source=189.19.243.110,5900;Initial Catalog=icorp;User Id=erp5;Password=erp"
connMinuano.CommandTimeout=600

strsql=	"select b.nome,b.cgc_cpf,a.emitente,a.nota,a.serie,c.xml_Email,d.nome nome_cliente,d.cgc_cpf cgc_cpf_cliente" & _
		",xmlNota=isnull(xmlNota,''),a.chave_autenticacao,a.nProt,a.data_autorizacao " & _
		"FROM fat_Nota_B01 a INNER JOIN fat_DESTINATARIO b ON (a.emitente=b.destinatario) " & _
		"inner join fat_Comercio c on a.destinatario=c.destinatario " & _
		"inner join fat_Destinatario d on a.destinatario=d.destinatario " & _
        "where c.xml_Email_Ativo=1 and emissao>='" & dt_fim(date) & "' " & _
        " and nota not in(select nota from fat_saida_observacao d where d.emitente=a.emitente and d.serie=a.serie and tipo=10010) " & _
        " and len(chave_autenticacao)>0 and a.situacao=1 and isnull(c.xml_email,'')<>'' " & _
        "order by b.cgc_cpf,a.nota"
'Response.Write strsql
set adors1=connMinuano.execute(strsql)
conta=0
do while not adors1.eof
	conta=conta+1			
	''dados do emitente
	emitente	=adors1("emitente")
	nota		=adors1("nota")
	serie		=adors1("serie")
	nome		=adors1("nome")
	cgc_cpf		=adors1("cgc_cpf")
	cgc_cpf2	=mid(cgc_cpf,1,2) & "." & mid(cgc_cpf,3,3) & "." & mid(cgc_cpf,6,3) & "/" & mid(cgc_cpf,9,4) & "-" & mid(cgc_cpf,13,2)
	if len(trim(cgc_cpf2))=0 then cgc_cpf2=cgc_cpf
	xml					=adors1("xmlNota")
	nProt				=adors1("nProt")
	data_autorizacao	=adors1("data_autorizacao")
	
	''dados do destinat�rio
	cgc_cpf_cliente	=adors1("cgc_cpf_cliente")
	if len(cgc_cpf_cliente)=14 then
		cgc_cpf_cliente2="CNPJ: " & mid(cgc_cpf_cliente,1,2) & "." & mid(cgc_cpf_cliente,3,3) & "." & mid(cgc_cpf_cliente,6,3) & "/" & mid(cgc_cpf_cliente,9,4) & "-" & mid(cgc_cpf_cliente,13,2)
	else
		cgc_cpf_cliente2="CPF: " & cgc_cpf_cliente
	end if	
	if len(trim(cgc_cpf_cliente2))=0 then cgc_cpf_cliente2=cgc_cpf_cliente
	
	nome_cliente	=adors1("nome_cliente")
	email			=adors1("xml_Email")	
		
	chave_autenticacao	=adors1("chave_autenticacao")
	chave_autenticacao2 =mid(chave_autenticacao,1,4) & " " & mid(chave_autenticacao,5,4) & " " & mid(chave_autenticacao,9,4) & " " & _
						 mid(chave_autenticacao,13,4) & " " & mid(chave_autenticacao,17,4) & " " & mid(chave_autenticacao,21,4) & " " & _
						 mid(chave_autenticacao,25,4) & " " & mid(chave_autenticacao,29,4) & " " & mid(chave_autenticacao,33,4) & " " & _
						 mid(chave_autenticacao,37,4) & " " & mid(chave_autenticacao,41,4)																						

	assunto	=email_assunto & ": " & nota & "-" & serie 
	texto	=email_texto & _
			"<br>" & nome_cliente & _
			"<br>" & cgc_cpf_cliente2 & _
			"<br><br>Nota Fiscal: " & nota & " - " & serie & _
			"<br>Chave de Acesso: " & chave_autenticacao2 & _
			"<br>Protocolo de Autoriza��o: " & nProt & _
			"<br>Data de Autoriza��o: " & data_autorizacao & _
			"<br><br>Atenciosamente," & _
			"<br>" & nome & _
			"<br>CNPJ: " & cgc_cpf2 & _
			"<br><br><br><i>## E-mail enviado automaticamente, favor n�o responder ##</i>" & _
			"<br><br>--------------------------------------------------------------" & _						
			"        <br><b><font face='verdana' size='1'>XC<font color=red>1</font><font color='black'> - Nota Fiscal Eletr�nica</b>" & _			
			"        <br>Acesse <a href='http://www.xc1.com.br'>www.xc1.com.br</a> ou entre em contato para maiores informa��es." & _
			"		 <br>Fone: (11) 5584-4880</font>" & _			
			"<br>--------------------------------------------------------------"
		
	
	posicao1	=instr(1,xml,"<NFe")
	posicao2	=instr(1,xml,"</NFe>")				
	if posicao1=0 then posicao1=1
	if posicao2=0 then posicao2=len(xml)	
	xml = mid(xml,posicao1,(posicao2-posicao1+6))    
    xml	= EncodeUTF8(xml)
	
	arq			= chave_autenticacao & ".xml" 'cgc_cpf & "_" & emitente & "_" & nota & ".xml"
	arquivoXml	= "xml\" & arq
	set objArquivo = createobject("scripting.filesystemobject")	
	Set arquivo = objArquivo.CreateTextFile(server.mappath(arquivoXml), true) 
    arquivo.writeline xml
    set objArquivo=nothing
    
    arquivo_ftp	="http://xc1.tempsite.ws/erp/nfe/xml/" & arq
   'Response.Write arquivo_ftp
    'Response.end

    'email="kleber@xcompany.com.br"
	
	call Email_EnviarMinuano(email,assunto,texto,"","","","","TRUE","TRUE",de,"","","","1",arquivo_ftp)
	
	strsql="insert into sys_email_log (email,email_assunto,texto,data,usuario,destinatario,status) values " & _
		"('" & email & "','" & assunto & "','" & replace(texto,"'","") & "',getdate(),'admin','" & destinatario & "','1')"
	connMinuano.execute(strsql)
	
	strsql= "insert into fat_saida_observacao (emitente,nota,serie,linha,tipo,complemento,concessorio) values " & _
			"('" & emitente & "'," & nota & ",'" & serie & "',1,10010,'" & email & "',0)"
	connMinuano.execute(strsql)			
	
	adors1.movenext
loop 
set adors1=nothing
set connMinuano=nothing



''======================================================================================
''ONLINE
''alterado em 06/12/11 (Kleber).
''liberado em produ��o em 09/12/11 - 08:40 (Kleber, conforme aprova��o do Jefferson)
''======================================================================================

email_texto_online	="<center><font size=5><b>Sistema de Envio do XML (NFe)</b></font>" & _
		"<br><font size=3>IBC - INSTITUTO BRASILEIRO DE CULTURA LTDA</font></center>" & _
		"<br><br><br>Segue dados da NFe em anexo:"

set connOnline=createobject("ADODB.Connection")
connOnline.Open "Provider=SQLOLEDB.1;Persist Security Info=False;Data Source=189.8.82.82,5900;Initial Catalog=icorp;User Id=sa;Password=@!erp2009"
connOnline.CommandTimeout=600

'' Notas Fiscais Aprovadas
'''''''''''''''''''''''''''''''''''''
strsql=	"select b.nome,b.cgc_cpf,a.emitente,a.nota,a.serie,c.xml_Email,d.nome nome_cliente,d.cgc_cpf cgc_cpf_cliente" & _
		",xmlNota=isnull(xmlNota,''),a.chave_autenticacao,a.nProt,a.data_autorizacao,a.emissao,total_geral=isnull(e.total_geral,0) " & _
		"FROM fat_Nota_B01 a INNER JOIN fat_DESTINATARIO b ON (a.emitente=b.destinatario) " & _
		"inner join fat_Comercio c on a.destinatario=c.destinatario " & _
		"inner join fat_Destinatario d on a.destinatario=d.destinatario " & _
		"inner join fat_nota_w02 e on a.emitente=e.emitente and a.nota=e.nota and a.serie=e.serie " & _
        "where c.xml_Email_Ativo=1 and emissao>='" & dt_fim(date) & "' " & _
        " and a.nota not in(select nota from fat_saida_observacao d where d.emitente=a.emitente and d.serie=a.serie and tipo=10010) " & _
        " and len(chave_autenticacao)>0 and a.situacao=1 and isnull(c.xml_email,'')<>'' " & _
        "order by b.cgc_cpf,a.nota"
'Response.Write strsql
set adors1=connOnline.execute(strsql)
conta=0
do while not adors1.eof
	conta=conta+1			
	''dados do emitente
	emitente	=adors1("emitente")
	nota		=adors1("nota")
	serie		=adors1("serie")
	nome		=adors1("nome")
	cgc_cpf		=adors1("cgc_cpf")
	cgc_cpf2	=mid(cgc_cpf,1,2) & "." & mid(cgc_cpf,3,3) & "." & mid(cgc_cpf,6,3) & "/" & mid(cgc_cpf,9,4) & "-" & mid(cgc_cpf,13,2)
	if len(trim(cgc_cpf2))=0 then cgc_cpf2=cgc_cpf
	xml					=adors1("xmlNota")
	nProt				=adors1("nProt")
	data_autorizacao	=adors1("data_autorizacao")
	emissao				=adors1("emissao")
	total_geral			=adors1("total_geral")
	
	''dados do destinat�rio
	cgc_cpf_cliente	=adors1("cgc_cpf_cliente")
	if len(cgc_cpf_cliente)=14 then
		cgc_cpf_cliente2=mid(cgc_cpf_cliente,1,2) & "." & mid(cgc_cpf_cliente,3,3) & "." & mid(cgc_cpf_cliente,6,3) & "/" & mid(cgc_cpf_cliente,9,4) & "-" & mid(cgc_cpf_cliente,13,2)
	else
		cgc_cpf_cliente2=cgc_cpf_cliente
	end if	
	if len(trim(cgc_cpf_cliente2))=0 then cgc_cpf_cliente2=cgc_cpf_cliente
	
	nome_cliente	=adors1("nome_cliente")
	email			=adors1("xml_Email")	
		
	chave_autenticacao	=adors1("chave_autenticacao")
	chave_autenticacao2 =mid(chave_autenticacao,1,4) & " " & mid(chave_autenticacao,5,4) & " " & mid(chave_autenticacao,9,4) & " " & _
						 mid(chave_autenticacao,13,4) & " " & mid(chave_autenticacao,17,4) & " " & mid(chave_autenticacao,21,4) & " " & _
						 mid(chave_autenticacao,25,4) & " " & mid(chave_autenticacao,29,4) & " " & mid(chave_autenticacao,33,4) & " " & _
						 mid(chave_autenticacao,37,4) & " " & mid(chave_autenticacao,41,4)																						

	assunto	=email_assunto & ": " & nota & "-" & serie 
	
	
	'' texto n�o usado a partir de 06/03/12
	texto_antigo	=email_texto & _
				"<br>" & nome_cliente & _
				"<br>" & cgc_cpf_cliente2 & _
				"<br><br>Nota Fiscal: " & nota & " - " & serie & _
				"<br>Chave de Acesso: " & chave_autenticacao2 & _
				"<br>Protocolo de Autoriza��o: " & nProt & _
				"<br>Data de Autoriza��o: " & data_autorizacao & _
				"<br><br>Atenciosamente," & _
				"<br>" & nome & _
				"<br>CNPJ: " & cgc_cpf2 & _
				"<br><br><br><i>## E-mail enviado automaticamente, favor n�o responder ##</i>"		
	
	'' alterado para esse novo texto, conforme solicita��o Jefferson/aquino.
	''' alterado em 06/03/12 (Kleber).
	texto	=email_texto_online & _
			"<table border=1 width='100%' bgcolor='#ffffcc'> " & _
			"<tr><td width='40%'>N�mero do Documento:</td><td width='60%'>" & formatnumber(nota,0) & "</td></tr>" & _
			"<tr><td>S�rie:</td><td>" & serie & "</td></tr>" & _
			"<tr><td>Raz�o Social:</td><td>" & nome_cliente & "</td></tr>" & _
			"<tr><td>CNPJ/CPF:</td><td>" & cgc_cpf_cliente2 & "</td></tr>" & _
			"<tr><td>Data da Emiss�o:</td><td>" & emissao & "</td></tr>" & _
			"<tr><td>Valor Total:</td><td>R$ " & formatnumber(total_geral,2) & "</td></tr>" & _
			"<tr><td>Chave:</td><td><a href='https://nfe.fazenda.sp.gov.br/ConsultaNFe'>" & chave_autenticacao & "</a></td></tr>" & _			
			"</table>" & _
			"<br><br>" & _			
			"Atenciosamente," & _
			"<br>Departamento Fiscal" & _
			"<br><a href='http://www.revistaonline.com.br'>http://www.revistaonline.com.br</a>" & _
			"<br><b>Online Editora!</b>" & _
			"<br><br><center>MENSAGEM GERADA AUTOMATICAMENTE. N�O RESPONDER.</center>"						


	'"Visualizar Danfe: <a href='http://www.xc1.com.br/erp/danfe/danfe_Consulta.asp?Chave=" & chave_autenticacao & "'>[clique aqui]</a>" & _
	'"<br><br>" & _
	
	posicao1	=instr(1,xml,"<NFe")
	posicao2	=instr(1,xml,"</NFe>")				
	if posicao1=0 then posicao1=1
	if posicao2=0 then posicao2=len(xml)	
	xml = mid(xml,posicao1,(posicao2-posicao1+6))    
    xml	= EncodeUTF8(xml)
	
	arq			= chave_autenticacao & ".xml"	
	arquivoXml	= "xml\" & arq
	set objArquivo = createobject("scripting.filesystemobject")	
	Set arquivo = objArquivo.CreateTextFile(server.mappath(arquivoXml), true) 
    arquivo.writeline xml
    set objArquivo=nothing
    
    arquivo_ftp	="http://xc1.tempsite.ws/erp/nfe/xml/" & arq    

    'email="jeffersonsouza@editoraonline.com.br"    
	
	call Email_EnviarOnline(email,assunto,texto,"","","","","TRUE","TRUE",de,"","","","1",arquivo_ftp)
	
	strsql= "insert into fat_saida_observacao (emitente,nota,serie,linha,tipo,complemento,concessorio) values " & _
			"('" & emitente & "'," & nota & ",'" & serie & "',1,10010,'" & email & "',0)"
	connOnline.execute(strsql)	
	
	strsql="insert into sys_email_log (email,email_assunto,texto,data,usuario,destinatario,status) values " & _
		"('" & email & "','" & assunto & "','" & replace(texto,"'","") & "',getdate(),'admin','" & destinatario & "','1')"
	connOnline.execute(strsql)
	
			
	
	adors1.movenext
loop 
set adors1=nothing


'' Notas Fiscais Canceladas
'''''''''''''''''''''''''''''''''''''
strsql=	"select b.nome,b.cgc_cpf,a.emitente,a.nota,a.serie,c.xml_Email,d.nome nome_cliente,d.cgc_cpf cgc_cpf_cliente" & _
		",xmlNotaCanc=isnull(x.xmlcancelamento,''),a.chave_autenticacao " & _
		"FROM fat_nota_cc01 x inner join fat_Nota_B01 a on x.emitente=a.emitente and x.nota=a.nota and x.serie=a.serie " & _
		"INNER JOIN fat_DESTINATARIO b ON (a.emitente=b.destinatario) " & _
		"inner join fat_Comercio c on a.destinatario=c.destinatario " & _
		"inner join fat_Destinatario d on a.destinatario=d.destinatario " & _
        "where c.xml_Email_Ativo=1 and convert(datetime,convert(varchar(10),x.data,120))>='" & dt_fim(date) & "' " & _
        " and x.nota not in(select nota from fat_saida_observacao d where d.emitente=a.emitente and d.serie=a.serie and tipo=10030) " & _
        " and len(a.chave_autenticacao)>0 and x.situacao=1 " & _
        "order by b.cgc_cpf,a.nota"
'call rw(strsql,1)
set adors1=connOnline.execute(strsql)
conta=0
do while not adors1.eof
	conta=conta+1			
	''dados do emitente
	emitente	=adors1("emitente")
	nota		=adors1("nota")
	serie		=adors1("serie")
	nome		=adors1("nome")
	cgc_cpf		=adors1("cgc_cpf")
	cgc_cpf2	=mid(cgc_cpf,1,2) & "." & mid(cgc_cpf,3,3) & "." & mid(cgc_cpf,6,3) & "/" & mid(cgc_cpf,9,4) & "-" & mid(cgc_cpf,13,2)
	if len(trim(cgc_cpf2))=0 then cgc_cpf2=cgc_cpf
	xml					=adors1("xmlNotaCanc")	
	
	''dados do destinat�rio
	cgc_cpf_cliente	=adors1("cgc_cpf_cliente")
	if len(cgc_cpf_cliente)=14 then
		cgc_cpf_cliente2="CNPJ: " & mid(cgc_cpf_cliente,1,2) & "." & mid(cgc_cpf_cliente,3,3) & "." & mid(cgc_cpf_cliente,6,3) & "/" & mid(cgc_cpf_cliente,9,4) & "-" & mid(cgc_cpf_cliente,13,2)
	else
		cgc_cpf_cliente2="CPF: " & cgc_cpf_cliente
	end if	
	if len(trim(cgc_cpf_cliente2))=0 then cgc_cpf_cliente2=cgc_cpf_cliente
	
	nome_cliente	=adors1("nome_cliente")
	email			=adors1("xml_Email")	
		
	chave_autenticacao	=adors1("chave_autenticacao")
	chave_autenticacao2 =mid(chave_autenticacao,1,4) & " " & mid(chave_autenticacao,5,4) & " " & mid(chave_autenticacao,9,4) & " " & _
						 mid(chave_autenticacao,13,4) & " " & mid(chave_autenticacao,17,4) & " " & mid(chave_autenticacao,21,4) & " " & _
						 mid(chave_autenticacao,25,4) & " " & mid(chave_autenticacao,29,4) & " " & mid(chave_autenticacao,33,4) & " " & _
						 mid(chave_autenticacao,37,4) & " " & mid(chave_autenticacao,41,4)																						

	assunto	=email_assuntoCanc & ": " & nota & "-" & serie 
	texto	=email_textoCanc & _
			"<br>" & nome_cliente & _
			"<br>" & cgc_cpf_cliente2 & _
			"<br><br>Nota Fiscal: " & nota & " - " & serie & _
			"<br>Chave de Acesso: " & chave_autenticacao2 & _
			
			"<br><br>Atenciosamente," & _
			"<br>" & nome & _
			"<br>CNPJ: " & cgc_cpf2 & _
			"<br><br><br><i>## E-mail enviado automaticamente, favor n�o responder ##</i>"		
	
	posicao1	=instr(1,xml,"<NFe")
	posicao2	=instr(1,xml,"</NFe>")				
	if posicao1=0 then posicao1=1
	if posicao2=0 then posicao2=len(xml)	
	xml = mid(xml,posicao1,(posicao2-posicao1+6))    
    xml	= EncodeUTF8(xml)
	
	arq			= chave_autenticacao & ".xml"	
	arquivoXml	= "xml\" & arq
	set objArquivo = createobject("scripting.filesystemobject")	
	Set arquivo = objArquivo.CreateTextFile(server.mappath(arquivoXml), true) 
    arquivo.writeline xml
    set objArquivo=nothing
    
    arquivo_ftp	="http://xc1.tempsite.ws/erp/nfe/xml/" & arq    

	'' for�ar envio do e-mail para o suporte@
	'' alterado em 08/02/12 18:45 (Kleber), solicitado por aquino.
	'' *** AP�S OK, COMENTAR A LINHA ABAIXO PARA O E-MAIL SEGUIR PARA O PR�PRIO CLIENTE.
    email="suporte@xcompany.com.br;jeffersonsouza@editoraonline.com.br;faturamento01@editoraonline.com.br"
	
	call Email_EnviarOnline(email,assunto,texto,"","","","","TRUE","TRUE",de,"","","","1",arquivo_ftp)
	
	strsql="insert into sys_email_log (email,email_assunto,texto,data,usuario,destinatario,status) values " & _
		"('" & email & "','" & assunto & "','" & replace(texto,"'","") & "',getdate(),'admin','" & destinatario & "','1')"
	connOnline.execute(strsql)
	
	strsql= "insert into fat_saida_observacao (emitente,nota,serie,linha,tipo,complemento,concessorio) values " & _
			"('" & emitente & "'," & nota & ",'" & serie & "',1,10030,'" & email & "',0)"
	connOnline.execute(strsql)			
	
	adors1.movenext
loop 
set adors1=nothing

set connOnline=nothing
	
	
	
	
''======================================================================================	

function Email_Enviar(id_addAddress, id_Subject, id_Body, id_Destinatario, id_Server, id_User, id_Password, id_Queue, id_Html, id_From, id_FromName, id_Addcc, id_AddBcc, id_Opcao_Envio, anexo)
	dim servidor
	dim de
	'servidor="187.45.208.159"
	'de	    ="webmaster@xcompany.com.br"
	
	servidor="smtp.xcompany.com.br"	
	de	    ="nfexc1@xcompany.com.br"
	user	="nfexc1@xcompany.com.br"
	pass	="ano2011@@"
	id_server=servidor
	id_from=de

	Set Mail = Server.CreateObject("CDO.Message") 
	Set MailCon = Server.CreateObject ("CDO.Configuration") 
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = id_Server
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 587 
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 
	if len(user)>0 then
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = user
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = pass
	end if
	MailCon.Fields.update 
	Set Mail.Configuration = MailCon 
	Mail.From = id_From
		
	lista_email=split(id_AddAddress,";")	
	copia=""
	for i=0 to ubound(lista_email)
		if cdbl(i)=cdbl(0) then
			Mail.To = lista_email(i)
		else
			if i>1 then copia=copia & ";"
			copia=copia & lista_email(i)			
		end if
	next	
	if len(copia)>0 then mail.CC	= copia
	
	Mail.Subject = id_Subject	
	
	if len(trim(anexo))>0 then mail.AddAttachment anexo,"",""			
	id_Body	="<html><head><title></title></head><body>" & _
	 "	<table border='0' width='100%' cellppading='0' cellspacing='0'>" & _
	 "		<tr><td width='100%' align='left' valign='top'>" & id_Body & "</td></tr>" & _		 
	 "	</table>" & _
	 "</body></html>"
 
	Mail.HTMLBody=id_Body
	Mail.Fields.update
	
	on error resume next
	Mail.Send
	on error goto 0

	Set Mail = Nothing 
	Set MailCon = Nothing 
end function



function Email_EnviarKenko(id_addAddress, id_Subject, id_Body, id_Destinatario, id_Server, id_User, id_Password, id_Queue, id_Html, id_From, id_FromName, id_Addcc, id_AddBcc, id_Opcao_Envio, anexo)
	dim servidor
	dim de
	servidor="mail.photonplatinum.com.br"
	de	    ="administrativo@photonplatinum.com.br"
	user	="administrativo@photonplatinum.com.br"
	pass	="Administra01"
	
	id_server=servidor
	id_from=de

	Set Mail = Server.CreateObject("CDO.Message") 
	Set MailCon = Server.CreateObject ("CDO.Configuration") 
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = id_Server
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 587 
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 
	if len(user)>0 then
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = user
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = pass
	end if
	MailCon.Fields.update 
	Set Mail.Configuration = MailCon 
	Mail.From = id_From
		
	lista_email=split(id_AddAddress,";")	
	copia=""
	for i=0 to ubound(lista_email)
		if cdbl(i)=cdbl(0) then
			Mail.To = lista_email(i)
		else
			if i>1 then copia=copia & ";"
			copia=copia & lista_email(i)			
		end if
	next	
	if len(copia)>0 then mail.CC	= copia
	
	Mail.Subject = id_Subject
	
	if len(trim(anexo))>0 then mail.AddAttachment anexo,"",""			
	id_Body	="<html><head><title></title></head><body>" & _
	 "	<table border='0' width='100%' cellppading='0' cellspacing='0'>" & _
	 "		<tr><td width='100%' align='left' valign='top'>" & id_Body & "</td></tr>" & _		 
	 "	</table>" & _
	 "</body></html>"
 
	Mail.HTMLBody=id_Body
	Mail.Fields.update
	
	on error resume next
	Mail.Send
	on error goto 0

	Set Mail = Nothing 
	Set MailCon = Nothing 
end function

function Email_EnviarMinuano(id_addAddress, id_Subject, id_Body, id_Destinatario, id_Server, id_User, id_Password, id_Queue, id_Html, id_From, id_FromName, id_Addcc, id_AddBcc, id_Opcao_Envio, anexo)
	dim servidor
	dim de
	servidor="smtp.edminuano.com.br"
	de	    ="xcompany@edminuano.com.br"
	user	="xcompany@edminuano.com.br"
	pass	="minuano2011"
	
	id_server=servidor
	id_from=de

	Set Mail = Server.CreateObject("CDO.Message") 
	Set MailCon = Server.CreateObject ("CDO.Configuration") 
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = id_Server
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 587 
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 
	if len(user)>0 then
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = user
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = pass
	end if
	MailCon.Fields.update 
	Set Mail.Configuration = MailCon 
	Mail.From = id_From
		
	lista_email=split(id_AddAddress,";")	
	copia=""
	for i=0 to ubound(lista_email)
		if cdbl(i)=cdbl(0) then
			Mail.To = lista_email(i)
		else
			if i>1 then copia=copia & ";"
			copia=copia & lista_email(i)			
		end if
	next	
	if len(copia)>0 then mail.CC	= copia
	
	Mail.Subject = id_Subject
	
	if len(trim(anexo))>0 then mail.AddAttachment anexo,"",""			
	id_Body	="<html><head><title></title></head><body>" & _
	 "	<table border='0' width='100%' cellppading='0' cellspacing='0'>" & _
	 "		<tr><td width='100%' align='left' valign='top'>" & id_Body & "</td></tr>" & _		 
	 "	</table>" & _
	 "</body></html>"
 
	Mail.HTMLBody=id_Body
	Mail.Fields.update
	
	on error resume next
	Mail.Send
	on error goto 0

	Set Mail = Nothing 
	Set MailCon = Nothing 
end function


function Email_EnviarOnline(id_addAddress, id_Subject, id_Body, id_Destinatario, id_Server, id_User, id_Password, id_Queue, id_Html, id_From, id_FromName, id_Addcc, id_AddBcc, id_Opcao_Envio, anexo)
	dim servidor
	dim de
	
	servidor="mail.editoraonline.com.br"	
	de	    ="nfe@editoraonline.com.br"
	user	="nfe@editoraonline.com.br"
	pass	="@!xpto20062011"
	id_server=servidor
	id_from=de

	Set Mail = Server.CreateObject("CDO.Message") 
	Set MailCon = Server.CreateObject ("CDO.Configuration") 
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = id_Server
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 587 
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
	MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 
	if len(user)>0 then
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = user
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = pass
	end if
	MailCon.Fields.update 
	Set Mail.Configuration = MailCon 
	Mail.From = id_From
		
	'Response.Write id_AddAddress
	'Response.End 
	
	lista_email=split(id_AddAddress,";")	
	copia=""
	for i=0 to ubound(lista_email)
		if cdbl(i)=cdbl(0) then
			Mail.To = lista_email(i)
		else
			if i>1 then copia=copia & ";"
			copia=copia & lista_email(i)			
		end if
	next	
	if len(copia)>0 then mail.CC	= copia
	
	
	Mail.Subject = id_Subject		
	
	if len(trim(anexo))>0 then mail.AddAttachment anexo,"",""			
	id_Body	="<html><head><title></title></head><body>" & _
	 "	<table border='0' width='100%' cellppading='0' cellspacing='0'>" & _
	 "		<tr><td width='100%' align='left' valign='top'>" & id_Body & "</td></tr>" & _		 
	 "	</table>" & _
	 "</body></html>"
 
	Mail.HTMLBody=id_Body
	Mail.Fields.update
	
	on error resume next
	Mail.Send
	on error goto 0

	Set Mail = Nothing 
	Set MailCon = Nothing 
end function

           
function EncodeUTF8(s)
	dim i 
	dim c  
	i = 1
	
	do while i <= len(s)
		c = asc(mid(s,i,1))    
		if c >= &H80 then
			s = left(s,i-1) + chr(&HC2 + ((c and &H40) / &H40)) + chr(c and &HBF) + mid(s,i+1)      
			i = i + 1
		end if    
		i = i + 1
	loop  
	EncodeUTF8 = s 
end function

%>
<!--#include virtual="public/pub_Geral1.asp"-->