<%'@LANGUAGE = VBScript.Encode %>
<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10594)

emitente	=session("empresa_id")
strIni		=request("strini")
strFim		=request("strfim")
situacao	=request("situacao")
importacao	=request("importacao")
completo	=request("completo")

cancelada	=request("cancelada")

cnpj	=trim(request("cnpj"))
dest	=""

if len(cnpj)>0 then
	strsql="select a.destinatario " & _
    " from fat_Destinatario a inner join fat_comercio b on a.destinatario=b.destinatario " & _
    " where a.cgc_cpf='" & cnpj & "' and b.filial='"&emitente&"' "
	set adors1=locConnVenda.execute(strsql)
	if not adors1.eof then dest=adors1("destinatario")
	set adors1=nothing
end if
     
strsql=	"select tipo='NFE',b.cgc_cpf,a.emitente,a.nota,a.serie,xmlNota=isnull(convert(xml,xmlNota),''),a.chave_autenticacao " & _
		"FROM fat_Nota_b01 a INNER JOIN fat_DESTINATARIO b ON (a.emitente=b.destinatario) " & _
        "where a.emitente='" & emitente & "' and (a.destinatario='" & dest & "' or " & len(dest) & "=0) and convert(datetime,convert(varchar(10),a.emissao,120)) between '" & dt_Fim(strIni) & "' and '" & dt_Fim(strfim) & "' " & _
        "and situacao in(" & situacao & ") and len(chave_autenticacao)>0 "

strsql=	strsql&" UNION ALL " &_
        "select tipo='NFE',b.cgc_cpf,a.emitente,a.nota,a.serie,xmlNota=isnull(convert(xml,xmlNota),''),a.chave_autenticacao " & _
		"FROM hinode_historico..fat_Nota_b01 a INNER JOIN fat_DESTINATARIO b ON (a.emitente=b.destinatario) " & _
        "where a.emitente='" & emitente & "' and (a.destinatario='" & dest & "' or " & len(dest) & "=0) and convert(datetime,convert(varchar(10),a.emissao,120)) between '" & dt_Fim(strIni) & "' and '" & dt_Fim(strfim) & "' " & _
        "and situacao in(" & situacao & ") and len(chave_autenticacao)>0 "

if cancelada="1" then
	strsql=strsql &	" union all " & _
			"select tipo='CANC',b.cgc_cpf,a.emitente,a.nota,a.serie,xmlNota=isnull(convert(xml,c.xmlCancelamento),''),a.chave_autenticacao " & _
			"FROM fat_Nota_b01 a INNER JOIN fat_DESTINATARIO b ON (a.emitente=b.destinatario) " & _
			"inner join fat_nota_cc01 c on a.emitente=c.emitente and a.nota=c.nota and a.serie=c.serie " & _
			"where a.emitente='" & emitente & "' and (a.destinatario='" & dest & "' or " & len(dest) & "=0) and convert(datetime,convert(varchar(10),a.emissao,120)) between '" & dt_Fim(strIni) & "' and '" & dt_Fim(strfim) & "' " & _
			"and a.situacao in(" & situacao & ") and len(a.chave_autenticacao)>0 and c.situacao=1"

    strsql=strsql &	" union all " & _
			"select tipo='CANC',b.cgc_cpf,a.emitente,a.nota,a.serie,xmlNota=isnull(convert(xml,c.xmlCancelamento),''),a.chave_autenticacao " & _
			"FROM hinode_historico..fat_Nota_b01 a INNER JOIN fat_DESTINATARIO b ON (a.emitente=b.destinatario) " & _
			"inner join fat_nota_cc01 c on a.emitente=c.emitente and a.nota=c.nota and a.serie=c.serie " & _
			"where a.emitente='" & emitente & "' and (a.destinatario='" & dest & "' or " & len(dest) & "=0) and convert(datetime,convert(varchar(10),a.emissao,120)) between '" & dt_Fim(strIni) & "' and '" & dt_Fim(strfim) & "' " & _
			"and a.situacao in(" & situacao & ") and len(a.chave_autenticacao)>0 and c.situacao=1"
end if                

'call rw(strsql,1)

set adors1=locConnVenda.execute(strsql)
if adors1.eof then
    %><script>alert('Nenhuma nota-fiscal localizada no per�odo.'); history.back(-1);</script><%
    Response.end
end if

set objArquivo = createobject("scripting.filesystemobject")
pasta	=server.MapPath("xml\" & session("usuario") & "\")

'' apagar pasta se j� existir, para excluir todos arquivos.
if objArquivo.FolderExists(pasta) then objArquivo.DeleteFolder(pasta)

'' criar pasta
objArquivo.CreateFolder(pasta)

do while not adors1.eof
	cnpj				=adors1("cgc_cpf")
	chave_autenticacao	=adors1("chave_autenticacao")
	tipo				=adors1("tipo")
	
	if len(trim(adors1("xmlNota")))>0 and not isnull(adors1("xmlNota")) then
			
		xml=adors1("xmlNota")	                	                
	    if mid(xml,1,5) <> "<?xml" then '''se n�o tiver a tag inicial, acrescentar.
			xml="<?xml version=""1.0"" encoding=""iso-8859-1""?>" & xml
		end if
		
		xml=replace(xml,"<?xml version=""1.0"" encoding=""iso-8859-1""?>","<?xml version=""1.0"" encoding=""UTF-8""?>")
		
		'posicao1=0
		'posicao2=0		
		'if cstr(completo)="0" then
		'	posicao1	=instr(1,xml,"<NFe")
		'	posicao2	=instr(1,xml,"</NFe>")
		'end if			
		'if posicao1=0 then posicao1=1
		'if posicao2=0 then posicao2=len(xml)
		'xml = mid(xml,posicao1,(posicao2-posicao1+6))

		xml	= EncodeUTF8(xml)				
		'arquivoXml= "xml\" & session("usuario") & "\NFE_"&adors1("cgc_cpf") & "_" & adors1("serie") & "_" & adors1("nota") & ".xml"
		arquivoXml= "xml\" & session("usuario") & "\" & chave_autenticacao & "-" & tipo & ".xml"
		                 
		'cria o arquivo texto no disco com op��o de sobrescrever o arquivo existente
		Set arquivo = objArquivo.CreateTextFile(server.mappath(arquivoXml), true) 
		arquivo.writeline xml
		Arquivo = arquivoXml
	end if
	
	adors1.movenext
loop
set adors1=nothing

''gerar arquivo compactado pra download
arqZip	=pasta & "\NFE_" & cnpj & "_" & replace(strini,"/","") & "_" & replace(strFim,"/","") & ".zip"
Dim objZip
Set objZip = Server.CreateObject("XStandard.Zip") 
objZip.Pack pasta, arqZip
Set objZip = Nothing 

'Response.end

''efetuar download - adicionado por Ewerton em 20/08/2015
downloadZip = "xml/" & session("usuario") & "/" & replace(replace(arqZip,pasta,""),"\","")
Response.Redirect downloadZip


''efetuar download
Response.Buffer = True
Response.AddHeader "Content-Type","application/x-msdownload"
Response.AddHeader "Content-Disposition","attachment; filename=" & arqZip
Response.Flush

Set objStream = Server.CreateObject("ADODB.Stream")
objStream.Open
objStream.Type = 1
objStream.LoadFromFile arqZip
Response.BinaryWrite objStream.Read
objStream.Close
Set objStream = Nothing
Response.Flush  



function EncodeUTF8(s)
	dim i 
	dim c  
	i = 1
	
	do while i <= len(s)
		c = asc(mid(s,i,1))    
		if c >= &H80 then
			s = left(s,i-1) + chr(&HC2 + ((c and &H40) / &H40)) + chr(c and &HBF) + mid(s,i+1)      
			i = i + 1
		end if    
		i = i + 1
	loop  
	EncodeUTF8 = s 
end function


%>
<!--#include virtual="public/pub_Geral1.asp"-->

