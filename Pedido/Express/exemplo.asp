
<HTML>

<HEAD>
<title>P�gina de exemplo de envio de dados - ItauShopline</title>
</HEAD>
 
  <BODY leftmargin=0 topmargin=0  bgcolor="#ffffff">

 <%
 
 	'-------------------------
	' ACRESCENTE NESTES CAMPOS O C�DIGO DE EMPRESA E CHAVE REFERENTE A SUA EMPRESA.
	'-------------------------
               
     codemp= "J0032659300001650000003257"
     chave = "DFSHOPLINE081374" 
               
    '-------------------------
	' A CADA PROCESSO, ALTERAR O NUMERO DO PEDIDO (900001,900002,900003,ETC...)
	'-------------------------
     pedido = "96997011"
    '-------------------------
     
    '-------------------------
    ' VALOR FIXADO EM R$1,00 - VALOR DE TESTE
    '-------------------------
     valor = "1,00"

	'-------------------------
	' OS DEMAIS CAMPOS ABAIXO N�O OBRIGATORIOS.
	' SE CASO DESEJAR PREENCHER ALGUNS DESTES CAMPOS VERIFIQUE A ESPECIFICA��O
	' DO FORMATO DOS CAMPOS NO MANUAL.  
	'-------------------------
	observacao=""
	nomeSacado="Teste do Ita�"
	codigoInscricao=""
	numeroInscricao= ""
	enderecoSacado = ""
	bairroSacado = ""
	cepSacado = ""
	cidadeSacado = ""
	estadoSacado = ""
	dataVencimento = ""
	urlRetorna = ""
	obsad1 = ""
	obsad2 = ""
    obsad3 = ""
	'-----------------------------------------------------------------------------
			
	'NO M�TODO ABAIXO, GERADADOS,OS CAMPOS DEVEM ESTAR NECESS�RIAMENTE COMO NA ORDEM ABAIXO.	
				
   	Set cripto = server.createobject("itaucripto.cripto")
    	dados = cripto.geraDados(codEmp,pedido,valor,observacao,chave,nomeSacado,codigoInscricao,numeroInscricao,enderecoSacado,bairroSacado,cepSacado,cidadeSacado,estadoSacado,dataVencimento,urlRetorna,obsad1,obsad2,obsad3,"","","","")
	Set obj = nothing
 %>

	 <br><br><br>

   <!-- FORM PARA CHAMADA DO SITE DO ITAU SHOPLINE PASSANDO OS CAMPOS CRIPTOGRAFADOS-->

	 <form action="https://shopline.itau.com.br/shopline/shopline.asp" method="POST" name="form" onsubmit=carregabrw() target="SHOPLINE">
	 	<input type="hidden" name="DC" value="<%=dados%>">
	    	<center>
		    	<input type="submit" value="Ita� Shopline." name="Shopline">
	    	</center>
 	</form>
	
	<!-- FORM PARA CHAMADA DO BOLETO NO AMBIENTE DE TESTES-->

	 <form action="https://shopline.itau.com.br/shopline/emissao_teste.asp" method="POST" name="form" onsubmit=carregabrw() target="SHOPLINE">
	 	<input type="hidden" name="DC" value="<%=dados%>">
	    	<center>
		    	<input type="submit" value="Emiss�o de Boleto em teste." name="Shopline">
	    	</center>
 	</form>
	
	 <!-- FORM PARA CHAMADA DO BOLETO PASSANDO OS CAMPOS CRIPTOGRAFADOS -->

	 <form action="https://shopline.itau.com.br/shopline/Itaubloqueto.asp" method="POST" name="form" onsubmit=carregabrw() target="SHOPLINE">
	 	<input type="hidden" name="DC" value="<%=dados%>">
	    	<center>
		    	<input type="submit" value="Emiss�o de Boleto." name="Shopline">
	    	</center>
 	</form>


    <!-- FUN��O PARA CHAMADA DA ABERTURA DA JANELA DO SITE DENTRO DA ESPECIFICA��O DE TAMANHO  -->

	<script language="JavaScript">
   	   function carregabrw()
	    {
	    	window.open('','SHOPLINE',"toolbar=yes,menubar=yes,resizable=yes,status=no,scrollbars=yes,width=675,height=485");
	  	}						
    </script>
				
    </BODY>
  </HTML>
