<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<!--#include virtual='public/pub_BodyOculto.asp'-->
<!--#include file='ped_Funcoes.asp'-->

<!--#include file='css_Logo.asp'-->
<link rel="stylesheet" href="css.css" type="text/css" />


<script language="javascript">

function validaCartao()
{
  var path;
  path=document.frm1;
  
  if(path.numero.value.length<16)
  { 
    alert("Digite o n�mero do cart�o!!");
    path.numero.focus();
	return false;
  };
  
  if(path.vencMes.value.length<2)
  { 
    alert("Digite o M�S de vencimento do cart�o!!");
    path.vencMes.focus();
	return false;
  };
  
  if(path.vencAno.value.length<2)
  { 
    alert("Digite o ANO de vencimento do cart�o!!");
    path.vencAno.focus();
	return false;
  };
  
  if(path.seguranca.value.length<3)
  { 
    alert("Digite o c�digo de seguran�a do cart�o!!");
    path.seguranca.focus();
	return false;
  };
  
  return true;

}

</script>

<%
call verifica_sessao()

if session("cartaoc")<>"S" then
	session("msg_pedido")	="Necess�rio selecionar a forma de pagamento Cart�o de Cr�dito."
	Response.Redirect "ped_frete.asp"
end if

form_validar="validaCartao()"
call form_criar("frm1","ped_Cartao1.asp")
%>
<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border-collapse: collapse" bordercolor="#C0C0C0">		
		<tr>
			<td width="100%"><font face="verdana" size="2"><b>.Informe os dados do cart�o</font></td>
		</tr>
		<tr><td width="100%" colspan="2"><hr size="1"></td></tr>
</table>		
	
<br />
<br />
<br />	
		
<table width="300" border="0" cellpadding="0" cellspacing="1" style="border-collapse: collapse" bordercolor="#C0C0C0" align="center">		
	<%
		cartao	=session("c_cartao")
		numero	=session("c_numero")
		venc	=session("c_dt_vencimento")
		vencMes =session("c_vencMes")
		vencAno =session("c_vencAno")
		autorizacao	=session("c_cautorizacao")
		seguranca	=session("c_cseguranca")
		documento	=session("c_ndocumento")
		
		if len(trim(cartao))=0 then cartao=0

	strsql="select * from fat_cartaocredito order by descricao"
	set adoCartao=locconnvenda.execute(strsql)
	%>
	<tr>
		<td width="15%" align="left" bgcolor="#f5f5f5"><font face="verdana" size="1">Cart�o:</font></td>
		<td width="85%" align="left"><font face="verdana" size="1"><%
			call select_criar("cartao","")
				do while not adoCartao.eof
					if cdbL(adocartao("cartao"))=cdbl(cartao) then
						s="selected"
					else
						s=""
					end if
					
					call select_item(adocartao("cartao"),adocartao("Descricao"),s)
					adocartao.movenext
				loop
				set adoacartao=nothing
			call select_fim()
		%></font></td>
	</tr>
	<tr>
		<td width="15%" align="left" bgcolor="#f5f5f5"><font face="verdana" size="1">N�mero:</font></td>
		<td width="85%" align="left"><font face="verdana" size="1"><%call txt_criar("inteiro","numero",18,16,numero)%><i>somente n�meros</i></font></td>
	</tr>
	<tr>
		<td width="15%" align="left" bgcolor="#f5f5f5"><font face="verdana" size="1">Vencimento:</font></td>
		<td width="85%" align="left"><font face="verdana" size="1">
		  <%call txt_criar("inteiro","vencMes",2,2,vencMes)%>
		   /
		  <%call txt_criar("inteiro","vencAno",4,4,vencAno)%>
		  (mm / aaaa)
	    <i></i></font></td>
	</tr>
	<tr>
		<td width="15%" align="left" bgcolor="#f5f5f5"><font face="verdana" size="1">C�d Seguran�a:</font></td>
		<td width="85%" align="left"><font face="verdana" size="1"><%call txt_criar("inteiro","seguranca",4,5,seguranca)%><i>somente n�meros</i></font></td>
	</tr>
	<tr>
		<td width="100%" align="right" bgcolor="#f5f5f5" colspan="2"><%call button_criar("submit","btn1","Gravar","")%></td>
	</tr>

	<%
	call form_fim()
	%>
</table>
<!--#include virtual="public/pub_FimOculto.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->