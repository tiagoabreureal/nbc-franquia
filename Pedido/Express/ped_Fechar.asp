<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<!--#include file='ped_Funcoes.asp'-->

<!--#include file='css_Logo.asp'-->
<link rel="stylesheet" href="css.css" type="text/css" />


<%	
call verifica_sessao()

if session("calcular")	="" then call calcula_pedido()

strsql=	"select top 1 idProduto from TabPedidoTemp " & _
		"where idPedidoTemp='" & session("pedido") & "'"
set adoa=connLoja.execute(strsql)
if adoa.eof then
	session("msg_pedido")="N� h� produtos adicionados ao carrinho de compras!"
	Response.Redirect "ped_carrinho.asp"
end if
set adoa=nothing

if session("valida_frete")="" or session("valida_pagamento")="" then
	session("msg_pedido")="Necess�rio selecionar a transportadora / forma de pagamento"
	Response.Redirect "ped_Frete.asp"
end if

if len(trim(session("vendedor")))=0 then
	session("msg_pedido")="C�digo do Superior n�o localizado."
	Response.Redirect "ped_Carrinho.asp"
end if



%>
<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border-collapse: collapse" bordercolor="#C0C0C0">
		<tr>
			<td width="70%"><font face="verdana" size="2"><b>.Fechando Pedido - Confirma��o dos Dados</font></td>
			<td width="30%"><font face="verdana" size="1">&nbsp</td>
		</tr>
		<%
		if len(trim(session("msg_pedido")))>0 then			
		%><tr><td width="100%" colspan="2" align="center"><font face='verdana' size='1' color='red'><b><%=session("msg_pedido")%></b></font></tr><%
			session("msg_pedido")=""
		end if
		%>
		<tr><td width="100%" colspan="2"><hr size="1"></td></tr>
</table>
<table border="0" width="650">
	<tr><td width="100%" bgcolor="#f5f5f5"><font face="verdana" size="1"><b>..Dados de Entrega</b></font></td></tr>

	<tr><td width="100%">

<%
call exibir_entrega()
%>
	<br></td></tr>
	
	</tr>

<%
 if ( session("ped_localEntrega")="1" ) then
%>

	<tr><td width="100%" align="right"><input type="button" class="btn" value="Alterar Endere�o" onclick="window.location.href='ped_Entrega.asp?fecharPedido=OK'"/></td></tr>
	
<%
end if
%>	
	
	<br />
	<br />
	
	<tr><td width="100%" bgcolor="#f5f5f5"><font face="verdana" size="1"><b>..Pedido</b></font></td></tr>
	<tr><td width="100%">
	
<%
'call define_coluna (Titulo, campo, tamanho, alinhamento, formato, total,repete)
call define_coluna ("c�digo","integracao_entrada","6","c","t","n","s","","")
call define_coluna ("descri��o","descricao_abreviada","*","e","t","n","s","","")
'call define_coluna ("opera��o","operacao","10","e","t","n","s","","")
call define_coluna ("unit�rio","precoproduto","10","d","2","n","s","","")
call define_coluna ("qtd","qtd","10","d","0","s","s","","")
'call define_coluna ("desconto","valordesconto","5","d","2","s","s","","")
call define_coluna ("valor","valormercadoria","15","d","2","s","s","","")
call define_coluna ("pontos","pontos","10","d","2","s","s","","")
'call define_coluna ("icms retido","icmsretido","6","d","2","s","s","","")
'call define_coluna ("","obs","25","e","t","n","s","","")


'call define_link(1,"venda_nf.asp?nota=[nota]&serie=[serie]")
'call define_quebra ("", "depto","n","depto")

strsql=	"select pedicodigo=a.idPedidoTemp,qtd=a.quantidade" & _
		",precoproduto=a.valor,valormercadoria=a.valor*a.quantidade" & _
		",produto=b.codigo,integracao_entrada=b.codigo,descricao_abreviada=replace(a.nome,'<br>','') " & _		
		",pontos=a.quantidade*b.pontuacao " & _
		"from tabPedidoTemp a inner join tabProdutos b on a.idProduto=b.idProduto " & _
		"where idPedidotemp='" & session("pedido") & "' order by a.data"
set adors1=connLoja.execute(strsql)
total=0
call fun_rel_cab()
do while not adoRs1.eof
	total= cdbl(total) + cdbl(adors1("valorMercadoria"))
	call fun_rel_det()
	adoRs1.movenext
loop
call fun_fim_rel()

call zerar_relatorio()
%>
</table>
		</td>
	</tr>
<tr><td width="100%"><br>

<table width="100%" border="0" cellpadding="0" cellspacing="1" style="border-collapse: collapse" bordercolor="#C0C0C0">
	
	<tr>
		<td width="60%" bgcolor="#f5f5f5"><font face="verdana" size="1"><b>.Transportadora/Frete:</b> <%=session("valida_frete_descricao")%> (prazo: <%=session("valida_frete_prazo")%> dias) </b></td>
		<td width="30%" align="right" bgcolor="#f5f5f5"><font face="verdana" size="1"><%=formatnumber(session("valida_frete_valor"),2)%></td>
		<td width="10%" align="right" bgcolor="#f5f5f5"><font face="verdana" size="1">&nbsp;</td>
	</tr>
	<tr>
		<td bgcolor="#f5f5f5"><font face="verdana" size="1"><b>.Forma de Pagamento:</b> <%=session("valida_pagamento_descricao")%></b></td>
		<td bgcolor="#f5f5f5" align="right"><font face="verdana" size="1">&nbsp;</td>
		<td bgcolor="#f5f5f5"><font face="verdana" size="1">&nbsp</td>
	</tr>	
	
	<tr>
		<td width="100%" colspan="3"><br /></td>
	</tr>

<%
total =cdbl(total) + cdbl(session("valida_frete_valor"))
%>
	
	<tr>
		<td bgcolor="#f5f5f5"><font face="verdana" size="2"><b>Valor Total a Pagar</b></td>
		<td bgcolor="#f5f5f5" align="right"><font face="verdana" size="2" color="red"><b><%=session("moeda") & " " & formatnumber(total,2)%></b></td>
		<td><font face="verdana" size="1" color="red">&nbsp;</td>
	</tr>

</table>

<center>
<%
	call form_criar("frm1","ped_Confirmar.asp")
		call button_criar("submit","btn1","Concluir Pedido","")
	call form_fim()
%>
</center>

<!--#include virtual="public/pub_FimOculto.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->

















