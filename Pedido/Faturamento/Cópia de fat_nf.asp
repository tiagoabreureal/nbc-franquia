<%'@LANGUAGE = VBScript.Encode %>
<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%call Verifica_Permissao(114)%>
<!--#include virtual="public/pub_BodyRel750.asp"-->
<%
reqNf	 =request("nf") 
reqSerie =trim(request("se"))
strFilial=request("filial")
strEntrada=trim(request("entrada"))

if len(strFilial) < 1 then strFilial=session("emitente")

'' buscar dados no fat_entrada
if strEntrada="1" then 
	tabela="fat_entrada"
	tabela_ped_PedidoTotal="ped_PedidoTotal"
	strSql="select nome from fat_Destinatario where destinatario='" & strFilial & "'"
	set adoF=conn1.execute(strSql)
	if not adoF.eof then
		strNomeFilial=adoF("nome")
	end if
	adoF.close
else
	strsql="select nota from fat_saida where nota=" & reqNf & " and serie='" & reqSerie & "' and emitente='" & strfilial & "'"
	set adoa=locconnvenda.execute(strsql)
	if adoa.eof then
		tabela					="fat_saida_hist"
		tabela_ped_PedidoTotal	="ped_PedidoTotal_hist"
		tabela_ped_Observacao	="ped_Observacao_Hist"
	else
		tabela						="fat_saida"
		tabela_ped_PedidoTotal		="ped_PedidoTotal"
		tabela_ped_Observacao		="ped_Observacao"
	end if
	set adoa=nothing
	
	strSql="select cod_depto,depto,distrito,divisao,subgrupo,grupo " + _
		   "from view_depto where destinatario='" & strFilial & "'"
	set adoF=conn1.execute(strSql)
	if not adoF.eof then
		strCodDepto	 =adoF("cod_depto")
		strNomeFilial=adoF("depto")
		strDistrito	= adoF("distrito")
		strDivisao	= adoF("divisao")
		strSubGrupo	= adoF("subgrupo")
	end if
	adoF.close
end if

strMes=request("mes")
strAno=request("ano")

strSql="select mov.emitente,mov.destinatario,mov.destinatario_entrega,c.integracao," & _
	   "t.nome," & _
	   "(isnull(t.endereco,'') + ' ' + isnull(t.numero,'') + ' ' + isnull(t.complemento,'')) as endereco," & _
	   "isnull(t.cgc_cpf,'') as cgc, " & _
	   "isnull(t.inscricao_rg,'') as inscricao, " & _
	   "t.bairro, " & _
	   "t.cidade, " & _
	   "t.estado, " & _
	   "t.cep, " & _
	   "Canhoto=case canhoto_cf when 1 then 'sim' else 'n�o' end, " & _
	   "status=case situacao when 0 then 'Ativo' else 'Cancelado' end,situacao,emissao,pedido," & _	   
	   "desconto=isnull(sum(Desconto),0),valor=sum(valor),valor_total=isnull(sum(Valor_total),0),baseicms=isnull(sum(BaseIcms),0),icms=isnull(sum(icms),0)," + _
	   "baseretido=sum(baseretido),retido=sum(retido),frete=isnull(sum(frete),0),ipi=isnull(sum(mov.ipi),0)," & _
	   "e.responsavel" & _
	   ",emitenteCGC=d.cgc_cpf,emitenteINSC=d.inscricao_rg,"
	   if strentrada="1" then
		  strsql=strsql & "seguro=0," & _
						  "frete_Destinatario=0"
	   else
		  strsql=strsql & "seguro=isnull(sum(seguro),0),"  & _
						  "frete_Destinatario=case frete_destinatario when 0 then 'EMITENTE' else 'DESTINAT�RIO' end"
	   end if
	   strsql=strsql & " from " & tabela & " mov inner join sys_produto p on mov.produto=p.produto left join fat_destinatario t on "
	   if strentrada="1" then
			strSql=strsql & "mov.destinatario=t.destinatario "
	   else
			strSql=strsql & "mov.destinatario=t.destinatario "
	   end if
	   strsql=strsql & "left join fat_ca c on mov.destinatario=c.destinatario " + _
	   "left join fat_Destinatario d on mov.emitente=d.destinatario " + _	   
	   "left join fat_hierarquia e on mov.rota=e.hierarquia " & _
	   "where nota=" & reqNf & _
	   " and mov.produto<>'999999' and p.tipo<>'K' and mov.emitente='" & strfilial & "' "
	   if reqserie <> "x" then
	       strsql = strsql & " and serie='" & reqSerie & "'"
	   end if
	   if strentrada="1" then
		strsql = strsql & " group by e.responsavel,mov.emitente,mov.destinatario,c.integracao,t.nome,t.endereco,t.numero,t.complemento,t.cgc_cpf," + _
			"t.inscricao_rg,t.bairro,t.cidade,t.estado,t.cep,canhoto_cf,situacao,emissao,pedido,d.cgc_cpf,d.inscricao_rg"		
	   else
		strsql = strsql & " group by e.responsavel,mov.frete_Destinatario,mov.emitente,mov.destinatario,c.integracao,t.nome,t.endereco,t.numero,t.complemento,t.cgc_cpf," + _
			"t.inscricao_rg,t.bairro,t.cidade,t.estado,t.cep,canhoto_cf,situacao,emissao,pedido,d.cgc_cpf,d.inscricao_rg,mov.destinatario_entrega"
	   end if
'Response.Write strsql
'Response.End 
set adoNf=conn1.Execute(strsql)

if not adoNf.eof then
	strDestinatario	=adoNf("destinatario")
	strDestinatarioEntrega=adoNf("destinatario_entrega")
	strEmitente		=adoNf("emitente")
	strEmitenteCGC	=replace(replace(replace(adoNf("emitenteCGC"),".",""),"/",""),"-","")
	stremitenteCgc	=mid(stremitenteCgc,1,2) & "." & mid(stremitenteCgc,3,3) & "." & mid(stremitenteCgc,6,3) & "/" & mid(stremitenteCgc,9,4) & "-" & mid(stremitenteCgc,13,2)
	strEmitenteInsc	=replace(replace(replace(adoNf("emitenteInsc"),".",""),"/",""),"-","")
	strEmitenteInsc	=mid(strEmitenteInsc,1,3) & "." & mid(strEmitenteInsc,4,3) & "." & mid(strEmitenteInsc,7,3) & "." & mid(strEmitenteInsc,10,3)
	strNome			=adoNf("nome")
	'strCodDeptoD	=adoNF("cod_depto")
	'strDepto		=adoNF("depto")
	strEndereco		=adoNf("endereco")
	strBairro		=adoNf("bairro")
	strCgc			=replace(replace(replace(adoNf("cgc"),".",""),"/",""),"-","")
	strCgc			=mid(strCgc,1,2) & "." & mid(strCgc,3,3) & "." & mid(strCgc,6,3) & "/" & mid(strCgc,9,4) & "-" & mid(strCgc,13,2)
	strInscricao	=replace(replace(replace(adoNf("inscricao"),".",""),"/",""),"-","")
	strInscricao	=mid(strInscricao,1,3) & "." & mid(strInscricao,4,3) & "." & mid(strInscricao,7,3) & "." & mid(strInscricao,10,3)
	strCidade		=adoNf("cidade")
	strEstado		=adoNf("estado")
	strCep			=adoNf("cep")
	strEmissao		=adoNf("emissao")
	strPedido		=adoNf("pedido")
	intDesconto		=adoNf("desconto")
	intTValorTotal  =adonf("valor_total")
	intTValor		=adonf("valor")
	intTBaseIcms	=adonf("baseicms")
	intTIcms		=adonf("icms")
	intTBaseRetido	=adonf("baseretido")
	intTRetido		=adonf("retido")
	intTFrete		=adonf("frete")
	intTSeguro		=adonf("seguro")
	intTIpi			=adonf("ipi")
	strStatus		=adonf("status")
	strSituacao		=adonf("situacao")
	strCanhoto		=adonf("canhoto")
	strIntegracao	=adoNf("integracao")
	strFreteDestinatario=adoNf("frete_Destinatario")
	strVendedor		=adoNf("responsavel")	
else
	call redi_Aviso("Nota Fiscal","Nota fiscal n�o localizada.")	
end if	

if len(trim(strIntegracao))>0	then strNome=strIntegracao & " - " & strNome
if len(trim(strCodDeptoD))>0	then strNome=strNome & " - " & strCodDeptoD & " " & strDepto
adoNf.close  

'' pegar endereco do historico
strsql=	"select nome=isnull(nome,'')" & _
		",(isnull(endereco,'') + ' ' + isnull(numero,'') + ' ' + isnull(complemento,'')) as endereco" & _
		",bairro=isnull(bairro,''),cgc=isnull(cgc_cpf,''),inscricao=isnull(inscricao_rg,''),cidade=isnull(cidade,'')" & _
		",estado=isnull(estado,''),cep=isnull(cep,''),b.nomepat logradouro" & _
		",e_nome=isnull(e_nome,'')" & _
		",(isnull(e_endereco,'') + ' ' + isnull(e_numero,'') + ' ' + isnull(e_complemento,'')) as e_endereco" & _
		",e_bairro=isnull(e_bairro,''),e_cgc=isnull(e_cgc_cpf,''),e_inscricao=isnull(e_inscricao_rg,''),e_cidade=isnull(e_cidade,'')" & _
		",e_estado=isnull(e_estado,''),e_cep=isnull(e_cep,''),c.nomepat e_logradouro  " & _
		"from fat_saida_endereco a left join tblGPBTipolog b ON (a.logradouro=b.CHAVEPAT) " & _
		"left join tblGPBTipolog c ON (a.e_logradouro=c.CHAVEPAT) " & _
		"where nota=" & reqNF & " and serie='" & reqSerie & "' and a.emitente='" & strfilial & "'"
set adoa=locconnvenda.execute(strsql)
if not adoa.eof then
	strNome			=adoa("nome")
	strEndereco		=adoa("endereco")
	strBairro		=adoa("bairro")
	strCgc			=replace(replace(replace(adoa("cgc"),".",""),"/",""),"-","")
	strCgc			=mid(strCgc,1,2) & "." & mid(strCgc,3,3) & "." & mid(strCgc,6,3) & "/" & mid(strCgc,9,4) & "-" & mid(strCgc,13,2)
	strInscricao	=replace(replace(replace(adoa("inscricao"),".",""),"/",""),"-","")
	strInscricao	=mid(strInscricao,1,3) & "." & mid(strInscricao,4,3) & "." & mid(strInscricao,7,3) & "." & mid(strInscricao,10,3)
	strCidade		=adoa("cidade")
	strEstado		=adoa("estado")
	strCep			=adoa("cep")	
	strLogradouro	=adoa("logradouro")

	stre_Nome		=adoa("e_nome")
	stre_Endereco	=adoa("e_endereco")
	strE_Bairro		=adoa("e_bairro")
	strE_Cgc		=replace(replace(replace(adoa("e_cgc"),".",""),"/",""),"-","")
	strE_Cgc		=mid(strCgc,1,2) & "." & mid(strCgc,3,3) & "." & mid(strCgc,6,3) & "/" & mid(strCgc,9,4) & "-" & mid(strCgc,13,2)
	strE_Inscricao	=replace(replace(replace(adoa("e_inscricao"),".",""),"/",""),"-","")
	strE_Inscricao	=mid(strInscricao,1,3) & "." & mid(strInscricao,4,3) & "." & mid(strInscricao,7,3) & "." & mid(strInscricao,10,3)
	strE_Cidade		=adoa("e_cidade")
	strE_Estado		=adoa("e_estado")
	strE_Cep		=adoa("e_cep")
	strE_Logradouro =adoa("e_logradouro")
end if
set adoa=nothing
''

'--operacao
StrSql = "select distinct " & _
         "c.descricao operacao,c.ordem, " & _
         "tipo=case entsai when 1 then 'ENTRADA' else 'SAIDA' end " & _
         "from (" & tabela & " f inner join sys_produto s on f.produto=s.produto) " & _
         "inner join fat_operacao c on f.Operacao=c.operacao " & _         
		 "where nota=" & reqNf  
if reqSerie <> "x" then
	strsql = strsql & " and serie='" & reqSerie & "'"
end if
if strentrada = "2" then
	strsql = strsql & " and f.produto <> '999999' and tipo<>'K' and destinatario='" & strFilial & "' "
else
	strsql = strsql & " and f.produto <> '999999' and tipo<>'K' "
end if
strsql=strsql & " order by c.ordem"
set adoTemp1=conn1.execute(strSql)

if not adotemp1.eof then
	do while not adotemp1.eof
		strOperacao	=strOperacao + trim(adotemp1("operacao")) & "/"
		strTipo=trim(adoTemp1("tipo"))
		adotemp1.movenext
	loop
	strOperacao=mid(strOperacao,1,len(strOperacao)-1)
else
	strOperacao=""
	strTipo=""
end if
adotemp1.close
%>
<table border="0" width="100%" cellspacing="0" cellpadding="0">				
    <tr><td width="100%" bgcolor="#dfdfdf"><b><font face="Verdana" size="1">EMITENTE</font></b></td></tr>
</table>

<table style="border-collapse: collapse" bordercolor="#C0C0C0" border="1" width="100%" cellspacing="1" cellpadding="0" align="center">
	<%if len(session("cod_tipo"))=0 then%>
	<tr>
		<td width="100%" colspan="5"><font face="Verdana" size="1"><b>Departamento</b></font></td>        
    </tr>
    <tr>
		<td width="40%" colspan="2"><font face="Verdana" size="1"><%=strCodDepto & "- " & strNomeFilial%></font></td>
        <td width="60%" colspan="3"><font face="Verdana" size="1"><%="Distrito" & ":" & strDistrito & " - Divis�o:" & strDivisao & " - SubGrupo:" & strSubGrupo%></font></td>
    </tr>
	<%end if%>
	<tr>
		<td width="23%"><font face="Verdana" size="1"><b>Nota Fiscal</b></font></td>
        <td width="17%"><font face="Verdana" size="1"><b>Status</b></font></td>
        <td width="20%"><font face="Verdana" size="1"><b>Pedido</b></font></td>
        <td width="20%"><font face="Verdana" size="1"><b>Tipo</b></font></td>
        <td width="20%"><font face="Verdana" size="1"><b>Impresso</b></font></td>
    </tr>
    <tr>
        <td width="23%"><font face="Verdana" size="1" color="blue"><%=reqNf & "-" & reqSerie%></font></td>
        <td width="17%"><font face="Verdana" color="<%=strCor1%>" size="1"><%=strStatus%></font></td>
        <td width="20%"><a href="../pedido/ped_Pedido.asp?Ped=<%=strPedido%>&Em=<%=strFilial%>"><font face="Verdana" color="black" size="2"><b><%=strPedido%></b></font></a></td>
        <td width="20%"><font face="Verdana" size="1" color="black"><%=strTipo%></font></td>
        <td width="20%"><font face="Verdana" size="1" color="black"><%=strCanhoto%></font></td>
    </tr>

	<tr>
		<td width="23%"><font face="Verdana" size="1"><b>Natureza Opera��o</b></font></td>
        <td width="17%"><font face="Verdana" size="1"><b>Vendedor</b></font></td>
        <td width="20%"><font face="Verdana" size="1"><b>Inscr. Est. Subst.</b></font></td>
        <td width="20%"><font face="Verdana" size="1"><b>C.N.P.J.</b></font></td>
        <td width="20%"><font face="Verdana" size="1"><b>Inscr. Estadual</b></font></td>
    </tr>
    <tr>
        <td width="23%"><font face="Verdana" size="1" color="black"><%=strOperacao%></font></td>
        <td width="17%"><font face="Verdana" size="1" color="black"><%=strVendedor%></font></td>
        <td width="20%"><font face="Verdana" size="1" color="black"></font></td>
        <td width="20%"><font face="Verdana" size="1" color="black"><%=strEmitenteCGC%></font></td>
        <td width="20%"><font face="Verdana" size="1" color="black"><%=strEmitenteInsc%></font></td>
    </tr>
</table>                
            
<table border="0" width="100%" cellspacing="0" cellpadding="0">				
    <tr><td width="100%" bgcolor="#dfdfdf"><b><font face="Verdana" size="1">DESTINAT�RIO</font></b></td></tr>
</table>

<table style="border-collapse: collapse" bordercolor="#C0C0C0" border="1" width="100%" cellspacing="1" cellpadding="0">
    <tr>
		<td width="60%" colspan="2" align="left"><b><font face="Verdana" size="1">Nome / Raz�o Social</font></b></td>
        <td width="20%" valign="bottom" align="left"><font face="Verdana" size="1"><b>C.N.P.J./ C.P.F.</b></font></td>
        <td width="20%" valign="bottom" align="left"><font face="Verdana" size="1"><b>Data Emiss�o</b></font></td>
    </tr>
    <tr>
        <td width="60%" colspan="2" height="13"><font face="Verdana" size="1" color="black">
			<a href="../cliente/Cli_Dados.asp?Dest=<%=strDestinatario%>"><%=strNome%></font></a></td>
        <td width="20%" height="13"><font face="Verdana" size="1" color="black"><%=strCGC%></font></td>
        <td width="20%" height="13"><font face="Verdana" size="1" color="black"><%=strEmissao%></font></td>
    </tr>
    <tr>
        <td width="60%" colspan="2" height="13"><font size="1" face="Verdana"><b>Endere�o</b></font></td>
        <td width="20%" height="13"><font size="1" face="Verdana"><b>Bairro</b></font></td>
        <td width="20%" height="13"><font size="1" face="Verdana"><b>Cep</b></font></td>
    </tr>
    <tr>
        <td width="60%" colspan="2" height="13"><font face="Verdana" size="1" color="black"><%=strlogradouro & ":" & strEndereco%></font></td>
        <td width="20%" height="13"><font face="Verdana" size="1" color="black"><%=strBairro%></font></td>
        <td width="20%" height="13"><font face="Verdana" size="1" color="black"><%=strCep%></font></td>
    </tr>
    <tr>
        <td width="40%" height="13"><font size="1" face="Verdana"><b>Munic�pio</b></font></td>
        <td width="20%" height="13"><font size="1" face="Verdana"><b>Fone / Fax</b></font></td>
        <td width="20%" height="13"><font size="1" face="Verdana"><b>U.F.</b></font></td>
        <td width="20%" height="13"><font size="1" face="Verdana"><b>Inscr. Estadual</b></font></td>
    </tr>
    <tr>
        <td width="40%"><font size="1" face="Verdana" color="black"><%=strCidade%></font></td>
        <td width="20%"><font size="1" face="Verdana" color="black">--</font></td>
        <td width="20%"><font size="1" face="Verdana" color="black"><%=strEstado%></font></td>
        <td width="20%"><font size="1" face="Verdana" color="black"><%=strInscricao%></font></td>
    </tr>
</table>
<%
'Response.Write strdestinatario & "-" & strDestinatarioEntrega
'Response.end
if cstr(strDestinatario) <> cstr(strDestinatarioEntrega) then
%>
		<table border="0" width="100%" cellspacing="0" cellpadding="0">				
		    <tr><td width="100%" bgcolor="#dfdfdf"><b><font face="Verdana" size="1">DESTINAT�RIO/ENTREGA</font></b></td></tr>
		</table>

		<table style="border-collapse: collapse" bordercolor="#C0C0C0" border="1" width="100%" cellspacing="1" cellpadding="0">
		    <tr>
				<td width="60%" colspan="2" align="left"><b><font face="Verdana" size="1">Nome / Raz�o Social</font></b></td>
		        <td width="20%" valign="bottom" align="left"><font face="Verdana" size="1"><b>C.N.P.J./ C.P.F.</b></font></td>
		        <td width="20%" valign="bottom" align="left"><font face="Verdana" size="1"><b>Data Emiss�o</b></font></td>
		    </tr>
		    <tr>
		        <td width="60%" colspan="2" height="13"><font face="Verdana" size="1" color="black">
					<a href="../cliente/Cli_Dados.asp?Dest=<%=strDestinatarioEntrega%>"><%=stre_Nome%></font></a></td>
		        <td width="20%" height="13"><font face="Verdana" size="1" color="black"><%=stre_CGC%></font></td>
		        <td width="20%" height="13"><font face="Verdana" size="1" color="black"><%=strEmissao%></font></td>
		    </tr>
		    <tr>
		        <td width="60%" colspan="2" height="13"><font size="1" face="Verdana"><b>Endere�o</b></font></td>
		        <td width="20%" height="13"><font size="1" face="Verdana"><b>Bairro</b></font></td>
		        <td width="20%" height="13"><font size="1" face="Verdana"><b>Cep</b></font></td>
		    </tr>
		    <tr>
		        <td width="60%" colspan="2" height="13"><font face="Verdana" size="1" color="black"><%=strE_logradouro & ":" & strEndereco%></font></td>
		        <td width="20%" height="13"><font face="Verdana" size="1" color="black"><%=strE_Bairro%></font></td>
		        <td width="20%" height="13"><font face="Verdana" size="1" color="black"><%=strE_Cep%></font></td>
		    </tr>
		    <tr>
		        <td width="40%" height="13"><font size="1" face="Verdana"><b>Munic�pio</b></font></td>
		        <td width="20%" height="13"><font size="1" face="Verdana"><b>Fone / Fax</b></font></td>
		        <td width="20%" height="13"><font size="1" face="Verdana"><b>U.F.</b></font></td>
		        <td width="20%" height="13"><font size="1" face="Verdana"><b>Inscr. Estadual</b></font></td>
		    </tr>
		    <tr>
		        <td width="40%"><font size="1" face="Verdana" color="black"><%=strE_Cidade%></font></td>
		        <td width="20%"><font size="1" face="Verdana" color="black">--</font></td>
		        <td width="20%"><font size="1" face="Verdana" color="black"><%=strE_Estado%></font></td>
		        <td width="20%"><font size="1" face="Verdana" color="black"><%=strE_Inscricao%></font></td>
		    </tr>
		</table>
<%
end if
%>
<table border="0" width="100%" cellspacing="0" cellpadding="0">				
	<tr>
		<td width="100%" bgcolor="#dfdfdf"><font face="Verdana" size="1"><b>DADOS DO PRODUTO</b></font></td>
    </tr>
</table>            

   <%

   strsql="select sit='',class='',mov.produto,mov.kit,quantidade,mov.unitario,mov.valor,valor_total," & _
		  "retido,desconto=isnull(desconto,0),porcentagem,porcipi,mov.cfo,unidade,tipoP=b.tipo" & _
		  ",descricao = case " & _
		  "				  when b.tipo='P' and mov.kit<>mov.produto then '---' + isnull(b.descricao,'') " & _
		  "			   else " & _
		  "				  isnull(b.descricao,'') " & _
		  "			  end " & _
		  " from " & tabela & " mov inner join sys_produto b on mov.produto=b.produto " & _
		  "left join fat_LinhaProduto c on b.linha=c.linha " & _
		  "where mov.produto <> '999999' and nota=" & reqNf & " and mov.emitente='" & strfilial & "' "
	if reqSerie <> "x" then
		strsql = strsql & " and serie='" & reqSerie & "'"
	end if
	if strentrada = "2" then
		strsql = strsql &  " order by mov.kit,mov.tipoP,b.descricao"
	else
		strsql = strsql &  " order by mov.kit,mov.tipoP,b.descricao"
	end if		
	'Response.Write strsql
	set adoRs1=conn1.Execute(strsql)
	if not adoRs1.eof then
		valor_c=adors1("quantidade")
		'response.write cdbl(valor_c) & "<br>" & cdbl(formatnumber(valor_c,0))
		'response.end
		'if (cdbl(adoRs1("quantidade")) < 1 and cdbl(formatnadoRs1("quantidade")) > 0) then
		if cdbl(valor_c) <> cdbl(formatnumber(valor_c,0)) then
			ab=3
		else
			ab=0
		end if
	end if
		
   call define_coluna ("codigo","produto","6","c","t","n","s","","")
   call define_coluna ("descri��o","descricao","*","e","t","n","s","","")
   call define_coluna ("cfo","cfo","5","c","t","n","s","","")
   call define_coluna ("class.","class","5","c","t","n","s","","")
   call define_coluna ("trib.","sit","5","c","t","n","s","","")
   call define_coluna ("unid","unidade","3","c","t","n","s","","")
   call define_coluna ("qtd","quantidade","7","d",ab,"n","s","","")
   call define_coluna ("unit","unitario","10","d","3","n","s","","")
   call define_coluna ("total","valor","10","d","2","n","s","","")
   call define_coluna ("ret","retido","3","d","2","n","s","","")
   call define_coluna ("icms","porcentagem","3","d","2","n","s","","")
   
   call define_link (1,"../produto/prod_Dados.asp?prod=[produto]")
   call define_link (2,"../produto/prod_Dados.asp?prod=[produto]")
	
	'impCab=1
	intDesconto=0
	imp_MaxLinha=2000
	call fun_rel_cab()	
	do while not adoRs1.eof
	  	intConta=intConta+1
	   	call fun_rel_det()
	   	intDesconto=intDesconto + Cdbl(adoRs1("desconto"))
	  	adoRs1.movenext
	loop
	call fun_fim_rel()				

if cdbl(intDesconto) <> 0 then%>
	<tr>
	  <td width="6%"><font face="verdana" size="1">&nbsp;</font></td>
	  <td width="*"><font size="1" face="Verdana" color="#800000">DESCONTO...</font></td>
	  <td width="5%"><font face="verdana" size="1">&nbsp;</font></td>
	  <td width="5%"><font face="verdana" size="1">&nbsp;</font></td>
	  <td width="5%"><font face="verdana" size="1">&nbsp;</font></td>
	  <td width="3%"><font face="verdana" size="1">&nbsp;</font></td>
	  <td width="7%"><font face="verdana" size="1">&nbsp;</font></td>
	  <td width="10%"><font face="verdana" size="1">&nbsp;</font></td>
	  <td width="10%" align="right"><font size="1" face="Verdana" color="#800000"><%=formatnumber(intDesconto,2)%></font></td>
	  <td width="3%"><font face="verdana" size="1">&nbsp;</font></td>
	  <td width="3%"><font face="verdana" size="1">&nbsp;</font></td>
	</tr><%
end if
%>
</table>

<tr>
	<td width="100%">&nbsp</td>
</tr>
<tr>
	<td width="100%">            
		<table border="0" width="100%" cellspacing="0" cellpadding="0">            
		   <tr>
		      <td width="100%" bgcolor="#dfdfdf"><b><font size="1" face="Verdana">C�LCULO DO IMPOSTO</font></b></td>
		   </tr>
		</table>

	<table style="border-collapse: collapse" bordercolor="#C0C0C0" border="1" width="100%" cellspacing="1" cellpadding="0">
	  <tr>
	    <td width="20%"><font size="1" face="Verdana"><b>Base ICMS</b></font></td>
	    <td width="20%"><font size="1" face="Verdana"><b>Valor ICMS</b></font></td>
	    <td width="20%"><font size="1" face="Verdana"><b>Base ICMS Subst.</b></font></td>
	    <td width="20%"><font size="1" face="Verdana"><b>ICMS Subst.</b></font></td>
	    <td width="20%"><font size="1" face="Verdana"><b>Valor Total Produtos</b></font></td>
	  </tr>
	  <tr>
	    <td width="20%" align="center"><font face="Verdana" size="1" color="black"><%=formatnumber(intTBaseIcms,2)%></font></td>
	    <td width="20%" align="center"><font face="Verdana" size="1" color="black"><%=formatnumber(intTIcms,2)%></font></td>
	    <td width="20%" align="center"><font face="Verdana" size="1" color="black"><%=formatnumber(intTBaseRetido,2)%></font></td>
	    <td width="20%" align="center"><font face="Verdana" size="1" color="black"><%=formatnumber(intTRetido,2)%></font></td>
	    <td width="20%" align="center"><font face="Verdana" size="1" color="black"><%=formatnumber(cdbl(intTValor)-cdbl(intDesconto),2)%></font></td>
	  </tr>
	  <tr>
	    <td width="20%"><font face="Verdana" size="1"><b>Valor Frete</b></font></td>
	    <td width="20%"><font face="Verdana" size="1"><b>Valor Seguro</b></font></td>
	    <td width="20%"><font face="Verdana" size="1"><b>Outras Despesas</b></font></td>
	    <td width="20%"><font face="Verdana" size="1"><b>Valor Total IPI</b></font></td>
	    <td width="20%"><font face="Verdana" size="1"><b>Valor Total Nota</b></font></td>
	  </tr>
	  <tr>
	    <td width="20%" align="center"><font size="1" face="Verdana" color="black"><%=formatnumber(intTFrete,2)%></font></td>
	    <td width="20%" align="center"><font size="1" face="Verdana" color="black"><%=formatnumber(intTSeguro,2)%></font></td>
	    <td width="20%" align="center"><font size="1" face="Verdana" color="black"></font></td>
	    <td width="20%" align="center"><font size="1" face="Verdana" color="black"><%=formatnumber(intTIpi,2)%></font></td>
	    <td width="20%" align="center"><font size="2" face="Verdana" color="black"><b><%=formatnumber(cdbl(intTValor)+cdbl(intTRetido)+cdbl(intTFrete)+cdbl(intTIpi)-cdbl(intDesconto),2)%></b></font></td>
	  </tr>
	  <%
		peso=0
		strsql="exec fat_peso_bruto " & reqNF & ",'" & strfilial & "','" & reqSerie & "'"
		set adoa=locconnvenda.execute(strsql)
		if not adoa.eof then
			peso=formatnumber(adoa("peso_bruto"),2)
		end if
		set adoa=nothing
		%>
				<tr>
					<td width="40%" colspan="2" align="left"><font face="verdana" size="1">Peso: <%=peso%></font></td>
					<td width="*" colspan="2" align="right"><font face="verdana" size="1">&nbsp;</font></td>
				</tr>                                                
	</table>
<tr>
	<td width="100%">            
		<table border="0" width="100%" cellspacing="0" cellpadding="0">            
		   <tr>
		      <td width="100%" bgcolor="#dfdfdf"><b><font size="1" face="Verdana">COBRAN�A</font></b></td>
		   </tr>
		   <%		
		strSql=	"select c.descricao,transporte=isnull(d.descricao,'')" & _
				"from " & tabela & " a inner join " & tabela_ped_PedidoTotal & " b on a.pedido=b.pediCodigo " & _
				"inner join fat_Tipo_Cobranca c on b.tipocobranca=c.tipo " & _
				"left join fat_transporte d on b.tranCodigo=d.transporte " & _
				"where nota=" & reqNF & " and a.serie='" & reqSerie & "' and a.emitente='" & strfilial & "' and a.produto='999999'"
		set adoA=locConnVenda.execute(strSql)
		if not adoA.eof then
			forma_pg=adoA("descricao")
			transporte=adoA("transporte")
		end if
		set adoA=nothing

		'strsql=	"select vencimento " & _
		'		"from " & tabela & "'"
		%>
		   <tr>
		      <td width="100%"><font size="1" face="Verdana"><%=forma_pg%></font></td>
		   </tr>
		   <tr>
		      <td width="100%">
		      <%
					dim total	
					total=0		
					primeiro=0
					strsql= "select a.documento,a.valor,vencimento=convert(varchar(10),isnull(vencimento,'2000-1-1'),103),s.descricao" & _
							",isnull(b.valor,0) valor_movimento,c.descricao descOperacao,b.data data_movimento,b.usuario" & _
							",valor_calculo=case c.processo when 'D' then b.valor*-1 when 'S' then b.valor else 0 end " & _
							"from " & tabela  & " x inner join cob_titulos a on x.pedido=a.pedido inner join cob_situacao s on a.situacao=s.situacao " & _
							"left join cob_movimento b on a.emitente=b.emitente and a.documento=b.documento " & _
							"left join cob_operacao c on b.operacao=c.operacao " & _
							"where nota=" & reqNf & " and x.serie='" & reqSerie & "' and x.emitente='" & strfilial & "' and x.produto='999999' " & _
							" order by a.documento,b.data,c.descricao"
					'Response.Write strsql
					set adoa=locconnvenda.execute(strsql)
					if not adoa.eof then					
						do while not adoa.eof
							if ultimo_doc<>adoa("documento") then
								if primeiro=1 then call imprime_total()
								call imprime_topo()
								ultimo_doc=adoa("documento")
								total=adoa("valor")
								primeiro=1
							end if
							'if adoa("vencimento")<>"01/01/2000" then
							'	'Response.Write adoA("vencimento")
							'end if
							if cdbl(adoa("valor_calculo"))<>0 then
								total=cdbl(total) + cdbl(adoa("valor_calculo"))
								%>
								<tr><td width='10%' align='right'><font face='verdana' size='1'></font></td>
									<td align='center' colspan="2"><font face='verdana' size='1'><%=adoa("descOperacao")%></font></td>
								   	<td width='10%' align='right'><font face='verdana' size='1'><%=formatnumber(adoa("valor_movimento"),2)%></font></td>
								   	<td width='10%' align='center'><font face='verdana' size='1'></font></td>
								   	<td width='10%' align='center' ><font face='verdana' size='1'><%=adoa("data_movimento")%></font></td>							   	
								   	<td width='10%' align='center' ><font face='verdana' size='1'><%=adoa("usuario")%></font></td>
								   	<td width='10%' align='center'><font face='verdana' size='1'></font></td>
								</tr>							
							<%
							end if
							adoA.movenext
							'if not adoA.eof then 'Response.Write "<br>"
						loop
						'if cdbl(total)<>0 then 
						call imprime_total()
						%>
							
							</table>
						<%
					end if
					set adoa=nothing
		      %>
		      </td>
		   </tr>
		   <tr>
		      <td width="100%" bgcolor="#dfdfdf"><b><font size="1" face="Verdana">FRETE POR CONTA (<%=strFreteDestinatario%>)</font></b></td>
		   </tr>	   
		   <tr>
		      <td width="100%"><font size="1" face="Verdana"><%=transporte%></font></td>
		   </tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%">            
		<table border="0" width="100%" cellspacing="0" cellpadding="0">            
		   <tr>
		      <td width="100%" bgcolor="#dfdfdf"><b><font size="1" face="Verdana">OBSERVA��O</font></b></td>
		   </tr>
		   <tr>
				<td width="100%">    
			    <%
			    call zerar_relatorio()	
			    strSql="select mov.observacao,complemento,b.descricao " & _
						 "from " & tabela_ped_Observacao & " mov inner join fat_observacao b on mov.observacao=b.observacao " & _
						 "where pediCodigo=" & strPedido 
						 'call rw(strsql,1)
				set adoRs1=locConnVenda.Execute(strSql)
				
				call define_coluna ("Tipo","descricao","35","e","t","n","s","","")
				call define_coluna ("Observa��o","complemento","*","e","t","n","s","","")

				tam_tabela="60"
				call fun_rel_cab()	
				do while not adoRs1.eof
					intConta=intConta+1
					
					if adors1("observacao")=1003 then
						call define_link(2,"javascript:correio('[complemento]')")
					elseif adors1("observacao")>=1004 and adors1("observacao")<=1008 then
						call define_link(2,"../email/eml_email_pedido.asp?pedido=" & strPedido)
					else
						call define_link(2,"")
					end if
					call fun_rel_det()
					adoRs1.movenext
				loop
				call fun_fim_rel()		
			    %></table>
			    </td>
			</tr>    
		</table>
	</td>
</tr>		   
<%
sub imprime_topo()
	Response.Write "<table border='1' width='80%' style='border-collapse: collapse' width='100%' cellpadding='0' cellspacing='0'>" & _
			   "<tr><td width='10%' align='right' bgcolor='#f5f5f5'><font face='verdana' size='1'>Documento:</font></td>" & _
			   "	<td width='10%' align='center'><font face='verdana' size='1'>" & adoa("documento") & "</font></td>" & _									   
			   "	<td width='10%' align='right' bgcolor='#f5f5f5'><font face='verdana' size='1'>Valor:</font></td>" & _
			   "	<td width='10%' align='right'><font face='verdana' size='1'>" & formatnumber(adoa("valor"),2) & "</font></td>" & _
			   "	<td width='10%' align='right' bgcolor='#f5f5f5'><font face='verdana' size='1'>Vencimento:</font></td>" & _
			   "	<td width='10%' align='center'><font face='verdana' size='1'>" & adoa("vencimento") & "</font></td>" & _
			   "	<td width='10%' align='right' bgcolor='#f5f5f5'><font face='verdana' size='1'>Situa��o:</font></td>" & _
			   "	<td width='10%' align='center'><font face='verdana' size='1'>" & adoa("descricao") & "</font></td>" & _
			   "</tr>" 
end sub			

sub imprime_total()
%>
	<tr><td colspan=3 align='right' bgcolor='#E8E8FF'><font face='verdana' size='1'></font></td>
   		<td width='10%' align='right' bgcolor='#E8E8FF'><font face='verdana' size='1'><%=formatnumber(total,2)%></font></td>
   		<td colspan=4 align='center' bgcolor='#E8E8FF'><font face='verdana' size='1'></font></td>
	</tr>
<%							
end sub
%>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->