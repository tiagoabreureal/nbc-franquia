<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10210)

opcao		=rec("opcao")
operacao	=request("operacao")

descricao	=ucase(rec("descricao"))
cfo			=rec("cfo")
cfofora		=rec("cfofora")
entsai		=rec("entsai")
icms		=rec("icms")
retido		=rec("retido")
venda		=rec("venda")
obs			=ucase(rec("obs"))

link="fat_CadastroOperacao1.asp"

if opcao="I" then ''incluir
	if len(descricao)=0 then
		session("msg")="Para incluir uma Opera��o, necess�rio informar a descri��o."
		Response.Redirect link
		Response.end
	end if
	
	produtos=trim(request("produtos"))
	
	
	maxOp=0
	strsql="select op=isnull(max(operacao),0)+1 from fat_operacao"
	set adors1=locConnVenda.execute(strsql)
	if not adors1.eof then maxOp=adors1("op")
	set adors1=nothing
	
	strsql= "insert into fat_Operacao (atividade,operacao,descricao,cfo,cfofora,icms,retido,venda,entsai,obs) values " & _
			"(" & session("atividade") & "," & maxOp & ",'" & descricao & "'" & _
			",'" & cfo & "','" & cfofora & "'," & icms & "," & retido & "," & venda & "," & entsai & ",'" & obs & "')"		
	'call rw(strsql,1)
	locConnVenda.execute(strsql)
	
	if cstr(entsai)="1" then 
		es	="E"
	else
		es	="S"
	end if
	
	strsql=	"insert into est_Operacao (operacao,descricao,entrada_saida,tipo_destino) values " & _
			"(" & maxOp & ",'" & descricao & "','" & es & "',0)"
	locConnVenda.execute(strsql)
	
	if cstr(produtos)="1" then
		strsql=	"insert into fat_filial_produto_op (emitente,produto,operacao) " & _
				"select '" & session("emitente") & "',a.produto," & maxOp & " " & _
				"from sys_produto_aux a where atividade=" & session("atividade")
		locConnVenda.execute(strsql)
	end if
	
	session("msg")="Opera��o inclu�da com sucesso."
elseif opcao="A" then ''alterar
	strsql="select operacao from fat_operacao where atividade=" & session("atividade") & " and isnull(exigenfrefer,0)=0"
	set adors1=locconnvenda.execute(strsql)
	do while not adors1.eof
		operacao=adors1("operacao")		
		cdescricao	="op" & operacao
		ccfo		="cfo" & operacao
		ccfofora	="cfofora" & operacao
		centsai		="entsai" & operacao
		cicms		="icms" & operacao
		cretido		="retido" & operacao
		cvenda		="venda" & operacao
		cobs		="obs" & operacao
		
		descricao	=ucase(rec(cdescricao))
		cfo			=rec(ccfo)
		cfofora		=rec(ccfofora)
		entsai		=rec(centsai)
		icms		=rec(cicms)
		retido		=rec(cretido)
		venda		=rec(cvenda)
		obs			=ucase(rec(cobs))
		
		strsql=	"update fat_operacao set descricao='" & descricao & "'" & _
				",cfo='" & cfo & "',cfofora='" & cfofora & "',entsai=" & entsai & " " & _
				",icms=" & icms & ",retido=" & retido & ",venda=" & venda & ",obs='" & obs & "' " & _
				"where atividade=" & session("atividade") & " and operacao=" & operacao
		'call rw(strsql,1)
		locconnvenda.execute(strsql)
								
		if cstr(centsai)="1" then 
			es	="E"
		else
			es	="S"
		end if
		
		strsql=	"update est_operacao set descricao='" & descricao & "',entrada_saida='" & es & "' " & _
				"where operacao=" & operacao
		locconnvenda.execute(strsql)
				
		adors1.movenext
	loop
	set adors1=nothing
	session("msg")="Atualizado com sucesso."

elseif opcao="E" then ''excluir
	
	strsql="select top 1 nota,serie from fat_saida where operacao=" & operacao
	set adors1=locconnvenda.execute(strsql)
	if not adors1.eof then
		session("msg")="Existe nota fiscal emitida com essa opera��o (" & adors1("nota") & "-" & adors1("serie") & "), n�o � poss�vel excluir."
		Response.Redirect link
		Response.end
	end if
	set adors1=nothing
		
	strsql=	"delete fat_filial_produto_op " & _
			"where emitente='" & session("emitente") & "' and operacao=" & operacao
	locConnVenda.execute(strsql)
		
	strsql="delete fat_operacao where atividade=" & session("atividade") & " and operacao=" & operacao
	locconnvenda.execute(strsql)
	
	strsql="delete est_operacao where operacao=" & operacao
	locconnvenda.execute(strsql)
	
	session("msg")="Opera��o exclu�da com sucesso."	
	
end if


Response.Redirect link

%>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->