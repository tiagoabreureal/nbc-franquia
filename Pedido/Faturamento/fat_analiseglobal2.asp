<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10437)

ordem	=request("ordem")
col		=request("col")
codigo	=request("id")
ano		=request("ano")
frete	=request("frete")

strsql=	"select nome,b.descricao " & _
		"from view_consultor a inner join rede_qualificacao b on a.classificacao=b.qualificacao " & _
		"where integracao='" & codigo & "'"
set adors1=locconnvenda.execute(strsql)
if not adors1.eof then
	nome		=adors1("nome")
	qualificacao=adors1("descricao")
end if
set adors1=nothing

if ordem=1 then
	d="VENDAS"
	call define_coluna ("ID","superior_integracao","10","c","t","n","s","","")
	call define_coluna ("Patrocinador","superior_descricao","*","e","t","n","s","","")
	call define_coluna ("Vendas","vendas","15","d","2","s","s","","")
	
	link="../cliente/cli_dados.asp?dest=[superior_destinatario]"
	call define_link(1,link)
	call define_link(2,link)
	call define_link(3,"fat_analiseglobal3.asp?codigo=" & codigo & "&ano=" & ano & "&ordem=" & ordem & "&col=" & col & "&patrocinador=[superior_integracao]&frete=" & frete)
elseif ordem=2 or ordem=18 then
	d="NOVOS CONSULTORES"
	call define_coluna ("ID","superior_integracao","7","c","t","n","n","","")
	call define_coluna ("Patrocinador","superior_descricao","40","e","t","n","n","","")
	call define_coluna ("ID","integracao","7","c","t","n","s","","")
	call define_coluna ("Nome","Nome","*","e","t","n","s","","")
	call define_coluna ("Cadastro","dt_cad","8","c","t","n","s","","")
elseif ordem=3 or ordem=20 then
	d="ATIVOS"
	call define_coluna ("ID","superior_integracao","7","c","t","n","n","","")
	call define_coluna ("Patrocinador","superior_descricao","40","e","t","n","n","","")
	call define_coluna ("ID","integracao","7","c","t","n","s","","")
	call define_coluna ("Nome","Nome","*","e","t","n","s","","")
	call define_coluna ("Vendas","vendas","8","d","2","s","s","","")
elseif ordem=4 then
	d="QUANTIDADE DE PEDIDOS"
	call define_coluna ("ID","superior_integracao","10","c","t","n","s","","")
	call define_coluna ("Patrocinador","superior_descricao","*","e","t","n","s","","")
	call define_coluna ("Pedidos","qtd","15","d","0","s","s","","")
	call define_coluna ("Vendas","vendas","15","d","2","s","s","","")
	
	link="../cliente/cli_dados.asp?dest=[superior_destinatario]"
	call define_link(1,link)
	call define_link(2,link)
	
	link2="fat_analiseglobal3.asp?codigo=" & codigo & "&ano=" & ano & "&ordem=" & ordem & "&col=" & col & "&patrocinador=[superior_integracao]&frete=" & frete
	call define_link(3,link2)
	call define_link(4,link2)
elseif ordem=5 then
	d="VALOR M�DIO DO PEDIDO"
	call define_coluna ("ID","superior_integracao","8","c","t","n","n","","")
	call define_coluna ("Patrocinador","superior_descricao","*","e","t","n","n","","")
	call define_coluna ("ID","integracao","8","c","t","n","n","","")
	call define_coluna ("Consultor","nome","*","e","t","n","n","","")
	call define_coluna ("Pedidos","qtd_pedido","5","d","0","s","s","","")
	call define_coluna ("Vendas","vendas","8","d","2","s","s","","")
	call define_coluna ("Valor M�dio","valor_medio_pedido","8","d","2","s","s","","")
	
	link="../cliente/cli_dados.asp?dest=[destinatario]"
	call define_link(3,link)
	call define_link(4,link)
	call define_link(5,link)
	call define_link(6,link)
	call define_link(7,link)
elseif ordem=6 then
	d="VOLUME DE GRUPO"
	call define_coluna ("ID","integracao","8","c","t","n","n","","")
	call define_coluna ("Consultor","nome","*","e","t","n","n","","")
	call define_coluna ("Vol Pessoal","vol_pessoal","15","d","2","s","s","","")
	
	link="../cliente/cli_dados.asp?dest=[destinatario]"
	call define_link(1,link)
	call define_link(2,link)		
elseif ordem=11 then
	d="FATURADO LOJA CLIENTE"
	call define_coluna ("ID","integracao","8","c","t","n","n","","")
	call define_coluna ("Consultor","nome","*","e","t","n","n","","")
	call define_coluna ("Cliente","cliente","*","e","t","n","n","","")
	call define_coluna ("Vendas","vendas","8","d","2","s","s","","")
	
	link="../cliente/cli_dados.asp?dest=[destinatario_loja]"
	call define_link(1,link)
	call define_link(2,link)
	
	link="../cliente/cli_dados.asp?dest=[destinatario_cliente]"
	call define_link(3,link)
elseif ordem=12 then
	d="ITENS M�DIO POR PEDIDO"
	call define_coluna ("ID","superior_integracao","10","c","t","n","s","","")
	call define_coluna ("Patrocinador","superior_descricao","*","e","t","n","s","","")
	call define_coluna ("Pedidos","qtd","8","d","0","s","s","","")
	call define_coluna ("Qtd Itens","total_itens","8","d","0","s","s","","")
	call define_coluna ("M�dia de Itens","itens_medio_pedido","8","d","1","s","s","","")
	call define_coluna ("Vendas","vendas","8","d","2","s","s","","")
	
	link="../cliente/cli_dados.asp?dest=[superior_destinatario]"
	call define_link(1,link)
	call define_link(2,link)
	
	link2="fat_analiseglobal3.asp?codigo=" & codigo & "&ano=" & ano & "&ordem=" & ordem & "&col=" & col & "&patrocinador=[superior_integracao]&frete=" & frete
	call define_link(3,link2)
	call define_link(4,link2)
	call define_link(5,link2)	
	call define_link(6,link2)	
elseif ordem=13 then
	d="TOTAL DE ITENS"
	call define_coluna ("ID","superior_integracao","10","c","t","n","s","","")
	call define_coluna ("Patrocinador","superior_descricao","*","e","t","n","s","","")
	call define_coluna ("Pedidos","qtd","8","d","0","s","s","","")
	call define_coluna ("Qtd Itens","total_itens","8","d","0","s","s","","")
	call define_coluna ("M�dia de Itens","itens_medio_pedido","8","d","1","s","s","","")
	call define_coluna ("Vendas","vendas","8","d","2","s","s","","")
	
	link="../cliente/cli_dados.asp?dest=[superior_destinatario]"
	call define_link(1,link)
	call define_link(2,link)
	
	link2="fat_analiseglobal3.asp?codigo=" & codigo & "&ano=" & ano & "&ordem=" & ordem & "&col=" & col & "&patrocinador=[superior_integracao]&frete=" & frete
	call define_link(3,link2)
	call define_link(4,link2)
	call define_link(5,link2)	
	call define_link(6,link2)		
elseif ordem=16 then
	d="DISTRIBUI��O B�NUS"
	call define_coluna ("ID","integracao","8","c","t","n","n","","")
	call define_coluna ("Consultor","nome","*","e","t","n","n","","")
	call define_coluna ("UF","estado","5","c","t","n","s","","")
	call define_coluna ("B�nus","bonus","10","d","2","s","s","","")
	call define_coluna ("%","part","8","d","2","s","s","","")		
	
	link="../cliente/cli_dados.asp?dest=[destinatario]"
	call define_link(1,link)
	call define_link(2,link)
elseif ordem=17 then
	d="CONSULTORES IN�CIO"
	call define_coluna ("Estado","estado","*","e","t","n","s","","")
	call define_coluna ("Quantidade","c","10","d","0","s","s","","")
	
	link="fat_analiseglobal3.asp?codigo=" & codigo & "&ano=" & ano & "&ordem=" & ordem & "&col=" & col & "&estado=[estado]&frete=" & frete
	call define_link(1,link)
	call define_link(2,link)
elseif ordem=19 then
	d="CONSULTORES FINAL"
	call define_coluna ("Estado","estado","*","e","t","n","s","","")
	call define_coluna ("Quantidade","c","10","d","0","s","s","","")
	
	link="fat_analiseglobal3.asp?codigo=" & codigo & "&ano=" & ano & "&ordem=" & ordem & "&col=" & col & "&estado=[estado]&frete=" & frete
	call define_link(1,link)
	call define_link(2,link)				
elseif ordem=51 then
	d="MASTER"
	call define_coluna ("ID","superior_integracao","8","c","t","n","n","","")
	call define_coluna ("Patrocinador","superior_descricao","*","e","t","n","n","","")
	call define_coluna ("ID","integracao","8","c","t","n","n","","")
	call define_coluna ("Consultor","nome","*","e","t","n","n","","")
	call define_coluna ("","rede","10","c","t","n","s","","")

	link="../cliente/cli_dados.asp?dest=[superior_destinatario]"
	call define_link(1,link)
	call define_link(2,link)
	
	link="../cliente/cli_dados.asp?dest=[destinatario]"
	call define_link(3,link)
	call define_link(4,link)
	call define_link(5,"fat_Analiseglobal1.asp?ano=" & ano & "&frete=" & frete & "&codigo=[integracao]")
elseif ordem=52 then
	d="MASTER BRONZE"
	call define_coluna ("ID","superior_integracao","8","c","t","n","n","","")
	call define_coluna ("Patrocinador","superior_descricao","*","e","t","n","n","","")
	call define_coluna ("ID","integracao","8","c","t","n","n","","")
	call define_coluna ("Consultor","nome","*","e","t","n","n","","")
	call define_coluna ("","rede","10","c","t","n","s","","")

	link="../cliente/cli_dados.asp?dest=[superior_destinatario]"
	call define_link(1,link)
	call define_link(2,link)
	
	link="../cliente/cli_dados.asp?dest=[destinatario]"
	call define_link(3,link)
	call define_link(4,link)
	call define_link(5,"fat_Analiseglobal1.asp?ano=" & ano & "&frete=" & frete & "&codigo=[integracao]")
elseif ordem=53 then
	d="MASTER PRATA"
	call define_coluna ("ID","superior_integracao","8","c","t","n","n","","")
	call define_coluna ("Patrocinador","superior_descricao","*","e","t","n","n","","")
	call define_coluna ("ID","integracao","8","c","t","n","n","","")
	call define_coluna ("Consultor","nome","*","e","t","n","n","","")
	call define_coluna ("","rede","10","c","t","n","s","","")

	link="../cliente/cli_dados.asp?dest=[superior_destinatario]"
	call define_link(1,link)
	call define_link(2,link)
	
	link="../cliente/cli_dados.asp?dest=[destinatario]"
	call define_link(3,link)
	call define_link(4,link)
	call define_link(5,"fat_Analiseglobal1.asp?ano=" & ano & "&frete=" & frete & "&codigo=[integracao]")
elseif ordem=54 then
	d="MASTER OURO"
	call define_coluna ("ID","superior_integracao","8","c","t","n","n","","")
	call define_coluna ("Patrocinador","superior_descricao","*","e","t","n","n","","")
	call define_coluna ("ID","integracao","8","c","t","n","n","","")
	call define_coluna ("Consultor","nome","*","e","t","n","n","","")
	call define_coluna ("","rede","10","c","t","n","s","","")

	link="../cliente/cli_dados.asp?dest=[superior_destinatario]"
	call define_link(1,link)
	call define_link(2,link)
	
	link="../cliente/cli_dados.asp?dest=[destinatario]"
	call define_link(3,link)
	call define_link(4,link)
	call define_link(5,"fat_Analiseglobal1.asp?ano=" & ano & "&frete=" & frete & "&codigo=[integracao]")
elseif ordem=55 then
	d="MASTER PLATINA"
	call define_coluna ("ID","superior_integracao","8","c","t","n","n","","")
	call define_coluna ("Patrocinador","superior_descricao","*","e","t","n","n","","")
	call define_coluna ("ID","integracao","8","c","t","n","n","","")
	call define_coluna ("Consultor","nome","*","e","t","n","n","","")
	call define_coluna ("","rede","10","c","t","n","s","","")

	link="../cliente/cli_dados.asp?dest=[superior_destinatario]"
	call define_link(1,link)
	call define_link(2,link)
	
	link="../cliente/cli_dados.asp?dest=[destinatario]"
	call define_link(3,link)
	call define_link(4,link)	
	call define_link(5,"fat_Analiseglobal1.asp?ano=" & ano & "&frete=" & frete & "&codigo=[integracao]")
elseif ordem=56 then
	d="DIAMANTE"
	call define_coluna ("ID","superior_integracao","8","c","t","n","n","","")
	call define_coluna ("Patrocinador","superior_descricao","*","e","t","n","n","","")
	call define_coluna ("ID","integracao","8","c","t","n","n","","")
	call define_coluna ("Consultor","nome","*","e","t","n","n","","")
	call define_coluna ("","rede","10","c","t","n","s","","")

	link="../cliente/cli_dados.asp?dest=[superior_destinatario]"
	call define_link(1,link)
	call define_link(2,link)
	
	link="../cliente/cli_dados.asp?dest=[destinatario]"
	call define_link(3,link)
	call define_link(4,link)	
	call define_link(5,"fat_Analiseglobal1.asp?ano=" & ano & "&frete=" & frete & "&codigo=[integracao]")	
elseif ordem=57 then
	d="DUPLO DIAMANTE"
	call define_coluna ("ID","superior_integracao","8","c","t","n","n","","")
	call define_coluna ("Patrocinador","superior_descricao","*","e","t","n","n","","")
	call define_coluna ("ID","integracao","8","c","t","n","n","","")
	call define_coluna ("Consultor","nome","*","e","t","n","n","","")
	call define_coluna ("","rede","10","c","t","n","s","","")

	link="../cliente/cli_dados.asp?dest=[superior_destinatario]"
	call define_link(1,link)
	call define_link(2,link)
	
	link="../cliente/cli_dados.asp?dest=[destinatario]"
	call define_link(3,link)
	call define_link(4,link)	
	call define_link(5,"fat_Analiseglobal1.asp?ano=" & ano & "&frete=" & frete & "&codigo=[integracao]")
elseif ordem=58 then
	d="TRIPLO DIAMANTE"
	call define_coluna ("ID","superior_integracao","8","c","t","n","n","","")
	call define_coluna ("Patrocinador","superior_descricao","*","e","t","n","n","","")
	call define_coluna ("ID","integracao","8","c","t","n","n","","")
	call define_coluna ("Consultor","nome","*","e","t","n","n","","")
	call define_coluna ("","rede","10","c","t","n","s","","")

	link="../cliente/cli_dados.asp?dest=[superior_destinatario]"
	call define_link(1,link)
	call define_link(2,link)
	
	link="../cliente/cli_dados.asp?dest=[destinatario]"
	call define_link(3,link)
	call define_link(4,link)	
	call define_link(5,"fat_Analiseglobal1.asp?ano=" & ano & "&frete=" & frete & "&codigo=[integracao]")					
end if

impcab = 1
'impressao_tipo="P"

strsql="exec fat_analiseglobal_detalhe_sp " & ano & ",'" & codigo & "'," & col & "," & ordem & ",'" & frete & "'"
'call rw(strsql,0)
if len(trim(request("btn2"))) > 0 then call Redi_Arquivo(strSql,"")
set adoRs1=locConnVenda.execute(strSql)
if not adors1.eof then
	inicio=adors1("ini")
	fim	=adors1("fim")
end if

lbl1	=""
txt1	=""
lbl2	=""
txt2	=""
lbl3	="Per�odo/Ciclo:"
txt3	=inicio & " - " & fim
lbl4	=""
txt4	=""
lbl5	="Relat�rio:"
txt5	="<b>" & ordem & " - " & d & "</b>"

strParametrotab1="<tr><td colspan=15><table border='0' cellpadding=0 cellspacing=0 width='100%'>" & _
	"	<tr><td width='10%' bgcolor='#c0c0c0'><font face='verdana' size='1'>ID:</font></td>" & _
	"		<td width='10%'><font face='verdana' size='1'><b>" & codigo & "</b></font></td>" & _
	"		<td width='10%' bgcolor='#c0c0c0'><font face='verdana' size='1'>Consultor:</font></td>" & _
	"		<td width='40%'><font face='verdana' size='1'><b>" & nome & "</b></font></td>" & _
	"		<td width='10%' bgcolor='#c0c0c0'><font face='verdana' size='1'>Qualifica��o:</font></td>" & _
	"		<td width='20%'><font face='verdana' size='1'><b>" & qualificacao & "</b></font></td></tr>" & _
	"</table></td></tr>"

call fun_rel_cab()
do while not adoRs1.eof
	inttotal=inttotal+1
	call fun_rel_det()
	adoRs1.movenext
loop
if ordem=5 then
	if cdbl(tg(5,9))>0 then
		tg(7,9)=cdbl(tg(6,9)) / cdbl(tg(5,9))
	else
		tg(7,9)=0
	end if
elseif ordem=12 or ordem=13 then
	if cdbl(tg(3,9))>0 then
		tg(5,9)=cdbl(tg(4,9)) / cdbl(tg(3,9))
	else
		tg(5,9)=0
	end if
end if

call fun_fim_rel()
call Func_ImpPag(1,iColuna)
%>
</table></td></tr></table>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->