<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%call Verifica_Permissao(88)%>
<%
strStatus	=request("status")
strObs		=replace(replace(request("obs"),",","."),"'"," ")
strPedido	=request("pedido")
strStatusAtual=request("statusAtual")
strFilial=request("filial")

strEmitente_TEMP="00052920"

'verificar se o usu�rio tem permissao para trocar o status
'--------------------------------------------------------------
strSql=	"select t=count(0) from sys_UsuarioMovCStatus " & _
		"where usuario='" & session("usuario") & "' and stnfCodigo='" & strStatusAtual & "'"
set adoVerificaS=locConnVenda.execute(strSql)
if not adoVerificaS.eof then
	if adoVerificaS("t")=0 then
	'Response.Write "OK"
	'Response.End
		call Redi_Aviso("Acesso Negado","Sem permiss�o para alterar esse status.")
		Response.End
	end if
end if
set adoVerificaS=nothing

'' verificar qual tabela est� o pedido
strsql="select pedicodigo from ped_Pedidototal where emitente='" & strFilial & "' and pedicodigo=" & strPedido
set adoa=locconnvenda.execute(strsql)
if adoa.eof then
	tabela_pedido_total			="ped_PedidoTotal_Hist"
	tabela_pedido_item			="ped_PedidoItem_Hist"
	tabela_fat_saida			="fat_saida_Hist"
	tabela_ped_Movimento_status	="ped_MovimentoStatus_Hist"
	tabela_ped_Observacao		="ped_Observacao_Hist"
else
	tabela_pedido_total			="ped_PedidoTotal"
	tabela_pedido_item			="ped_PedidoItem"
	tabela_fat_saida			="fat_saida"
	tabela_ped_Movimento_status	="ped_MovimentoStatus"
	tabela_ped_Observacao		="ped_Observacao"
end if
set adoa=nothing
''

'Verificar se j� foi emitido nf. Caso positivo, nao pode alterar.
'-------------------------------------------------------------------

''' ALTERADO EM 16/01/07
'if strStatusAtual=1 then ' and session("usuario")<>"KLEBER" then
'	strSql="select nota,serie from " & tabela_fat_saida & " where pedido='" & strPedido & "' and situacao=0 and emitente='" & strFilial & "'"
'	set adoV=locConnVenda.execute(strSql)
'	if not adoV.eof then		
'		call Redi_Aviso("Acesso Negado","Pedido com nota fiscal emitida (" & adoV("nota") & "-" & adoV("serie") & "), n�o pode ser alterado.")					
'	end if
'	adoV.close
'end if
''' -------------------------------------------------

'' s� pode alterar para processado quando opera��o = requisi��o, reenvio, devolu��o
'if strStatus=1 then
'	strsql= "select s=isnull(status_baixa_estoque,0) " & _
'			"from " & tabela_pedido_total & " a inner join ped_StatusNf b on a.stnfcodigo=b.stnfcodigo " & _
'			"where emitente='" & strFilial & "' and pedicodigo=" & strPedido
	'Response.Write strsql
	'Response.end
'	set adoA=locConnVenda.execute(strsql)
'	if adoA.eof then
'		call Redi_Aviso("Acesso Negado","Status do pedido n�o pode ser alterado para Processado. Status Atual n�o permitido")
'	else
'		if cstr(adoa("s"))="0" then 
'			call Redi_Aviso("Acesso Negado","Status do pedido n�o pode ser alterado para Processado. Status Atual n�o permitido")
'		end if
'	end if
'	set adoA=nothing
'end if
''

'quando for para trocar o status para gerencia comercial,
'sendo cartao,alterar para "gerencia comercial cartao".
'--------------------------------------------------------------
if strStatus=30 then
	strSql= "select Tot=count(0) " & _
			"from " & tabela_pedido_total & " a inner join fat_tipo_cobranca b on a.tipocobranca=b.tipo " & _
			"where pediCodigo='" & strPedido & "' and emitente='" & strFilial & "' and Tipo_Cartao='S'"
	set adob=locConnVenda.execute(strSql)
	if not adob.eof then
		if adoB("tot")>0 then
			strStatus="29"
			strObs=strObs & "<br>Alterado para An�lise Ger�ncia (Cart�o) pelo sistema."
		end if
	end if
	adob.close
end if

'' quando operacao de baixa de estoque, e alterado para processado ou Agrupa NF
'' efetuar baixa no estoque

situacao_estoque=0
strsql	="select s=isnull(status_baixa_estoque,0) from ped_StatusNf where stnfcodigo=" & strStatusAtual
set adoa=locConnVenda.execute(strsql)
if not adoa.eof then situacao_estoque=adoa("s")
set adoa=nothing

if cstr(situacao_estoque)="1" and (strStatus=1 or strStatus=41) then
	''verificar estoque
	strSql=	"select estoque=estoque_sem_box-qtd,c.descricao " & _
			"from " & tabela_pedido_item & " a inner join view_Estoque_Atual b on a.prodcodigo=b.produto " & _
			"inner join sys_produto c on a.prodCodigo=c.produto " & _
			"where b.emitente='" & strEmitente_TEMP & "' and pedicodigo='" & strPedido & "' " & _
			" and (estoque_sem_box-qtd)<0"			
	set adoX=locConnVenda.execute(strSql)
	if not adoX.eof then
		strStatus=40
		do while not adoX.eof
			strObs=strObs & "<b>Sem Estoque:</b> " & adoX("descricao") & "<br>"
			adoX.movenext
		loop
	else
		'' inserir baixa no estoque.
		
		strsql=	"select count(0) c from est_movimento " & _
				"where local_estoque='" & strEmitente_TEMP & "' and documento=" & strpedido & _
				" and operacao=22 and observacao='Lan�ado ap�s alterar status.'"
		set adors4=locconnvenda.execute(strsql)
		if not adors4.eof then
			if adors4("c")>0 then
				strObs=strObs & "Estoque n�o baixado. Baixa j� efetuada anteriormente."
			else	
				strSql="select max(id_movimento) cod from est_movimento"
				set adoA=locConnVenda.execute(strSql)
				if not adoA.eof then novo_cod=cdbl(adoA("cod"))
				set adoA=nothing
		
				strSql=	"select emitente,prodcodigo,qtd,tipooperacao " & _
						"from " & tabela_pedido_item & " " & _
						"where emitente='" & strFilial & "' and pedicodigo='" & strPedido & "'"
				set adoA=locConnVenda.execute(strSql)
				do while not adoA.eof						
					novo_cod=novo_cod+1	
					
					'' pula trigger
					strsql= "update sys_pulatrigger set est_movimento='S'" & _
							",usuario='" & session("usuario") & "',data=getdate(),obs='ped_altStatus - est_movimento=s'"
					locConnVenda.execute(strsql)
					
						strSql	="insert into est_movimento (id_movimento,local_estoque,produto,data,operacao,quantidade,usuario,documento,observacao) values " & _
								 "(" & novo_cod & ",'" & strEmitente_TEMP & "','" & adoA("prodCodigo") & "',convert(varchar(10),getdate(),120),22," & adoA("qtd") & ",'" & session("usuario") & "','" & strPedido & "','Lan�ado ap�s alterar status.')"
						locConnVenda.execute(strSql)
					
					'' fim pula trigger
					strsql= "update sys_pulatrigger set est_movimento='N'" & _
							",usuario='" & session("usuario") & "',data=getdate(),obs='ped_altStatus - est_movimento=n'"
					locConnVenda.execute(strsql)
					
					'' inserir box sugest�o
					qtdMovimento	=cdbl(adoA("qtd"))
					qtdBox			=0			
					
					strsql=	"select box,qtd=sum(entrada-saida) from view_estoque_saldo_box " & _
							"where local_estoque='" & strEmitente_TEMP & "' and produto='" & adoA("prodCodigo") & "' and box<>0 " & _
							"group by box,ordem_box order by ordem_box"
					set adoBox=locConnVenda.execute(strsql)
					
					do while not adoBox.eof
						qtdBox=cdbl(adoBox("qtd"))
						if adoA("tipooperacao")=41 then '' quando for reparte - box 40000
							box_padrao="40000"
						else
							box_padrao=adoBox("box")
						end if	
							
						if qtdMovimento<=qtdBox then
							strsql=	"insert into est_movimento_box (id_movimento,box,quantidade) values " & _
									"(" & novo_cod & "," & box_padrao & "," & qtdMovimento & ")"
							locConnVenda.execute(strsql)
							qtdMovimento=0
							exit do
						else
							strsql=	"insert into est_movimento_box (id_movimento,box,quantidade) values " & _
									"(" & novo_cod & "," & box_padrao & "," & qtdBox & ")"
							locConnVenda.execute(strsql)
							qtdMovimento=qtdMovimento-qtdBox
						end if
							
						adoBox.movenext
					loop
					set adoBox=nothing
						
				'	if cdbl(qtdMovimento)>0 then
				'		'' faltou saldo para inserir box, insere com box 0
				'		strsql=	"insert into est_movimento_box (id_movimento,box,quantidade) values " & _
				'						"(" & novo_cod & ",0," & qtdMovimento & ")"
				'		locConnVenda.execute(strsql)
						
				'		strObs="Falta de saldo de box para fazer baixa. Colocado � classificar.<br>"
				'	end if
					'' 
					adoA.movenext
				loop
				set adoA=nothing	
		
				strObs=strObs & "Efetuado baixa no estoque dos itens do pedido"
			end if
		end if
	'' FIM
	end if
	adoX.close	
end if

''
strSql="insert into " & tabela_ped_Movimento_status & " " & _
	   "(pediCodigo,emitente,stnfCodigo,usuario,DataMovimento,Obs) " & _
	   "values " & _
	   "(" & strPedido & ",'" & strFilial & "','" & strStatus & "','" & session("usuario") & "',getdate(),'" & strObs & "')"
'Response.Write strsql
'Response.end	   
locConnVenda.execute(strSql)

strSql= "update " & tabela_pedido_total & " set " & _
		"stnfCodigo=" & strStatus & _
		" where pediCodigo=" & strPedido & " and emitente='" & strFilial & "'"
		'Response.Write strSql
		'Response.End
locConnVenda.execute(strSql)

'quando alterar status para liberar pedido, liberar produtos pendente por estoque...
if strStatus="10" or strStatus="2" then
	strSql=	"update " & tabela_pedido_item & " set " & _
			"faltaEstoque=0 " & _
			"where pediCodigo=" & strPedido & " and emitente='" & strFilial & "'"
	locConnVenda.execute(strSql)
end if

if strStatus=99 then 'cancelar titulo
	strSql="update cob_Titulos set situacao=9 where emitente='" & strFilial & "' and pedido='" & strPedido & "'"
	locConnVenda.execute strSql	
elseif strStatus=10 then 'ativar titulo
	strSql="update cob_Titulos set situacao=1 where emitente='" & strFilial & "' and pedido='" & strPedido & "'"
	locConnVenda.execute strSql
end if

strDestino="ped_Status.asp?P=" & strPedido & "&Filial=" & strFilial
Response.Redirect strDestino
%>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->