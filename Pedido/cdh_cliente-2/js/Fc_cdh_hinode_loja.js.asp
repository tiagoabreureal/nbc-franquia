﻿<% 'if request.servervariables("HTTP_REFERER")="http://cdh.hinode.com.br/pedido/cdh_hinode/mw-gera-pedido-loja-cdh.asp" then %>
$(document).ready(function(){
			
if($("#id_cons").val()=="" || parseInt($("#id_cons").val(),10)==0){
	
	// REDIRECIONA SE NÃO TIVER LOGADO
	window.parent.location.href = "../../index.asp";
}
			
		/* LISTA CARRINHO */
		if($("#id_cons").val()!=""){
			$("#list_prod").html("");
			Fc_lista_cat_subcat(0,'lista_cat_subcat');
			Fc_car_lista_hist();
			Fc_lista_end_cons();
			Fc_credito_cdh();
			
		}
		
		
$("#list_cat").delegate("a","click", function(){
	var vidcat = parseInt(this.href.split('#').pop());
	$("#list_prod").html("");
	$("#txt_prod").val("");
    $("#txt_qtd").val("");
  	Fc_lista_cat_subcat(vidcat,'lista_cat_subcat');
});

$("#list_sub_cat").delegate("a","click", function(){
  var vidsubcat = parseInt(this.href.split('#').pop());
	$("#txt_prod").val("");
    $("#txt_qtd").val("");
		
		//alert(vidsubcat);
		Fc_lista_cat_subcat(vidsubcat,'lista_prod_subcat');
});

$("#list_prod").delegate("a","click", function(){
  var vidprod = parseInt(this.href.split('#').pop());
  $("#txt_prod").val(vidprod);
  $("#txt_qtd").val('1');
  //$("#coluna-descricao").show();
  //$("#categorias").hide();		
		
});


		
		$('input').keydown(function(e) { 

	var tecla = (e.keyCode?e.keyCode:e.which);

       if (tecla == 13) { 
	   
	        vloc_prod = $("#txt_prod").val().preencherEsq(0,6);
			vtxt_qtd = $("#txt_qtd").val();
			
			if(vtxt_qtd>0 && vloc_prod!=''){
			Fc_additemcar(vloc_prod,vtxt_qtd)
			}else if(vloc_prod!='' && vtxt_qtd==''){
				$("#txt_qtd").focus();
			}else{
			$('#retorno').html('Favor informe uma quantidade maior que zero para o item.');
			}
	   
	   e.preventDefault();
	   }     
	});

		
		
		
		/* Verifica o modo de entrega */
		if($("#modo_entrega").val()==1){
		 vend_entr = $("#endereco").val()+" - CEP: "+$("#cep").val();
	     vend_entr += "<BR />"+$("#numero").val()+" - "+$("#complemento").val();
         vend_entr += "<BR />"+$("#bairro").val()+" - "+$("#cidade").val();
         vend_entr += "<BR />"+$("#estado").val();
		 $("#mod_entr").html(vend_entr);
		}else if($("#modo_entrega").val()==2){
		 vcdh_desc = $("#id_cdh_retira_desc").val();	
		 $("#mod_entr").html(vcdh_desc);
		}
			
			
				
		/*botao tipo ao se clicado carrega os sub-tipo*/
		$("#tipo").click(function ( event ) {
		event.preventDefault();		
		$("#sub-tipo").show();		
		});	
		
		/*botao sub-tipo ao se clicado carrega os produtos*/		
		$("#sub-tipo").click(function ( event ) {
		event.preventDefault();
		$("#produto").show();
		});
		
		/*botao produto ao se clicado carrega a descrição do produto e esconte as categorias*/
		$("#produto").click(function ( event ) {
		event.preventDefault();
		$("#categorias").hide();
		$("#coluna-descricao").show();
		});
		
		/*botao voltar produto ao se clicado carrega as categorias e esconte descrição do produto*/
		$("#voltar-produtos").click(function ( event ) {
		event.preventDefault();
		$("#coluna-descricao").hide();
		$("#categorias").show();			
	        });	
				
				
		/*botao comprar ao se clicado carrega os produtos no carrinho de compras*/
		$("#bt_add_item").click(function ( event ) {
		 
		    vloc_prod = $("#txt_prod").val().preencherEsq(0,6);
			vtxt_qtd = $("#txt_qtd").val();
			
			if(vtxt_qtd>0 && vloc_prod!=''){
			Fc_additemcar(vloc_prod,vtxt_qtd)
			}else{
			$('#retorno').html('Favor informe uma quantidade maior que zero para o item.');
			}
		});
		
		
		$("#bt_continuar").click(function ( event ) {
			
			var vmodo_entrega = $("#modo_entrega").val();
			var vnome_cons = $("#nome_cons").val();
			var vid_cons = $("#id_cons").val();
			
			var vcep = $("#cep").val();
			var vendereco = $("#endereco").val();
			var vnumero = $("#numero").val();
			var vcomplemento = $("#complemento").val();
			var vbairro = $("#bairro").val();
			var vcidade = $("#cidade").val();
			var vestado = $("#estado").val();
			
			var vss_pg = $("#ss_pg").val();
			var vid_cdh_retira = $("#id_cdh_retira").val();
			var vid_cdh_retira_desc = $("#id_cdh_retira_desc").val();
			var vvl_total_pedido = $("#vl_total_pedido").val();
			var vpontos_total_pedido = $("#pontos_total_pedido").val();
			var vpeso_total_pedido = $("#peso_total_pedido").val();
			var vvl_sub_total_pedido = $("#vl_sub_total_pedido").val();
			var vvl_total_pedido_frete = $("#vl_total_pedido_frete").val();
			var vforma_envio_transp = $("#forma_envio_transp_ped").val();
			var vforma_envio_transp_desc_ped = $("#forma_envio_transp_desc_ped").val();
			
			var vprazo_transp_ped = $("#prazo_transp_ped").val();
			var vvl_credito_usado = $("#vl_credito_usado").val();
			var vemail_ped = $("#email_ped").val();
			
			
		if(vforma_envio_transp==""){
				alert("Favor selecione uma forma de envio / recebimento!");
		}else if((vvl_total_pedido=='0.00' && vvl_credito_usado=='0.00') || (vvl_total_pedido=='' && vvl_credito_usado=='') ){
				alert("Favor adicione um produto no carrinho!");
		}else{
			
			
				
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			$.ajax({
        		url : 'ajax/ajax_cdh_hinode_gera_pedido.asp',
        		type :'POST',
				data : {acao:'gera_pedido',idconsultor:vid_cons,ss_pg:vss_pg,modo_entrega:vmodo_entrega,nome_cons:vnome_cons,id_cdhret:vid_cdh_retira,id_cdhret_desc:vid_cdh_retira_desc,
cep_entr:vcep,endereco:vendereco,numero:vnumero,compl:vcomplemento,bairro:vbairro,cidade:vcidade,estado:vestado,id_forma_pag:18,desc_forma_pag:'FATURAR',vl_frete:vvl_total_pedido_frete,vl_ped:vvl_total_pedido,vl_sub_ped:vvl_sub_total_pedido,
pontos_ped:vpontos_total_pedido,peso_ped:vpeso_total_pedido,prazo_transp_ped:vprazo_transp_ped,forma_envio_transp_ped:vforma_envio_transp,qtd_parcela_card:1,status_ped:1,forma_envio_transp_desc_ped:vforma_envio_transp_desc_ped,vl_credito_usado:vvl_credito_usado,ped_tipo:'CDH>HINODE',email_ped:vemail_ped}, 
				dataType:"text",
				beforeSend : function(){
            		$('#carregando').show(100);
					$('#bt_continuar').hide(100);
        		},
        		complete : function(){
            		$('#carregando').hide(100);
					$('#bt_continuar').hide(100);
        		},
        		success : function(retorno){
					
					var rs = retorno.split('|');
						
					    if(parseInt(rs[0])==0){
							$("#retorno").html(rs[1]);
                            $('#bt_continuar').show(100);
						}else{
							
window.parent.$.fancybox.open({href:"https://cdh.hinode.com.br/Pedido/cdh_hinode/mw-gera-pedido-finalizado-cdh.asp?id_pedido_oficial="+rs[0],
								type : 'iframe',
								width : '100%', 
                    			height: '400'
							});
			
							
						}
						
				},
				error : function(retorno){
					if ( retorno.status == 404 ){
        				$('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
    				}else{
      					$('#retorno').html('Houve um erro desconhecido entre em contato com o administrador...');
        			}
				}
			})
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			
			
			
			
		  }
		
		});
		
		
		
		
		});
		
		
		
		function Fc_car_lista_hist(){
			
			var vid_cons = document.getElementById("id_cons").value;
            var vid_cdh_retira = document.getElementById("id_cdh_retira").value;
			
			
		//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			$.ajax({
        		url : 'ajax/ajax_cdh_hinode_gera_pedido.asp',
        		type :'POST',
				data : {acao:'car_lista_hist',idconsultor:vid_cons,id_cdhret:vid_cdh_retira}, 
				dataType:"text",
				beforeSend : function(){
            		$('#carregando').show(100);
        		},
        		complete : function(){
            		$('#carregando').hide(100);
        		},
        		success : function(retorno){
					var rs = retorno.split('|');
						if(parseInt(rs[0])>0){
							$("#ss_pg").val(rs[1]);
							Fc_listaitemcar();
						}
					
				},
				error : function(retorno){
					 //console.log(retorno);
					if ( retorno.status == 404 ){
        				$('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
    				}else{
      					$('#retorno').html('Houve um erro desconhecido entre em contato com o administrador...');
        			}
				}
			})
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		}
		
		
		
		
		
		
		function Fc_listaitemcar(){
			
			var vss_pg = document.getElementById("ss_pg").value;
			var vid_cons = document.getElementById("id_cons").value;
			var vvl_total_pedido_frete = document.getElementById("vl_total_pedido_frete").value;
            var vid_cdh_retira = document.getElementById("id_cdh_retira").value;
			$("#forma_envio_transp_ped").val("");
			$("#forma_envio_transp_desc_ped").val("");
			$("#prazo_transp_ped").val("");
			
			
		//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			$.ajax({
        		url : 'ajax/ajax_cdh_hinode_gera_pedido.asp',
        		type :'POST',
				data : {acao:'car_lista_item',idconsultor:vid_cons,ss_pg:vss_pg,id_cdhret:vid_cdh_retira}, 
				dataType:"json",
				beforeSend : function(){
            		$('#carregando').show(100);
        		},
        		complete : function(){
            		$('#carregando').hide(100);
        		},
        		success : function(retorno){
					
					//console.log(retorno);
					
					var meusItens = '';
					var valor_total_item = 0;
					var pontos_total_item = 0;
					var peso_total_item = 0;
					
					var valor_total_pedido_calc = 0;
					var valor_total_pedido = 0;
					var pontos_total_pedido = 0;
					var peso_total_pedido = 0;
					var encomendar_item = '';
					
					if(retorno.length > 0 ){
						
						
						
					$.each(retorno, function (i, item) {
					
					valor_total_item = eval(item.Car_prod_qtd*item.Car_prod_valor_unt);	
					valor_total_pedido += valor_total_item;
					
					pontos_total_item = eval(item.Car_prod_qtd*item.Car_prod_pontuacao);
					pontos_total_pedido += pontos_total_item;
					
					if(item.Car_prod_peso!=''){	
					peso_total_item = eval(item.Car_prod_qtd*item.Car_prod_peso);	
					peso_total_pedido += peso_total_item;
					}
					
					if(item.Car_prod_qtd_encomendar>0){
						encomendar_item = '</br><span class="texto_vermelho">Item sob encomenda, quantidade encomendar:</span>'+item.Car_prod_qtd_encomendar;
					}else{
					encomendar_item = '';
					}
					
					//AVISO//
if(parseInt(item.Car_prod_codigo)==parseInt($("#txt_prod").val(),10) && parseInt(item.Car_prod_qtd,10) < parseInt($("#txt_qtd").val(),10)){
						alert("ATENÇÃO: QUANTIDADE SOLICITADA INDISPONÍVEL, QUANTIDADE DISPONÍVEL "+item.Car_prod_qtd);
						$("#txt_prod").val("");
						$("#txt_qtd").val("");
}
					
		vnome_prod = item.Car_prod_nome.replace(new RegExp( "\\n", "g" ),"").replace(new RegExp( "\\<br>", "g" )," ")
						
					meusItens += '<li style="height:auto; padding-bottom:5px; padding-top:5px;">';
						//meusItens += '<div class="item">'+item.Car_idProduto+'</div>';
						meusItens += '<div class="codigo">'+item.Car_prod_codigo+'</div>'
                    	meusItens += '<div class="produto">'+vnome_prod+'</div>'
                    	meusItens += '<div class="quant">'+item.Car_prod_qtd+'</div>'
						meusItens += '<div class="quant-add" style="height: auto;">'
  meusItens += '<div style="float:left">'
  meusItens += '<input type="button" class="botao-adiciona"' 
  meusItens += ' onClick="Fc_upditemcar(\''+ item.Car_prod_codigo +'\',\''+item.CdCarrinho+'\',\''+item.Car_prod_qtd+'\',\'Add\');"/>'
  meusItens += '</div>'
  meusItens += '<div style="float:left">'
  meusItens += '<input type="button" class="botao-subtrai"'
  meusItens += ' onClick="Fc_upditemcar(\''+ item.Car_prod_codigo +'\',\''+item.CdCarrinho+'\',\''+item.Car_prod_qtd+'\',\'Remov\');"/>'
  meusItens += '</div>'
                        meusItens += '</div>'
						meusItens += '<div class="valor">R$ '+number_format(item.Car_prod_valor_unt,2,',','.')+'</div>'
                    	meusItens += '<div class="valor-total">R$ '+number_format(valor_total_item,2,',','.')+'</div>'
						meusItens += '<div class="desconto">&nbsp;</div>' //number_format(pontos_total_item,2,',','.')
                    	meusItens += '<div class="remover">'
                        meusItens += '<input name="del" type="button" height="10" width="10" class="botao-delete"'
meusItens += ' onClick="Fc_delitemcar(\''+ item.Car_prod_codigo +'\',\''+item.CdCarrinho+'\');"/>'
                        meusItens += '</div>'
					meusItens += '</li>'					
										
					})
					
					$('#carrinho_lista').show();
					$('#carrinho_lista').html('<ul>' + meusItens + '</ul>');
					var vl_credito = $("#vl_credito").val()==""?"0.00":$("#vl_credito").val();
					var valor_total_pedido_cred = 0.00;
							
					if(vl_credito>=valor_total_pedido){
						valor_total_pedido_cred = 0.00;
						//$("#vl_credito_usado").val(number_format(valor_total_pedido,2,'.',''));					
					}else{
						valor_total_pedido_cred = valor_total_pedido - eval(number_format(vl_credito,2,'.',''));
						//$("#vl_credito_usado").val(number_format(vl_credito,2,'.',''));
					}
					
	$('#txt_vl_total_pedido').html('Pedido Valor Total: R$ '+number_format(valor_total_pedido_cred,2,',','.'));
	$('#vl_total_pedido').val(number_format(valor_total_pedido_cred,2,'.',''));
	$('#vl_sub_total_pedido').val(number_format(valor_total_pedido,2,'.',''));
	$('#txt_pontos_total_pedido').html('Total em Pontos: '+number_format(pontos_total_pedido,2,',','.'));
	$('#pontos_total_pedido').val(number_format(pontos_total_pedido,2,'.',''));
	$('#peso_total_pedido').val(number_format(peso_total_pedido,3));
	
	    /* LISTA TRANSP E CALCULA FRETE */
		//if($("#modo_entrega").val()==2){
			Fc_calc_frete_list_transp();
		//}
		
					
				 }else{
					 
					 $('#carrinho_lista').hide();
					 $('#txt_vl_total_pedido').html('Pedido Valor Total: R$ 0,00');
					 $('#vl_total_pedido').val('0.00');
					 $('#vl_sub_total_pedido').val('0.00');
					 $('#txt_pontos_total_pedido').html('Total em Pontos: 0,00');
					 $('#pontos_total_pedido').val('0.00');
					 $('#peso_total_pedido').val('0.000');
 
				 }
					
				},
				error : function(retorno){
					 //console.log(retorno);
					if ( retorno.status == 404 ){
        				$('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
    				}else{
      					$('#retorno').html('Houve um erro desconhecido entre em contato com o administrador...');
        			}
				}
			})
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		}
		
		
		
		function Fc_calc_frete_list_transp(){
		
		var vss_pg = document.getElementById("ss_pg").value;
		var vpeso_ped = document.getElementById("peso_total_pedido").value;
		var vcep_entr = document.getElementById("cep").value;
		var vid_cdhret = document.getElementById("id_cdh_retira").value;
		
		//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			$.ajax({
        		url : 'ajax/ajax_cdh_hinode_gera_pedido.asp',
        		type :'POST',
		data : {acao:'calc_frete_list_transp',ss_pg:vss_pg,peso_ped:vpeso_ped,cep_entr:vcep_entr,id_cdhret:vid_cdhret}, 
				dataType:"json",
				beforeSend : function(){
            		$('#carregando').show(100);
        		},
        		complete : function(){
            		$('#carregando').hide(100);
        		},
        		success : function(retorno){
					
					//console.log(retorno);
					var vmostra_forma_envio = "";
					var vPrVl = "";
					var vPrVl_0 = "";
					var cont_trans = 0;
					var chk = "";
					
					var vtrans = "";
					var vprazo_trans = "";
					var vvalor_trans = "";
					var vdescricao_trans = "";
						
					if(retorno.length > 0 ) {
						
						$.each(retorno, function (i, item) {
						
					if(parseInt(cont_trans)==0){   
						chk = 'checked="checked"';
						vtrans = item.transporte;
						vprazo_trans = item.prazo;
						vvalor_trans = number_format(item.valor,2,'.','');
						vdescricao_trans = item.descricao;  
					}else{
						chk = '';
					}
						
  vPrVl = '\''+item.transporte+'\',\''+item.prazo+'\',\''+number_format(item.valor,2,'.','')+'\',\''+cont_trans+'\',\''+item.descricao+'\'';
						
vmostra_forma_envio += '<div style="float:left;">';
vmostra_forma_envio += '<input type="radio" id="forma_envio_transp"'+cont_trans+'" '; 
vmostra_forma_envio += ' name="forma_envio_transp" value="'+item.transporte+'"';
vmostra_forma_envio += ' onClick="Fc_forma_envio('+vPrVl+');" '+chk+'/>'+item.descricao+' - R$ '+number_format(item.valor,2,',','.') ;
vmostra_forma_envio += '</div>';
						cont_trans ++;
											
						})
						
						$("#mostra_forma_envio").html(vmostra_forma_envio);
						Fc_forma_envio(vtrans,vprazo_trans,vvalor_trans,0,vdescricao_trans);
					}else{
				    	
vPrVl = '\'6\',\'0\',\'0.00\',\'0\',\'FABRICA HINODE ALPHAVILLE (Grátis)\'';
						
vmostra_forma_envio += '<div style="float:left;">';
vmostra_forma_envio += '<input type="radio" id="forma_envio_transp0"'; 
vmostra_forma_envio += ' name="forma_envio_transp" value="6"';
vmostra_forma_envio += ' onClick="Fc_forma_envio('+vPrVl+');" checked="checked"/>FÁBRICA HINODE ALPHAVILLE (Grátis) - R$ 0,00';
vmostra_forma_envio += '</div>';
						
						$("#mostra_forma_envio").html(vmostra_forma_envio);
						Fc_forma_envio('6','0','0.00','0','FÁBRICA HINODE ALPHAVILLE (Grátis)  - R$ 0,00');					
					}
					
				},
				error : function(retorno){
					 //console.log(retorno);
					if ( retorno.status == 404 ){
        				$('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
    				}else{
      					$('#retorno').html('Houve um erro desconhecido entre em contato com o administrador...');
        			}
				}
			})
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		
		}
		
		
		function Fc_additemcar(vloc_prod,vtxt_qtd){
			
			vss_pg = document.getElementById("ss_pg").value;
			vid_cons = document.getElementById("id_cons").value;
			vid_cdhret = document.getElementById("id_cdh_retira").value;
			
		//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	$.ajax({
    url : 'ajax/ajax_cdh_hinode_gera_pedido.asp',
    type :'POST',
	data : {acao:'car_add_item',idconsultor:vid_cons,loc_prod:vloc_prod,qtd_prod:vtxt_qtd,ss_pg:vss_pg,id_cdhret:vid_cdhret,regra_estoque:0}, 
				dataType:"text",
				beforeSend : function(){
            		$('#carregando').show(100);
        		},
        		complete : function(){
            		$('#carregando').hide(100);
        		},
        		success : function(retorno){
					var rs = retorno.split('|');
					
						if(parseInt(rs[0])==0){
							$("#retorno").html(rs[1]);
						}else{
							$("#retorno").html("");
							$("#txt_prod").focus();
							$("#txt_prod").select();
							$("#txt_qtd").val("");
							Fc_listaitemcar();
						}
					
				},
				error : function(retorno){
					 console.log(retorno);
					if ( retorno.status == 404 ){
        				$('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
    				}else{
      					$('#retorno').html('Houve um erro desconhecido entre em contato com o administrador!');
        			}
				}
			})
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		}
		
		
		
		function Fc_delitemcar(vloc_prod,vid_car){
			
			vss_pg = document.getElementById("ss_pg").value;
			vid_cons = document.getElementById("id_cons").value;
			
			if(confirm('DESEJA REALMENTE DELETAR O ITEM '+ vloc_prod +' DO CARRINHO?')){
			
		//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			$.ajax({
        		url : 'ajax/ajax_cdh_hinode_gera_pedido.asp',
        		type :'POST',
				data : {acao:'car_del_item',idconsultor:vid_cons,loc_prod:vloc_prod,id_car:vid_car,ss_pg:vss_pg}, 
				dataType:"text",
				beforeSend : function(){
            		$('#carregando').show(100);
        		},
        		complete : function(){
            		$('#carregando').hide(100);
        		},
        		success : function(retorno){
					var rs = retorno.split('|');
						if(parseInt(rs[0])==0){
							$("#retorno").html(rs[1]);
						}else{
							Fc_listaitemcar();
						}
					
				},
				error : function(retorno){
					 //console.log(retorno);
					if ( retorno.status == 404 ){
        				$('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
    				}else{
      					$('#retorno').html('Houve um erro desconhecido entre em contato com o administrador...');
        			}
				}
			})
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			}
		}
		
		
		
		function Fc_upditemcar(vloc_prod,vid_car,vqtd_prod,tipo){
			
			var vss_pg = document.getElementById("ss_pg").value;
			var vid_cons = document.getElementById("id_cons").value;
			var vqtd_upd = 0;
			var vid_cdhret = document.getElementById("id_cdh_retira").value;
			
			$("#txt_prod").val("");
			$("#txt_qtd").val("");

			if(eval(parseInt(vqtd_prod) - 1)<=0 && tipo=='Remov'){
				$("#retorno").html('Não é possível atualizar o item para zero!');
			}else if(tipo=='Remov'){
				$("#retorno").html("");
				vqtd_upd = parseInt(vqtd_prod) - 1;
			}else{
				$("#retorno").html("");
				vqtd_upd = parseInt(vqtd_prod) + 1;
			}
			
			if(vqtd_upd>0){
		//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			$.ajax({
        		url : 'ajax/ajax_cdh_hinode_gera_pedido.asp',
        		type :'POST',
		data : {acao:'car_add_item',idconsultor:vid_cons,loc_prod:vloc_prod,id_car:vid_car,qtd_prod:vqtd_upd,ss_pg:vss_pg,id_cdhret:vid_cdhret,regra_estoque:0}, 
				dataType:"text",
				beforeSend : function(){
            		$('#carregando').show(100);
        		},
        		complete : function(){
            		$('#carregando').hide(100);
        		},
        		success : function(retorno){
					var rs = retorno.split('|');
						if(parseInt(rs[0])==0){
							$("#retorno").html(rs[1]);
						}else{
							Fc_listaitemcar();
						}
					
				},
				error : function(retorno){
					 //console.log(retorno);
					if ( retorno.status == 404 ){
        				$('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
    				}else{
      					$('#retorno').html('Houve um erro desconhecido entre em contato com o administrador...');
        			}
				}
			})
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			}
			
		}
		
				
function Fc_forma_envio(vtransp,vprazo,vvalorfrete,vcont,vtransp_desc){
	
	$("#forma_envio_transp"+vcont).attr("checked","checked");
	$("#forma_envio_transp_ped").val(vtransp);
	$("#forma_envio_transp_desc_ped").val(vtransp_desc);
	$("#prazo_transp_ped").val(vprazo);
	
	var vl_sub_total_pedido = $("#vl_sub_total_pedido").val();
	var vl_credito_cdh = $("#vl_credito").val()==""?"0.00":$("#vl_credito").val();
	var valor_total_pedido_calc = 0.00;
	
	if(vl_sub_total_pedido!='' && vl_sub_total_pedido!='0.00' && vl_sub_total_pedido!='0'){
	var valor_total_pedido_frt = eval(number_format(vvalorfrete,2,'.','')) + eval(number_format(vl_sub_total_pedido,2,'.',''));
	
	if(vl_credito_cdh >= valor_total_pedido_frt){
	$("#vl_credito_usado").val(number_format(valor_total_pedido_frt,2,'.',''));	 
	}else{
	valor_total_pedido_calc = valor_total_pedido_frt - eval(number_format(vl_credito_cdh,2,'.',''));
	$("#vl_credito_usado").val(number_format(vl_credito_cdh,2,'.',''));
	}
	
	
	$('#vl_total_pedido_frete').val(vvalorfrete);
	$('#txt_vl_total_pedido').html('Pedido Valor Total: R$ '+number_format(valor_total_pedido_calc,2,',','.'));
	$('#vl_total_pedido').val(number_format(valor_total_pedido_calc,2,'.',''));
	}
	
}

function Fc_lista_end_cons(){
	
	
	if($("#id_cons").val()>0){
		
		vid_cons = $("#id_cons").val();
		
	//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

    $.ajax({
        url : 'ajax/ajax_cdh_hinode_gera_pedido.asp',
        type :'POST',
		data: {acao:'lista_endereco',idconsultor:vid_cons}, 
		dataType:"text",
    	
	    beforeSend : function(){
            $('#carregando').show(100);
        },
        
        complete : function(){
            $('#carregando').hide(100);
        },
        
		success : function(retorno){
            //$('#retorno').html(retorno);
				    
			var rs_cons = retorno.split('|')

			if(parseInt(rs_cons[0])==0){
			$("#retorno").html(rs_cons[1]);	
			}else{
			
			        document.getElementById('id_cons').value=$.trim(rs_cons[0]);
				    document.getElementById('endereco').value=$.trim(rs_cons[1]);
					document.getElementById('numero').value=$.trim(rs_cons[2]);
					document.getElementById('complemento').value=$.trim(rs_cons[3]);
					document.getElementById('bairro').value=$.trim(rs_cons[4]);
					document.getElementById('cidade').value=$.trim(rs_cons[5]);
					document.getElementById('cep').value=$.trim(rs_cons[7]);
					document.getElementById('estado').value=$.trim(rs_cons[6]);
					//$('#estado option[value='+rs_cons[6]+']').attr({ selected : "selected" });
					
					vend_entr = $.trim(rs_cons[1])+" - CEP: "+$.trim(rs_cons[7]);
	     			vend_entr += "<BR />"+$.trim(rs_cons[2])+" - "+$.trim(rs_cons[3]);
         			vend_entr += "<BR />"+$.trim(rs_cons[4])+" - "+$.trim(rs_cons[5]);
         			vend_entr += "<BR />"+rs_cons[6];
		 			$("#mod_entr").html(vend_entr);
					
			
		    }//else
			
        },
    	//veja se teve erros
        error : function(retorno){
		//loga o retorno pra que possamos verificar no console
    	//console.log(retorno);
		// se o status for 404
    	if ( retorno.status == 404 ) {
        $('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
    	} else {
      $('#retorno').html('Houve um erro desconhecido entre em contato com o administrador.');
        }
}
	})
	
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

	}
	
}


function Fc_credito_cdh(){
	
	if($("#id_cons").val()>0){
		
		vid_cons = $("#id_cons").val();
		
	//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

    $.ajax({
        url : 'ajax/ajax_cdh_hinode_gera_pedido.asp',
        type :'POST',
		data: {acao:'credito_cdh',idconsultor:vid_cons}, 
		dataType:"text",
    	
	    beforeSend : function(){
            $('#carregando').show(100);
        },
        
        complete : function(){
            $('#carregando').hide(100);
        },
        
		success : function(retorno){
            //$('#retorno').html(retorno);
				    
			var rs_cons = retorno.split('|')


			if(parseInt(rs_cons[0])==1){        	
					$('#txt_vl_credito').html('Crédito: R$ '+number_format(rs_cons[1],2,',','.'));
					document.getElementById('vl_credito').value=$.trim(rs_cons[1]);
			}
			
			//$("#retorno").html(rs_cons[1]);
			//$('#txt_vl_credito').html('Crédito: R$ '+number_format(rs_cons[0],2,',','.'));
			//document.getElementById('vl_credito').value=$.trim(rs_cons[0]);
					
			
        },
    	//veja se teve erros
        error : function(retorno){
		//loga o retorno pra que possamos verificar no console
    	//console.log(retorno);
		// se o status for 404
    	if ( retorno.status == 404 ) {
        $('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
    	} else {
      $('#retorno').html('Houve um erro desconhecido entre em contato com o administrador.');
        }
}
	})
	
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

	}
	
}
		
		
function Fc_lista_cat_subcat(idcat,acao){
	
	var vidcat = parseInt(idcat);
	var vacao = acao;
	//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			$.ajax({
        		url : 'ajax/ajax_cdh_hinode_gera_pedido.asp',
        		type :'POST',
		data : {acao:vacao,idcat:vidcat}, 
				dataType:"json",
				beforeSend : function(){
            		$('#carregando').show(100);
        		},
        		complete : function(){
            		$('#carregando').hide(100);
        		},
        		success : function(retorno){
					
					var vmostra_item = '';	
					var id_cat_sub_prod = 0;
					var tit_cat_sub_prod = "";
					
		if(retorno.length > 0){
			
				$.each(retorno, function (i, item){
					
					if(vidcat==0 && vacao=='lista_cat_subcat'){
						id_cat_sub_prod=item.idUso;
						tit_cat_sub_prod=item.Uso;
					}else if(vidcat>0 && vacao=='lista_cat_subcat'){
						id_cat_sub_prod=item.idLinha;
						tit_cat_sub_prod=item.Linha;
					}else if(vacao=='lista_prod_subcat'){
						id_cat_sub_prod=item.Codigo;
						tit_cat_sub_prod=item.Nome;
					}
				
				if(vacao=='lista_cat_subcat'){
					vmostra_item += '<li>';
					vmostra_item += '<a href="#'+id_cat_sub_prod+'">'; 
					vmostra_item += '<div class="categoria">'+tit_cat_sub_prod+'</div>';
					vmostra_item += '</a>';
					vmostra_item += '</li>';
				}else if(vacao=='lista_prod_subcat'){
				
		    vmostra_item += '<li>';
			vmostra_item += '<a href="#'+id_cat_sub_prod+'">'; 
			vmostra_item += '<div class="coluna-codigo">'+id_cat_sub_prod+'</div>';
			vmostra_item += '<div class="produtos-descricao">'+tit_cat_sub_prod+'</div>';
			vmostra_item += '</a>';
			vmostra_item += '</li>';
				
				}
				
				})
						
		}else{
			if(vacao=='lista_cat_subcat'){
				vmostra_item += '<li>';
				vmostra_item += '<a href="#0">'; 
				vmostra_item += '<div class="categoria">Indisponível</div>';
				vmostra_item += '</a>';
				vmostra_item += '</li>';	
			}else if(vacao=='lista_prod_subcat'){
				vmostra_item += '<li>';
				vmostra_item += '<a href="#0">'; 
				vmostra_item += '<div class="coluna-codigo">00000</div>';
				vmostra_item += '<div class="produtos-descricao">Indisponível</div>';
				vmostra_item += '</a>';
				vmostra_item += '</li>';
			}
		}			
					
					if(vidcat==0 && vacao=='lista_cat_subcat'){
						$("#list_cat").html("<ul>"+vmostra_item+"</ul>");
					}else if(vidcat>0 && vacao=='lista_cat_subcat'){
						$("#list_sub_cat").html("<ul>"+vmostra_item+"</ul>");	
					}else if(vacao=='lista_prod_subcat'){
					    $("#list_prod").html("<ul>"+vmostra_item+"</ul>");	
					}
					
				},
				error : function(retorno){
					 //console.log(retorno);
					if ( retorno.status == 404 ){
        				$('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
    				}else{
      					$('#retorno').html('Houve um erro desconhecido entre em contato com o administrador...');
        			}
				}
			})
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

}
	

		

		
		
String.prototype.preencherEsq = function(stringAdd, tamanhoFinal) {
    var str = this;
    while (str.length < tamanhoFinal)
        str = stringAdd + str;
    return str;
}
<% 'end if %>
