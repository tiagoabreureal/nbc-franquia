<% 'if request.servervariables("HTTP_REFERER")<>"" then %>
$(document).ready(function(){
			
			
		$("#bt_print").click(function ( event ) {	
			event.preventDefault();
			$('#tela').printElement({});
				
		})
			
		/* LISTA CARRINHO */
		var vid_pedido_oficial = $("#IdPedido_oficial").val();
		if(vid_pedido_oficial!='' && vid_pedido_oficial > 0){
			
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			$.ajax({
        		url : 'ajax/ajax_cdh_hinode_gera_pedido.asp',
        		type :'POST',
				data : {acao:'lista_pedido',id_pedido_oficial:vid_pedido_oficial}, 
				dataType:"json",
				beforeSend : function(){
            		$('#carregando').show(100);
        		},
        		complete : function(){
            		$('#carregando').hide(100);
        		},
        		success : function(retorno){
					
					//console.log(retorno);
					
					if(retorno.length > 0 ) {
						
						var vStatus_ped = "";
						var vqtd_parc = "";
						
						$.each(retorno, function (i, item) {
					    
						if(item.Ped_idStatus=='1'){
						vStatus_ped = "Aguardando Compensação";
						}else if(item.Ped_idStatus=='2'){
						vStatus_ped = "Pagamento Realizado";
						}else{
						vStatus_ped = "Cancelado";
						}
						
						vqtd_parc = " - "+item.Ped_QtdaParcela+"x";
						
						
						
		$("#vl_CdPedido").html("NÚMERO DO PEDIDO: "+item.IdPedido_oficial);
		$("#vl_Ped_idStatus").html(vStatus_ped);
		$("#vl_Ped_nome_consultor").html(item.Ped_nome_consultor);
						
		//item.Ped_email
		
		$("#vl_Ped_pagamento").html(item.Ped_pagamento+''+vqtd_parc);
		//item.Ped_CodigoAprovacao
		//$("#vl_Ped_QtdaParcela").html(item.Ped_QtdaParcela);
		$("#vl_Ped_vl_sub_total").html("R$ "+number_format(item.Ped_vl_sub_total,2,',','.'));
		
		$("#vl_Ped_vl_frete").html("R$ "+number_format(item.Ped_vl_frete,2,',','.'));
		
		$("#vl_Ped_vl_total").html("R$ "+number_format(item.Ped_vl_total,2,',','.'));
		
		$("#vl_credito_user").html("R$ "+number_format(item.Ped_Credito_usado,2,',','.'));
		
		
						
		//$("#vl_Ped_pontos").html(number_format(item.Ped_pontos,2,',','.'));
		//item.Ped_peso
		$("#vl_Ped_modo_entr").html(item.Ped_modo_entr);
		$("#vl_Ped_prazo_dias").html(item.Ped_prazo_dias);
		//item.Ped_idDestinatario_local_retira
		//$("#vl_Ped_local_entr").html(item.Ped_destinatario_local_retira_nome);
		$("#vl_Ped_Transporte").html(item.Ped_Transporte);
						
						$("#vl_Ped_cep").html(item.Ped_cep);
						$("#vl_Ped_endereco").html(item.Ped_endereco);
						$("#vl_Ped_numero").html(item.Ped_numero);
						$("#vl_Ped_compl").html(item.Ped_compl);
						$("#vl_Ped_bairro").html(item.Ped_bairro);
						$("#vl_Ped_cidade").html(item.Ped_cidade);
						$("#vl_Ped_estado").html(item.Ped_estado);
						
						
						$("#vl_Ped_dt_cadastro").html(item.Ped_dt_cadastro);
						//item.IdPedido_oficial
					   
					    $("#Ped_vl_total").val(number_format(item.Ped_vl_total,2,',','.'));
						$("#Ped_vl_frete").val(number_format(item.Ped_vl_frete,2,',','.'));
						$("#Ped_vl_sub_total").val(number_format(item.Ped_vl_sub_total,2,',','.'));
						$("#Ped_nome_consultor").val(item.Ped_nome_consultor);
						$("#Ped_idConsultor").val(item.Ped_idConsultor);
						
						$("#Ped_estado").val(item.Ped_estado);
						$("#Ped_cidade").val(item.Ped_cidade);
						$("#Ped_bairro").val(item.Ped_bairro);
						$("#Ped_compl").val(item.Ped_compl);
						$("#Ped_numero").val(item.Ped_numero);
						$("#Ped_endereco").val(item.Ped_endereco);
						$("#Ped_cep").val(item.Ped_cep);
						$("#Ped_idFormaPagamento").val(item.Ped_idFormaPagamento);
						$("#Ped_QtdaParcela").val(item.Ped_QtdaParcela);
						

					    })
						
						Fc_pedido_item(vid_pedido_oficial);
				    
					}else{
					 $("#retorno").html("Pedido Não Encontrado!");
				    }
					
				},
				error : function(retorno){
					 //console.log(retorno);
					if ( retorno.status == 404 ){
        				$('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
    				}else{
      					$('#retorno').html('Houve um erro desconhecido entre em contato com o administrador...');
        			}
				}
			})
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		}else{
		$("#retorno").html("Pedido Não Encontrado!!");
		}

       
})


function Fc_pedido_item(vid_pedido_oficial){
			
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			$.ajax({
        		url : 'ajax/ajax_cdh_hinode_gera_pedido.asp',
        		type :'POST',
				data : {acao:'lista_pedido_item',id_pedido_oficial:vid_pedido_oficial}, 
				dataType:"json",
				beforeSend : function(){
            		$('#carregando').show(100);
        		},
        		complete : function(){
            		$('#carregando').hide(100);
        		},
        		success : function(retorno){
					
					//console.log(retorno);
					
					if(retorno.length > 0 ) {
						
						var meusItens = '';
						var valor_total_item = 0;
						var pontos_total_item = 0;
						var encomendar_item = '';
					    var Status_item = '';
						var bg_item = '';
						
						$.each(retorno, function (i, item) {
							
							
							if(item.PedItem_prod_qtd_encomendar>0){
						encomendar_item = '</br><span class="texto_vermelho">Item sob encomenda, quantidade encomendar:</span>'+item.PedItem_prod_qtd_encomendar;
					}else{
					encomendar_item = '';
					}
					
					
					if(item.PedItem_prod_status==0){ 
                    Status_item = 'Cancelado'; bg_item = 'background-color:#FEF3B8';
                    }else{
                    Status_item = ''; 
                    bg_item = '';
                    }
					    
						valor_total_item = eval(item.PedItem_prod_qtd*item.PedItem_prod_valor_unt);	
					    pontos_total_item = eval(item.PedItem_prod_qtd*item.PedItem_prod_pontuacao);
						
					
			vnome_prod = item.PedItem_prod_nome.replace(new RegExp( "\\n", "g" ),"").replace(new RegExp( "\\<br>", "g" )," ")
					
					
							meusItens += '<li style="height:auto; padding-bottom:5px; padding-top:5px; '+bg_item+'">';
							
                            // += '<div class="item">'+ item.PedItem_idProduto+'</div>';
                            meusItens += '<div class="codigo">'+ item.PedItem_prod_codigo+'</div>';
                            meusItens += '<div class="produto">'+ vnome_prod+''+encomendar_item+'</div>';
                            meusItens += '<div class="quant">'+ item.PedItem_prod_qtd+'</div>';
							meusItens += '<div class="valor">R$ '+ number_format(item.PedItem_prod_valor_unt,2,',','.')+'</div>';
                            meusItens += '<div class="valor-total">R$ '+ number_format(valor_total_item,2,',','.')+'</div>';
						    meusItens += '<div class="desconto texto_vermelho">'+Status_item+'</div>'; //number_format(pontos_total_item,2,',','.')
                            meusItens += '</li>';
						 
						 })
						
						$("#carrinho_lista").html("<ul>"+ meusItens + "</ul>");
						
					}else{
					 $("#retorno").html("Item do Pedido Não Encontrado!");
				    }
					
				},
				error : function(retorno){
					//console.log(retorno);
					if ( retorno.status == 404 ){
        				$('#retorno').html('Houve um erro: ' + retorno.status + ' ' + retorno.statusText);
    				}else{
      					$('#retorno').html('Houve um erro desconhecido entre em contato com o administrador...');
        			}
				}
			})
			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXX AJAX XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		
		}
<% 'end if %>        