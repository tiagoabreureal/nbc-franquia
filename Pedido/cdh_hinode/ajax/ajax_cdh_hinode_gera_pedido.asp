﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="../mw-conexao-db.asp"-->
<!--#include file="../json/JSON_2.0.4.asp"-->
<!--#include file="../json/JSON_UTIL_0.1.1.asp"-->

<%

vacao = Request.Form("acao")
vid_pedido_oficial = Request.Form("id_pedido_oficial")

vidpatro = Request.Form("id_patro")
vidconsultor = Request.Form("idconsultor")
vss_pg = Request.Form("ss_pg")
vcpf = Request.Form("cpf")
vcnpj = Request.Form("cnpj")
vid_cad = Request.Form("id_cad")
vloc_cons_por = Request.Form("loc_cons_por")
vloc_prod = Request.Form("loc_prod")
vqtd = Request.Form("qtd_prod")
vid_car = Request.Form("id_car")

vpeso_ped = replace(Request.Form("peso_ped"),",","")
vcep_entr = Request.Form("cep_entr")
vid_cdhret = Request.Form("id_cdhret")


vmodo_entrega = Request.Form("modo_entrega")
vnome_cons = LimpaLixo(ExpurgaApostrofe(Request.Form("nome_cons")))  
vid_cdhret_desc = Request.Form("id_cdhret_desc") 

vendereco = LimpaLixo(ExpurgaApostrofe(Request.Form("endereco"))) 
vnumero = LimpaLixo(ExpurgaApostrofe(Request.Form("numero"))) 
vcompl = LimpaLixo(ExpurgaApostrofe(Request.Form("compl"))) 
vbairro = LimpaLixo(ExpurgaApostrofe(Request.Form("bairro"))) 
vcidade = LimpaLixo(ExpurgaApostrofe(Request.Form("cidade"))) 
vestado = LimpaLixo(ExpurgaApostrofe(Request.Form("estado"))) 

vid_forma_pag = Request.Form("id_forma_pag") 
vdesc_forma_pag = Request.Form("desc_forma_pag") 
vvl_frete = Request.Form("vl_frete") 
vvl_ped = Request.Form("vl_ped") 
vvl_sub_ped = Request.Form("vl_sub_ped") 
vpontos_ped = Request.Form("pontos_ped")
vl_credito_usado_ped = Request.Form("vl_credito_usado")

vped_tipo = Request.Form("ped_tipo")

vregra_estoque = Request.Form("regra_estoque")


vstatus_ped = Request.Form("status_ped")
vemail_ped = Request.Form("email_ped")
vcd_aprovacao_card = Request.Form("cd_aprovacao_card")
vqtd_parcela_card = Request.Form("qtd_parcela_card")
vprazo_transp_ped = Request.Form("prazo_transp_ped")
vid_transp = Request.Form("forma_envio_transp_ped")
vtransp = Request.Form("forma_envio_transp_desc_ped")

vidcat = Request.Form("idcat")
	
	


Select Case vacao

Case "lista_pedido"

if vid_pedido_oficial <> "" then
	Vretorno = Fc_lista_pedido(conexao_loja_mw,vid_pedido_oficial)
	response.Write(Vretorno)
else
	response.Write("0|Erro: Pedido não encontrado!")
end if 


Case "lista_pedido_item"

if vid_pedido_oficial <> "" then
	Vretorno = Fc_lista_pedido_item(conexao_loja_mw,vid_pedido_oficial)
	response.Write(Vretorno)
else
	response.Write("0|Erro: Item do Pedido não encontrado!")
end if 




Case "gera_pedido"

	if  vidconsultor <> "" and vss_pg <> "" and vid_forma_pag <> "" and vvl_ped <> "" then	
	    Vretorno = Fc_gera_pedido(conexao_loja_mw,vidconsultor,vss_pg,vmodo_entrega,vnome_cons,vid_cdhret,vid_cdhret_desc,vcep_entr,vendereco,vnumero,vcompl,vbairro,vcidade,vestado,vid_forma_pag,vdesc_forma_pag,vvl_frete,vvl_ped,vvl_sub_ped,vpontos_ped,vpeso_ped,vstatus_ped,vemail_ped,vcd_aprovacao_card,vqtd_parcela_card,vprazo_transp_ped,vid_transp,vtransp,vl_credito_usado_ped,vped_tipo)
		response.Write(Vretorno)
	else
	    response.Write("0|Favor informe a forma de entrega!")
	end if
	


Case "calc_frete_list_transp"

	if  vss_pg <> "" and vid_cdhret<>"" then	
	    Vretorno = Fc_car_calc_frete_list_transp(conexao_loja_mw,vss_pg,vpeso_ped,vcep_entr,vid_cdhret)
		response.Write(Vretorno)
	else
	    response.Write("0|Favor informe a forma de entrega!")
	end if

Case "car_lista_hist"

	if  vidconsultor <> "" then	
		Vretorno = Fc_car_lista_hist(conexao_loja_mw,vidconsultor,vid_cdhret)
		response.Write(Vretorno)
	else
		response.Write("0|Não há histórico de Carrinho desse consultor!")
	end if
	

Case "credito_cdh"

	if  vidconsultor <> "" then	
		Vretorno = Fc_credito_cdh(conexao_loja_mw,vidconsultor)
		response.Write(Vretorno)
	else
		response.Write("0|Consultor não informado!")
	end if	


Case "car_lista_item"

	if  vss_pg <> "" and  vidconsultor <> "" then	
		Vretorno = Fc_car_lista_item(conexao_loja_mw,vidconsultor,vss_pg,vid_cdhret)
		response.Write(Vretorno)
	else
		response.Write("0|Carrinho está vazio!")
	end if
	
Case "lista_cat_subcat"
Vretorno = Fc_lista_cat_subcat(conexao_loja_mw,vidcat)	
response.Write(Vretorno)

Case "lista_prod_subcat"
Vretorno = Fc_lista_prod_subcat(conexao_loja_mw,vidcat)	
response.Write(Vretorno)

Case "car_add_item"
     
	if  vloc_prod <> "" and  vidconsultor <> "" and vss_pg <> "" and vid_cdhret<>"" then	
		Vretorno = Fc_car_add_item(conexao_loja_mw,vidconsultor,vloc_prod,vqtd,vss_pg,vid_cdhret,vregra_estoque)
		response.Write(Vretorno)
	else
		response.Write("0|Favor informe o produto")
	end if
	

Case "car_del_item"

	if  vloc_prod <> "" and  vidconsultor <> "" and vss_pg <> "" and vid_car <> "" then	
		Vretorno = Fc_car_del_item(conexao_loja_mw,vidconsultor,vloc_prod,vss_pg,vid_car)
		response.Write(Vretorno)
	else
		response.Write("0|Favor informe o produto")
	end if	
	

Case "car_upd_item"

	if  vloc_prod <> "" and  vidconsultor <> "" and vss_pg <> "" and vid_car <> "" then	
		Vretorno = Fc_car_upd_item(conexao_loja_mw,1,vidconsultor,vloc_prod,vqtd,vss_pg,vid_car)
		response.Write(Vretorno)
	else
		response.Write("0|Favor informe o produto")
	end if	
	
	
Case "loc_consultor"

	if  vloc_cons_por <> "" and  vidconsultor <> "" then	
		Vretorno = Fc_loc_consultor(conexao_loja_mw,vidconsultor,vloc_cons_por)
		response.Write(Vretorno)
	else
		response.Write("0|Consultor Não Encontrado")
	end if
	

Case "lista_endereco"
	
	if vidconsultor <> "" then	
		Vretorno = Fc_lista_endereco(conexao_loja_mw,vidconsultor)
		response.Write(Vretorno)
	else
		response.Write("0|Consultor Não Encontrado")
	end if


Case "obter_cdh"

Vretorno = Fc_obter_cdh(conexao_mw,vss_pg)
response.Write(Vretorno)
		

Case Else
	Response.Write("0")
		
End Select



'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX FUNCTION  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

function Fc_gera_pedido(conexao,vidconsultor,vss_pg,vmodo_entrega,vnome_cons,vid_cdhret,vid_cdhret_desc,vcep_entr,vendereco,vnumero,vcompl,vbairro,vcidade,vestado,vid_forma_pag,vdesc_forma_pag,vvl_frete,vvl_ped,vvl_sub_ped,vpontos_ped,vpeso_ped,vstatus_ped,vemail_ped,vcd_aprovacao_card,vqtd_parcela_card,vprazo_transp_ped,vid_transp,vtransp,vl_credito_usado_ped,vped_tipo)


vSQLCAR = " SELECT * " &_
" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
" WHERE Car_idConsultor='"&vidconsultor &"' and Car_session = '"&vss_pg&"'"&_
" and Car_tipo_ped='CDH_HINODE' order by CdCarrinho asc "

set adocar=conexao.execute(vSQLCAR)
			
			if not adocar.eof then
			
			if vped_tipo = "CDH>HINODE" then
				'vid_cdhret = "00000849"
				'vid_cdhret_desc = "LARRUS"
				
				
					vid_cdhret = "01001736"
					vid_cdhret_desc = "DINAMIC"

			end if
			
vSQLPED = "INSERT INTO [hinode_loja].[dbo].[tbl_mw_gera_pedido] " &_
"(Ped_idConsultor,Ped_idStatus,Ped_nome_consultor,Ped_email,Ped_session,Ped_idFormaPagamento,Ped_pagamento"&_
",Ped_CodigoAprovacao,Ped_QtdaParcela,Ped_vl_sub_total,Ped_vl_frete,Ped_vl_total,Ped_pontos"&_
",Ped_peso,Ped_modo_entr,Ped_prazo_dias,Ped_idDestinatario_local_retira"&_
",Ped_destinatario_local_retira_nome,Ped_idTransporte,Ped_Transporte,Ped_cep,Ped_endereco,Ped_numero,Ped_compl"&_
",Ped_bairro,Ped_cidade,Ped_estado,Ped_ip_remoto,Ped_dt_cadastro,Ped_Credito_usado,Ped_tipo)" &_ 
" VALUES ('" & vidconsultor & "','" & vstatus_ped & "','" & vnome_cons &"','" & vemail_ped & "'," &_
				"'" & vss_pg & "','" & vid_forma_pag & "','"&vdesc_forma_pag&"','"&vcd_aprovacao_card&"'," &_
				"'" & vqtd_parcela_card & "','"& vvl_sub_ped &"','"& vvl_frete &"','"& vvl_ped &"','" & vpontos_ped & "',"&_
				"'"& vpeso_ped &"','"& vmodo_entrega &"','"& vprazo_transp_ped &"',"&_
		"'"& vid_cdhret &"','"& vid_cdhret_desc &"','"& vid_transp &"','"& vtransp &"','"& vcep_entr &"','"& vendereco &"',"&_
				"'"&vnumero&"','"& vcompl &"','"& vbairro &"','"& vcidade &"','"&vestado&"',"&_ 
				"'"& Request.ServerVariables("REMOTE_ADDR")&"',GetDate(),'"&vl_credito_usado_ped&"','"&vped_tipo&"');" 
				
				On Error Resume Next 
				conexao.Execute(vSQLPED)
				
			if err.number <> 0 then	
				response.Write("0|Erro ao gerar o pedido, revise seus dados!"&vSQLPED)	
			else
		set ObjRs = conexao.execute("SELECT IDENT_CURRENT('[hinode_loja].[dbo].[tbl_mw_gera_pedido]') AS [ID]")
				
				IDPED = objRs("ID")

				objRs.Close
				Set objRs = Nothing
				
				'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX INSERIR PEDIDOS ITENS XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
				vSQLPEDITEM = ""
				
				do while not adocar.eof
				
				vPeso = replace(replace(adocar("Car_prod_peso"),".",""),",",".")
				vValor_unt = replace(replace(adocar("Car_prod_valor_unt"),".",""),",",".")
				vValor_unt_desc = replace(replace(adocar("Car_prod_valor_desc"),".",""),",",".") 
				vPontuacao = replace(replace(adocar("Car_prod_pontuacao"),".",""),",",".")
		
				vSQLPEDITEM = vSQLPEDITEM & "INSERT INTO [hinode_loja].[dbo].[tbl_mw_gera_pedido_item] " &_
				"(PedItem_CdPedido,PedItem_idProduto,PedItem_prod_codigo,PedItem_prod_nome," &_
                "PedItem_prod_valor_unt,PedItem_prod_valor_desc,"&_
                "PedItem_prod_qtd,PedItem_prod_peso,PedItem_prod_pontuacao,PedItem_prod_qtd_encomendar) " &_			
                " VALUES ('" & IDPED & "','" & adocar("Car_idProduto") & "','" & adocar("Car_prod_codigo") &"'," &_
				"'" &  adocar("Car_prod_nome") & "','" & vValor_unt & "'," &_
				"'" & vValor_unt_desc & "','"& adocar("Car_prod_qtd") &"'," &_
				"'" & vPeso &"','"& vPontuacao &"','"&adocar("Car_prod_qtd_encomendar")&"');" 
				adocar.movenext
				loop
				
				On Error Resume Next 
				conexao.Execute(vSQLPEDITEM)
				
				if err.number <> 0 then	
					response.Write("0|Erro:Ocorreu um erro ao inserir os itens do pedido, revise seus dados!")
				else
				    
					vSQLCARDEL = " DELETE FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
	" WHERE Car_session='"&vss_pg&"' and Car_idConsultor='"&vidconsultor &"' and Car_tipo_ped='CDH_HINODE'"
					'On Error Resume Next
					conexao.execute(vSQLCARDEL)
					
					'XXXXXXXXXXXXXXXXXXXXXXXXX IMPORTA O PEDIDO XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
					
					strsql="exec [hinode_loja].[dbo].[loja_FechaPedido2_sp] '"&vid_cdhret&"','"&IDPED&"' "
					On Error Resume Next
					set ado_proc=conexao.execute(strsql)
							
					 if err.number <> 0 then
					 
					 EnviarEmail = EnviaNotificaTecnico("id:"&vId_cod&"-"& Err.number & "-" & err.Description,"IMPORTA-PEDIDO-CDH-HINODE-0",strsql,0)	
					 
					 else
					 
					  if ( not ado_proc.eof ) then
						
							vId_cod = ado_proc("cod")
							vmsg = ado_proc("msg")
							vId_pedido_oficial = ado_proc("idPedido") 
						    
							if vId_pedido_oficial>0 then
								
								EnviarEmail = EnviaPedidoEmail(vId_pedido_oficial,conexao)
								response.Write(vId_pedido_oficial&"|Pedido gerado com sucesso!")
							
							elseif cint(vId_cod)=95 then
								response.Write("0|Pedido Cancelado, Produtos esgotados!")
EnviarEmail = EnviaNotificaTecnico("Critica:"&vId_cod&"-"& Err.number & "-" & err.Description,"PEDIDO-CANCELADO-CDH-HINODE-0",strsql,vId_cod)
									
							elseif vId_cod>0 and vId_pedido_oficial=0 then
							
								response.Write("0|Pedido Cancelado, Pedido já importado ou falha na importação!")
								EnviarEmail = EnviaNotificaTecnico("idErro:"&vId_cod&"-"& Err.number & "-" & err.Description,"IMPORTA-PEDIDO-CDH-HINODE-1",strsql,vId_cod)
										
							else
	EnviarEmail = EnviaNotificaTecnico("idErro:"&vId_cod&"-"& Err.number & "-" & err.Description,"IMPORTA-PEDIDO-CDH-HINODE-2",strsql,vId_cod)
								response.Write("0|Erro:"&vId_cod&" - "&vmsg)
							end if
							
						else
						response.Write("0|Erro: Na Importação do Pedido gerado!")
						end if
						
					end if	

					'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
					
				    
				end if
				'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX INSERIR PEDIDOS ITENS XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			end if
			
			
else
response.Write("0|Não foi possível localizar o carrinho desse consultor / seção!")
end if
			
adocar.Close	
set adocar=nothing

conexao.Close
set conexao=nothing

Exit Function 
End Function

function Fc_lista_pedido(conexao,vid_pedido_oficial)

vSQLLISTPED = " SELECT * " &_
" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido] " &_
" WHERE IdPedido_oficial='"&vid_pedido_oficial &"'"

QueryToJSON(conexao, vSQLLISTPED).Flush				  

conexao.Close
set conexao=nothing
	
Exit Function
end function


function Fc_lista_pedido_item(conexao,vid_pedido_oficial)

vSQLLISTPEDITEM = " SELECT item.* " &_
" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_item] item " &_
" inner join [hinode_loja].[dbo].[tbl_mw_gera_pedido] p ON p.CdPedido = item.PedItem_CdPedido"&_
" WHERE p.IdPedido_oficial='"& vid_pedido_oficial &"'"

QueryToJSON(conexao, vSQLLISTPEDITEM).Flush				  

conexao.Close
set conexao=nothing
	
Exit Function
end function




function Fc_car_calc_frete_list_transp(conexao,vss_pg,vpeso_ped,vcep_entr,vid_cdhret)

'===================== LISTA TRANSPORTE E CALCULA O FRETE ==========================
	strsqlfrete= "exec [dbo].[loja_ListaTransportadora_sp] '"&vss_pg&"','"& vid_cdhret &"'"
	QueryToJSON(conexao,strsqlfrete).Flush
'====================================================================================

conexao.Close
set conexao=nothing

Exit Function 
End Function


function Fc_car_lista_hist(conexao,vidconsultor,vid_cdhret)

vSQLCARLISTHIST = " SELECT top 1 Car_session " &_
" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
"WHERE Car_idConsultor='"&vidconsultor &"' And Car_id_cdh_retira='"&vid_cdhret&"' and Car_tipo_ped='CDH_HINODE' "&_
" order by Car_DtCriacao desc "

set adohist=conexao.execute(vSQLCARLISTHIST)
			
			if not adohist.eof then
			response.Write("1|"&adohist("Car_session"))
			else
			response.Write("0|Não há histórico de Carrinho desse consultor!")
			end if
	
adohist.Close	
set adohist=nothing

conexao.Close
set conexao=nothing

Exit Function 
End Function


function Fc_credito_cdh(conexao,vidconsultor)

vSQL_CRED_CDH = " SELECT top 1 valor " &_
" FROM [hinode_loja].[dbo].[view_CreditoCDH] " &_
" WHERE integracao='"&vidconsultor&"'"

set adocred_cdh=conexao.execute(vSQL_CRED_CDH)
			
			if not adocred_cdh.eof then
			
				'if cdbl(adocred_cdh("valor")) > 0 then
					response.Write("0|Não há credito para este cdh!")
					'response.Write("1|"&replace(replace(adocred_cdh("valor"),".",""),",","."))
				'else
				'	response.Write("0|Não há credito para este cdh!")
				'end if
		
			else	
				response.Write("0|Consultor Não Encontrado!")
			end if
	
adocred_cdh.Close	
set adocred_cdh=nothing

conexao.Close
set conexao=nothing

Exit Function 
End Function



function Fc_lista_cat_subcat(conexao,idcat)

if cint(idcat)=0 then

vSQL = " SELECT idUso,Uso,ordem" &_
" FROM [hinode_loja].[dbo].[loja_ListaUso] " &_
" WHERE idUso > 0 order by ordem,Uso "

else

vSQL = " SELECT l.idLinha,l.Linha" &_
" FROM [hinode_loja].[dbo].[loja_ListaLinha] l " &_
" INNER JOIN [hinode_loja].[dbo].[loja_ListaUsoXLinha] uso ON uso.idLinha = l.idLinha " &_
" WHERE uso.idUso = "&cint(idcat)&" order by l.Linha "

end if

QueryToJSON(conexao,vSQL).Flush				  

conexao.Close
set conexao=nothing
	
Exit Function
end function


function Fc_lista_prod_subcat(conexao,idcat)

if cint(idcat)<>0 then

vSQL = " SELECT Codigo,Nome,Descricao,Valor_CDH" &_
" FROM [hinode_loja].[dbo].[loja_ListaProdutoCDH] " &_
" WHERE idLinha = "&cint(idcat)&" and codigo not in (select produto from hinode..sys_produto_ocorrencia where ocorrencia=6 and '"&mid(session("usuario"),1,3)&"' = 'cdh')"&_
" order by Nome "

end if

QueryToJSON(conexao,vSQL).Flush				  

conexao.Close
set conexao=nothing
	
Exit Function
end function
	



function Fc_car_lista_item(conexao,vidconsultor,vss_pg,vid_cdhret)

vSQLCARLIST = " SELECT CdCarrinho,Car_idProduto,Car_prod_codigo,Car_prod_nome,Car_prod_valor_unt," &_
" Car_prod_qtd,Car_prod_peso,Car_prod_pontuacao,Car_prod_qtd_encomendar " &_
" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
" WHERE Car_session='"&vss_pg&"' and Car_idConsultor='"&vidconsultor &"' "&_
" AND Car_id_cdh_retira= '"&vid_cdhret&"' and Car_tipo_ped='CDH_HINODE' order by CdCarrinho desc "

QueryToJSON(conexao, vSQLCARLIST).Flush				  

conexao.Close
set conexao=nothing
	
Exit Function
end function


function Fc_car_del_item(conexao,vidconsultor,vloc_prod,vss_pg,vid_car)

vSQLCARDEL = " DELETE FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
				  " WHERE Car_prod_codigo = '" & vloc_prod & "'" &_
	 " and Car_session='"&vss_pg&"' and Car_idConsultor='"&vidconsultor &"' and CdCarrinho='"&vid_car&"'" &_
	 " AND Car_tipo_ped='CDH_HINODE' "
				conexao.execute(vSQLCARDEL)
			
			'if not adoudel.eof then
			response.Write("1|Item excluido com sucesso!")
			'else 
			'response.Write("0|Erro: ao deletar o item do carrinho, revise seus dados!")
			'End if  

End function



function Fc_car_upd_item(conexao,cl_conexao,vidconsultor,vloc_prod,vqtd,vss_pg,vid_car)



vSQLCARUP = " SELECT CdCarrinho FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
				  " WHERE Car_prod_codigo = '" & vloc_prod & "'" &_
" and Car_session='"&vss_pg&"' and Car_idConsultor='"&vidconsultor &"' and CdCarrinho='"&vid_car&"' "&_ 
" and Car_tipo_ped='CDH_HINODE' "
				  
		set adoupd=conexao.execute(vSQLCARUP)
			
			if not adoupd.eof then
			
			vSQLUP = " UPDATE [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] set " &_
				   " Car_prod_qtd = '" & cint(vqtd) & "',"&_ 
				   " Car_DtAlteracao=GetDate() "& _
				   " where CdCarrinho = '" & adoupd("CdCarrinho") &"'"
				   On Error Resume Next 
				   conexao.Execute(vSQLUP)
				   
				   if err.number <> 0 then
	   					response.Write("0|Erro: ao atualizar o item no carrinho, revise seus dados!")
				   else
						response.Write("1|Atualização Do Item do Carrinho Efetuado Com Sucesso!")
				   end if
			
			else
			response.Write("0|Erro: item no carrinho indisponível, revise seus dados!")
			end if
			
adoupd.Close	
set adoupd=nothing

if cl_conexao = 1 then
	conexao.Close
	set conexao=nothing
end if


Exit Function 
End Function




function Fc_car_add_item(conexao,vidconsultor,vloc_prod,vqtd,vss_pg,vid_cdhret,vregra_estoque)
	  
' 7002, 7011, 7012, 7013, 7014 	  
	  
if int(vloc_prod)=7002 OR int(vloc_prod)=7008 OR int(vloc_prod)=7011 OR int(vloc_prod)=7012 OR int(vloc_prod)=7013 OR int(vloc_prod)=7014 then

vloc_prod = "007000"

end if	  
	  
vqtd_encomendar = 0


'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx DELETE ITEM, PARA PODER LIBERAR O ITEM EMPENHADO xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
vSQLCARDEL = " DELETE FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
				  " WHERE Car_prod_codigo = '" & vloc_prod & "'" &_
				  " and Car_session='"&vss_pg&"' and Car_idConsultor='"&vidconsultor &"' "&_
				 " and Car_prod_atv_consultor=0 AND Car_tipo_ped='CDH_HINODE' "
				conexao.execute(vSQLCARDEL)
'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx	  
	  
strSqlList = " SELECT top 1 IdProduto,Nome,Codigo,Valor_CDH," &_
			  " Ativo,Peso,isnull(pontuacao,0) as pontuacao from [hinode_loja].[dbo].[loja_ListaProdutoCDH] " &_
			  " WHERE Codigo = '" & vloc_prod & "' and Ativo = 1 and "&_
			  "codigo not in (select produto from hinode..sys_produto_ocorrencia where ocorrencia=6 and '"&mid(session("usuario"),1,3)&"' = 'cdh') ORDER BY Nome ASC "
	  
set adoadd=conexao.execute(strSqlList)

if not adoadd.eof then

   'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX VERIFICA ESTOQUE XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		 
	 'strEstoque = "SELECT Estoque=SUM(Estoque) " &_
	 '	" FROM [hinode_loja].[dbo].[view_ListaProdutoEstoque] " &_
	 '	" WHERE IdProduto = '" &adoadd("IdProduto")& "'" &_
	 '	" and Codigo = '" & vloc_prod & "' and empresa = '"&vid_cdhret&"'"
	 
	  'set adoest=conexao.execute(strEstoque)
	 
	'if not adoest.eof then
		 
	 '       vEstoque = 	"10000" 'adoest("Estoque")
 		 
	 '    if isNull(vEstoque) then 
		'    vEstoque = 0
	'	 end if
		 
	'  if vregra_estoque="0" and cdbl(vEstoque) <= 0 then ' Regra produto somente com estoque positivo
		 
	'	 response.Write("0|Produto Indisponível No Momento!")
		 
     ' else 
		 
		' if vregra_estoque="1" and cdbl(vEstoque) < cdbl(vqtd) then 
		 'Regra de estoque, vende produto com estoque negativo
		 
		 '		 vqtd_encomendar = cdbl(vEstoque)-cdbl(vqtd)
				 
			'	 if vqtd_encomendar < 0 and cdbl(vEstoque)<0 then 
			'	 	vqtd_encomendar = vqtd
			'	 end if  
		 
		 'elseif vregra_estoque = "0" and cdbl(vEstoque) < cdbl(vqtd) then
		 
		  '		 vqtd = cdbl(vEstoque)
			'	 vqtd_encomendar = 0 
		 
		' end if
		
		

		'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX add item no carrinho XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		
		vPeso = replace(replace(adoadd("Peso"),".",""),",",".")
		vValor_unt = replace(replace(adoadd("Valor_CDH"),".",""),",",".") 
		vPontuacao = replace(replace(adoadd("pontuacao"),".",""),",",".") 
		vIdProduto = adoadd("IdProduto")
		vCodigo = adoadd("Codigo")
		
		'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX VERIFICA SE O PROD JA TEM NO CARRINHO XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		
vSQLCAR = " SELECT CdCarrinho FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] " &_
" WHERE Car_idProduto = '"& vIdProduto &"' and Car_prod_codigo = '" & vCodigo & "'" &_
" and Car_session='"&vss_pg&"' and Car_idConsultor='"&vidconsultor &"' AND Car_tipo_ped='CDH_HINODE'"
		set adoprodcar=conexao.execute(vSQLCAR)
			
			if not adoprodcar.eof then
			
			'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX ATUALIZA O PROD NO CARRINHO XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			'vRetorno_up = Fc_car_upd_item(conexao,0,vidconsultor,vCodigo,vqtd,vss_pg,adoprodcar("CdCarrinho"))
			'response.Write(vRetorno)0
			
			vSQLUP = " UPDATE [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho] set " &_
				   " Car_prod_qtd = '" & cint(vqtd) & "',Car_prod_qtd_encomendar='"&abs(vqtd_encomendar)&"',"&_ 
				   " Car_DtAlteracao=GetDate(),Car_prod_atv_consultor=0 "& _
				   " where CdCarrinho = '" & adoprodcar("CdCarrinho") &"' AND Car_tipo_ped='CDH_HINODE'"
				   On Error Resume Next 
				   conexao.Execute(vSQLUP)
				   
				   if err.number <> 0 then
	   					response.Write("0|Erro: ao atualizar o item no carrinho, revise seus dados!")
				   else
						response.Write("1|Atualização Do Item do Carrinho Efetuado Com Sucesso!")
				   end if	   
			'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			
			else

				vSQL = "INSERT INTO [hinode_loja].[dbo].[tbl_mw_gera_pedido_carrinho]" &_
				" (Car_session,Car_idConsultor,Car_idProduto,Car_prod_codigo,Car_prod_nome,Car_prod_valor_unt,Car_prod_qtd," &_
		"Car_prod_peso,Car_prod_pontuacao,Car_prod_qtd_encomendar,Car_DtCriacao,Car_id_cdh_retira,Car_prod_atv_consultor,Car_tipo_ped)" &_ 
				" VALUES ('" & vss_pg & "','" & vidconsultor & "','" & vIdProduto &"','" & vCodigo & "'," &_
				"'" & adoadd("Nome") & "','" & vValor_unt & "','"&cint(vqtd)&"','"&vPeso&"'," &_
				"'" & vPontuacao & "','"&abs(vqtd_encomendar)&"',GetDate(),'"&vid_cdhret&"',0,'CDH_HINODE');" 
				On Error Resume Next 
				conexao.Execute(vSQL)
				if err.number <> 0 then	
					response.Write("0|Erro: Ao inserir item no carrinho, revise seus dados!")	
				else
					response.Write("1|Item adicionado no carrinho com sucesso!")
				end if
		 	
			end if
			
		adoprodcar.Close	
		set adoprodcar=nothing
						
		'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	
	'end if
		
	'else
	'response.Write("0|Produto Indisponível No Momento!")
	'end if
	
else
response.Write("0|Produto Não Encontrado..!")
end if


adoadd.Close	
set adoadd=nothing
conexao.Close
Set conexao = Nothing


Exit Function 
End Function



Function Fc_loc_consultor(conexao,vidconsultor,loc_por)

if loc_por = "1" then
vloc_por = " nome LIKE '%" & vidconsultor & "%'"
else
vloc_por = " integracao = '" & vidconsultor & "'"
end if

strSqlList = "SELECT integracao,nome,email,ativosn,dt_cad from [hinode_loja].[dbo].[view_Consultor] "& _
" WHERE " & vloc_por & " ORDER BY nome ASC"
	  
set adoloc=conexao.execute(strSqlList)

if not adoloc.eof then

		
			DATA1 = Cdate(Now()) 'DATA ATUAL
			DATA2 = Cdate(adoloc("dt_cad")) 'DATA CADASTRO
			DIAS = DateDiff("d",DATA2,DATA1)

			IF int(adoloc("ativosn"))=0 and int(DIAS) > 30 then
				response.Write("0|O Cadastro do Consultor Expirou O Prazo de 30 dias Para Ativação, Realize um Novo Cadastro!")		
			else
			vid_consultor = adoloc("integracao")
			vretorno = adoloc("integracao")&"|"& _
			" " & replace(adoloc("nome"),"|","")&""
				response.Write(vid_consultor&"|"&vretorno)	
			end if
	
else
	response.Write("0|O Consultor Não Foi Encontrado!")
end if

adoloc.Close	
set adoloc=nothing
conexao.Close
Set conexao = Nothing


Exit Function 
End Function


Function Fc_lista_endereco(conexao,vidconsultor)

strSqlList = " select top 1 * from[hinode].[dbo].[View_Consultor_DadosEntrega] "&_
" where integracao='" & vidconsultor & "' order by destinatario_entrega desc"

set adolist=conexao.execute(strSqlList)

if not adolist.eof then

		vid_consultor = adolist("integracao")
			vretorno = replace(adolist("endereco"),"|","")&"|"& _
			"" & replace(adolist("numero"),"|","")&"|"& _
			"" & replace(adolist("complemento"),"|","")&"|"& _
			"" & replace(adolist("bairro"),"|","")&"|"& _
			"" & replace(adolist("cidade"),"|","")&"|"& _
			"" & replace(adolist("estado"),"|","")&"|"& _
			"" & replace(adolist("cep"),"|","")&""
	response.Write(vid_consultor&"|"&vretorno)
else
	response.Write("0|Endereço do Consultor Não Encontrado")
end if

adolist.Close	
set adolist=nothing
conexao.Close
Set conexao = Nothing


Exit Function 
End Function



Function Fc_obter_cdh(conexao,vss_pg)

Set adocdh = Server.CreateObject("ADODB.Recordset")
strsql = " select b.destinatario,a.descricao "&_ 
    " from [hinode].[dbo].[sys_atividade] a "&_ 
	" inner join [hinode].[dbo].[fat_filial] b on a.destinatario_matriz=b.destinatario " & _
	" inner join [hinode].[dbo].[fat_Destinatario] c on b.destinatario=c.destinatario " & _
	" where b.cd in(1,2) and c.ativosn=1 order by a.descricao asc "
adocdh.Open strsql, conexao

	if ( not adocdh.eof ) then
	        vdiv = ""
			vcont = 0
			do while not adocdh.eof
			
			if vcont=0 then
			vCheck = "checked='checked'"
			end if
			
			
vdiv = vdiv & "<li><div class='cdh-nome'>"

if vss_pg <> "portal" then 
vdiv = vdiv & " <input type='radio' id='cdh_retira"&vcont&"' name='cdh_retira' value='"& adocdh("destinatario") &"@"& adocdh("descricao") &"' style='float:left;'/> "
end if

vdiv = vdiv & "" & adocdh("descricao") & "</div></li>"

			vcont = vcont + 1
			adocdh.movenext
			loop 
			
			response.Write("<ul>" & vdiv & "</ul>")
	else
	
vdiv = vdiv & "<li><div class='cdh-nome'>"&_
"<input type='radio' id='cdh_retira0' name='cdh_retira' value='0@Indiponivel' style='float:left;' checked='checked' />" & _
			    			"<p>Não há CDH Disponível</p>" & _
						  "</div>" & _
                		  
             		  "</li>"
					  
	response.Write("<ul>" & vdiv & "</ul>")	
	end if
	
adocdh.Close
Set adocdh = Nothing
conexao.Close
Set conexao = Nothing
	
Exit Function				  
End Function



Function EnviaPedidoEmail(vid_pedido_oficial,conexao)

if vid_pedido_oficial>0 then

Set adoPed = Server.CreateObject("ADODB.Recordset")
	  
vSQLLISTPED = " SELECT * " &_
" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido] " &_
" WHERE IdPedido_oficial='"&vid_pedido_oficial &"'"
				 	  
adoPed.Open vSQLLISTPED, conexao

if ( not adoPed.eof ) then
	do while not adoPed.eof
	
	vPed_idStatus = adoPed("Ped_idStatus")
	vPed_QtdaParcela = adoPed("Ped_QtdaParcela")
	vIdPedido_oficial = adoPed("IdPedido_oficial")
	vPed_nome_consultor = adoPed("Ped_nome_consultor")
	vPed_pagamento = adoPed("Ped_pagamento")
	vPed_vl_sub_total = FormatNumber(adoPed("Ped_vl_sub_total"),2)
	vPed_vl_frete = FormatNumber(adoPed("Ped_vl_frete"),2)
	vPed_vl_total = FormatNumber(adoPed("Ped_vl_total"),2)
	vPed_Credito_usado = FormatNumber(adoPed("Ped_Credito_usado"),2)
	vPed_modo_entr = adoPed("Ped_modo_entr")
	vPed_prazo_dias = adoPed("Ped_prazo_dias")
	vPed_Transporte = adoPed("Ped_Transporte")
	
	vPed_cep = adoPed("Ped_cep")
	vPed_endereco = adoPed("Ped_endereco")
	vPed_numero = adoPed("Ped_numero")
	vPed_compl = adoPed("Ped_compl")
	vPed_bairro = adoPed("Ped_bairro")
	vPed_cidade = adoPed("Ped_cidade")
	vPed_estado = adoPed("Ped_estado")
	vPed_dt_cadastro = adoPed("Ped_dt_cadastro")
	vPed_email = adoPed("Ped_email")
	
	adoPed.movenext
	loop
end if
	 

if vPed_email="" then
    vPed_email = "hinode_log@kplay.com.br"
end if


    
'XXXXXXXXXXXXXXXXX INICIO NOTIFICAÇÃO POR E-MAIL - CONSULTOR XXXXXXXXXXXXXXXXXXXX
	  
if vPed_email<>"" then

vMsg = "<table bgcolor='#FFFFFF' border='0' style='padding:20px;'>"& _
	"<tbody>"& _
	"<tr>"& _
	"<td width='550'>"& _
	"<table align='center' border='0' cellpadding='0' cellspacing='0'>"& _
	"<tbody>"& _
	"<tr>"& _
	"<td style='padding-bottom:5px;' width='550'>"& _
	"<table style='background-color:#f1e3c8; border:1px solid #e1caa8; width:100%; padding:10px 15px;'>"& _
	"<tbody>"& _
	"<tr>"& _
	"<td width='180'>"& _
"<img alt='Hinode.com.br' src='http://vo.hinode.com.br/images/logo-hinode-cosmeticos-email.png' /></td>"& _
"<td align='center' style='font-family:arial; font-size:16px; font-weight:bold; color:#000000;'>"& _
"PEDIDO CDH GERADO - "&vid_pedido_oficial&"</td>"& _
"</tr>"& _
"</tbody>"& _
"</table>"& _
"</td>"& _
"</tr>"& _
"<tr>"& _
"<td valign='top' style='padding:15px; border:1px solid #e1caa8;'>"& _
"<table align='center' border='0' cellpadding='0' cellspacing='0'>"& _
"<tbody>"& _
"<tr>"& _
"<td style='font:13px Arial;' width='520'>"& _
"<p>"& _
"Olá, "&vPed_nome_consultor&", <br>"& _ 
"À HINODE COSMÉTICOS, está enviando uma copia do pedido gerado.<br />"& _ 
"O seu número de pedido é: <strong>"&vid_pedido_oficial&"</strong>.<br /><br />"& _


"<strong>Dados do Pedido:</strong><br />"&_
""&vPed_nome_consultor&"<br />"&_
"Sub total pedido:"&vPed_vl_sub_total&"<br />"&_
"Frete:"&vPed_vl_frete&"<br />"&_
"Crédito Usado:"&vPed_Credito_usado&"<br />"&_
"Desconto:0,00<br />"&_
"Valor total pedido:"&vPed_vl_total&"<br />"&_
"Forma de Pagamento:"&vPed_pagamento&" "&vPed_QtdaParcela&"x <br />"&_
"<br />"&_
"<strong>Tipo de Envio:</strong><br />"&_
""&vPed_Transporte&"<br /><br />"&_

"<strong>Endereço de Entrega:</strong><br />"&_
""&vPed_endereco&" - N°:"&vPed_numero&""&vPed_compl&" - CEP:"& vPed_cep&"<br />"&_
"Bairro:"&vPed_bairro&" <br />"&_
"Cidade:"&vPed_cidade&"<br />"&_
"Estado:"&vPed_estado&""&_

"<br /><br />"&_

"<strong>Itens do pedido:</strong><br />"&_
"<table align='center' border='0' cellpadding='1' cellspacing='1' width='100%' bgcolor='#f1e3c8'>"&_
"<tbody>"&_
"<tr>"&_
"<td>Código</td>"&_
"<td>Produto</td>"&_
"<td>Quant.</td>"&_
"<td>Valor Unit.</td>"&_
"<td>Valor Total</td>"&_
"</tr>"

Set adoPeditem = Server.CreateObject("ADODB.Recordset")
	  
vSQLLISTPEDITEM = " SELECT item.* " &_
" FROM [hinode_loja].[dbo].[tbl_mw_gera_pedido_item] item " &_
" inner join [hinode_loja].[dbo].[tbl_mw_gera_pedido] p ON p.CdPedido = item.PedItem_CdPedido"&_
" WHERE p.IdPedido_oficial='"& vid_pedido_oficial &"'"
				 	  
adoPeditem.Open vSQLLISTPEDITEM, conexao

if ( not adoPeditem.eof ) then
	do while not adoPeditem.eof
	
	vPedItem_prod_codigo = adoPeditem("PedItem_prod_codigo")
	vPedItem_prod_nome = adoPeditem("PedItem_prod_nome")
	vPedItem_prod_qtd_encomendar = adoPeditem("PedItem_prod_qtd_encomendar")
	vPedItem_prod_qtd = adoPeditem("PedItem_prod_qtd")
	vPedItem_prod_valor_unt = adoPeditem("PedItem_prod_valor_unt")
	vvalor_total_item = vPedItem_prod_qtd*vPedItem_prod_valor_unt	
	vPedItem_prod_valor_unt2 = FormatNumber(vPedItem_prod_valor_unt,2)
	vvalor_total_item2 = FormatNumber(vvalor_total_item,2)
	vPedItem_prod_status = adoPeditem("PedItem_prod_status")
	
	if vPedItem_prod_qtd_encomendar>0 then
	encomendar_item = "</br><span class='texto_vermelho'>Item sob encomenda, quantidade encomendar:</span>"&vPedItem_prod_qtd_encomendar&""
	else
	encomendar_item = ""
	end if
	
	if vPedItem_prod_status=0then
	Status_item = "Cancelado"
	else
	Status_item = ""
	end if
	
	
vMsg = vMsg & "<tr bgcolor='#FFFFFF'>"&_
"<td>"&vPedItem_prod_codigo&"</td>"&_
"<td>"&vPedItem_prod_nome&" - <b>"&Status_item&"</b></td>"&_
"<td>"&vPedItem_prod_qtd&"</td>"&_
"<td>R$"&vPedItem_prod_valor_unt2&"</td>"&_
"<td>R$"&vvalor_total_item2&"</td>"&_
"</tr>"
adoPeditem.movenext
	loop
end if


vMsg = vMsg & "</tbody>"&_

"</table>"&_


"</td>"& _
"</tr>"& _

"</tbody>"& _
"</table>"& _
"</td>"& _
"</tr>"& _
"<tr>"& _
"<td height='90' style='font:12px Arial; background:#f1e3c8; color:#000000; padding:10px 15px'>"& _
"<table>"& _
"<tbody>"& _
"<tr>"& _
"<td style='padding-left:10px'>"& _
"<p style='font:12px arial; color:#000000'>"& _
"Atenciosamente,<br />"& _
"<span style='font:bold 20px arial;'>HINODE COSMÉTICOS</span><br />"& _
"<a href='http://www.hinode.com.br' style='color:#000000' target='_blank' "& _ 
"title='www.hinode.com.br'>www.hinode.com.br</a></p>"& _
"</td>"& _
"</tr>"& _
"</tbody>"& _
"</table>"& _
"</td>"& _
"</tr>"& _
"</tbody>"& _
"</table>"& _
"</td>"& _
"</tr>"& _
"</tbody>"& _
"</table>"
	 
		servidor="smtp.hinode.com.br"

		de  ="naoresponda@hinode.com.br"

		user="naoresponda@hinode.com.br"

		pass="druida@107"

		para = vPed_email

		assunto	= "PEDIDO CDH-HINODE [" & vid_pedido_oficial &"]"

Set Mail = Server.CreateObject("CDO.Message") 

               Set MailCon = Server.CreateObject ("CDO.Configuration") 

MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = servidor
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 587 
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 

               if len(user)>0 then

MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = user
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = pass

               end if

               MailCon.Fields.update
			   
			   Set Mail.Configuration = MailCon 
				   Mail.From = de
				   'Mail.CC="fabio@hinode.com.br;leandro@hinode.com.br;toni@hinode.com.br"
					Mail.BCC="toni@hinode.com.br;josivaldo@hinode.com.br"
				   Mail.To = para
	               Mail.Subject = assunto
				   'id_Body	="Esse é um teste de email."
                   Mail.HTMLBody=vMsg
	               Mail.Fields.update
	               Mail.Send
	           Set Mail = Nothing 
	           Set MailCon = Nothing 
			   'Response.Write "Enviado com sucesso às " & now
	end if		   
	
end if 	
'XXXXXXXXXXXXXXXXXXX FIM - ENVIA NOTIFICAÇÃO POR E-MAIL - CONSULTOR XXXXXXXXXXXXXXXXXXXX


adoPed.Close
Set adoPed = Nothing

adoPeditem.Close
Set adoPeditem = Nothing

conexao.Close
Set conexao = Nothing

Exit Function
				  
End Function


Function InverteData(data,inverte)
 If isDate(data) then
	 vData = data
	else
	 vData = now()	
	End If
	temp = split(vData,"/")
	
	 If len(temp(0)) <> 2 Then
		 temp(0) = "0" & temp(0)
		End If
		
		If len(temp(1)) <> 2 Then
		 temp(1) = "0" & temp(1)
		End If
	
	If inverte = true Then			 	 	 
 	InverteData = temp(1) & "/" & temp(0) & "/" & temp(2)
	Else 
	 InverteData	= temp(0) & "/" & temp(1) & "/" & temp(2)
	End If	
End Function

Function FormataData(Data)
  If Data <> "" Then
    FormataData = Right("0" & DatePart("d", Data),2) & "/" & Right("0" & DatePart("m", Data),2) & "/" & DatePart("yyyy", Data)
  End If
End Function


Function EnviaNotificaTecnico(Verro,vTela,Vquery,VerroID)

vMsg = "TELA:"&vTela&" <BR />" &_
"Descrição do Erro:"&Verro&"<BR />"&_
"Query: "&Vquery&"<br />"

servidor="smtp.hinode.com.br"

		de  ="naoresponda@hinode.com.br"

		user="naoresponda@hinode.com.br"

		pass="druida@107"

		para = "hinode_log@kplay.com.br"

		if cint(VerroID)=95 then
			assunto	= "Notificação de Pedido Cancelado"
		else
			assunto	= "Notificação de Erro Hinode"
		end if
		
		
		Set Mail = Server.CreateObject("CDO.Message") 

               Set MailCon = Server.CreateObject ("CDO.Configuration") 

MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = servidor
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 587 
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 

               if len(user)>0 then

MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusername") = user
MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendpassword") = pass

               end if

               MailCon.Fields.update
			   
			   Set Mail.Configuration = MailCon 
				   Mail.From = de
                   Mail.CC="ocorrencia@xcompany.com.br"
				   Mail.To = para
	               Mail.Subject = assunto
				   'id_Body	="Esse é um teste de email."
                   Mail.HTMLBody=vMsg
	               Mail.Fields.update
	               Mail.Send
	           Set Mail = Nothing 
	           Set MailCon = Nothing 
			   'Response.Write "Enviado com sucesso às " & now

Exit Function

end Function



'- Substituindo o apóstrofe(') pelo duplo apóstrofe ('')
Function ExpurgaApostrofe(texto)
    ExpurgaApostrofe = replace( Trim(texto) , "'" , "''")
End function

'- Substituindo os caracteres e palavras maliciosas por vazio("").
Function LimpaLixo(input)
    dim lixo
    dim textoOK

    lixo = array ("SELECT","DROP","UPDATE",";","--","INSERT","DELETE","xp_","<","SCRIPT",">")

    textoOK = Trim(input)

     for i = 0 to UBound(lixo)
          textoOK = replace( textoOK ,  lixo(i) , "")
     next

     LimpaLixo = textoOK

end Function


'- Rejeitando os dados maliciosos:
Function ValidaDados(input)
      lixo = array ( "SELECT" , "INSERT" , "UPDATE" , "DELETE" , "DROP" , "--" , "'")

      ValidaDados = true

      for i = LBound(lixo) to UBound(lixo)
            if ( instr(1 ,  input , lixo(i) , vbtextcompare ) <> 0 ) then
                  ValidaDados = False
                  exit function '}
            end if
      next
end function
%>