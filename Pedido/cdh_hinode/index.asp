<%@LANGUAGE="JAVASCRIPT" CODEPAGE="65001"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PEDIDOS - CHD >  HINODE</title>

	<!-- Add jQuery library -->
	<script type="text/javascript" src="../fancyBox/lib/jquery-1.8.0.min.js"></script>

	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="../fancyBox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="../fancyBox/source/jquery.fancybox.js?v=2.1.0"></script>
	<link rel="stylesheet" type="text/css" href="../fancyBox/source/jquery.fancybox.css?v=2.1.0" media="screen" />

	<script type="text/javascript">
		$(document).ready(function() {
			
			$('.fancybox').fancybox({ 
                                opacity : true, 
                                width : '100%', 
                                height: '95%', 
                                autoScale : true, 
                                transitionIn : 'elastic', 
                                transitionOut : 'elastic', 
                                type : 'iframe' 
                        });

		});
	</script>
	<style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}
	</style>

</head>

<body>
<a href="mw-gera-pedido-loja-cdh.asp" class="fancybox fancybox.iframe"> PEDIDO CDH > HINODE</a>
<br><br><a href="../../" > Voltar</a>
</body>
</html>

