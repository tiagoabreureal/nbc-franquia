<%
vIdPedido_oficial = REQUEST("id_pedido_oficial")
'response.Write(vIdPedido_oficial)
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/mw-style-gera-pedido.css" rel="stylesheet" type="text/css" media="all"/>
<link href='http://fonts.googleapis.com/css?family=Dosis:200,500,300,600,800,400,700' rel='stylesheet' type='text/css'>
		<title>Hinode</title>
		<script language="javascript" type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
		
		<!-- Add fancyBox main JS and CSS files -->
        <script type="text/javascript" src="../fancyBox/source/jquery.fancybox.js?v=2.1.0"></script>
        <link rel="stylesheet" type="text/css" href="../fancyBox/source/jquery.fancybox.css?v=2.1.0" media="screen" />

		<!-- Add Button helper (this is optional) -->
		<link rel="stylesheet" type="text/css" href="../fancyBox/source/helpers/jquery.fancybox-buttons.css?v=1.0.3" />
		<script type="text/javascript" src="../fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.3"></script>
        
		<script language="javascript" type="text/javascript" src="js/jquery.maskedinput.js"></script>
        <script language="javascript" type="text/javascript" src="js/number_format.js"></script>
        <script language="javascript" type="text/javascript" src="js/jquery.printElement.js"></script>
        
        
<script language="javascript" type="text/javascript" src="js/Fc_cdh_hinode_loja_final.js.asp"></script>
		</head>
		<body>
        
        <form name="form1" id="form1" method="post">
<input type="hidden" name="IdPedido_oficial" id="IdPedido_oficial" value="<%=vIdPedido_oficial%>"/>
<input type="hidden" name="Ped_vl_total" id="Ped_vl_total" />
<input type="hidden" name="Ped_vl_frete" id="Ped_vl_frete"/>
<input type="hidden" name="Ped_vl_sub_total" id="Ped_vl_sub_total"/>
<input type="hidden" name="Ped_nome_consultor" id="Ped_nome_consultor"/>
<input type="hidden" name="Ped_idConsultor" id="Ped_idConsultor"/>
<input type="hidden" name="Ped_estado" id="Ped_estado"/>
<input type="hidden" name="Ped_cidade" id="Ped_cidade"/>
<input type="hidden" name="Ped_bairro" id="Ped_bairro"/>
<input type="hidden" name="Ped_compl" id="Ped_compl"/>
<input type="hidden" name="Ped_numero" id="Ped_numero"/>
<input type="hidden" name="Ped_endereco" id="Ped_endereco"/>
<input type="hidden" name="Ped_cep" id="Ped_cep"/>
<input type="hidden" name="Ped_idFormaPagamento" id="Ped_idFormaPagamento"/>
<input type="hidden" name="Ped_QtdaParcela" id="Ped_QtdaParcela"/>

 
            <div class="conteudo-formulario" id="tela">
            
            <div class="mensagem">
            <div id="retorno" style="float:left;"></div>
			<div id="carregando" style="display:none; left;"></div>
            		</div>
                
                <div class="resumo">
                    <div class="titulo">
                        <div class="titulo-int font-bold">Pedido Finalizado
                        	
                        </div>
                    </div>
                    <div class="titulo-int texto_vermelho" style="margin-top: 0px; margin-left: 50px;" id="vl_CdPedido">
                    </div>
                    <div style="float:right; width:140px; font-size:12px; margin-top:5px;">
                            <a href="#" id="bt_print">[IMPRIMIR PEDIDO]</a></div>
                    <div class="dados-col-esq">
                        <ul>
                            <li>
                                <div class="dados-esq">Nome:</div>
                                <div class="dados-dir" id="vl_Ped_nome_consultor"></div>
                            </li>
                           
                            <li>
                                <div class="dados-esq">Tipo:</div>
                                <div class="dados-dir" id="vl_tipo">Normal</div>
                            </li>
                            <li>
                                <div class="dados-esq">Data:</div>
                                <div class="dados-dir" id="vl_Ped_dt_cadastro"></div>
                            </li>
                           
                            <!--
                            <li>
                                <div class="dados-esq">Local de Retira:</div>
                                <div class="dados-dir" id="vl_Ped_local_entr"></div>
                            </li>
                            
                            
                            <li>
                                <div class="dados-esq">Status do Pedido:</div>
                                <div class="dados-dir" id="vl_Ped_idStatus"></div>
                            </li>
                            //-->
                            
                        </ul>
                    </div>
                    <div class="dados-col-dir">
                        <ul>
                            <li>
                                <div class="dados-esq">Sub total pedido :</div>
                                <div class="dados-dir" id="vl_Ped_vl_sub_total"></div>
                            </li>
                            <li>
                                <div class="dados-esq">Frete:</div>
                                <div class="dados-dir" id="vl_Ped_vl_frete"></div>
                            </li>
                            
                            <li>
                                <div class="dados-esq texto_vermelho">Crédito Usado:</div>
                                <div class="dados-dir texto_vermelho" id="vl_credito_user">R$ 0,00</div>
                            </li>
                            
                            <li>
                                <div class="dados-esq">Desconto:</div>
                                <div class="dados-dir" id="vl_desconto">R$ 0,00</div>
                            </li>
                            <li>
                                <div class="dados-esq">Valor total pedido:</div>
                                <div class="dados-dir" id="vl_Ped_vl_total"></div>
                            </li>
                            <li>
                                <div class="dados-esq">Forma de Pagamento:</div>
                                <div class="dados-dir" id="vl_Ped_pagamento"></div>
                            </li>
                            <!--
                            <li>
                                <div class="dados-esq">Pontos:</div>
                                <div class="dados-dir" id="vl_Ped_pontos"></div>
                            </li>
                            //-->
                        </ul>
                    </div>
                    <div class="entrega">
                    <div class="titulo-int" style="margin-top: 0px; margin-left: 50px;">Endereço de entrega</div>
                        <div class="dados-col-esq">
                            <ul>
                                <li>
                                    <div class="dados-esq">Tipo de envio:</div>
                                    <div class="dados-dir" id="vl_Ped_Transporte"></div>
                                </li>
                                <li>
                                    <div class="dados-esq">Rua:</div>
                                    <div class="dados-dir" id="vl_Ped_endereco"></div>
                                </li>
                                <li>
                                    <div class="dados-esq">Numero:</div>
                                    <div class="dados-dir" id="vl_Ped_numero"></div>
                                </li>
                                <li>
                                    <div class="dados-esq">Complemento:</div>
                                    <div class="dados-dir" id="vl_Ped_compl"></div>
                                </li>
                                <li>
                                    <div class="dados-esq">Cep:</div>
                                    <div class="dados-dir" id="vl_Ped_cep"></div>
                                </li>
                                <li>
                                    <div class="dados-esq">Bairro:</div>
                                    <div class="dados-dir" id="vl_Ped_bairro"></div>
                                </li>
                                <li>
                                    <div class="dados-esq">Cidade:</div>
                                    <div class="dados-dir" id="vl_Ped_cidade"></div>
                                </li>
                                <li>
                                    <div class="dados-esq">Estado:</div>
                                    <div class="dados-dir" id="vl_Ped_estado"></div>
                                </li>
                            </ul>
                        </div>
                        
                        
                    </div>

                </div>
                
                
                <div class="conferir-carrinho">
                    <div class="titulo">
                        <div class="codigo">Codigo</div>
                        <div class="produto">Produto</div>
                        <div class="quant">Quant.</div>
                        <div class="valor">Valor Unit.</div>
                        <div class="valor-total">Valor Total</div>
                        <div class="desconto">&nbsp;</div>
                    </div>
                    <div class="carrinho-lista" id="carrinho_lista">
                        
                    </div>
                    
                    
                </div>
                <div class="butao-espaco">
                </div>
            </div>
        </form>      
</body>
</html>