﻿<%


vid_cons = session("rede_integracao") 'REQUEST("id_cons")

'response.Write(vid_cons)

if session("email") <> "" then
vemail1 = Split(session("email"), ";")
vemail = vemail1(0)
end if

vnome_cons = trim(session("descEmpresa")) 'REQUEST("nome_cons")
vmodo_entrega = "1" 'REQUEST("modo_entrega")
vid_cdh_retira = "00000849" '"00000849" 'REQUEST("id_cdh_retira")
vid_cdh_retira_desc = "Hinode Alphaville" 'REQUEST("id_cdh_retira_desc")

vcep = REQUEST("cep")
vendereco = REQUEST("endereco")
vnumero = REQUEST("numero")
vcomplemento = REQUEST("complemento")
vbairro = REQUEST("bairro")
vcidade = REQUEST("cidade")
vestado = REQUEST("estado")

vss_pg = REQUEST("ss_pg")

if vss_pg="" then
	vss_pg = RandNum()
end if


Function RandNum()

'response.write(rnd())
randomize()

Dim randomVar1, randomVar2, randomVar3, currDate, ip 

if Request.ServerVariables("HTTP_X_FORWARDED_FOR") <> "" then 
ip = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
else 
ip = Request.ServerVariables("REMOTE_ADDR")
end if 

ip = Replace(ip, ".", "") 
currDate = Replace(Now(), "/", "")
currDate = Replace(currDate, " ", "")
currDate = Replace(currDate, ":", "")
currDate = Replace(currDate, "AM", "")
currDate = Replace(currDate, "PM", "") 
randomVar1 = (Rnd() * 1000000)
randomVar2 = Sqr(randomVar1)
randomVar3 = Round(1000000 * (randomVar1 * randomVar2)) 

getRandomnum = Round(Rnd() * (currDate & randomVar3 & ip))
getRandomnum = Replace(getRandomnum, ".", "")
getRandomnum = Replace(getRandomnum, ",", "")
getRandomnum = Replace(getRandomnum, "+", "")
getRandomnum = Replace(getRandomnum, "E", "")

RandNum = getRandomnum

End Function

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<title>Hinode - Loja</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="css/mw-style-gera-pedido.css" rel="stylesheet" type="text/css" />
		<link href='https://fonts.googleapis.com/css?family=Dosis:200,500,300,600,800,400,700' rel='stylesheet' type='text/css'>
		<script language="javascript" type="text/javascript" src="js/jquery-1.8.3.min.js"></script>

		<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="../fancyBox/source/jquery.fancybox.js?v=2.1.0"></script>
<link rel="stylesheet" type="text/css" href="../fancyBox/source/jquery.fancybox.css?v=2.1.0" media="screen" />

		<!-- Add Button helper (this is optional) -->
		<link rel="stylesheet" type="text/css" href="../fancyBox/source/helpers/jquery.fancybox-buttons.css?v=1.0.3" />
		<script type="text/javascript" src="../fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.3"></script>

		<script language="javascript" type="text/javascript" src="js/jquery.maskedinput.js"></script>
        
        
<script language="javascript" type="text/javascript" src="js/number_format.js"></script>
                
<script language="javascript" type="text/javascript" src="js/Fc_cdh_hinode_loja.js.asp"></script>

		
        </head>

		<body>
        <form name="form1" id="form1" method="post">
        
        <input type="hidden" name="ss_pg" id="ss_pg" value="<%=vss_pg%>"/>
        <input type="hidden" name="id_cons" id="id_cons" value="<%=vid_cons%>"/>
      	<input type="hidden" name="modo_entrega" id="modo_entrega" value="<%=vmodo_entrega%>"/>
        <input type="hidden" name="nome_cons" id="nome_cons" value="<%=vnome_cons%>"/>
   		<input type="hidden" name="id_cdh_retira" id="id_cdh_retira" value="<%=vid_cdh_retira%>"/>
		<input type="hidden" name="id_cdh_retira_desc" id="id_cdh_retira_desc" value="<%=vid_cdh_retira_desc%>"/>
        <input type="hidden" name="email_ped" id="email_ped" value="<%=vemail%>" />
        
        <input type="hidden" name="cep" id="cep" value="<%=vcep%>"  />
		<input type="hidden" name="endereco" id="endereco" value="<%=vendereco%>" />
		<input type="hidden" name="numero" id="numero" value="<%=vnumero%>" />
		<input type="hidden" name="complemento" id="complemento" value="<%=vcomplemento%>" />
		<input type="hidden" name="bairro" id="bairro" value="<%=vbairro%>" />
		<input type="hidden" name="cidade" id="cidade" value="<%=vcidade%>" />
		<input type="hidden" name="estado" id="estado" value="<%=vestado%>" />
        
        
        
        <input type="hidden" name="vl_total_pedido_frete" id="vl_total_pedido_frete" value="0.00" />
        <input type="hidden" name="vl_total_pedido" id="vl_total_pedido" value="0.00" />
        <input type="hidden" name="vl_sub_total_pedido" id="vl_sub_total_pedido" value="0.00" />
        <input type="hidden" name="pontos_total_pedido" id="pontos_total_pedido" value="0.00" />
        <input type="hidden" name="peso_total_pedido" id="peso_total_pedido" value="" />
        <input type="hidden" name="forma_envio_transp_ped" id="forma_envio_transp_ped" value="" />
        <input type="hidden" name="forma_envio_transp_desc_ped" id="forma_envio_transp_desc_ped" value="" />
        <input type="hidden" name="prazo_transp_ped" id="prazo_transp_ped" value="" />
        
        <input type="hidden" name="vl_credito" id="vl_credito" value="0.00" />
        <input type="hidden" name="vl_credito_usado" id="vl_credito_usado" value="0.00" />
        <input type="hidden" name="txt_tit_prod" id="txt_tit_prod" value="" />
        <input type="hidden" name="txt_desc_prod" id="txt_desc_prod" value="" />
        
        

        
                    <div class="conteudo-formulario">
                    <div class="mensagem">
            <div id="retorno" style="float:left;"></div>
			<div id="carregando" style="display:none; left;"></div>
            		</div>                    
                        <div class="coluna-esquerda">
                            <h1 style="font-weight:bold; margin-left: 20px; margin-top: 15px;">Tipo de Pedido</h1>
                            <br/>
                            <h3 style="margin-left: 25px;">Pedido CDH > Hinode</h3>
                            <br/>
                            <br/>
             <h1 style="font-weight:bold; margin-left: 20px;"> &nbsp;Local de Entrega</h1>
                            <br/>
                     <h3 style="margin-left: 25px; height:auto;" id="mod_entr"></h3>
                    <br/>

<h1 style="margin-left: 20px;" class="font-bold"> &nbsp;Forma de Envio </h1>
<div class="entrega" style="margin-left:10px; height:auto;" id="mostra_forma_envio"></div>
<br />
  
<h1 style="font-weight:bold; margin-left: 20px; margin-top: 10px; font-size: 25px;" id="txt_vl_credito"></h1>

<h1 style="font-weight:bold; margin-left: 20px; margin-top: 10px; font-size: 25px;" id="txt_vl_total_pedido"></h1>

<h1 style="font-weight:bold; margin-left: 20px; margin-top: 10px; font-size: 25px; display:none;" id="txt_pontos_total_pedido"></h1>
                            <br />
                            <div style="margin-left: 20px;">
                                <input type="button" name="bt_continuar" id="bt_continuar"  class="button-finalizar-compra"/>
                            </div>
                            
                        </div>
                        <div class="coluna-direita">
   <div id="coluna-descricao" class="coluna-produtos-descricao" style="display: none;;">
                                <div class="titulo">&nbsp;&nbsp;Produto Descrição</div>
                                <div style="margin-left: 10px; margin-top: 10px; font-weight: bold;">Titulo Produto perfume</div>
                                <div class="detalhes">
                                    <p>Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas Perfume Tradição 10 caixas </p>
                                </div>
                                <div style="margin-left: 20px;">
      <input type="button" id="voltar-produtos" class="button-voltar-produtos" />
                                </div>
                            </div>
                            <div id="categorias">
                                <div class="coluna-tipo">
                                    <div class="titulo">
                                        <div class="categoria">USO</div>
                                    </div>
                                    <div id="list_cat"></div>
                                </div>
                                <div class="coluna-sub-tipo">
                                    <div class="titulo">
                                        <div class="categoria">LINHA</div>
                                    </div>
                                    <div id="list_sub_cat"></div>
                                </div>
                            </div>
                            <div class="coluna-produtos">
                                <div class="titulo">
                                    <div class="coluna-codigo">Código</div>
                                    <div class="produtos-descricao">Produto</div>
                                </div>
                                <div class="produtos-lista" id="list_prod">
                                    
                                </div>
                            </div>
                            <div class="coluna-quantidade">
                                <label for="Quantidade" style=" float:left; margin-top: 8px;">Código Produto:</label>
                                <div style="float: left; margin-top: 8px; width: 100px; margin-right: 20px;">
<input name="txt_prod" type="text" class="imput_codigo" id="txt_prod" value="" maxlength="10"/>
                                </div>
 <label for="Quantidade" style=" float:left; margin-top: 8px;">Quantidade:</label>
                                <div style="float: left; margin-top: 8px;">
<input name="txt_qtd" type="text" id="txt_qtd" value="" maxlength="4"  class="imput_quant" />
                                </div>
                                <div style="float: left;">
        <input name="bt_add_item" type="button" id="bt_add_item"  value="" class="button-add-item"/>
                                </div>
                            </div>
                        </div>
                        <div class="coluna-carrinho">
                            <div class="titulo">
                                
                                <div class="codigo" style="text-align:center;">Codigo</div>
                                <div class="produto" style="text-align:center;">Produto</div>
                                <div class="quant" style="text-align:right;">Quant.</div>
                                <div class="valor" style="text-align:right;">Valor Unit.</div>
                                <div class="valor-total" style="text-align:right;">Valor Total</div>
                                <div class="desconto" style="text-align:right;">&nbsp;</div>
                                <div class="remover" style="text-align:right;">Excluir</div>
                            </div>
                            <div class="carrinho-lista" id="carrinho_lista"></div>
                        </div>
                    </div>
                </form>
</body>
</html>