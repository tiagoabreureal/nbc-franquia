<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->

<%
call Verifica_Permissao(506)

ped	=request("pedido")
if len(strPedido)=0 then ped=40797

strsql=	"select pedicodigo,a.destinatario,b.nome,c.descricao,a.datapedido,b.contato" & _
		",endereco,numero,complemento,bairro,cidade,estado,cep,cgc_cpf,inscricao_rg,email " & _
		"from ped_PedidoTotal a inner join fat_Destinatario b on a.destinatario=b.destinatario " & _
		"inner join fat_hierarquia c on a.rota=c.hierarquia " & _
		"where pedicodigo=" & ped
set adors1=locconnvenda.execute(strsql)
if not adors1.eof then
	d		=adors1("destinatario")
	nome	=adors1("nome")
	data	=adors1("datapedido")
	descricao	=adors1("descricao")
	contato	=adors1("contato")
	endereco	=adors1("endereco") & " " & adors1("numero") & " " & adors1("complemento")
	bairro		=adors1("bairro")
	cidade		=adors1("cidade")
	estado		=adors1("estado")
	cep			=adors1("cep")
	cpf			=adors1("cgc_cpf")
	rg			=adors1("inscricao_rg")
	email		=adors1("email")
end if
set adors1=nothing		
%>
<!--#include virtual='public/pub_BodyOculto.asp'-->

<div align="center">
  <center>
  <table border="0" width="700" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
    <tr>
      <td width="100%" bgcolor="#C0C0C0">
        <p align="center"><font face="Verdana" size="3"><b>CONTRATO DE INSER��O DE PE�A PUBLICIT�RIA</b></font></td>
    </tr>
    <tr>
      <td width="100%">
        <p align="center"><font face="Verdana" size="2"><br>
        Data: <%=day(data)%> / <%=month(data)%> / <%=year(data)%>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Consultor: <%=descricao%></font></td>
    </tr>
    <tr>
      <td width="100%">
        &nbsp;
        <table border="1" width="100%" cellspacing="3" cellpadding="0" style="border-collapse: collapse">
          <tr>
            <td width="100%" bgcolor="#f5f5f5" colspan="4"><font face="Verdana" size="1" color="#0000FF">.DADOS
              CADASTRAIS</font></td>
          </tr>
          <tr>
            <td width="27%"><font face="Verdana" size="1">Contato:</font></td>
            <td width="73%" colspan="3"><font face="Verdana" size="1"><%=contato%></font></td>
          </tr>
          <tr>
            <td width="27%"><font face="Verdana" size="1">Raz�o Social (PJ) /
              Nome (PF):</font></td>
            <td width="73%" colspan="3"><font face="Verdana" size="1"><%=nome%></font></td>
          </tr>
          <tr>
            <td width="27%"><font face="Verdana" size="1">Endere�o:</font></td>
            <td width="73%" colspan="3"><font face="Verdana" size="1"><%=endereco%></font></td>
          </tr>
          <tr>
            <td width="27%"><font face="Verdana" size="1">Bairro:</font></td>
            <td width="30%"><font face="Verdana" size="1"><%=bairro%></font></td>
            <td width="18%" align="right"><font face="Verdana" size="1">Cidade:</font></td>
            <td width="25%"><font face="Verdana" size="1"><%=cidade%></font></td>
          </tr>
          <tr>
            <td width="27%"><font face="Verdana" size="1">UF:</font></td>
            <td width="30%"><font face="Verdana" size="1"><%=estado%></font></td>
            <td width="18%" align="right"><font face="Verdana" size="1">CEP:</font></td>
            <td width="25%"><font face="Verdana" size="1"><%=cep%></font></td>
          </tr>
          <tr>
            <td width="27%"><font face="Verdana" size="1">CNPJ/CPF:</font></td>
            <td width="30%"><font face="Verdana" size="1"><%=cpf%></font></td>
            <td width="18%" align="right"><font face="Verdana" size="1">Insc.
              Estadual/RG:</font></td>
            <td width="25%"><font face="Verdana" size="1"><%=rg%></font></td>
          </tr>
          <tr>
            <td width="27%"><font face="Verdana" size="1">Telefone/Fax:</font></td>
            <td width="30%"><font face="Verdana" size="1">&nbsp</font></td>
            <td width="18%" align="right"><font face="Verdana" size="1">E-mail:</font></td>
            <td width="25%"><font face="Verdana" size="1"><%=email%></font></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td width="100%"><br>
      </td>
    </tr>
    <tr>
      <td width="100%">
        <table border="1" width="100%" cellspacing="3" cellpadding="0" style="border-collapse: collapse">
          <tr>
            <td width="100%" colspan="2" bgcolor="#F5F5F5"><font face="Verdana" size="1" color="#0000FF">.DADOS
              DA PE�A PUBLICIT�RIA</font></td>
          </tr>
          <tr>
            <td width="22%"><font face="Verdana" size="1">Produto:</font></td>
            <td width="78%"><font face="Verdana" size="1"></font></td>
          </tr>
          <tr>
            <td width="22%"><font face="Verdana" size="1">Confec��o do
              An�ncio:</font></td>
            <td width="78%"><font face="Verdana" size="1"></font></td>
          </tr>
          <tr>
            <td width="100%" colspan="2"><font face="Verdana" size="1">Posi��o:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              Per�odo: de&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              a&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              Obs:</font></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td width="100%"><br>
      </td>
    </tr>
    <tr>
      <td width="100%">
        <table border="1" width="100%" cellspacing="3" cellpadding="0" style="border-collapse: collapse">
          <tr>
            <td width="100%" colspan="4" bgcolor="#F5F5F5"><font color="#0000FF" size="1" face="Verdana">.PROGRAMA��O</font></td>
          </tr>
          <tr>
            <td width="27%" align="center"><font size="1" face="Verdana">Edi��o</font></td>
            <td width="28%" align="center"><font size="1" face="Verdana">Formato</font></td>
            <td width="32%" align="center"><font size="1" face="Verdana">Datas
              de Pagamento</font></td>
            <td width="13%" align="center"><font size="1" face="Verdana">Valor
              da Parcela</font></td>
          </tr>
          <tr>
            <td width="27%"><font size="1" face="Verdana"></font></td>
            <td width="28%"><font size="1" face="Verdana"></font></td>
            <td width="32%">
              <p align="center"><font size="1" face="Verdana"></font></td>
  </center>
          <td width="13%">
            <p align="right"><font size="1" face="Verdana"></font></td>
        </tr>
      </table>
    </td>
    </tr>
  <center>
    <tr>
      <td width="100%"><br>
      </td>
    </tr>
    <tr>
      <td width="100%">
        <table border="1" width="100%" cellspacing="3" cellpadding="0" style="border-collapse: collapse">
          <tr>
            <td width="100%" colspan="3" bgcolor="#F5F5F5"><font color="#0000FF" size="1" face="Verdana">.CONDI��ES
              DE PAGAMENTO</font></td>
          </tr>
          <tr>
            <td width="27%"><font size="1" face="Verdana">Quantidade de
              inser��es:</font></td>
            <td width="34%" align="center"><font size="1" face="Verdana"></font></td>
            <td width="39%" rowspan="6">&nbsp;</td>
          </tr>
          <tr>
            <td width="27%"><font size="1" face="Verdana">Quantidade total de
              parcelas:</font></td>
            <td width="34%" align="center"><font size="1" face="Verdana"></font></td>
          </tr>
          <tr>
            <td width="27%"><font size="1" face="Verdana">Valor total do
              contrato: R$</font></td>
            <td width="34%" align="center"><font size="1" face="Verdana"></font></td>
          </tr>
          <tr>
            <td width="27%"><font size="1" face="Verdana">In�cio de pagamento:</font></td>
            <td width="34%" align="center"><font size="1" face="Verdana"></font></td>
          </tr>
          <tr>
            <td width="27%"><font size="1" face="Verdana">Forma de pagamento:</font></td>
            <td width="34%"><font size="1" face="Verdana"></font></td>
          </tr>
          <tr>
            <td width="27%"><font size="1" face="Verdana"><input type="radio" value="V1" name="R1" style="font-family: Verdana; font-size: 8pt">
              Cheques: </font></td>
            <td width="34%"><font size="1" face="Verdana">n�&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              </font></td>
          </tr>
          <tr>
            <td width="27%"><font size="1" face="Verdana"><input type="radio" value="V1" name="R1" style="font-family: Verdana; font-size: 8pt">
              Cart�o de cr�dito:</font></td>
            <td width="73%" colspan="2"><font size="1" face="Verdana">Titular: </font></td>
          </tr>
          <tr>
            <td width="100%" colspan="3"><font size="1" face="Verdana">N� do
              cart�o:
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              C�d. Seg:
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              Validade: </font></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td width="100%"><br>
      </td>
    </tr>
    <tr>
      <td width="100%"><font size="1" face="Verdana">O anunciante declara
        conhecer e concordar com as Condi��es Gerais do Contrato do Anverso
        deste instrumento, de que estas condi��es espec�ficas fazem parte
        integrante. Por assim estarem de acordo, assinam as Partes o presente em
        duas vias de igual teor e forma, obrigando-se, seus herdeiros e
        sucessores, em ju�zo ou fora dele.</font></td>
    </tr>
    <tr>
      <td width="100%"><br>
      </td>
    </tr>
    <tr>
      <td width="100%"><font size="1" face="Verdana">Para pessoa Jur�dica, caso
        o anunciante n�o possua carimbo da empresa ou do CNPJ, � necess�rio
        anexar:<br>
        </font>
        <table border="0" width="100%" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
          <tr>
            <td width="50%"><font size="1" face="Verdana"><input type="radio" value="V1" name="R1" style="font-family: Verdana; font-size: 8pt">C�pia
              do Contrato Social</font></td>
            <td width="50%"><font size="1" face="Verdana"><input type="radio" value="V1" name="R1" style="font-family: Verdana; font-size: 8pt">Cart�o
              do CNPJ</font></td>
          </tr>
          <tr>
            <td width="50%"><font size="1" face="Verdana"><input type="radio" value="V1" name="R1" style="font-family: Verdana; font-size: 8pt">C�pia
              do CPF</font></td>
            <td width="50%"><font size="1" face="Verdana"><input type="radio" value="V1" name="R1" style="font-family: Verdana; font-size: 8pt">C�pia
              de comprovante de resid�ncia</font></td>
          </tr>
        </table>
        <p>&nbsp;</td>
    </tr>
    <tr>
      <td width="100%">
        <table border="1" width="100%" cellspacing="3" cellpadding="0" style="border-collapse: collapse">
          <tr>
            <td width="23%"><font face="Verdana" size="1">Nome do Signat�rio:</font></td>
            <td width="77%">&nbsp;</td>
          </tr>
          <tr>
            <td width="23%"><font face="Verdana" size="1">Cargo do Signat�rio:</font></td>
            <td width="77%">&nbsp;</td>
          </tr>
          <tr>
            <td width="23%"><font face="Verdana" size="1">Representante Legal</font><font face="Verdana" size="1">:</font></td>
            <td width="77%">&nbsp;<font size="1" face="Verdana"><input type="radio" value="V1" name="R1" style="font-family: Verdana; font-size: 8pt">Sim&nbsp;&nbsp;
              <input type="radio" value="V1" name="R1" style="font-family: Verdana; font-size: 8pt">N�o</font></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td width="100%"><br>
        <br>
        <br>
        <br>
        <table border="0" width="100%" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
          <tr>
            <td width="50%" align="center"><font size="1" face="Verdana">_____________________________________________</font></td>
            <td width="50%" align="center"><font size="1" face="Verdana">_____________________________________________</font></td>
          </tr>
          <tr>
            <td width="50%" align="center"><font size="1" face="Verdana">Assinatura
              do Consultor</font></td>
            <td width="50%" align="center"><font size="1" face="Verdana">Carimbo
              e Assinatura do Cliente</font></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </center>
</div>


<p class="quebra">
<div align="center">
  <center>
  <table border="1" width="700" cellpadding="0" cellspacing="3" style="border-collapse: collapse">
    <tr>
      <td><font size="1" face="Verdana">J.B. COMMUNICATION DO BRASIL LTDA - Rua
        Loefgreen 1291, 1� andar - Vila Mariana -S�o Paulo/SP<br>
        CEP: 04040-031 -&nbsp; Tel.: (11) 5575-6286&nbsp; -&nbsp; Fax (11)
        5549-0319<br>
        Divis�o de Distribui��o e Log�stica - Avenida Nazar� 2000 -
        Ipiranga - S�o Paulo/SP<br>
        CEP: 04262-300 -&nbsp; Tel./Fax: (11) 5062-2327<br>
        JAP�O - T113-0021, Tokyo-to, Bunkyo-ku&nbsp; -&nbsp; Honkomagome
        5-70-9&nbsp; -&nbsp; Arita Bld. 4F<br>
        Tel.: (03) 5685-6891&nbsp; -&nbsp; Fax: (03) 5685-6894</font>
        <p>&nbsp;</td>
    </tr>
    <tr>
      <td>
        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><font size="1" face="Verdana">A
        J.B COMMUNICATION DO BRASIL LTDA (Editora JBC) acima qualificada,
        doravante denominada CONTRATADA, e de outro lado, o ANUNCIANTE,
        qualificado no verso deste contrato, t�m como certo e ajustado o
        seguinte:<o:p>
        </o:p>
        </font></span></p>
        <p class="MsoNormal" style="text-align:justify"><font size="1" face="Verdana"><o:p>
        &nbsp;<b><u><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF">CONDI��ES
        GERAIS<o:p>
        </o:p>
        </span></u></b></font></p>
        <p class="MsoNormal" style="text-align:justify"><font size="1" face="Verdana"><b><u><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><o:p>
        <span style="text-decoration:none">&nbsp;</span></span></u></b><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF">01-
        O presente Contrato tem como finalidade a figura��o do Anunciante no
        (s) ve�culo (s) publicit�rio (s) editado (s) pela Contratada,
        especificados no verso.<o:p>
        </o:p>
        </span></font></p>
        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><font size="1" face="Verdana"><o:p>
        &nbsp;02- Declara o signat�rio deste, que est� devidamente autorizado
        a responder ou assinar o presente Contrato, respondendo solidariamente
        pelas obriga��es decorrentes. <o:p>
        </o:p>
        </font></span></p>
        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><font size="1" face="Verdana"><o:p>
        &nbsp;03- O Anunciante fica devidamente esclarecido e ciente que o conte�do
        da figura��o � de sua exclusiva responsabilidade.<o:p>
        </o:p>
        </font></span></p>
        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><font size="1" face="Verdana"><o:p>
        &nbsp;04- A Contratada n�o intervir� em quest�es que possam surgir
        entre o Anunciante e terceiros, a prop�sito de marcas ou quaisquer
        inser��es e mat�rias publicit�rias, cujas caracter�sticas e conte�do
        s�o de responsabilidade exclusiva do Anunciante.<o:p>
        </o:p>
        </font></span></p>
        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><font size="1" face="Verdana"><o:p>
        &nbsp;05- Faz parte integrante deste contrato os anexos com os Originais
        dos An�ncios quando seus preenchimentos forem necess�rios.<o:p>
        </o:p>
        </font></span></p>
        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><font size="1" face="Verdana"><o:p>
        &nbsp;06- O pre�o e as condi��es de pagamento e as condi��es espec�ficas
        do presente contrato est�o discriminadas no verso, devidamente
        assinados pelas partes em 2 (duas) vias.<o:p>
        </o:p>
        </font></span></p>
        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><font size="1" face="Verdana"><o:p>
        &nbsp;07- O Anunciante poder� efetuar os pagamentos deste contrato
        atrav�s de cheques pr�-datados, boletos banc�rios ou cart�es de cr�dito
        ou d�bito. Caso a op��o seja por pagamento em cheques pr�-datados,
        eles devem ser todos nominais em favor da Contratada (J.B Communication
        do Brasil Ltda.).<o:p>
        </o:p>
        </font></span></p>
        <p class="MsoNormal" style="text-align:justify"><font size="1" face="Verdana"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><o:p>
        &nbsp;</span><b style="mso-bidi-font-weight:
normal"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF">PAGAMENTO<o:p>
        </o:p>
        </span></b></font></p>
        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><font size="1" face="Verdana"><o:p>
        &nbsp;08-Os pagamentos ser�o feitos mensalmente, e o pagamento das
        demais parcelas corresponder� sempre ao dia da data do primeiro
        pagamento, conforme especificado no Contrato de Inser��o (verso). Ap�s
        10 (dez) dias do vencimento, em caso de falta de <span style="mso-spacerun:yes">&nbsp;</span>pagamento,
        o t�tulo ser� encaminhado ao cart�rio, e estar� sujeito a protesto.<o:p>
        </o:p>
        </font></span></p>
        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><font size="1" face="Verdana"><o:p>
        &nbsp;09- Na hip�tese de atraso de 2 (duas) parcelas seguidas,
        considerar-se-�o vencidas de pleno direito todas as demais parcelas
        sujeitando-se o Anunciante ao pagamento de multa de 2% por atraso, e
        juros de 1% ao m�s sem preju�zo das demais san��es legais. A
        Contratada poder� cobrar as parcelas convencionadas no Contrato, todo
        ou em parte, mediante saque de t�tulo (s) de cr�dito contra o
        Anunciante.<o:p>
        </o:p>
        </font></span></p>
        <p class="MsoNormal" style="text-align:justify"><font size="1" face="Verdana"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><o:p>
        &nbsp;</span><b style="mso-bidi-font-weight:
normal"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF">A<span style="mso-bidi-font-weight:bold">LTERA��ES
        DOS DADOS DAS PUBLICA��ES<o:p>
        </o:p>
        </span></span></b></font></p>
        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><font size="1" face="Verdana"><o:p>
        &nbsp;10- � de exclusiva responsabilidade do Anunciante manter a
        Contratada permanentemente informada de qualquer altera��o ocorrida
        nos dados por ele fornecidos e constantes no anexo deste contrato.<o:p>
        </o:p>
        </font></span></p>
        <p class="MsoNormal" style="text-align:justify"><font size="1" face="Verdana"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><o:p>
        &nbsp;</span><b><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF">ERROS
        E OMISS�ES<o:p>
        </o:p>
        </span></b></font></p>
        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><font size="1" face="Verdana"><o:p>
        &nbsp;11- No caso de erro ou omiss�o parcial ou total que venham a
        ocorrer na publica��o de dados, informa��es ou figura��es
        autorizadas, a responsabilidade da Contratada limitar-se-� t�o somente
        ao abatimento do pre�o estipulado de acordo com a natureza do erro, e,
        conforme o caso, a devolu��o total dos valores pagos, sem nenhum acr�scimo.<o:p>
        </o:p>
        </font></span></p>
        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><font size="1" face="Verdana"><o:p>
        &nbsp;12- A Contratada n�o se responsabiliza por quaisquer outros preju�zos
        resultantes de tais erros ou omiss�es ou de quaisquer outros contidos
        na (s) publica��o (�es), nem pelos decorrentes em sua distribui��o.<o:p>
        </o:p>
        </font></span></p>
        <p class="MsoNormal" style="text-align:justify"><font size="1" face="Verdana"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><o:p>
        &nbsp;</span><b><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF">RETRATA��O<o:p>
        </o:p>
        </span></b></font></p>
        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><font size="1" face="Verdana"><o:p>
        &nbsp;13- O Anunciante poder� rescindir o presente contrato, sem �nus
        em at� 7 (sete) dias �teis antes do fechamento da edi��o Contratada,
        mediante comunica��o por escrito � Contratada. Ap�s este prazo, o an�ncio
        n�o poder� ser cancelado, ficando desde j� ciente o Anunciante, de
        que dever� cumprir com o valor integral do contrato por se tratar de
        edi��es j� fechadas.<o:p>
        </o:p>
        </font></span></p>
        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><font size="1" face="Verdana"><o:p>
        &nbsp;14- A Contratada devolver� os valores pagos correspondentes ao
        cancelamento em at� 15 (quinze) dias �teis, ap�s recebida a comunica��o
        por escrito, caso o mesmo tenha sido pago antecipadamente.<o:p>
        </o:p>
        </font></span></p>
        <p class="MsoNormal" style="text-align:justify"><font size="1" face="Verdana"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><o:p>
        &nbsp;</span><b><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF">FORO<o:p>
        </o:p>
        </span></b></font></p>
        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-family: Tahoma; mso-bidi-language: #00FF"><font size="1" face="Verdana"><o:p>
        &nbsp;16- Fica desde j� estabelecido que o foro do presente contrato �
        o da sede da Contratada, para dirimir qualquer d�vida decorrente do
        presente contrato, com ren�ncia de qualquer outro por mais privilegiado
        que seja.<o:p>
        </o:p>
        </font></span></td>
    </tr>
  </table>
  </center>
</div>

<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->