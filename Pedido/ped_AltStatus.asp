<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%call Verifica_Permissao(88)%>
<%
strStatus	=request("status")
strObs		=replace(replace(request("obs"),",","."),"'"," ")
strPedido	=request("pedido")
strStatusAtual=request("statusAtual")
strFilial=session("empresa_id")
strForma = request("formapag")

'' verificar se ciclo est� fechado
''---------------------------------------------------------------
strsql="exec fat_ValidaCancelamento_sp " & strPedido & "," & strStatus
set adoVerifica=locConnvenda.execute(strsql)
if not adoVerifica.eof then
    if cstr(adoverifica("cod"))<>"0" then
        call redi_aviso("Cancelar Pedido",adoVerifica("msg"))
        response.end
    end if
end if
set adoVerifica=nothing


tabela_pedido_total			="ped_PedidoTotal"
tabela_pedido_item			="ped_PedidoItem"
tabela_fat_saida			="fat_saida"
tabela_ped_Movimento_status	="ped_MovimentoStatus"
tabela_ped_Observacao		="ped_Observacao"

'' verificar qual tabela est� o pedido
strsql="select pedicodigo from ped_Pedidototal where pedicodigo=" & strPedido & " and emitente='" & session("empresa_id") & "'"
set adoa=locconnvenda.execute(strsql)
if adoa.eof then
	call redi_aviso("Pedido","Pedido n�o localizado")
end if
set adoa=nothing
''


if cstr(strStatusAtual)="99" then
    call redi_aviso("Pedido","N�o � poss�vel alterar pedido cancelado.")
end if

'call rw(strStatusAtual, 1)
'' verificar se tem estoque para liberar pedido
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
strsql="select stnfVerificaPendencia from ped_Statusnf where stnfCodigo=" & strStatusAtual
set adoRs1=locConnvenda.execute(strsql)
if not adors1.eof then VerificaPendenciaAtual= adors1("stnfVerificaPendencia")
set adors1=nothing

strsql="select stnfVerificaPendencia from ped_Statusnf where stnfCodigo=" & strStatus
set adoRs1=locConnvenda.execute(strsql)
if not adors1.eof then VerificaPendenciaNovo= adors1("stnfVerificaPendencia")
set adors1=nothing


'verificar se o usu�rio tem permissao para trocar o status
'--------------------------------------------------------------
strSql=	"select t=count(0) from sys_UsuarioMovCStatus " & _
		"where usuario='" & session("usuario") & "' and stnfCodigo='" & strStatusAtual & "'"
set adoVerificaS=locConnVenda.execute(strSql)
if not adoVerificaS.eof then
	if adoVerificaS("t")=0 then
	'Response.Write "OK"
	'Response.End
		call Redi_Aviso("Acesso Negado","Sem permiss�o para alterar esse status.")
		Response.End
	end if
end if
set adoVerificaS=nothing

'' Verificar se tem nota gerada
strsql="select nota from fat_saida where pedido=" & strPedido
set adoVerificaS=locConnVenda.execute(strSql)
if not adoVerificaS.eof then
    call Redi_Aviso("Acesso Negado","Pedido j� possui a nota-fiscal " & adoVerificaS("nota") & " emitida, n�o � poss�vel alterar o status.")
end if
set adoVerificaS=nothing

strSql="insert into " & tabela_ped_Movimento_status & " " & _
     "(pediCodigo,emitente,stnfCodigo,usuario,DataMovimento,Obs) " & _
     "values " & _
     "(" & strPedido & ",'" & strFilial & "','" & strStatus & "','" & session("usuario") & "',getdate(),'" & strObs & "')"     
'Response.Write strsql
'Response.end  
locConnVenda.execute(strSql)

if len(trim(strForma)) > 0 then
  strSql2="update ped_pedidototal set tipocobranca="&strForma&" where pedicodigo = "&strpedido
  'Response.Write strSql2
  'Response.end  
  locConnVenda.execute(strSql2)  
end if

strSql= "update " & tabela_pedido_total & " set " & _
		"stnfCodigo=" & strStatus & _
		" where pediCodigo=" & strPedido
		'Response.Write strSql
		'Response.End
locConnVenda.execute(strSql)

'CASO O STATUS SEJA PEDIDO RETIRADO DEVE LAN�AR AUTOMATICAMENTE O STATUS NOTA GERADA'
if strStatus = 11 then
    'VERIFICANDO SE J� POSSUI O STATUS NOTA GERADA'
    strsql = "select pedicodigo  from ped_MovimentoStatus where stnfcodigo = 1 and pedicodigo ="&strPedido
    set adoRetirado= conn1.execute(strsql)
    if adoRetirado.eof then
      strSql="insert into " & tabela_ped_Movimento_status & " " & _
        "(pediCodigo,emitente,stnfCodigo,usuario,DataMovimento,Obs) " & _
        "values " & _
        "(" & strPedido & ",'" & strFilial & "','1','" & session("usuario") & "',getdate(),'')"
      conn1.execute(strSql)
      'Response.write strSql&"<br>"

      strSql="update ped_Pedidototal set stnfcodigo = 1 where pedicodigo = "&strPedido
      'Response.write strSql
      conn1.execute(strSql)

      'ATUALIZANDO O ESTOQUE DA FRANQUIA  '
      strSql = "exec dbo.est_estoqueatual_sp_i"
      conn1.execute(strSql)

    end if
    
end if




strDestino="ped_Status.asp?P=" & strPedido & "&Filial=" & strFilial
Response.Redirect strDestino
%>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->