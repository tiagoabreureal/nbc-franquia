<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10342)

destinatario	=trim(request("destinatario"))
produto			=trim(request("produto"))
usu				=trim(request("usu"))
operacao		=trim(request("operacao"))
pagamento		=trim(request("pagamento"))
transportadora	=trim(request("transportadora"))

destinatario	=string(8-len(destinatario),"0") + cstr(destinatario)
if len(produto)>0 then produto=string(6-len(produto),"0") + cstr(produto)

if operacao="-1" then
	d1=""
else
	d1=operacao
end if

lbl1	=""
txt1	=""
lbl2	=""
txt2	=""
lbl3	="Cliente:"
txt3	=destinatario
lbl4	="Produto:"
txt4	=produto
lbl5	="Usu�rio :"
txt5	=usu
lbl6	=""
txt6	=""
%>
<!--#include virtual='public/pub_BodyRel750.asp'-->
<%
impcab = 1

'''ANALISE DE CLIENTE
dim ver(50,4)
dim max
max=11

strSql ="select nome,nome_abreviado,cgc_cpf,a.cep,fisica_juridica,b.status,st=case b.status when 0 then -1 else b.status end,b.descricao,descricao_grupo=isnull(e.descricao,'')" & _
		",uf=isnull(a.estado,''),uf2=case when len(a.estado)=0 then '-1' else isnull(a.estado,'-1') end" & _
		",a.local_entrega,lc=case a.local_entrega when 'S' then -1 else 0 end" & _
		",faixa_preco=isnull(c.faixa_preco,-1),descricao_faixa_preco=isnull(f.descricao,'')" & _
		",grupo=isnull(c.grupo,-1),rota=isnull(c.rota,-1),vendedor=isnull(d.descricao,'') " & _
		"from fat_Destinatario a inner join fat_Destinatario_Status b on a.ativosn=b.status " & _
		"left join fat_comercio c on a.destinatario=c.destinatario " & _
		"left join fat_Hierarquia d on c.rota=d.hierarquia " & _
		"left join fat_grupo e on c.grupo=e.grupo " & _
		"left join fat_grupo f on c.faixa_preco=f.grupo " & _
		"where a.destinatario='" & destinatario & "'"
'call rw(strsql,1)		
set adors1=locconnvenda.execute(strsql)
if adors1.eof then
	max=1
	for i=1 to max
		ver(i,2)="Cliente n�o localizado"
	next
else
	ver(1,1)	="Nome:"
	ver(1,2)	=destinatario & "-" & adors1("nome")
	ver(1,3)	=""
	ver(1,4)	=""	
	ver(2,1)	="Nome Abreviado:"
	ver(2,2)	=adors1("nome_abreviado")
	ver(2,3)	=""
	ver(2,4)	=""
	ver(3,1)	="CPF - CNPJ:"
	ver(3,2)	=adors1("cgc_cpf")
	ver(3,3)	=""
	ver(3,4)	=""
	ver(4,1)	="Tipo Pessoa:"
	ver(4,2)	=adors1("fisica_juridica")
	ver(4,3)	=""
	ver(4,4)	=""
	ver(5,1)	="Status:"
	ver(5,2)	=adors1("descricao")
	ver(5,3)	=adors1("st")
	ver(5,4)	="<font face='verdana' size='1' color='red'>Inativo.<br>N�o � poss�vel fazer pedido para esse cliente</font><br><font face='verdana' size='1' color='blue'>Cliente - Cadastro: mudar status para Ativo</font>"
	
	ver(6,1)	="Local de Entrega:"
	ver(6,2)	=adors1("local_entrega")
	ver(6,3)	=adors1("lc")
	ver(6,4)	="<font face='verdana' size='1' color='red'>Cliente de entrega<br>N�o � poss�vel fazer pedido para esse cliente</font><br><font face='verdana' size='1' color='blue'>Cliente - Cadastro: necess�rio efetuar novo cadastro do Cliente</font>"

	ver(7,1)	="Grupo:"
	ver(7,2)	=adors1("grupo") & "-" & adors1("descricao_grupo")
	ver(7,3)	=adors1("grupo")
	ver(7,4)	="<font face='verdana' size='1' color='red'>Nenhum grupo selecionado<br>Cliente n�o pertence � nenhum agrupamento</font><br><font face='verdana' size='1' color='blue'>Cliente - Cadastro: selecionar o Grupo"

	ver(8,1)	="Faixa de Pre�o:"
	ver(8,2)	=adors1("faixa_preco") & "-" & adors1("descricao_faixa_preco")
	ver(8,3)	=adors1("faixa_preco")
	ver(8,4)	="<font face='verdana' size='1' color='red'>Nenhuma faixa de pre�o selecionada<br>Cliente n�o consegue localizar produtos no pedido</font><br><font face='verdana' size='1' color='blue'>Cliente - Cadastro: selecionar o Grupo"
	
	ver(9,1)	="Vendedor Padr�o:"
	ver(9,2)	=adors1("rota") & "-" & adors1("vendedor")
	ver(9,3)	=adors1("rota")
	ver(9,4)	="<font face='verdana' size='1' color='red'>Nenhum vendedor selecionado</font><br><font face='verdana' size='1' color='blue'>Cliente - Cadastro: selecionar o Vendedor Padr�o"

	ver(10,1)	="UF:"
	ver(10,2)	=adors1("uf")
	ver(10,3)	=adors1("uf2")
	ver(10,4)	="<font face='verdana' size='1' color='red'>UF n�o selecionado</font><br>No c�lculo do pedido valores ficar�o zerados, � usado o UF do cliente para calcular os impostos<br><font face='verdana' size='1' color='blue'>Cliente - Cadastro: selecionar o Estado"
	
	ver(11,1)	="CEP:"
	ver(11,2)	=adors1("cep")
	ver(11,3)	=adors1("cep")
	ver(11,4)	="<font face='verdana' size='1' color='red'>CEP n�o informado</font><br>No pedido, n�o ser� listado as transportadoras para esse cliente<br><font face='verdana' size='1' color='blue'>Cliente - Cadastro: informar o CEP"

	for i=1 to max
		if ver(i,3)=-1 then
			ver(i,3)=-2
		end if
	next
	
	'faixa			=adors1("faixa_preco") & "-" & adors1("descricao_faixa_preco")
	'vendedor		=adors1("rota") & "-" & adors1("vendedor")
end if
set adors1=nothing

%>
<table border="1" width="100%" cellpadding="0" cellspacing="2" style='border-collapse: collapse'>
	<tr><td align="center" width="18%" bgcolor="#c0c0c0"><font face="verdana" size="1">Valida��o</font></td>
		<td align="center" width="40%" bgcolor="#c0c0c0"><font face="verdana" size="1">Conte�do</font></td>
		<td align="center" width="*" bgcolor="#c0c0c0"><font face="verdana" size="1">Mensagem</font></td>
	</tr>
	<tr><td align="left" colspan="3" bgcolor="#f6f6f6"><font face="verdana" size="1"><b>.CLIENTE</b></font></td>
		</tr>
<%
for i=1 to max
	if len(ver(i,1))>0 then
		%>
		<tr><td align="left" width="15%" bgcolor="#f6f6f6"><font face="verdana" size="1"><%=ver(i,1)%></font></td>			
			<%if ver(i,3)=-2 then%>
				<td align="left" width="40%"><font face="verdana" size="1" color="red"><%=ver(i,2)%></font></td>
				<td align="left" width="*"><%=ver(i,4)%></td>
			<%else%>
				<td align="left" width="40%"><font face="verdana" size="1"><%=ver(i,2)%></font></td>
				<td align="left" width="*"><font face="verdana" size="1">&nbsp;</font></td>
			<%end if%>
		</tr>
		<%
	end if
next

'''ANALISE DE PRODUTO
dim prod(13,4)
dim max2 
max2=13

if len(produto)>0 then		
		strsql	="select a.descricao,a.descricao_abreviada,a.inicio" & _
				 ",a.integracao_saida,descricao_integracao_saida=isnull(e.descricao,''),e.tipo pai_tipo,e.tipoestrutura pai_tipoestrutura,e.tipo_publicacao pai_tipo_publicacao " & _
				 ",a.ativo,descricao_ativo=case a.ativo when 1 then 'Ativo' else 'Inativo' end,at=case a.ativo when 0 then -1 else a.ativo end" & _
				 ",a.tipo,tp=case when len(a.tipo)=0 then '-1' else isnull(a.tipo,'-1') end" & _
				 ",linha=isnull(a.linha,-1),descricao_linha=b.descricao " & _
				 ",tipoestrutura=isnull(a.tipoestrutura,-1),descricao_tipoestrutura=c.descricao " & _
				 ",tipo_publicacao=isnull(a.tipo_publicacao,-1),descricao_tipo_publicacao=d.descricao " & _
				 "from sys_produto a left join fat_LinhaProduto b on a.linha=b.linha " & _
				 "left join pcp_RefTipoEstrutura c on a.tipoestrutura=c.tipoestrutura " & _
				 "left join sys_tipo_publicacao d on a.tipo_publicacao=d.tipo_publicacao " & _
				 "left join sys_produto e on a.integracao_saida=e.produto " & _
				 "where a.produto='" & produto & "'"
		'call rw(strsql,1)
		set adors1=locconnvenda.execute(strsql)
		if not adors1.eof then
			prod(1,1)	="Descri��o:"
			prod(1,2)	=produto & "-" & adors1("descricao")
			prod(1,3)	=""
			prod(1,4)	=""	
			prod(2,1)	="Abreviada:"
			prod(2,2)	=adors1("descricao_abreviada")
			prod(2,3)	=""
			prod(2,4)	=""	
			prod(3,1)	="Tipo:"
			prod(3,2)	=adors1("tipo")
			prod(3,3)	=adors1("tp")
			prod(3,4)	="<font face='verdana' size='1' color='red'>Tipo de Produto n�o selecionado</font><br>N�o localiza produto no pedido (vinculado com a opera��o)<br><font face='verdana' size='1' color='blue'>Produto - Cadastro: selecionar o Tipo de Produto"
			prod(4,1)	="Status:"
			prod(4,2)	=adors1("descricao_ativo")
			prod(4,3)	=adors1("at")
			prod(4,4)	="<font face='verdana' size='1' color='red'>Produto Inativo</font><br>N�o localiza produto no pedido<br><font face='verdana' size='1' color='blue'>Produto - Cadastro: ativar produto"
			prod(5,1)	="Linha:"
			prod(5,2)	=adors1("linha") & "-" & adors1("descricao_linha")
			prod(5,3)	=adors1("linha")
			prod(5,4)	="<font face='verdana' size='1' color='red'>Linha n�o selecionada</font><br>Produto n�o � listado nos relat�rios por Linha<br><font face='verdana' size='1' color='blue'>Produto - Cadastro: selecionar linha"
			prod(6,1)	="Estrutura:"
			prod(6,2)	=adors1("tipoestrutura") & "-" & adors1("descricao_tipoestrutura")
			prod(6,3)	=adors1("tipoestrutura")
			prod(6,4)	="<font face='verdana' size='1' color='red'>Tipo de Estrutura n�o selecionada</font><br>N�o localiza produto do pedido (permiss�o da estrutura por usu�rio)<br><font face='verdana' size='1' color='blue'>Produto - Cadastro: selecionar Estrutura / Produto - Tipo de Estrutura x Usuario: verificar permiss�o"
			prod(7,1)	="Publica��o:"
			prod(7,2)	=adors1("tipo_publicacao") & "-" & adors1("descricao_tipo_publicacao")
			prod(7,3)	=adors1("tipo_publicacao")
			prod(7,4)	="<font face='verdana' size='1' color='red'>Tipo de Publica��o n�o selecionada</font><br>Produto n�o � listado nos relat�rios por Tipo de Publica��o<br><font face='verdana' size='1' color='blue'>Produto - Cadastro: selecionar tipo de Publica��o"
			prod(8,1)	="Produto Pai:"
			prod(8,2)	=adors1("integracao_saida") & "-" & adors1("descricao_integracao_saida")
			prod(8,3)	=adors1("integracao_saida")
			prod(8,4)	="<font face='verdana' size='1' color='red'>Produto Pai (C�digo Sa�da) n�o selecionado</font><br>Produto n�o pertence a nenhum outro produto, problema em pedidos de Assinatura/Publicidade<br><font face='verdana' size='1' color='blue'>Produto - Cadastro: informar o c�digo do produto Pai"
			prod(9,1)	="Produto Pai Tipo:"
			prod(9,2)	=adors1("pai_tipo")
			prod(9,3)	=adors1("pai_tipo")
			prod(9,4)	="<font face='verdana' size='1' color='red'>Produto PAI necessariamente precisa ficar como tipo='A'</font><br>Problema em pedidos de Assinatura/Publicidade<br><font face='verdana' size='1' color='blue'>Produto - Cadastro: selecionar produto Pai e mudar tipo para 'A'"
			
			prod(10,1)	="Produto Pai Publica��o:"
			prod(10,2)	=adors1("pai_tipo_publicacao")
			prod(10,3)	=adors1("pai_tipo_publicacao")
			prod(10,4)	="<font face='verdana' size='1' color='red'>Tipo de Publica��o do Produto PAI � diferente do Tipo de Publica��o do Produto Filho</font><br>Pode gerar diferen�as em relat�rios de Tipo de Publicidade<br><font face='verdana' size='1' color='blue'>Produto - Cadastro: selecionar produto Pai e corrigir Tipo de Publica��o"
			
			prod(13,1)	="Lan�amento:"
			prod(13,2)	=adors1("inicio")
			prod(13,3)	=adors1("inicio")
			prod(13,4)	="<font face='verdana' size='1' color='red'>Lan�amento do produto � data superior a data atual</font><br>N�o exibe o produto na tela de pedido<br><font face='verdana' size='1' color='blue'>Produto - Cadastro: alterar a data de In�cio para uma data inferior ou igual a data atual"
		end if
		set adors1=nothing
		
		prod(11,1)	="Pre�o:"
		strsql="select preco_base from fat_faixa_preco where produto='" & produto & "' and faixa=" & ver(8,3)
		set a=locconnvenda.execute(strsql)
		if a.eof then
			prod(11,2)	="SEM PRE�O"
			prod(11,3)	=-2
			prod(11,4)	="<font face='verdana' size='1' color='red'>Produto sem pre�o cadastrado para a faixa do cliente [" & ver(8,2) & "]</font><font face='verdana' size='1' color='blue'>Produto - Faixa de Pre�o: cadastrar pre�o para esse produto nessa faixa"
		else
			prod(11,2)	=a("preco_base")
			prod(11,3)	=""
			prod(11,4)	=""
		end if
		set a=nothing
		
		prod(12,1)	="Estoque:"
		strsql="select estoque from view_estoque_atual where produto='" & produto & "'"
		set a=locconnvenda.execute(strsql)
		if a.eof then
			prod(12,2)	="N�O LOCALIZADO"
			prod(12,3)	=-2
			prod(12,4)	="<font face='verdana' size='1' color='red'>Nenhum estoque localizado para esse produto</font><font face='verdana' size='1' color='blue'><br>Verificar relat�rios de estoque para confirmar"
		else
			prod(12,2)	=a("estoque")
			prod(12,3)	=""
			prod(12,4)	=""
		end if
		set a=nothing
		

		for i=1 to max2
			if prod(i,3)=-1 then
				prod(i,3)=-2
			end if
		next
		
		if prod(8,3)=produto then
			if prod(9,2)<>prod(3,2) then
				prod(9,3)=-2
			end if
		end if
		
		if prod(10,2)<>prod(7,3) then
			prod(10,3)=-2		
		end if
		
		if cdate(prod(13,2))>cdate(date) then
			prod(13,3)=-2		
		end if

		%>
		<tr><td align="left" colspan="3" bgcolor="#FAF0E6"><font face="verdana" size="1"><b>.PRODUTO</b></font></td>
		</tr>
		<%

		for i=1 to max2
			if len(prod(i,1))>0 then
				%>
				<tr><td align="left" width="15%" bgcolor="#FAF0E6"><font face="verdana" size="1"><%=prod(i,1)%></font></td>
					<%if prod(i,3)=-2 then%>
						<td align="left" width="40%"><font face="verdana" size="1" color="red"><%=prod(i,2)%></font></td>
						<td align="left" width="*"><%=prod(i,4)%></td>
					<%else%>
						<td align="left" width="40%"><font face="verdana" size="1"><%=prod(i,2)%></font></td>
						<td align="left" width="*"><font face="verdana" size="1">&nbsp;</font></td>
					<%end if%>
				</tr>
				<%
			end if
		next
end if

'''ANALISE DE OPERA��O
dim op(10,4)
dim max3 
max3=3

if len(operacao)>0 and cstr(operacao)<>"-1" then		
		strsql	="select operacao,descricao from fat_operacao where operacao=" & operacao
		'call rw(strsql,1)
		set adors1=locconnvenda.execute(strsql)
		if not adors1.eof then
			op(1,1)	="Descri��o:"
			op(1,2)	=operacao & "-" & adors1("descricao")
			op(1,3)	=""
			op(1,4)	=""	
		end if
		set adors1=nothing
		
		op(2,1)	="Permiss�o Usu�rio:"
		strsql= "select a.operacao from ped_grupo a inner join sys_usuario b on a.grupo_pedido=b.grupo_pedido " & _
				"where b.usuario='" & usu & "' and a.operacao=" & operacao
		set a=locconnvenda.execute(strsql)
		if a.eof then			
			op(2,2)	="N�O"
			op(2,3)	=-2
			op(2,4)	="<font face='verdana' size='1' color='red'>Usu�rio [" & usu & "] n�o tem permiss�o nessa opera��o (Grupo do Usu�rio)</font><br><font face='verdana' size='1' color='blue'>Verificar permiss�o: Pedido - Grupo de Usu�rio x Forma de Pagamento"	
		else
			op(2,2)	="SIM"
			op(2,3)	=""
			op(2,4)	=""	
		end if
		
		op(3,1)	="Opera��o x Tipo Produto:"
		if len(trim(prod(3,2)))>0 then			
			strsql="select operacao from ped_operacaoTipoProduto where operacao=" & operacao & " and tipo_produto='" & prod(3,2) & "'"
			set a=locconnvenda.execute(strsql)
			if a.eof then
				op(3,2)	="N�O"
				op(3,3)	=-2
				op(3,4)	="<font face='verdana' size='1' color='red'>Opera��o n�o permite visualizar o tipo de produto [" & prod(3,2) & "]</font><br><font face='verdana' size='1' color='blue'>Verificar permiss�o: Pedido - Opera��o x Tipo de Produto"	
			else
				op(3,2)	="SIM"
				op(3,3)	=""
				op(3,4)	=""	
			end if
			set a=nothing
		else			
			op(3,2)	="Nenhum produto informado"
			op(3,1)	=""
			op(3,1)	=""
		end if

		%>
		<tr><td align="left" colspan="3" bgcolor="#FFE4E1"><font face="verdana" size="1"><b>.OPERA��O</b></font></td>
		</tr>
		<%

		for i=1 to max3
			if len(op(i,1))>0 then
				%>
				<tr><td align="left" width="15%" bgcolor="#FFE4E1"><font face="verdana" size="1"><%=op(i,1)%></font></td>
					
					<%if op(i,3)=-2 then%>
						<td align="left" width="40%"><font face="verdana" size="1" color="red"><%=op(i,2)%></font></td>
						<td align="left" width="*"><%=op(i,4)%></td>
					<%else%>
						<td align="left" width="40%"><font face="verdana" size="1"><%=op(i,2)%></font></td>
						<td align="left" width="*"><font face="verdana" size="1">&nbsp;</font></td>
					<%end if%>
				</tr>
				<%
			end if
		next
end if

'''ANALISE DE TRANSPORTADORA
dim transp(10,4)
dim max4 
max4=3

if len(transportadora)>0 and cstr(transportadora)<>"-1" then		
		strsql	="select transporte,descricao from fat_transporte where transporte=" & transportadora
		'call rw(strsql,1)
		set adors1=locconnvenda.execute(strsql)
		if not adors1.eof then
			transp(1,1)	="Descri��o:"
			transp(1,2)	=transportadora & "-" & adors1("descricao")
			transp(1,3)	=""
			transp(1,4)	=""	
		end if
		set adors1=nothing
		
		transp(2,1)	="Faixa de CEP:"
		strsql	="select cep_inicial,cep_final " & _
				 "from fat_Frete " & _
				 "where cep_inicial<='" & ver(11,3) & "' and cep_final>='" & ver(11,3) & "' and transporte=" & transportadora
				 'call rw(strsql,1)
		set a=locconnvenda.execute(strsql)
		if a.eof then
			transp(2,2)	="N�O"
			transp(2,3)	=-2
			transp(2,4)	="<font face='verdana' size='1' color='red'>Transportadora n�o atende a faixa de CEP do cliente</font><br><font face='verdana' size='1' color='blue'>Verificar: CEP do cliente (Cliente - Cadastro) / Verificar: faixa de CEP atendida pela transportadora (Estoque - Transportadora) "
		else
			transp(2,2)	=a("cep_inicial") & "-" & a("cep_final")
			transp(2,3)	=""
			transp(2,4)	=""
		end if
		set a=nothing		
		
		transp(3,1)	="CEP - Grupo:"
		strsql	="select grupo " & _
				 "from fat_Frete_grupo " & _
				 "where transporte=" & transportadora & " and grupo=" & ver(7,3)
				 'call rw(strsql,1)
		set a=locconnvenda.execute(strsql)
		if a.eof then
			transp(3,2)	="N�O"
			transp(3,3)	=-2
			transp(3,4)	="<font face='verdana' size='1' color='red'>N�o liberado a Transportadora para o grupo do cliente [" & ver(7,2) & "]</font><br><font face='verdana' size='1' color='blue'>Cliente - Cadastro: verificar grupo do cliente / Estoque - Transportadora: verificar permiss�o para o grupo do cliente"
		else
			transp(3,2)	="LIBERADO"
			transp(3,3)	=""
			transp(3,4)	=""
		end if
		set a=nothing					 
		
		%>
		<tr><td align="left" colspan="3" bgcolor="#DEB887"><font face="verdana" size="1"><b>.TRANSPORTADORA</b></font></td>
		</tr>
		<%

		for i=1 to max4
			if len(transp(i,1))>0 then
				%>
				<tr><td align="left" width="15%" bgcolor="#DEB887"><font face="verdana" size="1"><%=transp(i,1)%></font></td>
					
					<%if transp(i,3)=-2 then%>
						<td align="left" width="40%"><font face="verdana" size="1" color="red"><%=transp(i,2)%></font></td>
						<td align="left" width="*"><%=transp(i,4)%></td>
					<%else%>
						<td align="left" width="40%"><font face="verdana" size="1"><%=transp(i,2)%></font></td>
						<td align="left" width="*"><font face="verdana" size="1">&nbsp;</font></td>
					<%end if%>
				</tr>
				<%
			end if
		next
end if

'''ANALISE DE FORMA DE PAGAMENTO
dim pag(10,4)
dim max5 
max5=4

if len(pagamento)>0 and cstr(pagamento)<>"-1" then		
		strsql	="select tipo,descricao from fat_tipo_cobranca where tipo=" & pagamento
		'call rw(strsql,1)
		set adors1=locconnvenda.execute(strsql)
		if not adors1.eof then
			pag(1,1)	="Descri��o:"
			pag(1,2)	=pagamento & "-" & adors1("descricao")
			pag(1,3)	=""
			pag(1,4)	=""	
		end if
		set adors1=nothing
		
		pag(2,1)	="Pagamento - Grupo Cliente:"
		strsql	="select tipo " & _
				 "from fat_opcao_cobranca " & _
				 "where tipo=" & pagamento & " and grupo=" & ver(7,3)
				 'call rw(strsql,1)
		set a=locconnvenda.execute(strsql)
		if a.eof then
			pag(2,2)	="N�O"
			pag(2,3)	=-2
			pag(2,4)	="<font face='verdana' size='1' color='red'>Forma de Pagamento n�o liberada para o Grupo do Cliente [" & ver(7,2) & "]</font><br><font face='verdana' size='1' color='blue'>Pedido - Grupo de Cliente x Forma de Pagamento: verificar permiss�o para o grupo do cliente"
		else
			pag(2,2)	="LIBERADO"
			pag(2,3)	=""
			pag(2,4)	=""
		end if
		set a=nothing	
		
		pag(3,1)	="Pagamento - Filtro Opera��o:"
		strsql	="select tipo " & _
				 "from fat_opcao_cobranca_operacao " & _
				 "where operacao=" & operacao
				 'call rw(strsql,1)
		set a=locconnvenda.execute(strsql)
		if a.eof then
			pag(3,2)	="LIBERADO"
			pag(3,3)	=""
			pag(3,4)	=""
			set a=nothing
		else	
			set a=nothing
			strsql	="select tipo " & _
				 "from fat_opcao_cobranca_operacao " & _
				 "where operacao=" & operacao & " and tipo=" & pagamento
			set a=locconnvenda.execute(strsql)	
			if a.eof then						
				pag(3,2)	="N�O"
				pag(3,3)	=-2
				pag(3,4)	="<font face='verdana' size='1' color='red'>Filtro Ativado para essa opera��o. Opera��o n�o permite listar esas forma de pagamento.</font><br><font face='verdana' size='1' color='blue'>Pedido - Opera��o x Forma de Pagamento: verificar permiss�o"
			else
				pag(3,2)	="LIBERADO"
				pag(3,3)	=""
				pag(3,4)	=""
			end if
			set a=nothing
		end if
		
		pag(4,1)	="Pagamento - Filtro Usu�rio:"
		strsql	="select b.grupo_pedido " & _
				 "from fat_opcao_cobranca_usuario a inner join sys_usuario b on a.grupo_usuario=b.grupo_pedido " & _
				 "where b.usuario='" & usu & "'"
				 'call rw(strsql,1)
		set a=locconnvenda.execute(strsql)
		if a.eof then
			pag(4,2)	="LIBERADO"
			pag(4,3)	=""
			pag(4,4)	=""
			set a=nothing
		else	
			set a=nothing
			strsql	="select tipo " & _
				 "from fat_opcao_cobranca_usuario a inner join sys_usuario b on a.grupo_usuario=b.grupo_pedido " & _
				 "where b.usuario='" & usu & "' and tipo=" & pagamento
			set a=locconnvenda.execute(strsql)	
			if a.eof then						
				pag(4,2)	="N�O"
				pag(4,3)	=-2
				pag(4,4)	="<font face='verdana' size='1' color='red'>Filtro Ativado para o Usu�rio. Grupo do Usu�rio n�o permite listar essa Forma de Pagamento.</font><br><font face='verdana' size='1' color='blue'>Pedido - Grupo de Usu�rio x Forma de Pagamento: verificar permiss�o"
			else
				pag(4,2)	="LIBERADO"
				pag(4,3)	=""
				pag(4,4)	=""
			end if
			set a=nothing
		end if
		
		%>
		<tr><td align="left" colspan="3" bgcolor="#FAFAD2"><font face="verdana" size="1"><b>.FORMA DE PAGAMENTO</b></font></td>
		</tr>
		<%

		for i=1 to max5
			if len(pag(i,1))>0 then
				%>
				<tr><td align="left" width="15%" bgcolor="#FAFAD2"><font face="verdana" size="1"><%=pag(i,1)%></font></td>
					
					<%if pag(i,3)=-2 then%>
						<td align="left" width="40%"><font face="verdana" size="1" color="red"><%=pag(i,2)%></font></td>
						<td align="left" width="*"><%=pag(i,4)%></td>
					<%else%>
						<td align="left" width="40%"><font face="verdana" size="1"><%=pag(i,2)%></font></td>
						<td align="left" width="*"><font face="verdana" size="1">&nbsp;</font></td>
					<%end if%>
				</tr>
				<%
			end if
		next
end if
%>	
</table></td></tr></table>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->