<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(506)
%><!--#include virtual='public/pub_BodyOculto.asp'-->
<%

ped	=rec("Ped")

if len(ped)=0 then call redi_aviso("Pedido","Nenhum pedido informado")

'strsql="ped_ImprimirPedidoCD_sp '10091159',282717,'ADMIN',1"
strsql="ped_ImprimirPedidoCD_sp '" & session("empresa_id") & "'," & ped & ",'" & session("usuario") & "',1"
'response.write strsql
'response.end

set adoRs1=locConnvenda.execute(strsql)
if adors1.eof then
	call redi_aviso("Pedido","Pedido n�o localizado")
else
	nome		=adors1("integracao") & "-" & mid(adors1("nome"),1,30)
	valorFrete	=adors1("valorfrete")
	transporte	=adors1("transp")
	cobranca	=adors1("pagamento")
	credito		=adors1("credito")
	
	if transporte="RETIRAR NO CD" then transporte=""
end if

		
%>

<table border=0 width='400' align=center cellpadding=1 cellspacing=1>

	<tr>
		<td align=center><font face=verdana size=2><hr size=2><br><b>HINODE COSM�TICOS</b><br><hr size=2></font></td>
	</tr>
	
	<tr>
		<td align=center><font face=verdana size=2>P E D I D O</font></td>
	</tr>
	
	<tr>
		<td align=left><font face=verdana size=2><br>
			<br>Pedido:	<b><%=formatnumber(ped,0)%></b>
			<br>Data:	<%=now%>
			<br>Destinat�rio
			<br><%=nome%>
			</font>
		</td>
	</tr>
	
	<%
	'' ITENS
	
	%>
	
	<tr>
		<td colspan=4>
			<table border=0 width=100% cellpadding=0 cellspacing=0>
				<tr>
					<td align=center width=50%><font face=verdana size=2>Produto</font></td>
					<td align=center width=15%><font face=verdana size=2>Unit</font></td>
					<td align=center width=15%><font face=verdana size=2>Qtd</font></td>
					<td align=center width=20%><font face=verdana size=2>Total</font></td>
				</tr>
				<%
				
				valor_total=cdbl(valorfrete)-cdbl(credito)
				total_pontos=0
				tot_desconto=0
				
				do while not adoRs1.eof
					produto		=adors1("produto")
					descricao	=adors1("descricao")
					precoproduto=adors1("precoproduto")
					qtd			=adors1("qtd")
					valormercadoria	=adors1("valortotalitem")
					pontos_calculados_i=adors1("pontos_calculados_i")
					
					valor_total=cdbl(valor_total) + cdbl(valormercadoria)
					total_pontos=cdbl(total_pontos) + cdbl(pontos_calculados_i)
					
					tot_desconto=cdbl(tot_desconto) + cdbl(adors1("valorDesconto"))
				%>
				<tr>
					<td align=left colspan=4><font face=verdana size=2><%=produto %>-<%=descricao%></font></td>					
				</tr>
				<tr>
					<td align=center><font face=verdana size=2>&nbsp;</font></td>
					<td align=right><font face=verdana size=2><%=formatnumber(precoproduto,2)%></font></td>
					<td align=right><font face=verdana size=2><%=formatnumber(qtd,0)%></font></td>
					<td align=right><font face=verdana size=2><%=formatnumber(valormercadoria,2)%></font></td>
				</tr>
				<%
					adoRs1.movenext
				loop
				set adors1=nothing
				
				if cdbl(tot_desconto)>0 then					
				%>
					<tr>
						<td colspan=3><font face=verdana size=2>Desconto Ativa��o</font></td>
						<td align=right><font face=verdana size=2>- <%=formatnumber(tot_Desconto,2)%></font></td>
					</tr>
				<%
				end if
				
				%>
				<tr>
					<td colspan=3><font face=verdana size=2>Frete (<%=transporte%>)</font></td>
					<td align=right><font face=verdana size=2><%=formatnumber(valorFrete,2)%></font></td>
				</tr>
				<%if cdbl(credito)>0 then %>
					<tr>
						<td colspan=3><font face=verdana size=2>Desconto (cr�dito): </font></td>
						<td align=right><font face=verdana size=2><%=formatnumber(cdbl(credito) * -1,2)%></font></td>
					</tr>
				<%end if%>
				<tr><td colspan=4><hr size=2></td></tr>
				<tr>
					<td colspan=3><font face=verdana size=2><B>TOTAL PEDIDO:</B></font></td>
					<td align=right><font face=verdana size=2><B>R$ <%=formatnumber(valor_total,2)%></font></td>
				</tr>
				<tr><td colspan=4><hr size=2></td></tr>
				<tr>
					<td colspan=3><font face=verdana size=2>Total Pontos:</font></td>
					<td align=right><font face=verdana size=2><%=formatnumber(total_pontos,2)%></font></td>
				</tr>
				<tr><td colspan=4><hr size=2></td></tr>
				<tr>
					<td colspan=4><font face=verdana size=2>Forma Pagamento: <%=cobranca%></font></td>					
				</tr>
				
				<tr>
					<td colspan=4> <center> <font face=verdana size=2>N�o V�lido como Cupom Fiscal</font> </center></td>					
				</tr>
				
				
				<tr><td colspan=4><hr size=2></td></tr>
				
			</table>
		</td>
	</tr>
	
</table>
		

</table></td></tr></table>
<!--#include virtual="public/pub_FimOculto.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->
