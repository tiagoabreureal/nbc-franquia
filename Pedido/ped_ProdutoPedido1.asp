<%@LANGUAGE = VBScript.Encode %>
<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%call Verifica_Permissao(10032)

'Define as colunas e titulos do gerar arquivo

	session("arquivo_titulo")=""   
	session("arquivo_campo")=""
	
	configura_arquivo=true 
	session("arquivo_titulo")="Pedido,Nome,Emiss�o,Opera��o,Qtd,Pre�o,Status,Usu�rio,Estoque"   
	session("arquivo_campo")="pedicodigo,nome_abreviado,datapedido,descricao,qtd,precoproduto,stnfdescricao,usuario,estoque"

	

filial	=request("filial")
strIni	=request("strINI")
strFim	=request("strFIM")
produto	=request("produto")
descricao=replace(replace(trim(request("descricao")),"'"," ")," ","%")
vendedor = request("hierarquia")
operacao = replace(request("operacao")," ","")
status	= request("status")
resumo	=request("resumo")
p_usuario=trim(request("p_usuario"))
cobranca	=trim(request("cobranca"))

todosProdutos = request("todosProdutos")

mostrarNf = request("MostrarNF")



if len(p_usuario)=0 then
	d="TODOS"
else
	d=p_usuario
end if

if len(trim(produto))=0 and	len(trim(descricao))=0 and len(todosProdutos) = 0 then
	session("msg")="Informe o produto que deseja pesquisar (c�digo do produto ou descri��o do produto)."
	Response.Redirect "ped_ProdutoPedido.asp"
end if

if len(vendedor) = 0 then vendedor = 0
if len(operacao) = 0 then operacao = 999

if len(trim(filial))=0 then filial="0"


lbl1 = "Emiss�o:" 
txt1 = now
lbl2 = "" 
txt2 = ""
lbl3 = "Per�odo:"
txt3 = strIni & "-" & strFim
lbl4 = ""
txt4 = ""
lbl5 = ""
txt5 = ""
lbl6 = ""
txt6 = ""





strSql	="select a.pediCodigo,d.nome_Abreviado,dataPedido=convert(datetime,convert(varchar(10),datapedido,120)), c.stnfDescricao,qtd,precoproduto,e.estoque,a.usuario" & _
		 ",a.emitente,a.destinatario,c.cor,o.descricao,p.descricao descProduto " & _
		 ",nota=isnull(s.nota,''),serie=isnull(s.serie,'') " & _
		 "from ped_PedidoTotal a inner join ped_PedidoItem b on a.emitente=b.emitente and a.pediCodigo=b.pediCodigo " & _
		 "inner join fat_operacao o on b.tipooperacao=o.operacao " & _
		 "inner join sys_produto p on b.prodcodigo=p.produto " & _
		 "left join ped_StatusNF c on a.stnfCodigo=c.stnfCodigo " & _
		 "left join fat_Destinatario d on a.destinatario=d.destinatario " & _
		 "left join view_Estoque_Atual e on a.emitente=e.emitente and b.prodCodigo=e.produto " & _
		 "left join fat_saida s ON (s.pedido=a.pediCodigo) " & _
		 "where (a.emitente='" & filial & "' or '0'='" & filial & "') and (prodcodigo='" & produto & "' or " & len(produto) & "=0) " & _
		 " and (p.descricao like '%" & descricao & "%')" & _
		 " and datapedido between '" & dt_fim(strIni) & "' and '" & dt_fim(strFim) & "' " & _
		 " and (a.rota=" & vendedor & " or " & vendedor & "=0) " & _
		 " and (a.stnfcodigo=" & status & " or '" & status & "'='9999') " & _
		 " and (a.tipocobranca=" & cobranca & " or " & cobranca & "=-1) " & _
		 " and (a.usuario='" & p_usuario & "' or " & len(p_usuario) & "=0) " & _
		 " and tipooperacao in(" & operacao & ") " & _
		 "union " & _
"select a.pediCodigo,d.nome_Abreviado,dataPedido=convert(datetime,convert(varchar(10),datapedido,120)), c.stnfDescricao,qtd,precoproduto,e.estoque,a.usuario" & _
		 ",a.emitente,a.destinatario,c.cor,o.descricao,p.descricao descProduto " & _
		 ",nota=isnull(s.nota,''),serie=isnull(s.serie,'') " & _
		 "from ped_PedidoTotal_Hist a inner join ped_PedidoItem_Hist b on a.emitente=b.emitente and a.pediCodigo=b.pediCodigo " & _
		 "inner join fat_operacao o on b.tipooperacao=o.operacao " & _
		 "inner join sys_produto p on b.prodcodigo=p.produto " & _
		 "left join ped_StatusNF c on a.stnfCodigo=c.stnfCodigo " & _
		 "left join fat_Destinatario d on a.destinatario=d.destinatario " & _
		 "left join view_Estoque_Atual e on a.emitente=e.emitente and b.prodCodigo=e.produto " & _
		 "left join fat_saida s ON (s.pedido=a.pediCodigo) " & _
		 "where (a.emitente='" & filial & "' or '0'='" & filial & "') and (prodcodigo='" & produto & "' or " & len(produto) & "=0) " & _
		 " and (p.descricao like '%" & descricao & "%')" & _
		 " and datapedido between '" & dt_fim(strIni) & "' and '" & dt_fim(strFim) & "' " & _
		 " and (a.rota=" & vendedor & " or " & vendedor & "=0) " & _
		 " and (a.stnfcodigo=" & status & " or '" & status & "'='9999') " & _
		 " and (a.tipocobranca=" & cobranca & " or " & cobranca & "=-1) " & _
		 " and tipooperacao in(" & operacao & ") " & _
		 " and (a.usuario='" & p_usuario & "' or " & len(p_usuario) & "=0) " & _
		 "order by p.descricao,a.pedicodigo desc"

'Response.Write strsql
'Response.end
if len(trim(request("btn2"))) > 0 then
	call Redi_Arquivo(strSql,"")
end if
set adoRs1=locConnVenda.execute(strSql)


call define_coluna ("pedido","pedicodigo","3","c","0","n","n","","")

if(mostrarNf="S") then
  call define_coluna ("nota","nota","3","c","0","n","n","","")
end if

call define_coluna ("abreviado","nome_abreviado","*","e","t","n","s","","")
call define_coluna ("Emiss�o","datapedido","9","c","t","n","s","","")
call define_coluna ("Opera��o","descricao","17","e","t","n","s","","")
call define_coluna ("Qtd","qtd","5","d","0","s","n","","")
call define_coluna ("Pre�o","precoproduto","7","d","2","s","n","","")
call define_coluna ("status","stnfdescricao","13","c","t","n","s","$cor","")
call define_coluna ("usu�rio","usuario","12","c","t","n","s","","")
call define_coluna ("Estoque","estoque","4","d","0","n","n","","")

call define_quebra("Total ","descProduto","s","descProduto")

call define_link(1,"ped_Pedido.asp?ped=[pedicodigo]&em=[emitente]")


if(mostrarNf="S") then
  call define_link(2,"../faturamento/fat_Nf.asp?nf=[nota]&se=[serie]&filial=[emitente]")
  call define_link(3,"../cliente/cli_Dados.asp?dest=[destinatario]")
else
  call define_link(2,"../cliente/cli_Dados.asp?dest=[destinatario]")
end if


call define_link(7,"javascript:AbreStatus([pedicodigo],'[emitente]')")

impCab=1
call fun_rel_cab()	
do while not adoRs1.eof
  	intConta=intConta+1
   	call fun_rel_det()
  	adoRs1.movenext
  loop
  call fun_fim_rel()

if resumo="1" then  
		  %></table></td></tr>
		  <p class="quebra">
		<tr><td width="100%"><br>
		<%
		call zerar_relatorio()

		strSql2	="select c.stnfDescricao,qtd=sum(qtd)" & _
				 ",c.cor,o.descricao,p.descricao descProduto " & _
				 "from ped_PedidoTotal a inner join ped_PedidoItem b on a.emitente=b.emitente and a.pediCodigo=b.pediCodigo " & _
				 "inner join fat_operacao o on b.tipooperacao=o.operacao " & _
				 "inner join sys_produto p on b.prodcodigo=p.produto " & _
				 "left join ped_StatusNF c on a.stnfCodigo=c.stnfCodigo " & _
				 "where (a.emitente='" & filial & "' or '0'='" & filial & "') and (prodcodigo='" & produto & "' or " & len(produto) & "=0) " & _
				 " and (p.descricao like '%" & descricao & "%')" & _
				 " and datapedido between '" & dt_fim(strIni) & "' and '" & dt_fim(strFim) & "' " & _
				 " and (rota=" & vendedor & " or " & vendedor & "=0) " & _
				 " and (a.stnfcodigo=" & status & " or '" & status & "'='9999') " & _
				 " and (a.usuario='" & p_usuario & "' or " & len(p_usuario) & "=0) " & _
				 " and tipooperacao in(" & operacao & ") group by c.stnfdescricao,c.cor,o.descricao,p.descricao " & _
				 "union " & _
		"select c.stnfDescricao,qtd=sum(qtd)" & _
				 ",c.cor,o.descricao,p.descricao descProduto " & _
				 "from ped_PedidoTotal_Hist a inner join ped_PedidoItem_Hist b on a.emitente=b.emitente and a.pediCodigo=b.pediCodigo " & _
				 "inner join fat_operacao o on b.tipooperacao=o.operacao " & _
				 "inner join sys_produto p on b.prodcodigo=p.produto " & _
				 "left join ped_StatusNF c on a.stnfCodigo=c.stnfCodigo " & _
				 "where (a.emitente='" & filial & "' or '0'='" & filial & "') and (prodcodigo='" & produto & "' or " & len(produto) & "=0) " & _
				 " and (p.descricao like '%" & descricao & "%')" & _
				 " and datapedido between '" & dt_fim(strIni) & "' and '" & dt_fim(strFim) & "' " & _
				 " and (rota=" & vendedor & " or " & vendedor & "=0) " & _
				 " and (a.stnfcodigo=" & status & " or '" & status & "'='9999') " & _
				 " and (a.usuario='" & p_usuario & "' or " & len(p_usuario) & "=0) " & _
				 " and tipooperacao in(" & operacao & ") group by c.stnfdescricao,c.cor,o.descricao,p.descricao " & _
				 "order by p.descricao,c.stnfdescricao"

		set adoRs1=locConnVenda.execute(strSql2)

		call define_coluna ("Produto","descProduto","*","e","t","n","s","","")
		call define_coluna ("Opera��o","descricao","20","e","t","n","s","","")
		call define_coluna ("Qtd","qtd","8","d","0","s","s","","")
		call define_coluna ("status","stnfdescricao","15","c","t","n","s","$cor","")

		strparametrotab1="<tr><td width='100%' colspan='4'><font face='verdana' size='1'><b>Resumo por Opera��o / Status</b></font></td></tr>"
		call fun_rel_cab()	
		do while not adoRs1.eof
		  	intConta=intConta+1
		   	call fun_rel_det()
		  	adoRs1.movenext
		  loop
		  call fun_fim_rel()
end if
  %></table></td></tr></table>

<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->

<script>
function AbreStatus(strD,strFil)	{
	window.open("ped_Status.asp?p=" + strD + "&Filial=" + strFil,"Status","scrollbars=yes,toobar=no,width=600,height=320,screenX=0,screenY=0,top=100,left=100");	
}
</script>

