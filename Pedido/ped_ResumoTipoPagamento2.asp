<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10427)

data	=request("data")
tipo	=request("tipo")
status  =request("status")
f		=request("f")
ciclo	=request("ciclo")

impcab = 1
'strSql ="select a.pedicodigo,a.destinatario,datapedido,a.nfreferencia,stnfdescricao,d.cor" & _
'		",c.nome,total=sum(valortotalitem)+valorfrete" & _
'		",f.descricao tipocob " & _
'		"from ped_Pedidototal a inner join ped_pedidoitem b on a.pedicodigo=b.pedicodigo " & _
'		"inner join fat_destinatario c on a.destinatario=c.destinatario "& _
'		"inner join ped_Statusnf d on a.stnfcodigo=d.stnfcodigo " & _
'		"inner join fat_operacao e on b.tipooperacao=e.operacao " & _
'		"inner join fat_tipo_cobranca f on a.tipocobranca=f.tipo " & _
'		"where a.emitente='" & session("empresa_id") & "' and convert(datetime,convert(varchar(10),datapedido,120))='" & dt_fim(data) & "' and venda=1 " & _
'		"and (a.tipocobranca in(" & tipo & ") or '" & tipo & "'='-1') " & _
'		"and a.stnfcodigo in(" & status & ") " & _
'		" group by f.descricao,a.destinatario,a.pedicodigo,datapedido,d.cor,nfreferencia,c.nome,valorfrete,stnfdescricao " & _
'		"order by a.pedicodigo"

if len(ciclo)>0 then
	if f="1" then
		strSql ="select a.emitente,a.pedicodigo,a.destinatario,datapedido=convert(varchar(10),a.datapedido,103),datafat=g.referencia,a.nfreferencia,stnfdescricao,d.cor" & _
				",c.nome,total=sum(valor_total) + convert(numeric(18,2),isnull(j.complemento,0))  " & _
				",f.descricao tipocob " & _
				"from ped_Pedidototal a inner join fat_destinatario c on a.destinatario=c.destinatario "& _
				"inner join ped_Statusnf d on a.stnfcodigo=d.stnfcodigo " & _
				"inner join fat_tipo_cobranca f on a.tipocobranca=f.tipo " & _
				"inner join fat_saida g on a.pedicodigo=g.pedido " & _
				"inner join fat_operacao h on g.operacao=h.operacao " & _
				"inner join rede_parametros i on a.ciclo=i.ciclo " & _
				"left join ped_observacao j on a.pedicodigo=j.pedicodigo and observacao=211 " & _
				"where i.inicio='" & dt_fim(ciclo) & "' and a.emitente='" & session("empresa_id") & "' and convert(datetime,convert(varchar(10),g.referencia,120))='" & dt_fim(data) & "' " & _
				"and (a.tipocobranca in(" & tipo & ") or '" & tipo & "'='-1') " & _
				"and a.stnfcodigo in(" & status & ") and g.situacao=0 and h.venda=1 and g.produto<>'999999' " & _
				" group by j.complemento,a.emitente,f.descricao,a.destinatario,a.datapedido,a.pedicodigo,g.referencia,d.cor,nfreferencia,c.nome,valorfrete,stnfdescricao " & _
				"order by a.pedicodigo"
	else		
		strSql ="select a.emitente,a.pedicodigo,a.destinatario,datapedido=convert(varchar(10),a.datapedido,103)" & _
				",datafat='',a.nfreferencia,stnfdescricao,d.cor" & _
				",c.nome,total=sum(valortotalnf+valorfrete) + convert(numeric(18,2),isnull(j.complemento,0))" & _
				",f.descricao tipocob " & _
				"from ped_Pedidototal a inner join fat_destinatario c on a.destinatario=c.destinatario "& _
				"inner join ped_Statusnf d on a.stnfcodigo=d.stnfcodigo " & _
				"inner join fat_tipo_cobranca f on a.tipocobranca=f.tipo " & _
				"inner join rede_parametros i on a.ciclo=i.ciclo " & _
				"left join ped_observacao j on a.pedicodigo=j.pedicodigo and observacao=211 " & _
				"where i.inicio='" & dt_fim(ciclo) & "' and a.emitente='" & session("empresa_id") & "' and convert(datetime,convert(varchar(10),datapedido,120))='" & dt_fim(data) & "' " & _
				"and (a.tipocobranca in(" & tipo & ") or '" & tipo & "'='-1') " & _
				"and a.stnfcodigo in(" & status & ") " & _
				" group by j.complemento,a.emitente,f.descricao,a.destinatario,a.datapedido,a.pedicodigo,datapedido,d.cor,nfreferencia,c.nome,valorfrete,stnfdescricao " & _
				"order by a.pedicodigo"
	end if
else
	if f="1" then
		strSql ="select a.emitente,a.pedicodigo,a.destinatario,datapedido=convert(varchar(10),a.datapedido,103),datafat=g.referencia,a.nfreferencia,stnfdescricao,d.cor" & _
				",c.nome,total=sum(valor_total) + convert(numeric(18,2),isnull(j.complemento,0))" & _
				",f.descricao tipocob " & _
				"from ped_Pedidototal a inner join fat_destinatario c on a.destinatario=c.destinatario "& _
				"inner join ped_Statusnf d on a.stnfcodigo=d.stnfcodigo " & _
				"inner join fat_tipo_cobranca f on a.tipocobranca=f.tipo " & _
				"inner join fat_saida g on a.pedicodigo=g.pedido " & _
				"inner join fat_operacao h on g.operacao=h.operacao " & _
				"left join ped_observacao j on a.pedicodigo=j.pedicodigo and observacao=211 " & _
				"where a.emitente='" & session("empresa_id") & "' and convert(datetime,convert(varchar(10),g.referencia,120))='" & dt_fim(data) & "' " & _
				"and (a.tipocobranca in(" & tipo & ") or '" & tipo & "'='-1') " & _
				"and a.stnfcodigo in(" & status & ") and g.situacao=0 and h.venda=1 and g.produto<>'999999' " & _
				" group by j.complemento,a.emitente,f.descricao,a.destinatario,a.datapedido,a.pedicodigo,g.referencia,d.cor,nfreferencia,c.nome,valorfrete,stnfdescricao " & _
				"order by a.pedicodigo"
	else		
		strSql ="select a.emitente,a.pedicodigo,a.destinatario,datapedido=convert(varchar(10),a.datapedido,103)" & _
				",datafat='',a.nfreferencia,stnfdescricao,d.cor" & _
				",c.nome,total=sum(valortotalnf+valorfrete)+ convert(numeric(18,2),isnull(j.complemento,0))" & _
				",f.descricao tipocob " & _
				"from ped_Pedidototal a inner join fat_destinatario c on a.destinatario=c.destinatario "& _
				"inner join ped_Statusnf d on a.stnfcodigo=d.stnfcodigo " & _
				"inner join fat_tipo_cobranca f on a.tipocobranca=f.tipo " & _
				"left join ped_observacao j on a.pedicodigo=j.pedicodigo and observacao=211 " & _
				"where a.emitente='" & session("empresa_id") & "' and convert(datetime,convert(varchar(10),datapedido,120))='" & dt_fim(data) & "' " & _
				"and (a.tipocobranca in(" & tipo & ") or '" & tipo & "'='-1') " & _
				"and a.stnfcodigo in(" & status & ") " & _
				" group by j.complemento,a.emitente,f.descricao,a.destinatario,a.datapedido,a.pedicodigo,datapedido,d.cor,nfreferencia,c.nome,valorfrete,stnfdescricao " & _
				"order by a.pedicodigo"
	end if
end if
'call rw(strsql,0)
if len(trim(request("btn2"))) > 0 then call Redi_Arquivo(strSql,"")
set adoRs1=locConnVenda.execute(strSql)
if not adors1.eof then
	descricao=adors1("tipocob")
end if

lbl1	=""
txt1	=""
lbl2	=""
txt2	=""
lbl3	="Tipo:"
txt3	=descricao
lbl4	=""
txt4	=""

call define_coluna ("Pedido","pedicodigo","7","c","0","n","s","","")
call define_coluna ("Site","nfreferencia","7","c","0","n","s","","")
call define_coluna ("Data Pedido","datapedido","12","c","t","n","s","","")
call define_coluna ("Faturamento","datafat","12","c","t","n","s","","")
call define_coluna ("Nome","Nome","*","e","t","n","s","","")
call define_coluna ("Valor","total","10","d","2","s","s","","")
call define_coluna ("Status","stnfdescricao","20","c","t","n","s","$cor","")

call define_link(1,"ped_Pedido.asp?ped=[pedicodigo]")
call define_link(2,"ped_Pedido.asp?ped=[pedicodigo]")
call define_link(5,"../cliente/cli_dados.asp?dest=[destinatario]")
call define_link(7,"javascript:AbreStatus([pedicodigo],'[emitente]','" & session("usuario") & "'," & session("atividade") & ")")

'call define_LinkCab(1,"ped_lista1.asp?ordem=pediCodigo&pesquisar=" & strCampo & "&strini=" & strIni & "&strFim=" & strfim & "&Status=" & Status & "&Filial=" & Filial & "&cobranca=" & cob & "&tipo=" & tipo)
'call define_quebra ("TOTAL ", "nome_abreviado","s","nome_abreviado")
'call define_link(1,"ped_ResumoTipoPagamento2.asp?data=[data]&tipo=4&status=")
call fun_rel_cab()
do while not adoRs1.eof
	intTotal=intTotal+1
	call fun_rel_det()
	adoRs1.movenext
loop
call fun_fim_rel()
call Func_ImpPag(1,iColuna)
%>
</table></td></tr></table>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->

<script>
function AbreStatus(strD,strFil,c,d)	{
	window.open("ped_Status.asp?xatividade=" + d + "&p=" + strD + "&Filial=" + strFil + "&xusuario=" + c,"Status","scrollbars=yes,toobar=no,width=600,height=320,screenX=0,screenY=0,top=100,left=100");	
						}						
</script>