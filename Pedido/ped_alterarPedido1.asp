<%'@LANGUAGE = VBScript.Encode %>
<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10385)

pedido	=request("pedido")

strsql=	"select nome,c.descricao transp,d.descricao cob,e.descricao vendedor" & _
		",a.rota,trancodigo=isnull(a.trancodigo,0),a.tipocobranca "
if session("empresa_id")="00000008" then
	strsql=strsql & ",ciclo "
else
	strsql=strsql & ",ciclo=0 "
end if
strsql=strsql & "from ped_pedidoTotal a inner join fat_destinatario b on a.destinatario=b.destinatario " & _
		"left join fat_transporte c on a.trancodigo=c.transporte " & _
		"left join fat_tipo_cobranca d on a.tipocobranca=d.tipo " & _
		"left join fat_hierarquia e on a.rota=e.hierarquia " & _
		"where pedicodigo=" & pedido
'Response.Write strsql
'Response.end		
set adors1=locconnvenda.execute(strsql)
if not adors1.eof then
	nome	=adors1("nome")
	transp	=adors1("transp")
	cob		=adors1("cob")
	vendedor=adors1("vendedor")
	
	rota	=adors1("rota")
	trancodigo=adors1("trancodigo")
	tipo	=adors1("tipocobranca")
	
	ciclo=adors1("ciclo")
end if

lbl1	=""
txt1	=""
lbl2	=""
txt2	=""
lbl3	="Pedido:"
txt3	="<a href='ped_Pedido.asp?ped=" & pedido & "'>" & formatnumber(pedido,0) & "</a>"
lbl4	=""
txt4	=""
lbl5	="Nome:"
txt5	="<b>" & nome

impcab = 1

if session("empresa_id")<>"00000008" then
	call define_coluna ("Vendedor","vendedor","6","c","t","n","s","","")
end if
call define_coluna ("Transportadora","transp","6","c","t","n","s","","")
call define_coluna ("Forma Pagamento","cob","6","c","t","n","s","","")

call form_criar("frm1","ped_alterarPedido2.asp")
	call txtOculto_criar("pedido",pedido)
	call txtOculto_criar("opcao","P")	
call fun_rel_cab()
do while not adoRs1.eof
	call fun_rel_det()
	adoRs1.movenext
loop
call fun_fim_rel()
%>
</td></tr>

<%
if ucase(session("combo_vendedor")) = "S" then
		%>

		<tr><td colspan="3" align="left"><font face="verdana" size="1" color="red"><b>Alterar para:</b></font></td></tr>
		<tr>
			<td>
			<%
			strsql="select hierarquia,descricao from fat_hierarquia order by descricao"
			set adors1=locconnvenda.execute(strsql)
			call select_criar("vendedor","")
				do while not adors1.eof
					if cstr(adors1("hierarquia"))=cstr(rota) then
						s="selected"
					else
						s=""
					end if
					
					call select_item(adors1("hierarquia"),adors1("descricao"),s)
					adors1.movenext
				loop
				set adors1=nothing
			call select_fim()
			%></td>
			<td>
			<%
			strsql="select transporte,descricao from fat_transporte order by descricao"
			set adors1=locconnvenda.execute(strsql)
			call select_criar("transporte","")
				do while not adors1.eof
					if cstr(adors1("transporte"))=cstr(trancodigo) then
						s="selected"
					else
						s=""
					end if
					
					call select_item(adors1("transporte"),adors1("descricao"),s)
					adors1.movenext
				loop
				set adors1=nothing
			call select_fim()
			%></td>
			<td>
			<%
			strsql="select tipo,descricao from fat_tipo_cobranca order by descricao"
			set adors1=locconnvenda.execute(strsql)
			call select_criar("cobranca","")
				do while not adors1.eof
					if cstr(adors1("tipo"))=cstr(tipo) then
						s="selected"
					else
						s=""
					end if
					
					call select_item(adors1("tipo"),adors1("descricao"),s)
					adors1.movenext
				loop
				set adors1=nothing
			call select_fim()
			%></td>
		</table></tr>
		<tr><td colspan="3" align="center"><font face="verdana" size="1">
		<%call button_criar("submit","btn1","Alterar Dados B�sicos","")%></font></td></tr>

		<%		
end if
call form_fim()

if session("empresa_id")="00000008" then
	strsql="select mes=month(inicio),ano=year(inicio) from rede_parametros where ciclo=" & ciclo
	set adors1=locconnvenda.execute(strsql)
	if not adors1.eof then
		ciclo_mes	=adors1("mes")
		ciclo_ano	=adors1("ano")
	end if
	set adors1=nothing

	call form_criar("frm1","ped_alterarPedido2.asp")
		call txtOculto_criar("pedido",pedido)
		call txtOculto_criar("opcao","C")
		call txtOculto_criar("ciclo_anterior",ciclo)
%>
<tr><td colspan="3" align="center"><font face="verdana" size="1"><hr size="1"></td></tr>
<tr><td colspan="3" align="center">
	<table border="0" width="100%" cellpadding="2" cellspacing="1">
		<tr><td width="20%" bgcolor="#c0c0c0"><font face="verdana" size="1">Ciclo do Pedido:</font></td>
			<td width="12%"><font face="verdana" size="1">
				<%=ciclo_mes & "/" & ciclo_ano%></font></td>			
		</tr>
		<tr><td width="20%" bgcolor="#c0c0c0"><font face="verdana" size="1">Alterar para:</font></td>
			<td width="12%"><font face="verdana" size="1">
				<%call txt_criar("inteiro","ciclo_mes",2,2,ciclo_mes)
				  call txt_criar("inteiro","ciclo_ano",4,4,ciclo_ano)
				%></font></td>
			<td align="left"><font face="verdana" size="1"><%call button_criar("submit","btn1","Gravar","")%></font></td>
		</tr>
	</table>
	</td>
</tr>
<%
	call form_fim()
end if
call Func_ImpPag(0,iColuna)
%></table>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->