<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->

<%	
call Verifica_Permissao(10547)

 pediCodigo  = replace(trim(request("ped")),"'","")
 tipo        = replace(trim(request("tipo")),"'","")
 retorno        = replace(trim(request("retorno")),"'","")
 
 
''' validar se n�o est� autorizando um produto e j� tem kit.
  strsql="select b.destinatario,b.cgc_cpf " & _
        "from ped_Pedidototal a inner join view_Consultor b on a.destinatario=b.destinatario " & _
        "where a.pedicodigo=" & pediCodigo & " and b.ativosn=0"
 set adors1=locConnvenda.execute(strsql)
 if not adors1.eof then
    d       =adors1("destinatario")
    cgc_cpf =adors1("cgc_cpf")
    
    strsql="select integracao,nome from view_Consultor where cgc_cpf='" & cgc_cpf & "' and destinatario<>'" & d & "' and ativosn=1"
    set adors2=locConnVenda.execute(strsql)
    if not adoRs2.eof then
        call redi_Aviso("CPF j� cadastrado: " & cgc_cpf,"O pedido n�o pode ser liberado porque j� existe um cadastro ATIVO para esse cliente (" & adors2("integracao") & " - " & adors2("nome") & "). Entre em contato com a Hinode para maiores detalhes.")
        response.end
    end if
    set adors2=nothing
                          
 end if
 set adors1=nothing    
 
 
''' validar se n�o est� autorizando um pedido de KIT e o cpf j� est� ativo.
  strsql="select b.destinatario,b.cgc_cpf " & _
        "from ped_Pedidototal a inner join view_Consultor b on a.destinatario=b.destinatario " & _
        "inner join ped_Pedidoitem c on a.pedicodigo=c.pedicodigo " & _
        "inner join sys_produto d on c.prodcodigo=d.produto " & _
        "where a.pedicodigo=" & pediCodigo & " and d.linha=15 and d.classificacao=9 and d.produto<>'007000'"
 set adors1=locConnvenda.execute(strsql)
 if not adors1.eof then
    d       =adors1("destinatario")
    cgc_cpf =adors1("cgc_cpf")
    
    strsql="select integracao,nome from view_Consultor where cgc_cpf='" & cgc_cpf & "' and ativosn=1"
    set adors2=locConnVenda.execute(strsql)
    if not adoRs2.eof then

        '' verificar se � upgrade.
        '' se n�o tiver ocorrencia de upgrade, n�o deixa prosseguir.
        strsql="select pedicodigo from ped_Observacao where pedicodigo=" & pediCodigo & " and observacao=10032"
        set adors3=locConnVenda.execute(strsql)
        if adoRs3.eof then
            call redi_Aviso("Pedido de KIT","O pedido de KIT " & pediCodigo & " n�o pode ser liberado porque o cadastro do consultor j� est� ATIVO.")
            response.end
        end if
        set adoRs3=nothing

        
    end if
    set adors2=nothing
    
    strsql= "select a.pediCodigo " & _
            "from ped_PedidoTotal a inner join ped_PedidoItem b on a.pedicodigo=b.pedicodigo " & _
            "inner join sys_produto c on b.prodcodigo=c.produto " & _
            "inner join view_consultor d on a.destinatario=d.destinatario " & _
            "where d.cgc_cpf='" & cgc_cpf & "' and c.linha=15 and c.classificacao=9 and c.produto<>'007000' "  & _
            " and a.stnfcodigo in(1,10) and d.ativosn=1"            
    set adors2=locConnVenda.execute(strsql)
    if not adoRs2.eof then

        '' verificar se � upgrade.
        '' se n�o tiver ocorrencia de upgrade, n�o deixa prosseguir.
        strsql="select pedicodigo from ped_Observacao where pedicodigo=" & pediCodigo & " and observacao=10032"
        set adors3=locConnVenda.execute(strsql)
        if adoRs3.eof then
            call redi_Aviso("Pedido de KIT","O pedido de KIT " & pediCodigo & " n�o pode ser liberado porque j� existe outro Pedido Aprovado (de Kit) para esse cliente (" & adors2("pediCodigo") & "). Entre em contato com a Hinode para maiores detalhes.")
            response.end
        end if
        set adoRs3=nothing


        
    end if
    set adors2=nothing
                          
 end if
 set adors1=nothing  

 
 
 'strsql="select pediCodigo from ped_Pedidototal where pediCodigo=" & pediCodigo & " and stnfcodigo in(1,2)"
 strsql="select top 1 pedido from fat_saida where pedido=" & pediCodigo
 set adoVerificaPedido=locConnVenda.execute(strsql)
 if not adoVerificaPedido.eof then
 
    if retorno="1" then
        session("msg")="Pedido j� est� faturado, n�o � poss�vel alterar sua forma de pagamento ou status."
	    Response.Redirect "../faturamento/fat_GestaoFaturamento1.asp"
	    response.end
    else
	    Response.Redirect "ped_liberarPagamentoCD.asp"
    end if
 
 end if
 set adoVerificaPedido=nothing

'''''''''''''''''''''''''''''''''''''''''''''''''''''''
'' validar estoque
'''''''''''''''''''''''''''''''''''''''''''''''''''''''
statusAtual=0
strSql="select b.stnfVerificaPendencia from ped_PedidoTotal a inner join ped_StatusNf b on a.stnfCodigo=b.stnfCodigo where a.pediCodigo=" & pediCodigo
set adoVerificaPedido=locConnVenda.execute(strsql)
if not adoVerificaPedido.eof then

''' quando est� mudando status (empenhado) para outro status empenhado, deve-se somar a qtd do item pq a view de estoque j� est� descontando esse item.
    soma=0
    if adoVerificaPedido("stnfVerificaPendencia")=true or cstr(pediCodigo)="3074673" then soma=1

    Erro="0"
    strSql="exec ped_ValidarEstoque_sp '" & session("empresa_id") & "'," & pediCodigo & "," & soma
  '  if session("usuario")="ADMIN" then
     '   call rw(strsql,1)
    '    response.end
  '  end if
    set adoEstoque=locConnvenda.execute(strsql)
    if not adoEstoque.eof then
        Erro=adoEstoque("cod")
        msg =adoEstoque("msg")
    end if
    set adoEstoque=nothing

    if cstr(Erro)<>"0" then
        call redi_Aviso("Estoque",msg)
    end if



   ' strSql="select emitente,prodCodigo,qtd=sum(qtd) from ped_PedidoItem a inner join fat_Operacao b on a.tipoOperacao=b.operacao " & _
   '         "where a.pediCodigo=" & pediCodigo & " and a.prodCodigo not like '007%' and a.prodcodigo<>'020001' and b.venda=1 group by emitente,prodcodigo"
   '         'call rw(strsql,1)
   ' set adoItens=locConnvenda.execute(strsql)
    'do while not adoItens.eof
   '     emitente_estoque=adoItens("emitente")
   '     produto         =adoItens("prodCodigo")
    '    qtd             =adoItens("qtd")
    
    '    saldo=0
    '    strsql="select estoque=sum(estoque) from hinode_loja.dbo.view_ListaProdutoEstoque where empresa='" & emitente_estoque & "' and Codigo='" & produto & "'"
    '    set adoEstoque=locConnVenda.execute(strsql)
   '     if not adoEstoque.eof then saldo=adoEstoque("estoque")                
    '    set adoEstoque=nothing
'
    '    if cdbl(soma)=1 then saldo=cdbl(saldo) + cdbl(qtd)
    '            
   '     if cdbl(qtd) > cdbl(saldo) then call redi_Aviso("Estoque","O produto " & produto & " n�o tem estoque (Estoque: " & saldo & ") para esse pedido. Pedido n�o pode ser alterado para esse status.")
   '     
   '     adoItens.movenext        
   ' loop
   ' set adoItens=nothing    
    
end if
set adoVerificaPedido=nothing

'' Fim Estoque
 

 if( tipo="maquineta" ) then
  
  strsql="INSERT INTO ped_movimentoStatus " & _
        " (pediCodigo,emitente,stnfCodigo,usuario,dataMovimento,Obs) " & _
		" VALUES " & _
		" ( " & _
		"   '" &pediCodigo& "' " & _
		"  ,'" &session("empresa_id")& "' " & _
		"  ,'68' " & _
		"  ,'" &session("usuario")& "'" & _
		"  ,getdate() " & _
		"  ,'Pedido liberado. (ped_liberarPagamentoCD2.asp). Tipo=Maquineta.' " & _
		" ) "
		'response.write strsql&"<br>"
		set adoRs1=locconnvenda.execute(strsql)	
 
 end if
 
				
 strsql="UPDATE ped_pedidoTotal SET stnfCodigo='10' WHERE pediCodigo='" &pediCodigo& "' and emitente='" & session("empresa_id") & "'"
 'response.write strsql&"<br>"
 set adoRs1=locconnvenda.execute(strsql)
				
 strsql="INSERT INTO ped_movimentoStatus " & _
        " (pediCodigo,emitente,stnfCodigo,usuario,dataMovimento,Obs) " & _
		" VALUES " & _
		" ( " & _
		"   '" &pediCodigo& "' " & _
		"  ,'" &session("empresa_id")& "' " & _
		"  ,'10' " & _
		"  ,'" &session("usuario")& "'" & _
		"  ,getdate() " & _
		"  ,'Pedido liberado. (ped_liberarPagamentoCD2.asp)' " & _
		" ) "
		'response.write strsql&"<br>"
		set adoRs1=locconnvenda.execute(strsql)	
				
 			
 strsql="SELECT top 1 destinatario,b.prodcodigo FROM ped_pedidoTotal a inner join ped_PedidoItem b on a.pedicodigo=b.pedicodigo " & _
        "WHERE a.pediCodigo=" &pediCodigo& " and a.emitente='" & session("empresa_id") & "' and b.prodcodigo like '007%'"
  set adoRs1=locconnvenda.execute(strsql)
  
  if not adoRs1.eof then
    destinatario = adoRs1("destinatario")    

    strSql = "UPDATE fat_destinatario SET ativoSN='1',usuario_altera='" &session("usuario")& "' WHERE destinatario='" &destinatario& "' and ativosn=0"
    locconnvenda.execute(strsql)    
    
    if mid(adors1("prodcodigo"),1,4)="0072" then
        strsql="insert into fat_destinatario_ocorrencia (destinatario,ocorrencia,complemento,usuario,data,dt_movimento) values " & _
				"('" & destinatario & "',218,'Ativado ap�s confirma��o de pagamento no CDH, pedido sistema " & pediCodigo & "','" & session("usuario") & "',GETDATE(),GETDATE())"		
        locconnvenda.execute(strsql) 
    end if
    
'    ''se n�o tiver nenhuma nota emitida pro cliente, atualiza data-cadastro
'	strsql="select c=count(0) from fat_saida where destinatario='" & destinatario & "'"
'	set adors2=locconnvenda.execute(strsql)
'	if not adors2.eof then
'		if cdbl(adors2("c")) = cdbl(0) then
'			strSql = "UPDATE fat_destinatario SET data_cadastro=getdate(),usuario_altera='" &session("usuario")& "' WHERE destinatario='" &destinatario& "'"
'			locconnvenda.execute(strsql)
'		end if		
'	end if
'	set adors2=nothing
	''
  end if
  set adors1=nothing  										
%>

<script>
  alert('Pagamento liberado!');
  //window.opener.refresh();
  //window.close();
</script>


<%

 session("msg") = "Pedido Liberado!"  
 
 if retorno="1" then
    strIni=request("strIni")
    strFim=request("strFim")
	Response.Redirect "../faturamento/fat_GestaoFaturamento1.asp?strini=" & strini & "&strfim=" & strFim
 else
	Response.Redirect "ped_liberarPagamentoCD.asp"
 end if

%>

<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->