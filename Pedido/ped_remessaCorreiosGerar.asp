<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<!--#include virtual="public/pub_Componentes.asp"-->
<%call Verifica_Permissao(10272)%>
<!--#include virtual="public/pub_Body.asp"-->



<%
 strOpcao     = request("opcao")
 strRemessa   = request("remessa")

if (strRemessa=0) then 
 form_validar = "confirma();"
end if 

 if(strOpcao="Remessa Reclama��o") then
 
  strSql="SELECT a.destinatario,b.nome,sac_ocorrencia,c.descricao as ocorrencia,sac_tipo,d.descricao as tipo,sac_statusIni, e.descricao as descIni,f.descricao as descFim,sac_statusFim FROM sys_integracao_correios a INNER JOIN fat_destinatario b ON (a.destinatario=b.destinatario) INNER JOIN fat_ocorrencia c ON (a.sac_ocorrencia=c.ocorrencia) INNER JOIN fat_ocorrencia_tipo d ON (a.sac_tipo=d.tipo) INNER JOIN fat_ocorrencia_status e ON (a.sac_statusIni=e.status) INNER JOIN fat_ocorrencia_status f ON (a.sac_statusFim=f.status)"
  set adoRs1=locConnVenda.Execute(strSql)

 'response.write strSql

call Rel_Criar (40,"frm1","ped_remessaCorreiosGerar1.asp")
  
  	call rel_item_selectINI(0,0,"Destinatario","d","disabled")
			call select_item(adoRs1("destinatario"),adoRs1("nome"),"")
	call rel_item_selectFIM()
	call txtoculto_criar("destinatario",adoRs1("destinatario"))

	
	call rel_item_selectINI(0,0,"Ocorrencia","o","disabled")
			call select_item(adoRs1("sac_ocorrencia"),adoRs1("ocorrencia"),"")
	call rel_item_selectFIM()
	call txtoculto_criar("ocorrencia",adoRs1("sac_ocorrencia"))
	

	call rel_item_selectINI(0,0,"Tipo","t","disabled")
			call select_item(adoRs1("sac_tipo"),adoRs1("tipo"),"")
	call rel_item_selectFIM()
	call txtoculto_criar("tipo",adoRs1("sac_tipo"))
	
	call rel_item_selectINI(0,0,"Status Atual","sA","disabled")
			call select_item(adoRs1("sac_statusIni"),adoRs1("descIni"),"")
			strStatusAtualDesc=adoRs1("descIni")
	call rel_item_selectFIM()
	call txtoculto_criar("statusAtual",adoRs1("sac_statusIni"))
	
	call rel_item_selectINI(0,0,"Novo Status","sN","disabled")
			call select_item(adoRs1("sac_statusFim"),adoRs1("descFim"),"")
	call rel_item_selectFIM()
	call txtoculto_criar("statusNovo",adoRs1("sac_statusFim"))
	
	
	
	if(strRemessa>0) then 
	   call Rel_Item_Ini(50,50,"Layout")
		call radio_Criar("layout","S","checked","Excel - CSV</br>")
		call radio_Criar("layout","N","","Arquivo - PER</br>")
	   call rel_item_fim()	
 	end if
  call Rel_Checkbox_Criar(40,60,"Download do arquivo","download","TRUE","","","")
  call txtOculto_criar("remessa",strRemessa)
  call txtoculto_criar("opcao",strOpcao)
 call rel_Fim(0)
%>

<script>

 function confirma()
 {
  if(confirm("Essa a��o ir� gerar uma nova remessa para todas as ocorrencias que estiverem com status '<%=strStatusAtualDesc%>'\n Confirma ??")==true)
  {
    return true;
  }
  else
  {
   return false;
  }
  
 }

</script>

<%
else 'Remessa de etiquetas
 
 
  strSql="SELECT a.destinatario,b.nome FROM sys_integracao_correios a INNER JOIN fat_destinatario b ON (a.destinatario=b.destinatario)"
  set adoRs1=locConnVenda.Execute(strSql)
  
    strSql="SELECT DISTINCT a.remessa,convert(varchar(5),a.remessa)+' -> '+convert(char(10),data_remessa,103) as descricao FROM ped_assinatura a WHERE remessa NOT IN (SELECT remessa FROM sys_integracao_correios_remessas) and remessa >0 AND data_remessa >=(SELECT inicio_remessas FROM sys_integracao_correios) ORDER BY a.remessa"
	
	set adoRs2=locConnVenda.Execute(strSql)
 
 
 call Rel_Criar (60,"frm1","ped_remessaCorreiosGerar1.asp")
  
  if(strRemessa=0) then 
	call rel_item_selectINI(30,0,"Remessa Assinatura","remessa","")
	  	do while not adoRs2.eof
		  call select_item(adoRs2("remessa"),adoRs2("descricao"),"")
		adoRs2.movenext
	loop
	call rel_item_selectFIM()
	
 else
 call txtOculto_criar("remessa",strRemessa)
	   call Rel_Item_Ini(50,50,"Layout")
		call radio_Criar("layout","S","checked","Excel - CSV</br>")
		call radio_Criar("layout","N","","Arquivo - PER</br>")
	   call rel_item_fim()	
 
 end if	
	
	  	call rel_item_selectINI(0,0,"Destinatario","d","disabled")
			call select_item(adoRs1("destinatario"),adoRs1("nome"),"")
	call rel_item_selectFIM()
	call txtoculto_criar("destinatario",adoRs1("destinatario"))
    strDestinatario=adoRs1("nome")
	
    call rel_item_txt(28,72,"Data de Entrega","data","dataEntrega",10,10,dt_formata(date+2))
	call rel_item_txt(28,72,"Hora de Entrega","texto","horaEntrega",8,5,"14:00")
    call Rel_Checkbox_Criar(40,60,"Download do arquivo","download","TRUE","","","")
  
  call txtOculto_criar("emitente",session("emitente"))
  call txtoculto_criar("opcao",strOpcao)
 call rel_Fim(0)
 %>
 
 <script>

 function confirma()
 {
  if(confirm("Essa a��o ir� gerar uma nova remessa para o destinatario: '<%=strDestinatario%>' com base na remessa de assinaturas:  " +document.frm1.remessaAssinatura.value+ "\nCom data de entrega do material em: "+document.frm1.dataEntrega.value+"\n\n Confirma ??")==true)
  {
    return true;
  }
  else
  {
   return false;
  }
  
 }

</script>
 
<% 
 end if

%>




<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->
