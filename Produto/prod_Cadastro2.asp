<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%
produto					=rec("produto")
descricao				=ucase(rec("descricao"))
descricao_abreviada		=ucase(rec("descricao_abreviada"))
unidade					=ucase(rec("unidade"))
peso_liquido			=rec("peso_liquido")
peso_bruto				=rec("peso_bruto")
ativo					=rec("ativosn")
classificacao_ipi		=replace(replace(rec("classificacao_ipi"),".",""),"-","")
origem					=rec("origem")
integracao_saida		=rec("integracao_saida")
grupo_icms				=rec("grupo_icms")
codean					=rec("codean")
tipo_embalagem			=rec("tipo_embalagem")
ipi						=rec("ipi")

cst_icms		=rec("cst_icms")
cst_pis			=rec("cst_pis")
cst_cofins		=rec("cst_cofins")
cst_ipi			=rec("cst_ipi")

cst_pis_isento			=rec("cst_pis_isento")
cst_cofins_isento		=rec("cst_cofins_isento")
cst_ipi_isento			=rec("cst_ipi_isento")

libera_desconto			=rec("libera_desconto")


if len(tipo_embalagem)=0 then tipo_embalagem=1
if len(ipi)=0 then ipi=0
if len(libera_desconto)=0 then libera_desconto=0
opcao					=rec("opcao")







''validar destinatario
if len(produto)>0 then
	strsql=	"select produto " & _
			"from sys_produto_aux " & _
			"where atividade='" & session("atividade") & "' and produto='" & produto & "'"
	set adors1=locconnvenda.execute(strsql)
	if adors1.eof then
		Response.Write "Erro ao acessar os dados."
		Response.end
	end if
	set adors1=nothing
end if
''

if len(integracao_saida)>0 and len(integracao_saida)<6 then integracao_saida	=string(6-len(integracao_saida),"0") & cstr(integracao_saida)
if len(ativo)=0 then ativo=1

if opcao="E" then ''excluir
	link="prod_ListaGeral1.asp"
	strsql="select top 1 nota,serie from fat_saida where emitente='" & session("emitente") & "' and produto='" & produto & "'"
	'call rw(strsql,1)
	set adors1=locconnvenda.execute(strsql)
	if not adors1.eof then
		session("msg")="Existe nota-fiscal emitida para esse produto (" & adors1("nota") & adors1("serie") & "), n�o � poss�vel excluir."
		Response.Redirect link
		Response.end
	end if
	set adors1=nothing
	
	strsql="delete fat_faixa_preco where produto='" & produto & "'"
	locconnvenda.execute(strsql)
	
	strsql="delete fat_filial_produto_op where produto='" & produto & "'"
	locconnvenda.execute(strsql)
	
	strsql="delete sys_produto_aux where produto='" & produto & "'"
	locconnvenda.execute(strsql)
	
	strsql="delete sys_produto where produto='" & produto & "'"
	locconnvenda.execute(strsql)

	session("msg")="Produto exclu�do com sucesso."
	Response.Redirect link
	Response.end
end if



if not isnumeric(peso_liquido) then
	Response.Write "Valor inv�lido: Peso L�quido ==> " & peso_liquido
	Response.end
end if

if not isnumeric(peso_bruto) then
	Response.Write "Valor inv�lido: Peso Bruto ==> " & peso_bruto
	Response.end
end if


if opcao="I" then ''inserir
	Application.Lock	
		strsql=	"select isnull(max(produto),0)+1 d from sys_produto "				
		set adors1=locConnvenda.execute(strsql)
		if not adors1.eof then 
			produto=adors1("d")
			produto	=string(6-len(produto),"0") & cstr(produto)
			
			strsql=	"insert into sys_produto (produto,usuario,data_cadastro,descricao) values " & _
					"('" & produto & "','" & session("usuario") & "',getdate(),'') "	
			
			locConnvenda.execute(strsql)
			
					
		end if
		set adors1=nothing			
	Application.UnLock

	strsql=	"insert into sys_produto_aux (produto,atividade,preco_aberto,libera_desconto) values " & _
			"('" & produto & "'," & session("atividade") & ",1,99) "	
	locConnvenda.execute(strsql)				
end if


''atualizar sys_produto
strsql="update sys_produto set " & _
		" descricao='" & descricao & "',descricao_abreviada='" & descricao_abreviada & "',ativo=" & ativo & _
		",unidade='" & unidade & "',origem=" & origem & ",codean='" & codean & "',integracao_entrada='" & produto & "'" & _
		",peso_bruto=" & replace(replace(peso_bruto,".",""),",",".") & ",peso_liquido=" & replace(replace(peso_liquido,".",""),",",".") & _
		",classificacao_ipi='" & classificacao_ipi & "',integracao_saida='" & integracao_saida & "',grupo_icms=" & grupo_icms & _		
		",ipi=" & ipi & ",tipo_embalagem=" & tipo_embalagem & ",dataatualizacao=getdate(),usuario='" & session("usuario") & "' " & _
		",cst_icms='" & cst_icms & "',cst_pis='" & cst_pis & "',cst_cofins='" & cst_cofins & "',cst_ipi='" & cst_ipi & "' " & _
		",cst_pis_isento='" & cst_pis_isento & "',cst_cofins_isento='" & cst_cofins_isento & "',cst_ipi_isento='" & cst_ipi_isento & "' " & _
		"where produto='" & produto & "'"
'call rw(strsql,1)		
locConnvenda.execute(strsql)

''atualziar sys_produto_aux
strsql="update sys_produto_aux set libera_desconto=" & libera_desconto & _
		" where produto='" & produto & "'"
locConnvenda.execute(strsql)		

''precos
strsql="delete fat_faixa_preco where produto='" & produto & "'"
locconnvenda.execute(strsql)

strsql="select grupo from fat_grupo where atividade=" & session("atividade")
set adors1=locconnvenda.execute(strsql)
do while not adors1.eof
	grupo	=adors1("grupo")
	campo1	="base" & grupo
	campo2	="cons" & grupo
	campo3	="ret" & grupo
	campo4	="baseret" & grupo
	
	valor1	=rec(campo1)
	valor2	=rec(campo2)
	valor3	=rec(campo3)
	valor4	=rec(campo4)
	if len(valor1)=0 then valor1=0
	if len(valor2)=0 then valor2=0
	if len(valor3)=0 then valor3=0
	if len(valor4)=0 then valor4=0
	
	valor1	=replace(replace(replace(valor1,"'",""),".",""),",",".")
	valor2	=replace(replace(replace(valor2,"'",""),".",""),",",".")
	valor3	=replace(replace(replace(valor3,"'",""),".",""),",",".")
	valor4	=replace(replace(replace(valor4,"'",""),".",""),",",".")
	
	strsql="insert into fat_faixa_preco (atividade,faixa,produto,preco_base,preco_consumidor,antecipado_retido,antecipado_base,dataatualizacao,usuario) values " & _
			"(" & session("atividade") & "," & grupo & ",'" & produto & "'," & valor1 & "," & valor2 & "," & valor3 & "," & valor4 & ",getdate(),'" & session("usuario") & "')"
	locconnvenda.execute(strsql)		
	
	adors1.movenext
loop
set adors1=nothing

''opera��es fiscais
strsql="delete fat_filial_produto_op where emitente='" & session("emitente") & "' and produto='" & produto & "'"
locconnvenda.execute(strsql)

strsql="select operacao from fat_Operacao where atividade=" & session("atividade") & " and isnull(exigeNfRefer,0)=0"
set adors1=locconnvenda.execute(strsql)
do while not adors1.eof
	operacao=adors1("operacao")
	campo1	="op" & operacao	
	
	valor1	=rec(campo1)
	if len(valor1)=0 then valor1=0
	
	if cstr(valor1)="1" then	
		strsql="insert into fat_filial_produto_op (emitente,produto,operacao) values ('" & session("emitente") & "','" & produto & "'," & operacao & ")"
		locconnvenda.execute(strsql)
	end if
	
	adors1.movenext
loop
set adors1=nothing




if opcao="I" then
	session("msg")="Cadastro Inclu�do com Sucesso."
else
	session("msg")="Cadastro Atualizado com Sucesso."
end if

'Response.Redirect "prod_cadastro1.asp?produto=" & produto & "&opcao=A"
Response.Redirect "prod_cadastro.asp"

%>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual='public/pub_Geral1.asp'-->