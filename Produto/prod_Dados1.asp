<%@LANGUAGE = VBScript.Encode %>
<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%call Verifica_Permissao(10444)

strMes	=request("mes")
strAno	=request("ano")
strProd	=request("prod")

lbl1 = "Emiss�o:" 
txt1 = now
lbl2 = "" 
txt2 = ""
lbl3 = "Per�odo:"
txt3 = mes(strMes) & "-" & strAno
lbl4 = ""
txt4 = ""
lbl5 = ""
txt5 = ""
lbl6 = ""
txt6 = ""

if len(strMes)=0 then strMes="0"

call define_coluna ("Nota","nota",7,"c","0","n","s","","")
call define_coluna ("S�rie","serie",2,"c","t","n","s","","")
call define_coluna ("Nome","nome_Abreviado","*","e","t","n","s","","")
call define_coluna ("Vendedor","descricao","15","e","t","n","s","","")
call define_coluna ("Refer�ncia","referencia","8","c","t","n","s","","")
call define_coluna ("Quantidade","quantidade","10","d","0","s","s","","")
call define_coluna ("Valor","valor","8","d","2","s","s","","")
call define_coluna ("Desconto","desconto","6","d","2","s","s","","")
call define_coluna ("Total","valor_total","8","d","2","s","s","","")

call define_link(1,"../faturamento/fat_nf.asp?nf=[nota]&se=[serie]&filial=[emitente]")
call define_link(3,"../cliente/cli_dados.asp?dest=[destinatario]")
call define_quebra ("Total ", "descricao_abreviada","s","")
  
strSql= "select a.emitente,a.destinatario,nota,serie,referencia,a.produto,b.descricao_abreviada,d.nome_Abreviado" & _
		",valor=a.valor,desconto=a.desconto,valor_total=a.valor_total,a.quantidade,e.descricao " & _
		"from fat_saida a inner join sys_produto b on a.produto=b.produto " & _
		"inner join fat_operacao c on a.operacao=c.operacao " & _
		"inner join fat_destinatario d on a.destinatario=d.destinatario " & _
		"left join fat_hierarquia e on a.rota=e.hierarquia " & _
		"where (month(referencia)=" & strMes & " or " & strMes & "='0') " & _
		"and year(referencia)=" & strAno & " and a.produto='" & strProd & "' " & _
		"and c.venda=1 and situacao=0 " & _
		"order by referencia,nota"
		'Response.Write strsql
		'Response.end
		
set adoRs1=conn1.execute(strSql)    
impcab = 1
call fun_rel_cab()	
do while not adoRs1.eof
	intConta=intConta+1
	'auto=adoRs1("destinatario")
	call fun_rel_det()
	adoRs1.movenext
loop
call fun_fim_rel()
  %>
</table>
</td></tr>
<tr><td width="100%">
<%
call zerar_relatorio()

strSql= "select e.descricao" & _
		",valor=sum(valor),valor_total=sum(a.valor_total),desconto=sum(a.desconto),quantidade=sum(a.quantidade) " & _
		"from fat_saida a inner join fat_operacao c on a.operacao=c.operacao " & _		
		"left join fat_hierarquia e on a.rota=e.hierarquia " & _
		"where (month(referencia)=" & strMes & " or " & strMes & "='0') " & _
		"and year(referencia)=" & strAno & " and a.produto='" & strProd & "' " & _
		"and c.venda=1 and situacao=0 " & _
		"group by e.descricao " & _
		"order by e.descricao"
set adoRs1=locConnVenda.execute(strsql)

call define_coluna ("Vendedor","descricao","*","e","t","n","s","","")
call define_coluna ("Quantidade","quantidade","10","d","0","s","s","","")
call define_coluna ("Valor","valor","8","d","2","s","s","","")
call define_coluna ("Desconto","desconto","6","d","2","s","s","","")
call define_coluna ("Total","valor_total","8","d","2","s","s","","")

strParametroTab1="<tr><td width='100%' colspan='5'><font face='verdana' size='1'><b>Resumo por Vendedor</b></font></td></tr>"
call fun_rel_cab()
do while not adoRs1.eof	
	call fun_rel_det()
	adoRs1.movenext
loop
call fun_fim_rel()
  %>
</table>
</td></tr></table>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->
      