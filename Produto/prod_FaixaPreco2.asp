<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10407)

faixa	=replace(trim(request("faixa")),"'","")
produto	=trim(request("produto"))
preco	=replace(trim(request("preco")),"'","")
opcao   =trim(request("opcao"))

if opcao="1" then
	strsql="select nome from fat_destinatario a inner join fat_comercio b on a.destinatario=b.destinatario " & _
		  "where faixa_preco=" & faixa & " order by nome"
	set adors1=locconnvenda.execute(strsql)
	if not adors1.eof then
		session("msg")="N�o � poss�vel excluir, existe cliente vinculado � essa faixa de pre�o: " & adors1("nome")
		Response.Redirect "prod_faixapreco1.asp"
	end if
	set adors1=nothing
	
	strsql="delete fat_faixa_preco where faixa=" & faixa & " and produto='" & produto & "'"
	locconnvenda.execute(strsql)
	
	strsql="delete fat_grupo where grupo=" & faixa
	locconnvenda.execute(strsql)
	
	strsql="delete fat_frete_grupo where grupo=" & faixa
	locconnvenda.execute(strsql)
	
	strsql="delete fat_opcao_cobranca where grupo=" & faixa
	locconnvenda.execute(strsql)		

	session("msg")="Exclu�do com sucesso."
	Response.Redirect "prod_faixapreco1.asp"
end if

if len(trim(faixa))=0 then
	session("msg")="Informe a Descri��o da Faixa de Pre�o"
	Response.Redirect "prod_FaixaPreco1.asp"
end if
if len(trim(produto))=0 then
	session("msg")="Selecione o Produto"
	Response.Redirect "prod_FaixaPreco1.asp"
end if
if len(trim(preco))=0 then
	session("msg")="Informe o Pre�o do Produto"
	Response.Redirect "prod_FaixaPreco1.asp"
end if

if not isnumeric(preco) then
	session("msg")="Pre�o Inv�lido"
	Response.Redirect "prod_FaixaPreco1.asp"
end if

impcab = 1

strsql="select c=isnull(max(convert(integer,grupo)),0)+1 from fat_grupo"
set adors1=locconnvenda.execute(strsql)
if not adors1.eof then c=adors1("c")
set adors1=nothing

strsql=	"insert into fat_grupo (atividade,grupo,descricao,icmssub,valorminimo,dataatualizacao,desconto,ordem,valormaximo,limite_padrao_credito) values " & _
		"(" & session("atividade") & "," & c & ",'" & ucase(faixa) & "','S',0,getdate(),0,10001,0,0)"
locconnvenda.execute(strsql)

strsql=	"insert into fat_faixa_preco (atividade,faixa,produto,preco_base,preco_consumidor,dataatualizacao,venda_consumidor,usuario) values " & _
		"(" & session("atividade") & "," & c & ",'" & produto & "'," & replace(replace(preco,".",""),",",".") & "," & replace(replace(preco,".",""),",",".") & ",getdate(),0,'" & session("usuario") & "')"
locconnvenda.execute(strsql)

strsql=	"insert into fat_frete_grupo " & _
		"select " & c & ",transporte,100000,100,getdate(),'' from fat_transporte"
locconnvenda.execute(strsql)		

strsql=	"insert into fat_opcao_cobranca " & _
		"select " & c & ",tipo,0,getdate() from fat_tipo_cobranca"
locconnvenda.execute(strsql)

session("msg")="Cadastrado com sucesso."
Response.Redirect "prod_faixapreco1.asp"

%>