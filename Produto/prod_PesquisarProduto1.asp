<%@LANGUAGE = VBScript.Encode %>
<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10007)

produto	=request("prod")
opcao	=request("opcao")

lbl1	=""
txt1	=""
lbl2	=""
txt1	=""
lbl3	=""
txt3	=""
lbl4	=""
txt4	=""

if opcao="1" then
	msg="PEDIDOS PENDENTES"
end if

impcab = 1

strsql=	"select a.emitente,a.destinatario,d.descricao_abreviada,a.pedicodigo" & _
		",datapedido=convert(varchar(10),a.datapedido,103),empenhado=isnull(d.empenhado,0)" & _
		",b.qtd,c.stnfDescricao,c.cor,e.nome " & _
		"from ped_PedidoTotal a inner join ped_PedidoItem b on a.emitente=b.emitente and a.pedicodigo=b.pedicodigo " & _
		"inner join ped_StatusNf c on a.stnfcodigo=c.stnfcodigo " & _
		"inner join sys_produto d on b.prodcodigo=d.produto " & _
		"inner join fat_Destinatario e on a.destinatario=e.destinatario " & _
		"inner join fat_operacao f on b.tipooperacao=f.operacao " & _
		"where prodcodigo='" & produto & "' and stnfVerificaPendencia=1 and f.entsai=2 " & _
		"and a.emitente='" & session("emitente") & "' and isnull(a.box_padrao,0)<=0 " & _
		"order by a.pedicodigo"
'Response.Write strsql
'Response.end	
set adors1=locConnVenda.execute(strsql)

if session("empresa_id")="00000002" then
	if not adors1.eof then empenhado=adors1("empenhado")
end if

'call define_coluna (Titulo, campo, tamanho, alinhamento, formato, total,repete)
call define_coluna ("Pedido","pedicodigo","10","c","0","n","s","","")
call define_coluna ("Data","datapedido","10","c","t","n","s","","")
call define_coluna ("Nome","nome","*","e","t","n","s","","")
call define_coluna ("Quantidade","qtd","10","d","0","s","s","","")
call define_coluna ("Status","stnfDescricao","20","c","t","n","s","$cor","")

call define_quebra ("", "descricao_abreviada","p","descricao_abreviada")
call define_link(1,"../pedido/ped_Pedido.asp?em=[emitente]&ped=[pedicodigo]")
call define_link(3,"../cliente/cli_dados.asp?dest=[destinatario]")
''call define_Linkqb(1,"cli_Dados.asp?dest=[destinatario]")

strParametroTab1="<tr><td width='100%' colspan='5'><font face='verdana' size='1'><b>" & msg & "</b></font></tr>"

if session("empresa_id")="00000002" then strParametroTab1=strParametroTab1 & "<tr><td width='100%' colspan='5'><font face='verdana' size='1'><b>Empenhado Site: " & formatnumber(empenhado,0) & "</b></font></tr>"
call fun_rel_cab()
do while not adoRs1.eof
	call fun_rel_det()
	adoRs1.movenext
loop
call fun_fim_rel()
call Func_ImpPag(0,iColuna)


'INCLUDES OBRIGATÓRIOS%>
</table></td></tr></table>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->