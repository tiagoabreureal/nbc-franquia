<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%	
call Verifica_Permissao(10560)

opcao		=rec("opcao")
grupo_icms	=rec("grupo_icms")
uforig		=ucase(rec("uforig"))
ufdest		=ucase(rec("ufdest"))

link="prod_TabelaAliquota1.asp"

if opcao="I" then ''incluir
	aliquota_icms=rec("aliquota_icms")
	aliq_interna =rec("aliq_interna")
	icms_com	 =rec("icms_com")
	retido_com	 =rec("retido_com")
	pis			 =rec("pis")
	cofins		 =rec("cofins")
	base_reduzida=rec("base_reduzida")
	iva			 =rec("iva")
	porc_me		 =rec("porc_me")
			
	if len(uforig)=0 or len(ufdest)=0 then
		session("msg")="Para incluir uma Al�quota, � necess�rio informar o estado Origem e Destino."
		Response.Redirect link
		Response.end
	end if
	
	if len(aliquota_icms)=0 then
		session("msg")="Para incluir uma Al�quota, � necess�rio informar a Al�quota de ICMS."
		Response.Redirect link
		Response.end
	end if	

	if len(grupo_icms)=0 then
		session("msg")="Para incluir uma Al�quota, � necess�rio informar o Grupo de ICMS."
		Response.Redirect link
		Response.end
	end if		
	
	strsql="select top 1 grupo_icms from fat_Aliquota where atividade=" & session("atividade") & _
		   " and grupo_icms=" & grupo_icms & " and uforig='" & uforig & "' " & _
		   " and ufdest='" & ufdest & "'"
	set adors1=locconnvenda.execute(strsql)
	if not adors1.eof then
		session("msg")="Al�quota j� cadastrada para esse Grupo x Origem x Destino."
		Response.Redirect link
		Response.end
	end if
	set adors1=nothing
	
	if len(aliq_interna)=0 then aliq_interna=0
	if len(porc_me)=0 then porc_me=0
	if len(pis)=0	 then pis=0
	if len(cofins)=0 then cofins=0
	

	aliquota_icms	=replace(replace(aliquota_icms,".",""),",",".")
	aliq_interna	=replace(replace(aliq_interna,".",""),",",".")
	pis				=replace(replace(pis,".",""),",",".")
	cofins			=replace(replace(cofins,".",""),",",".")
	base_reduzida	=replace(replace(base_reduzida,".",""),",",".")
	iva				=replace(replace(iva,".",""),",",".")
	porc_me			=replace(replace(porc_me,".",""),",",".")
	
	strsql= "insert into fat_Aliquota (atividade,grupo_icms,uforig,ufdest,aliquota_icms,icms_com,retido_com,base_reduzida,[%_consumidor],aliq_interna,[%_me]) " & _
			"values (" & session("atividade") & "," & grupo_icms & ",'" & uforig & "','" & ufdest & _
			"'," & aliquota_icms & "," & icms_com & "," & retido_com & "," & base_reduzida & "," & iva & "," & aliq_interna & "," & porc_me & ")"
	'call rw(strsql,1)
	locConnVenda.execute(strsql)
	
	
	'' PIS/COFINS	
	strsql="delete fat_aliquota_piscofins where atividade=" & session("atividade") & _
			"and grupo_icms=" & grupo_icms
	'call rw(strsql,1)				
	locconnvenda.execute(strsql)
	
	strsql=	"insert into fat_Aliquota_PisCofins (atividade,grupo_icms,pis,cofins) " & _
			"values (" & session("atividade") & "," & grupo_icms & "," & pis & "," & cofins & ")"
	'call rw(strsql,1)
	locConnVenda.execute(strsql)
	
	session("msg")="Al�quota inclu�da com sucesso."
elseif opcao="A" then ''alterar

	ultimo_grupo_icms=0

	strsql="select grupo_icms,uforig,ufdest from fat_aliquota where atividade=" & session("atividade")
	set adors1=locconnvenda.execute(strsql)
	do while not adors1.eof
		grupo_icms	=adors1("grupo_icms")
		uforig		=adors1("uforig")
		ufdest		=adors1("ufdest")
				
		caliquota_icms	="aliquota_icms" & cstr(grupo_icms) & uforig & ufdest
		caliq_interna	="aliq_interna" & cstr(grupo_icms) & uforig & ufdest
		cicms_com		="icms_com" & cstr(grupo_icms) & uforig & ufdest
		cretido_com		="retido_com" & cstr(grupo_icms) & uforig & ufdest
		cpis			="pis" & cstr(grupo_icms) '& uforig & ufdest
		ccofins			="cofins" & cstr(grupo_icms)' & uforig & ufdest
		cbase_reduzida	="base_reduzida" & cstr(grupo_icms) & uforig & ufdest
		civa			="iva" & cstr(grupo_icms) & uforig & ufdest
		cporc_me		="porc_me" & cstr(grupo_icms) & uforig & ufdest
				
		aliquota_icms	=rec(caliquota_icms)
		aliq_interna	=rec(caliq_interna)
		icms_com		=rec(cicms_com)
		retido_com		=rec(cretido_com)
		pis				=rec(cpis)
		cofins			=rec(ccofins)
		base_reduzida	=rec(cbase_reduzida)
		iva				=rec(civa)
		porc_me			=rec(cporc_me)
				
		if len(aliquota_icms)=0 then aliquota_icms=0
		if len(aliq_interna)=0	then aliq_interna=0
		if len(pis)=0			then pis=0
		if len(cofins)=0		then cofins=0
		if len(base_reduzida)=0	then base_reduzida=0
		if len(iva)=0			then iva=0
		if len(porc_me)=0		then porc_me=0
				
		aliquota_icms	=replace(replace(aliquota_icms,".",""),",",".")
		aliq_interna	=replace(replace(aliq_interna,".",""),",",".")
		pis				=replace(replace(pis,".",""),",",".")
		cofins			=replace(replace(cofins,".",""),",",".")
		base_reduzida	=replace(replace(base_reduzida,".",""),",",".")
		iva				=replace(replace(iva,".",""),",",".")
		porc_me			=replace(replace(porc_me,".",""),",",".")
		
		
		'Response.Write porc_me
		'Response.end
						
		strsql=	"update fat_Aliquota set aliquota_icms=" & aliquota_icms & _
				",icms_com=" & icms_com & ",retido_com=" & retido_com & _
				",base_reduzida=" & base_reduzida & ",[%_consumidor]=" & iva & ",[%_me]=" & porc_me & _
				",aliq_interna=" & aliq_interna & _
				" where grupo_icms=" & grupo_icms & " and atividade=" & session("atividade") & _
				" and uforig='" & uforig & "' and ufdest='" & ufdest & "'"
		'call rw(strsql,1)
		locconnvenda.execute(strsql)
		
		'' PIS/COFINS
		strsql="delete fat_aliquota_piscofins where atividade=" & session("atividade") & _
				"and grupo_icms=" & grupo_icms
		'call rw(strsql,1)				
		locconnvenda.execute(strsql)
											
		strsql=	"insert into fat_Aliquota_PisCofins (atividade,grupo_icms,pis,cofins) " & _
				"values (" & session("atividade") & "," & grupo_icms & "," & pis & "," & cofins & ")"
		'call rw(strsql,1)
		locConnVenda.execute(strsql)
		
			
		adors1.movenext
	loop
	set adors1=nothing
	session("msg")="Atualizado com sucesso."

elseif opcao="E" then ''excluir
	
	strsql=	"select top 1 produto=a.integracao_saida + ' - ' + a.descricao " & _
			"from sys_produto a inner join sys_produto_aux b on a.produto=b.produto " & _
			"where a.grupo_icms=" & grupo_icms & " and b.atividade=" & session("atividade")
	set adors1=locconnvenda.execute(strsql)
	if not adors1.eof then
		session("msg")="Existe produto vinculado a essa al�quota (" & adors1("produto") & "), n�o � poss�vel excluir."
		Response.Redirect link
		Response.end
	end if
	set adors1=nothing
	
	strsql=	"delete fat_Aliquota_PisCofins " & _
			"where grupo_icms=" & grupo_icms & " and atividade=" & session("atividade")
	locConnVenda.execute(strsql)
	
	strsql=	"delete fat_Aliquota " & _
			"where grupo_icms=" & grupo_icms & " and atividade=" & session("atividade") & _
			" and uforig='" & uforig & "' and ufdest='" & ufdest & "'"
	locConnVenda.execute(strsql)
		
	session("msg")="Al�quota exclu�da com sucesso."		
end if

'' Adicionado 2 comandos abaixo (Kleber), solicitado por renato em 23/01/12 (09:50)
'strsql="update fat_aliquota set base_reduzida_ca = 1, base_reduzida_com = 1 where base_reduzida > 0 and atividade=" & session("atividade")
'locConnVenda.execute(strsql)

'strsql="update fat_aliquota set base_reduzida_ca = 0, base_reduzida_com = 0 where base_reduzida = 0 and atividade=" & session("atividade")
'locConnVenda.execute(strsql)
''''''''''''''''''''

Response.Redirect link

%>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->