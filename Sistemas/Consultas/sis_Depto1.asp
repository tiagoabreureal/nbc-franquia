<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%call Verifica_Permissao(177)%>
<!--#include virtual="public/pub_Body.asp"-->
<%

strDepto	=request("depto")

strSql="select cod_depto,depto nome,endereco,bairro,cidade,cep,estado,nome razao," + _
	   "cgc_cpf,inscricao_rg,ativo_sn,responsavel,gerente," + _
	   "geral,grupo,subgrupo,divisao,regional,distrito,praca " + _
	   "from view_depto " + _
	   "where cod_depto='" & strDepto & "'"
set adoRel=locConnVenda.execute(strSql)
if not adoRel.eof then
	strNome		=adoRel("nome")
	strRazao	=adoRel("razao")
	strCgc		=adoRel("cgc_cpf")
	strInscr	=adoRel("inscricao_rg")
	strAtivo	=ucase(adoRel("ativo_SN"))
	strEndereco =adoRel("endereco")
	strBairro	=adoRel("bairro")
	strCidade	=adoRel("cidade")
	strUf		=adoRel("estado")
	strCep		=adoRel("cep")
	strResp		=adoRel("responsavel")
	strGerente	=adoRel("gerente")
	
	grupo_geral	=adoRel("geral")
	grupo		=adoRel("grupo")
	divisao		=adoRel("divisao")
	distrito	=adoRel("distrito")
	regional	=adoRel("regional")
	praca		=adoRel("praca")
	subgrupo	=adoRel("subgrupo")	
end if
adoRel.close
%>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%">
            <table style="border-collapse: collapse" bordercolor="#C0C0C0" border="1" width="100%" cellspacing="1">
               <%if strAtivo="N" then
					cor="red"
				else
					cor="black"
                end if
              %>
             <tr>
                <td width="9%" align="right"><font size="1" face="Verdana">Depto:</font></td>
                <td width="91%" colspan="4"><font size="2" face="Verdana" color="<%=cor%>"><b><%=strDepto & " - " & strNome%></b></font></td>                
              </tr>           
              <tr>
                <td width="9%" align="right"><font size="1" face="Verdana">Raz�o:</font></td>
                <td width="76%" colspan="3"><font size="1" face="Verdana" color="<%=cor%>"><%=strRazao%></font></td>
                <td width="17%" align="center"><font size="1" face="Verdana">Ativo: <font color="<%=cor%>"><%=strAtivo%></font></font></td>
              </tr>
              <tr>
                <td width="9%" align="right"><font size="1" face="Verdana">CGC:</font></td>
                <td width="40%"><font size="1" face="Verdana"><%=strCgc%></font></td>
                <td width="17%">
                  <p align="right"><font size="1" face="Verdana">Inscr.:</font></td>
                <td width="34%" colspan="2"><font size="1" face="Verdana"><%=strInscr%></font></td>
              </tr>
              <tr>
                <td width="9%" align="right"><font size="1" face="Verdana">End.:</font></td>
                <td width="40%"><font size="1" face="Verdana"><%=strEndereco%></font></td>
                <td width="17%" align="right"><font size="1" face="Verdana">Bairro:</font></td>
                <td width="34%" colspan="2"><font size="1" face="Verdana"><%=strBairro%></font></td>
              </tr>
              <tr>
                <td width="9%" align="right"><font size="1" face="Verdana">Cidade</font></td>
                <td width="40%"><font size="1" face="Verdana"><%=strCidade%></font></td>
                <td width="17%" align="right"><font size="1" face="Verdana">Uf:</font></td>
                <td width="17%"><font size="1" face="Verdana"><%=strUf%></font></td>
                <td width="17%">
                  <p align="center"><font size="1" face="Verdana">CEP: <%=strCep%></font></td>
              </tr>
            </table>
            <p></td>
        </tr>
        <tr>
          <td width="100%">
            <table style="border-collapse: collapse" bordercolor="#C0C0C0" border="1" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td width="15%" align="center" valign="top"><font size="1" face="Verdana" color="#000080"><b>Telefones</b></font>
                  <div align="center">
                  <%
                  strSql="select descricao,ddd,telefone " + _
						 "from view_Telefone " + _						 
						 "where cod_depto='" & strDepto & "'"
						 'Response.Write strsql						 
				  set adoTel=conn5.execute(strSql)
                  %>
                    <table border="0" width="100%" cellspacing="1" cellpadding="0">
                    <%
                    intAlt=1
                    do while not adoTel.eof
						if intAlt=1 then
							strCor=" bgcolor='#f5f5f5' "
							intAlt=0
						else
							strCor=""
							intAlt=1
						end if
                    %>
                      <tr>
                        <td width="30%" align="center" <%=strCor%>>
                          <font size="1" face="Verdana"><%=mid(adoTel("descricao"),1,3)%></font></td>
                        <td width="10%" align="center" <%=strCor%>>
                          <font size="1" face="Verdana"><%=adoTel("ddd")%></font></td>
                        <td width="60%" align="right" <%=strCor%>>
                          <font size="1" face="Verdana"><%=mid(adoTel("telefone"),1,len(adoTel("telefone"))\2) & "-" & right(adoTel("telefone"),4)%></font></td>
                      </tr>
                     <%
						adoTel.movenext
					loop
					adoTel.close
                     %>
                    </table>
                  </div>
                </td>
                <td width="45%" align="center" valign="top"><font size="1" face="Verdana" color="#000080"><b>Respons�veis</b></font>
                 
                  <%
                  strSql="select DescFuncaoAux Cargo,nome " + _
						 "from view_func " + _
						 "where filial='" & strDepto & "' and FuncaoAux in(1,2,3,4) and situacao<4 " + _
						 "order by cargo,nome"
				  set adoResp=conn5.execute(strSql)
                  %>                 
                    <table border="0" width="95%" cellspacing="1" cellpadding="0" align="center">
                    <%intAlt=0
                    do while not adoResp.eof
						if intAlt=1 then
							strCor=" bgcolor='#f5f5f5' "
							intAlt=0
						else
							strCor=""
							intAlt=1
						end if
							%>
                      <tr>
                        <td width="65%" <%=strCor%>><font size="1" face="Verdana"><%=adoResp("Nome")%></font></td>
                        <td width="35%" <%=strCor%> align="center"><font size="1" face="Verdana"><%=adoResp("Cargo")%></font></td>
                      </tr>
                    <%adoResp.movenext
                    loop
                    adoResp.close%>
                    </table>                  
                </td>
                <td width="40%" align="center" valign="top">
                <font size="1" face="Verdana" color="#000080"><b>Hierarquia</b></font>            
                 <table border="0" width="95%" cellspacing="1" cellpadding="0">               
                      <tr>
                        <td width="30%" bgcolor="#f5f5f5"><font size="1" face="Verdana">Geral:</font></td>
                        <td width="30%" bgcolor="#f5f5f5"><font size="1" face="Verdana"><%=grupo_geral%></font></td>
                        <td width="40%" bgcolor="#f5f5f5"><font size="1" face="Verdana">&nbsp</font></td>
                      </tr> 
                       <tr>
                        <td width="30%"><font size="1" face="Verdana">Grupo:</font></td>
                        <td width="30%"><font size="1" face="Verdana"><%=grupo%></font></td>
                        <td width="40%"><font size="1" face="Verdana">&nbsp</font></td>
                      </tr> 
                       <tr>
                        <td width="30%" bgcolor="#f5f5f5"><font size="1" face="Verdana">Subgrupo:</font></td>
                        <td width="30%" bgcolor="#f5f5f5"><font size="1" face="Verdana"><%=subgrupo%></font></td>
                        <td width="40%" bgcolor="#f5f5f5"><font size="1" face="Verdana">&nbsp</font></td>
                      </tr> 
                       <tr>
                        <td width="30%"><font size="1" face="Verdana">Divis�o</font></td>
                        <td width="30%"><font size="1" face="Verdana"><%=divisao%></font></td>
                        <td width="40%"><font size="1" face="Verdana"><%=strResp%></font></td>
                      </tr> 
                       <tr>
                        <td width="30%" bgcolor="#f5f5f5"><font size="1" face="Verdana">Regional:</font></td>
                        <td width="30%" bgcolor="#f5f5f5"><font size="1" face="Verdana"><%=regional%></font></td>
                        <td width="40%" bgcolor="#f5f5f5"><font size="1" face="Verdana">&nbsp</font></td>
                      </tr> 
                       <tr>
                        <td width="30%"><font size="1" face="Verdana">Distrito:</font></td>
                        <td width="30%"><font size="1" face="Verdana"><%=distrito%></font></td>
                        <td width="40%"><font size="1" face="Verdana"><%=strGerente%></font></td>                        
                      </tr>                      
                       <tr>
                        <td width="30%" bgcolor="#f5f5f5"><font size="1" face="Verdana">Pra�a:</font></td>
                        <td width="30%" bgcolor="#f5f5f5"><font size="1" face="Verdana"><%=praca%></font></td>
                        <td width="40%" bgcolor="#f5f5f5"	><font size="1" face="Verdana">&nbsp</font></td>
                      </tr>                      
                    </table>
                </td>
              </tr>			
            </table><p>        
          </td>
          </tr>
          <%if strAtivo="N" then
				strSql="select  cod_depto,depto,porcentagem " & _
					   "from fat_FilialTransf a, view_depto b " & _
					   "where a.filial_Destino=b.cod_depto " & _
					   "  and a.filial_origem='" & strDepto & "' order by cod_depto"
			    set adoA=conn6.execute(strSql)%>
            <tr>
			  <td width="100%"> 
			  	<table style="border-collapse: collapse" bordercolor="#C0C0C0" border="1" width="50%" cellspacing="0" cellpadding="0">
			  		<tr>
			  			<td width="100%" colspan="2"><b><%call label("TRANSFER�NCIA","1","","")%></b></td>
			  		</tr>
			  		<tr>
			  			<td width="80%" align="center" bgcolor="#f5f5f5"><%call label("FILIAL",1,"","")%></td>
			  			<td width="20%" align="center" bgcolor="#f5f5f5"><%call label("PORCENTAGEM",1,"","")%></td>
			  		</tr>
			  		<%do while not adoA.eof%>
			  			<tr>
			  				<td width="80%" align="left">
			  					<a href="sis_Depto1.asp?depto=<%=adoA("cod_depto")%>">
			  					<%call label(adoA("cod_Depto") & "-" & adoA("depto"),1,"","")%></a></td>
			  				<td width="20%" align="center"><%call label(adoA("porcentagem") & "%",1,"","")%></td>
			  			</tr>
			  		<%adoA.movenext
			  		loop
			  		set adoA=nothing%>
			  	</table>
			  </td>
			</tr>
            <%end if%>  
        </table>       
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->