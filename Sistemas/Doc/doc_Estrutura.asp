<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MODELO DE ARQUIVO DE TELA DE PAR�METROS
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'INCLUDES OBRIGAT�RIOS%>
<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%

'C�DIGO DA APLICA��O (SYS_APLICA��O)
call Verifica_Permissao(382)

tipo	=trim(request("tipo"))

strSql="select descricao from doc_Tipo where tipo=" & tipo
set adoA=conn5.execute(strSql)
if not adoA.eof then
	descTipo	=adoA("descricao")
end if
set adoA=nothing

strOnload="frm1.descricao.focus()"
%>
<!--#include virtual="public/pub_Body.asp"-->
<br>
<%
call label("Tipo: " & descTipo,"1","center","")

strSql= "select tipo,campo,a.descricao,obrigatorio,minimo,maximo,b.descricao d2,valorPadrao, excluir='excluir' " & _
		"from doc_campo a, doc_TipoControle b where a.tipoControle=b.tipoControle and tipo=" & tipo
set adoRs1=conn5.execute(strSql)

call define_coluna ("campo","campo","5","c","t","n","n","","")
call define_coluna ("descri��o","descricao","35","e","t","n","n","","")
call define_coluna ("obrig.","obrigatorio","5","c","t","n","n","","")
call define_coluna ("min","minimo","5","c","t","n","n","","")
call define_coluna ("max","maximo","5","c","t","n","n","","")
call define_coluna ("tipo campo","d2","20","c","t","n","n","","")
call define_coluna ("valor padr�o","valorPadrao","15","c","t","n","n","","")
call define_coluna ("excluir","excluir","10","c","t","n","n","","")

call define_link (8,"doc_EstruturaApagar.asp?tipo=" & tipo & "&campo=[campo]")
	
call fun_rel_cab()
do while not adoRs1.eof
	call fun_rel_det()
	adoRs1.movenext
loop
call fun_fim_rel()
%>
</table>

<%
mensa="INCLUIR CAMPO"
%>
<hr size="1">
<table border="1" width="100%" style="border-collapse: collapse">
<tr><td width="100%" colspan="8"><b><font color="<%=cor%>"><%call label(mensa,1,"left","")%></b></td></tr>
<%
'form_Validar="Validar()"
call form_Criar("frm1","doc_EstruturaIncluir.asp")
	call txtOculto_Criar("tipo",tipo)
%>
<tr><td width="40%" colspan="2" align="center"><%call txt_Criar("texto","descricao",40,50,alt_Descricao)%></td>
	<td width="5%" align="center"><%call checkBox_Criar("obrigatorio","1","","","checked")%></td>
    <td width="5%" align="center"><%call txt_Criar("inteiro","minimo",1,1,0)%></td>    
    <td width="5%" align="center"><%call txt_Criar("inteiro","maximo",3,5,50)%></td>
    <td width="20%" align="center"><%
		strSql="select tipoControle,descricao from doc_TipoControle order by descricao"
		set adoA=conn5.execute(strSql)
		call select_Criar("tipoControle","")
		
		do while not adoA.eof
			call select_item(adoA("tipoControle"),adoA("descricao"),"")
			adoA.movenext
		loop
		set adoA=nothing
		
		%></td>    
	<td width="15%" align="center"><%call txt_Criar("texto","valorPadrao",10,50,"")%></td>
	<td width="10%" align="center">&nbsp</td>
</tr>
<tr><td width="100%" align="center" colspan="8"><b><%call button_Criar("submit","btn1","GRAVAR","")%></b></td></tr>
<%call form_Fim()%>
</table>

<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->