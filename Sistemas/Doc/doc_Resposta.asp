<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MODELO DE ARQUIVO DE TELA DE PAR�METROS
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'INCLUDES OBRIGAT�RIOS%>
<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<!--#include virtual="public/pub_BodyOculto.asp"-->
<%

'C�DIGO DA APLICA��O (SYS_APLICA��O)
call Verifica_Permissao(469)

tipo	=trim(request("tipo"))
doc		=trim(request("doc"))

strSql="select descricao from doc_Tipo where tipo=" & tipo
set adoA=conn5.execute(strSql)
if not adoA.eof then
	descTipo=adoA("descricao")
end if
set adoA=nothing

strSql="select descricao from doc_documento where tipo=" & tipo & " and doc=" & doc
set adoA=conn5.execute(strSql)
if not adoA.eof then
	descDoc=adoA("descricao")
end if
set adoA=nothing

strSql="select * from doc_Campo where tipo=" & tipo & " order by campo"
set adoA=conn5.execute(strSql)

call form_Criar("frm1","doc_RespostaIncluir.asp")
	call txtOculto_Criar("tipo",tipo)
	call txtOculto_Criar("doc",doc)
%>

<table border="1" style='border-collapse: collapse' width="560" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td width="100%" bgcolor="#c0c0c0" align="center"><%call label(descTipo,2,"center","")%></td>
	</tr>
	<tr>
		<td width="100%"  align="center"><%call label("Documento: " & descDoc,2,"center","")%></td>
	</tr>
	<tr>
		<td width="100%">
			<table border="0" width="100%" cellpadding="0" cellspacing="0" align="center">
				<%do while not adoA.eof
					nomeCampo	="descricao" & cstr(adoA("campo"))
					
					strSql=	"select descricao from doc_Resposta " & _
							"where tipo=" & tipo & " and doc=" & doc & " and campo=" & adoA("campo")
					set adoX=conn5.execute(strSql)
					if not adoX.eof then
						valorPadrao=adoX("descricao")
					else				 					
						if ucase(adoA("valorPadrao"))="LINK" then
							strSql="select link from sys_LayoutPrincipal where aplicacao=" & doc
							'Response.Write strsql
							set adoX=conn5.execute(strSql)
							if not adoX.eof then
								valorPadrao=adoX("Link")
							end if
							set adoX=nothing
								
						else
							valorPadrao=adoA("valorPadrao")
						end if
					end if
				%>
				
				<tr>
					<td width="100%"><b><%call label(adoA("descricao"),"1","left","")%></b></td>
				</tr>
				<tr>
					<td width="100%"><%										
					if adoA("tipocontrole")=1  then'' text normal											
						call txt_Criar("texto",nomeCampo,50,adoA("Maximo"),valorPadrao)
					elseif adoA("tipoControle")=2 then '' textarea
						call textArea_Criar(nomeCampo,5,70,valorPadrao)
					elseif adoA("tipoControle")=3 then '' text com usu�rio
						call txt_Criar("texto",nomeCampo,50,adoA("Maximo"),now)
					elseif adoA("tipoControle")=4 then '' text com data
						call txt_Criar("texto",nomeCampo,50,adoA("Maximo"),session("usuario"))
					end if
						%>						
					</td>
				</tr>
				<%adoA.movenext
				loop
				set adoA=nothing%>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%" align="center"><%call button_Criar("submit","btn1","Gravar","")%></td>
	</tr>
</table>
<%call form_Fim()%>

<!--#include virtual="public/pub_FimOculto.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->


<%
fechar=request("fechar")
if fechar=1 then%>
	<script language="VBSCRIPT">
		window.close()
	</script>
<%
end if
%>