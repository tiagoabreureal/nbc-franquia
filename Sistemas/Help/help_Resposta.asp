<%
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'MODELO DE ARQUIVO DE TELA DE PAR�METROS
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'INCLUDES OBRIGAT�RIOS%>
<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<!--#include virtual="public/pub_BodyOculto.asp"-->
<%

'C�DIGO DA APLICA��O (SYS_APLICA��O)
call Verifica_Permissao(449)

tipo	=trim(request("tipo"))
doc		=trim(request("doc"))

if len(trim(tipo))=0 then tipo=1

strSql="select descricao from doc_Tipo where tipo=" & tipo
set adoA=conn5.execute(strSql)
if not adoA.eof then
	descTipo=adoA("descricao")
end if
set adoA=nothing

strSql=	"select a.descricao,b.descricao DescCampo,c.descricao doc " & _
		"from doc_Resposta a, doc_Campo b, doc_Documento c " & _
		"where a.tipo=b.tipo and a.campo=b.campo and a.tipo=c.tipo and a.doc=c.doc " & _
		" and a.tipo=" & tipo & " and a.doc=" & doc & " and c.status=0 " & _
		" order by a.campo"
		'Response.Write strsql
		'Response.end
set adoA=conn5.execute(strSql)
if not adoA.eof then
	descDoc	=adoA("doc")
end if

%>
<table border="1" style='border-collapse: collapse' width="500" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td width="100%" bgcolor="#c0c0c0" align="center"><%call label(descTipo,2,"center","")%></td>
	</tr>
	<tr>
		<td width="100%"  align="center"><%call label("Documento: " & descDoc,2,"center","")%></td>
	</tr>
	<tr>
		<td width="100%">
			<table border="0" width="100%" cellpadding="0" cellspacing="0" align="center">
				<%do while not adoA.eof%>				
				<tr>
					<td width="100%"><b><%call label(adoA("DescCampo"),"1","left","")%></b></td>
				</tr>
				<tr>
					<td width="100%" align="justify"><%call label(replace(adoA("descricao"),chr(13) + chr(10),"<BR>"),"1","justify","")%></td>
				</tr>
				<tr>
					<td width="100%">&nbsp</td>
				</tr>
				<%adoA.movenext
				loop
				set adoA=nothing%>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%">
			<%
			strSql=	"select a.status,b.descricao,a.usuario,a.data,a.obs " & _
					"from doc_Status a, doc_TipoStatus b " & _
					"where a.status=b.status and tipo=" & tipo & " and doc=" & doc & _
					" order by data desc"
			set adors1=conn5.execute(strSql)
				
			call define_coluna ("Status","descricao","15","c","t","n","s","","")
			call define_coluna ("usu�rio","usuario","10","c","t","n","s","","")
			call define_coluna ("data","data","20","c","t","n","n","","")
			call define_coluna ("observa��o","obs","*","e","t","n","n","","")
				
			call fun_rel_cab()
			do while not adoRs1.eof
				call fun_rel_det()
				adoRs1.movenext
			loop
			call fun_fim_rel()
			%>
				</table>
		</td>
	</tr>	
	<tr>
		<td width="100%">
			<%	call form_Criar("frm1","../doc/doc_Status.asp")
					call txtOculto_Criar("tipo",tipo)
					call txtOculto_Criar("doc",doc)
					call button_Criar("submit","btn1","Inserir Observa��o","")
				call form_Fim()
			%>
		</td>
	</tr>	
</table>

<!--#include virtual="public/pub_FimOculto.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->


<%
fechar=request("fechar")
if fechar=1 then%>
	<script language="VBSCRIPT">
		window.close()
	</script>
<%
end if
%>