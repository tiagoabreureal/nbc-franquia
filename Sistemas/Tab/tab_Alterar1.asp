<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%
call Verifica_Permissao(session("tab_aplicacao_temp"))
%>
<!--#include virtual="public/pub_Body.asp"-->
<!--#include file='tab_Funcoes.asp'-->
<%
cod		=session("tab_cod")
item	=request("item")

tabela	=session("tab_nome")
%>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%"><font size="2" face="Verdana"><b>.<%=session("tab_titulo")%></b></font></td>
    <td width="50%" align="right"><font size="2" face="Verdana"><b>Altera��o</b></font></td>
  </tr>
  <tr>
    <td width="100%" colspan="2"><font size="1" face="Verdana"><hr size="1" color="black"></font></td>
  </tr>
</table>
<%

mensagem=""
somacampo=""
str1=""
str2=""
str3=""
chave=split(session("tab_chave"),",")

conta=0
item2	=split(item,",")
for i=0 to ubound(item2)
	str1=""
	item3	=split(item2(i),"|")
	for j=0 to ubound(item3)
		strsql="select tipo_dado from view_tab where cod=" & cod & " and campo='" & chave(j) & "'"
		set adoa=conn5.execute(strsql)
		if not adoa.eof then tipo_dado=adoa("tipo_dado")
		set adoa=nothing
			
		conteudo=trim(item3(j))
		if len(trim(conteudo))=0 and tipo_dado="6" then conteudo="0"

		if tipo_dado=1 then '' texto
			str1=str1 & chave(j) & "='" & conteudo & "' and "
		elseif tipo_dado=2 or tipo_dado=3 or tipo_dado=6 then
			str1=str1 & chave(j) & "=" & replace(replace(conteudo,".",""),",",".") & " and "
		elseif tipo_dado=4 then
			str1=str1 & chave(j) & "='" & dt_fim(conteudo) & "' and "
		end if
			
	next
	if len(trim(str1))>0 then str1=mid(str1,1,len(str1)-5)
		
	if len(trim(str1))=0 then
		session("msg")="Nenhum campo chave foi definido para essa tabela. N�o � poss�vel alterar."
	end if
next

tot_c=ubound(chave)
dim chave2(2,10)
dim gerar_log(1000)
dim conta_log

conta_log=0
mensagem=""
strsql="select campo,descricao,obrigatorio,tipo_dado,valor_default,valor_default_Gravar=isnull(valor_default_Gravar,''),altera from view_tab where cod=" & cod & " order by ordem"
set adoa=conn5.execute(strsql)
do while not adoa.eof
	campo		=replace(replace(trim(adoa("campo")),"[",""),"]","")
	descricao	=trim(adoa("descricao"))
	tipo_dado	=trim(adoa("tipo_dado"))
	conteudo	=trim(request(campo))
	valor_default_Gravar=trim(adoa("valor_default_Gravar"))
	obrigatorio	=trim(adoa("obrigatorio"))
	altera		=trim(adoa("altera"))
	
	if tipo_dado="6" and len(trim(conteudo))=0 then conteudo="0"

	somacampo=somacampo & campo

	if len(trim(conteudo))=0 and tipo_dado<>"6" and obrigatorio="1" and len(valor_default_gravar)=0 and altera="1" then
		mensagem=mensagem & " - " & descricao & "<br>"
	else
		if altera="1" then
			if tipo_dado=1 or tipo_dado=7 or tipo_dado=8 then '' texto
				str2=str2 & "[" & campo & "]='" & replace(conteudo,"'","''") & "',"
			elseif tipo_dado=2 or tipo_dado=3 or tipo_dado=6 then
				if len(trim(conteudo))=0 then
					str2=str2 & "[" & campo & "]=null,"
				else
					str2=str2 & "[" & campo & "]=" & replace(replace(conteudo,".",""),",",".") & ","
				end if
			elseif tipo_dado=4 then
				if len(trim(valor_default_gravar))>0 then 
					str2=str2 & "[" & campo & "]=" & valor_default_gravar & ","
				else
					if len(trim(conteudo))=0 then
						str2=str2 & "[" & campo & "]=null,"
					else
						str2=str2 & "[" & campo & "]='" & dt_fim(conteudo) & "',"
					end if
				end if
			end if
		end if
	end if
	
	'' rotina de log
	if session("tab_gerar_log")=1 then
		strsql= "insert into sys_alteracaotabela (tabela,campo,atual,anterior,alterado,usuario,destinatario,obs) values " & _
				"('" & tabela & "','" & campo & "','" & mid(replace(replace(conteudo,".",""),",","."),1,5000) & "','',getdate(),'" & session("usuario") & "','','Alterado')"
		conta_log=conta_log+1
		gerar_log(conta_log)=strsql
	end if
	''
	
	adoa.movenext
	if not adoa.eof then 
		somacampo=somacampo & ","
	end if
loop
if len(trim(str2))>0 then str2=mid(str2,1,len(str2)-1)

if len(trim(mensagem))>0 then
	%>
	<p align="left">
	<font face="verdana" size="2" color="black">Necess�rio preencher o(s) seguinte(s) campo(s):</font><br>
	  <font face="verdana" size="1" color="red"><%=mensagem%></font><br>
	  <center><font face="verdana" size="2" color="black"><a href="javascript:history.back()">voltar</a></font></center>
	<%
else
	str1="update " & tabela & " set " & str2 & " where " & str1
	strsql=str1
	locconnvenda.execute(strsql)
	
	''gerar log
	if conta_log>0 then
		for i=1 to conta_log
			locconnvenda.execute(gerar_log(i))
		next
	end if
	
	Response.Redirect "tab_dados.asp"
end if

set adoa=nothing
%>

<!--#include virtual="public/pub_FimOculto.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->