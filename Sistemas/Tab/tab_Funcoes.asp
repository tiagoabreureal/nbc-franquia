<%
function gerar_codigo()
	strsql="exec ped_CodigoDataHora_SP_S"
	set adoa=locconnvenda.execute(strsql)
	if not adoa.eof then
		gerar_codigo=adoa("codigo")
	else
		gerar_codigo=""
	end if
	set adoa=nothing
end function

sub zerar_sessao()
	session("tab_cod")	=""
	session("tab_nome")	=""
	session("tab_titulo")=""
	session("tab_obs")=""
	session("tab_data")=""
	session("tab_usuario")=""
	session("tab_chave")=""
	session("tab_aplicacao_temp")=""
	session("tab_permissao_consultar")=""
	session("tab_permissao_alterar")=""
	session("tab_permissao_incluir")=""
	session("tab_permissao_excluir")=""
	session("tab_query")=""
	session("tab_gerar_log")=""
	session("tab_nivel_acesso")=""
	session("tab_nivel_acesso_cor")=""
	session("tab_nivel_acesso_ok")=""
	session("tab_nivel_acesso_texto")=""
end sub

sub carrega_tab(cod)
	session("tab_cod")	=cod

	strsql=	"select a.nome,a.titulo,a.data_cadastro,a.usuario,a.obs,query=isnull(a.query,'')" & _
			",consulta=isnull(b.consulta,0),altera=isnull(b.altera,0),inclui=isnull(inclui,0),exclui=isnull(b.exclui,0)" & _
			",gerar_log_alteracao=isnull(a.gerar_log_alteracao,0),nivel_acesso=isnull(a.nivel_acesso,0) " & _
			"from sys_tab a inner join sys_tab_permissao b on a.cod=b.aplicacao " & _
			"where a.cod=" & cod

	set adoa=conn5.execute(strsql)
	if not adoa.eof then 
		session("tab_nome")		=trim(adoa("nome"))
		session("tab_titulo")	=trim(adoa("titulo"))
		session("tab_obs")		=trim(adoa("obs"))
		session("tab_data")		=trim(adoa("data_cadastro"))
		session("tab_usuario")	=trim(adoa("usuario"))
		session("tab_query")	=trim(adoa("query"))
		session("tab_gerar_log")=trim(adoa("gerar_log_alteracao"))
		session("tab_nivel_acesso")=trim(adoa("nivel_acesso"))
		
		if session("tab_nivel_acesso")>90 then
			session("tab_nivel_acesso_cor")="red"
			session("tab_nivel_acesso_texto")="* Tabela de Sistema"
		else
			session("tab_nivel_acesso_cor")="black"
			session("tab_nivel_acesso_texto")=""
		end if
		
		if (session("tab_nivel_acesso")<90 or session("usuario")="ADMIN" or session("usuario")="KLEBER") then
			session("tab_nivel_acesso_ok")=1
		else
			session("tab_nivel_acesso_ok")=0
		end if
		
		session("tab_aplicacao_temp")=cod
		session("tab_permissao_consultar")=adoa("consulta")
		session("tab_permissao_alterar")=adoa("altera")
		session("tab_permissao_incluir")=adoa("inclui")
		session("tab_permissao_excluir")=adoa("exclui")
	end if
	set adoa=nothing
	
	a=""
	strsql=	"select a.campo from sys_tab_chave a inner join sys_tab_campo b on a.cod=b.cod and a.campo=b.campo " & _
			"where a.cod=" & cod & " order by b.ordem"
	set adoa=conn5.execute(strsql)
	do while not adoa.eof
		a=a & trim(adoa("campo"))
		adoa.movenext
		if not adoa.eof then a=a & ","
	loop
	session("tab_chave")=a	
	set adoa=nothing
end sub

sub verifica_sessao()
	if len(trim(session("pedido")))=0 then
		call redi_aviso("Aten��o","A sess�o foi finalizada porque ficou muito tempo sem atividade.<br><a href='../cliente/cli_PesquisarCliente.asp' target='_parent'>Clique aqui para prosseguir.</a>")
	end if
end sub
%>	



