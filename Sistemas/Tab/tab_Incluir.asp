<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%
call Verifica_Permissao(session("tab_aplicacao_temp"))
%>
<!--#include virtual="public/pub_Body.asp"-->
<!--#include file='tab_Funcoes.asp'-->

<%
cod		=session("tab_cod")
continuar_inserindo=trim(request("continuar_inserindo"))

flag_ordem	=request("flag_ordem")
%>

<table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="5%"><font size="2" face="Verdana"><img src="mail20.ico" border="0"></b></font></td>
    <td width="50%"><font size="2" face="Verdana"><b>.<%=session("tab_titulo")%></b></font></td>
    <td width="50%" align="right"><font size="2" face="Verdana"><b>Inclus�o</b></font></td>
  </tr>
  <tr>
    <td width="100%" colspan="3"><font size="1" face="Verdana"><hr size="1" color="black"></font></td>
  </tr>
   <%
	if session("tab_nivel_acesso")>90 then
	%>
	<tr>
		<td align="left" colspan="3"><font face="verdana" size="1" color="<%=session("tab_nivel_acesso_cor")%>"><i><%=session("tab_nivel_acesso_texto")%></i></font></td>
	</tr>
	<%
	end if
	%>
</table>

<table border="0" width="100%" cellpadding="0" cellspacing="1">
	<%call form_criar("frmIncluir","tab_Incluir1.asp")
	%>
	
	<tr><td width="100%">
			<%call monta_tabela(cod)%>
     	 </td>
	</tr>
	<tr><td width="100%">
			<font face="verdana" size="1" color="black"><i>* campos obrigat�rios em </font><font face="verdana" size="1" color="blue">azul</i></font>
     	 </td>
	</tr>
	<tr><td width="100%">
			<%
				if continuar_inserindo="1" then
					marca_inserir="checked"
				else
					marca_inserir=""
				end if
				call checkbox_criar("continuar_inserindo","1","","continuar inserindo",marca_inserir)
			%>
     	 </td>
	</tr>
	
	
	<tr><td width="100%">
			<hr size="1" color="black">
     	 </td>
	</tr>
	<tr><td width="100%" align="center">
			<%
			call button_criar("submit","bnt1","Gravar","")
			response.write "&nbsp;&nbsp;&nbsp;"
			call button_criar("button","bnt3","Voltar","onclick='javascript=window.history.back()'")
			response.write "&nbsp;&nbsp;&nbsp;"
			call button_criar("button","bnt4","Voltar Tela Dados","onclick=javascript:location.href='tab_dados.asp'")
			response.write "&nbsp;&nbsp;&nbsp;"
			call button_criar("button","bnt4","Voltar Menu Principal","onclick=javascript:location.href='tab_index.asp'")
			%>
     	 </td>
	</tr>

	<tr><td width="100%">
			<hr size="1" color="black">
     	 </td>
	</tr>
	<%call form_fim()%>
</table>

<!--#include virtual="public/pub_FimOculto.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->


<%
sub monta_tabela(cod_tab)

	strsql=	"select campo,descricao,tipo_dado,tamanho,precisao=isnull(precisao,0),valor_Default=isnull(valor_Default,'')" & _
			",valor_default_gravar=isnull(valor_default_gravar,''),ultimo_campo=isnull(ultimo_campo,0)" & _
			",obrigatorio=isnull(obrigatorio,'')" & _
			",tab_dependencia=isnull(tab_dependencia,''),mostrar_campo=isnull(mostrar_campo,''),gravar_campo=isnull(gravar_campo,'')" & _
			",tab_where=isnull(tab_where,''),comentario=isnull(comentario,'') " & _
			"from view_tab where cod=" & cod_tab & " order by ordem"
		'Response.Write strsql
		'Response.end
	set adoa=conn5.execute(strsql)
	%>
	<table border="1" width="100%" cellpadding="0" cellspacing="1" style="border-collapse: collapse">
	<%	
		dim conta_fixar
		conta_fixar=0
		do while not adoa.eof
			campo		=replace(replace(trim(adoa("campo")),"[",""),"]","")
			descricao		=trim(adoa("descricao"))
			tipo_dado		=trim(adoa("tipo_dado"))
			tamanho			=adoa("tamanho")
			precisao		=adoa("precisao")
			valor_default	=trim(adoa("valor_default"))
			valor_default_gravar=trim(adoa("valor_default_gravar"))
			ultimo_campo	=adoa("ultimo_campo")
			obrigatorio	=adoa("obrigatorio")
			comentario		=trim(adoa("comentario"))
			
			tab_dependencia	=trim(adoa("tab_dependencia"))
			tab_where		=trim(adoa("tab_where"))
			mostrar_campo	=trim(adoa("mostrar_campo"))
			gravar_campo	=trim(adoa("gravar_campo"))
			flag_dependencia="0"
			
			if cod_tab=10208 and ucase(campo)="ORDEM" and len(flag_ordem)>0 then valor_default=flag_ordem+1

			if len(trim(tab_dependencia))>0 then		
				'strsql=	"select a=convert(varchar(20)," & gravar_campo & ") + ' - ' + convert(varchar(100)," &  mostrar_campo & "),b=" & gravar_campo & _
				strsql=	"select a=" & mostrar_campo & ",b=" & gravar_campo & _
						" from " & tab_dependencia
				if len(tab_where)>0 then strsql=strsql & " " & tab_where
				strsql=strsql & " order by " & mostrar_campo
				'Response.Write strsql
				'Response.end
				set adoMonta=conn5.execute(strsql)
				flag_dependencia="1"
			end if

			if valor_default="getdate()" then valor_default=date()
			
			if tamanho>50 then
				text_tamanho=50			
			else
				text_tamanho=tamanho
			end if
			
			if ultimo_campo>0 then
				
				strsql="select ultimo=isnull(max(convert(numeric(18,0)," & campo & ")),0) + 1 from " & session("tab_nome")
				'on error resume next
				'Response.write strsql
				'Response.end
				'end if
				set adoultimo=conn5.execute(strsql)
				'if err.number<>0 then Response.Write strsql
				
				if not adoultimo.eof then valor_default	=adoultimo("ultimo")
				set adoultimo=nothing
			end if
			
			if len(trim(valor_default_gravar))>0 then
				tipo_dado=9
				conteudo=tipo_dado
			end if
			
			if obrigatorio=1 then
				ob1="blue"
			else
				ob1="black"
			end if
			
			''fixar campo
			conta_fixar=conta_fixar+1
			campo_fixar	=trim(request("campo_fixar000" & conta_fixar))
			
			if len(trim(campo_fixar))>0 then
				fixar		=campo_fixar
				fixar_marca="checked"
			else
				fixar		=""
				fixar_marca	=""
			end if
			''
		%>
			<tr>
				<td width="2%" align="center" valign="top" bgcolor="#FFFFCC">
					<font face="verdana" size="1">
					<%
					if flag_dependencia="1" then
						call checkbox_criar("fixar000" & campo,"1",fixar_marca,"","")
					end if%>
					</font>
				</td>
				<td width="25%" bgcolor="#FFFFCC" valign="top">
					<font face="verdana" size="1" color="<%=ob1%>"><%=descricao%>:</font>
				</td>
				<td width="68%">
					<font face="verdana" size="1">
					<%
					if flag_dependencia="1" then
						'Response.Write adomonta("b") & "-" & valor_default
						'Response.end
						'Response.Write valor_default
						call select_criar(campo,"")
							call select_item("","","")
						do while not adomonta.eof
							if len(trim(valor_default))=0 then valor_default="-1"
							
							if len(fixar)>0 then
								if valor_default="-1" then valor_default=fixar
							end if
							
							if cstr(adomonta("b"))=cstr(valor_default) then
								sel_val1	="selected"
							else
								sel_val1	=""
							end if
							
							call select_item(adoMonta("b"),adoMonta("a"),sel_val1)
							adomonta.movenext
						loop
						call select_Fim()
					else
					
						if tipo_dado=2 or tipo_dado=1 or tipo_dado=4 or tipo_dado=3 or tipo_dado=7 then
							if tipo_dado=1 or tipo_dado=7 then 		''Texto
								call txt_criar("texto",campo,text_tamanho,tamanho,valor_default)
							elseif tipo_dado=2 or tipo_dado=3 then		''Inteiro / decimal
								call txt_criar("inteiro",campo,text_tamanho,tamanho,valor_default)
							elseif tipo_dado=4 then		''Data
								call txt_criar("data",campo,text_tamanho,tamanho,valor_default)
							end if
						elseif tipo_dado=6 then
							if cstr(valor_default)="1" then
								sel1	="checked"
							elseif cstr(valor_default)="0" then
								sel1	=""
							else
								sel1	=""
							end if
							
							call checkbox_criar(campo,"1",sel1,"","")
						'elseif tipo_dado=7 then
						'	call txtSenha_criar(campo,text_tamanho,tamanho,valor_default)
						elseif tipo_dado=8 then ''textarea
							call TextArea_Criar(campo,10,70,valor_default)
							Response.Write tamanho
						elseif tipo_dado=9 then
							if valor_default="[usuario]" then
								Response.Write "[Usu�rio]"
							else
								response.write "Data/Hora"
							end if
							call txtoculto_criar(campo,"")
						end if
					end if
					%>
					</font>
				</td>
				<td width="5%" align="center" valign="top">
					<font face="verdana" size="1">
					<%if len(comentario)>0 then%>
					<img src="../../imagem/sistema/img_interrog.gif" border="0" alt="<%=comentario%>"></font>
					<%end if%>
				</td>
			</tr>
		<%
			adoa.movenext
		loop
		set adoa=nothing
	%>
	
	</table>
	<%
end sub
%>