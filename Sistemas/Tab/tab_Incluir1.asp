<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%
call Verifica_Permissao(session("tab_aplicacao_temp"))
%>
<!--#include virtual="public/pub_Body.asp"-->
<!--#include file='tab_Funcoes.asp'-->
<%
cod		=session("tab_cod")
tabela	=session("tab_nome")

continuar_inserindo=trim(request("continuar_inserindo"))
%>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%"><font size="2" face="Verdana"><b>.<%=session("tab_titulo")%></b></font></td>
    <td width="50%" align="right"><font size="2" face="Verdana"><b>Inclusão</b></font></td>
  </tr>
  <tr>
    <td width="100%" colspan="2"><font size="1" face="Verdana"><hr size="1" color="black"></font></td>
  </tr>
</table>
<%

mensagem=""
somacampo=""
str1=""
str2=""
str3=""
chave=split(session("tab_chave"),",")

tot_c=ubound(chave)
dim chave2(2,10)
'for i=1 to 2
'	for j=1 to 10
'		chave2(i,j)=""
'	next
'next


dim gerar_log(1000)
dim conta_log
dim campo_fixar(30)
dim conta_fixar
dim link_fixar

conta_log=0
conta_fixar=0

strsql="select campo,descricao,obrigatorio,tipo_dado,valor_default,valor_default_Gravar=isnull(valor_default_Gravar,'') from view_tab where cod=" & cod & " order by ordem"
set adoa=conn5.execute(strsql)

do while not adoa.eof
	campo		=replace(replace(trim(adoa("campo")),"[",""),"]","")
	descricao	=trim(adoa("descricao"))
	tipo_dado	=trim(adoa("tipo_dado"))
	valor_default_Gravar=trim(adoa("valor_default_Gravar"))
	obrigatorio	=trim(adoa("obrigatorio"))

	conteudo	=trim(request(campo))
	''ordem
	if ucase(campo)="ORDEM" then flag_ordem=conteudo
	''
	
	if tipo_dado="6" and len(trim(conteudo))=0 then conteudo="0"
	
	somacampo=somacampo & "[" & campo & "]"

	if len(conteudo)=0 and tipo_dado<>"6" and obrigatorio="1" and len(trim(valor_default_Gravar))=0 then
		mensagem=mensagem & " - " & descricao & "<br>"
	else
		if tipo_dado=1 or tipo_dado=7 or tipo_dado=8 then '' texto / senha / texto longo
			if len(valor_default_gravar)=0 then
				str2=str2 & "'" & replace(conteudo,"'","''") & "'"
			else
				if valor_default_gravar="[usuario]" then
					str2=str2 & "'" & session("usuario") & "'"
				else
					str2=str2 & "'" & replace(conteudo,"'","''") & "'"
				end if
			end if
		elseif tipo_dado=2 or tipo_dado=3 or tipo_dado=6 then
			if len(trim(conteudo))=0 then
				str2=str2 & "null"
			else
				str2=str2 & replace(replace(conteudo,".",""),",",".")
			end if
		elseif tipo_dado=4 then
			if len(valor_default_Gravar)=0 then
				if len(trim(conteudo))=0 then
					str2=str2 & "null"
				else
					str2=str2 & "'" & dt_fim(conteudo) & "'"
				end if
			else
				str2=str2 & valor_default_Gravar
			end if
		end if
		
	end if
	
	for i=0 to ubound(chave)
		
		if ucase(chave(i))=ucase(campo) then
			chave2(1,i)=descricao
			chave2(2,i)=conteudo
			
			if tipo_dado=1 or tipo_dado=7 or tipo_dado=8 then '' texto
				str3=str3 & trim(chave(i)) & "='" & conteudo & "' and "
			elseif tipo_dado=2 or tipo_dado=3 or tipo_dado=6 then '' numero / decimal
				str3=str3 & trim(chave(i)) & "=" & replace(replace(conteudo,".",""),",",".") & " and "
			elseif tipo_dado=4 then '' data
				'hora=" " & hour(time) & ":" & minute(time) & ":" & second(time)
				str3=str3 & trim(chave(i)) & "='" & dt_fim(conteudo) & "' and "
			end if
		end if
		'str3 = str3
	next
	
	''registrar log
		strsql= "insert into sys_alteracaotabela (tabela,campo,atual,anterior,alterado,usuario,destinatario,obs) values " & _
				"('" & tabela & "','" & campo & "','" & mid(replace(replace(conteudo,".",""),",","."),1,5000) & "','',getdate(),'" & session("usuario") & "','','Incluído')"
		conta_log=conta_log+1
		gerar_log(conta_log)=strsql
	''
	''fixar campo
	fixar	=request("fixar000" & campo)
	conta_fixar=conta_fixar+1
	if len(fixar)>0 then campo_fixar(conta_fixar)=conteudo
	''
	
	adoa.movenext
	if not adoa.eof then 
		somacampo=somacampo & ","
		str2 = str2 & ","
	end if
loop
if len(trim(str3))>0 then str3=mid(str3,1,len(str3)-5)

if len(trim(mensagem))>0 then
	%>
	<p align="left">
	<font face="verdana" size="2" color="black">Necessário preencher o(s) seguinte(s) campo(s):</font><br>
	  <font face="verdana" size="1" color="red"><%=mensagem%></font><br>
	  <center><font face="verdana" size="2" color="black"><a href="javascript:history.back()">voltar</a></font></center>
	<%
else
	
	''verifar chave
	if len(trim(str3))>0 then
		strsql=	"select " & chave(0) & " from " & tabela & _
				" where " & str3
		set adoa=conn5.execute(strsql)
		if not adoa.eof then
		%><p align="left">
		  <font face="verdana" size="2" color="black">Campo(s) de identificação em duplicidade, verifique:</font><br>
		  <%
		  for i=0 to ubound(chave)
		  %><font face="verdana" size="1" color="red">- <%=chave2(1,i) & " = " & chave2(2,i)%></font><br>
		  <%
		  next
		  %><br>
		  <center><font face="verdana" size="2" color="black"><a href="javascript:history.back()">voltar</a></font></center>
		<%
			Response.end	
		end if
		set adoa=nothing

	end if
	''
	
	str1="insert into " & tabela & " (" & somacampo & ") values (" & str2 & ")"
	strsql=str1
	'Response.Write strsql
	'Response.end
	locconnvenda.execute(strsql)
	
	''gerar log
	if conta_log>0 then
		for i=1 to conta_log
			locconnvenda.execute(gerar_log(i))
		next
	end if

	if continuar_inserindo="1" then
		link_fixar=""
		for i=1 to 30
			if len(trim(campo_fixar(i)))>0 then link_fixar=link_fixar & "&campo_fixar000" & i & "=" & campo_fixar(i)
		next
		Response.Redirect "tab_incluir.asp?continuar_inserindo=" & continuar_inserindo & "&flag_ordem=" & flag_ordem & link_fixar
	else
		Response.Redirect "tab_dados.asp"
	end if
end if

set adoa=nothing
%>

<!--#include virtual="public/pub_FimOculto.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->