<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%call Verifica_Permissao(239)%>
<!--#include virtual="public/pub_Body.asp"-->
<%
strT=request("t")

strSql=	"select a.descricao,solicitado,inicio,fim," + _
		"previsao,atualizado,usuariocad,b.descricao descricaoS,b.cor " + _
		"from cpd_Task a, cpd_TaskStatus b " + _
		"where a.status=b.status and a.tarefa=" & strT
set adoA=locConnVenda.execute(strSql)
if not adoA.eof then
	strDesc	=adoA("descricao")
	strSol	=adoA("solicitado")
	strIni	=adoA("inicio")
	strFim	=adoA("fim")
	strPrev	=adoA("previsao")
	strAtu	=adoA("atualizado")
	strCad	=adoA("usuarioCad")
	strDescS=adoA("descricaoS")
	strCor	=adoA("cor")
end if
adoA.close		
dif=datediff("d",strIni,strPrev)
%>
  <table style="border-collapse: collapse" bordercolor="#C0C0C0" border="1" cellpadding="0" cellspacing="1" width="100%">
    <tr>
      <td width="100%">
        <p align="center"><font size="2" face="Verdana" color="#0000FF"><b>Detalhe
        de Tarefa - <%=session("usuarioTask")%></b></font></td>
    </tr>
    <tr>
      <td width="100%">&nbsp;
        <div align="center">
          <table border="0" width="95%">
            <tr>
              <td width="8%" align="right" bgcolor="#EAEAFF"><font size="1" face="Verdana">Tarefa:</font></td>
              <td width="92%" colspan="7"><font size="1" face="Verdana"><%=strDesc%></font></td>
            </tr>
            <tr>
              <td width="8%" align="right" bgcolor="#EAEAFF"><font size="1" face="Verdana">Solicitado:</font></td>
              <td width="53%" colspan="4"><font size="1" face="Verdana"><%=strSol%></font></td>                          
              <td width="10%" bgcolor="#EAEAFF" align="right">
                 <font size="1" face="Verdana">Status:</font></td>
              <td width="28%" colspan="2" bgcolor="<%=strCor%>" align="center">
                 <font size="1" face="Verdana"><%=strDescS%></font></td>
            </tr>
            <tr>
              <td width="8%" align="right" bgcolor="#EAEAFF"><font size="1" face="Verdana">In�cio:</font></td>
              <td width="18%"><font size="1" face="Verdana"><%=strIni%></font></td>
              <td width="10%" bgcolor="#EAEAFF" align="right">
                 <font size="1" face="Verdana">Previs�o:</font></td>
              <td width="12%"><font size="1" face="Verdana"><%=strPrev%></font></td>
              <td width="13%" bgcolor="#EAEAFF" align="right">
                 <font size="1" face="Verdana">Dias:</font></td>
              <td width="10%" align="center"><font size="1" face="Verdana"><%=dif%></font></td>
              <td width="8%" bgcolor="#EAEAFF" align="right">
                 <font size="1" face="Verdana">Fechado:</font></td>
              <td width="20%"><font size="1" face="Verdana"><%=strFim%></font></td>
            </tr>
            <tr>
              <td width="100%" align="right" colspan="8">
                <br>
              </td>
            </tr>
            <tr>
              <td width="26%" align="right" colspan="2"><font size="1" face="Verdana">Cadastrado
                por:</font></td>            
            <td width="22%" colspan="2" align="left">
              <p align="left"><font size="1" face="Verdana"><%=strCad%></font></td>
            <td width="31%" colspan="3">
              <p align="center"><font size="1" face="Verdana"><%=strAtu%></font></td>
            <td width="20%"></td>
            </tr>
          </table>
        </div></td>
    </tr>
    <tr>
      <td width="100%">&nbsp;
        <div align="center">
          <table border="0" cellpadding="0" cellspacing="0" width="98%">
            <tr>
              <td width="75%" valign="top" align="center"><font size="1" face="Verdana"><b>MOVIMENTO DE STATUS</b></font>
                <div align="center">
                  <table style="border-collapse: collapse" bordercolor="#C0C0C0" border="0" cellpadding="0" cellspacing="1" width="98%">
                    <tr>
                      <td width="25%" align="center" bgcolor="#C0C0C0"><font size="1" face="Verdana">Data</font></td>
                      <td width="10%" align="center" bgcolor="#C0C0C0"><font size="1" face="Verdana">Status</font></td>
                      <td width="15%" align="center" bgcolor="#C0C0C0"><font size="1" face="Verdana">Usu�rio</font></td>
                      <td width="50%" align="center" bgcolor="#C0C0C0"><font size="1" face="Verdana">Observa��o</font></td>
                    </tr>
                    <%strSql="select descricao,cor,usuario,data,obs " + _
							 "from cpd_TaskMovStatus a, cpd_TaskStatus b " + _
							 "where a.status=b.status and a.tarefa=" & strT + _
							 " order by data desc"
					  set adoStatus=locConnVenda.execute(strSql)
					  do while not adoStatus.eof
						if alt=1 then
							strcor="bgcolor='#f5f5f5'"
							alt=0
						else
							strcor=""
							alt=1
						end if		
					  
					  %>
                    <tr>
                      <td width="25%" align="center" bgcolor="<%=adoStatus("cor")%>">
						<font face="verdana" size="1"><%=adoStatus("data")%></font></td>
                      <td width="10%" align="center" <%=strCor%>>
						<font face="verdana" size="1"><%=adoStatus("descricao")%></font></td>
                      <td width="15%" align="center" <%=strCor%>>
                        <font face="verdana" size="1"><%=adoStatus("usuario")%></font></td> 
                      <td width="50%" align="left" <%=strCor%>>
                        <font face="verdana" size="1"><%=adoStatus("obs")%></font></td>                    
                    </tr>
                   <%	
                    adoStatus.movenext
                    loop
                    adoStatus.close%>
                  </table>
                </div>
              </td>
              <td width="25%" valign="top" align="center"><font size="1" face="Verdana"><b>NOTIFICA��O</b></font>
                <div align="center">
                  <table style="border-collapse: collapse" bordercolor="#C0C0C0" border="0" cellpadding="0" cellspacing="1" width="95%">
                    <tr>
                      <td width="100%" bgcolor="#C0C0C0">
                        <p align="center"><font size="1" face="Verdana">Data</font></td>
                    </tr>
                    <%
                    strSql="select data,usuario " + _
						   "from cpd_tasknotificacao where tarefa=" & strT & " order by data desc"
					set adoN=locConnVenda.execute(strSql)
					do while not adoN.eof				
                    %>
                    <tr>
                      <td width="100%" align="center">
                        <font face="verdana" size="1"><%=adoN("data")%></font></td>
                    </tr>
                    <%adoN.movenext
                    loop
                    adoN.close%>
                  </table>
                </div>
              </td>
            </tr>
          </table>
</td>
    </tr>
  </table>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->