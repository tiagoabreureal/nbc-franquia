<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<%call Verifica_Permissao(10193)%>
<!--#include virtual="public/pub_BodyOculto.asp"-->
<%

raiz		=request("raiz")
pasta		=request("pasta")
opcao		=request("opcao")

id_arquivo	=trim(request("id_arquivo"))
id_pasta	=trim(request("id_pasta"))

if len(id_arquivo)=0 and len(id_pasta)=0 then
	session("msg")	= "Nenhum arquivo ou pasta selecionado!"
	Response.redirect "up_listar1.asp?raiz=" & raiz & "&pasta=" & pasta
end if

arquivo		=split(replace(trim(id_arquivo)," ",""),",")
apagar_pasta=split(replace(trim(id_pasta)," ",""),",")
caminho	=raiz & pasta & "\"

set Fsys=server.CreateObject("scripting.fileSystemObject")

if opcao="D" then '' deletar arquivos
	for i=0 to ubound(arquivo)
		if fsys.FileExists(caminho & arquivo(i)) then fsys.DeleteFile caminho & arquivo(i)	
	next
	
	for i=0 to ubound(apagar_pasta)
		if fsys.FolderExists(caminho & apagar_pasta(i)) then fsys.DeleteFolder caminho & apagar_pasta(i),1
	next

	session("msg")	="Arquivo(s) deletado(s) com sucesso"
elseif opcao="DW" then

	if len(id_arquivo)=0 then
		session("msg")	= "Nenhum arquivo selecionado para Download!"
		Response.redirect "up_listar1.asp?raiz=" & raiz & "&pasta=" & pasta
	end if
	
	dim MyUpLoad
	set MyUpLoad=server.CreateObject("AspSmartUpload.SmartUpload")
	
	
	for i=0 to ubound(arquivo)
		if fsys.FileExists(caminho & arquivo(i)) then 
			'Response.Write "achou"
			'Response.end
			MyUpLoad.downloadFile caminho & arquivo(i), "application/x-zip-compressed","ab.txt"
		end if
	next
	
end if

Response.redirect "up_listar1.asp?raiz=" & raiz & "&pasta=" & pasta
%>
<!--#include virtual="public/pub_Fim.asp"-->
<!--#include virtual="public/pub_Geral1.asp"-->