﻿// Codigo escrito por NiL 05/12/2014 \ó
$(document).ready(function () {
    //Função que chama a tela de cadastros para tela principal
    $("#cad_categoria").click(function () {
        //evt.preventDefault();
        //alert("entrou ?");
        var href = "fincdh_cadastro_categoria.asp";

        $.ajax({
            type: "POST",
            url: href,
            //data: "url="+ href,
            beforeSend: function () {
                $("#pop").css({ display: "Block" });
            },
            success: function (data) {
                $("#pop").css({ display: "none" });

                $("#pagina").html(data);

                $("#btn_gravar").show();
                $("#btn_alterar").css({ display: "none" });
                $("#descricao_categoria").focus();

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");
            }
        });
    });

    // função para listar as categorias de acordo com a descrição
    $("#Listar_categoria").click(function () {
        var descricao = $("#descricao").val();
       
            var href = "fincdh_categoria.asp";

            $.ajax({
                type: "POST",
                url: href,
                data: {descricao:descricao},
                beforeSend: function () {
                    $("#pop").css({ display: "Block" });
                },
                success: function (data) {
                    $("#pop").css({ display: "none" });

                    $("#pagina").html(data);
                    $("#descricao").focus();

                },
                error: function () {
                    $("#pop").css({ display: "none" });
                    alert("Erro");

                }

            });//Fim do ajax
    });

    //função botão voltar
    $("#btn_voltar").click(function () {

        var href = "fincdh_categoria.asp";

        $.ajax({
            type: "POST",
            url: href,
            beforeSend: function () {
                $("#pop").css({ display: "Block" });
            },
            success: function (data) {
                $("#pop").css({ display: "none" });

                $("#pagina").html(data);

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");

            }
        });
    });

    // Função para gravar nova inclusão 
    $("#btn_gravar").click(function () {

        var descricao = $("#descricao_categoria").val();
        var tipo = $("#tipo option:selected").val();
        //alert(tipo);

        if (descricao == "") {
            alert("Digite uma descrição !");
            return false;
        }
       //Ajax para gravar a nova inclusão
       $.ajax({
            type: "post",
            url: "ajax_cadastrar.asp",
            data: { acao: 'cadastrar_categoria', descricao: descricao , tipo: tipo },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                alert(resposta);

                $("#pop").css({ display: "none" });

                $("#btn_voltar").click();

            },
            error: function () {

                $("#pop").css({ display: "none" });
                alert("Erro");

            }
        });//Fim do Ajax*/
    });

    //Função para selecionar uma linha de registro para ser alterado
    $("#pagina a").click(function (evt) {

        evt.preventDefault();

        var id = $(this).text();
        //alert(id);

        // ajax que vai buscar os dados da categoria para alteração        
        $.ajax({
            type: "post",
            url: "ajax_cadastrar.asp",
            data: { acao: 'buscar_categoria', id: id  },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                var res_array = resposta.split("|");
                var id =res_array[1];
                var descricao = res_array[2];
                var tipo = res_array[3];


                $("#pop").css({ display: "none" });
                //ajax que envia os dados da categoria para a pagina de cadastro para alteração
                $.ajax({
                    type: "POST",
                    url: "fincdh_cadastro_categoria.asp",
                    /*data: {opcao : 1, id: id, descricao: descricao, tipo: tipo },
                    async: false,
                    cache: false,
                    datatype: "text",*/
                    beforeSend: function () {
                        $("#pop").css({ display: "Block" });
                    },
                    success: function (data) {

                        $("#pagina").html(data);

                        $("#pop").css({ display: "none" });

                        $("#id").val(id);
                        $("#descricao_categoria").val(descricao);
                        $("#tipo").val(tipo);

                        $("#btn_gravar").css({ display: "none" });
                        $("#btn_alterar").show();
                        $("#descricao_categoria").focus();



                    },
                    error: function () {
                        $("#pop").css({ display: "none" });
                        alert("Erro");
                    }
                });

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");
            }
        });
    });

    //função para gravar as alterações assim que ele clicar no botao alterar
    $("#btn_alterar").click(function () {
        
        var id = $("#id").val();
        var descricao = $("#descricao_categoria").val();
        var tipo = $("#tipo option:selected").val();


        $.ajax({
            type: "post",
            url: "ajax_cadastrar.asp",
            data: { acao: 'alterar_categoria', id: id, descricao: descricao, tipo: tipo },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                $("#pop").css({ display: "none" });

                alert(resposta);

                $("#btn_voltar").click();

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");

            }
        });//Fim do Ajax
    });
});


// Função js para deletar aplicada no botão [x]
function deletar(id) {
    var id = id;
    //alert(id);
    var confirma = confirm("Deseja mesmo deletar esse cadastro ??");

    if(confirma ==  true){

    $.ajax({
        type: "post",
        url: "ajax_cadastrar.asp",
        data: { acao: 'deletar_categoria', id: id},
        async: false,
        cache: false,
        datatype: "text",
        beforeSend: function () {

            $("#pop").css({ display: "Block" });

        },
        success: function (resposta) {

            $("#pop").css({ display: "none" });

            alert(resposta);

            $("#btn_voltar").click();

        },
        error: function () {
            $("#pop").css({ display: "none" });
            alert("Erro");
        }
    });//Fim do Ajax*/
    } else {
        $("#btn_voltar").click();
    }
}