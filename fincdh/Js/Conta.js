﻿$(document).ready(function(){

    // Função que chama a tela de cadastro de conta
    $("#cad_conta").click(function () {

        //evt.preventDefault();
        //alert("entrou ?");
        var href = "fincdh_cadastro_conta.asp";

        $.ajax({
            type: "POST",
            url: href,
            //data: "url="+ href,
            beforeSend: function () {
                $("#pop").css({ display: "Block" });
            },
            success: function (data) {
                $("#pop").css({ display: "none" });

                $("#pagina").html(data);

                $("#descricao").focus();
                $("#btn_alterar").hide();

            }

        });
    });

    //Função para listar previsões
    $("#lista_conta").click(function () {
        var descricao = $("#descricao").val();

        var href = "fincdh_conta.asp";


        $.ajax({
                type: "POST",
                url: href,
                data: { descricao: descricao },
                beforeSend: function () {
                    $("#pop").css({ display: "Block" });
                },
                success: function (data) {
                    $("#pop").css({ display: "none" });

                    $("#pagina").html(data);
                    $("#descricao").focus();

                },
                error: function () {
                    $("#pop").css({ display: "none" });
                    alert("Erro");

                }

            });//Fim do ajax*/
    });
    
    // Função voltar para tela de listagem
    $("#btn_voltar").click(function () {

        var href = "fincdh_conta.asp";

        $.ajax({
            type: "POST",
            url: href,
            beforeSend: function () {
                $("#pop").css({ display: "Block" });
            },
            success: function (data) {
                $("#pop").css({ display: "none" });

                $("#pagina").html(data);


            }
        });
    });


    // Função para gravar nova inclusão
    $("#btn_gravar").click(function () {
        
        var descricao = $("#descricao").val();
        var cod_banco = $("#cod_banco option:selected").val();
        var num_conta = $("#num_conta").val();
        var dig_conta = $("#dig_conta").val();
        var num_agencia = $("#num_agencia").val();
        var dig_agencia = $("#dig_agencia").val();
        var saldo_inicial = $("#saldo_inicial").val();
        saldo_inicial = saldo_inicial.replace(/[^\d]+/g, '.');


        if (descricao == "") {
            alert("Descrição é um campo obrigatorio !");
            $("#descricao").focus();
            return false;
        }

        if (saldo_inicial == "") {
            alert("saldo inicial é obrigatorio");
            $("#saldo_inicial").focus();
            return false;
        }

        if (isNaN(saldo_inicial)) {
            $("#saldo_inicial").val("");
            alert("Valor só aceita numéricos");
            $("#saldo_inicial").focus();
            return false;
        }


        //Ajax que envia os dados para os asp fazer a inserção do novo registro \ó
        $.ajax({
            type: "post",
            url: "ajax_cadastrar.asp",
            data: {
                acao: 'cadastrar_conta', descricao: descricao, cod_banco: cod_banco, num_conta: num_conta, dig_conta: dig_conta,
                num_agencia: num_agencia, dig_agencia: dig_agencia,saldo_inicial: saldo_inicial
            },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                $("#pop").css({ display: "none" });

                alert(resposta);

                $("#btn_voltar").click();

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");

            }
        });//Fim do Ajax
    });

    //Função para selecionar uma linha de registro para ser alterado
    $("#pagina a").click(function (evt) {

        evt.preventDefault();

        var id = $(this).text();
        //alert(id);

        // ajax que vai buscar os dados da categoria para alteração        
        $.ajax({
            type: "post",
            url: "ajax_cadastrar.asp",
            data: { acao: 'buscar_conta', id: id },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                //alert(resposta);

                var res_array = resposta.split("|");
                var id = res_array[1];
                var cod_banco = res_array[2];
                var descricao = res_array[3];
                var num_conta = res_array[4];
                var dig_conta = res_array[5];
                var num_agencia = res_array[6];
                var dig_agencia = res_array[7];
                var saldo_inicial = res_array[8];


                $("#pop").css({ display: "none" });
                //ajax que envia os dados da categoria para a pagina de cadastro para alteração
                $.ajax({
                    type: "POST",
                    url: "fincdh_cadastro_conta.asp",
                    /*data: {opcao : 1, id: id, descricao: descricao, tipo: tipo },
                    async: false,
                    cache: false,
                    datatype: "text",*/
                    beforeSend: function () {
                        $("#pop").css({ display: "Block" });
                    },
                    success: function (data) {

                        $("#pagina").html(data);

                        $("#pop").css({ display: "none" });

                        $("#id").val(id);
                        $("#descricao").val(descricao);
                        $("#cod_banco").val(cod_banco);
                        $("#num_conta").val(num_conta);
                        $("#dig_conta").val(dig_conta);
                        $("#num_agencia").val(num_agencia);
                        $("#dig_agencia").val(dig_agencia);
                        $("#saldo_inicial").val(saldo_inicial);

                        $("#descricao").focus();
                        $("#btn_gravar").hide();
                        $("#btn_alterar").show();



                    },
                    error: function () {
                        $("#pop").css({ display: "none" });
                        alert("Erro ao buscar previsão");
                    }
                });

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");
            }
        });
    });

    // Função para gravar nova inclusão
    $("#btn_alterar").click(function () {
        var id = $("#id").val();
        var descricao = $("#descricao").val();
        var cod_banco = $("#cod_banco option:selected").val();
        var num_conta = $("#num_conta").val();
        var dig_conta = $("#dig_conta").val();
        var num_agencia = $("#num_agencia").val();
        var dig_agencia = $("#dig_agencia").val();
        var saldo_inicial = $("#saldo_inicial").val();
        saldo_inicial = saldo_inicial.replace(/[^\d]+/g, '.');


        if (descricao == "") {
            alert("Descrição é um campo obrigatorio !");
            $("#descricao").focus();
            return false;
        }

        if (isNaN(saldo_inicial)) {
            $("#saldo_inicial").val("");
            alert("Valor só aceita numéricos");
            $("#saldo_inicial").focus();
            return false;
        }

        //Ajax que envia os dados para os asp fazer a inserção do novo registro \ó
        $.ajax({
            type: "post",
            url: "ajax_cadastrar.asp",
            data: {
                acao: 'alterar_conta', id: id, descricao: descricao, cod_banco: cod_banco, num_conta: num_conta, dig_conta: dig_conta,
                num_agencia: num_agencia, dig_agencia: dig_agencia, saldo_inicial : saldo_inicial
            },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                $("#pop").css({ display: "none" });

                alert(resposta);

                $("#btn_voltar").click();

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");

            }
        });//Fim do Ajax
    });
});


// Função js para deletar aplicada no botão [x]
function deletar(id) {
    var id = id;
    //alert(id);
    var confirma = confirm("Deseja mesmo deletar esse cadastro ??");

    if (confirma == true) {

        $.ajax({
            type: "post",
            url: "ajax_cadastrar.asp",
            data: { acao: 'deletar_conta', id: id },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                $("#pop").css({ display: "none" });

                $("#btn_voltar").click();

                alert(resposta);
            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");
            }
        });//Fim do Ajax*/
    } else {
        $("#btn_voltar").click();
    }
}