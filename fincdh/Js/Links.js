$(document).ready(function () {
    $("#pop").hide;
    
    $("#content a").click(function (evt) {
        
        evt.preventDefault();

        var href = $(this).attr("href");

        $.ajax({
            type: "POST",
            url: href,
            //data: "url="+ href,
            beforeSend: function () {
                $("#pop").css({ display: "Block" });
            },
            success: function (data) {

                $("#pagina").html(data);


                // Pegando a data atual com dt_final e dt_inicio como dia 01 do mes atual
                var d = new Date();
                var dia = d.getDate();
                var mes = d.getMonth() + 1;
                var ano = d.getFullYear();
                var dt_final = dia + "/" + mes + "/" + ano
                var dt_inicio = "01" + "/" + mes + "/" + ano

                // Acrescenta o valor na data onde pesquisas s�o realizadas por datas
                $("#dt_inicio").val(dt_inicio);
                $("#dt_final").val(dt_final);
                $("#descricao").focus();
                $("#descricao").focus();
                //Chama fun��o listar previs�es quando a tela de previs�o for chamada para usar as datas pegas acima
                $("#listar_previsao").click();
                //Chama fun��o listar receitas quando a tela de receita for chamada para usar as datas pegas acima
                $("#listar_receita").click();
                //Chama a fun��o listar despesas quando a tela de desepesa for chamada para usar as datas pegas acima
                $("#listar_despesa").click();
                //Chama a fun��o listar transferencias quando a tela de desepesa for chamada para usar as datas pegas acima
                $("#listar_transferencia").click();
                //Chama a fun��o para listar o resumo do movimento
                $("#Listar_resumo").click();
                //Deixar sempre pendentes com todos
                $("#pendentes").val(-1);


                $("#pop").css({ display: "none" });



            }

        });
    });

});

