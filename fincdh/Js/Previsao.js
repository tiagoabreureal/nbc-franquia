﻿$(document).ready(function () {
    $(":input").inputmask();

    // chama a tela de cadastro de previsão
    $("#cad_previsao").click(function () {

        //evt.preventDefault();
        //alert("entrou ?");
        var href = "fincdh_cadastro_previsao.asp";
       

        $.ajax({
            type: "POST",
            url: href,
            //data: "url="+ href,
            beforeSend: function () {
                $("#pop").css({ display: "Block" });
            },
            success: function (data) {
                
                $("#pop").css({ display: "none" });



                $("#pagina").html(data);

                var d = new Date();
                var dia = d.getDate();
                var mes = d.getMonth() + 1;
                var ano = d.getFullYear();
                var dt_final = dia + "/" + mes + "/" + ano
                var dt_inicio = "01" + "/" + mes + "/" + ano

                $("#dt_inicio").val(dt_inicio);
                $("#dt_final").val(dt_final);
                $("#dt_inicio").focus();
                $("#btn_alterar").hide();
            }

        });
    });

    //Função para listar previsões
    $("#listar_previsao").click(function () {

        var inicio = $("#dt_inicio").val();
        var fim = $("#dt_final").val();

         //alert(inicio);
        //alert(fim);

        if (inicio != '' && fim != ''){
        var data1 = parseInt(inicio.split("/")[2].toString() + inicio.split("/")[1].toString() + inicio.split("/")[0].toString());
        var data2 = parseInt(fim.split("/")[2].toString() + fim.split("/")[1].toString() + fim.split("/")[0].toString());
        
            if (data1 > data2) {
                alert("Data inicio não pode ser maior que a data final");
                $("#dt_inicio").focus();
                return false;
            }
        
        }
        if (inicio == '' && fim == '') {
            inicio = "01/01/0001";
            fim = "30/12/9999";

        }// fim validação

            var href = "fincdh_previsao.asp";

            $.ajax({
                type: "POST",
                url: href,
                data: { inicio: inicio, fim : fim },
                beforeSend: function () {
                    $("#pop").css({ display: "Block" });
                },
                success: function (data) {
                    $("#pop").css({ display: "none" });

                    $("#pagina").html(data);

                    $("#dt_inicio").focus();

                },
                error: function () {
                    $("#pop").css({ display: "none" });
                    alert("Erro");

                }

            });//Fim do ajax*/
    });

    // Função botão voltar para a tela de listagem de previsão
    $("#btn_voltar").click(function () {

        var href = "fincdh_previsao.asp";

        $.ajax({
            type: "POST",
            url: href,
            beforeSend: function () {
                $("#pop").css({ display: "Block" });
            },
            success: function (data) {
                $("#pop").css({ display: "none" });

                $("#pagina").html(data);

            }
        });
    });
    
    // Função para gravar nova inclusão
    $("#btn_gravar").click(function () {
        var inicio = $("#dt_inicio").val();
        var fim = $("#dt_final").val();
        var valor = $("#valor").val();
        var tipo = $("#tipo option:selected").val();
        var data1 = parseInt(inicio.split("/")[2].toString() + inicio.split("/")[1].toString() + inicio.split("/")[0].toString());
        var data2 = parseInt(fim.split("/")[2].toString() + fim.split("/")[1].toString() + fim.split("/")[0].toString());

        valor = valor.replace(/[^\d]+/g, '.');

        //Validações dos campos
        if (inicio == "" || fim == "") {
            alert("Campos data são obrigatorios !");
            $("#dt_inicio").focus();
            return false;
        }


        if (data1 > data2) {
            alert("Data inicio não pode ser maior que a data final");
            $("#dt_inicio").focus();
            return false;
        }

        if (valor == "") {
            alert("Campo valor é obrigatorio !");
            $("#valor").focus();
            return false;
        }

        if(isNaN(valor)) {
            $("#valor").val("");
            alert("Valor Só aceita numéricos");
            $("#valor").focus();
            return false;
        }//Fim das validações
        
            //Ajax para gravar a nova inclusão
            $.ajax({
                type: "post",
                url: "ajax_cadastrar.asp",
                data: { acao: 'cadastrar_previsao', inicio: inicio ,fim: fim, valor: valor, tipo: tipo },
                async: false,
                cache: false,
                datatype: "text",
                beforeSend: function () {

                    $("#pop").css({ display: "Block" });

                },
                success: function (resposta) {

                    alert(resposta);

                    $("#pop").css({ display: "none" });

                    $("#btn_voltar").click();

                },
                error: function () {
                    $("#pop").css({ display: "none" });
                    alert("Erro");

                }
            });//Fim do Ajax*/
    });

    //Função para selecionar uma linha de registro para ser alterado
    $("#pagina a").click(function (evt) {

        evt.preventDefault();

        var id = $(this).text();
        //alert(id);

        // ajax que vai buscar os dados da categoria para alteração        
        $.ajax({
            type: "post",
            url: "ajax_cadastrar.asp",
            data: { acao: 'buscar_previsao', id: id },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                //alert(resposta);

                var res_array = resposta.split("|");
                var id = res_array[1];
                var data_inicio = res_array[2];
                var data_fim = res_array[3];
                var valor = res_array[4].replace(',','.');
                var tipo = res_array[5]


                $("#pop").css({ display: "none" });
                //ajax que envia os dados da categoria para a pagina de cadastro para alteração
                $.ajax({
                    type: "POST",
                    url: "fincdh_cadastro_previsao.asp",
                    /*data: {opcao : 1, id: id, descricao: descricao, tipo: tipo },
                    async: false,
                    cache: false,
                    datatype: "text",*/
                    beforeSend: function () {
                        $("#pop").css({ display: "Block" });
                    },
                    success: function (data) {

                        $("#pagina").html(data);

                        $("#pop").css({ display: "none" });

                        $("#id").val(id);
                        $("#dt_inicio").val(data_inicio);
                        $("#dt_final").val(data_fim);
                        $("#valor").val(valor);
                        $("#tipo").val(tipo);

                        $("#btn_gravar").hide();
                        $("#btn_alterar").show();



                    },
                    error: function () {
                        $("#pop").css({ display: "none" });
                        alert("Erro");
                    }
                });

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro ao buscar previsão");
            }
        });
    });

    //função para gravar as alterações assim que ele clicar no botao alterar
    $("#btn_alterar").click(function () {

        var id =$("#id").val();
        var inicio = $("#dt_inicio").val();
        var fim = $("#dt_final").val();
        var valor = $("#valor").val();
        var tipo = $("#tipo").val();

        valor = valor.replace(/[^\d]+/g, '.');

        var data1 = parseInt(inicio.split("/")[2].toString() + inicio.split("/")[1].toString() + inicio.split("/")[0].toString());
        var data2 = parseInt(fim.split("/")[2].toString() + fim.split("/")[1].toString() + fim.split("/")[0].toString());

        //Validações dos campos
        if (inicio == "" || fim == "") {
            alert("Campos data obrigatorios !");
            $("#inicio").focus();
            return false;
        }


        if (data1 > data2) {
            alert("Data inicio não pode ser maior que a data final");
            $("#dt_inicio").focus();
            return false;
        }

        if (valor == "") {
            alert("Campo valor é obrigatorio !");
            $("#valor").focus();
            return false;
        }

        if (isNaN(valor)) {
            $("#valor").val("");
            alert("Valor Só aceita valores numericos");
            $("#valor").focus();
            return false;
        }//Fim das validações

        $.ajax({
            type: "post",
            url: "ajax_cadastrar.asp",
            data: { acao: 'alterar_previsao', id: id, inicio: inicio, fim: fim, valor: valor, tipo: tipo },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                $("#pop").css({ display: "none" });

                alert(resposta);

                $("#btn_voltar").click();

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");

            }
        });//Fim do Ajax
    });

});


// Função js para deletar aplicada no botão [x]
function deletar(id) {
    var id = id;
    //alert(id);
    var confirma = confirm("Deseja mesmo deletar esse cadastro ??");

    if (confirma == true) {

        $.ajax({
            type: "post",
            url: "ajax_cadastrar.asp",
            data: { acao: 'deletar_previsao', id: id },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                $("#pop").css({ display: "none" });

                $("#btn_voltar").click();

                alert(resposta);
            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");
            }
        });//Fim do Ajax*/
    } else {
        $("#btn_voltar").click();
    }
}