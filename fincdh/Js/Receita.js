﻿$(document).ready(function () {
    $(":input").inputmask();

    $("#cad_receita").click(function () {

        //evt.preventDefault();
        //alert("entrou ?");
        var href = "fincdh_cadastro_receita.asp";

        $.ajax({
            type: "POST",
            url: href,
            //data: "url="+ href,
            beforeSend: function () {
                $("#pop").css({ display: "Block" });
            },
            success: function (data) {
                $("#pop").css({ display: "none" });

                $("#pagina").html(data);
                $("#btn_alterar").hide();
                // Adicionar data atual
                var d = new Date();
                var dia = d.getDate();
                var mes = d.getMonth() + 1;
                var ano = d.getFullYear();
                var dt = dia + "/" + mes + "/" + ano
                $("#data").val(dt);
                $("#data").focus();

            }

        });
    });

    $("#btn_voltar").click(function () {

        var href = "fincdh_receita.asp";

        $.ajax({
            type: "POST",
            url: href,
            beforeSend: function () {
                $("#pop").css({ display: "Block" });
            },
            success: function (data) {
                $("#pop").css({ display: "none" });

                $("#pagina").html(data);
                
                // Pegando a data atual com dt_final e dt_inicio como dia 01 do mes atual
                var d = new Date();
                var dia = d.getDate();
                var mes = d.getMonth() + 1;
                var ano = d.getFullYear();
                var dt_final = dia + "/" + mes + "/" + ano
                var dt_inicio = "01" + "/" + mes + "/" + ano

                // Acrescenta o valor na data onde pesquisas são realizadas por datas
                $("#dt_inicio").val(dt_inicio);
                $("#dt_final").val(dt_final);
                //Chama função listar receitas quando a tela de previsão for chamada para usar as datas pegas acima
                $("#listar_receita").click();

            }
        });
    });
    
    $("#btn_gravar").click(function () {
        var data = $("#data").val();
        var descricao = $("#descricao").val();
        var valor = $("#valor").val();
        var categoria = $("#categoria option:selected").val();
        var num_doc = $("#num_doc").val();
        var conta_destino = $("#conta_destino option:selected").val();

        valor = valor.replace(/[^\d]+/g, '.');

        // Checando se o check box foi selecionado ou não
        if ($("#recebido").prop('checked')){
            var recebido = 1;
        } else{
            var recebido = 0;
        }

        //Validações
        if (data == "") {
            alert("Campo data é obrigatorio");
            $("#data").focus();
            return false;
        }
        if (descricao == "") {
            alert("Campo descrição é obrigatorio");
            $("#descricao").focus();
            return false;
        }
        if (valor == "") {
            alert("Campo valor é obrigatorio");
            $("#valor").focus();
            return false;
        }

        if (isNaN(valor)) {
            $("#valor").val("");
            alert("Valor só aceita numéricos");
            $("#valor").focus();
            return false;
        }//Fim das validações

        
        //Ajax para gravar a nova inclusão
        $.ajax({
            type: "post",
            url: "ajax_movimento.asp",
            data: {
                acao: 'cadastrar_receita', data: data, descricao: descricao, valor: valor, categoria: categoria,
                num_doc: num_doc, conta_destino: conta_destino, recebido: recebido
            },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                alert(resposta);

                $("#pop").css({ display: "none" });

                $("#btn_voltar").click();

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");

            }
        });//Fim do Ajax*/

    });

    //Função para listar previsões
    $("#listar_receita").click(function () {

        var inicio = $("#dt_inicio").val();
        var fim = $("#dt_final").val();
        var pendente = $("#pendentes option:selected").val();


        //alert(inicio);
        //alert(fim);

        if (inicio != '' && fim != '') {
            var data1 = parseInt(inicio.split("/")[2].toString() + inicio.split("/")[1].toString() + inicio.split("/")[0].toString());
            var data2 = parseInt(fim.split("/")[2].toString() + fim.split("/")[1].toString() + fim.split("/")[0].toString());


            if (data1 > data2) {
                alert("Data inicio não pode ser maior que a data final");
                
                return false;
            }

        }
        if (inicio == '' && fim == '') {
            inicio = "01/01/0001";
            fim = "31/12/9999";

        }// fim validação

        var href = "fincdh_receita.asp";

        $.ajax({
            type: "POST",
            url: href,
            data: { inicio: inicio, fim: fim, pendente: pendente },
            beforeSend: function () {
                $("#pop").css({ display: "Block" });
            },
            success: function (data) {
                $("#pop").css({ display: "none" });

                $("#pagina").html(data);

                var d = new Date();
                var dia = d.getDate();
                var mes = d.getMonth() + 1;
                var ano = d.getFullYear();
                var dt_final = dia + "/" + mes + "/" + ano
                var dt_inicio = "01" + "/" + mes + "/" + ano

                $("#dt_inicio").val(dt_inicio);
                $("#dt_final").val(dt_final);
                $("#dt_inicio").focus();
                $("#pendentes").val(pendente);


            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");

            }

        });//Fim do ajax*/
    });

    //Função para selecionar uma linha de registro para ser alterado
    $("#pagina a").click(function (evt) {

        evt.preventDefault();

        var id = $(this).text();
        //alert(id);

        // ajax que vai buscar os dados da categoria para alteração        
        $.ajax({
            type: "post",
            url: "ajax_movimento.asp",
            data: { acao: 'buscar_receita', id: id },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                //alert(resposta);
                var res_array = resposta.split("|");
                var id = res_array[1];
                var data_receita = res_array[2];
                var descricao = res_array[3];
                var conta_destino = res_array[4];
                var categoria = res_array[5];
                var valor = res_array[6].replace('.', '').replace(',', '.');
                var recebidoSN = res_array[7];
                var num_doc = res_array[8];


                $("#pop").css({ display: "none" });
                //ajax que envia os dados da categoria para a pagina de cadastro para alteração
                $.ajax({
                    type: "POST",
                    url: "fincdh_cadastro_receita.asp",
                    /*data: {opcao : 1, id: id, descricao: descricao, tipo: tipo },
                    async: false,
                    cache: false,
                    datatype: "text",*/
                    beforeSend: function () {
                        $("#pop").css({ display: "Block" });
                    },
                    success: function (data) {

                        $("#pagina").html(data);

                        $("#pop").css({ display: "none" });

                        $("#id").val(id);
                        $("#data").val(data_receita);
                        $("#descricao").val(descricao);
                        $("#valor").val(valor);
                        $("#categoria").val(categoria);
                        $("#conta_destino").val(conta_destino);
                        if (recebidoSN == 1){
                            $("#recebido").prop('checked', true);
                        } else{
                            $("#recebido").prop('checked', false);
                        }
                        $("#num_doc").val(num_doc);

                        $("#btn_gravar").hide();
                        $("#btn_alterar").show();



                    },
                    error: function () {
                        $("#pop").css({ display: "none" });
                        alert("Erro");
                    }
                });

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");
            }
        });
    });


    //função para gravar as alterações assim que ele clicar no botao alterar
    $("#btn_alterar").click(function () {
        var data = $("#data").val();
        var descricao = $("#descricao").val();
        var valor = $("#valor").val();
        var categoria = $("#categoria option:selected").val();
        var num_doc = $("#num_doc").val();
        var conta_destino = $("#conta_destino option:selected").val();
        var id = $("#id").val();

        valor = valor.replace(/[^\d]+/g, '.');

        // Checando se o check box foi selecionado ou não
        if ($("#recebido").prop('checked')) {
            var recebido = 1;
        } else {
            var recebido = 0;
        }

        //Validações
        if (data == "") {
            alert("Campo data é obrigatorio");
            $("#data").focus();
            return false;
        }
        if (descricao == "") {
            alert("Campo descrição é obrigatorio");
            $("#descricao").focus();
            return false;
        }
        if (valor == "") {
            alert("Campo valor é obrigatorio");
            $("#valor").focus();
            return false;
        }

        if (isNaN(valor)) {
            $("#valor").val("");
            alert("Valor só aceita numéricos");
            $("#valor").focus();
            return false;
        }//Fim das validações
       

        $.ajax({
            type: "post",
            url: "ajax_movimento.asp",
            data: {
                acao: 'alterar_receita', data: data, descricao: descricao, valor: valor, categoria: categoria,
                num_doc: num_doc, conta_destino: conta_destino, recebido: recebido, id: id
            },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                alert(resposta);

                $("#pop").css({ display: "none" });

                $("#btn_voltar").click();

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");

            }
        });//Fim do Ajax
    });

    //Alterar listagem de acordo com o que o usuario escolheu
    $("#pendentes").change(function () {
        $("#listar_receita").click();
    });

});//fim do DOM

// Função js para deletar aplicada no botão [x]
function deletar(id) {
    var id = id;
    //alert(id);
    var confirma = confirm("Deseja mesmo deletar esse cadastro ??");

    if (confirma == true) {

        $.ajax({
            type: "post",
            url: "ajax_movimento.asp",
            data: { acao: 'deletar_receita', id: id },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                $("#pop").css({ display: "none" });

                $("#btn_voltar").click();

                alert(resposta);
            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");
            }
        });//Fim do Ajax*/
    } else {
        $("#btn_voltar").click();
    }
}

// Função js para mudar status do recebimento de uma receita
function receber(id) {
    var id = id;
    //alert(id);
    var confirma = confirm("Deseja mesmo alterar o status  desse cadastro ??");

    if ($("#" + id).prop('checked')) {
        var recebido = 1;
    } else {
        var recebido = 0;
    }

    //alert(recebido);
    if (confirma == true) {


        //alert(recebido);

       $.ajax({
            type: "post",
            url: "ajax_movimento.asp",
            data: { acao: 'aletrar_recebimento_receita', id: id, recebido: recebido },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                $("#pop").css({ display: "none" });

                $("#btn_voltar").click();

                alert(resposta);
            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");
            }
        });//Fim do Ajax*/
      } else {
        $("#btn_voltar").click();
    }
}

function tecla() {
    var atalho;
    atalho = event.keyCode;
    //alert(atalho);
    if (atalho == 113) $("#listar_receita").click();;
    
    if (atalho == 115) $("#cad_receita").click();
    if (atalho == 118) $("#btn_voltar").click();
    if (atalho == 117) $("#btn_gravar").click();
    if (atalho == 119) $("#btn_alterar").click();
}