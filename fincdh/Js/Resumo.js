﻿$(document).ready(function () {
    $(":input").inputmask();

    //Função para listar despesas
    $("#Listar_resumo").click(function () {

        var inicio = $("#dt_inicio").val();
        var fim = $("#dt_final").val();
        var pendente = $("#pendentes option:selected").val();

        //alert(pendente);
        //alert(fim);

        if (inicio != '' && fim != '') {
            var data1 = parseInt(inicio.split("/")[2].toString() + inicio.split("/")[1].toString() + inicio.split("/")[0].toString());
            var data2 = parseInt(fim.split("/")[2].toString() + fim.split("/")[1].toString() + fim.split("/")[0].toString());

            if (data1 > data2) {
                alert("Data inicio não pode ser maior que a data final");
                $("#dt_inicio").focus();
                return false;
            }

        }
        if (inicio == '' && fim == '') {
            inicio = "01/01/0001";
            fim = "31/12/9999";

        }// fim validação

        var href = "fincdh_resumo.asp";

        $.ajax({
            type: "POST",
            url: href,
            data: { inicio: inicio, fim: fim, pendente: pendente },
            beforeSend: function () {
                $("#pop").css({ display: "Block" });
            },
            success: function (data) {
                $("#pop").css({ display: "none" });

                $("#pagina").html(data);

                var d = new Date();
                var dia = d.getDate();
                var mes = d.getMonth() + 1;
                var ano = d.getFullYear();
                var dt_final = dia + "/" + mes + "/" + ano
                var dt_inicio = "01" + "/" + mes + "/" + ano

                $("#dt_inicio").val(dt_inicio);
                $("#dt_final").val(dt_final);
                $("#pendentes").val(pendente);
                $("#dt_inicio").focus();
            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");

            }

        });//Fim do ajax*/
    });

    //Alterar listagem de acordo com o que o usuario escolheu
    $("#pendentes").change(function () {
        $("#Listar_resumo").click();
    });
});


// Função js para mudar status do recebimento de uma receita
function receber(id) {
    var id = id;
    //alert(id);
    var confirma = confirm("Deseja mesmo alterar o status  desse cadastro ??");

    if ($("#" + id).prop('checked')) {
        var recebido = 1;
    } else {
        var recebido = 0;
    }

    //alert(recebido);
    if (confirma == true) {


        //alert(recebido);

        $.ajax({
            type: "post",
            url: "ajax_movimento.asp",
            data: { acao: 'aletrar_recebimento_receita', id: id, recebido: recebido },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                $("#pop").css({ display: "none" });

                // $("#btn_voltar").click();

                alert(resposta);
                $("#Listar_resumo").click();
            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");
            }
        });//Fim do Ajax*/
    } else {
        $("#Listar_resumo").click();
    }
}
