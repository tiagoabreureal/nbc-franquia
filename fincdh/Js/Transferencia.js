﻿$(document).ready(function () {
    $(":input").inputmask();

    //Função para chamar a tela de cadastro
    $("#cad_transferencia").click(function () {

        //evt.preventDefault();
        //alert("entrou ?");
        var href = "fincdh_cadastro_transferencia.asp";

        $.ajax({
            type: "POST",
            url: href,
            //data: "url="+ href,
            beforeSend: function () {
                $("#pop").css({ display: "Block" });
            },
            success: function (data) {
                $("#pop").css({ display: "none" });

                $("#pagina").html(data);
                $("#btn_alterar").hide();
                // Adicionar data atual
                var d = new Date();
                var dia = d.getDate();
                var mes = d.getMonth() + 1;
                var ano = d.getFullYear();
                var dt = dia + "/" + mes + "/" + ano
                $("#data").val(dt);
                $("#data").focus();
            }

        });

    });


    $("#btn_voltar").click(function () {

        var href = "fincdh_transferencia.asp";

        $.ajax({
            type: "POST",
            url: href,
            beforeSend: function () {
                $("#pop").css({ display: "Block" });
            },
            success: function (data) {
                $("#pop").css({ display: "none" });

                $("#pagina").html(data);


                // Pegando a data atual com dt_final e dt_inicio como dia 01 do mes atual
                var d = new Date();
                var dia = d.getDate();
                var mes = d.getMonth() + 1;
                var ano = d.getFullYear();
                var dt_final = dia + "/" + mes + "/" + ano
                var dt_inicio = "01" + "/" + mes + "/" + ano

                // Acrescenta o valor na data onde pesquisas são realizadas por datas
                $("#dt_inicio").val(dt_inicio);
                $("#dt_final").val(dt_final);
                //Chama função listar receitas quando a tela de previsão for chamada para usar as datas pegas acima
                $("#listar_transferencia").click();

            }
        });

    });


    //Função para gravar nova inclusão
    $("#btn_gravar").click(function () {
        var data = $("#data").val();
        var descricao = $("#descricao").val();
        var valor = $("#valor").val();
        var categoria = $("#categoria option:selected").val();
        var num_doc = $("#num_doc").val();
        var conta_destino = $("#conta_destino option:selected").val();
        var conta_origem = $("#conta_origem option:selected").val();
        var recebido = 1;

        valor = valor.replace(/[^\d]+/g, '.');

        //Validações
        if (data == "") {
            alert("Campo data é obrigatorio");
            $("#data").focus();
            return false;
        }
        if (descricao == "") {
            alert("Campo descrição é obrigatorio");
            $("#descricao").focus();
            return false;
        }
        if (valor == "") {
            alert("Campo valor é obrigatorio");
            $("#valor").focus();
            return false;
        }

        if (isNaN(valor)) {
            $("#valor").val("");
            alert("Valor só aceita numéricos");
            $("#valor").focus();
            return false;
        }

        if (conta_destino == conta_origem) {
            alert("Conta de origem e destino não podem ser iguais");
            return false;
        }//Fim das validações


        //Ajax para gravar a nova inclusão
        $.ajax({
            type: "post",
            url: "ajax_movimento.asp",
            data: {
                acao: 'cadastrar_transferencia', data: data, descricao: descricao, valor: valor, categoria: categoria,
                num_doc: num_doc, conta_destino: conta_destino, conta_origem: conta_origem, recebido: recebido
            },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                alert(resposta);

                $("#pop").css({ display: "none" });

                $("#btn_voltar").click();

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");

            }
        });//Fim do Ajax*/

    });

    //Função para listar transfrencias
    $("#listar_transferencia").click(function () {

        var inicio = $("#dt_inicio").val();
        var fim = $("#dt_final").val();

        //alert(inicio);
        //alert(fim);

        if (inicio != '' && fim != '') {
            var data1 = parseInt(inicio.split("/")[2].toString() + inicio.split("/")[1].toString() + inicio.split("/")[0].toString());
            var data2 = parseInt(fim.split("/")[2].toString() + fim.split("/")[1].toString() + fim.split("/")[0].toString());

            if (data1 > data2) {
                alert("Data inicio não pode ser maior que a data final");
                $("#dt_inicio").focus();
                return false;
            }

        }
        if (inicio == '' && fim == '') {
            inicio = "01/01/0001";
            fim = "31/12/9999";

        }// fim validação

        var href = "fincdh_transferencia.asp";

        $.ajax({
            type: "POST",
            url: href,
            data: { inicio: inicio, fim: fim },
            beforeSend: function () {
                $("#pop").css({ display: "Block" });
            },
            success: function (data) {
                $("#pop").css({ display: "none" });

                $("#pagina").html(data);

                var d = new Date();
                var dia = d.getDate();
                var mes = d.getMonth() + 1;
                var ano = d.getFullYear();
                var dt_final = dia + "/" + mes + "/" + ano
                var dt_inicio = "01" + "/" + mes + "/" + ano

                $("#dt_inicio").val(dt_inicio);
                $("#dt_final").val(dt_final);
                $("#dt_inicio").focus();

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");

            }

        });//Fim do ajax*/
    });

    //Função para selecionar uma linha de registro para ser alterado
    $("#pagina a").click(function (evt) {

        evt.preventDefault();

        var id = $(this).text();
        //alert(id);

        // ajax que vai buscar os dados da categoria para alteração        
        $.ajax({
            type: "post",
            url: "ajax_movimento.asp",
            data: { acao: 'buscar_transferencia', id: id },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                //alert(resposta);
                var res_array = resposta.split("|");
                var id = res_array[1];
                var data_transferencia = res_array[2];
                var descricao = res_array[3];
                var conta_origem = res_array[4];
                var conta_destino = res_array[5];
                var categoria = res_array[6];
                var valor = res_array[7].replace('.', '').replace(',', '.');
                var num_doc = res_array[8];


                $("#pop").css({ display: "none" });
                //ajax que envia os dados da categoria para a pagina de cadastro para alteração
                $.ajax({
                    type: "POST",
                    url: "fincdh_cadastro_transferencia.asp",
                    /*data: {opcao : 1, id: id, descricao: descricao, tipo: tipo },
                    async: false,
                    cache: false,
                    datatype: "text",*/
                    beforeSend: function () {
                        $("#pop").css({ display: "Block" });
                    },
                    success: function (data) {

                        $("#pagina").html(data);

                        $("#pop").css({ display: "none" });

                        $("#id").val(id);
                        $("#data").val(data_transferencia);
                        $("#descricao").val(descricao);
                        $("#valor").val(valor);
                        $("#categoria").val(categoria);
                        $("#conta_origem").val(conta_origem);
                        $("#conta_destino").val(conta_destino);
                        $("#num_doc").val(num_doc);

                        $("#btn_gravar").hide();
                        $("#btn_alterar").show();



                    },
                    error: function () {
                        $("#pop").css({ display: "none" });
                        alert("Erro");
                    }
                });

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");
            }
        });
    });

    //Função para alterar os registros de receita
    $("#btn_alterar").click(function () {
        var data = $("#data").val();
        var descricao = $("#descricao").val();
        var valor = $("#valor").val();
        var categoria = $("#categoria option:selected").val();
        var num_doc = $("#num_doc").val();
        var conta_destino = $("#conta_destino option:selected").val();
        var conta_origem = $("#conta_origem option:selected").val();
        var recebido = 1;
        var id = $("#id").val();

        valor = valor.replace(/[^\d]+/g, '.');

        //Validações
        if (data == "") {
            alert("Campo data é obrigatorio");
            $("#data").focus();
            return false;
        }
        if (descricao == "") {
            alert("Campo descrição é obrigatorio");
            $("#descricao").focus();
            return false;
        }
        if (valor == "") {
            alert("Campo valor é obrigatorio");
            $("#valor").focus();
            return false;
        }

        if (isNaN(valor)) {
            $("#valor").val("");
            alert("Valor só aceita numéricos");
            $("#valor").focus();
            return false;
        }

        if (conta_destino == conta_origem) {
            alert("Conta de origem e destino não podem ser iguais");
            return false;
        }//Fim das validações


        //Ajax para alterar 
        $.ajax({
            type: "post",
            url: "ajax_movimento.asp",
            data: {
                acao: 'alterar_transferencia', data: data, descricao: descricao, valor: valor, categoria: categoria,
                num_doc: num_doc, conta_destino: conta_destino, conta_origem: conta_origem, recebido: recebido, id: id
            },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                alert(resposta);

                $("#pop").css({ display: "none" });

                $("#btn_voltar").click();

            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");

            }
        });//Fim do Ajax*/

    });

});


// Função js para deletar aplicada no botão [x]
function deletar(id) {
    var id = id;
    //alert(id);
    var confirma = confirm("Deseja mesmo deletar esse cadastro ??");

    if (confirma == true) {

        $.ajax({
            type: "post",
            url: "ajax_movimento.asp",
            data: { acao: 'deletar_transferencia', id: id },
            async: false,
            cache: false,
            datatype: "text",
            beforeSend: function () {

                $("#pop").css({ display: "Block" });

            },
            success: function (resposta) {

                $("#pop").css({ display: "none" });

                $("#listar_transferencia").click();

                alert(resposta);
            },
            error: function () {
                $("#pop").css({ display: "none" });
                alert("Erro");
            }
        });//Fim do Ajax*/
    } else {
        $("#listar_transferencia").click();
    }
}