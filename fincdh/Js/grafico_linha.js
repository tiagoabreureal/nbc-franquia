$(document).ready(function () {
    //para abrir o relatorio ao abrir a pagina
    grafico();
    $(":input").inputmask();
    // Pegando a data atual com dt_final e dt_inicio como dia 01 do mes atual
    var d = new Date();
    var dia = d.getDate();
    var mes = d.getMonth() + 1;
    var ano = d.getFullYear();
    var dt_final = dia + "/" + mes + "/" + ano
    var dt_inicio = "01" + "/" + mes + "/" + ano
    var serie = '';
    // Acrescenta o valor na data onde pesquisas são realizadas por datas
    $("#dt_inicio").val(dt_inicio);
    $("#dt_final").val(dt_final);

    //Função para chamar o charts
    function grafico() {
        $(function () {
            var inicio = $("#dt_inicio").val();
            var fim = $("#dt_final").val();

            // Get the CSV and create the chart
            $.getJSON('https://cdh.hinode.com.br/fincdh/json.asp', { acao: 'gerar_linha', inicio: inicio, fim: fim }, function (csv) {
                var str = JSON2CSV(csv);
                //alert(inicio);
                //alert(fim);

                // Converte o Objeto que retornou no JSON para um arquivo CSV
                function JSON2CSV(objArray) {
                    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
                    var str = '';
                    var line = '';


                    for (var i = 0; i < array.length; i++) {
                        var line = '';
                        for (var index in array[i]) {
                            if (i == 0) {
                                serie += index + ";"
                            }
                            line += array[i][index] + ';';

                        }
                        line = line.slice(0, -1);
                        str += line + '\r\n';
                    }
                    str = serie.slice(0, -1) + '\r\n' + str;
                    return str;
                }

                //Colocando o conteudo do highcharts em uma div
                var chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'container',
                        borderColor: '#000000',
                        borderWidth: 2,
                        type: 'spline',
                        width: 1400,
                        zoomType: 'x'
                    },
                    // Dados que vem do banco nesse caso em CSV
                    data: {
                        csv: str
                    },
                    title: {
                        text: 'Relatório Grafico de Movimentos e Saldo'
                    },

                    subtitle: {
                        text: 'Modulo de Gestão Financeira'
                    },
                    colors: ['#ec2a2a', '#57ec00', '#0027ec', '#0d0000'],
                    xAxis: {
                        tickInterval: 30 * 24 * 3600 * 1000, // one week
                        tickWidth: 0,
                        gridLineWidth: 1,
                        labels: {
                            align: 'center',
                            x: 5,
                            y: 10
                        }
                    },

                    yAxis: [{ // left y axis
                        offset: 45,
                        title: {
                            text: null
                        },
                        labels: {
                            align: 'left',
                            x: 3,
                            y: 16,
                            format: '{value:.,0.2f}'

                        },
                        showFirstLabel: false
                    }, { // right y axis

                        linkedTo: 0,
                        gridLineWidth: 0,
                        opposite: true,
                        title: {
                            text: null
                        },
                        labels: {
                            align: 'right',
                            x: -3,
                            y: 16,
                            format: '{value:.,0.2f}'
                        },
                        showFirstLabel: false
                    }],

                    legend: {
                        align: 'left',
                        verticalAlign: 'top',
                        y: 20,
                        floating: true,
                        borderWidth: 0
                    },

                    tooltip: {
                        shared: true,
                        crosshairs: true
                    },

                    plotOptions: {
                        series: {
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function (e) {
                                        hs.htmlExpand(null, {
                                            pageOrigin: {
                                                x: e.pageX,
                                                y: e.pageY
                                            },
                                            headingText: this.series.name,
                                            maincontentText: Highcharts.dateFormat('%A, %e %b, %Y', this.x) + ':<br/> ' +
                                               'R$' + Highcharts.numberFormat(this.y),
                                            width: 200
                                        });
                                    }
                                }
                            },
                            marker: {
                                lineWidth: 1
                            }
                        }
                    },

                    series: [],
                    exporting: {
                        sourceWidth: 1700,
                        sourceHeight: 600
                    },

                });
            });


            // Opção dos pontos decimais
            Highcharts.setOptions({
                lang: {
                    decimalPoint: ',',
                    thousandsSep: '.'
                },

                tooltip: {
                    yDecimals: 2 // If you want to add 2 decimals
                }
            });
                
        });
    }
    // Função para chamar os filtros
    $("#Acessar").click(function () {
        grafico();
    })
});