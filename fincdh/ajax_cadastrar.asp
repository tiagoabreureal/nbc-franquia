﻿<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<!--#include virtual="public/pub_Geral1.asp"-->
<%
emitente = session("emitente")
descricao = request("descricao")
tipo = request("tipo")
acao = request("acao")
id = request("id")
inicio = request("inicio")
fim = request("fim")
valor = request("valor")
cod_banco = request ("cod_banco")
num_conta = request("num_conta")
dig_conta = request("dig_conta")
num_agencia = request("num_agencia")
dig_agencia = request("dig_agencia")
saldo_inicial =  request("saldo_inicial")

'Case para selecionar qual ação o ajax ira executara
Select Case acao

    Case "cadastrar_categoria"
        Vretorno = fc_cadastrar_categoria(emitente,descricao,tipo)
        response.Write(Vretorno)
    Case "cadastrar_previsao"
        Vretorno = fc_cadastrar_previsao(emitente,inicio,fim,valor)
        response.Write(Vretorno)
    Case "cadastrar_conta"
        Vretorno = fc_cadastrar_conta(emitente,descricao,cod_banco,num_conta,dig_conta,num_agencia,dig_agencia,saldo_inicial)
        response.Write(Vretorno)
    Case "deletar_categoria"
        Vretorno = fc_deletar_categoria(id,emitente)
        response.Write(Vretorno)
    Case "deletar_previsao"
        Vretorno = fc_deletar_previsao(id,emitente)
        response.Write(Vretorno)
    Case "deletar_conta"
        Vretorno = fc_deletar_conta(id,emitente)
        response.Write(Vretorno)
    Case "buscar_categoria"
        Vretorno = fc_buscar_categoria(id,emitente)
        response.Write(Vretorno)
    Case "buscar_previsao"
        Vretorno = fc_buscar_previsao(id,emitente)
        response.Write(Vretorno)
    Case "buscar_conta"
        Vretorno = fc_buscar_conta(id,emitente)
        response.Write(Vretorno)
    Case "alterar_categoria"
        Vretorno = fc_alterar_categoria(id,emitente,descricao,tipo)
        response.Write(Vretorno)
    Case "alterar_previsao"
        Vretorno = fc_alterar_previsao(id,emitente,inicio,fim,valor,tipo)
        response.Write(Vretorno)
    Case "alterar_conta"
        Vretorno = fc_alterar_conta(id,emitente,descricao,cod_banco,num_conta,dig_conta,num_agencia,dig_agencia,saldo_inicial)
        response.Write(Vretorno)
        
End Select

'Função para cadastrar a categoria
Function fc_cadastrar_categoria(emitente,descricao,tipo)

    strsql= "exec fincdh_cadastro_categoria_sp 'I','"& emitente &"','"& descricao &"','"& tipo &"',''"	
    set adors1 = conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function

'Função para cadastrar a previsão
Function fc_cadastrar_previsao(emitente,inicio,fim,valor)

    strsql= "exec fincdh_cadastro_previsao_sp 'I','"& emitente &"','"& dt_fim(inicio) &"','"& dt_fim(fim) &"',"& valor &",'"& tipo &"',''"	
    set adors1 = conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function
  
'Função para cadastrar a conta
Function fc_cadastrar_conta(emitente,descricao,cod_banco,num_conta,dig_conta,num_agencia,dig_agencia,saldo_inicial)

    strsql= "exec fincdh_cadastro_conta_sp 'I','"& emitente &"','"& cod_banco &"','"& descricao &"','"& num_conta &"','"& dig_conta &"','"& num_agencia &"','"& dig_agencia &"','',"&saldo_inicial&""	
    set adors1 = conn1.execute(strsql)

    'resposta = strsql
    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function


'Função para deletar a categoria    
Function fc_deletar_categoria(id,emitente)

    strsql= "exec fincdh_cadastro_categoria_sp 'E','"& emitente &"','','',"& id &""	
    set adors1 = conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function
    
    'Função para deletar a previsão    
Function fc_deletar_previsao(id,emitente)

    strsql= "exec fincdh_cadastro_previsao_sp 'E','"& emitente &"','','','0.00','',"& id &""	
    set adors1 = conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function
    
  'Função para deletar a conta    
Function fc_deletar_conta(id,emitente)

    strsql= "exec fincdh_cadastro_conta_sp 'E','"& emitente &"','','','','','','','"& id &"',0.00"	
    set adors1 = conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function      
  
    
'Função para buscar a categoria para alteração       
Function fc_buscar_categoria(id,emitente)

    strsql= "exec fincdh_lista_categoria_sp '"& emitente &"','',"& id &""	
    set adors1 = conn1.execute(strsql)
    
    resposta = "|" & adors1("id_categoria") & "|" & adors1("descricao") & "|" & adors1("tiponum") &""
    response.Write resposta
    
Exit Function
End Function
    
'Função para buscar a categoria para alteração       
Function fc_buscar_previsao(id,emitente)

    strsql= "exec fincdh_lista_previsao2_sp '"& emitente &"',"& id &""	
    set adors1 = conn1.execute(strsql)
    
    'resposta = strsql
    resposta = "|" & adors1("id_previsao") & "|" & adors1("data_inicio") & "|"  & adors1("data_fim") &  "|" & adors1("valor") &  "|" & adors1("tiponum") &""
    response.Write resposta
    
Exit Function
End Function
    
'Função para buscar a categoria para alteração       
Function fc_buscar_conta(id,emitente)

    strsql= "exec fincdh_lista_conta_sp '"& emitente &"','',"& id &""	
    'response.Write strSql
    set adors1 = conn1.execute(strsql)
    
    resposta = "|" & adors1("id_conta") & "|" & adors1("codigo_banco") & "|"  & adors1("nome") &  "|" & adors1("conta") &  "|" & adors1("conta_digito") & "|" & adors1("agencia") & "|" & adors1("agencia_digito") &"|"&adors1("saldo_inicial")& ""
    response.Write resposta
    
Exit Function
End Function    

'Função para alterar a categoria    
Function fc_alterar_categoria(id,emitente,descricao,tipo)

    strsql= "exec fincdh_cadastro_categoria_sp 'A','"& emitente &"','"& descricao &"',"& tipo &","& id &" "
    set adors1 = conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta

Exit Function
End Function
    
'Função para alterar a previsão    
Function fc_alterar_previsao(id,emitente,inicio,fim,valor,tipo)

    strsql= "exec fincdh_cadastro_previsao_sp 'A','"& emitente &"','"& dt_fim(inicio) &"','"& dt_fim(fim) &"',"& valor &","& tipo &","& id &" "
    set adors1 = conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta

Exit Function
End Function  

'Função para alterar a previsão    
Function fc_alterar_conta(id,emitente,descricao,cod_banco,num_conta,dig_conta,num_agencia,dig_agencia,saldo_inicial)

    strsql= "exec fincdh_cadastro_conta_sp 'A','"& emitente &"','"& cod_banco &"','"& descricao &"','"& num_conta &"','"& dig_conta &"','"& num_agencia &"','"& dig_agencia &"','"& id &"',"&saldo_inicial&""	
    set adors1 = conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta

Exit Function
End Function  
%>