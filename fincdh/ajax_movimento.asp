﻿<!--#include virtual="public/pub_Componentes.asp"-->
<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<!--#include virtual="public/pub_Geral1.asp"-->

<%
emitente = session("emitente")
usuario = session("usuario")
acao = request("acao")
data = request("data")
descricao = request("descricao")
num_doc = request("num_doc")
valor = request("valor")
conta_destino = request("conta_destino")
conta_origem = request("conta_origem")
categoria = request("categoria")
pagoSn = request("recebido")
repeteSn = request("repeteSn")
emitir_recibo = request("emitir_recibo")
id = request("id")



'Case para selecionar qual ação o ajax ira executara
Select Case acao

    Case "cadastrar_receita"
        Vretorno = fc_cadastrar_receita(emitente,data,descricao,valor ,categoria ,num_doc ,conta_destino ,usuario , pagoSn,emitir_recibo)
        response.Write(Vretorno)
    Case "cadastrar_despesa"
        Vretorno = fc_cadastrar_despesa(emitente,data,descricao,valor ,categoria ,num_doc ,conta_origem ,usuario , pagoSn,emitir_recibo)
        response.Write(Vretorno)
    Case "cadastrar_transferencia"
        Vretorno = fc_cadastrar_transferencia(emitente,data,descricao,valor ,categoria ,num_doc ,conta_origem, conta_destino ,usuario,emitir_recibo)
        response.Write(Vretorno)
    Case "buscar_receita"
        Vretorno = fc_buscar_receita(id,emitente)
        response.Write(Vretorno)
    Case "buscar_despesa"
        Vretorno = fc_buscar_despesa(id,emitente)
        response.Write(Vretorno)
    Case "buscar_transferencia"
        Vretorno = fc_buscar_transferencia(id,emitente)
        response.Write(Vretorno)
    Case "alterar_receita"
        Vretorno = fc_alterar_receita(emitente,data,descricao,valor ,categoria ,num_doc ,conta_destino ,usuario , pagoSn,emitir_recibo,id)
        response.Write(Vretorno)
    Case "alterar_despesa"
        Vretorno = fc_alterar_despesa(emitente,data,descricao,valor ,categoria ,num_doc ,conta_origem ,usuario , pagoSn,emitir_recibo,id)
        response.Write(Vretorno)
    Case "alterar_transferencia"
        Vretorno = fc_alterar_transferencia(emitente,data,descricao,valor ,categoria ,num_doc ,conta_origem, conta_destino ,usuario,emitir_recibo,id)
        response.Write(Vretorno)
    Case "aletrar_recebimento_receita"
        Vretorno = fc_aletrar_recebimento_receita(emitente,usuario,pagoSn,id)
        response.Write(Vretorno)
    Case "deletar_receita"
        Vretorno = fc_deletar_receita(emitente,id)
        response.Write(Vretorno)
    Case "deletar_despesa"
        Vretorno = fc_deletar_despesa(emitente,id)
        response.Write(Vretorno)
    Case "deletar_transferencia"
        Vretorno = fc_deletar_transferencia(emitente,id)
        response.Write(Vretorno)

End Select

'Função para cadastrar a receita
Function fc_cadastrar_receita(emitente,data,descricao,valor ,categoria ,num_doc ,conta_destino ,usuario , pagoSn,emitir_recibo)


    strSql= "exec fincdh_cadastro_movimento_sp 'I','R','"& emitente &"','"& dt_fim(data) &"','"& descricao &"','"& num_doc &"','"& valor &"','"& usuario &"',"&_
    "'"& conta_destino &"','"& conta_origem &"','"& categoria &"',"& pagoSn &",0,''"
    set adors1 = conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function

'Função para cadastrar a despesa
Function fc_cadastrar_despesa(emitente,data,descricao,valor ,categoria ,num_doc ,conta_origem ,usuario , pagoSn,emitir_recibo)


    strSql= "exec fincdh_cadastro_movimento_sp 'I','D','"& emitente &"','"& dt_fim(data) &"','"& descricao &"','"& num_doc &"','"& valor &"','"& usuario &"',"&_
    "'"& conta_destino &"','"& conta_origem &"','"& categoria &"',"& pagoSn &",0,''"
    set adors1 = conn1.execute(strsql)



    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function

'Função para cadastrar os movimentos de transferencia (que são 2)
Function fc_cadastrar_transferencia(emitente,data,descricao,valor ,categoria ,num_doc ,conta_origem, conta_destino ,usuario,emitir_recibo)


    strSql= "exec fincdh_cadastro_movimento_sp 'I','T','"& emitente &"','"& dt_fim(data) &"','"& descricao &"','"& num_doc &"','"& valor &"','"& usuario &"',"&_
    "'"& conta_destino &"','"& conta_origem &"','"& categoria &"',"& pagoSn &",0,''"
    set adors1 = conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function

'Função para buscar a receita que vai ser alterada
Function fc_buscar_receita(id,emitente)


    strSql= "exec fincdh_lista_movimento_sp 'R','"& emitente &"','0001-01-01','9999-12-30',"& id &""
    set adors1 = conn1.execute(strsql)

    resposta = "|" & adors1("id_movimento") & "|" & adors1("data") & "|" & adors1("descricao") & "|" & adors1("id_conta") & "|" & adors1("id_categoria") & "|" & adors1("valor")& "|" & adors1("pagosn")& "|" & adors1("num_doc") & "|"
    response.Write resposta
    
Exit Function
End Function

'Função para buscar a despesa que vai ser alterada
Function fc_buscar_despesa(id,emitente)


    strSql= "exec fincdh_lista_movimento_sp 'D','"& emitente &"','0001-01-01','9999-12-30',"& id &""
    set adors1 = conn1.execute(strsql)

    resposta = "|" & adors1("id_movimento") & "|" & adors1("data") & "|" & adors1("descricao") & "|" & adors1("id_conta") & "|" & adors1("id_categoria") & "|" & adors1("valor")& "|" & adors1("pagosn")& "|" & adors1("num_doc") & "|"
    response.Write resposta
    
Exit Function
End Function

'Função para buscar a transferencia que vai ser alterada
Function fc_buscar_transferencia(id,emitente)


    strSql= "exec fincdh_Lista_movimento_transferencia_sp '"& emitente &"',"& id &""
    set adors1 = conn1.execute(strsql)

    resposta = strSql
    resposta = "|" & adors1("referencia") & "|" & adors1("data") & "|" & adors1("descricao") & "|" & adors1("conta_origem") & "|" & adors1("conta_destino") & "|" & adors1("categoria") & "|" & adors1("valor")& "|" & adors1("num_doc") & "|"
    response.Write resposta
    
Exit Function
End Function

'Função para alterar a receita
Function fc_alterar_receita(emitente,data,descricao,valor ,categoria ,num_doc ,conta_destino ,usuario , pagoSn,emitir_recibo,id)


    strSql= "exec fincdh_cadastro_movimento_sp 'A','R','"& emitente &"','"& dt_fim(data) &"','"& descricao &"','"& num_doc &"','"& valor &"','"& usuario &"',"&_
    "'"& conta_destino &"','','"& categoria &"',"& pagoSn &",0,"& id &" "
    set adors1 = conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function

'Função para alterar a despesa
Function fc_alterar_despesa(emitente,data,descricao,valor ,categoria ,num_doc ,conta_destino ,usuario , pagoSn,emitir_recibo,id)


    strSql= "exec fincdh_cadastro_movimento_sp 'A','D','"& emitente &"','"& dt_fim(data) &"','"& descricao &"','"& num_doc &"','"& valor &"','"& usuario &"',"&_
    "'"& conta_destino &"','"& conta_origem &"','"& categoria &"',"& pagoSn &",0,"& id &" "
    set adors1 = conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function


'Função para alterar os movimentos de transferencia (que são 2)
Function fc_alterar_transferencia(emitente,data,descricao,valor ,categoria ,num_doc ,conta_origem, conta_destino ,usuario,emitir_recibo,id)


    strSql= "exec fincdh_cadastro_movimento_sp 'A','T','"& emitente &"','"& dt_fim(data) &"','"& descricao &"','"& num_doc &"','"& valor &"','"& usuario &"',"&_
    "'"& conta_destino &"','"& conta_origem &"','"& categoria &"',"& pagoSn &",0,'"& id &"'"
    set adors1 = conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function

'Função para alterar recebido S ou não
Function fc_aletrar_recebimento_receita(emitente,usuario,pagoSn,id)


    strSql= "exec fincdh_cadastro_movimento_sp 'P','','"& emitente &"','','','',0,'"& usuario &"','','','',"& pagoSn &",0,"& id &" "
    set adors1 =  conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function

'Função para deletar receita
Function fc_deletar_receita(emitente,id)


    strSql= "exec fincdh_cadastro_movimento_sp 'E','R','"& emitente &"','','','',0,0,0,0,0,0,0,"& id &" "
    set adors1 = conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function

'Função para deletar despesa
Function fc_deletar_despesa(emitente,id)


    strSql= "exec fincdh_cadastro_movimento_sp 'E','D','"& emitente &"','','','',0,0,0,0,0,0,0,"& id &" "
    set adors1 = conn1.execute(strsql)

    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function

'Função para deletar despesa
Function fc_deletar_transferencia(emitente,id)


    strSql= "exec fincdh_cadastro_movimento_sp 'E','T','"& emitente &"','','','',0,0,0,0,0,0,0,"& id &" "
    set adors1 = conn1.execute(strsql)

    'resposta = strSql
    resposta = adors1("msg")
    response.Write resposta
    
Exit Function
End Function


%>