﻿<script type="text/javascript" src="js/dist/jquery.inputmask.bundle.min.js"></script>  
<script type="text/javascript" src="js/Previsao.js"></script>
<div id="pagina" class="conteudo">
 <fieldset>
    <legend>Cadastro de Previ&otilde;es:</legend>
    <div class="linha">
       <label for="dt_inicio">Data In&iacute;cio*:</label><input type="text"  data-inputmask="'alias': 'date'" id="dt_inicio" />
       <label for="dt_final">Data Final*:</label><input type="text" data-inputmask="'alias': 'date'" id="dt_final" />

    </div>
    <div class="linha">
        <label for="serie">Valor*: </label></label><input type="text" id="valor" size="10" value="" maxlength="14"/>
        <label for="serie">Tipo*: </label><select id="tipo" style="border:1px solid">
            <option value="1">Receita</option>
            <option value="2">Despesa</option>
        </select>
        <input type="hidden"  id="id"/><br /><br />
        *<label for="aviso" style="color:red">&nbspCampos obrigat&oacute;rios</label><br />
    </div>
    <div class="linha">
        <input type="button" value="Gravar" id="btn_gravar" />
        <input type="button" value="Alterar" id="btn_alterar" />
        <input type="button" value="Voltar" id="btn_voltar" />
    </div>
    </fieldset>   
</div>
