﻿<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<!--#include virtual="public/pub_Geral1.asp"-->

<html>
<body>
<script type="text/javascript" src="js/Receita.js"></script>
<div id="pagina" class="conteudo">
 <fieldset>
    <legend>Cadastro de Receita:</legend>
    <div class="linha">
        <label for="data">Data*:</label><input type="text" id="data" data-inputmask="'alias': 'date'" size="12" value="" maxlength="12" />
        <label for="descricao">Descri&ccedil;&atilde;o*:</label><input type="text" id="descricao" size="21" value="" maxlength="50"/>
    </div>
    <div class="linha">
         <label for="valor">Valor*:</label><input type="text" id="valor" size="11" maxlength="14"/>
        <label for="categoria">Categoria*:</label><select id="categoria" style="border:1px solid">
            <%
                strSql = "exec fincdh_lista_categoria_receita_sp '"& session("emitente") &"'"
                response.Write strSql
                set adors1 = conn1.execute(strSql)
                do while not adors1.eof
            %>
                <option value="<%=adors1("id_categoria")%>"><%=adors1("descricao")%></option>
            <%
                adors1.movenext
                loop
            %>
            </select>
    </div>
    <div class="linha">
        <label for="conta_destino">Conta Destino*:</label><select id="conta_destino" style="border:1px solid">
            <%
                strSql2 = "select id_conta,nome from fincdh_conta where emitente = '"& session("emitente") &"'"
                set adors2 = conn1.execute(strSql2)
                do while not adors2.eof
            %>
            <option value="<%=adors2("id_conta")%>"><%=adors2("nome")%></option>
            <%
                adors2.movenext
                loop
            %>
        </select>
    <label for="recebido">Recebido</label><input type="checkbox" id="recebido" />
    </div>
    <div class="linha">
        <label for="num_doc">Numero Documento:</label><input type="text" id="num_doc" size="32" value="" maxlength="20"/>
        <input type="hidden" id="id" /><br /><br />
        *<label for="aviso" style="color:red">&nbspCampos obrigat&oacute;rios</label><br />
    </div>
    <div class="linha">
        <input type="button" value="(F6) - Gravar" id="btn_gravar" />
        <input type="button" value="(F8) - Alterar" id="btn_alterar" />
        <input type="button" value="(F7) - Voltar" id ="btn_voltar" />
    </div>
    </fieldset>   
</div>
</body>

</html>
