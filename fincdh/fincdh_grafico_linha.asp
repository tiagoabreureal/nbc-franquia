﻿<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
		<style type="text/css">
${demo:css}
		</style>
		<script type="text/javascript">
       $ (function () {

// Get the CSV and create the chart
$.getJSON('fincdh_json.asp', function (data) {



    function JSON2CSV(objArray) {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;

        var str = '';
        var line = '';

        if ($("#labels").is(':checked')) {
            var head = array[0];
            if ($("#quote").is(':checked')) {
                for (var index in array[0]) {
                    var value = index + "";
                    line += '"' + value.replace(/"/g, '""') + '",';
                }
            } else {
                for (var index in array[0]) {
                    line += index + ',';
                }
            }

            line = line.slice(0, -1);
            str += line + '\r\n';
        }

        for (var i = 0; i < array.length; i++) {
            var line = '';

            if ($("#quote").is(':checked')) {
                for (var index in array[i]) {
                    var value = array[i][index] + "";
                    line += '"' + value.replace(/"/g, '""') + '",';
                }
            } else {
                for (var index in array[i]) {
                    line += array[i][index] + ',';
                }
            }

            line = line.slice(0, -1);
            str += line + '\r\n';
        }
        alert(str);
        return str;
        

    }

    JSON2CSV(data);

                

        $('#container').highcharts({

            data: {
                data: data
            },

            title: {
                text: 'Grafico de movimento financeiros'
            },

            subtitle: {
                //text: 'Source: Google Analytics'
            },

            xAxis: {
                tickInterval: 7 * 24 * 3600 * 1000, // one week
                tickWidth: 0,
                gridLineWidth: 1,
                labels: {
                    align: 'left',
                    x: 3,
                    y: -3
                }
            },

            yAxis: [{ // left y axis
                title: {
                    text: null
                },
                labels: {
                    align: 'left',
                    x: 3,
                    y: 16,
                    format: 'R${value:.,0.2f}'
                },
                showFirstLabel: false
            }, { // right y axis
                linkedTo: 0,
                gridLineWidth: 0,
                opposite: true,
                title: {
                    text: null
                },
                labels: {
                    align: 'right',
                    x: -3,
                    y: 16,
                    format: 'R${value:.,0f}'
                },
                showFirstLabel: false
            }],
            // Cor das linhas do grafico    
            colors: ['#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE',
                    '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92'],
            legend: {
                align: 'left',
                verticalAlign: 'top',
                y: 20,
                floating: true,
                borderWidth: 0
            },

            tooltip: {
                shared: true,
                crosshairs: true
            },

            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                hs.htmlExpand(null, {
                                    pageOrigin: {
                                        x: e.pageX,
                                        y: e.pageY
                                    },
                                    // Nome que vem do JSON
                                    headingText: this.series.name,
                                    // Contexto da label no caso dia da semana A, dia do mes e , mes Texto b, ano Y
                                    maincontentText: Highcharts.dateFormat('%A, %e  %b ,%Y', this.x) + ':<br/> ' +
                                        this.y + ' Valor',
                                    width: 200
                                });
                            }
                        }
                    },
                    marker: {
                        lineWidth: 1
                    }
                }
            },

            series: [{
                name: 'All visits',
                lineWidth: 4,
                marker: {
                    radius: 4
                }
            }, {
                name: 'New visitors'
            }]
        });
    });

});


		</script>
	</head>
	<body>
<script src="js/Highcharts-4.0.4/js/highcharts.js"></script>
<script src="js/Highcharts-4.0.4/js/modules/data.js"></script>
<script src="js/Highcharts-4.0.4/js/modules/exporting.js"></script>

<!-- Additional files for the Highslide popup effect -->
<script type="text/javascript" src="js/Highcharts-4.0.4/highslide-full.min.js"></script>
<script type="text/javascript" src="js/Highcharts-4.0.4/highslide.config.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="js/Highcharts-4.0.4/highslide.css" />

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<div id="Teste"></div>
	</body>
</html>
