﻿<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<!--#include virtual="public/pub_Geral1.asp"-->
<html>
<head>
<link href="estilo.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/Receita.js"></script>
</head>
<body>
<%

inicio = request("inicio")
fim = request("fim")
id = request("id")               
pendente = request("pendente")
total = 0.00
        
'Select para listar todas as previsões
    strSql = "exec fincdh_Lista_movimento_resumo_sp 'R','"& session("emitente")&"','"& dt_fim(inicio) &"','"& dt_fim(fim) &"','','"& pendente &"'"
    'response.Write strSql
    set adors1 = conn1.execute(strSql)

    
%>

<div id="pagina" class="conteudo">
 <fieldset>
        <legend>Resumo das Receitas:</legend>
        
        <label for="dt_inicio">Periodo:&nbsp</label><input type="text" data-inputmask="'alias': 'date'" id="dt_inicio" />&nbsp-&nbsp<input type="text" data-inputmask="'alias': 'date'" id="dt_final" />
        <label for="pendente">Recebido:&nbsp</label><select id="pendentes" style="border:1px solid">       
            <option value="-1">Todos</option>
            <option value="1">Sim</option>
            <option value="0">N&atilde;o</option>
        </select>
        <br /><br />
        <input type="button" value="(F2) - Listar Receitas" id="listar_receita" />
        <input type="button" value="(F4) - Incluir Receita" id="cad_receita" />
        <input type="button" value="Voltar" id="btn_voltar" style="visibility: hidden;"/>
        <br /><br />
        <table style="border-collapse: collapse" bordercolor="#C0C0C0"" border="1" width="100%" cellspacing="1" cellpadding="0" align="left">
            <thead>
                <tr> 
                    <th width="10%">C&oacute;digo</th>      
                    <th width="10%">Data</th>
                    <th width="30%">Descri&ccedil;&atilde;o</th>
                    <th width="20%">Conta</th>
                    <th width="20%">Categoria</th>
                    <th width="10%">Valor</th>
                    <th width="10%">Recebido</th>
                    <th width="10%">Excluir</th>
                    
                </tr>
            </thead>
            <tbody>
                <% do while not adors1.eof %>
                <tr>
                    <td class="numero"><a href="fincdh_cadastro_receita.asp" ><%=adors1("id_movimento")%></td>
                    <td class="numero"><%=adors1("data")%></td>
                    <td class="texto"><%=adors1("descricao")%></td>
                    <td class="texto"><%=adors1("conta")%></td>
                    <td class="texto"><%=adors1("categoria")%></td>    
                    <td class="numerico"><b><label style="color:green;">+<%=formatnumber(adors1("valor2"))%></label></b></td>
                    <%recebido = adors1("pagoSn")
                     if recebido = 1 then
                        %>
                    <td class="numero"><input id="<%=adors1("id_movimento")%>" type="checkbox" checked onclick="receber(this.id);"/></td>
                    <%else%>
                    <td class="numero"><input id="<%=adors1("id_movimento")%>" type="checkbox" onclick="receber(this.id);" /></td>
                    <%end if%>
                    <td class="numero"><input type="image" id="<%=adors1("id_movimento")%>" src="imagens/deletar.png" onclick="deletar(this.id);"/></td>
                </tr>
                <%
                total =  CDbl(adors1("valor")) + CDbl(total)

                adors1.movenext
                loop 
                %>
            </tbody>
            <thead>
                <th colspan ="5">Total</th>
                <th colspan ="3"><%=formatnumber(total)%></th>
            </thead>
        </table>  
     </fieldset>   
</div>
</body>

</html>
