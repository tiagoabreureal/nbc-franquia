﻿<!--#include virtual='public/pub_ParametrosTopo.asp'-->
<!--#include virtual="public/pub_Geral1.asp"-->
<html>
<head>
<title></title>
<link href="estilo.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/Resumo.js"></script>
</head>
<body>
<%

inicio = request("inicio")
fim = request("fim")
id = request("id")
pendente = request("pendente")               
total = 0 
        
'Select para listar todas as previsões
    strSql = "exec fincdh_Lista_movimento_resumo_sp '','"& session("emitente")&"','"& dt_fim(inicio) &"','"& dt_fim(fim) &"','','"& pendente &"'"
    'response.Write strSql
    set adors1 = conn1.execute(strSql)
    
%>


<div id="pagina" class="conteudo">
 <fieldset>
     <legend>Resumo Financeiro:</legend>
           
        <label for="dt_inicio">Periodo:&nbsp</label><input type="text" data-inputmask="'alias': 'date'" id="dt_inicio" />&nbsp-&nbsp<input type="text" data-inputmask="'alias': 'date'" id="dt_final" />
        <label for="pendente">Realizados:&nbsp</label><select id="pendentes" style="border:1px solid">
            <option value="-1">Todos</option>
            <option value="1">Sim</option>
            <option value="0">N&atilde;o</option>
        </select>
        <br /><br />
        <input type="button" value="Listar Resumo" id="Listar_resumo" />
        <br /><br />
        <table style="border-collapse: collapse" bordercolor="#C0C0C0"" border="1" width="100%" cellspacing="1" cellpadding="0" align="left">
            <thead>
                <tr>
                    <th width="05%">C&oacute;digo</th>
                    <th width="10%" >Data</th>
                    <th width="30%" >Descri&ccedil;&atilde;o</th>
                    <th width="15%" >Conta</th>
                    <th width="15%" >Categoria</th>
                    <th width="10%" >Valor</th>
                    <th width="05%" >Realizados</th> 
                    <th width="15%" >Tipo</th> 
                </tr>
            </thead>
            <tbody>
                <% do while not adors1.eof %>
                <tr>
                    <td class="numero"><%=adors1("id_movimento")%></td>
                    <td class="numero"><%=adors1("data")%></td>
                    <td class="texto"><%=adors1("descricao")%></td>
                    <td class="texto"><%=adors1("conta")%></td>
                    <td class="texto"><%=adors1("categoria")%></td>
                    <% valor = CDbl(adors1("valor2"))
                        if valor > 0 then %>    
                    <td class="numerico"><b><label style="color:green;">+<%=formatnumber(adors1("valor2"))%></label></b></td>
                    <%else%>
                    <td class="numerico"><b><label style="color:red;"><%=formatnumber(adors1("valor2"))%></label></b></td>
                    <%end if%>
                    <%pendente = adors1("pagoSn")
                     if pendente = 1 then
                        %>
                    <td class="numero"><input id="<%=adors1("id_movimento")%>" type="checkbox" checked onclick="receber(this.id);"/></td>
                    <%else%>
                    <td class="numero"><input id="<%=adors1("id_movimento")%>" type="checkbox" onclick="receber(this.id);" /></td>
                    <%end if%>
                    <td class="texto"><%=adors1("tipo")%></td>
                </tr>
                <%
                total =  CDbl(adors1("valor")) + CDbl(total)

                adors1.movenext
                loop 
                %>
            </tbody>
            <thead>
                <th colspan ="5">Total</th>
                <th colspan ="3"><%=formatnumber(total)%></th>
            </thead>     
        </table>
 
     </fieldset>   
</div>
</body>

</html>
