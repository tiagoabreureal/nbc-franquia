<%
Function pCase(strIn) 
strOut = "" 
nextLetterUp = True 
For x = 1 To Len(strIn)
    c = Mid(strIn, x, 1)
    If isNumeric(c) Then
        strOut = strOut & c
        nextLetterUp = true 
    ElseIf c = " " or c = "'" or c = "-" then 
        strOut = strOut & c 
        nextLetterUp = True 
    Else 
        If nextLetterUp Then 
            tc = Ucase(c) 
        Else 
            tc = LCase(c) 
        End If 
        strOut = strOut & tc 
        nextLetterUp = False 
    End If 
Next
strOut=replace(strOut," Da "," da ")
strOut=replace(strOut," De "," de ")
strOut=replace(strOut," Do "," do ")
strOut=replace(strOut," A "," a ")
strOut=replace(strOut," E "," e ")
strOut=replace(strOut," O "," o ")
strOut=replace(strOut," Na "," na ")
strOut=replace(strOut," No "," no ")
strOut=replace(strOut," Em "," em ")
pCase = strOut
End Function 
%>