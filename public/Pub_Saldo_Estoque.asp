<%
sub calcula_estoque_final(mes,ano)
	dim datafim
	relatorio=1
	
	for i=31 to 28 step -1
		if isdate(i & "/" & mes & "/" & ano) then
			datafim=cdate(i & "/" & mes & "/" & ano)
			exit for
		end if
	next
			
	sqlr = "select  count(*) as colunas from est_relatorio_titulo "
	sqlr = sqlr & " where relatorio = 1 and coluna > 0 "
	set adoRs1=conn1.execute(sqlr)

	l = adoRs1("colunas")
	set adoRs1 = nothing

	sqlr = "select  distinct * from est_relatorio_titulo "
	sqlr = sqlr & " where relatorio = 1 and coluna > 0 "
	sqlr = sqlr & " order by coluna"
	set adoRs1=conn1.execute(sqlr)
	on error resume next
		conn1.execute("drop table tmpEstoque")
		conn1.execute("drop table tmpSaldo")
	on error goto 0
	set adoRs2=nothing

	sqlr = "select distinct codint as produto "
	do while not adoRs1.eof
		sqlr = sqlr & ",0 as c" & adors1("coluna")
		adoRs1.movenext
	loop
	sqlr = sqlr & " into tmpEstoque from produto"
	sqlr = sqlr & " where produto <> '999999'"
	conn1.execute(sqlr)
'	Response.Write(sqlr & "<BR>")
	set adoRs2=nothing
	conn1.execute("delete from tmpEstoque")
	set adoRs2=nothing

	'criar tmp com saldo inicial do dia

	sqlr = "select max(data) as ultimo from est_movimento"
	sqlr = sqlr & " where data <= " & dt_fim(datafim)
	sqlr = sqlr & " and emitente in (" & empresa & ") and operacao=99"
'	Response.Write(sqlr & "<BR>")
	set adoRs2=conn1.execute(sqlr)
	ultimo = adoRs2("ultimo")

	sqlr = " select codint as produto, sum(s.quantidade) * r.operador as quantidade "
	sqlr = sqlr & " into tmpSaldo"
	sqlr = sqlr & " from saida s, produto i, est_operacao r, operacao_nf o"
	sqlr = sqlr & " where s.operacao = o.operacao and o.integracao = r.operacao"
	sqlr = sqlr & " and s.produto = i.produto "
	sqlr = sqlr & " and referencia >= " & dt_fim(ultimo) 
	sqlr = sqlr & " and referencia < " & dt_fim(ultimo) 
	sqlr = sqlr & " and s.emitente in (" & empresa & ")"
	sqlr = sqlr & " and s.produto <> '999999' and s.situacao = 0"
	sqlr = sqlr & " group by codint, r.operador"
'	Response.Write(sqlr & "<BR>")
	set adoRs2=conn1.execute(sqlr)
	set adoRs2=nothing

	sqlr = "insert into tmpSaldo "
	sqlr = sqlr & " select codint as produto, sum(s.quantidade) * r.operador as quantidade"
	sqlr = sqlr & " from EST_MOVIMENTO s, produto i, est_operacao r"
	sqlr = sqlr & " where s.operacao = r.operacao"
	sqlr = sqlr & " and s.produto = i.produto "
	sqlr = sqlr & " and ((r.operacao <> 99 "
	sqlr = sqlr & " and data >= " & dt_fim(ultimo) 
	sqlr = sqlr & "      and data < " & dt_fim(ultimo) & ") "
	sqlr = sqlr & " or (data = " & dt_fim(ultimo) & " and r.operacao = 99))"
	sqlr = sqlr & " and s.emitente in (" & empresa & ")"
	sqlr = sqlr & " and s.produto <> '999999'"
	sqlr = sqlr & " group by codint, r.operador"
'	Response.Write(sqlr & "<BR>")
	set adoRs2=conn1.execute(sqlr)
	set adoRs2=nothing
	
	sqlr = "insert into tmpSaldo "
	sqlr = sqlr & " select codint as produto, sum(s.quantidade) * r.operador as quantidade "
	sqlr = sqlr & " from entrada s, produto i, est_operacao r, operacao_nf o"
	sqlr = sqlr & " where s.operacao = o.operacao and o.integracao = r.operacao"
	sqlr = sqlr & " and s.produto = i.produto "
	sqlr = sqlr & " and referencia >= " & dt_fim(ultimo) 
	sqlr = sqlr & " and referencia < " & dt_fim(ultimo) 
	sqlr = sqlr & " and s.emitente in (" & empresa & ")"
	sqlr = sqlr & " and s.produto <> '999999'"
	sqlr = sqlr & " group by codint, r.operador"
'	Response.Write(sqlr & "<BR>")
	set adoRs2=conn1.execute(sqlr)
	set adoRs2=nothing

	'criar tmp com os dados por coluna

	adoRs1.movefirst

	sqlf = "select t.produto, mid(p.descricao,1,22) as descricao"

	do while not adoRs1.eof
		sqlr = "insert into tmpEstoque (produto, c" & adors1("coluna") & ")"
		sqlr = sqlr & " select codint, sum(s.quantidade) * r.soma_diminui "
		sqlr = sqlr & " from saida s, produto i, est_relatorio r, operacao_nf o"
		sqlr = sqlr & " where s.operacao = o.operacao and o.integracao = r.operacao"
		sqlr = sqlr & " and s.produto = i.produto and r.relatorio = " & relatorio
		sqlr = sqlr & " and r.coluna = " & adors1("coluna") & " and r.operacao <> 99 "
		sqlr = sqlr & " and referencia >= " & dt_fim(ultimo) 
		sqlr = sqlr & " and referencia <= " & dt_fim(datafim) 
		sqlr = sqlr & " and s.emitente in (" & empresa & ")"
		sqlr = sqlr & " and s.produto <> '999999' and s.situacao = 0"
		sqlr = sqlr & " group by codint, r.soma_diminui"
		'Response.Write(sqlr & "<BR>")

		set adoRs2=conn1.execute(sqlr)
		set adoRs2=nothing

		sqlr = "insert into tmpEstoque (produto, c" & adors1("coluna") & ")"
		sqlr = sqlr & " select codint, sum(s.quantidade) * r.soma_diminui "
		sqlr = sqlr & " from EST_MOVIMENTO s, produto i, est_relatorio r"
		sqlr = sqlr & " where s.operacao = r.operacao"
		sqlr = sqlr & " and s.produto = i.produto and r.relatorio = " & relatorio
		sqlr = sqlr & " and r.coluna = " & adors1("coluna") & " and r.operacao <> 99 "
		sqlr = sqlr & " and DATA >= " & dt_fim(ultimo) 
		sqlr = sqlr & " and DATA <= " & dt_fim(datafim) 
		sqlr = sqlr & " and s.emitente in (" & empresa & ")"
		sqlr = sqlr & " and s.produto <> '999999'"
		sqlr = sqlr & " group by codint, r.soma_diminui"
		'Response.Write(sqlr & "<BR>")

		set adoRs2=conn1.execute(sqlr)
		set adoRs2=nothing
		
		sqlr = "insert into tmpEstoque (produto, c" & adors1("coluna") & ")"
		sqlr = sqlr & " select s.produto, sum(s.quantidade) * r.soma_diminui "
		sqlr = sqlr & " from tmpSaldo s, produto i, est_relatorio r"
		sqlr = sqlr & " where s.produto = i.produto and r.relatorio = " & relatorio
		sqlr = sqlr & " and r.coluna = " & adors1("coluna") & " and r.operacao = 99 "
		sqlr = sqlr & " group by s.produto, r.soma_diminui"
		'Response.Write(sqlr & "<BR>")

		set adoRs2=conn1.execute(sqlr)
		set adoRs2=nothing
		
		sqlr = "insert into tmpEstoque (produto, c" & adors1("coluna") & ")"
		sqlr = sqlr & " select codint, sum(s.quantidade) * r.soma_diminui "
		sqlr = sqlr & " from entrada s, produto i, est_relatorio r, operacao_nf o"
		sqlr = sqlr & " where s.operacao = o.operacao and o.integracao = r.operacao"
		sqlr = sqlr & " and s.produto = i.produto and r.relatorio = " & relatorio
		sqlr = sqlr & " and r.coluna = " & adors1("coluna") & " and r.operacao <> 99 "
		sqlr = sqlr & " and referencia >= " & dt_fim(ultimo) 
		sqlr = sqlr & " and referencia <= " & dt_fim(datafim) 
		sqlr = sqlr & " and s.emitente in (" & empresa & ")"
		sqlr = sqlr & " and s.produto <> '999999'"
		sqlr = sqlr & " group by codint, r.soma_diminui"
		'Response.Write(sqlr & "<BR>")
		set adoRs2=conn1.execute(sqlr)
		set adoRs2=nothing
		sqlf = sqlf & " ,sum(c" & adors1("coluna") & ") as tc" & adors1("coluna")
		adoRs1.movenext
	loop
	set adoRs1=nothing

	sqlf = sqlf & " from tmpEstoque t, produto p"
	sqlf = sqlf & " where t.produto = p.produto"
	sqlf = sqlf & " group by t.produto,  mid(p.descricao,1,22)"

	set adoRs1=conn1.execute(sqlf)
	do while not adoRs1.eof
	'    Response.Write("update wsfaturamento set estoque=" & adors1("tc10") & " where produto='" & adors1("produto") & "'")
	'    Response.End
		sqlx="update wsfaturamento,faixa_preco f set estoque = " & adors1("tc10") & " * preco_venda "  
		sqlx=sqlx & " where wsfaturamento.produto='" & adors1("produto") & "' and wsfaturamento.produto=f.produto and faixa=5 "
		sqlx=sqlx & " and mes=" & mes & " and ano=" & ano & " and icms_ent > 0"
		'Response.Write(sqlx & "<BR>")
		'Response.End
		conn1.execute sqlx
		adors1.movenext
	loop
	adors1.close
	set adors1=nothing
	
	conn1.execute("drop table tmpEstoque")
	conn1.execute("drop table tmpSaldo")
end sub
%>