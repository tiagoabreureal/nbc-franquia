<%
function Email_Enviar(id_addAddress, id_Subject, id_Body, id_Destinatario, id_Server, id_User, id_Password, id_Queue, id_Html, id_From, id_FromName, id_Addcc, id_AddBcc, id_Opcao_Envio)
'' id_addAddress: lista de endere�os
'' id_Subject	: assunto
'' id_Body		: mensagem
'' id_Destinatario: destinatario
'' id_Server	: servidor SMTP **
'' id_User		: usu�rio **
'' id_Senha		: senha **
'' id_Queue		: coloca mensagem em fila (true/false)
'' id_Html		: permite HTML (true/false)
'' id_From		: e-mail do remetente **
'' id_FromName	: nome do remetente **
'' id_Addcc		: lista de endere�os "com c�pia"
'' id_AddBcc	: lista de endere�os "com c�pia oculta"
'' id_Opcao_Envio: 0 - enviar via procedure (sql) |||| 1 - enviar via componente ASPX

'' ** esses par�metros, se passar com valor "" o sistema busca da tabela sys_parametro
	strsql	="select email,email_nome,email_servidor,email_usuario,email_senha,email_assinatura=isnull(email_assinatura,'') " & _
			 "from sys_parametro " & _
			 "where atividade=" & session("atividade")
	set adoEmail=locConnVenda.execute(strsql)
	if not adoEmail.eof		then
		if id_From=""		then id_From	=adoEmail("email")
		if id_FromName=""	then id_FromName=adoEmail("email_nome")
		if id_server=""		then id_server	=adoEmail("email_servidor")
		if id_User=""		then id_User	=adoEmail("email_usuario")
		if id_Password=""	then id_Password=adoEmail("email_senha")
		email_assinatura	=adoEmail("email_assinatura")
	end if
	set adoEmail=nothing

	'on error resume next
	if id_Opcao_Envio="0" then
			strsql="exec master.dbo.sp_enviar_email '" & id_From & "','" & id_addAddress & "','" & id_Subject & "','" & id_Body & "'"
			set adors1=conn5.locconnvenda.execute(strsql)
			if adors1.eof then
				strsql=	"insert into sys_email_log (email,email_nome,email_add,email_cc,email_bcc,email_assunto,texto,usuario,destinatario,status,data_enviado) values " & _
						"('" & id_From & "','" & id_FromName & "','" & id_addAddress & "','" & id_Addcc & "','" & id_Addbcc & "','" & id_Subject & "','" & replace(id_Body,"'","") & "'," & _
						"'" & session("usuario") & "','" & id_Destinatario & "',0,getdate())"
				locConnVenda.execute(strsql)
			else
				if adors1("erro")="0" then
					strsql=	"insert into sys_email_log (email,email_nome,email_add,email_cc,email_bcc,email_assunto,texto,usuario,destinatario,status,data_enviado) values " & _
						"('" & id_From & "','" & id_FromName & "','" & id_addAddress & "','" & id_Addcc & "','" & id_Addbcc & "','" & id_Subject & "','" & replace(id_Body,"'","") & "'," & _
						"'" & session("usuario") & "','" & id_Destinatario & "',0,getdate())"
					locConnVenda.execute(strsql)
				else
					strsql=	"insert into sys_email_log (email,email_nome,email_add,email_cc,email_bcc,email_assunto,texto,usuario,destinatario,status) values " & _
							"('" & id_From & "','" & id_FromName & "','" & id_addAddress & "','" & id_Addcc & "','" & id_Addbcc & "','" & id_Subject & "','" & replace(id_Body,"'","") & "'," & _
							"'" & session("usuario") & "','" & id_Destinatario & "',98)"
					locConnVenda.execute(strsql)
					call redi_Aviso("E-mail", "Erro ao enviar e-mail: " & err.Description)
				end if
			end if
			set adors1=nothing

	else
		dim email_assinatura
		email_assinatura="-"

		Set Mail = Server.CreateObject("CDO.Message") 
		Set MailCon = Server.CreateObject ("CDO.Configuration") 
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = id_Server
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport")= 25 
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
		MailCon.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 
		MailCon.Fields.update 
		Set Mail.Configuration = MailCon 
		Mail.From = id_From
		
		'mail.CC="suporte@xcompany.com.br"
		Mail.To = id_AddAddress
		Mail.Subject = id_Subject
		
		id_Body	="<html><head><title></title></head><body>" & _
		 "	<table border='0' width='100%' cellppading='0' cellspacing='0'>" & _
		 "		<tr><td width='100%' align='center' valign='top'>" & id_Body & "</td></tr>" & _
		 "		<tr><td width='100%' align='center' valign='top'>" & email_assinatura & "</td></tr>" & _
		 "	</table>" & _
		 "</body></html>"
 
		Mail.HTMLBody=id_Body
		Mail.Fields.update
		Mail.Send 

		Set Mail = Nothing 
		Set MailCon = Nothing 
			
		If Err <> 0 Then 
			strsql=	"insert into sys_email_log (email,email_nome,email_add,email_cc,email_bcc,email_assunto,texto,usuario,destinatario,status,data) values " & _
					"('" & id_From & "','" & id_FromName & "','" & id_addAddress & "','" & id_Addcc & "','" & id_Addbcc & "','" & id_Subject & "','" & replace(id_Body,"'","") & "'," & _
					"'" & session("usuario") & "','" & id_Destinatario & "',98,getdate())"
			locConnVenda.execute(strsql)
			'call redi_Aviso("E-mail", "Erro ao enviar e-mail: " & err.Description)
			
		else
			strsql=	"insert into sys_email_log (email,email_nome,email_add,email_cc,email_bcc,email_assunto,texto,usuario,destinatario,status,data) values " & _
					"('" & id_From & "','" & id_FromName & "','" & id_addAddress & "','" & id_Addcc & "','" & id_Addbcc & "','" & id_Subject & "','" & replace(id_Body,"'","") & "'," & _
					"'" & session("usuario") & "','" & id_Destinatario & "',0,getdate())"
				'Response.Write strsql
				'Response.end
			locConnVenda.execute(strsql)
		end if
	end if
	

	
end function
%>