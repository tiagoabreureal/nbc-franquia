<%
'' Fun��es
function dt_fim(strD)
	dt_fim=year(strD) & "-" & month(strD) & "-" & day(strD)
end function

function dt_formata(a)
	aDia=day(a)
	aMes=month(a)
	aAno=year(a)
	
	if aDia<10 then aDia="0" & cstr(aDia)
	if aMes<10 then aMes="0" & cstr(aMes)
	
	dt_Formata=aDia & "/" & aMes & "/" & aAno
end function

function cpf_formata(Cpf)
	Cpf=trim(Cpf)
	Cpf=replace(replace(replace(replace(Cpf,".",""),"-",""),",","")," ","")
	
	if len(cpf)>0 then
		if len(Cpf)=14 then  '''CNPJ
			cpf_formata=mid(Cpf,1,2) & "." & mid(Cpf,3,3) & "." & mid(Cpf,6,3) & "/" & mid(Cpf,9,4) & "-" & mid(Cpf,13,2)
		elseif len(strCpf)=11 then ''CPF
			cpf_formata=mid(Cpf,1,3) & "." & mid(Cpf,4,3) & "." & mid(Cpf,7,3) & "-" & mid(Cpf,10,2)
		else
			cpf_formata=Cpf
		end if	
	else
		cpf_formata=Cpf
	end if
	
end function

function rg_formata(Rg,strOpcao)
	Rg=trim(Rg)
	Rg=replace(replace(replace(replace(Rg,".",""),"-",""),",","")," ","")
	
	if strOpcao="1" then  ''RG
		if len(rg)=9 then
			rg_formata=mid(Rg,1,2) & "." & mid(Rg,3,3) & "." & mid(Rg,6,3) & "-" & mid(Rg,9,1)
		else
			rg_formata=rg
		end if
	elseif strOpcao="2" then ''inscricao estadual
		if len(rg)>0 then
			rg=string(14-len(rg),"0") & cstr(rg)				
			rg_formata=mid(Rg,1,2) & "." & mid(Rg,3,3) & "." & mid(Rg,6,3) & "." & mid(Rg,9,3) & "." & mid(Rg,12,3)
		else
			rg_formata=""
		end if
	else
		rg_formata=rg
	end if
end function

function cep_formata(Cep)
	Cep=trim(Cep)
	Cep=replace(replace(replace(replace(Cep,".",""),"-",""),",","")," ","")
	if len(trim(cep))>0 then		
		Cep=string(8-len(Cep),"0") & cep	
		cep_formata=mid(cep,1,5) & "-" & mid(cep,6,8)	
	else
		cep_formata=""
	end if
end function

function dt_brasil(strD)
	dt_brasil=day(strD) & "/" & month(strD) & "/" & year(strD)
end function

function Func_Marquee(strMarqueeTxt,strMarqueeCorFundo,strMarqueeTam,strMarqueeAltura,strMarqueeVelocidade,strMarqueeNVezes)
	'modelo
	'<marquee width="80" height="19" bgcolor="yellow" scrolldelay="50" scrollamount="5" align="middle" behavior="slide" loop="5">1234567890</marquee>
	'defini��o padr�o
	if strMarqueeTxt		="0" then strMarqueeTxt			="teste"
	if strMarqueeCorFundo	="0" then strMarqueeCorFundo	="yellow"
	if strMarqueeTam		= 0 then strMarqueeTam			=len(trim(strMarqueeTxt)) * 7
	if strMarqueeAltura		= 0 then strMarqueeAltura		=12
	if strMarqueeVelocidade	= 0 then strMarqueeVelocidade	=100
	if strMarqueeNVezes		= 0 then strMarqueeNVezes		=5

	Response.Write "<marquee width='" & strMarqueeTam & "' height='" & strMarqueeAltura & "' bgcolor='" & strMarqueeCorFundo & "' scrolldelay='" & strMarqueeVelocidade & "' scrollamount='5' align='middle' behavior='slide' loop='" & strMarqueeNVezes & "'>"
	Response.write strMarqueeTxt
	Response.Write "</marquee>"
end function

function Func_ImpPag(intTipo,intColSpan)
	if bassinatura then
		Response.Write("<TR><TD colspan=" & intcolspan & "><BR><BR>" & "______________________&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp______________________<BR>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspassinatura&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspassinatura&nbsp&nbsp&nbsp&nbsp</td></TR>")
	end if
	if intTipo="0" then
		strMsg1=" "
	else
		strMsg1="Qtd: " & intTotal
	end if
	%>
	<tr>
	  <td colspan="<%=intColSpan%>">
		<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
			<tr>
				<td align="left" valign="top" width="50%">
					<font face="verdana" size="1"><%=strMsg1%></font>
				</td>
				<td align="right" valign="top" width="50%">
					<font face="verdana" size="1">P�gina: <%=intContaPag%></font>
				</td>
			</tr>
		</table>
	  </td>
	</tr>
<%
end function

Function CalculaDigito(strP, strBase)
	strP = cstr(strP) & "0"

	Dim intAcum, intAlterna, intPeso
	intAlterna = 0

	For intI = Len(strP) To 1 Step -1
	'alterar peso
	    If intAlterna = 0 Then
	        intPeso = 2
	        intAlterna = 1
	    Else
	        intPeso = 1
	        intAlterna = 0
	    End If
	    
	'verificar tamanho do resultado e acumular
	    If Len(Trim(Mid(strP, intI, 1) * intPeso)) > 1 Then
	        intAcum = intAcum + Int(Mid(Mid(strP, intI, 1) * intPeso, 1, 1)) + Int(Mid(Mid(strP, intI, 1) * intPeso, 2, 1))
	    Else
	        intAcum = intAcum + Mid(strP, intI, 1) * intPeso
	    End If
	Next
	CalculaDigito = strBase - intAcum Mod strBase
End Function

function encripta(valor,opcao)
	novo=""
	max=len(trim(valor))

	if opcao=1 then
		for intI=1 to max
			novo = novo + string(3-len(asc(mid(valor,max-intI+1,1))),"0") & asc(mid(valor,max-intI+1,1))
		next
	else
		for intI=1 to max/3
			novo = novo + chr(mid(valor,max-(intI*3)+1,3))
		next
	end if
	encripta=novo
end function

function GerarCodigo(tipo)
	GCdia = day(now)
	if GCdia<10 then GCdia="0" & GCdia
	GCmes = cstr(month(now))
	if GCmes<10 then GCmes="0" & GCmes
	GChora=hour(now)
	if GChora <10 then GChora="0" & GChora
	GCano=year(date)
	if GCano<1900 then GCano=1900 + GCano
	GCminuto=minute(now)
	if GCminuto<10 then GCminuto="0" & GCminuto
	GCsegundo=second(now)
	if GCsegundo<10 then GCsegundo="0" & GCsegundo

	if tipo=1 then 
		codigo=GCano & GCmes & GCdia
	else
		codigo=GCsegundo & GCminuto & GChora & GCano & GCmes & GCdia
	end if

	max=len(codigo)
	for intI=1 to max
		novoCodigo=novoCodigo & mid(codigo,max-inti+1,1)
	next

	GerarCodigo=novoCodigo
end function

function Verifica_Permissao_Aplicacao(id)
	strSql="select b.gerar_log from sys_permissao a inner join sys_aplicacao b on a.aplicacao=b.aplicacao " & _
		   "where a.usuario='" & session("usuario") & "' and a.aplicacao=" & id
	set adoVerifica=conn5.execute(strSql)
	
	if adoVerifica.eof then
		Verifica_Permissao_Aplicacao=false		
	else
		Verifica_Permissao_Aplicacao=true					
	end if
	set adoVerifica=nothing				
end function

function iif(condicao,retTrue,retFalse) 
    if eval(condicao) = true then 
        iif = retTrue 
        else
        iif = retFalse 
    end if 
end function

function dias_mes(data)
	if isdate(data) then
		dias_mes=day(dateadd("d",-day(data),dateadd("m",1,data)))		
	end if
end function


' *********************************************************
' funcoes_valida.asp
' *********************************************************


function Manutencao(Manu_Usuario)
	if ucase(Manu_Usuario)<>ucase(session("usuario")) then
		call redi_aviso("P�gina em Manuten��o","Essa p�gina est� em manuten��o pelo usu�rio " & Manu_Usuario & ", favor tentar mais tarde.")
	end if
end function

function Manutencao2(tempo)	
	call redi_aviso("Manuten��o","Sistema Intranet/Corporate em manuten��o, favor tentar mais tarde.<br>Prazo: " & tempo)
end function

function rw(strTexto, strParar)
	Response.Write "<table border='1' cellpadding='0' cellspacing=1 align='center' bordercolor='#c0c0c0' style='border-collapse: collapse'>" & _
				   "<tr><td width='100%'><font face='verdana' size='1' color='red'>" & strTexto & "</font></td></tr></table>"
	if strParar="1" then Response.end
end function

function exibe_descricao_pagina(id_pagina)
	strSql="select alias from sys_aplicacao where aplicacao=" & id_pagina & " and gerar_log=1"
	set	adoA=conn5.execute(strSql)
	if not adoA.eof then%>
		<center><font face="verdana" size="1" color="<%=session("cor")%>"><b><%=adoa("alias")%></b></font></center>
		<%
	end if
	set adoA=nothing
end function

sub Rel_Item_Texto(texto,corFundo)'Funcao para exibir um texto na linha ou uma linha em branco
'Par�metros: 
 '    texto -> Texto a ser exibido
 '    corFundo -> S: Exibe a cor padr�o de fundo de coluna N: Fundo fica branco  

    if ( ucase(corFundo)="S" ) then
	 corFundo = "bgcolor='" & session("cor_rel_parametro") & "'"
	else 
	 corFundo = ""
	end if

	Response.write "<tr><td width='100%' " &corFundo& " align='left' colspan='2'>"
	
	 Response.write "<font face='verdana' size='1'><strong>" & texto & "</strong>&nbsp; </font>"
	
	Response.write "</td></tr>"
end sub

sub exibe_mensagem_nfe(opcao)
	strsql="select mensagem,data from fat_mensagem"
	set adoM=connNfe.execute(strsql)
	if not adoM.eof then
		if len(trim(adoM("mensagem")))>0 then
			strParametroTab1="<tr><td colspan=20 align='center'><font face='verdana' size='2' color='red'><b>" &  adoM("mensagem") & "<br></b></font><font face='verdana' size='1' color='black'><i>�ltima atualiza��o: " & adoM("data") & "</i></font></td></tr>"
			if opcao="1" then Response.Write strParametroTab1
		end if
	end if
	set adoM=nothing
end sub

function rec(valor) 
	rec = replace(trim(request(valor)),"'","") 
end function


function formata_num(valor,casas)
	if cdbl(valor)=cdbl(0) then
		formata_num	=""
	else
		formata_num	=formatnumber(valor,casas)
	end if
end function


%>