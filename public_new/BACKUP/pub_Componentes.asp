<%
'' variaveis
dim variavel(20)
dim intContaPag
dim iColuna
dim iquebra
dim intContaLinha 
dim Fundo
dim Fundo_Aux
dim Fundo_Conta
dim Fundo_Alterna
dim desc(50)
dim tam(50)
dim ali(50)
dim fmt(50)
dim alid(50)
dim cpod(50)
dim tcoluna(50)
dim cpo(50)
dim ctotal(50)
dim iLink(50)
dim iLinkTarget(50)
dim itarget
dim iLinkqb(50)
dim repete(50)
dim linkCab(50)
dim cpoant(50)
dim corColunaFundo(50)
dim formatofont(50)
dim Impressao_Tipo
dim imp_MaxLinha
dim strOnload
dim form_Validar
dim strMediaGeral
dim strParametroTab1
dim empresaRel
dim fechaTD
dim strBotaoArquivo
dim nomeBotao
dim MsgEmail
dim strParametroVoltar
dim hierarquia_todos
dim exibe_itens_cab
dim linhacab
dim linharodape
dim tableborder
dim TipoMsg
dim TituloMsg
dim retzero
dim semDetalhe
dim StrNomeCab
dim habilita_gerar_arq
dim configura_arquivo

dim empresa
dim ordem 
dim sqlp 
dim sqlc 
dim sqlf 

dim link_a
dim link_b
dim link_string

dim linkqb_a
dim linkqb_b
dim linkqb_string

dim qb_cpo(50)
dim qb_desc(50)
dim qb_ant(50)
dim qb_lista_ant(50)
dim qb_cab(50)
dim qb_corFonte(50)
dim qb_corFundo(50)
dim qb_Negrito(50)
dim qb_tamanho(50)
dim qb_traco
dim tg(50,10)
dim tot_link(50)
dim impcab
dim novo_cab(50)
dim formata_qtd

dim tam_tabela
dim tipo_tam_tabela
dim tipo_tam_coluna
dim opcao_mensagem_aviso
dim opcao_mensagem_tipo

dim pag_total_reg
dim pag_qtd
dim pag_pagina_atual

nomeBotao = "Acessar"
tam_tabela="100"
tipo_tam_tabela="%"
tipo_tam_coluna="%"
opcao_mensagem_aviso="0"

strParametroTab1=""
impcab = 0
iColuna=0
iquebra = 0
intContaPag = 1
intContaLinha = 0
imp_MaxLinha=65
tableborder=1
strOnload=""
form_validar=""
strBotaoArquivo="1"
exibe_itens_cab="1"
retzero=false
Fundo_Alterna=session("alterna_linha")
habilita_gerar_arq=true
configura_arquivo=false

qb_desc(9) = "T O T A L"
qb_desc(10)= "M � D I A"
qb_ant(9) = ""
qb_traco = false

'' Fun��es - Criar Componentes
'' textbox
sub Txt_Criar(strTipo,strNome,strTamanho,strMaximo,strValor)	
	if ucase(strTipo)="DATA" then
		if ucase(strNome)="STRFIM" then ''' colocar validacao para data fim ser maior q data inicio
			'tipoValidacao="onkeydown='PreencheData(" & strNome & ")' onblur='ValidarData(" & strNome & ",1)'"
'			tipoValidacao="onKeyPress='if (!(PreencheData(" & strNome & "))) return false;' onblur='ValidarData(" & strNome & ",1)'"
			tipoValidacao="onFocus=javascript:vDateType='3'" & " onBlur=DateFormat(this,this.value,event,true,'3')" & " onKeyUP=DateFormat(this,this.value,event,false,'3')"
		elseif ucase(strNome)="STRINI" then
			'tipoValidacao="onkeydown='PreencheData(" & strNome & ")' onblur='ValidarData(" & strNome & ",2)'"
'			tipoValidacao="onKeyPress='if (!(PreencheData(" & strNome & "))) return false;' onblur='ValidarData(" & strNome & ",2)'"
			tipoValidacao="onFocus=javascript:vDateType='3'" & " onBlur=DateFormat(this,this.value,event,true,'3')" & " onKeyUP=DateFormat(this,this.value,event,false,'3')"
		else
			'tipoValidacao="onkeydown='PreencheData(" & strNome & ")' onblur='ValidarData(" & strNome & ",0)'"
'			tipoValidacao="onKeyPress='if (!(PreencheData(" & strNome & "))) return false;' onblur='ValidarData(" & strNome & ",0)'"
			tipoValidacao="onFocus=javascript:vDateType='3'" & " onBlur=DateFormat(this,this.value,event,true,'3')" & " onKeyUP=DateFormat(this,this.value,event,false,'3')"
		end if		
	elseif ucase(strTipo)="INTEIRO" then 
		tipoValidacao="onblur='ValidarInteiro(" & strNome & ")'"
	elseif ucase(strTipo)="DISABLED" then
		tipoValidacao="disabled"	
	elseif ucase(strTipo)="ANO_REDIRECIONAR" then
		tipoValidacao="onblur='ValidarInteiro(" & strNome & ")'  onchange='carrega()'"
	end if	
'	Response.Write "<input type='textbox' name='" & strNome & "' size='" & strTamanho & "' maxlength='" & strMaximo & "' value='" & strValor & "' style='font-family: Verdana; font-size: 8pt' " & tipoValidacao & " onkeyup='autotab2(this,event)'>"
	Response.Write "<input type='textbox' name='" & strNome & "' size='" & strTamanho & "' maxlength='" & strMaximo & "' value='" & strValor & "' style='font-family: Verdana; font-size: 8pt' " & tipoValidacao & "> " 
end sub

'' hidden
sub TxtSenha_Criar(strNome,strTamanho,strMaximo,strValor)	
	Response.Write "<input type='password' name='" & strNome & "' size='" & strTamanho & "' maxlength='" & strMaximo & "' value='" & strValor & "' style='font-family: Verdana; font-size: 8pt' onkeyup='autotab2(this,event)'>"
end sub

'' hidden
sub TxtOculto_Criar(strNome,strValor)	
	Response.Write "<input type='hidden' name='" & strNome & "' value='" & strValor & "'>"
end sub

'' file
sub TxtFile_Criar(strNome,strTamanho,strMaximo,strValor)
	Response.Write "<input type='file' name='" & strNome & "' size='" & strTamanho & "' maxlength='" & strMaximo & "' value='" & strValor & "' style='font-family: Verdana; font-size: 8pt' " & tipoValidacao & "> " 
end sub

'' Per�odo
sub Txt_Periodo(default1,default2)
	call Txt_Criar("data","strIni",10,10,default1)
	Response.Write "<font face='verdana' size='1'>-</font>"
	call Txt_Criar("data","strFim",10,10,default2)
end sub

'' Intervalo de codigos
sub Txt_item(default1,default2,codt)
	call Txt_Criar("texto","strCodI",codt,codt,default1)
	Response.Write "<font face='verdana' size='1'>-</font>"
	call Txt_Criar("texto","strCodF",codt,codt,default2)
end sub

'' Per�odo2
sub Txt_Periodo2(default1,default2)
	call Txt_Criar("data","strIni2",10,10,default1)
	Response.Write "<font face='verdana' size='1'>-</font>"
	call Txt_Criar("data","strFim2",10,10,default2)
end sub

'' select ini
sub Select_Criar(strNome,opcao)	
	Response.Write "<select name='" & strNome & "' style='font-family: Verdana; font-size: 8pt' " & opcao & ">"
end sub

'' select item
sub Select_Item(strValor,strDesc,strSel)	
	Response.Write "<option value='" & strValor & "' " & strSel & ">" & strDesc & "</option>"
end sub

'' select fim
sub Select_Fim()	
	Response.Write "</select>"
end sub

'' select com todos meses
sub Select_Mes(strNome,opcao,default1)
	call Select_Criar(strNome,opcao)
		for i=1 to 12
			if cstr(default1)=cstr(i) then
				sel="selected"
			else
				sel=""
			end if			
			call Select_Item(i,mes(i),sel)
		next
	call Select_Fim()
end sub

'' button
sub Button_Criar(strTipo,strNome,strValor, strOpcao)
	Response.Write "<input type='" & strTipo & "' name='" & strNome & "' value='" & strValor & "' style='font-family: Verdana; font-size: 8pt' " & strOpcao & ">"
end sub

'' checkbox
sub Checkbox_Criar(strNome,strValor,strSel,strTexto,strOpcao)
	Response.Write "<input type='checkbox' name='" & strNome & "' value='" & strValor & "' " & strSel & " style='font-family: Verdana; font-size: 8pt' " & strOpcao & ">"
	Response.Write "<font face='verdana' size='1'>" & strTexto & "</font>"
end sub

'' radio
sub radio_Criar(strNome,strValor,strSel,strTexto)
	Response.Write "<input type='radio' name='" & strNome & "' value='" & strValor & "' " & strSel & " style='font-family: Verdana; font-size: 8pt'>"
	Response.Write "<font face='verdana' size='1'>" & strTexto & "</font>"
end sub

'' textarea
sub TextArea_Criar(strNome,strRow,strCol,strTexto)
	Response.Write "<textarea name='" & strNome & "' rows='" & strRow & "' cols='" & strCol & "' style='font-family: Verdana; font-size: 8pt'>" & strTexto & "</textarea>"
end sub

'' formul�rio ini
sub Form_Criar(strNome,strAcao)
	if len(trim(form_Validar))>0 then		
		Validar="onsubmit='return " & form_Validar & "'"
	else		
		if strNome="frm1" then	Validar="onsubmit='frm1.btn1.disabled=true'"			
	end if
	Response.Write "<form name='" & strNome & "' method='post' action='" & strAcao & "' " & Validar & ">"
end sub

'' formul�rio fim
sub Form_Fim()
	Response.Write "</form>"
end sub

'' rel ini
sub Rel_Criar(strTamanhoTab,strNomeForm,strAcao)
	if strTamanhoTab=0 then strTamanhoTab=50	
	
	Response.Write "<br><br><table style='border-collapse: collapse' width='" & strTamanhoTab & "%' border='1' bordercolor='#c0c0c0' cellspacing='1' cellpadding='0' align='center'>"
	call Form_Criar (strNomeForm,strAcao)
end sub

'' rel ini com 2 colunas
sub Rel2_Criar(strTamanhoTab,strNomeForm,strAcao)
	if strTamanhoTab=0 then strTamanhoTab="80"
	
	Response.Write	"<br><br>" & _
				    "<table style='border-collapse: collapse' width='100%' border='0' bordercolor='#c0c0c0' cellspacing='1' cellpadding='0' align='center'>" & _
				    "<tr><td width='65%' align='center' valign='top'>" & _
					"	<table style='border-collapse: collapse' width='" & strTamanhoTab & "%' border='1' bordercolor='#c0c0c0' cellspacing='1' cellpadding='0' align='center'>"
	call Form_Criar (strNomeForm,strAcao)
end sub

sub Rel2_Quebra(strArquivo)
	Response.Write	"</td></tr>" & _
					"<tr><td width='100%' colspan='2' align='center' valig='top'>"
	call Button_Criar("submit","btn1",nomeBotao,"")
	if strArquivo=1 then
		Response.Write "&nbsp&nbsp;&nbsp&nbsp;&nbsp&nbsp;"
		call Button_Criar("submit","btn2","Gerar Arquivo","")
		session("path_original")=Request.ServerVariables("PATH_INFO")
		session("path_nome_aplicacao")=StrNomeCab
	end if
	'botao ajuda
	'Response.Write "&nbsp&nbsp"
	'call Button_Criar("button","btn3","Ajuda?","onclick=javascript:Help('" & application("servidor") & "'," & novoID & ")")
	''''
	Response.Write	"  </table>" & _
					"</td>" & _
					"<td width='35%' align='center' valign='top'>"
end sub

sub Rel2_Fim(strArquivo)
	Response.Write "</td></tr>"
	call Form_Fim()
	Response.Write "</table>"
end sub

'' rel ini sem componente
sub Rel_Item_Ini(str1,str2,strDesc)
''' CRIAR ITEM SIMPLES (SEM COMPONENTE) DE PAR�METRO DE RELAT�RIO
'str1		=tam col esquerda (0)
'str2		=tam col direita  (0)
'strDesc	=desc col esquerda
	if str1=0 then str1="20"
	if str2=0 then str2="80"	
	
	Response.Write "<tr><td width='" & str1 & "%' align='right' valig='top' bgcolor='" & session("cor_rel_parametro") & "'><font face='verdana' size='1'>" & strDesc & ":</font></td>" + _
				   "<td width='" & str2 & "%' align='left' valign='top'>"
end sub

'' rel fim sem componente
sub Rel_Item_Fim()
''' FIM ITEM SIMPLES (SEM COMPONENTE) DE PAR�METRO DE RELAT�RIO
	Response.Write "</td></tr>"
end sub

'' rel item com txt
sub Rel_Item_txt(str1,str2,strDesc,strTipo,strNome,strTam,strMax,strValor)
''' CRIAR ITEM TEXT DE PAR�METRO DE RELAT�RIO
'str1		=tam col esquerda (0)
'str2		=tam col direita  (0)
'strDesc	=desc col esquerda
'strTipo	=tipo de campo
'strNome	=nome do campo
'strTam		=tam do campo
'strMax		=tam m�ximo
'strValor	=valor do campo

	if str1=0 then str1="20"
	if str2=0 then str2="80"	
	
	Response.Write "<tr><td width='" & str1 & "%' align='right' valig='top' bgcolor='" & session("cor_rel_parametro") & "'><font face='verdana' size='1'>" & strDesc & ":</font></td>" + _
				   "<td width='" & str2 & "%' align='left' valign='top'>"
	call Txt_Criar(strTipo,strNome,strTam,strMax,strValor)			 
	Response.Write "</td></tr>"
end sub

'' rel item com file
sub Rel_Item_file(str1,str2,strDesc,strNome,strTam,strMax,strValor)
''' CRIAR ITEM TEXT DE PAR�METRO DE RELAT�RIO
'str1		=tam col esquerda (0)
'str2		=tam col direita  (0)
'strDesc	=desc col esquerda
'strTipo	=tipo de campo
'strNome	=nome do campo
'strTam		=tam do campo
'strMax		=tam m�ximo
'strValor	=valor do campo

	if str1=0 then str1="20"
	if str2=0 then str2="80"	
	
	Response.Write "<tr><td width='" & str1 & "%' align='right' valig='top' bgcolor='" & session("cor_rel_parametro") & "'><font face='verdana' size='1'>" & strDesc & ":</font></td>" + _
				   "<td width='" & str2 & "%' align='left' valign='top'>"
	call Txtfile_Criar(strNome,strTam,strMax,strValor)			 
	Response.Write "</td></tr>"
end sub

'' rel item com txtSenha
sub Rel_Item_txtSenha(str1,str2,strDesc,strNome,strTam,strMax,strValor)
''' CRIAR ITEM TEXT DE PAR�METRO DE RELAT�RIO
'str1		=tam col esquerda (0)
'str2		=tam col direita  (0)
'strDesc	=desc col esquerda
'strTipo	=tipo de campo
'strNome	=nome do campo
'strTam		=tam do campo
'strMax		=tam m�ximo
'strValor	=valor do campo

	if str1=0 then str1="20"
	if str2=0 then str2="80"	
	
	Response.Write "<tr><td width='" & str1 & "%' align='right' valig='top' bgcolor='" & session("cor_rel_parametro") & "'><font face='verdana' size='1'>" & strDesc & ":</font></td>" + _
				   "<td width='" & str2 & "%' align='left' valign='top'>"
	call TxtSenha_Criar(strNome,strTam,strMax,strValor)			 
	Response.Write "</td></tr>"
end sub

'' rel item com select ini
sub Rel_Item_SelectINI(str1,str2,strDesc,strNome,opcao)
''' CRIAR ITEM SELECT DE PAR�METRO DE RELAT�RIO
'str1		=tam col esquerda (0)
'str2		=tam col direita  (0)
'strDesc	=desc col esquerda
'strNome	=nome do campo

	if str1=0 then str1="20"
	if str2=0 then str2="80"
	
	Response.Write "<tr><td width='" & str1 & "%' align='right' valig='top' bgcolor='" & session("cor_rel_parametro") & "'><font face='verdana' size='1'>" & strDesc & ":</font></td>" + _
				   "<td width='" & str2 & "%' align='left' valign='top'>"
	call Select_Criar(strNome,opcao)
end sub

'' rel item com select FIM
sub Rel_Item_SelectFIM
	call Select_Fim()
	Response.Write "</td></tr>"
end sub

'' checkbox
sub Rel_Checkbox_Criar(str1,str2,strDesc,strNome,strValor,strSel,strTexto,strOpcao)
	Response.Write "<tr><td width='" & str1 & "%' align='right' valig='top' bgcolor='" & session("cor_rel_parametro") & "'><font face='verdana' size='1'>" & strDesc & ":</font></td>" + _
				   "<td width='" & str2 & "%' align='left' valign='top'>"
				   
	call Checkbox_Criar(strNome,strValor,strSel,strTexto,strOpcao)	
	Response.Write "<font face='verdana' size='1'>" & strTexto & "</font>"
end sub

sub Rel_TextArea_Criar(str1,str2,strDesc,strNome,strRow,strCol,strTexto)
	Response.Write "<tr><td width='" & str1 & "%' align='right' valig='top' bgcolor='" & session("cor_rel_parametro") & "'><font face='verdana' size='1'>" & strDesc & ":</font></td>" + _
				   "<td width='" & str2 & "%' align='left' valign='top'>"
	call TextArea_Criar(strNome,strRow,strCol,strTexto)
	Response.Write "</td></tr>"
end sub

sub Rel_Item_Linha()
	Response.write "<td width='100%'  colspan='2'><hr size='1'></td>"
end sub




'Funcao para exibir um texto na linha ou uma linha em branco
'Par�metros: 
 '    texto -> Texto a ser exibido
 '    corFundo -> S: Exibe a cor padr�o de fundo de coluna N: Fundo fica branco  
sub Rel_Item_Texto(texto,corFundo)

    if ( ucase(corFundo)="S" ) then
	 corFundo = "bgcolor='" & session("cor_rel_parametro") & "'"
	else 
	 corFundo = ""
	end if

	Response.write "<tr><td width='100%' " &corFundo& " align='left' colspan='2'>"
	
	 Response.write "<font face='verdana' size='1'><strong>" & texto & "</strong>&nbsp; </font>"
	
	Response.write "</td></tr>"
end sub


sub Rel_Intervalo_codigo(str1,str2,strDesc,default1,default2,codt)
''' CRIAR intervalo de c�digos DE PAR�METRO DE RELAT�RIO
'str1		=tam col esquerda (0)
'str2		=tam col direita  (0)
'strDesc	=desc col esquerda
'default1	=valor default do campo 1
'default2	=valor default do campo 2

	if str1=0 then str1="20"
	if str2=0 then str2="80"
	
	Response.Write "<tr><td width='" & str1 & "%' align='right' valig='top' bgcolor='" & session("cor_rel_parametro") & "'><font face='verdana' size='1'>" & strDesc & ":</font></td>" + _
				   "<td width='" & str2 & "%' align='left' valign='top'>"
	call txt_item(default1,default2,codt)
	if fechaTD <> "N" then
		Response.Write "</td></tr>"
	end if	
end sub

'' rel item com periodo ini-fim
sub Rel_Item_Periodo(str1,str2,strDesc,default1,default2)
''' CRIAR ITEM PERIODO (DATA IN�CIO E DATA FIM) DE PAR�METRO DE RELAT�RIO
'str1		=tam col esquerda (0)
'str2		=tam col direita  (0)
'strDesc	=desc col esquerda
'default1	=valor default do campo 1
'default2	=valor default do campo 2

	if str1=0 then str1="20"
	if str2=0 then str2="80"
	
	'Response.Write "entrei"
	'Response.End
	
	Response.Write "<tr><td width='" & str1 & "%' align='right' valig='top' bgcolor='" & session("cor_rel_parametro") & "'><font face='verdana' size='1'>" & strDesc & ":</font></td>" + _
				   "<td width='" & str2 & "%' align='left' valign='top'>"
	call txt_Periodo(default1,default2)
	if fechaTD <> "N" then
		Response.Write "</td></tr>"
	end if	
end sub

'' rel item com periodo ini-fim 2
sub Rel_Item_Periodo2(str1,str2,strDesc,default1,default2)
''' CRIAR ITEM PERIODO (DATA IN�CIO E DATA FIM) DE PAR�METRO DE RELAT�RIO
'str1		=tam col esquerda (0)
'str2		=tam col direita  (0)
'strDesc	=desc col esquerda
'default1	=valor default do campo 1
'default2	=valor default do campo 2

	if str1=0 then str1="20"
	if str2=0 then str2="80"
	
	Response.Write "<tr><td width='" & str1 & "%' align='right' valig='top' bgcolor='" & session("cor_rel_parametro") & "'><font face='verdana' size='1'>" & strDesc & ":</font></td>" + _
				   "<td width='" & str2 & "%' align='left' valign='top'>"
	call txt_Periodo2(default1,default2)				   	
	Response.Write "</td></tr>"
end sub

'' rel item com mes/ano
sub Rel_Item_MesAno(str1,str2,strDesc,opcao,default1,default2)
''' CRIAR ITEM PERIODO (MES E ANO) DE PAR�METRO DE RELAT�RIO
'str1		=tam col esquerda (0)
'str2		=tam col direita  (0)
'strDesc	=desc col esquerda
'default1	=valor default do campo 1
'default2	=valor default do campo 2

	if str1=0 then str1="20"
	if str2=0 then str2="80"	
	
	Response.Write "<tr><td width='" & str1 & "%' align='right' valig='top' bgcolor='" & session("cor_rel_parametro") & "'><font face='verdana' size='1'>" & strDesc & ":</font></td>" + _
				   "<td width='" & str2 & "%' align='left' valign='top'>"
	call Select_Mes("strMes",opcao,default1)
	
	Response.Write "<font face='verdana' size='1'>/</font>"
	call Txt_Criar("ANO_REDIRECIONAR","strAno",4,5,default2)
	Response.Write "</td></tr>"
end sub

'' rel Fim
sub Rel_Fim(parametro_strArquivo)
	Response.Write "<tr><td width='100%' colspan='2' align='center' valign='top'>"
	if parametro_strArquivo<>"2" then
		call Button_Criar("submit","btn1",nomeBotao,"onclick='javascript:frm1.btn1.disabled=false'")
	end if
	if parametro_strArquivo="1" or parametro_strArquivo="2" then
		session("path_original")=Request.ServerVariables("PATH_INFO")
		session("path_nome_aplicacao")=StrNomeCab
		Response.Write "&nbsp&nbsp;&nbsp&nbsp;&nbsp&nbsp;"
		call Button_Criar("submit","btn2","Gerar Arquivo","")
	end if
	Response.Write "</td></tr>"
	call Form_Fim()
	Response.Write "</table>"
end sub

sub Rel_Fim1()
	Response.Write "<tr><td width='100%' colspan='2' align='center' valig='top'>"
	call Button_Criar("submit","btn1",nomeBotao,"")
	Response.Write "&nbsp&nbsp;&nbsp&nbsp;&nbsp&nbsp;"
	call Button_Criar("submit","btn2","Gerar Arquivo","")
	session("path_original")=Request.ServerVariables("PATH_INFO")
	session("path_nome_aplicacao")=StrNomeCab
	Response.Write strA
	'botao ajuda
	'Response.Write "&nbsp&nbsp"
	'call Button_Criar("button","btn3","Ajuda?","onclick=javascript:Help('" & application("servidor") & "'," & novoID & ")")
	''''
	Response.Write "</td></tr>"
'	call Form_Fim()
'	Response.Write "</table>"
end sub
'' fim de parametros para relat�rios

sub Redi_Aviso(titulo,mensagem)	
	session("msgAviso")	=mensagem
	Response.Redirect "http://" & application("servidor") & "/default/def_Aviso.asp?t=" & titulo
end sub

sub Redi_Arquivo(sqlq,Opcao)
	session("strSql") = sqlq
	
	Response.Redirect "http://" & application("servidor") & "/default/def_GerenciadorArquivo.asp?c=" & opcao
	
	'if opcao="EXCEL" then
'		Response.Redirect "http://" & application("servidor") & "/default/def_GerarArquivo.asp?c=" & opcao
	'elseif opcao="TXT" then
	'	Response.Redirect "http://" & application("servidor") & "/default/def_GerarArquivoTXT.asp?c=" & opcao
	'end if
end sub

sub Redi_ArquivoTXT(sqlq,Opcao)
	session("strSql") = sqlq	
	Response.Redirect "http://" & application("servidor") & "/default/def_GerenciadorArquivo.asp?c=" & opcao
	'Response.Redirect "http://" & application("servidor") & "/default/def_GerarArquivoTXT.asp?c=" & opcao
end sub

sub Ado_Rs1(sql)
	on error resume next
	set adoRs1=conn1.execute(sql)	
	if err.number<>0 then
		call Redi_Aviso("Erro para executar comando SQL",err.Description & "<p>" & sql)
	end if	
end sub

sub fun_rel_det()
	if iquebra > 0 then
		for di = iquebra to 1  step -1
			if adoRs1(qb_cpo(di)) <> qb_ant(di) and len(qb_ant(di)) > 0 then
				for dj = 1 to di
					func_totaliza(dj)
					qb_ant(dj) = adoRs1(qb_cpo(dj))
					if len(qb_cab(dj)) > 0 then
						novo_cab(dj) =  qb_cab(dj)							
					end if
				next
			end if
			qb_ant(di) = adoRs1(qb_cpo(di))
		next						
	end if
	if intContaLinha >= imp_MaxLinha then
		call Func_ImpPag(0,iColuna)
		Response.Write "</table></td></tr></table>"
		Response.Write "<p class='quebra'></p>"
		intContaPag=intContaPag+1
		call fun_rel_cab()
	end if
	if iquebra > 0 then		
		for dj = iquebra to 1  step -1
			a = "-"
		'' link quebra
			if len(trim(iLinkqb(dj)))>0 then
				linkqb_string=iLinkqb(dj)			
				linkqb_a=instr(linkqb_string,"[")
				linkqb_b=instr(linkqb_string,"]")
					
				while linkqb_a>0					
					'Response.Write link_string & "-" & link_a & "-" & link_b & "<BR>"
					'Response.Write mid(link_string,link_a+1,link_b-link_a-1)
					linkqb_string=mid(linkqb_string,1,linkqb_a-1) & adors1(mid(linkqb_string,linkqb_a+1,linkqb_b-linkqb_a-1)) & mid(linkqb_string,linkqb_b+1,len(linkqb_string)-linkqb_b)					
					
					linkqb_a=instr(linkqb_string,"[")
					linkqb_b=instr(linkqb_string,"]")
					
				wend		
				'Response.Write link_String		
				linkqb_string ="<a href=" & chr(34) & linkqb_String & chr(34) & ">"			
			end if
			if len(novo_cab(dj)) > 0 then
				a = ""
				for i = 1 to iquebra - dj
					a = a + "&nbsp&nbsp&nbsp&nbsp&nbsp"
				next
				if len(qb_corFundo(dj)) > 0 then
					corFundo = qb_corFundo(dj)
				else
					corFundo = session("cor_rel_subtotal")
				end if
				if len(qb_corFonte(dj)) > 0 then
					corFonte = "Color='" & qb_corFonte(dj) & "'"
				else
					corFonte = ""
				end if
				if qb_tamanho(dj) > 0 then
					tamanho = qb_tamanho(dj)
				else
					tamanho = 1
				end if
				if qb_negrito(dj) = 1 then
					negrito1 = "<b>"
					negrito2 = "</b>"
				else
					negrito1 = ""
					negrito2 = ""
				end if
				Response.Write "<tr>"
				Response.Write "<td width='100%' colspan='" & iColuna & "' bgcolor='" & corFundo & "' align='left'>"
				Response.Write "<font face='Arial' " & corFonte & " size='" & tamanho & "' " & formatofont(j) & ">" & negrito1 & linkqb_string & a & adoRs1(novo_cab(dj)) & negrito2 & "</a></td>"
				Response.Write "</tr>"
				novo_cab(dj) = ""
				intContaLinha=intContaLinha + 1
			end if				
		next
	end if
	
	if Fundo_Alterna=1 then
		if fundo_conta = "0" then
			fundo_aux = "bgcolor='" & session("cor_rel_alternar") & "'"
			fundo_conta="1"		
		else
			fundo_aux = ""
			fundo_conta="0"
		end if
		fundo=fundo_aux		
	else
		fundo=""
	end if

	if len(linhaCab) > 0 then
		Response.Write linhaCab
		intContaLinha=intContaLinha + 1
	end if

	intContaLinha=intContaLinha + 1
	response.write "<tr>"
	todos = "n"
	
	for j = 1 to iColuna
'	Response.Write  cstr(cpo(j)) 
'	Response.end
		if isnumeric(cpo(j)) then
			var_campo = variavel(cstr(mid(cpo(j),1,2)))
		else
			var_campo = adoRs1(cpo(j))
		end if
		''cor de fundo da tabela
		fonte = "black"
		if len(trim(corColunaFundo(j)))>0 then				
			if mid(corColunaFundo(j),1,1)="$" then			
				fundo = "bgcolor='" & adoRs1(right(corColunaFundo(j),len(corColunaFundo(j))-1)) & "'"
			elseif mid(corColunaFundo(j),1,1)="@" then
				fundo = ""
				fonte = adoRs1(right(corColunaFundo(j),len(corColunaFundo(j))-1)) 
			elseif mid(corColunaFundo(j),1,1)="#" then
				fundo = "bgcolor='" & right(corColunaFundo(j),len(corColunaFundo(j))-1) & "'"
			end if
		else
			fundo=fundo_aux
		end if
		
		if ali(j) = "E" then alid(j) = "left" end if
		if ali(j) = "C" then alid(j) = "center" end if
		if ali(j) = "D" then alid(j) = "right" end if
		cpod(j) = ""
		if len(cpo(j)) > 0  then
	  		if isnull(var_campo) then
				cpod(j) = "xxxx"
			end if
		end if
		if cpod(j) = "xxxx" then
			cpod(j) = ""
		else
			if len(cpo(j)) > 0 then
				mostra = var_campo
			else
				mostra = tcoluna(j)
			end if
			
'			primeiromaior=instr(1,mostra,"<")
'			if primeiromaior > 0 then
'				primeirohref=mid(mostra,primeiromaior,instr(1,mostra,">"))
'				segundohref=mid(mostra,instr(primeiromaior+1,mostra,"<"),instr(instr(primeiromaior+1,mostra,"<"),mostra,">"))
'				Response.Write(primeirohref & " " & segundohref & "<BR>")
'			end if
						
			if fmt(j) = "T" then cpod(j) = mostra end if
			if fmt(j) = "0" then
				if len(trim(formata_qtd))>0 then
					cpod(j) = formatNumber(mostra,formata_qtd)
				else
					cpod(j) = formatNumber(mostra,0)
				end if
			end if
			
			if fmt(j) = "1" then
				if len(trim(formata_qtd))>0 then
					cpod(j) = formatNumber(mostra,formata_qtd)
				else
					cpod(j) = formatNumber(mostra,1)
				end if
			end if
			if fmt(j) = "2" then
				if len(trim(formata_qtd))>0 then
					cpod(j) = formatNumber(mostra,formata_qtd)
				else
					cpod(j) = formatNumber(mostra,2)
				end if
			end if
			if fmt(j) = "3" then
				if len(trim(formata_qtd))>0 then
					cpod(j) = formatNumber(mostra,formata_qtd)
				else
					cpod(j) = formatNumber(mostra,3)
				end if
			end if
			if fmt(j) = "4" then
				if len(trim(formata_qtd))>0 then
					cpod(j) = formatNumber(mostra,formata_qtd)
				else
					cpod(j) = formatNumber(mostra,4)
				end if
			end if
			if ucase(fmt(j)) = "CPF" then  ''formatar para Cpf/Cnpj
				cpod(j) = cpf_formata(mostra)
			end if
			if ucase(fmt(j)) = "CEP" then  ''formatar para Cpf/Cnpj
				cpod(j) = cep_formata(mostra)
			end if
			
			'nao mostrar nada quando valor=0			
			if fmt(j)<>"T" and ucase(fmt(j))<>"CPF" and ucase(fmt(j))<>"CEP"  then	
				if cdbl(cpod(j))=cdbl(0) and not retzero then cpod(j)=""			
			end if
			
		end if
		'fun_mostra(cpod(j))
		if repete(j) = "N" and len(cpo(j)) > 0 then
			'on error resume next
		  if isnull(var_campo)  then
				cpoant(j) = ""
				cpod(j) = ""
		  else
				if cstr(var_campo) = cstr(cpoant(j)) and todos = "n" then
					cpod(j) = ""
				else
					todos = "s"
				end if
				cpoant(j) = var_campo
		  end if
		end if
		if semDetalhe <> "S" then
			Response.Write "<td width='" & tam(j) & tipo_tam_coluna & "' " & fundo & " align='" & alid(j) & "'>"
		end if
	'' link
	'Response.Write	iLink(j)
		if len(trim(iLink(j)))>0 then
			link_string=iLink(j)			
			link_a=instr(link_string,"[")
			link_b=instr(link_string,"]")
				
			while link_a>0					
				'Response.Write link_string & "-" & link_a & "-" & link_b & "<BR>"
				'Response.Write mid(link_string,link_a+1,link_b-link_a-1)
				link_string=mid(link_string,1,link_a-1) & adors1(mid(link_string,link_a+1,link_b-link_a-1)) & mid(link_string,link_b+1,len(link_string)-link_b)					
				
				link_a=instr(link_string,"[")
				link_b=instr(link_string,"]")				
			wend		
			'Response.Write link_String
			if len(trim(iLinkTarget(j)))>0 then
				itarget=" target='" & iLinkTarget(j) & "'"
			else
				itarget=""
			end if
			'Response.write itarget
				
			if semDetalhe <> "S" then
				Response.Write "<a href=" & chr(34) & link_String & chr(34) & itarget & ">"			
			end if
		end if
	''
		if semDetalhe <> "S" then
			Response.write "<font face='Arial' size='1' color='" &  fonte & "' "  & formatofont(j) & ">" & cpod(j) & "</font></a></td>"
		end if
		if ctotal(j) = "S" or ctotal(j) = "A" or ctotal(j) = "C" then
			for n = 1 to 9
				if len(cpod(j)) > 0 then
					if ctotal(j) = "C" then
						tg(j,n) = tg(j,n) + 1
					else
						tg(j,n) = tg(j,n) + cdbl(cpod(j))
					end if
				end if
'				Response.Write (j & " " & n & " " & tg(j,n) & "<BR>")
			next
		end if
	next 
	if semDetalhe <> "S" then
		response.write "</tr>"
	end if
	
	if len(linharodape) > 0 then
		Response.Write linharodape
		intContaLinha=intContaLinha + 1
	end if
	
end sub

sub zerar_relatorio()
	for i = 1 to icoluna
		desc(i)	= ""
		cpo(i)	= ""
		tam(i)	= 0
		ali(i)	= ""
		fmt(i)	= 0
		ctotal(i) = ""
		repete(i) = ""
		tcoluna(i)	= 0
		iLink(i)=""
		iLinkTarget(i)=""
		qb_lista_ant(i)=""
		qb_ant(i) = ""
		corColunaFundo(i)=""
	next
	for h = 0 to icoluna
		for i = 0 to 9
			if ctotal(h) <> "A" then
				tg(h,i) = 0
			end if
		next
	next
	iColuna=0
	intcontalinha=0
	iquebra=0
	impcab=0
	fundo_conta=""
end sub

sub fun_rel_cab()
	session("strSql") = strsql
	if len(trim(session("strSql")))=0 then session("strSql")=sqlq
	if configura_arquivo=false then
		session("arquivo_titulo")=""
		session("arquivo_campo")=""
	end if

x=impcab
y=impressao_Tipo
if x=1 then
	if impressao_Tipo="P" then
		imp_MaxLinha=45
	%>
		<!--#include virtual='public/pub_BodyRel1000.asp'-->
	<%else%>
		<!--#include virtual='public/pub_BodyRel750.asp'-->
<%	end if
end if%>
<table style="border-collapse: collapse" bordercolor="#C0C0C0" border=<%=tableborder%> width="<%=tam_tabela%><%=tipo_tam_tabela%>" cellspacing="1" cellpadding="0" align="left">
<%
	call fun_Rel_Cab_Colunas()
intContalinha = 0%>
<%end sub

sub fun_Rel_Cab_Colunas()
	Response.Write strParametroTab1
	Response.Write "<tr>"
	for j = 1 to iColuna		
		%><td width="<%=tam(j)%><%=tipo_tam_coluna%>" bgcolor="<%=session("cor_rel_topo")%>" align="center">
			<%if len(trim(linkCab(j)))>0 then
				Response.Write "<a href='" & trim(linkCab(j)) & "'>"
			  end if
			%>
		<font face="Arial" size="1" color="black" <%=formatofont(j)%>><%=desc(j)%></font></a></td><%
    next
    response.write "</tr>"
end sub

sub func_totaliza(t)
	if qb_lista_ant(t) = "P" then
		exit sub
	end if
	if t < 9 then
		fundo = session("cor_rel_subtotal")
	else
		fundo = session("cor_rel_total")
	end if
	span = 0
	tem = "n"
	for j = 1 to iColuna
	  if ctotal(j) = "S" or ctotal(j) = "A" or ctotal(J) = "C" then
		if tem = "n" then
			intContaLinha=intContaLinha + 1
			response.write "<tr>"
			tem = "s"
	    end if 
	    if span > 0 and span < 99 then
			if qb_lista_ant(t) = "N" then
				qb_ant(t)= ""
			end if
'			Response.Write span
			a = ""
			for i = 1 to iquebra - t
				a = a + "&nbsp&nbsp&nbsp&nbsp&nbsp"
			next
			Response.Write "<td width='*%' bgcolor='" & fundo & "' colspan=" & span & "><p align='left'>"
			Response.write "	<font face='Arial' size='1' " & formatofont(j) & ">" & a & qb_desc(t) & qb_ant(t) & "</font></td>"
		end if			
		if ali(j) = "E" then alid(j) = "left" end if
		if ali(j) = "C" then alid(j) = "center" end if
		if ali(j) = "D" then alid(j) = "right" end if
		
		d = t
		if t=10 then ''m�dia
			dividir=strMediaGeral
			d = 9
		else
			dividir=1
		end if
		if ctotal(J) = "C" then 
			d = cstr(tg(j,d))
		else
			if fmt(j) = "0" then d = formatNumber(tg(j,d)/dividir,0) end if
			if fmt(j) = "1" then d = formatNumber(tg(j,d)/dividir,1) end if
			if fmt(j) = "2" then d = formatNumber(tg(j,d)/dividir,2) end if
			if fmt(j) = "3" then d = formatNumber(tg(j,d)/dividir,3) end if
			if fmt(j) = "4" then d = formatNumber(tg(j,d)/dividir,4) end if
		end if
		
		if t = 9 and len(tot_link(j)) > 0 then
			link = tot_link(j)
		else
			link = ""
		end if
		'fun_mostra(cpod(j))
			Response.Write "<td width='" & tam(j) & tipo_tam_coluna & "' bgcolor='" & fundo & "'><p align='" & alid(j) & "'>"
			Response.write "	<font face='Arial' size='1' " & formatofont(j) & ">" & link & d & "</font></td>"
			if t < 9 then
				if ctotal(j) <> "A" then
					tg(j,t) = 0
				end if
			end if
			span = 99
	  else
		if span <> 99 then
			span = j
		else
'			if qb_lista_ant(t) <> "P" then
				Response.Write "<td width='" & tam(j) & tipo_tam_coluna & "' bgcolor='" & fundo & "'><p align='" & alid(j) & "'>"
				Response.write "	<font face='Arial' size='1' " & formatofont(j) & "></font></td>"
'			end if
		end if
	  end if	  
	  cpoant(j)=""
	next 
	if tem = "s" then
		response.write "</tr>"
	end if
	if qb_traco then
		Response.Write ("<TR><TD colspan=" & j & " height=5><HR></td></tr>")
		intContaLinha=intContaLinha + 1
	end if
end sub

sub define_coluna(a,b,c,d,e,f,g,h,i)
	icoluna = icoluna + 1
	desc(icoluna)	= a
	cpo(icoluna)	= b
	tam(icoluna)	= c
	ali(icoluna)	= ucase(d)
	fmt(icoluna)	= ucase(e)
	ctotal(icoluna) = ucase(f)
	repete(icoluna) = ucase(g)
	tcoluna(icoluna)= icoluna
	iLink(iColuna)=""
	corColunaFundo(icoluna)=h
	formatofont(icoluna)=i
end sub

sub define_quebra(a,b,c,d)
	iquebra = iquebra + 1
	qb_cpo(iquebra) = ucase(b)
	qb_desc(iquebra) = ucase(a)
	qb_lista_ant(iquebra) = ucase(c)
	qb_cab(iquebra) = ucase(d)
	if len(d) > 0 then
		novo_cab(iquebra) = ucase(d)
	end if
	qb_ant(iquebra) = ""
end sub

sub define_quebra1(a,b,c,d,e,f,g)
	qb_corFonte(iquebra) = ucase(a)
	qb_corFundo(iquebra) = ucase(b)
	qb_negrito(iquebra) = ucase(c)
	qb_tamanho(iquebra) = ucase(d)
end sub

sub define_link(a,b)
	iLink(a)=b
end sub

sub define_linkTarget(a,b)
	iLinkTarget(a)=b
end sub

sub define_LinkCab(a,b)
	linkCab(a)=b
end sub

sub define_linkqb(a,b)
	iLinkqb(a)=b
end sub

sub fun_fim_rel()
	set adors1=nothing
	for dj = 1 to iquebra	
		func_totaliza(dj)
	next
	call Func_Totaliza(9)	
end sub

sub Label(a,b,c,d)
	'a - texto
	'b - tamanho
	'c - alinhamento
	'd - cor	
	if len(trim(c))=0 then c="left"
	if len(trim(d))=0 then d="black"
	Response.Write "<p align='" & c & "'><font face='verdana' size='" & b & "' color='" & d & "'>" & a & "</font>"
end sub

%>
